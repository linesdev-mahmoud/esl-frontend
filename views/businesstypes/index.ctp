<div class="side-app">

	<!-- page-header -->
	<div class="page-header">
		<ol class="breadcrumb">
			<!-- breadcrumb -->
			<li class="breadcrumb-item"><a href="<?= $this->Html->url('/') ?>">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Business Types</li>
		</ol><!-- End breadcrumb -->
		<div class="ml-auto">
			<div class="input-group">
				<a href="<?= $this->Html->url('/Businesstypes/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add New Business Type</a>

			</div>
		</div>
	</div>
	<!-- End page-header -->

	<!-- row -->
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="card">
				<div class="card-header pb-0">
					<div class="card-title">Business Types</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered text-nowrap w-100">
							<thead>

								<tr>
									<th class="wd-15p">#</th>
									<th class="wd-15p">Title</th>
									<th class="wd-15p">Title Arabic</th>
									<th class="wd-15p">Actions</th>
								</tr>
							</thead>
							<tbody>
							    <?php
	$i = 0;
	foreach ($businesstypes as $businesstype):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $businesstype['Businesstype']['id']; ?>&nbsp;</td>
		<td><?php echo $businesstype['Businesstype']['title']; ?>&nbsp;</td>
		<td><?php echo $businesstype['Businesstype']['title_ar']; ?>&nbsp;</td>
		<td class="actions">
										<div class="input-group">
											
											<?= $this->Html->link(
								'<span><i class="si si-pencil"></i></span>',
								['action' => 'edit',  $businesstype['Businesstype']['id']],
								['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
							) ?>
											<?= $this->Html->link(
								'<span><i class="si si-trash"></i></span>',
								['action' => 'delete',  $businesstype['Businesstype']['id']],
								['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $businesstype['Businesstype']['id'])
							) ?>
										</div>
									</td>
	</tr>
<?php endforeach; ?>

							</tbody>
						</table>
					</div>
				</div>
				<!-- table-wrapper -->
			</div>
			<!-- section-wrapper -->
		</div>
	</div>
	<!-- row end -->

</div>
<!--

<div class="businesstypes index">
	<h2><?php __('Businesstypes');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('title_ar');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($businesstypes as $businesstype):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $businesstype['Businesstype']['id']; ?>&nbsp;</td>
		<td><?php echo $businesstype['Businesstype']['title']; ?>&nbsp;</td>
		<td><?php echo $businesstype['Businesstype']['title_ar']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $businesstype['Businesstype']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $businesstype['Businesstype']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $businesstype['Businesstype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $businesstype['Businesstype']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Businesstype', true), array('action' => 'add')); ?></li>
	</ul>
</div>-->
