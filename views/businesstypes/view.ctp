<div class="businesstypes view">
<h2><?php  __('Businesstype');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businesstype['Businesstype']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businesstype['Businesstype']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title Ar'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $businesstype['Businesstype']['title_ar']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Businesstype', true), array('action' => 'edit', $businesstype['Businesstype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Businesstype', true), array('action' => 'delete', $businesstype['Businesstype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $businesstype['Businesstype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Businesstypes', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Businesstype', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
