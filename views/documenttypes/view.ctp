<div class="documenttypes view">
<h2><?php  __('Documenttype');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $documenttype['Documenttype']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $documenttype['Documenttype']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title Ar'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $documenttype['Documenttype']['title_ar']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Documenttype', true), array('action' => 'edit', $documenttype['Documenttype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Documenttype', true), array('action' => 'delete', $documenttype['Documenttype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $documenttype['Documenttype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Documenttypes', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Documenttype', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
