<div class="contacts index">
	<h2><?php __('Contacts');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('customer_id');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('first_name');?></th>
			<th><?php echo $this->Paginator->sort('last_name');?></th>
			<th><?php echo $this->Paginator->sort('position');?></th>
			<th><?php echo $this->Paginator->sort('mobile_country_code');?></th>
			<th><?php echo $this->Paginator->sort('mobile_code_no');?></th>
			<th><?php echo $this->Paginator->sort('mobile_number');?></th>
			<th><?php echo $this->Paginator->sort('email');?></th>
			<th><?php echo $this->Paginator->sort('contacttypes_id');?></th>
			<th><?php echo $this->Paginator->sort('is_main');?></th>
			<th><?php echo $this->Paginator->sort('password');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($contacts as $contact):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $contact['Contact']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($contact['Customer']['id'], array('controller' => 'customers', 'action' => 'view', $contact['Customer']['id'])); ?>
		</td>
		<td><?php echo $contact['Contact']['title']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['first_name']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['last_name']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['position']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['mobile_country_code']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['mobile_code_no']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['mobile_number']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['email']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($contact['Contacttypes']['title'], array('controller' => 'contacttypes', 'action' => 'view', $contact['Contacttypes']['id'])); ?>
		</td>
		<td><?php echo $contact['Contact']['is_main']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['password']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['status']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['created']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['modified']; ?>&nbsp;</td>
		<td><?php echo $contact['Contact']['modified_by']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $contact['Contact']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $contact['Contact']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $contact['Contact']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $contact['Contact']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contact', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers', true), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer', true), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacttypes', true), array('controller' => 'contacttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contacttypes', true), array('controller' => 'contacttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>