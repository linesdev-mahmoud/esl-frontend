<div class="contacttypes view">
<h2><?php  __('Contacttype');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $contacttype['Contacttype']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $contacttype['Contacttype']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title Ar'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $contacttype['Contacttype']['title_ar']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contacttype', true), array('action' => 'edit', $contacttype['Contacttype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Contacttype', true), array('action' => 'delete', $contacttype['Contacttype']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $contacttype['Contacttype']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacttypes', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contacttype', true), array('action' => 'add')); ?> </li>
	</ul>
</div>
