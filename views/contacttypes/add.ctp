<div class="side-app">
	<!-- page-header -->
	<div class="page-header">
		<ol class="breadcrumb">
			<!-- breadcrumb -->
			<li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/Contacttypes"); ?>">Contact Types</a></li>
			<li class="breadcrumb-item active" aria-current="page">Add Contact Type</li>
		</ol><!-- End breadcrumb -->
	</div>
	<!-- End page-header -->
	<!-- row -->
	<div class="row">
		<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Add Contact Type</h3>
				</div>
				<div class="card-body">
					<?php echo $this->Form->create('Contacttype', array('type'=>'file','class' => 'form-horizontal form-material'));?>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="form-group">
								<label for="exampleInputname">Title</label>
								<?php
                    echo $this->Form->input('title', array('label'=> false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
                    ?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12">
							<div class="form-group">
								<label for="exampleInputname1">Title Arabic</label>
								<?php
                    echo $this->Form->input('title_ar', array('label'=>false,'placeholder'=>'Enter Title arabic','class' => 'form-control form-control-line'));
                    ?>
							</div>
						</div>
					</div>
					
					<input type="submit" style="float:right" value="Submit" class="btn btn-success">
				</div>

			</div>
		</div>
	</div>
	<!-- row end -->

</div>
<!--


<div class="contacttypes form">
<?php echo $this->Form->create('Contacttype');?>
	<fieldset>
		<legend><?php __('Add Contacttype'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('title_ar');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contacttypes', true), array('action' => 'index'));?></li>
	</ul>
</div>-->
