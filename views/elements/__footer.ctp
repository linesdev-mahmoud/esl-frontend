<!--footer-->
<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12 col-sm-12   text-center">
                Copyright © 2019 <a href="#">Sparic</a>. Designed by <a href="https://www.spruko.com/">Spruko</a> All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->

</div>
<!-- End app-content-->
</div>
</div>
<!-- End Page -->

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<!-- Jquery js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/js/vendors/jquery-3.2.1.min.js"></script>

<!--Bootstrap.min js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/bootstrap/popper.min.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!--Jquery Sparkline js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/js/vendors/jquery.sparkline.min.js"></script>

<!-- Chart Circle js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/js/vendors/circle-progress.min.js"></script>

<!-- Star Rating js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/rating/jquery.rating-stars.js"></script>

<!--Moment js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/moment/moment.min.js"></script>

<!-- Daterangepicker js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<!--Side-menu js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/side-menu/sidemenu.js"></script>

<!-- Horizontal-menu js -->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/horizontal-menu/horizontalmenu.js"></script>

<!--News Ticker js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/newsticker/breaking-news-ticker.min.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/newsticker/newsticker.js"></script>

<!-- Sidebar Accordions js -->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/sidemenu-responsive-tabs/js/easyResponsiveTabs.js"></script>

<!-- Perfect scroll bar js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/pscrollbar/perfect-scrollbar.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/pscrollbar/pscroll.js"></script>

<!-- Rightsidebar js -->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/sidebar/sidebar.js"></script>

<!--Time Counter js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/counters/jquery.missofis-countdown.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/counters/counter.js"></script>

<!-- ApexChart -->
<script src="<?php echo $this->Html->url("/"); ?>assets/js/apexcharts.js"></script>

<!-- Charts js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/chart/chart.bundle.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/chart/utils.js"></script>

<!--Peitychart js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/peitychart/jquery.peity.min.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/peitychart/peitychart.init.js"></script>
<!--MutipleSelect js-->
		<script src="../assets/plugins/multipleselect/multiple-select.js"></script>
		<script src="../assets/plugins/multipleselect/multi-select.js"></script>

<!-- Custom-charts js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/js/index1.js"></script>

<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/jquery.dataTables.min.js"></script>
		<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>
		<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/datatable.js"></script>
		<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/datatable-2.js"></script>
		<script src="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/dataTables.responsive.min.js"></script>

<!-- Custom js-->
<script src="<?php echo $this->Html->url("/"); ?>assets/js/custom.js"></script>

</body>
</html>