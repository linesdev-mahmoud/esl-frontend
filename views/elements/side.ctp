<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <div class="side-tab-body p-0 border-0" id="parentVerticalTab">
        <div class="first-sidemenu">
            <ul class="resp-tabs-list hor_1">
                <li class="resp-tab-active active" data-toggle="tooltip" data-placement="right" title="Home"><i class="side-menu__icon typcn typcn-device-desktop"></i><div class="slider-text">Home</div></li>
                <li data-toggle="tooltip" data-placement="right" title="CMS"><i class="fa fa-newspaper-o"></i><div class="slider-text">CMS</div></li>
                <li data-toggle="tooltip" data-placement="right" title="Standard"><i class="fe fe-layers"></i><div class="slider-text">Standard</div></li>
                <li data-toggle="tooltip" data-placement="right" title="Staff"><i class="fa fa-group"></i><div class="slider-text">Staff</div></li>
                <li data-toggle="tooltip" data-placement="right" title="Elements"><i class="side-menu__icon typcn typcn-spanner-outline"></i><div class="slider-text">Elements</div></li>
                <li data-toggle="tooltip" data-placement="right" title="Icons"><i class="side-menu__icon typcn typcn-point-of-interest-outline"></i><div class="slider-text">Icons</div></li>
                <li data-toggle="tooltip" data-placement="right" title="Pages"><i class="side-menu__icon typcn typcn-cog-outline"></i><div class="slider-text">Pages</div></li>
                <li data-toggle="tooltip" data-placement="right" title="Sign Out"><i class="mdi mdi-logout-variant"></i><a href="<?php echo $this->Html->url("/admins/logout"); ?>" class="slider-text">Sign Out</a></li>
            </ul>
            <div class="second-sidemenu">
                <div class="resp-tabs-container hor_1">
                    <div class="resp-tab-content-active">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="typcn typcn-device-desktop"></i> Home</h4>
                                <a class="slide-item" href="index.html">Dashboard 01</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Notifications</h6>
                                <div class="row p-2">
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0">
                                            <i class="ti-headphone fs-30 text-secondary"></i>
                                            <a><small class="mb-0">Support</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center">
                                            <i class="ti-bell fs-30 text-warning"></i>
                                            <a><small class="mb-0">Notify</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0 border-top-0">
                                            <i class="ti-panel fs-30 text-primary"></i>
                                            <a><small class="mb-0">Settings</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-top-0">
                                            <i class="ti-layers fs-30 text-danger"></i>
                                            <a><small class="mb-0">Layouts</small></a>
                                        </div>
                                    </div>
                                </div>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <h6 class="font-weight-semibold mt-5 mb-4">Sales Updates</h6>
                                <div class="mb-4">
                                    <p class="mb-2">Daily<span class="float-right text-default">85%</span></p>
                                    <div class="progress progress-sm mb-3 h-1">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-secondary w-85"></div>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <p class="mb-2">Weekly<span class="float-right text-default">75%</span></p>
                                    <div class="progress progress-sm mb-3 h-1">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary w-75"></div>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <p class="mb-2">Monthly<span class="float-right text-default">70%</span></p>
                                    <div class="progress progress-sm mb-3 h-1">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger w-70"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="fa fa-newspaper-o"></i> CMS</h4>
                                <a href="<?php echo $this->Html->url("/apages/posts"); ?>" class="slide-item">News</a>
                                <a href="<?php echo $this->Html->url("/apages"); ?>" class="slide-item">Pages</a>
                                <a href="<?php echo $this->Html->url("/settings"); ?>" class="slide-item">Settings</a>
                                <a href="<?php echo $this->Html->url("/banners"); ?>" class="slide-item">Sliders</a>
                                <a href="<?php echo $this->Html->url("/partners"); ?>" class="slide-item">Partners / Clients</a>
                                <a href="<?php echo $this->Html->url("/seo_urls"); ?>" class="slide-item">SEO urls</a>
                                <a href="<?php echo $this->Html->url("/seo_attributes"); ?>" class="slide-item">SEO attributes</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <h6 class="font-weight-semibold mt-5 mb-4">Notifications</h6>
                                <div class="row p-2">
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0">
                                            <i class="ti-headphone fs-30 text-secondary"></i>
                                            <a><small class="mb-0">Support</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center">
                                            <i class="ti-bell fs-30 text-warning"></i>
                                            <a><small class="mb-0">Notify</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0 border-top-0">
                                            <i class="ti-panel fs-30 text-primary"></i>
                                            <a><small class="mb-0">Settings</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-top-0">
                                            <i class="ti-layers fs-30 text-danger"></i>
                                            <a><small class="mb-0">Layouts</small></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="fe fe-layers"></i> Standard</h4>
                                <a href="<?php echo $this->Html->url("/standards"); ?>" class="slide-item">Standard</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <h6 class="font-weight-semibold mt-5 mb-4">Notifications</h6>
                                <div class="row p-2">
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0">
                                            <i class="ti-headphone fs-30 text-secondary"></i>
                                            <a><small class="mb-0">Support</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center">
                                            <i class="ti-bell fs-30 text-warning"></i>
                                            <a><small class="mb-0">Notify</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0 border-top-0">
                                            <i class="ti-panel fs-30 text-primary"></i>
                                            <a><small class="mb-0">Settings</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-top-0">
                                            <i class="ti-layers fs-30 text-danger"></i>
                                            <a><small class="mb-0">Layouts</small></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="fa fa-group"></i> Staff</h4>
                                <a href="<?php echo $this->Html->url("/users"); ?>" class="slide-item">Staff</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <h6 class="font-weight-semibold mt-5 mb-4">Notifications</h6>
                                <div class="row p-2">
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0">
                                            <i class="ti-headphone fs-30 text-secondary"></i>
                                            <a><small class="mb-0">Support</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center">
                                            <i class="ti-bell fs-30 text-warning"></i>
                                            <a><small class="mb-0">Notify</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0 border-top-0">
                                            <i class="ti-panel fs-30 text-primary"></i>
                                            <a><small class="mb-0">Settings</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-top-0">
                                            <i class="ti-layers fs-30 text-danger"></i>
                                            <a><small class="mb-0">Layouts</small></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="typcn typcn-spanner-outline"></i> Elements</h4>
                                <a href="alerts.html" class="slide-item">Alerts</a>
                                <a href="buttons.html" class="slide-item">Buttons</a>
                                <a href="colors.html" class="slide-item">Colors</a>
                                <a href="avatars.html" class="slide-item">Avatars</a>
                                <a href="dropdown.html" class="slide-item">Drop downs</a>
                                <a href="thumbnails.html" class="slide-item">Thumbnails</a>
                                <a href="mediaobject.html" class="slide-item">Media Object</a>
                                <a href="list.html" class="slide-item">List</a>
                                <a href="tags.html" class="slide-item">Tags</a>
                                <a href="pagination.html" class="slide-item">Pagination</a>
                                <a href="navigation.html" class="slide-item">Navigation</a>
                                <a href="typography.html" class="slide-item">Typography</a>
                                <a href="breadcrumbs.html" class="slide-item">Breadcrumbs</a>
                                <a href="badge.html" class="slide-item">Badges</a>
                                <a href="jumbotron.html" class="slide-item">Jumbotron</a>
                                <a href="panels.html" class="slide-item">Panels</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-primary mt-3 btn-block"><i class="fe fe-eye mr-1 mt-1"></i> View All</a>
                                <a href="#" class="btn btn-secondary mt-3 btn-block"><i class="fe fe-plus mr-1 mt-1"></i> Add New page</a>
                            </div>
                        </div>
                    </div>
                   
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="typcn typcn-point-of-interest-outline"></i> Icons</h4>
                                <a href="icons.html" class="slide-item">Font Awesome</a>
                                <a href="icons2.html" class="slide-item">Material Design Icons</a>
                                <a href="icons3.html" class="slide-item">Simple Line Iocns</a>
                                <a href="icons4.html" class="slide-item">Feather Icons</a>
                                <a href="icons5.html" class="slide-item">Ionic Icons</a>
                                <a href="icons6.html" class="slide-item">Flag Icons</a>
                                <a href="icons7.html" class="slide-item">pe7 Icons</a>
                                <a href="icons8.html" class="slide-item">Themify Icons</a>
                                <a href="icons9.html" class="slide-item">Typicons Icons</a>
                                <a href="icons10.html" class="slide-item">Weather Icons</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <h6 class="font-weight-semibold mt-5 mb-4">Notifications</h6>
                                <div class="row p-2">
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0">
                                            <i class="ti-headphone fs-30 text-secondary"></i>
                                            <a><small class="mb-0">Support</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center">
                                            <i class="ti-bell fs-30 text-warning"></i>
                                            <a><small class="mb-0">Notify</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-right-0 border-top-0">
                                            <i class="ti-panel fs-30 text-primary"></i>
                                            <a><small class="mb-0">Settings</small></a>
                                        </div>
                                    </div>
                                    <div class="col-6 p-0">
                                        <div class="border text-center border-top-0">
                                            <i class="ti-layers fs-30 text-danger"></i>
                                            <a><small class="mb-0">Layouts</small></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-weight-semibold"><i class="typcn typcn-cog-outline"></i> Pages</h4>
                                <a href="profile.html" class="slide-item">Profile</a>
                                <a href="editprofile.html" class="slide-item">Edit Profile</a>
                                <a href="email.html" class="slide-item">Email</a>
                                <a href="emailservices.html" class="slide-item">Email Inbox</a>
                                <a href="gallery.html" class="slide-item">Gallery</a>
                                <a href="about.html" class="slide-item">About Company</a>
                                <a href="services.html" class="slide-item">Services</a>
                                <a href="faq.html" class="slide-item">FAQS</a>
                                <a href="terms.html" class="slide-item">Terms and Conditions</a>
                                <a href="empty.html" class="slide-item">Empty Page</a>
                                <a href="construction.html" class="slide-item">Under Construction</a>
                                <a href="blog.html" class="slide-item">Blog</a>
                                <a href="invoice.html" class="slide-item">Invoice</a>
                                <a href="pricing.html" class="slide-item">Pricing Tables</a>
                                <h6 class="font-weight-semibold mt-5 mb-4">Active Users</h6>
                                <div class="row">
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.65" data-thickness="5" data-color="#467fcf">
                                            <div class="chart-circle-value"><div class="">65% </div></div>
                                        </div>
                                        <small>Active</small>
                                    </div>
                                    <div class="col text-center">
                                        <div class="chart-circle mt-2 mb-2 chart-circle-sm" data-value="0.35" data-thickness="5" data-color="#5eba00">
                                            <div class="chart-circle-value"><div class="">35% </div></div>
                                        </div>
                                        <small>Deactive</small>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-primary mt-3 btn-block"><i class="fe fe-eye mr-1 mt-1"></i> View All</a>
                                <a href="#" class="btn btn-secondary mt-3 btn-block"><i class="fe fe-plus mr-1 mt-1"></i> Add New page</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- col-4 -->
</aside>
<!--sidemenu end-->

<!-- app-content-->
<div class="app-content  my-3 my-md-5">
    <?php echo $this->Session->flash(); ?>