<!DOCTYPE html>
<html>
    <head>
        <?php if (!isset($meta) || $meta == "") { ?>
            <title>Emirates Safety Laboratory | ESL </title>
        <?php
        } else {
            echo $meta;
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick.css" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick-theme.css" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/aos.css" />
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.js"></script>
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.min.js"></script>
        <!--<script type="text/javascript" async="" src="./assets/js/insight.min.js"></script>-->
        <!--<script async="" src="./assets/js/gtm.js"></script>
<script src="./assets/380429773370024" async=""></script>-->
        <script async="" src="<?= $this->Html->url('/') ?>assets/js/fbevents.js"></script>
        <meta name="viewport" content="width=device-width" />
        <meta name="next-head-count" content="4" />
        <?php if ($controller == 'customers' || $controller == 'apages'||($controller == 'page_settings' && $action == 'contact')) { ?>
            <link rel="stylesheet" href="<?= $this->Html->url('/') ?>css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php } ?>
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/main.css" />

        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/custom<?= $_ar ?>.css" />
        <style data-styled="active" data-styled-version="5.1.1"></style>
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/file8.js"></script>
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/file9.js"></script>
        <link rel="icon" type="image/png" href="<?= $this->Html->url('/') ?>assets/img/fave.png" sizes="32x32">
        <style>
            .slick-list::before{
                content: "<?= $settings_data['home_part1']['value' . $_ar] ?>";
                color: #383838;
                font-size: 40px;
                font-weight: bold;
            }
        </style>
         <?php
    
        echo implode(' ', $metaheader);
    ?>
    </head>
    <?php
    $lang_action = '';
    if($controller == 'pages' && $action == 'display'){
        $lang_action = 'display';
    }
                        $lang_link = 'ara';
                        $img_lang = $this->Html->url('/assets/img/ara.png');
                        $lang_title = 'العربية';
                        if ($this->Session->read('Language.locale') == 'ara') {
                            $lang_link = 'eng';
                            $img_lang = $this->Html->url('/assets/img/eng.png');
                            $lang_title = 'English';
                        }
                        ?>
    <body class="demo-3" data-ls="1" data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
        <svg class="hidden">
        <defs>
    <filter id="filter-7">
        <feturbulence type="fractalNoise" baseFrequency="0" numOctaves="5" result="warp"></feturbulence>
        <fedisplacementmap xChannelSelector="R" yChannelSelector="G" scale="90" in="SourceGraphic" in2="warp"></fedisplacementmap>
    </filter>
    </defs>
    </svg>
    <div id="__next">
        <div id="awwwards" style="position: fixed; z-index: 999; transform: translateY(-50%); top: 50%; right: 0;display: none;">
            <a href="<?= $this->Html->url('/settings/lang_choice/').$lang_link.'/'.$lang_action ?>" target="_self" class="image-lang">
                <img src="<?=$img_lang?>"><span><?=$lang_title?></span>
            </a>
            
        </div>
        <div id="mobile-awwwards" style="position: fixed; z-index: 999; transform: translateY(-50%); top: 50%; right: 0;display: none;" wfd-id="39">
            <a href="<?= $this->Html->url('/settings/lang_choice/').$lang_link.'/'.$lang_action ?>" target="_self" class="image-lang">
                <img src="<?=$img_lang?>"><span><?=$lang_title?></span>
            </a>
        </div>
        <div class="Layout undefined">
            <nav style="background: #fbfbfb;">
                <div class="flex justify-between container-1">
                    <a href="<?= $this->Html->url('/') ?>">
                        <img class="cursor-pointer logo" src="<?= $this->Html->url('/') ?>assets/img/logo.png" alt="" />
                    </a>
                    <div class="menu menu--line links">
<?php foreach ($menus as $menu) { ?>
                        <a class="menu__link effect-link nav-effect-link false" href="<?= $menu['Menue']['href'] ?>" target="_parent">
                                <span class="menu__link-inner"><?= $menu['Menue']['title' . $_ar] ?></span>
                                <span class="menu__link-deco"></span>
                            </a>
<?php } ?>
                        <!--                            <a class="menu__link effect-link nav-effect-link false" href="#">
                                                        <span class="menu__link-inner">About</span>
                                                        <span class="menu__link-deco"></span>
                                                    </a>
                                                    <a class="menu__link effect-link nav-effect-link false" href="#">
                                                        <span class="menu__link-inner">Services </span>
                                                        <span class="menu__link-deco"></span>
                                                    </a>
                                                    <a class="menu__link effect-link nav-effect-link false" href="<?= $this->Html->url('/blog') ?>">
                                                        <span class="menu__link-inner">Blog</span>
                                                        <span class="menu__link-deco"></span>
                                                    </a>-->
                        <!--<a class="menu__link effect-link nav-effect-link false" href="contact.html">
                            <span class="menu__link-inner">Contact us</span>
                            <span class="menu__link-deco"></span>
                        </a>-->

                        
<!--                        <a href="<?= $this->Html->url('/settings/lang_choice/').$lang_link ?>" class="image-lang"><img src="<?=$img_lang?>"></a>-->
                        <a href="#" class="c-button--marquee"><span data-text="<?= __('Get Started') ?>"><?= __('Get Started') ?></span></a>
                    </div>
                </div>
            </nav>
            <div class="mobile-nav">
                <div class="hamburger hamburger--demo-3 js-hover">
                    <div class="hamburger__line hamburger__line--01"><div class="hamburger__line-in hamburger__line-in--01"></div></div>
                    <div class="hamburger__line hamburger__line--02"><div class="hamburger__line-in hamburger__line-in--02"></div></div>
                    <div class="hamburger__line hamburger__line--03"><div class="hamburger__line-in hamburger__line-in--03"></div></div>
                    <div class="hamburger__line hamburger__line--cross01"><div class="hamburger__line-in hamburger__line-in--cross01"></div></div>
                    <div class="hamburger__line hamburger__line--cross02"><div class="hamburger__line-in hamburger__line-in--cross02"></div></div>
                </div>
                <div class="global-menu">
                    <div class="global-menu__wrap">
<?php foreach ($menus as $menu) { ?>
                            <a href="<?= $menu['Menue']['href'] ?>" class="global-menu__item global-menu__item--demo-3 false" target="_parent"><?= $menu['Menue']['title' . $_ar] ?></a>
<?php } ?>
                            <a href="<?= $this->Html->url('/login') ?>" class="global-menu__item global-menu__item--demo-3 started_link"><?= __('Get Started') ?></a>
                        <!--                            <a href="#" class="global-menu__item global-menu__item--demo-3 false">About</a>
                        <a href="#" class="global-menu__item global-menu__item--demo-3 false">Services </a>
                        <a href="<?= $this->Html->url('/blog') ?>" class="global-menu__item global-menu__item--demo-3 false">Blog</a>-->

<!--                        <a href="<?= $this->Html->url('/settings/lang_choice/').$lang_link ?>" class="global-menu__item global-menu__item--demo-3 image-lang "><img src="<?=$img_lang?>"></a>-->
                        
                        <!--                            <a href="contact.html" class="global-menu__item global-menu__item--demo-3 false">Contact us</a>-->
                    </div>
                </div>
                <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path class="shape-overlays__path"></path>
                <path class="shape-overlays__path"></path>
                <path class="shape-overlays__path"></path>
                </svg>
                <span class="is-opened-navi is-opened hidden"></span>
            </div>