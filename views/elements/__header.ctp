<!doctype html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="Sparic– Creative Admin Multipurpose Responsive Bootstrap4 Dashboard HTML Template" name="description">
        <meta content="Spruko Technologies Private Limited" name="author">
        <meta name="keywords" content="html admin template, bootstrap admin template premium, premium responsive admin template, admin dashboard template bootstrap, bootstrap simple admin template premium, web admin template, bootstrap admin template, premium admin template html5, best bootstrap admin template, premium admin panel template, admin template"/>

        <!-- Favicon -->
        <link rel="icon" href="<?php echo $this->Html->url("/"); ?>assets/images/brand/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->Html->url("/"); ?>assets/images/brand/favicon.ico" />

        <!-- Title -->
        <title>Sparic – Creative Admin Multipurpose Responsive Bootstrap4 Dashboard HTML Template</title>

        <!--Bootstrap.min css-->
        <link rel="stylesheet" href="<?php echo $this->Html->url("/"); ?>assets/plugins/bootstrap/css/bootstrap.min.css">

        <!-- Dashboard css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/css/style.css" rel="stylesheet" />

        <!-- Combined css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/css/combined.css" rel="stylesheet" />

        <!-- Horizontal-menu css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/horizontal-menu/dropdown-effects/fade-down.css" rel="stylesheet">
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/horizontal-menu/horizontalmenu.css" rel="stylesheet">

        <!-- Perfect scroll bar css-->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/pscrollbar/perfect-scrollbar.css" rel="stylesheet" />

        <!-- Sidemenu css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/side-menu/full-sidemenu.css" rel="stylesheet" />

        <!--Daterangepicker css-->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />

        <!-- Sidebar Accordions css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/sidemenu-responsive-tabs/css/easy-responsive-tabs.css" rel="stylesheet">

        <!-- Rightsidebar css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/sidebar/sidebar.css" rel="stylesheet">

        <!--News ticker css -->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/newsticker/breaking-news-ticker.css" rel="stylesheet" />

        <!---Font icons css-->
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/webfonts/plugin.css" rel="stylesheet" />
        <link href="<?php echo $this->Html->url("/"); ?>assets/plugins/webfonts/icons.css" rel="stylesheet" />
        <link  href="<?php echo $this->Html->url("/"); ?>assets/fonts/fonts/font-awesome.min.css" rel="stylesheet">
        
        <!-- Data table css -->
		<link href="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
		<link href="<?php echo $this->Html->url("/"); ?>assets/plugins/datatable/responsivebootstrap4.min.css" rel="stylesheet" />
        <!--Mutipleselect css-->
		<link rel="stylesheet" href="../assets/plugins/multipleselect/multiple-select.css">


        <!-- Color-skins css -->
        <link id="theme" rel="stylesheet" type="text/css" media="all" href="<?php echo $this->Html->url("/"); ?>assets/colors/color-skins/color.css" />
        <link rel="stylesheet" href="<?php echo $this->Html->url("/"); ?>assets/css/demo-styles.css"/>
        <link rel="stylesheet" href="<?php echo $this->Html->url("/"); ?>assets/css/custom.css"/>
    </head>

    <body class="app sidebar-mini">

        <!--Global-Loader-->
        <div id="global-loader">
            <img src="<?php echo $this->Html->url("/"); ?>assets/images/brand/icon.png" alt="loader">
        </div>

        <div class="page">
            <div class="page-main">

                <!--app-header-->
                <div class="app-header header d-flex">
                    <div class="container-fluid">
                        <div class="d-flex">
                            <a class="header-brand" href="index.html">
                                <img src="<?php echo $this->Html->url("/"); ?>assets/images/brand/logo.png" class="header-brand-img main-logo" alt="Sparic logo">
                                <img src="<?php echo $this->Html->url("/"); ?>assets/images/brand/logo-small.png" class="header-brand-img icon-logo" alt="Sparic logo">
                            </a><!-- logo-->
                            <a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
                            <a id="horizontal-navtoggle" class="animated-arrow hor-toggle"><span></span></a>
                            <a href="#" data-toggle="search" class="nav-link nav-link  navsearch"><i class="fa fa-search"></i></a><!-- search icon -->

                            <ul class="nav navbar-nav horizontal-nav header-nav">
                                <li class="mega-dropdown nav-item">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fe fe-grid mr-2"></i>UI Kit <i class="fa fa-angle-down ml-1"></i>
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu container row p-5">
                                        <li>
                                            <div class="row">
                                                <div class="col-2">
                                                    <h4  class="mb-3">Pages</h4>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Client Support</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> About Us</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Calendar</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Add New Pages</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Login Pages</a>
                                                </div>
                                                <div class="col-2">
                                                    <h4  class="mb-3">Pages</h4>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Documentation</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Multi Pages</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Edit Profile</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Mail Settings</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Default Setting</a>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="">
                                                        <div class="card-body p-0 relative">
                                                            <div class="arrow-ribbon">Today Events</div>
                                                            <img class="" alt="img" src="<?php echo $this->Html->url("/"); ?>assets/images/photos/32.png">
                                                            <div class="btn-absolute">
                                                                <a class="btn btn-primary  btn-sm" href="#">More info</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="">
                                                        <div class="card-body p-0 relative">
                                                            <div class="arrow-ribbon">Comming Events</div>
                                                            <img class="" alt="img" src="<?php echo $this->Html->url("/"); ?>assets/images/photos/32.jpg">
                                                            <div class="btn-absolute">
                                                                <a class="btn btn-primary  btn-sm" href="#">More info</a>
                                                                <span id="timer-countercallback1" class="h5 text-white float-right mb-0 mt-1"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav horizontal-nav header-nav">
                                <li class="mega-dropdown nav-item">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fe fe-grid mr-2"></i>Pages<i class="fa fa-angle-down ml-1"></i>
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu container row p-5">
                                        <li>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <h4  class="mb-3">Pages</h4>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Client Support</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> About Us</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Calendar</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Add New Pages</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Login Pages</a>

                                                </div>
                                                <div class="col-md-3">
                                                    <h4  class="mb-3">Pages</h4>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Documentation</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Multi Pages</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Edit Profile</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Mail Settings</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Default Setting</a>
                                                </div>
                                                <div class="col-md-3">
                                                    <h4  class="mb-3">Pages</h4>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Client Support</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> About Company</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Calendar</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Add New Pages</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Login Pages</a>

                                                </div>
                                                <div class="col-md-3">
                                                    <h4  class="mb-3">Pages</h4>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Client Support</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> About Us</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Calendar</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Add New Pages</a>
                                                    <a class="dropdown-item pl-0 pr-0" href="#"><i class="fa fa-angle-double-right text-muted mr-1"></i> Login Pages</a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav header-nav navbar-nav">
                                <li class="nav-item dropdown header-option m-2 pb-3">
                                    <a  class="mr-2 nav-link "  id="daterange-btn">
                                        <span>
                                            <i class="fa fa-calendar"></i> Events Settings
                                        </span>
                                        <i class="fa fa-angle-down ml-1"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="d-flex order-lg-2 ml-auto header-rightmenu">

                                <div class="dropdown d-sm-flex d-none">
                                    <a  class="nav-link icon full-screen-link" id="fullscreen-button">
                                        <i class="fe fe-maximize-2"></i>
                                    </a>
                                </div><!-- full-screen -->
                                <div class="dropdown header-notify d-sm-flex d-none">
                                    <a class="nav-link icon" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fe fe-bell "></i>
                                        <span class="pulse bg-success"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">
                                        <a href="#" class="dropdown-item text-center">4 New Notifications</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item d-flex pb-3">
                                            <div class="notifyimg bg-green">
                                                <i class="fe fe-mail"></i>
                                            </div>
                                            <div>
                                                <strong>Message Sent.</strong>
                                                <div class="small text-muted">12 mins ago</div>
                                            </div>
                                        </a>
                                        <a href="#" class="dropdown-item d-flex pb-3">
                                            <div class="notifyimg bg-pink">
                                                <i class="fe fe-shopping-cart"></i>
                                            </div>
                                            <div>
                                                <strong>Order Placed</strong>
                                                <div class="small text-muted">2  hour ago</div>
                                            </div>
                                        </a>
                                        <a href="#" class="dropdown-item d-flex pb-3">
                                            <div class="notifyimg bg-blue">
                                                <i class="fe fe-calendar"></i>
                                            </div>
                                            <div>
                                                <strong> Event Started</strong>
                                                <div class="small text-muted">1  hour ago</div>
                                            </div>
                                        </a>
                                        <a href="#" class="dropdown-item d-flex pb-3">
                                            <div class="notifyimg bg-orange">
                                                <i class="fe fe-monitor"></i>
                                            </div>
                                            <div>
                                                <strong>Your Admin Lanuch</strong>
                                                <div class="small text-muted">2  days ago</div>
                                            </div>
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item text-center">View all Notifications</a>
                                    </div>
                                </div><!-- notifications -->
                                <div class="dropdown header-user d-sm-flex d-none">
                                    <a class="nav-link leading-none siderbar-link"  data-toggle="sidebar-right" data-target=".sidebar-right">
                                        <span class="mr-3 d-none d-lg-block ">
                                            <span class="text-gray-white"><span class="ml-2">Alison</span></span>
                                        </span>
                                        <span class="avatar avatar-md brround"><img src="<?php echo $this->Html->url("/"); ?>assets/images/users/avatars/19.png" alt="Profile-img" class="avatar avatar-md brround"></span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <div class="header-user text-center mt-4 pb-4">
                                            <span class="avatar avatar-xxl brround"><img src="<?php echo $this->Html->url("/"); ?>assets/images/users/avatars/19.png" alt="Profile-img" class="avatar avatar-xxl brround"></span>
                                            <a href="#" class="dropdown-item text-center font-weight-semibold user h3 mb-0">Alison</a>
                                            <small>Web Designer</small>
                                        </div>
                                        <a class="dropdown-item" href="#">
                                            <i class="dropdown-icon mdi mdi-account-outline "></i> Spruko technologies
                                        </a>
                                        <a class="dropdown-item" href="#">
                                            <i class="dropdown-icon  mdi mdi-account-plus"></i> Add another Account
                                        </a>
                                        <div class="card-body border-top">
                                            <div class="row">
                                                <div class="col-6 text-center">
                                                    <a class="" href=""><i class="dropdown-icon mdi  mdi-message-outline fs-30 m-0 leading-tight"></i></a>
                                                    <div>Inbox</div>
                                                </div>
                                                <div class="col-6 text-center">
                                                    <a class="" href="<?php echo $this->Html->url("/admins/logout"); ?>"><i class="dropdown-icon mdi mdi-logout-variant fs-30 m-0 leading-tight"></i></a>
                                                    <div>Sign out</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- profile -->

                                <button class="navbar-toggler navresponsive-toggler d-sm-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                                        aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon fe fe-more-vertical"></span>
                                </button>
                                <!--Navbar -->
                                <div class="header-form">
                                    <form class="form-inline">
                                        <div class="search-element mr-3">
                                            <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                                            <span class="Search-icon"><i class="fa fa-search"></i></span>
                                        </div>
                                    </form><!-- search-bar -->
                                </div>
                                <div class="dropdown">
                                    <a  class="nav-link icon siderbar-link" data-toggle="sidebar-right" data-target=".sidebar-right">
                                        <i class="fe fe-more-horizontal"></i>
                                    </a>
                                </div><!-- Right-siebar-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--/app-header-->

                <!--Resposnisve Navbar-->
                <div class="mb-1 navbar navbar-expand-lg  responsive-navbar navbar-dark d-sm-none bg-white">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                        <div class="d-flex order-lg-2 ml-auto">
                            <div class="">
                                <a href="#" data-toggle="search" class="icon navsearch">
                                    <i class="fe fe-search"></i>
                                </a>
                            </div>
                            <div class="dropdown d-md-flex header-message">
                                <a  class="nav-link icon full-screen-link" id="fullscreen-button">
                                    <i class="fe fe-maximize-2"></i>
                                </a>
                            </div>
                            <div class="dropdown d-md-flex header-message">
                                <a class="nav-link icon" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fe fe-bell "></i>
                                    <span class="pulse bg-success"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow ">
                                    <a href="#" class="dropdown-item text-center">4 New Notifications</a>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item d-flex pb-3">
                                        <div class="notifyimg bg-green">
                                            <i class="fe fe-mail"></i>
                                        </div>
                                        <div>
                                            <strong>Message Sent.</strong>
                                            <div class="small text-muted">12 mins ago</div>
                                        </div>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex pb-3">
                                        <div class="notifyimg bg-pink">
                                            <i class="fe fe-shopping-cart"></i>
                                        </div>
                                        <div>
                                            <strong>Order Placed</strong>
                                            <div class="small text-muted">2  hour ago</div>
                                        </div>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex pb-3">
                                        <div class="notifyimg bg-blue">
                                            <i class="fe fe-calendar"></i>
                                        </div>
                                        <div>
                                            <strong> Event Started</strong>
                                            <div class="small text-muted">1  hour ago</div>
                                        </div>
                                    </a>
                                    <a href="#" class="dropdown-item d-flex pb-3">
                                        <div class="notifyimg bg-orange">
                                            <i class="fe fe-monitor"></i>
                                        </div>
                                        <div>
                                            <strong>Your Admin Lanuch</strong>
                                            <div class="small text-muted">2  days ago</div>
                                        </div>
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item text-center">View all Notifications</a>
                                </div>
                            </div>
                            <div class="dropdown d-md-flex header-message">
                                <a class="nav-link leading-none siderbar-link"  data-toggle="sidebar-right" data-target=".sidebar-right">
                                    <span class="mr-3 d-none d-lg-block ">
                                        <span class="text-gray-white"><span class="ml-2">Alison</span></span>
                                    </span>
                                    <span class="avatar avatar-md brround"><img src="<?php echo $this->Html->url("/"); ?>assets/images/users/avatars/19.png" alt="Profile-img" class="avatar avatar-md brround"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <div class="header-user text-center mt-4 pb-4">
                                        <span class="avatar avatar-xxl brround"><img src="<?php echo $this->Html->url("/"); ?>assets/images/users/avatars/19.png" alt="Profile-img" class="avatar avatar-xxl brround"></span>
                                        <a href="#" class="dropdown-item text-center font-weight-semibold user h3 mb-0">Alison</a>
                                        <small></small>
                                    </div>
                                    <a class="dropdown-item" href="#">
                                        <i class="dropdown-icon mdi mdi-account-outline "></i> Spruko technologies
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="dropdown-icon  mdi mdi-account-plus"></i> Add another Account
                                    </a>
                                    <div class="card-body border-top">
                                        <div class="row">
                                            <div class="col-6 text-center">
                                                <a class="" href=""><i class="dropdown-icon mdi  mdi-message-outline fs-30 m-0 leading-tight"></i></a>
                                                <div>Inbox</div>
                                            </div>
                                            <div class="col-6 text-center">
                                                <a class="" href="<?php echo $this->Html->url("/users/logout"); ?>"><i class="dropdown-icon mdi mdi-logout-variant fs-30 m-0 leading-tight"></i></a>
                                                <div>Sign out</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Horizontal-menu -->
                <div class="horizontal-main hor-menu clearfix">
                    <div class="horizontal-mainwrapper clearfix">
                        <nav class="horizontalMenu clearfix">
                            <a class="horizontal-right-icon" href="#">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <ul class="horizontalMenu-list">
                                <li aria-haspopup="true"><a href="#" class="sub-icon active"><i class="typcn typcn-device-desktop hor-icon"></i> Dashboard </a>
                                    
                                </li>
                                <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fa fa-newspaper-o"></i> CMS <i class="fa fa-angle-down horizontal-icon"></i></a>
                                    <ul class="sub-menu">
                                        <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/apages"); ?>">Pages</a></li>
                                        <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/settings"); ?>">Settings</a></li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">News</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/apages/posts"); ?>">News</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/apages/add"); ?>">Add news</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Sliders</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/banners"); ?>">Sliders</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/banners/add"); ?>">Add Slide</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Partners / Clients</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/partners"); ?>">Partners / Clients</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/partners/add"); ?>">Add New</a></li>
                                            </ul>
                                        </li>
                                        <!--<li aria-haspopup="true" class="sub-menu-sub"><a href="#">Gallary</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/galleries"); ?>">Gallary</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/galleries/add"); ?>">Add Gallary</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/gallerycats"); ?>">Gallary Categories</a></li>
                                            </ul>
                                        </li>-->
                                        
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">SEO</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/seo_urls"); ?>">SEO urls</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/seo_attributes"); ?>">SEO attributes</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                 <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fe fe-layers"></i> Standard <i class="fa fa-angle-down horizontal-icon"></i></a>
                                    
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/standards"); ?>">Standards</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/standards/add"); ?>">Add Standard</a></li>
                                            </ul>
                                </li>
                                <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="fa fa-group"></i> Staff <i class="fa fa-angle-down horizontal-icon"></i></a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/users"); ?>">Staff</a></li>
                                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/users/add"); ?>">Add Staff</a></li>
                                            </ul>
                                </li>

                                <li aria-haspopup="true"><a href="#" class="sub-icon "><i class="typcn typcn-spanner-outline"></i> Elements  <i class="fa fa-angle-down horizontal-icon"></i></a>
                                    <div class="horizontal-megamenu clearfix">
                                        <div class="container">
                                            <div class="mega-menubg">
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                        <ul>
                                                            <li aria-haspopup="true"><a href="alerts.html">Alerts</a></li>
                                                            <li aria-haspopup="true"><a href="buttons.html">Buttons</a></li>
                                                            <li aria-haspopup="true"><a href="colors.html">Colors</a></li>
                                                            <li aria-haspopup="true"><a href="avatars.html">Avatars</a></li>
                                                            <li aria-haspopup="true"><a href="dropdown.html">Drop downs</a></li>
                                                            <li aria-haspopup="true"><a href="thumbnails.html">Thumbnails</a></li>
                                                            <li aria-haspopup="true"><a href="mediaobject.html">Media Object</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                        <ul>
                                                            <li aria-haspopup="true"><a href="list.html">List</a></li>
                                                            <li aria-haspopup="true"><a href="tags.html">Tags</a></li>
                                                            <li aria-haspopup="true"><a href="pagination.html">Pagination</a></li>
                                                            <li aria-haspopup="true"><a href="navigation.html">Navigation</a></li>
                                                            <li aria-haspopup="true"><a href="typography.html">Typography</a></li>
                                                            <li aria-haspopup="true"><a href="breadcrumbs.html">Breadcrumbs</a></li>
                                                            <li aria-haspopup="true"><a href="badge.html">Badges</a></li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                        <ul>
                                                            <li aria-haspopup="true"><a href="email.html">Email</a></li>
                                                            <li aria-haspopup="true"><a href="emailservices.html">Email Inbox</a></li>
                                                            <li aria-haspopup="true"><a href="gallery.html">Gallery</a></li>
                                                            <li aria-haspopup="true"><a href="about.html">About Company</a></li>
                                                            <li aria-haspopup="true"><a href="services.html">Services</a></li>
                                                            <li aria-haspopup="true"><a href="faq.html">FAQS</a></li>
                                                            <li aria-haspopup="true"><a href="terms.html">Terms and Conditions</a></li>

                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-3 col-md-12 col-xs-12 link-list">
                                                        <ul>
                                                            <li aria-haspopup="true"><a href="empty.html">Empty Page</a></li>
                                                            <li aria-haspopup="true"><a href="blog.html">Blog</a></li>
                                                            <li aria-haspopup="true"><a href="invoice.html">Invoice</a></li>
                                                            <li aria-haspopup="true"><a href="pricing.html">Pricing Tables</a></li>
                                                            <li aria-haspopup="true"><a href="jumbotron.html">Jumbotron</a></li>
                                                            <li aria-haspopup="true"><a href="panels.html">Panels</a></li>
                                                            <li aria-haspopup="true"><a href="search.html">Search page</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <li aria-haspopup="true"><a href="#" class="sub-icon "><i class="typcn typcn-cog-outline"></i> Pages <i class="fa fa-angle-down horizontal-icon"></i></a>
                                    <ul class="sub-menu">
                                        <li aria-haspopup="true"><a href="profile.html">Profile</a></li>
                                        <li aria-haspopup="true"><a href="editprofile.html">Edit Profile</a></li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Tables</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="tables.html">Default table</a></li>
                                                <li aria-haspopup="true"><a href="datatable.html">Data Table</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Forms</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="form-elements.html">Form Elements</a></li>
                                                <li aria-haspopup="true"><a href="form-wizard.html">Form-wizard</a></li>
                                                <li aria-haspopup="true"><a href="wysiwyag.html">Text Editor</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">E-commerce</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="shop.html">Products</a></li>
                                                <li aria-haspopup="true"><a href="shop-des.html">Product Details</a></li>
                                                <li aria-haspopup="true"><a href="cart.html">Shopping Cart</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Custom </a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="login.html">Login</a></li>
                                                <li aria-haspopup="true"><a href="register.html">Register</a></li>
                                                <li aria-haspopup="true"><a href="forgot-password.html">Forgot Password</a></li>
                                                <li aria-haspopup="true"><a href="lockscreen.html">Lock screen</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="sub-menu-sub"><a href="#">Error Pages</a>
                                            <ul class="sub-menu">
                                                <li aria-haspopup="true"><a href="400.html">400 Error</a></li>
                                                <li aria-haspopup="true"><a href="401.html">401 Error</a></li>
                                                <li aria-haspopup="true"><a href="403.html">403 Error</a></li>
                                                <li aria-haspopup="true"><a href="404.html">404 Error</a></li>
                                                <li aria-haspopup="true"><a href="500.html">500 Error</a></li>
                                                <li aria-haspopup="true"><a href="503.html">503 Error</a></li>
                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" ><a href="construction.html">Under Construction</a></li>
                                    </ul>
                                </li>
                                <li aria-haspopup="true"><a href="#" class="sub-icon"><i class="typcn typcn-point-of-interest-outline"></i> Icons <i class="fa fa-angle-down horizontal-icon"></i></a>
                                    <ul class="sub-menu">
                                        <li aria-haspopup="true"><a href="icons.html">Font Awesome</a></li>
                                        <li aria-haspopup="true"><a href="icons2.html">Material Design Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons3.html">Simple Line Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons4.html">Feather Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons5.html">Ionic Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons6.html">Flag Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons7.html">pe7 Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons8.html">Themify Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons9.html">Typicons Icons</a></li>
                                        <li aria-haspopup="true"><a href="icons10.html">Weather Icons</a></li>
                                    </ul>
                                </li>
                                <li aria-haspopup="true"><a href="<?php echo $this->Html->url("/admins/logout"); ?>" class="sub-icon"><i class="mdi mdi-logout-variant" ></i>Sign Out</a>
                                </li>
                            </ul>
                            <a class="horizontal-left-icon" href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </nav>
                        <!--Nav end -->

                    </div>
                </div>
                <!-- Horizontal-menu end -->

                <!--News Ticker-->
                <div class="container-fluid bg-white news-ticker">
                    <div class="bg-white">
                        <div class="best-ticker" id="newsticker">
                            <div class="bn-news">
                                <ul>
                                    <li><span class="fa fa-users bg-danger-transparent text-danger mr-1"></span> Total Users <span class="bn-positive mr-5">1,653</span></li>
                                    <li><span class="fa fa-signal bg-info-transparent text-info mr-1"></span> Total Leads <span class="bn-negative mr-5">639</span></li>
                                    <li><span class="fa fa-briefcase mr-1 bg-success-transparent text-success"></span> Total Trials <span class="bn-negative mr-5">12,765</span></li>
                                    <li><span class="fa fa-trophy mr-1 bg-warning-transparent text-warning"></span> Total Wins <span class="bn-positive mr-5">24</span></li>
                                    <li><span class="fa fa-envelope mr-1 bg-primary-transparent text-primary"></span> Active Email Accounts <span class="bn-positive mr-5">74,526</span></li>
                                    <li><span class="fa fa-check-circle mr-1 bg-danger-transparent text-danger"></span> Active Requests <span class="bn-positive mr-5">14,526</span></li>
                                    <li><span class="fa fa-envelope mr-1 bg-secondary-transparent text-secondary"></span> Deactive Email Accounts <span class="bn-positive mr-5">7,325 </span></li>
                                    <li><span class="fa fa-times-circle mr-1 bg-info-transparent text-info"></span> Deactive Requests <span class="bn-positive mr-5"> 1,425 </span></li>
                                    <li><span class="fa fa-usd mr-1 bg-success-transparent text-success"></span> Total Balance <span class="bn-negative mr-5">$1,52,654</span></li>
                                    <li><span class="fa fa-shopping-cart mr-1 bg-danger-transparent text-danger"></span> Total Sales <span class="bn-negative mr-5">23,15,2654</span></li>
                                    <li><span class="fa fa-money mr-1 bg-warning-transparent text-warning"></span> Total Purchase <span class="bn-positive mr-5">$7,483</span></li>
                                    <li><span class="fa fa-usd mr-1 bg-danger-transparent text-danger"></span> Total Cost Reduction <span class="bn-negative mr-5">$23,567</span></li>
                                    <li><span class="fa fa-money mr-1 bg-primary-transparent text-primary"></span> Total Cost Savings <span class="bn-negative mr-5">15.2%</span></li>
                                    <li><span class="fa fa-briefcase mr-1 bg-info-transparent text-info"></span> Total Projects <span class="bn-positive mr-5">3,456</span></li>
                                    <li><span class="fa fa-users mr-1 bg-success-transparent text-success"></span> Total Employes <span class="bn-positive mr-5">4,738</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/News Ticker-->