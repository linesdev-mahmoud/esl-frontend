<script language="javascript" src="<?php echo $this->Html->url("/js/fckjs/ckeditor_OK/ckeditor.js"); ?>"></script>
  <?php
  $checked="";
  if($this->data['Customernotification']['status']==1){
      $checked="checked";
  }else{
       $checked="";
  }
                        ?>

<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/customernotifications"); ?>">Custom Notifications</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Custom Notification</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Custom Notification</h3>
                </div>
                <div class="card-body">
                    <?php echo $this->Form->create('Customernotification', array('type'=>'file','class' => 'form-horizontal form-material'));?>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">Title</label>
                                <?php
		echo $this->Form->input('title', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">SMS Content</label>
                                <?php
		echo $this->Form->input('sms_content', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">SMS Content - arabic</label>
                                <?php
		echo $this->Form->input('sms_content_ar', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Email Content</label>
                                
                                <?php
		echo $this->Form->input('email_content', array('value'=>$this->data['Customernotification']['email_content'],'label'=>false,'placeholder'=>'Enter Content','class' => 'form-control form-control-line'));
		echo $fck->load("Customernotification.email.content");
	?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Email Content - arabic</label>
                                
                                <?php
		echo $this->Form->input('email_content_ar', array('value'=>$this->data['Customernotification']['email_content_ar'],'label'=>false,'class' => 'form-control form-control-line'));
		echo $fck->load("Customernotification.email.content.ar");
	?> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputnumber">Active?</label>
                        		<label class="custom-switch form-group">
												<input type="checkbox" name="data[Customernotification][status]" class="custom-switch-input" <?= $checked ?>>
												<span class="custom-switch-indicator"></span>
								</label>
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                   
                       <input type="submit" class="btn btn-success mt-1" value="Save">
                </div>
            </div>
        </div>
    </div>
    <!-- row end -->

</div>