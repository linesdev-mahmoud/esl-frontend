<div class="customernotifications view">
<h2><?php  __('Customernotification');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Title'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['title']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sms Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['sms_content']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Sms Content Ar'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['sms_content_ar']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Email Content'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['email_content']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Email Content Ar'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['email_content_ar']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Status'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['status']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['modified']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified By'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $customernotification['Customernotification']['modified_by']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Customernotification', true), array('action' => 'edit', $customernotification['Customernotification']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Customernotification', true), array('action' => 'delete', $customernotification['Customernotification']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $customernotification['Customernotification']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Customernotifications', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customernotification', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Approvals', true), array('controller' => 'approvals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Approval', true), array('controller' => 'approvals', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Approvals');?></h3>
	<?php if (!empty($customernotification['Approval'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Title'); ?></th>
		<th><?php __('Title For Customer'); ?></th>
		<th><?php __('Title For Customer Ar'); ?></th>
		<th><?php __('Sequence'); ?></th>
		<th><?php __('Role Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th><?php __('Workflow Id'); ?></th>
		<th><?php __('Available For Customer'); ?></th>
		<th><?php __('Approval Type'); ?></th>
		<th><?php __('Yes Action'); ?></th>
		<th><?php __('No Action'); ?></th>
		<th><?php __('Accept Action'); ?></th>
		<th><?php __('Reject Action'); ?></th>
		<th><?php __('Send Sms'); ?></th>
		<th><?php __('Send Email'); ?></th>
		<th><?php __('Customernotification Id'); ?></th>
		<th><?php __('Is For Payment'); ?></th>
		<th><?php __('Status'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th><?php __('Modified By'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($customernotification['Approval'] as $approval):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $approval['id'];?></td>
			<td><?php echo $approval['title'];?></td>
			<td><?php echo $approval['title_for_customer'];?></td>
			<td><?php echo $approval['title_for_customer_ar'];?></td>
			<td><?php echo $approval['sequence'];?></td>
			<td><?php echo $approval['role_id'];?></td>
			<td><?php echo $approval['user_id'];?></td>
			<td><?php echo $approval['workflow_id'];?></td>
			<td><?php echo $approval['available_for_customer'];?></td>
			<td><?php echo $approval['approval_type'];?></td>
			<td><?php echo $approval['yes_action'];?></td>
			<td><?php echo $approval['no_action'];?></td>
			<td><?php echo $approval['accept_action'];?></td>
			<td><?php echo $approval['reject_action'];?></td>
			<td><?php echo $approval['send_sms'];?></td>
			<td><?php echo $approval['send_email'];?></td>
			<td><?php echo $approval['customernotification_id'];?></td>
			<td><?php echo $approval['is_for_payment'];?></td>
			<td><?php echo $approval['status'];?></td>
			<td><?php echo $approval['created'];?></td>
			<td><?php echo $approval['modified'];?></td>
			<td><?php echo $approval['modified_by'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'approvals', 'action' => 'view', $approval['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'approvals', 'action' => 'edit', $approval['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'approvals', 'action' => 'delete', $approval['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $approval['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Approval', true), array('controller' => 'approvals', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
