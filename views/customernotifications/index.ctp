<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?=$this->Html->url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Customer Notifications</li>
        </ol><!-- End breadcrumb -->
        <div class="ml-auto">
            <div class="input-group">
                <a href="<?=$this->Html->url('/customernotifications/add') ?>" class="btn btn-secondary padbtn"><i class="fe fe-plus mr-1"></i>Add New</a>
                
            </div>
        </div>
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="card-title">Customer Notifications</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                            <thead>
                                <tr>
                                    <th class="wd-15p">Id</th>
                                    <th class="wd-15p">Title</th>
                                    <th class="wd-20p">SMS Content</th>
                                    <th class="wd-20p">SMS Content - arabic</th>
                                    <th class="wd-20p">Email Content</th>
                                    <th class="wd-20p">Email Content - arabic</th>
                                    <th class="wd-20p">Status</th>
                                    <th class="wd-20p">Created</th>
                                    <th class="wd-20p">Modified</th>
                                    <th class="wd-20p">Modified By</th>
                                    <th class="wd-20p">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                           <?php
	$i = 0;
	foreach ($customernotifications as $customernotification):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
                                <tr>
                                    <td><?php echo $customernotification['Customernotification']['id']; ?>&nbsp;</td>
                                    <td><?php echo $customernotification['Customernotification']['title']; ?>&nbsp;</td>
                                    <td><?php echo $customernotification['Customernotification']['sms_content']; ?>&nbsp;</td>
                                    <td><?php echo $customernotification['Customernotification']['sms_content_ar']; ?>&nbsp;</td>
                                    <td><?php echo $customernotification['Customernotification']['email_content']; ?>&nbsp;</td>
                                    <td><?php echo $customernotification['Customernotification']['email_content_ar']; ?>&nbsp;</td>
                                    <td>
                                <?php if($customernotification['Customernotification']['status']==0){?>
                                   <i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
                                <?php }else{?>
                                     <i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
                                <?php }?>
                            </td>
                                    <td><?php echo $customernotification['Customernotification']['created']; ?>&nbsp;</td>
                                    <td><?php echo $customernotification['Customernotification']['modified']; ?>&nbsp;</td>
                                    <td><?php echo $usermod[$customernotification['Customernotification']['modified_by']]; ?>&nbsp;</td>
                                    <td class="actions">
                                        <div class="input-group">
                                            <?= $this->Html->link(
    '<span><i class="si si-pencil"></i></span>',
    ['action' => 'edit', $customernotification['Customernotification']['id']],
    ['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
) ?>
                                            <?= $this->Html->link(
    '<span><i class="si si-trash"></i></span>',
    ['action' => 'delete', $customernotification['Customernotification']['id']],
    ['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $customernotification['Customernotification']['id'])
) ?>
                                        </div>
                                    </td>
                                </tr>
                        <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
            <!-- section-wrapper -->
        </div>
    </div>
    <!-- row end -->

</div>
<!-- Right-sidebar-->





<!--<div class="customernotifications index">
	<h2><?php __('Customernotifications');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('sms_content');?></th>
			<th><?php echo $this->Paginator->sort('sms_content_ar');?></th>
			<th><?php echo $this->Paginator->sort('email_content');?></th>
			<th><?php echo $this->Paginator->sort('email_content_ar');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($customernotifications as $customernotification):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $customernotification['Customernotification']['id']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['title']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['sms_content']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['sms_content_ar']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['email_content']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['email_content_ar']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['status']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['created']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['modified']; ?>&nbsp;</td>
		<td><?php echo $customernotification['Customernotification']['modified_by']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $customernotification['Customernotification']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $customernotification['Customernotification']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $customernotification['Customernotification']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $customernotification['Customernotification']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customernotification', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Approvals', true), array('controller' => 'approvals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Approval', true), array('controller' => 'approvals', 'action' => 'add')); ?> </li>
	</ul>
</div>-->