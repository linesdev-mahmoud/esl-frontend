<script language="javascript" src="<?php echo $this->Html->url("/js/fckjs/ckeditor_OK/ckeditor.js"); ?>"></script>

<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/customernotifications"); ?>">Custom Notification</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Custom Notification</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add Custom Notification</h3>
                </div>
                <div class="card-body">
                    <?php echo $this->Form->create('Customernotification', array('type'=>'file','class' => 'form-horizontal form-material'));?>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">Title</label>
                                <?php
		echo $this->Form->input('title', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">SMS Content</label>
                                <?php
		echo $this->Form->input('sms_content', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">SMS Content - arabic</label>
                                <?php
		echo $this->Form->input('sms_content_ar', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Email Content</label>
                                
                                <?php
		echo $this->Form->input('email_content', array('label'=>false,'placeholder'=>'Enter Content','class' => 'form-control form-control-line'));
		echo $fck->load("Customernotification.email.content");
	?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Email Content - arabic</label>
                                
                                <?php
		echo $this->Form->input('email_content_ar', array('label'=>false,'class' => 'form-control form-control-line'));
		echo $fck->load("Customernotification.email.content.ar");
	?> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputnumber">Active?</label>
                        		<label class="custom-switch form-group">
												<input type="checkbox" name="data[Customernotification][status]" class="custom-switch-input">
												<span class="custom-switch-indicator"></span>
								</label>
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                   
                       <input type="submit" class="btn btn-success mt-1" value="Save">
                </div>
            </div>
        </div>
    </div>
    <!-- row end -->

</div>





<!--<div class="customernotifications form">
<?php echo $this->Form->create('Customernotification');?>
	<fieldset>
		<legend><?php __('Add Customernotification'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('sms_content');
		echo $this->Form->input('sms_content_ar');
		echo $this->Form->input('email_content');
		echo $this->Form->input('email_content_ar');
		echo $this->Form->input('status');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Customernotifications', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Approvals', true), array('controller' => 'approvals', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Approval', true), array('controller' => 'approvals', 'action' => 'add')); ?> </li>
	</ul>
</div>-->