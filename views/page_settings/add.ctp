<div class="pageSettings form">
<?php echo $this->Form->create('PageSetting');?>
	<fieldset>
		<legend><?php __('Add Page Setting'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('title_ar');
		echo $this->Form->input('code');
		echo $this->Form->input('value');
		echo $this->Form->input('value_ar');
		echo $this->Form->input('modified_by');
		echo $this->Form->input('type');
		echo $this->Form->input('image');
		echo $this->Form->input('module');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Page Settings', true), array('action' => 'index'));?></li>
	</ul>
</div>