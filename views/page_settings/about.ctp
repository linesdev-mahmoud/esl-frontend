<div class="Content">
    <div class="page page-about">
        <div class="section hero text-center new_hero">
            <div class="container-2">
                <h1><?= $settings['our_story_title']['value' . $_ar] ?></h1>
            </div>
            <div class="container-3">
                <p class=" text-6"><?= $settings['our_story_content']['value' . $_ar] ?> </p>
            </div>
            <div class="mb-80p" style="margin-top: 50px;">
                <p class="text-2 text-center section__title aos-init" ><?= $settings['About_static_title2']['value' . $_ar] ?> </p>
            </div>
            <div class="container-3">
                <p class=" text-6"><?= $settings['About_static_content2']['value' . $_ar] ?> </p>
            </div>
            <!--            <div class="mb-80p" style="margin-top: 50px;">
                            <p class="text-2 text-center section__title aos-init" ><?= $settings['About_static_title3']['value' . $_ar] ?> </p>
                        </div>
                        <div class="container-3">
                            <h2><?= $settings['About_static_content3']['value' . $_ar] ?> </h2>
                        </div>-->
        </div>
        <section class="section multi-columns bg-light-grey-two about-statistic">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?= $settings['numbers_title']['value' . $_ar] ?> </p>
                </div>
                <div class="container-3 text-center">
                    <p class=" text-6"><?= $settings['About_static_content3']['value' . $_ar] ?> </p>
                </div>
                <div class="items grid grid-cols-1 md:grid-cols-3">
                    <?php
                    foreach ($block_data['Dynamic part1'] as $blk_data) {
                        //print_r($blk_data);die($blk_data['content'.$_ar]);
                        ?>
                        <div class="text-center item">
                            <div class="flex items-end image-container aos-init" data-aos="slide-down"><img src="<?= $admin_url . $blk_data['image'] ?>" alt=""></div>
                            <p class="title text-1 aos-init" data-aos="fade-up"><?= $blk_data['title' . $_ar] ?></p>
                            <p class="description text-6 aos-init" data-aos="fade-up"><?= $blk_data['content' . $_ar] ?></p>
                        </div>
                    <?php } ?>
                </div>
                <div class="text-center"><a href="<?= $settings['clients_link_url']['value' . $_ar] ?>" class="btn btn-effect aos-init" data-aos="fade-up"><span class="arrow arrow-left"></span><span class="btn-text"><?= $settings['clients_link_title']['value' . $_ar] ?></span><span class="arrow arrow-right"></span></a></div>

            </div>
        </section>
        <div class="section hero text-center new_hero new_hero2">
            <div class="mb-80p" style="margin-top: 50px;">
                <p class="text-2 text-center section__title aos-init" ><?= $settings['About_static_title4']['value' . $_ar] ?> </p>
            </div>
            <div class="container-3">
                <p class=" text-6"><?= $settings['About_static_content4']['value' . $_ar] ?> </p>
            </div>
        </div>
        <div class="aboutStyles__Container-mofc8b-0 gjjTia section ideas">
            <div class="aboutStyles__Wrapper-mofc8b-1 foveKv">
                <!--<img src="<?= $admin_url ?>about-logo-dark.svg" data-aos="fade-up" class="aboutStyles__LeftImage-mofc8b-2 fGuXqz aos-init">-->
                <img src="<?= $admin_url . $settings['about_static_background']['image'] ?>" data-aos="fade-up" class="aboutStyles__LeftImage-mofc8b-2 fGuXqz aos-init">
                <div data-aos="fade-down" class="aboutStyles__Title-mofc8b-3 ibXbq aos-init"><?= $settings['our_talents_title']['value' . $_ar] ?></div><img data-aos="fade-up" src="<?= $admin_url . $settings['about_static_image']['image'] ?>" class="aboutStyles__Image-mofc8b-4 iXVvcC aos-init">
                <div data-aos="fade-up" class="aboutStyles__Text-mofc8b-5 jegGtt aos-init"><?= $settings['our_talents_first_content']['value' . $_ar] ?></div>
                <div data-aos="fade-up" class="aboutStyles__Text-mofc8b-5 jegGtt aos-init"><?= $settings['our_talents_second_content']['value' . $_ar] ?></div>
            </div>
        </div>
        <div class="section ideas">
            <div class="container-2">
                <div class="text-2 text-center mb-80p bottom-title"><?= $settings['creative_ideas']['value' . $_ar] ?></div>
                <div class="container-3 text-center" style="margin-bottom: 50px;">
                    <p class=" text-6"><?= $settings['our_talents_content']['value' . $_ar] ?> </p>
                </div>
                <div class="grid grid-cols-1 md:grid-cols-3 items">
                    <?php 
                    $i = 1;
                    foreach ($block_data['Dynamic part2'] as $blk_data) { ?>
                        <div class="item">
                            <div class="number"><?= $i ?></div>
                            <div class="name"><?= $blk_data['title' . $_ar] ?></div>
                            <div class="description"><?= $blk_data['content' . $_ar] ?></div>
                        </div>
                    <?php 
                    $i++;
                    } ?>
                </div>
            </div>
        </div>
        
        <div class="section contact">
            <div class="container-2">
                <div class="flex flex-wrap justify-between">
                    <div class="text">
                        <div class="text-2 aos-init" data-aos="slide-up"><?= $settings['contact_title']['value' . $_ar] ?></div>
                        <div class="text-6"><?= $settings['contact_content']['value' . $_ar] ?></div>
                    </div>
                    <div class="right text-center w-full md:w-auto"><a href="<?= $settings['about_CTA_link_url']['value' . $_ar] ?>" class="btn btn-effect btn-dark aos-init" data-aos="slide-left"><span class="arrow arrow-left"></span><span class="btn-text"><?= $settings['about_CTA_link_title']['value' . $_ar] ?></span><span class="arrow arrow-right"></span></a></div>
                </div>
            </div>
        </div>
    </div>
</div>