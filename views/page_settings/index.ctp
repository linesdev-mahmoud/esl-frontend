<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb">
            <!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?= $this->Html->url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Page</li>
        </ol><!-- End breadcrumb -->

    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="card-title">Goods</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                            <thead>

                                <tr>
                                    <th class="wd-15p">#</th>
                                    <th class="wd-15p">Title</th>
                                    <th class="wd-15p">Modified by</th>


                                    <th class="wd-15p">status</th>
                                    <th class="wd-15p">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($pageSettings as $pageSetting):
                                    $class = null;
                                    if ($i++ % 2 == 0) {
                                        $class = ' class="altrow"';
                                    }
                                    ?>
                                    <tr<?php echo $class; ?>>
                                        <td><?php echo $pageSetting['PageSetting']['id']; ?>&nbsp;</td>
                                        <td><?php echo $pageSetting['PageSetting']['title']; ?>&nbsp;</td>
                                        <td><?php echo $usermod[$pageSetting['PageSetting']['modified_by']]; ?>&nbsp;</td>
                                        
                                        <td>
                                            <?php if ($pageSetting['PageSetting']['status'] == 0) { ?>
                                                <i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
                                            <?php } else { ?>
                                                <i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
                                            <?php } ?>
                                        </td>

                                        <td>
                                            <div class="input-group">

                                                <!--
                                                <?php
                                                /*echo $this->Html->link(
                                                        '<span><i class="fe fe-eye"></i></span>',
                                                        ['action' => 'view', $pageSetting['PageSetting']['id']],
                                                        ['escape' => false, 'class' => 'btn bg-primary text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'View']
                                                )*/
                                                ?>
                                                -->

                                                <?=
                                                $this->Html->link(
                                                        '<span><i class="si si-pencil"></i></span>',
                                                        ['action' => 'edit', $pageSetting['PageSetting']['id'],$pageSetting['PageSetting']['module']],
                                                        ['escape' => false, 'class' => 'btn btn-yellow text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                )
                                                ?>
                                                <?php 
                                                /*echo $this->Html->link(
                                                        '<span><i class="si si-trash"></i></span>',
                                                        ['action' => 'delete', $pageSetting['PageSetting']['id']],
                                                        ['escape' => false, 'class' => 'btn bg-danger text-white btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Delete'], sprintf(__('Do you want to delete %s?', true), $good['Good']['id'])
                                                )*/
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
<?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
            <!-- section-wrapper -->
        </div>
    </div>
    <!-- row end -->

</div>

