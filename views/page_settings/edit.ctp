<div class="side-app">
    <?php
    $checked = "";
    if ($this->data['PageSetting']['status'] == 1) {
        $checked = "checked";
    } else {
        $checked = "";
    }
    ?>
    <!-- page-header -->
    <!--    <div class="page-header">
            <ol class="breadcrumb">
                
                <li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/goodscategories/view/") . $goodcatid; ?>">Goods Categories</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit Page</li>
            </ol>
        </div>-->
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <?php echo $this->Form->create('PageSetting', array('type' => 'file', 'url' => '/' . $this->params['url']['url'], 'class' => 'form-horizontal form-material')); ?>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit Page</h3>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>
                                    Title
                                    <?php
                                    echo $this->Form->input('title', array('label' => false, 'placeholder' => 'Enter Title', 'class' => 'form-control form-control-line', 'disabled' => 'disabled'));
                                    echo $this->Form->input('module', array('type' => 'hidden', 'value' => $module));
                                    ?>
                                </label>
                            </div>
                        </div>

                        <!--<div class="form-group col-lg-6 col-md-6">
                            <label >Title Arabic
                        <?php
                        echo $this->Form->input('title_ar', array('label' => false, 'placeholder' => 'Enter Title Arabic', 'class' => 'form-control form-control-line', 'required' => 'required'));
                        ?></label>

                        </div>-->
                        <div class="form-group col-lg-6 col-md-6">
                            <label >
                                Enter Value
                                <?php
                                echo $this->Form->input('value', array('label' => false, 'placeholder' => 'Enter Value', 'class' => 'form-control form-control-line', 'required' => 'required'));
                                ?>
                            </label>

                        </div>
                        <div class="form-group col-lg-6 col-md-6">
                            <label >
                                Enter Value Arabic
                                <?php
                                echo $this->Form->input('value_ar', array('label' => false, 'placeholder' => 'Enter Value Arabic', 'class' => 'form-control form-control-line', 'required' => 'required'));
                                ?>
                            </label>

                        </div>
                        <div class="form-group col-lg-12 col-md-12">
                            <label class="custom-switch form-group">Active
                                <input type="checkbox" name="data[PageSetting][status]" class="custom-switch-input" <?= $checked ?>>
                                <span class="custom-switch-indicator"></span>
                            </label>

                        </div>

                        <div class="card-footer text-right col-lg-12 col-md-12">

                            <input type="submit" class="btn btn-success mt-1" value="Save">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row end -->

    </div>
</div>

