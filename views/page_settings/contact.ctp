<script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
<!--<script src="<?= $this->Html->url('/') ?>js/bootstrap.min.js"></script>-->
<!--<script src="<?= $this->Html->url('/') ?>js/bootstrap-multiselect.min.js"></script>-->
<!--<link rel="stylesheet" href="<?= $this->Html->url('/') ?>css/bootstrap.min.css">-->
<!--<link rel="stylesheet" href="<?= $this->Html->url('/') ?>css/bootstrap-multiselect.min.css">-->
<div class="Content login_page">
    <div class="page page-get-started ">
        <div class="section hero text-center">
            <div class="container-2">
                <?php echo $this->Session->flash(); ?>
                <h1>
                    <?= __('Drop us a line') ?></h1>
            </div>
            <div class="container-3">
                <h2>
                    <?= __('Fill out the below form and an ESL representative will contact you as soon as possible.') ?></h2>
            </div>
        </div>
        <div class="container">
            
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills mb-3 col-sm-12 col-xs-12 col-md-8 col-lg-8 login_tabs" id="pills-tab" role="tablist">

                        <li class="nav-item col-3 no_padding">
                            <a class="nav-link active" id="product_cert" data-toggle="pill" href="#cert" role="tab" aria-controls="pills-profile" aria-selected="true">
                                <?= __('Product Certification') ?>
                            </a>
                        </li>
                        <li class="nav-item col-3 no_padding">
                            <a class="nav-link" id="fire_test" data-toggle="pill" href="#fire" role="tab" aria-controls="pills-home" aria-selected="false">
                                <?= __('Fire Testing') ?>
                            </a>
                        </li>
                        <li class="nav-item col-3 no_padding">
                            <a class="nav-link" id="market_entry" data-toggle="pill" href="#market" role="tab" aria-controls="pills-home" aria-selected="false">
                                <?= __('Market Entry') ?>
                            </a>
                        </li>


                        <li class="nav-item col-3 no_padding">
                            <a class="nav-link" id="labelling" data-toggle="pill" href="#label" role="tab" aria-controls="pills-profile" aria-selected="false">
                                <?= __('Labelling') ?>
                            </a>
                        </li>

                    </ul>
                    <form method="post">
                        <input type="hidden" value="Product Certification" name="request_type">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="cert" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 form_content">
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Type of Certification') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Which category applies to you?') ?></div>
                                            </div>


                                            <div class="row btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="type" value="New Product Certificate" id="option1" autocomplete="off"> <?= __('New Product Certificate') ?>
                                                </label>
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="type" value="Certificate Renewal" id="option2" autocomplete="off"> <?= __('Certificate Renewal') ?>
                                                </label>
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="type" value="Change in existing Certificate" id="option3" autocomplete="off"> <?= __('Change in existing Certificate') ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Type of Product or System') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What is your product or system?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="product_name" type="text" class="form-control" placeholder="<?= __('What is your product or system?') ?>" >
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Type of Application') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What are the intended uses for the product or system?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="intended_use" type="text" class="form-control" placeholder="<?= __('What are the intended uses for the product or system?') ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Product Standard') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Which standards are you interested in?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="standards" type="text" class="form-control" placeholder="<?= __('Which standards are you interested in?') ?>" >
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="fire" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 form_content">
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Type of Product or System') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What is your product or system?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="product_name2" type="text" class="form-control" placeholder="<?= __('Product description') ?>" >
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Types of Application') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What are the intended uses for the product or system?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="intended_use2" type="text" class="form-control" placeholder="<?= __('What are the intended uses for the product or system? ') ?>">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Market') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Are the test results intended for the UAE market?') ?></div>
                                            </div>
                                            <div class="row btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary" id="uae_yes">
                                                    <input type="radio" name="market" value="Yes" autocomplete="off"> <?= __('Yes') ?>
                                                </label>
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary" id="uae_no">
                                                    <input type="radio" name="market" value="No" autocomplete="off"> <?= __('No') ?>
                                                </label>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <?php
                                                        echo $this->Form->input('country_id', array('name'=>'country_id','label' => false, 'div' => false, 'empty' => __('Country', true), 'class' => 'form-control market_country', 'options' => $countries));
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Certification') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Is your testing request for certification purposes?') ?></div>
                                            </div>
                                            <div class="row btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="cert" value="Yes" autocomplete="off"> <?= __('Yes') ?>
                                                </label>
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="cert" value="No" autocomplete="off"> <?= __('No') ?>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Test Standard') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Which test standards are you interested in? ') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="standards2" type="text" class="form-control" placeholder="<?= __('Which test standards are you interested in?') ?>" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="market" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 form_content">
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Market') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Which market are you interested in?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <?php
                                                        echo $this->Form->input('country_id', array('name'=>'country_id2','label' => false, 'div' => false, 'empty' => __('Country', true), 'class' => 'form-control', 'options' => $countries));
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Type of Product or System') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What is your product or system?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="product_name3" type="text" class="form-control" placeholder="<?= __('What is your product or system?') ?>" >
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Types of Application') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What are the intended uses for the product or system?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="intended_use3" type="text" class="form-control" placeholder="<?= __('What are the intended uses for the product or system?') ?>" >
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Product Standard') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Which standards are you interested in?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="standards3" type="text" class="form-control" placeholder="<?= __('Which standards are you interested in?') ?>" >
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Fire Testing') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Did you test your product or system?') ?></div>
                                            </div>
                                            <div class="row btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="fire" value="Yes" autocomplete="off"> <?= __('Yes') ?>
                                                </label>
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="fire" value="No" autocomplete="off"> <?= __('No') ?>
                                                </label>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Certification') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('Do you have product certificate?') ?></div>
                                            </div>
                                            <div class="row btn-group btn-group-toggle" data-toggle="buttons">
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="cert2" value="Yes" autocomplete="off"> <?= __('Yes') ?>
                                                </label>
                                                <label class="col-lg-4 col-sm-12 col-xs-12 btn btn-secondary">
                                                    <input type="radio" name="cert2" value="No" autocomplete="off"> <?= __('No') ?>
                                                </label>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="label" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12 col-md-8 col-lg-8 form_content">
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Type of Product or System') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What is your product which will receive label/mark?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="product_name4" type="text" class="form-control" placeholder="<?= __('What is your product which will receive label/mark?') ?>" >
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group container-5">
                                            <div class="form-header" style="margin-top: 50px;">
                                                <div class="text-5 form-header__header">
                                                    <?= __('Dimensions') ?></div>
                                                <div class="text-9 form-header__sub">
                                                    <?= __('What are the dimensions in millimetre of the product area available to apply the label?') ?></div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                                <div class="form-row">
                                                    <div class="col-sm-12">
                                                        <input name="dimension" type="text" class="form-control" placeholder="<?= __('What are the dimensions in millimetre of the product area available to apply the label?') ?>" >
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group container-5">
                                <div class="form-header" style="margin-top: 50px;">
                                    <div class="text-9 form-header__sub">
                                        <?= __('What else should we know about your request?') ?></div>
                                    <div class="text-5 form-header__header" style="margin-top: 20px;">
                                        <?= __('Contact Information*') ?></div>
                                    <div class="text-9 form-header__sub">
                                        <?= __('Fill in your personal details!') ?></div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-lg-12 form_content register__form">
                                    <div class="form-row">

                                        <div class="col-sm-6">
                                            <input name="first_name" type="text" class="form-control" placeholder="<?= __('First Name') ?>" required="required">
                                        </div>
                                        <div class="col-sm-6">
                                            <input name="last_name" type="text" class="form-control" placeholder="<?= __('Last Name') ?>" required="required">
                                        </div>
                                        <div class="col-sm-6">
                                            <input name="company_name" type="text" class="form-control" placeholder="<?= __('Company') ?>" required="required" >
                                        </div>
                                        <div class="col-sm-6">
                                            <?php
                                            echo $this->Form->input('country_id', array('name'=>'main_country','id' => 'CustomerCountryId', 'label' => false, 'div' => false, 'empty' => __('Country', true), 'class' => 'form-control', 'options' => $countries, 'required' => 'required', 'onchange' => 'selectcode()'));
                                            ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <input name="corporate_email" type="email" class="form-control" placeholder="<?= __('Corporate Email') ?>" required="required" id="corporate_email">
                                        </div>
                                        <div class="col-sm-6">
                                            <!--<label style="float: left;padding-top: 0px;border-right: unset" id="codeid" class="col-4 form-control"></label>-->
                                            <input style="float: left;" id="CustomerPhonNumber" name="phone_number" type="number"  class= 'form-control col-12'  placeholder = "<?= __('Phone Number', true) ?>" required = 'required'>
                                            <input id="phone_number" name="phone_number1" type="hidden">
                                            
                                        </div>


                                        <div class="col-sm-12">
                                            <textarea class="form-control md:col-span-2" id="detail" name="notes" placeholder="<?= __('What else should we know about your project?') ?>"></textarea>
                                        </div>

                                    </div>

                                    <div class="btn_login col-sm-12">
                                        <button type="submit" class="btn btn-primary" name="send_contact" id="send_contact">
                                            <span class="arrow arrow-left"></span>
                                            <span class="btn-text"><?= __('Submit') ?></span>
                                            <span class="arrow arrow-right"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.market_country').hide();
    $("#uae_yes").click(function () {
        var uae_yes = $('#uae_yes input:radio[name="market"]');
        uae_yes.prop('checked', true);
        //uae_yes.val(1);
        $('.market_country').hide();
        console.log(uae_yes.val());
    });
    $("#uae_no").click(function () {
        var uae_no = $('#uae_no input:radio[name="market"]');
        uae_no.prop('checked', true);
        //uae_no.val(0);
        console.log(uae_no.val());
            $('.market_country').show();
    });
<?php
$codedd = json_encode($codecountries)
?>
    var codevalues =<?= $codedd ?>;
    //console.log(codevalues);
    var countryvalue = $('#CustomerCountryId').val();
    if (countryvalue != '') {
        console.log(countryvalue);
        $('#codeid').text(codevalues[countryvalue]);
    } else {
        $('#codeid').text('+00');
    }

    function selectcode() {
        //var codevalues=<?= $codedd ?>;
        //console.log(codevalues);
        var countryvalue = $('#CustomerCountryId').val();
        if (countryvalue == '1') {
            $('#CustomerPhonNumber').val('5');
        } else {
            $('#CustomerPhonNumber').val(' ');
        }
        if (countryvalue != '') {
            $('#codeid').text(codevalues[countryvalue]);

        } else {
            $('#codeid').text('+00');
        }
        //$('#CustomerPhonNumber').val($('#codeid').text()+$('#CustomerPhonNumber').val());

    }
    
    /*$('#send_contact').click(function(){
        $('#phone_number').val($('#codeid').text()+$('#CustomerPhonNumber').val());
    });*/
    
    $('#product_cert').click(function(){
        //alert();
        var request_type = $('input[name="request_type"]');
        request_type.val('Product Certification');
        console.log(request_type.val());
    });
    
    $('#fire_test').click(function(){
        //alert();
        var request_type = $('input[name="request_type"]');
        request_type.val('Fire Testing');
        console.log(request_type.val());
    });
    
    $('#market_entry').click(function(){
        //alert();
        var request_type = $('input[name="request_type"]');
        request_type.val('Market Entry');
        console.log(request_type.val());
    });
    
    $('#labelling').click(function(){
        //alert();
        var request_type = $('input[name="request_type"]');
        request_type.val('Labelling');
        console.log(request_type.val());
    });

</script>