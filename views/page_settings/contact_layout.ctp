<!DOCTYPE html>
<html>
    <head>
        <style>
            .title{font-weight: bold;font-size: 20px;text-align: center;}
            .container{
                padding: 30px;
            }
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
                margin-top: 20px;
                margin-bottom: 20px;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 10px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
            td.strong{font-weight: bold;}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="title"><?= __('Request for ') . $form['request_type'] ?></div>
            <?php if ($form['request_type'] == 'Product Certification') { ?>
                <table>
                    <?php if(!empty($form['type'])){?>
                    <tr>
                        <td class="strong"><?= __('Type of Certification') ?></td>
                        <td><?= $form['type'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['product_name'])){?>
                    <tr>
                        <td class="strong"><?= __('Type of Product or System') ?></td>
                        <td><?= $form['product_name'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['intended_use'])){?>
                    <tr>
                        <td class="strong"><?= __('Type of Application') ?></td>
                        <td><?= $form['intended_use'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['standards'])){?>
                    <tr>
                        <td class="strong"><?= __('Product Standard') ?></td>
                        <td><?= $form['standards'] ?></td>
                    </tr>
                    <?php }?>
                </table>
            <?php } ?>
            <?php if ($form['request_type'] == 'Fire Testing') { ?>
                <table>
                    <?php if(!empty($form['product_name2'])){?>
                    <tr>
                        <td class="strong"><?= __('Type of Product or System') ?></td>
                        <td><?= $form['product_name2'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['intended_use2'])){?>
                    <tr>
                        <td class="strong"><?= __('Types of Application') ?></td>
                        <td><?= $form['intended_use2'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['market'])){?>
                    <tr>
                        <td class="strong"><?= __('Market') ?></td>
                        <td><?= $form['market'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['country_id'])){?>
                    <tr>
                        <td class="strong"><?= __('Market Country') ?></td>
                        <td><?= $countries[$form['country_id']] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['cert'])){?>
                    <tr>
                        <td class="strong"><?= __('Certification') ?></td>
                        <td><?= $form['cert'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['standards2'])){?>
                    <tr>
                        <td class="strong"><?= __('Test Standard') ?></td>
                        <td><?= $form['standards2'] ?></td>
                    </tr>
                    <?php }?>
                </table>
            <?php } ?>
            <?php if ($form['request_type'] == 'Market Entry') { ?>
                <table>
                    <?php if(!empty($form['country_id2'])){?>
                    <tr>
                        <td class="strong"><?= __('Market') ?></td>
                        <td><?= $countries[$form['country_id2']] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['product_name3'])){?>
                    <tr>
                        <td class="strong"><?= __('Type of Product or System') ?></td>
                        <td><?= $form['product_name3'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['intended_use3'])){?>
                    <tr>
                        <td class="strong"><?= __('Types of Application') ?></td>
                        <td><?= $form['intended_use3'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['standards3'])){?>
                    <tr>
                        <td class="strong"><?= __('Product Standard') ?></td>
                        <td><?= $form['standards3'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['fire'])){?>
                    <tr>
                        <td class="strong"><?= __('Fire Testing') ?></td>
                        <td><?= $form['fire'] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['cert2'])){?>
                    <tr>
                        <td class="strong"><?= __('Certification') ?></td>
                        <td><?= $form['cert2'] ?></td>
                    </tr>
                    <?php }?>
                </table>
            <?php } ?>
            <?php if ($form['request_type'] == 'Labelling') { ?>
                <table>
                    <?php if(!empty($form['product_name4'])){?>
                    <tr>
                        <td class="strong"><?= __('Type of Product or System') ?></td>
                        <td><?= $countries[$form['product_name4']] ?></td>
                    </tr>
                    <?php }?>
                    <?php if(!empty($form['dimension'])){?>
                    <tr>
                        <td class="strong"><?= __('Dimensions') ?></td>
                        <td><?= $form['dimension'] ?></td>
                    </tr>
                    <?php }?>
                </table>
            <?php } ?>
            <div class="title"><?= __('Contact Information')?></div>
            <table>
                    
                    <tr>
                        <td class="strong"><?= __('First Name') ?></td>
                        <td><?= $form['first_name'] ?></td>
                    </tr>
                    
                    
                    <tr>
                        <td class="strong"><?= __('Last Name') ?></td>
                        <td><?= $form['last_name'] ?></td>
                    </tr>
                    
                    
                    <tr>
                        <td class="strong"><?= __('Company') ?></td>
                        <td><?= $form['company_name'] ?></td>
                    </tr>
                    
                    
                    <tr>
                        <td class="strong"><?= __('Country') ?></td>
                        <td><?= $countries[$form['main_country']] ?></td>
                    </tr>
                    <tr>
                        <td class="strong"><?= __('Corporate Email') ?></td>
                        <td><?= $form['corporate_email'] ?></td>
                    </tr>
                    <tr>
                        <td class="strong"><?= __('Phone Number') ?></td>
                        <td><?= $form['phone_number'] ?></td>
                    </tr>
                    <tr>
                        <td class="strong"><?= __('What else should we know about your project?') ?></td>
                        <td><?= $form['notes'] ?></td>
                    </tr>
                </table>
        </div>
    </body>
</html>