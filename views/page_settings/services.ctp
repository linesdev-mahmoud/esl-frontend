<div class="Content">
    <div class="page page-service">
        <div class="section hero text-center new_hero">
            <div class="container-2">
                <h1><?=$settings['our_service_code']['value'.$_ar]?></h1>
            </div>
            <div class="container-3">
                <h2><?=$settings['our_service_code_content']['value'.$_ar]?></h2>
            </div>
        </div>
        <section class="section multi-columns bg-light-grey-two co-develop">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['Co-develop_product_title']['value'.$_ar]?></p>
                    <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['Co_develop_product_content']['value'.$_ar]?></p>
                </div>
                <div class="items grid grid-cols-1 md:grid-cols-3">
                     <?php foreach($block_data['Dynamic part1'] as $blk_data){ ?>
                    <div class="text-center item">
                        <div class="flex items-end image-container aos-init" data-aos="slide-down"><img src="<?= $admin_url. $blk_data['image']?>" alt=""></div>
                        <p class="title text-5 aos-init" data-aos="fade-up"><?=$blk_data['title'.$_ar]?></p>
                        <p class="description text-6 aos-init" data-aos="fade-up"><?=$blk_data['content'.$_ar]?></p>
                    </div>
                    <?php } ?>
                </div>
                <div class="text-center"><a href="<?=$settings['get_started_link_url1']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['get_started_link_title1']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
            </div>
        </section>
        <section class="section multi-columns bg-white ">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['talent_solutions_title']['value'.$_ar]?></p>
                    <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['talent_solutions_content']['value'.$_ar]?></p>
                </div>
                <div class="items grid grid-cols-1 md:grid-cols-3">
                   <?php foreach($block_data['Dynamic part2'] as $blk_data){ ?>
                    <div class="text-center item">
                        <div class="flex items-end image-container aos-init" data-aos="slide-down"><img src="<?= $admin_url. $blk_data['image']?>" alt=""></div>
                        <p class="title text-5 aos-init" data-aos="fade-up"><?=$blk_data['title'.$_ar]?></p>
                        <p class="description text-6 aos-init" data-aos="fade-up"><?=$blk_data['content'.$_ar]?></p>
                    </div>
                     <?php } ?>
                
                </div>
                <div class="text-center"><a href="<?=$settings['start_hire_link_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['start_hire_link_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
            </div>
        </section>
        <div class="section bg-light-grey-two mb-30p">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['Quick_project_title']['value'.$_ar]?></p>
                    <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['Quick_project_content']['value'.$_ar]?></p>
                </div>
                <div class="grid grid-cols-3 md:grid-cols-5">
                   <?php foreach($block_data['Dynamic part3'] as $blk_data){ ?>
                    <div class="item">
                        <div class="image-container"><img src="<?= $admin_url. $blk_data['image']?>" alt=""></div>
                        <div class="text-center"><?=$blk_data['title'.$_ar]?></div>
                    </div>
                       <?php } ?>
                    
                </div>
                <div class="text-center"><a href="<?=$settings['get_started_link_url2']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['get_started_link_title2']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
            </div>
        </div>
        <div class="section contact">
            <div class="container-2">
                <div class="flex flex-wrap justify-between">
                    <div class="text">
                        <div class="text-2 aos-init" data-aos="slide-up"><?=$settings['contact_title']['value'.$_ar]?></div>
                        <div class="text-6"><?=$settings['contact_content']['value'.$_ar]?></div>
                    </div>
                    <div class="right text-center w-full md:w-auto"><a href="<?=$settings['get_started_CTA_link_url']['value'.$_ar]?>" class="btn btn-effect btn-dark aos-init" data-aos="slide-left"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['get_started_CTA_link_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
                </div>
            </div>
        </div>
    </div>
</div>