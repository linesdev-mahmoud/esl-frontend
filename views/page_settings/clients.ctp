<div class="Content">
    <div class="page page-clients">
        <div class="section hero text-center new_hero">
            <div class="container-2">
                <h1><?=$settings['our_clients_title']['value'.$_ar]?></h1>
            </div>
            <div class="container-3">
                <h2><?=$settings['our_clients_content']['value'.$_ar]?></h2>
            </div>
        </div>
        <div class="section clients">
            <div class="container-2">
                <div class="text-center title mb-80p"><?=$settings['company_title']['value'.$_ar]?></div>
                <div class="grid grid-cols-2 md:grid-cols-4">
                   <?php foreach($block_data['Dynamic part1'] as $blk_data){ ?>
                    <div class="item">
                        <div class="wrap">
                            <div class="image-container"><img src="<?= $admin_url. $blk_data['image']?>" alt=""></div>
                            <div class="name"><?=$blk_data['title'.$_ar]?></div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="text-center more"><?=$settings['and_more']['value'.$_ar]?></div>
            </div>
        </div>
        <div class="section contact">
            <div class="container-2">
                <div class="flex flex-wrap justify-between">
                    <div class="text">
                        <div class="text-2 aos-init" data-aos="slide-up"><?=$settings['contact_title']['value'.$_ar]?></div>
                        <div class="text-6"><?=$settings['contact_content']['value'.$_ar]?></div>
                    </div>
                    <div class="right text-center w-full md:w-auto"><a href="<?=$settings['clients_CtA_link_url']['value'.$_ar]?>" class="btn btn-effect btn-dark aos-init" data-aos="slide-left"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['clients_CtA_link_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
                </div>
            </div>
        </div>
    </div>
</div>