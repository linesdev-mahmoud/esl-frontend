<div class="Content page-about">
    <div class="page page-how-it-works">
        <div class="section hero text-center new_hero">
            <div class="container-2">
                <h1><?=$settings['how_it_works_part1_title']['value'.$_ar]?></h1>
            </div>
            <div class="container-3">
                <p class=" text-6"><?=$settings['how_it_works_part1_content']['value'.$_ar]?></p>
            </div>
            <div class="mb-80p mt-50p" style="margin-top: 50px;">
                <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2-2_title']['value'.$_ar]?></p>
                <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2-2_content']['value'.$_ar]?></p>
            </div>
            <div class="mb-80p mt-50p" style="margin-top: 50px;">
                <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2-3_title']['value'.$_ar]?></p>
                <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2-3_content']['value'.$_ar]?></p>
            </div>
        </div>
        <div class="section vetting-process bg-light-grey-two new_hero2">
            <div class="mb-80p">
                <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2_title']['value'.$_ar]?></p>
                <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2_content']['value'.$_ar]?></p>
            </div>
            
            <div class="section ideas">
            <div class="container-2">
                <div class="text-2 text-center mb-80p bottom-title"><?= $settings['creative_ideas']['value' . $_ar] ?></div>
                <div class="container-3 text-center" style="margin-bottom: 50px;">
                    <p class=" text-6"><?= $settings['our_talents_content']['value' . $_ar] ?> </p>
                </div>
                <div class="grid grid-cols-1 md:grid-cols-4 items">
                        <div class="item">
                            <div class="name"><?= $settings['ways_part1_title']['value' . $_ar] ?> </div>
                            <div class="description"><?= $settings['ways_part1_content']['value' . $_ar] ?></div>
                            <?php if(!empty($settings['ways_part1_link_text']['value' . $_ar])){?>
                            <div class="text-left" style="margin-top:20px;">
                                <a href="<?=$settings['ways_part1_link_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up">
                                    <span class="arrow arrow-left"></span>
                                    <span class="btn-text"><?= $settings['ways_part1_link_text']['value' . $_ar] ?></span>
                                    <span class="arrow arrow-right"></span>
                                </a>
                            </div>
                            <?php }?>
                        </div>
                        <div class="item">
                            <div class="name"><?= $settings['ways_part2_title']['value' . $_ar] ?> </div>
                            <div class="description"><?= $settings['ways_part2_content']['value' . $_ar] ?></div>
                            <?php if(!empty($settings['ways_part2_link_text']['value' . $_ar])){?>
                            <div class="text-left" style="margin-top:20px;">
                                <a href="<?=$settings['ways_part2_link_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up">
                                    <span class="arrow arrow-left"></span>
                                    <span class="btn-text"><?= $settings['ways_part2_link_text']['value' . $_ar] ?></span>
                                    <span class="arrow arrow-right"></span>
                                </a>
                            </div>
                            <?php }?>
                        </div>
                        <div class="item">
                            <div class="name"><?= $settings['ways_part3_title']['value' . $_ar] ?> </div>
                            <div class="description"><?= $settings['ways_part3_content']['value' . $_ar] ?></div>
                            <?php if(!empty($settings['ways_part3_link_text']['value' . $_ar])){?>
                            <div class="text-left" style="margin-top:20px;">
                                <a href="<?=$settings['ways_part3_link_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up">
                                    <span class="arrow arrow-left"></span>
                                    <span class="btn-text"><?= $settings['ways_part3_link_text']['value' . $_ar] ?></span>
                                    <span class="arrow arrow-right"></span>
                                </a>
                            </div>
                            <?php }?>
                        </div>
                    <div class="item">
                            <div class="name"><?= $settings['ways_part4_title']['value' . $_ar] ?> </div>
                            <div class="description"><?= $settings['ways_part4_content']['value' . $_ar] ?></div>
                            <?php if(!empty($settings['ways_part4_link_text']['value' . $_ar])){?>
                            <div class="text-left" style="margin-top:20px;">
                                <a href="<?=$settings['ways_part4_link_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up">
                                    <span class="arrow arrow-left"></span>
                                    <span class="btn-text"><?= $settings['ways_part4_link_text']['value' . $_ar] ?></span>
                                    <span class="arrow arrow-right"></span>
                                </a>
                            </div>
                            <?php }?>
                        </div>
                </div>
            </div>
        </div>
            <div class="steps grid grids-col-1">
                <div class="mb-50p mt-50p" style="margin-top: 50px;">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2-4_title']['value'.$_ar]?></p>
                    <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part2-4_content']['value'.$_ar]?></p>
                </div>
                  <?php foreach($block_data['Dynamic part1'] as $blk_data){ ?>
                <div class="step">
                    <div class="float-left">
                        <div class="image-container table-cell align-middle text-center"><img class="inline-block" src="<?= $admin_url. $blk_data['image']?>" alt=""></div>
                    </div>
                    <div class="right col-398 float-left">
                        <div class="name"><?=$blk_data['title'.$_ar]?></div>
                        <div class="description"><?=$blk_data['content'.$_ar]?></div>
                    </div>
                </div>
                <?php } ?>
      
            </div>
            <div class="text-center" style="margin-top:20px;"><a href="<?=$settings['get_your_certification_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['get_your_certification_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
        </div>
        <section class="section multi-columns bg-white ">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part3_title']['value'.$_ar]?></p>
                    <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part3_content']['value'.$_ar]?></p>
                </div>
                <div class="items grid grid-cols-1 md:grid-cols-4">
                    <?php foreach($block_data['Dynamic part2'] as $blk_data){ ?>
                    <div class="text-center item">
                        <div class="flex items-end image-container aos-init" data-aos="slide-down"><img src="<?= $admin_url. $blk_data['image']?>" alt=""></div>
                        <p class="title text-5 aos-init" data-aos="fade-up"><?=$blk_data['title'.$_ar]?></p>
                        <p class="description text-6 aos-init" data-aos="fade-up"><?=$blk_data['content'.$_ar]?></p>
                    </div>
                    <?php } ?>
              
                </div>
                <div class="text-center"><a href="<?=$settings['get_started_link_url']['value'.$_ar]?>" class="btn btn-effect aos-init" data-aos="fade-up"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['get_started_link_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
            </div>
        </section>
        <div class="section get-started-steps bg-light-grey-two">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?=$settings['how_it_works_part4_title']['value'.$_ar]?></p>
                </div>
                <div class="items grid grid-cols-1 md:grid-cols-4">
                        <?php foreach($block_data['Dynamic part3'] as $k=> $blk_data){ ?>
                    <div class="item aos-init" data-aos="fade-left" data-aos-delay="<?= $k*300?>">
                        <div class="name"><?=$blk_data['title'.$_ar]?></div>
                        <div class="description"><?=$blk_data['content'.$_ar]?></div>
                    </div>
                     <?php } ?>
                </div>
            </div>
        </div>
        <div class="section contact">
            <div class="container-2">
                <div class="flex flex-wrap justify-between">
                    <div class="text">
                        <div class="text-2 aos-init" data-aos="slide-up"><?=$settings['contact_title']['value'.$_ar]?></div>
                        <div class="text-6"><?=$settings['contact_content']['value'.$_ar]?></div>
                    </div>
                    <div class="right text-center w-full md:w-auto"><a href="<?=$settings['how_CTA_link_url']['value'.$_ar]?>" class="btn btn-effect btn-dark aos-init" data-aos="slide-left"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['how_CTA_link_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span></a></div>
                </div>
            </div>
        </div>
    </div>
</div>