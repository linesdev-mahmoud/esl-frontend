<div class="customersdocs index">
	<h2><?php __('Customersdocs');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('customer_id');?></th>
			<th><?php echo $this->Paginator->sort('documenttype_id');?></th>
			<th><?php echo $this->Paginator->sort('issue_date');?></th>
			<th><?php echo $this->Paginator->sort('expiry_date');?></th>
			<th><?php echo $this->Paginator->sort('issued_by');?></th>
			<th><?php echo $this->Paginator->sort('path');?></th>
			<th><?php echo $this->Paginator->sort('url');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($customersdocs as $customersdoc):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $customersdoc['Customersdoc']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($customersdoc['Customer']['legal_name'], array('controller' => 'customers', 'action' => 'view', $customersdoc['Customer']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($customersdoc['Documenttype']['title'], array('controller' => 'documenttypes', 'action' => 'view', $customersdoc['Documenttype']['id'])); ?>
		</td>
		<td><?php echo $customersdoc['Customersdoc']['issue_date']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['expiry_date']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['issued_by']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['path']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['url']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['status']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['created']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['modified']; ?>&nbsp;</td>
		<td><?php echo $customersdoc['Customersdoc']['modified_by']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $customersdoc['Customersdoc']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $customersdoc['Customersdoc']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $customersdoc['Customersdoc']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $customersdoc['Customersdoc']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customersdoc', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers', true), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer', true), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Documenttypes', true), array('controller' => 'documenttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Documenttype', true), array('controller' => 'documenttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>