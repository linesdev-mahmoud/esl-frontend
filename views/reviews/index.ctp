<div class="reviews index">
	<h2><?php __('Reviews');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('application_id');?></th>
			<th><?php echo $this->Paginator->sort('can_label');?></th>
			<th><?php echo $this->Paginator->sort('can_mark');?></th>
			<th><?php echo $this->Paginator->sort('product_name');?></th>
			<th><?php echo $this->Paginator->sort('product_type');?></th>
			<th><?php echo $this->Paginator->sort('product_manufacturer');?></th>
			<th><?php echo $this->Paginator->sort('certificate_no');?></th>
			<th><?php echo $this->Paginator->sort('issued_on');?></th>
			<th><?php echo $this->Paginator->sort('revision_or_version');?></th>
			<th><?php echo $this->Paginator->sort('expiry_date');?></th>
			<th><?php echo $this->Paginator->sort('certificationbody_id');?></th>
			<th><?php echo $this->Paginator->sort('product_description');?></th>
			<th><?php echo $this->Paginator->sort('product_manual');?></th>
			<th><?php echo $this->Paginator->sort('technecal_description');?></th>
			<th><?php echo $this->Paginator->sort('trade_license');?></th>
			<th><?php echo $this->Paginator->sort('product_conformity_certificate');?></th>
			<th><?php echo $this->Paginator->sort('product_coc');?></th>
			<th><?php echo $this->Paginator->sort('classification_or_product_test_report');?></th>
			<th><?php echo $this->Paginator->sort('factory_audit_report');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($reviews as $review):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $review['Review']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($review['Application']['id'], array('controller' => 'applications', 'action' => 'view', $review['Application']['id'])); ?>
		</td>
		<td><?php echo $review['Review']['can_label']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['can_mark']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['product_name']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['product_type']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['product_manufacturer']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['certificate_no']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['issued_on']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['revision_or_version']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['expiry_date']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($review['Certificationbody']['title'], array('controller' => 'certificationbodies', 'action' => 'view', $review['Certificationbody']['id'])); ?>
		</td>
		<td><?php echo $review['Review']['product_description']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['product_manual']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['technecal_description']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['trade_license']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['product_conformity_certificate']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['product_coc']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['classification_or_product_test_report']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['factory_audit_report']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['status']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['created']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['modified']; ?>&nbsp;</td>
		<td><?php echo $review['Review']['modified_by']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $review['Review']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $review['Review']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $review['Review']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $review['Review']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Review', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Applications', true), array('controller' => 'applications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Application', true), array('controller' => 'applications', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Certificationbodies', true), array('controller' => 'certificationbodies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Certificationbody', true), array('controller' => 'certificationbodies', 'action' => 'add')); ?> </li>
	</ul>
</div>