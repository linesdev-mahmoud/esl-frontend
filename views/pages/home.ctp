
<div class="Content">
    <div class="page page-home main" id="fullpage">
        <section  data-anchor="Page 1" class="container-1 hero flex pt-10 relative">

            <div class="left">

                <div class="sc-fzqNqU kikdAh home-slick">
                    <div class="test" dir="ltr"></div>
                    <div class="slick-slider slick-initialized" dir="ltr">
                        <div class="slick-list">
                            <div class="slick-track" style="width: 4837px; opacity: 1; transform: translate3d(-1382px, 0px, 0px);">
                                <div data-index="-1" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true" style="width: 691px;">
                                    <div>
                                        <h1 id="effect" class="first" tabindex="-1" style="width: 100%; display: inline-block;"><?= $banners[0]['Banner']['content' . $_ar] ?> </h1>
                                    </div>
                                </div>
                                <div data-index="0" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: none; width: 691px;">
                                    <div>
                                        <h1 id="effect1" tabindex="-1" style="width: 100%; display: inline-block;"><?= $banners[1]['Banner']['content' . $_ar] ?> </h1>
                                    </div>
                                </div>
                                <div data-index="1" class="slick-slide" tabindex="-1" aria-hidden="true" style="outline: none; width: 691px;">
                                    <div>
                                        <h1 id="effect2" tabindex="-1" style="width: 100%; display: inline-block;"><?= $banners[2]['Banner']['content' . $_ar] ?> </h1>
                                    </div>
                                </div>
                                <div data-index="2" class="slick-slide slick-active slick-current" tabindex="-1" aria-hidden="false" style="outline: none; width: 691px;">
                                    <div>
                                        <h1 id="effect" tabindex="-1" style="width: 100%; display: inline-block;"><?= $banners[3]['Banner']['content' . $_ar] ?></h1>
                                    </div>
                                </div>
                                

                            </div>
                        </div>
                        <ul style="display: block;" class="slick-dots">
                            <li class="slick-active">
                                <div class="custom-slick-dot">
                                    1<span><?= $banners[0]['Banner']['title' . $_ar] ?></span>
                                    <div class="loader loader1"></div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom-slick-dot">
                                    2<!-- -->
                                    <span><?= $banners[1]['Banner']['title' . $_ar] ?></span>
                                    <div class="loader loader2"></div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom-slick-dot">
                                    3<!-- -->
                                    <span><?= $banners[3]['Banner']['title' . $_ar] ?></span>
                                    <div class="loader loader3"></div>
                                </div>
                            </li>
                            <li class="">
                                <div class="custom-slick-dot">
                                    4<!-- -->
                                    <span><?= $banners[4]['Banner']['title' . $_ar] ?></span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right">
                <img
                    srcset="<?= $admin_url . $settings_data['banner_image']['image'] ?>"
                    src="<?= $admin_url . $settings_data['banner_image']['image'] ?>"
                    alt="illustration character"
                    />
            </div>
            <div id="wrapper" class="absolute">
                <div id="wrapper-inner">
                    <div class="text-center"><span id="scroll-title" class="transform rotate-90"><?= __('scroll') ?></span></div>
                    <div id="scroll-down"></div>
                </div>
            </div>
        </section>
        <section  data-anchor="about" class="section bg-yellow no-secret relative aos-init" id="no-secret" data-aos="zoom-in">
            <div class="container-3">
                <p class="text-3">
                    <?= $settings_data['home_part2']['value' . $_ar] ?><br />
                    <br />
                    <?= $settings_data['home_part3']['value' . $_ar] ?>  <br />
                    <br />
                    <?= $settings_data['home_part4']['value' . $_ar] ?><br />
                    <br />
                    <?= $settings_data['home_part5']['value' . $_ar] ?> 
                </p>
                <p class="text-5 text-center aos-init" data-aos="fade-up"><?= $settings_data['home_part6']['value' . $_ar] ?></p>
                <div class="clients grid grid-cols-3 md:grid-cols-3 aos-init" data-aos="fade-right">
                    <?php foreach ($block_data['Dynamic part1'] as $block) { ?>
                        <img src="<?= $admin_url . $block['image'] ?>" alt="" />
                    <?php } ?>
                </div>
                <p class="text-5 text-center aos-init second_txt" data-aos="fade-up"><?= $settings_data['home_part7']['value' . $_ar] ?></p>
            </div>
            <img src="./assets/img/esl-text.png" id="logo-text" class="absolute" alt="" style="transform: matrix(1, 0, 0, 1, 0, 0);" />
        </section>
        <section data-anchor="Page 3" class="section bg-yellow meet-and-hire clearfix" id="meet-and-hire">
            <div class="container-2 flex flex-wrap justify-between white_color">
                <div class="col-398 left">
                    <p class="text-2 aos-init" data-aos="fade-right"><?= $settings_data['home_part8']['value' . $_ar] ?></p>
                    <p class="text-6 aos-init text_replace" data-aos="fade-right">
                        <?= $settings_data['home_part9']['value' . $_ar] ?>

                    </p>

                    <p class="text-6 aos-init" data-aos="fade-right">
                        <?= $settings_data['home_part10']['value' . $_ar] ?>

                    </p>
                    <a href="<?=$settings_data['how_it_works_link_url']['value'.$_ar]?>" target="_parent" class="btn btn-effect btn-dark mb-10 md:mb-0 aos-init" data-aos="fade-up">
                        <span class="arrow arrow-left"></span><span class="btn-text"><?=$settings_data['how_it_works_link_title']['value'.$_ar]?></span><span class="arrow arrow-right"></span>
                    </a>
                </div>
                <div class="grid right">
                    <?php foreach ($block_data['Dynamic part2'] as $block) { ?>
                        <div id="meet-and-hire-col1" class="meet-and-hire-item item bg-white col-398">
                            <div class="image-container image-left"><img src="<?= $admin_url . $block['image'] ?>" alt="" /></div>
                            <div class="text-7 name"><?= $block['title' . $_ar] ?></div>
                            <div class="text-8 description"><?= $block['content' . $_ar] ?></div>
                            <a target="_parent" href="#" class="block read-more text-7"><span><?= __('Read more') ?></span></a>
                        </div>
                    <?php } ?>
                    <!--                                    <div id="meet-and-hire-col2" class="meet-and-hire-item item bg-white col-398">
                                                            <div class="image-container image-left"><img src="<?= $admin_url . $settings[13]['Setting']['image'] ?>" alt="" /></div>
                                                            <div class="text-7 name"><?= $settings[13]['Setting']['title' . $_ar] ?> </div>
                                                            <div class="text-8 description"><?= $settings[13]['Setting']['value' . $_ar] ?></div>
                                                            <a href="#" class="block read-more text-7"><span>Read more</span></a>
                                                        </div>
                                                        <div id="meet-and-hire-col3" class="meet-and-hire-item item bg-white col-398" style="height: 235px;">
                                                            <div class="image-container image-left"><img src="<?= $admin_url . $settings[14]['Setting']['image'] ?>" alt="" /></div>
                                                            <div class="text-7 name"><?= $settings[14]['Setting']['title' . $_ar] ?></div>
                                                            <div class="text-8 description"><?= $settings[14]['Setting']['value' . $_ar] ?>  </div>
                                                            <a href="#" class="block read-more text-7"><span>Read more</span></a>
                                                        </div>
                                                        <div id="meet-and-hire-col4" class="meet-and-hire-item item bg-white col-398">
                                                            <div class="image-container image-left"><img src="<?= $admin_url . $settings[15]['Setting']['image'] ?>" alt="" /></div>
                                                            <div class="text-7 name"><?= $settings[15]['Setting']['title' . $_ar] ?> </div>
                                                            <div class="text-8 description"><?= $settings[15]['Setting']['value' . $_ar] ?> </div>
                                                            <a href="#" class="block read-more text-7"><span>Read more</span></a>
                                                        </div>
                                                        <div id="meet-and-hire-col5" class="meet-and-hire-item item bg-white col-398" style="height: 202px;">
                                                            <div class="image-container image-left"><img src="<?= $admin_url . $settings[16]['Setting']['image'] ?>" alt="" /></div>
                                                            <div class="text-7 name"><?= $settings[16]['Setting']['title' . $_ar] ?></div>
                                                            <div class="text-8 description"><?= $settings[16]['Setting']['value' . $_ar] ?></div>
                                                            <a href="#" class="block read-more text-7"><span>Read more</span></a>
                                                        </div>
                                                        <div id="meet-and-hire-col6" class="meet-and-hire-item item bg-white col-398">
                                                            <div class="image-container image-left"><img src="<?= $admin_url . $settings[17]['Setting']['image'] ?>" alt="" /></div>
                                                            <div class="text-7 name"><?= $settings[17]['Setting']['title' . $_ar] ?></div>
                                                            <div class="text-8 description"><?= $settings[17]['Setting']['value' . $_ar] ?></div>
                                                            <a href="#" class="block read-more text-7"><br><span>Read more</span></a>
                                                        </div>-->






                </div>
            </div>
        </section>
        <section class="section multi-columns bg-white services_section">
            <div class="container-2">
                <div class="mb-80p">
                    <p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?= $settings_data['home_part11']['value' . $_ar] ?></p>
                    <p class="text-6 text-center section__sub-title aos-init" data-aos="fade-up"><?= $settings_data['home_part12']['value' . $_ar] ?>
                    </p>

                </div>
                <div class="items grid grid-cols-1 md:grid-cols-4">
                    <?php foreach ($block_data['Dynamic part3'] as $block) { ?>
                        <div class="text-center item">
                            <div class="flex items-end image-container aos-init" data-aos="slide-down"><img src="<?= $admin_url . $block['image'] ?>" alt="" /></div>
                            <p class="title text-5 aos-init service_title" data-aos="fade-up"><?= $block['title' . $_ar] ?></p>
                            <p class="description text-6 aos-init" data-aos="fade-up"><?= $block['content' . $_ar] ?> </p>
                        </div>
                    <?php } ?>
                </div>
                <div class="text-center">
                    <a target="_parent" href="<?= $settings_data['get_started_link_url']['value' . $_ar] ?>" class="btn btn-effect aos-init" data-aos="fade-up">
                        <span class="arrow arrow-left"></span><span class="btn-text"><?= $settings_data['get_started_link_title']['value' . $_ar] ?></span><span class="arrow arrow-right"></span>
                    </a>
                </div>
            </div>
        </section>
        <div class="section contact new_contact">
            <div class="container-2">
                <div class="flex flex-wrap justify-between">
                    <div class="text">
                        <div class="text-2 aos-init" data-aos="slide-up"><?= $settings[27]['Setting']['title' . $_ar] ?></div>
                        <div class="text-6"><?= $settings[27]['Setting']['value' . $_ar] ?></div>
                    </div>
                    <div class="right text-center w-full md:w-auto">
                        <a target="_parent" href="<?= $this->Html->url('/contact-us') ?>" class="btn btn-effect btn-dark aos-init" data-aos="slide-left">
                            <span class="arrow arrow-left"></span><span class="btn-text">Contact us</span><span class="arrow arrow-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <section class="section services aos-init" data-aos="slide-up">
            <div class="container-2">
                <div class="mb-80p"><p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?= $settings_data['home_part13']['value' . $_ar] ?></p></div>
                <div class="row-1 flex justify-between flex-wrap">
                    <div class="accordion-container aos-init" data-aos="fade-right">
                        <?php
                        $active = 1;
                        foreach ($block_data['Dynamic part4'] as $block) {
                            ?>
                            <div class="item">
                                <div class="accordion name <?php if ($active == 1) {
                            echo 'active';
                        } ?>"><?= $block['title' . $_ar] ?><img class="arrow float-right" src="./assets/img/ic-arrow-left_2.svg" alt="" /></div>
                                <div class="panel description" style="display: <?php if ($active == 1) {
                            echo 'block';
                        } else {
                            echo 'none';
                        } ?>;">
    <?= $block['content' . $_ar] ?>
                                </div>
                            </div>
    <?php
    $active++;
}
?>
                        <!--                                        <div class="item">
                                                                    <div class="accordion name"><?= $settings[30]['Setting']['title' . $_ar] ?><img class="arrow float-right" src="./assets/img/ic-arrow-left_2.svg" alt="" /></div>
                                                                    <div class="panel description" style="display: none;">
<?= $settings[30]['Setting']['value' . $_ar] ?>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                    <div class="accordion name"><?= $settings[31]['Setting']['title' . $_ar] ?><img class="arrow float-right" src="./assets/img/ic-arrow-left_2.svg" alt="" /></div>
                                                                    <div class="panel description" style="display: none;">
<?= $settings[31]['Setting']['value' . $_ar] ?>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                    <div class="accordion name"><?= $settings[32]['Setting']['title' . $_ar] ?><img class="arrow float-right" src="./assets/img/ic-arrow-left_2.svg" alt="" /></div>
                                                                    <div class="panel description" style="display: none;">
<?= $settings[32]['Setting']['value' . $_ar] ?>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                    <div class="accordion name"><?= $settings[33]['Setting']['title' . $_ar] ?><img class="arrow float-right" src="./assets/img/ic-arrow-left_2.svg" alt="" /></div>
                                                                    <div class="panel description" style="display: none;">
<?= $settings[33]['Setting']['value' . $_ar] ?>
                                                                    </div>
                                                                </div>
                                                                <div class="item">
                                                                    <div class="accordion name"><?= $settings[34]['Setting']['title' . $_ar] ?><img class="arrow float-right" src="./assets/img/ic-arrow-left_2.svg" alt="" /></div>
                                                                    <div class="panel description" style="display: none;">
                        <?= $settings[34]['Setting']['value' . $_ar] ?>
                                                                    </div>
                                                                </div>-->
                    </div>
                    <div class="image-container">
<?php
$active_img = 1;
foreach ($block_data['Dynamic part4'] as $block) {
    ?>
                            <img class="accordion-image" src="<?= $admin_url . $block['image'] ?>" style="display: <?php if ($active_img == 1) {
        echo 'block';
    } else {
        echo 'none';
    } ?>;" alt="" />
    <?php
    $active_img++;
}
?>
<!--                                        <img class="accordion-image" src="<?= $admin_url . $settings[30]['Setting']['image'] ?>" style="display: none;" alt="" />
                        <img class="accordion-image" src="<?= $admin_url . $settings[31]['Setting']['image'] ?>" alt="" style="display: none;" />
                        <img class="accordion-image" src="<?= $admin_url . $settings[32]['Setting']['image'] ?>" style="display: none;" alt="" />
                        <img class="accordion-image" src="<?= $admin_url . $settings[33]['Setting']['image'] ?>" style="display: none;" alt="" />
                        <img class="accordion-image" src="<?= $admin_url . $settings[34]['Setting']['image'] ?>" alt="" style="display: none;" />-->
                    </div>

                </div>
                <div class="link-container">
                    <a target="_parent" href="<?= $settings_data['our_services_link_url']['value' . $_ar] ?>" class="btn btn-effect aos-init" data-aos="fade-up">
                        <span class="arrow arrow-left"></span><span class="btn-text"><?= $settings_data['our_services_link_title']['value' . $_ar] ?></span><span class="arrow arrow-right"></span>
                    </a>
                </div>
            </div>
        </section>
        <!--<section class="section testimonials relative">
<div class="container-2">
<div class="mb-80p"><p class="text-2 text-center section__title aos-init" data-aos="fade-up">Trusted by leading organisations</p></div>
<div class="sc-fzqARJ eLpUJW mb-80p items">
<div class="slick-slider slick-initialized">
<div class="slick-list">
<div class="slick-track" style="width: 972px; opacity: 1; transform: translate3d(0px, 0px, 0px);">
<div data-index="0" class="slick-slide slick-active slick-current" tabindex="-1" aria-hidden="false" style="outline: none; width: 324px;">
<div>
<div class="item" tabindex="-1" style="width: 100%; display: inline-block;">
<div class="background">
<div class="image-container"><img src="./assets/img/broker-colour.svg" alt="" /></div>
<div class="content">
"We were able to access a world class team of SEO consultant, developers &amp; UXUI designers assembled to our needs. The project was completed on time and on budget, a
rare occurrence in this industry. We now have a team that we can always go to whenever we need!"
</div>
<div class="name">Louise</div>
<div class="company-name">Broker (Australia)</div>
</div>
</div>
</div>
</div>
<div data-index="1" class="slick-slide slick-active" tabindex="-1" aria-hidden="false" style="outline: none; width: 324px;">
<div>
<div class="item" tabindex="-1" style="width: 100%; display: inline-block;">
<div class="background">
<div class="image-container"><img src="./assets/img/fcb-colour.png" alt="" /></div>
<div class="content">
"We were flying into Singapore for a pitch. While we were on the plane and wondering if we would get a talent due to the short notice, Chanceupon delighted us with one of
the best skilled designers at our SG office. The skilled designer was assigned to be part of our team for the important pitch."
</div>
<div class="name">Jennifer</div>
<div class="company-name">FCB Agency (United States)</div>
</div>
</div>
</div>
</div>
<div data-index="2" class="slick-slide slick-active" tabindex="-1" aria-hidden="false" style="outline: none; width: 324px;">
<div>
<div class="item" tabindex="-1" style="width: 100%; display: inline-block;">
<div class="background">
<div class="image-container"><img src="./assets/img/ey-2-colour.svg" alt="" /></div>
<div class="content">
"We are a strong team missing a UI designer urgently as we have a tight timeline. Within 3 days, Chanceupon provided us with the best UI designer who assisted us across the
borders, remotely. All timelines were met and we created one of the best team!"
</div>
<div class="name">Mark</div>
<div class="company-name">Ernst &amp; Young (Malaysia)</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="text-center">
<a href="#" class="btn btn-effect aos-init" data-aos="fade-up">
<span class="arrow arrow-left"></span><span class="btn-text">View clients</span><span class="arrow arrow-right"></span>
</a>
</div>
</div>
<img src="./assets/img/icon-quotes.svg" class="icon-quotes left absolute z-10 aos-init" alt="" data-aos="fade-right" />
<img src="./assets/img/icon-quotes-right.svg" class="icon-quotes right absolute z-10 aos-init" alt="" data-aos="fade-left" />
</section>-->
        <!--<div class="sc-fzozJi dteCCc">
<div class="sc-fzoLsD fYZyZu container-2">
<div class="sc-fzpans cKJzHK">Chanceupon Insights</div>
<div class="sc-fznKkj cUJXDb">
<div class="slick-slider slick-initialized">
<div class="slick-list">
<div class="slick-track" style="width: 972px; opacity: 1; transform: translate3d(0px, 0px, 0px);">
<div data-index="0" class="slick-slide slick-active slick-current" tabindex="-1" aria-hidden="false" style="outline: none; width: 324px;">
<div>
<div class="sc-fzokOt hLgJkJ">
<div class="sc-fznZeY gJlwEu">
<a href="#">
<div class="sc-fzplWN hRBsWH">
<img src="./assets/img/chanceupon-Heres_A_Smarter_Way_Of_Hiring_(And_It_Isnt_Permanent).jpg" alt="error" />
<div class="sc-fzqBZW eNQuho"></div>
</div>
</a>
<div class="sc-fzoyAV fQsatj">Articles - 3 min read</div>
<div class="sc-fzoLag BNtsP"></div>
<div class="sc-fzoXzr jYxGEJ">
<a href="#">
<div class="sc-fznyAO hrrMRC"><span class="title-text">Here’s A Smarter Way Of Hiring (And It Isn’t Permanent)</span></div>
</a>
<div class="sc-fzqNJr bMndKH">
Recruitment is headed for a revolution. The pandemic has changed the way we work, and naturally, the way we hire. Temporary talent hires are no longer quick fixes, but
fast evolving into ‘go-to’ hiring solutions.
</div>
</div>
<div class="sc-fzpjYC gJohPa">
<img src="./assets/img/cu-talent_profile.png" alt="erorr" class="sc-fznxsB cUWXFh" />
<div class="sc-fznJRM bTIjTR">
<div class="sc-fznWqX dAkvW">Jillian W</div>
<div class="sc-fzoiQi ozSmQ">Contributing Talent</div>
</div>
</div>
<div class="sc-fzoLag BNtsP"></div>
</div>
</div>
</div>
</div>
<div data-index="1" class="slick-slide slick-active" tabindex="-1" aria-hidden="false" style="outline: none; width: 324px;">
<div>
<div class="sc-fzokOt hLgJkJ">
<div class="sc-fznZeY gJlwEu">
<a href="#">
<div class="sc-fzplWN hRBsWH">
<img
src="./assets/img/chanceupon-Future-Proof-Your-Organization-Because-Full-timers-Arent-Here-To-Stay.jpg"
alt="error"
/>
<div class="sc-fzqBZW eNQuho"></div>
</div>
</a>
<div class="sc-fzoyAV fQsatj">Articles - 4 min read</div>
<div class="sc-fzoLag BNtsP"></div>
<div class="sc-fzoXzr jYxGEJ">
<a href="#">
<div class="sc-fznyAO hrrMRC"><span class="title-text">Future-Proof Your Organization Because Full-timers Aren’t Here To Stay</span></div>
</a>
<div class="sc-fzqNJr bMndKH">
Covid-19 has undoubtedly accelerated the pace of remote working. Will full-timers head back to the office soon? Or is remote work going to be the norm from now? What
trends can we expect to see in this evolving future of work?
</div>
</div>
<div class="sc-fzpjYC gJohPa">
<img src="./assets/img/cu-talent_profile(1).png" alt="erorr" class="sc-fznxsB cUWXFh" />
<div class="sc-fznJRM bTIjTR">
<div class="sc-fznWqX dAkvW">Jillian Wong</div>
<div class="sc-fzoiQi ozSmQ">Contributing Talent</div>
</div>
</div>
<div class="sc-fzoLag BNtsP"></div>
</div>
</div>
</div>
</div>
<div data-index="2" class="slick-slide slick-active" tabindex="-1" aria-hidden="false" style="outline: none; width: 324px;">
<div>
<div class="sc-fzokOt hLgJkJ">
<div class="sc-fznZeY gJlwEu">
<a href="#">
<div class="sc-fzplWN hRBsWH">
<img src="./assets/img/chanceupon-Goodbye-Shareconomy-Hello-Flexconomy-1.jpg" alt="error" />
<div class="sc-fzqBZW eNQuho"></div>
</div>
</a>
<div class="sc-fzoyAV fQsatj">Articles - 4 min read</div>
<div class="sc-fzoLag BNtsP"></div>
<div class="sc-fzoXzr jYxGEJ">
<a href="#">
<div class="sc-fznyAO hrrMRC"><span class="title-text">Goodbye Shareconomy. Hello Flexconomy.</span></div>
</a>
<div class="sc-fzqNJr bMndKH">Move over Shareconomy. Your friendlier, flexible cousin has arrived.</div>
</div>
<div class="sc-fzpjYC gJohPa">
<img src="./assets/img/cu-talent_profile(2).png" alt="erorr" class="sc-fznxsB cUWXFh" />
<div class="sc-fznJRM bTIjTR">
<div class="sc-fznWqX dAkvW">Jillian Wong</div>
<div class="sc-fzoiQi ozSmQ">Contributing Talent</div>
</div>
</div>
<div class="sc-fzoLag BNtsP"></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<a href="#" class="btn btn-effect aos-init" style="margin-top: 40px;" data-aos="fade-up">
<span class="arrow arrow-left"></span><span class="btn-text">View more</span><span class="arrow arrow-right"></span>
</a>
</div>
</div>-->
        <div class="section get-started-steps bg-light-grey-two">
            <div class="container-2">
                <div class="mb-80p"><p class="text-2 text-center section__title aos-init" data-aos="fade-up"><?= $settings_data['home_part14']['value' . $_ar] ?></p></div>
                <div class="items grid grid-cols-1 md:grid-cols-4">
<?php
$i = 0;
foreach ($block_data['Dynamic part4'] as $block) {
    ?>
                        <div class="item aos-init" data-aos="fade-left" data-aos-delay="<?= $i * 300 ?>">
                            <div class="name"><?= $block['title' . $_ar] ?></div>
                            <div class="description"><?= $block['value' . $_ar] ?></div>
                        </div>
    <?php
    $i++;
}
?>
                    <!--                                    <div class="item aos-init" data-aos="fade-left" data-aos-delay="300">
                                                            <div class="name"><?= $settings[37]['Setting']['title' . $_ar] ?></div>
                                                            <div class="description"><?= $settings[37]['Setting']['value' . $_ar] ?></div>
                                                        </div>
                                                        <div class="item aos-init" data-aos="fade-left" data-aos-delay="600">
                                                            <div class="name"><?= $settings[38]['Setting']['title' . $_ar] ?></div>
                                                            <div class="description"><?= $settings[38]['Setting']['value' . $_ar] ?></div>
                                                        </div>
                                                        <div class="item aos-init" data-aos="fade-left" data-aos-delay="900">
                                                            <div class="name"><?= $settings[39]['Setting']['title' . $_ar] ?></div>
                                                            <div class="description"><?= $settings[39]['Setting']['value' . $_ar] ?></div>
                                                        </div>-->
                </div>
            </div>
        </div>
        <div class="section contact second">
            <div class="container-2">
                <div class="flex flex-wrap justify-between">
                    <div class="text">
                        <div class="text-2 aos-init" data-aos="slide-up"><?= $settings_data['home_part15']['value' . $_ar] ?></div>
                        <div class="text-6"><?= $settings_data['home_part16']['value' . $_ar] ?></div>
                    </div>
                    <div class="right text-center w-full md:w-auto">
                        <a target="_parent" href="<?= $settings_data['CTA_link_url']['value' . $_ar] ?>" class="btn btn-effect btn-dark aos-init" data-aos="slide-left">
                            <span class="arrow arrow-left"></span><span class="btn-text"><?= $settings_data['CTA_link_title']['value' . $_ar] ?></span><span class="arrow arrow-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
