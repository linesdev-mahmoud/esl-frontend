<div class="side-app">

	<!-- page-header -->
	<div class="page-header">
		<ol class="breadcrumb">
			<!-- breadcrumb -->
			<li class="breadcrumb-item"><a href="<?= $this->Html->url('/') ?>">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page">Customers</li>
		</ol><!-- End breadcrumb -->
		<div class="ml-auto">
			<div class="input-group">
				<a href="<?= $this->Html->url('/customers/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add New Customer</a>

			</div>
		</div>
	</div>
	<!-- End page-header -->

	<!-- row -->
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="card">
				<div class="card-header pb-0">
					<div class="card-title">Customers</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered text-nowrap w-100">
							<thead>

								<tr>
									<th class="wd-15p">First Name</th>
									<th class="wd-15p">Last Name</th>
									<th class="wd-15p">Company Name</th>
									<th class="wd-15p">Country</th>
									<th class="wd-15p">Phon Number</th>
									<th class="wd-15p">Corporate Email</th>
									<th class="wd-15p">status</th>
									<th class="wd-15p">created</th>
									<th class="wd-15p">modified</th>
									<th class="wd-15p">last_login</th>
									<th class="wd-15p">modified_by</th>
									<th class="wd-15p">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
	$i = 0;
	foreach ($customers as $customer):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
								<tr<?php echo $class;?>>
									<td><?php echo $customer['Customer']['first_name']; ?></td>
									<td><?php echo $customer['Customer']['last_name']; ?></td>
									<td><?php echo $customer['Customer']['company_name']; ?></td>
									<td><?php echo $customer['Customer']['country_id']; ?></td>
									<td><?php echo $customer['Customer']['phon_number']; ?></td>
									<td><?php echo $customer['Customer']['corporate_email']; ?></td>
									
									<td>
										<?php if($customer['Customer']['status']==0){?>
										<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
										<?php }else{?>
										<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
										<?php }?>
									</td>
									<td><?php echo $customer['Customer']['created']; ?></td>
									<td><?php echo $customer['Customer']['modified']; ?></td>
									<td><?php echo $customer['Customer']['last_login']; ?></td>
									<td><?php echo $users[$customer['Customer']['modified_by']]; ?></td>
									<td>
										<div class="input-group">

											
<!--
							<?= $this->Html->link(
								'<span><i class="fe fe-eye"></i></span>',
								['action' => 'view',  $customer['Customer']['id']],
								['escape' => false,  'class' => 'btn bg-primary text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'View']
							) ?>
-->


							<?= $this->Html->link(
								'<span><i class="si si-pencil"></i></span>',
								['action' => 'edit',  $customer['Customer']['id']],
								['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
							) ?>
<!--
							<?= $this->Html->link(
								'<span><i class="si si-trash"></i></span>',
								['action' => 'delete',  $customer['Customer']['id']],
								['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $customer['Customer']['id'])
							) ?>
-->
										</div>
									</td>
									</tr>
									<?php endforeach; ?>

							</tbody>
						</table>
					</div>
				</div>
				<!-- table-wrapper -->
			</div>
			<!-- section-wrapper -->
		</div>
	</div>
	<!-- row end -->

</div>

<!--

<div class="customers index">
	<h2><?php __('Customers');?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('legal_name');?></th>
			<th><?php echo $this->Paginator->sort('channel-id');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('last_login');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th class="actions"><?php __('Actions');?></th>
		</tr>
		<?php
	$i = 0;
	foreach ($customers as $customer):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
		<tr<?php echo $class;?>>
			<td><?php echo $customer['Customer']['id']; ?></td>
			<td><?php echo $customer['Customer']['legal_name']; ?></td>
			<td><?php echo $customer['Customer']['channel-id']; ?></td>
			<td><?php echo $customer['Customer']['status']; ?></td>
			<td><?php echo $customer['Customer']['created']; ?></td>
			<td><?php echo $customer['Customer']['modified']; ?></td>
			<td><?php echo $customer['Customer']['last_login']; ?></td>
			<td><?php echo $customer['Customer']['modified_by']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('action' => 'view', $customer['Customer']['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $customer['Customer']['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $customer['Customer']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $customer['Customer']['id'])); ?>
			</td>
			</tr>
			<?php endforeach; ?>
	</table>
	<p>
		<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?> </p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
		| <?php echo $this->Paginator->numbers();?>
		|
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Customer', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bankingetails', true), array('controller' => 'bankingetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bankingetail', true), array('controller' => 'bankingetails', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Businessrelates', true), array('controller' => 'businessrelates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Businessrelate', true), array('controller' => 'businessrelates', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts', true), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact', true), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customersdocs', true), array('controller' => 'customersdocs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customersdoc', true), array('controller' => 'customersdocs', 'action' => 'add')); ?> </li>
	</ul>
</div>-->
