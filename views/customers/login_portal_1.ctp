<div class="Content login_page">
    <div class="container">
        <?php echo $this->Session->flash(); ?>
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav nav-pills mb-3 col-sm-12 col-xs-12 col-md-8 col-lg-5 login_tabs" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php if(!isset($this->params['form']['register'])){echo'active';}?>" id="pills-home-tab" data-toggle="pill" href="#login" role="tab" aria-controls="pills-home" aria-selected="true"><?=__('Sign in')?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if(isset($this->params['form']['register'])){echo'active';}?>" id="pills-profile-tab" data-toggle="pill" href="#register" role="tab" aria-controls="pills-profile" aria-selected="false"><?=__('Sign up')?></a>
                    </li>

                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade <?php if(!isset($this->params['form']['register'])){echo'show active';}?>" id="login" role="tabpanel" aria-labelledby="pills-home-tab">
                        <h1><?=__('Login to your dashboard')?></h1>
                        <div class="row">

                            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 form_content">
                                <form method="post">
                                    <div class="form-group col-sm-12 col-xs-12 col-md-5 col-lg-5">
                                        <input type="email" class="form-control" placeholder="<?=__('Email')?>" name="email">
                                    </div>
                                    <div class="form-group col-sm-12 col-xs-12 col-md-5 col-lg-5">
                                        <input type="password" class="form-control" placeholder="<?=__('Password')?>" name="password">
                                    </div>
                                    <div class="btn_login col-sm-12">
                                        <button type="submit" class="btn btn-primary" name="login">
                                            <span class="arrow arrow-left"></span>
                                            <span class="btn-text"><?=__('Sign in')?></span>
                                            <span class="arrow arrow-right"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade <?php if(isset($this->params['form']['register'])){echo'active show';}?>" id="register" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <h1><?=__('Sign up')?></h1>
                        <div class="row">

                            <div class="col-sm-12 col-xs-12 col-lg-6 form_content register__form">
                                <?php echo $this->Form->create('Customer', array('class' => '')); ?>
                                <div class="form-row">
                                    
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('First Name',true), 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Last Name',true), 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('company_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Company',true), 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('country_id', array('label' => false, 'div' => false,'empty'=>__('Country',true), 'class' => 'form-control', 'placeholder' => 'Last Name', 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('phon_number', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Phone Number',true), 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('corporate_email', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Corporate Email',true), 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('password', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Password',true),'value'=>'', 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                        echo $this->Form->input('confirm', array('type'=>'password','label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Confirm Password',true),'value'=>'', 'required' => 'required'));
                                        ?>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" required="required" name="capt" placeholder="<?=__('Type The Letters')?>" class="form-control">
                                        
                                    </div>
                                    <div class="col-sm-2 capacha">
                                        <p style="font-size: 25px;
                                        color: #2ba78a"> <?php
                                        $rand = rand(1000, 10000);
                                        echo ($rand * 2) + 255;
                                        ?>
                                            <input type="hidden" required="required" name="capt_copy"  value="<?= $rand ?>" >
                                        </p>
                                    </div>
                                    
                                    
                                    
                                    </div>
                                
                                <div class="btn_login col-sm-12">
                                    <button type="submit" class="btn btn-primary" name="register">
                                        <span class="arrow arrow-left"></span>
                                        <span class="btn-text"><?=__('Save')?></span>
                                        <span class="arrow arrow-right"></span>
                                    </button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>