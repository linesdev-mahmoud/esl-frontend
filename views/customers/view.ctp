<style>
	.btn-success {
		margin: 10px 15px;
	}

	h3 {
		color: #0c6db3;
	}

</style>

<div class="side-app">
	<!-- page-header -->
	<div class="page-header">
		<ol class="breadcrumb">
			<!-- breadcrumb -->
			<li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/customers/view"); ?>">Customers</a></li>
			<li class="breadcrumb-item active" aria-current="page">View</li>
		</ol><!-- End breadcrumb -->
	</div>
	<!-- End page-header -->


	<div class="customers view ">
		
		<div class="table-responsive mb-3 card">
			<div class="card-header  pb-0">
					<h2 class="card-title text-left  col-lg-10 col-md-10" style="margin-bottom:15px">Profile Details</h2>
				<a href="<?= $this->Html->url('/customers/edit') ?>" style="margin-bottom:15px"class="btn  hidden-sm-down btn-primary col-lg-2 col-md-2">Edit Profile</a>

				</div>
														<table class="table row table-borderless w-100 m-0 border">
															<tbody class="col-lg-6 p-0">
<tr>
																	<td><strong>First Name :</strong> <?php echo $customer['Customer']['last_name']; ?></td>
																</tr>
																	<tr>
																	<td><strong>Company Name :</strong> <?php echo $customer['Customer']['company_name']; ?></td>
																</tr>
																<tr>
																	<td><strong>Channel :</strong> <?php echo $channels[$customer['Customer']['channel-id']]; ?></td>
																</tr>
																<tr>
																	<td><strong>Status :</strong> <?php if( $customer['Customer']['status']==0){?>
									<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
									<?php }elseif($customer['Customer']['status']==1){?>
									<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
									<?php }elseif($customer['Customer']['status']==2){ echo "Your detail still can be update"; }
									 elseif($customer['Customer']['status']==3){ echo "Your detail under review"; }?>
																</tr>
																
															
																
															</tbody>
															<tbody class="col-lg-6 p-0">												
												
																<tr>
																	<td><strong>Last Name :</strong><?php echo $customer['Customer']['last_name']; ?></td>
																</tr>
																<tr>
																	<td><strong>Corporate Email :</strong> <?php echo $customer['Customer']['corporate_email']; ?></td>
																</tr>
																<tr>
																	<td><strong>Phone Number :</strong> <?php echo $customer['Customer']['phon_number']; ?></td>
																</tr>
																
																<tr>
																	<td><strong>Country :</strong> 	<?php echo $countries[$customer['Customer']['country_id']]; ?></td>
																</tr>
															</tbody>
														</table>
													</div>
		
		
		
		
		
		</div>
	<hr>
	<div class="related">


		<div class="table-responsive" >
			<h3><?php __('Related Addresses');?></h3>
			<div class="form-group text-right">
				<a href="<?= $this->Html->url('/addresses/add') ?>" class="btn  hidden-sm-down btn-success">Add New Address</a>

			</div>
			<div class="card">
				<div class="card-header pb-0">
					<div class="card-title">Address Relates</div>
				</div>
				<?php if (!empty($customer['Address'])):?>
				<div class="card-body">
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered text-nowrap w-100">
							<tr>
								<th><?php __('Phone Country Code'); ?></th>
								<th><?php __('Phone Area Code'); ?></th>
								<th><?php __('Phone Number'); ?></th>
								<th><?php __('Po Box'); ?></th>
								<th><?php __('State Mirates'); ?></th>
								<th><?php __('Status'); ?></th>
								<th class="actions"><?php __('Actions');?></th>
							</tr>
							<?php
		$i = 0;
		foreach ($customer['Address'] as $address):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
							<tr<?php echo $class;?>>
								<td><?php echo $address['phone_country_code'];?></td>
								<td><?php echo $address['phone_area_code'];?></td>
								<td><?php echo $address['phone_number'];?></td>
								<td><?php echo $address['po_box'];?></td>
								<td><?php echo $address['state_mirates'];?></td>
								<td>
									<?php if( $address['status']==0){?>
									<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
									<?php }else{?>
									<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
									<?php }?>
								</td>

								<td class="actions">
									<?= $this->Html->link(
								'<span><i class="si si-pencil"></i></span>',
								['controller' => 'addresses','action' => 'edit',  $address['id']],
								['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
							) ?>
									<?= $this->Html->link(
								'<span><i class="si si-trash"></i></span>',
								['controller' => 'addresses','action' => 'delete',  $address['id']],
								['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $address['id'])
							) ?>

								</td>
								</tr>
								<?php endforeach; ?>
						</table>
					</div>
				</div>
				<?php endif; ?>
			</div>

		</div>
		<hr>
		<div class="related">

			
			<div class="row">
				<div class="col-md-12 col-lg-12">
					<h3><?php __('Related Banking Details');?></h3>
					<div class="form-group text-right">
						<a href="<?= $this->Html->url('/bankingetails/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add New Banking details</a>

					</div>
					<div class="card">
					<?php if (!empty($customer['Bankingetail'])):?>
						<div class="card-header pb-0">
							<div class="card-title">Banking Details</div>
						</div>
						
						<div class="card-body">
							<div class="table-responsive">
								<table id="example" class="table table-striped table-bordered text-nowrap w-100">
									<thead>
										<tr>
											<th><?php __('IBAN No'); ?></th>
											<th><?php __('Account No'); ?></th>
											<th><?php __('Bank Name'); ?></th>
											<th><?php __('Country'); ?></th>
											<th><?php __('Status'); ?></th>
											<th class="actions"><?php __('Actions');?></th>
										</tr>


									</thead>
									<tbody>
										<?php
//									print_r($customer);die;
										$i = 0;
										foreach ($customer['Bankingetail'] as $bankingetail):
											$class = null;
											if ($i++ % 2 == 0) {
												$class = ' class="altrow"';
											}
										?>
										<tr<?php echo $class;?>>

											<td><?php echo $bankingetail['IBAN_No']; ?>&nbsp;</td>
											<td><?php echo $bankingetail['Account_No']; ?>&nbsp;</td>
											<td><?php echo $bankingetail['Bank_Name']; ?>&nbsp;</td>
											<td>
												<?php echo $countries[$bankingetail['country_id']]; ?>
											</td>
											<td>
												<?php if($bankingetail['status']==0){?>
												<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
												<?php }else{?>
												<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
												<?php }?>
											</td>
											<td>
												<div class="input-group">


													<?= $this->Html->link(
								'<span><i class="si si-pencil"></i></span>',
								['controller' => 'bankingetails','action' => 'edit',  $bankingetail['id']],
								['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
							) ?>
													<?= $this->Html->link(
								'<span><i class="si si-trash"></i></span>',
								['controller' => 'bankingetails','action' => 'delete',  $bankingetail['id']],
								['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $bankingetail['id'])
							) ?>
												</div>
											</td>
											</tr>
											<?php endforeach; ?>

									</tbody>
								</table>
							</div>
						</div>
							<?php endif; ?>
						<!-- table-wrapper -->
					</div>
					<!-- section-wrapper -->
				</div>
			</div>
		
		</div>
	</div>
	<hr>
	<div class="related">


		<div class="row">
			<div class="col-md-12 col-lg-12">
				<h3><?php __('Related Business Relates');?></h3>
				<div class="form-group text-right">
					<a href="<?= $this->Html->url('/businessrelates/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add New Business Related</a>

				</div>
				<div class="card">
					<div class="card-header pb-0">
						<div class="card-title">Business Relates</div>
					</div>
					<?php if (!empty($customer['Businessrelate'])):?>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered text-nowrap w-100">
								<thead>
									<tr>
										<th><?php __('Business Type'); ?></th>
										<th><?php __('Website'); ?></th>
										<th><?php __('Status'); ?></th>
										<th class="actions"><?php __('Actions');?></th>
									</tr>


								</thead>
								<tbody>

									<?php
		$i = 0;
		foreach ($customer['Businessrelate'] as $businessrelate):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
									<tr<?php echo $class;?>>
										<td><?php echo $businesstypes[$businessrelate['businesstype_id']];?></td>
										<td><?php echo $businessrelate['website'];?></td>
										<td>
											<?php if($businessrelate['status']==0){?>
											<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
											<?php }else{?>
											<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
											<?php }?>
										</td>
					
										<td>

											<div class="input-group">


												<?= $this->Html->link(
								'<span><i class="si si-pencil"></i></span>',
								['controller' => 'businessrelates','action' => 'edit',  $businessrelate['id']],
								['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
							) ?>
												<?= $this->Html->link(
								'<span><i class="si si-trash"></i></span>',
								['controller' => 'businessrelates','action' => 'delete',  $businessrelate['id']],
								['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $businessrelate['id'])
							) ?>
											</div>
										</td>
										</tr>
										<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
					<!-- table-wrapper -->
					<?php endif; ?>
				</div>
						
				<!-- section-wrapper -->
			</div>
		</div>
	</div>
	<hr>
	<div class="related">


		
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<h3><?php __('Related Contacts');?></h3>
				<div class="form-group text-right">
					<a href="<?= $this->Html->url('/contacts/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add New Related Contacts</a>

				</div>
				<?php if (!empty($customer['Contact'])):?>
				<div class="card">
					<div class="card-header pb-0">
						<div class="card-title">Contacts</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered text-nowrap w-100">
								<thead>
									<tr>
										<th><?php __('Title'); ?></th>
										<th><?php __('First Name'); ?></th>
										<th><?php __('Last Name'); ?></th>
										<th><?php __('Position'); ?></th>
										<th><?php __('Mobile Country Code'); ?></th>
										<th><?php __('Mobile Code No'); ?></th>
										<th><?php __('Mobile Number'); ?></th>
										<th><?php __('Email'); ?></th>
										<th><?php __('Contact Types'); ?></th>
										<th><?php __('Is Main'); ?></th>
										<th><?php __('Status'); ?></th>

										<th class="actions"><?php __('Actions');?></th>
									</tr>


								</thead>
								<tbody>
									<?php
//									print_r($customer);die;
		$i = 0;
		foreach ($customer['Contact'] as $contact):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
									<tr<?php echo $class;?>>
										<td><?php echo $contact['title'];?></td>
										<td><?php echo $contact['first_name'];?></td>
										<td><?php echo $contact['last_name'];?></td>
										<td><?php echo $contact['position'];?></td>
										<td><?php echo $contact['mobile_country_code'];?></td>
										<td><?php echo $contact['mobile_code_no'];?></td>
										<td><?php echo $contact['mobile_number'];?></td>
										<td><?php echo $contact['email'];?></td>
										<td><?php echo $contacttypes[$contact['contacttypes_id']];?></td>
										<td>
											<?php if($contact['is_main']==0){?>
											<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
											<?php }else{?>
											<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
											<?php }?>
										</td>
										<td>
											<?php if($contact['status']==0){?>
											<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
											<?php }else{?>
											<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
											<?php }?>
										</td>
										<td>
											<div class="input-group">

												<?= $this->Html->link(
					'<span><i class="si si-pencil"></i></span>',
					['controller' => 'contacts','action' => 'edit',  $contact['id']],
					['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
				) ?>
												<?= $this->Html->link(
					'<span><i class="si si-trash"></i></span>',
					['controller' => 'contacts','action' => 'delete',  $contact['id']],
					['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $contact['id'])
				) ?>
											</div>
										</td>
										</tr>
										<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
					<!-- table-wrapper -->
				</div>
				<?php endif; ?>
				<!-- section-wrapper -->
			</div>
		</div>
		
	</div>

	<hr>
	<div class="related">

		<h3><?php __('Customers documents and information');?></h3>

		
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="form-group text-right">
					<a href="<?= $this->Html->url('/customersdocs/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add New Customers documents and information</a>

				</div>
				<div class="card">
				<?php if (!empty($customer['Customersdoc'])):?>
					<div class="card-header pb-0">
						<div class="card-title">Customers documents and information</div>
					</div>
					
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered text-nowrap w-100">
								<thead>
									<tr>
										<th><?php __('Documenttype Id'); ?></th>
										<th><?php __('Issue Date'); ?></th>
										<th><?php __('Expiry Date'); ?></th>
										<th><?php __('Issued By'); ?></th>
										<th><?php __('Path'); ?></th>
										<th><?php __('Url'); ?></th>
										<th><?php __('Status'); ?></th>
										<th><?php __('Actions'); ?></th>
									</tr>


								</thead>
								<tbody>
									<?php
									
		$i = 0;
		foreach ($customer['Customersdoc'] as $customersdoc):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
									<tr<?php echo $class;?>>
										<td><?php echo $documenttypes[$customersdoc['documenttype_id']];?></td>
										<td><?php echo $customersdoc['issue_date'];?></td>
										<td><?php echo $customersdoc['expiry_date'];?></td>
										<td><?php echo $customersdoc['issued_by'];?></td>
										<td><?php echo $customersdoc['path'];?></td>
										<td><?php echo $customersdoc['url'];?></td>
										<td>
											<?php if($customersdoc['status']==0){?>
											<i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
											<?php }else{?>
											<i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
											<?php }?>
										</td>
										<td>
											<div class="input-group">

												<?= $this->Html->link(
					'<span><i class="si si-pencil"></i></span>',
					['controller' => 'customersdocs','action' => 'edit',  $customersdoc['id']],
					['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
				) ?>
												<?= $this->Html->link(
					'<span><i class="si si-trash"></i></span>',
					['controller' => 'customersdocs','action' => 'delete',  $customersdoc['id']],
					['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__('Do you want to delete %s?', true), $customersdoc['id'])
				) ?>
											</div>
										</td>
										</tr>
										<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
						<?php endif; ?>
					<!-- table-wrapper -->
				</div>
				<!-- section-wrapper -->
			</div>
		</div>
	
	</div>


</div>
