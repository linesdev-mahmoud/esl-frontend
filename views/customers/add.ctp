<div class="side-app">
	<!-- page-header -->
	<div class="page-header">
		<ol class="breadcrumb">
			<!-- breadcrumb -->
			<li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/customers"); ?>">Customer</a></li>
			<li class="breadcrumb-item active" aria-current="page">Add Customer</li>
		</ol><!-- End breadcrumb -->
	</div>
	<!-- End page-header -->

	<!-- row -->
	<div class="row">
		<div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
			<?php echo $this->Form->create('Customer', array('type'=>'file','class' => 'form-horizontal form-material'));?>
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Add Customer</h3>
				</div>
				<div class="card-body">

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Legal Name</label>
								<?php
										echo $this->Form->input('legal_name', array('label'=>false,'class' => 'form-control form-control-line','required'=>'required' ));
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">First Name</label>

								<?php
									echo $this->Form->input('first_name',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Last Name</label>

								<?php
									echo $this->Form->input('last_name',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Company Name</label>

								<?php
									echo $this->Form->input('company_name',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Country</label>

								<?php
									echo $this->Form->input('country_id',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Phon Number</label>

								<?php
									echo $this->Form->input('phon_number',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Corporate Email</label>

								<?php
									echo $this->Form->input('corporate_email',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Password</label>

								<?php
									echo $this->Form->input('password',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>

						<div class="form-group col-lg-12 col-md-12 col-sm-12">
							<label for="exampleInputnumber">channel</label>
							<?php
								echo $this->Form->input('channel-id', array('label'=>false,'class' => 'form-control form-control-line','required'=>'required' ,'options'=>$channels));
								
										?>

						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group">
								<label for="exampleInputname">Last Login</label>

								<?php
									echo $this->Form->input('last_login',array('label'=>false,'class' => ' form-control-line','required'=>'required'));
										
									?>
							</div>
						</div>


						<div class="form-group col-lg-12 col-md-12 col-sm-12">
							<label for="exampleInputnumber">Active</label>
							<label class="custom-switch form-group">
								<input type="checkbox" name="data[Customer][status]" class="custom-switch-input">
								<span class="custom-switch-indicator"></span>
							</label>

						</div>

						<div class="card-footer text-right col-lg-12 col-md-12">

							<input type="submit" class="btn btn-success mt-1" value="Save">
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- row end -->

	</div>

	<!--

<div class="customers form">
<?php echo $this->Form->create('Customer');?>
	<fieldset>
		<legend><?php __('Add Customer'); ?></legend>
	<?php
		echo $this->Form->input('legal_name');
		echo $this->Form->input('channel-id');
		echo $this->Form->input('status');
		echo $this->Form->input('last_login');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Customers', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Bankingetails', true), array('controller' => 'bankingetails', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Bankingetail', true), array('controller' => 'bankingetails', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Businessrelates', true), array('controller' => 'businessrelates', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Businessrelate', true), array('controller' => 'businessrelates', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Contacts', true), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contact', true), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customersdocs', true), array('controller' => 'customersdocs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customersdoc', true), array('controller' => 'customersdocs', 'action' => 'add')); ?> </li>
	</ul>
</div>-->
