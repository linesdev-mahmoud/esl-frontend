<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Emirates Safety Laboratory |  ESL</title>

	<link rel="canonical" href="http://emirateslaboratory.com/">
	<!-- This site is optimized with the Yoast SEO Premium plugin v12.4 - https://yoast.com/wordpress/plugins/seo/ -->
	<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="ESL" />
	<meta property="og:title" content="Contact - ESL |" />
	<meta property="og:url" content="http://emirateslaboratory.com/signup" />
	<meta property="og:site_name" content="emirateslaboratory" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="Contact - ESL " />
	<meta name="twitter:site" content="@_ESL" />
	<meta name="twitter:creator" content="@_ESL" />
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	<meta content="Divi Child v.1.0.0" name="generator" />
	<link rel='stylesheet' id='parent-style-css' href='<?php echo $this->Html->url("/"); ?>css2/Divi/style.css?ver=5.3.2' type='text/css' media='all' />
	<link rel='stylesheet' id='gs-beh-custom-bootstrap-css' href='<?php echo $this->Html->url("/"); ?>css2/gs-beh-custom-bootstrap.css?ver=1.0.2' type='text/css' media='all' />
	<link rel='stylesheet' id='gs_behance_custom_css-css' href='<?php echo $this->Html->url("/"); ?>css2/gs-beh-custom.css?ver=1.0.2' type='text/css' media='all' />
	<link rel='stylesheet' id='gsdrib-fa-icons-css' href='<?php echo $this->Html->url("/"); ?>assets/fontawesome-free-5.13.0/css/fontawesome.min.css' type='text/css' media='all' />
	<link rel='stylesheet' id='gs-dribb-custom-bootstrap-css' href='<?php echo $this->Html->url("/"); ?>css2/gs-dibb-custom-bootstrap.css?ver=1.1.2' type='text/css' media='all' />
	<link rel='stylesheet' id='gs_drib_csutom_css-css' href='<?php echo $this->Html->url("/"); ?>css2/gs-drib-custom.css?ver=1.1.2' type='text/css' media='all' />
	<link rel='stylesheet' id='wp-block-library-css' href='<?php echo $this->Html->url("/"); ?>css2/style.min.css?ver=5.3.2' type='text/css' media='all' />
	<link rel='stylesheet' id='wc-block-style-css' href='<?php echo $this->Html->url("/"); ?>css2/style4.css?ver=2.5.11' type='text/css' media='all' />
	<link rel='stylesheet' id='contact-form-7-css' href='<?php echo $this->Html->url("/"); ?>css2/styles.css?ver=5.1.6' type='text/css' media='all' />

	<link rel='stylesheet' id='wc-gateway-ppec-frontend-css' href='<?php echo $this->Html->url("/"); ?>css2/wc-gateway-ppec-frontend.css?ver=5.3.2' type='text/css' media='all' />
	<link rel='stylesheet' id='divi-style-css' href='<?php echo $this->Html->url("/"); ?>css2/Divchild/style.css?ver=4.2.2' type='text/css' media='all' />
	<link rel='stylesheet' id='anythingslider-styles-css' href='<?php echo $this->Html->url("/"); ?>css2/styles/style.min.css?ver=1.6' type='text/css' media='all' />
	<link rel='stylesheet' id='et-builder-googlefonts-cached-css' href='https://fonts.googleapis.com/css?family=Titillium+Web%3A200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C900&#038;ver=5.3.2#038;subset=latin,latin-ext' type='text/css' media='all' />
	<link rel='stylesheet' id='dashicons-css' href='<?php echo $this->Html->url("/"); ?>css2/dashicons.min.css?ver=5.3.2' type='text/css' media='all' />
	<script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>css2/jquery.js?ver=1.12.4-wp'></script>
	<script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>css2/jquery-migrate.min.js?ver=1.4.1'></script>
	<script type='text/javascript'>
		/* <![CDATA[ */
		var et_core_api_spam_recaptcha = { "site_key": "", "page_action": { "action": "contact" } };
		/* ]]> */
	</script>
	<script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>css2/recaptcha.js?ver=5.3.2'></script>

	<link rel="alternate" type="application/json+oembed" href="<?php echo $this->Html->url("/"); ?>css2/embed.json" />
	<meta name="referrer" content="always" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

	<style type="text/css" id="custom-background-css">
		body.custom-background {
			/* background-image: url("<?php echo $this->Html->url("/"); ?>css2/messaging-hero-light.svg"); */
			background-position: left top;
			background-size: auto;
			background-repeat: repeat;
			background-attachment: scroll;
			font-family: 'Noto Sans' !important;
		}

		.inputheight {
			height: 40px;
		}
	</style>
	<!-- Start FontAweseome -->
	<link rel="stylesheet" href="<?php echo $this->Html->url("/"); ?>css2/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<!-- End FontAweseome -->
	<!-- Google Tag Manager -->
	<!--
	<script>
	(function (w, d, s, l, i) {
	w[l] = w[l] || []; w[l].push({
	'gtm.start':
	new Date().getTime(), event: 'gtm.js'
	}); var f = d.getElementsByTagName(s)[0],
	j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
	'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
	})(window, document, 'script', 'dataLayer', 'GTM-M7BGLVH');

	</script>

	-->
	<!-- End Google Tag Manager -->
	<!-- Reveal Javascript -->
	<script type="text/javascript">
		jQuery(document).ready(function () {
			// Hide the div
			jQuery('#reveal').hide();
			jQuery('.rv_button').click(function (e) {
				e.preventDefault(); jQuery("#reveal").slideToggle();
				jQuery('.rv_button').toggleClass('opened closed');
			});
		});
	</script>
	<!-- End Reveal Javascript -->
	<link rel="icon" href="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-32x32.png" sizes="32x32" />
	<link rel="icon" href="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-192x192.png" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-180x180.png" />
	<meta name="msapplication-TileImage" content="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-270x270.png" />
	<link rel="stylesheet" id="et-core-unified-1133-cached-inline-styles" href="<?php echo $this->Html->url("/"); ?>css2/styles/et-core-unified-1133-158516164351.min4.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />


	<style media="all">
		@charset "UTF-8";
		@import url(https://fonts.googleapis.com/css?family=Varela+Round&display=swap);

		@font-face {
			font-family: 'Noto Sans';
			font-display: swap;
			font-weight: 700;
			font-style: normal;
			src: url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.woff2) format('woff2'),url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.woff) format('woff'),url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.ttf) format('truetype');
		}

		@font-face {
			font-family: 'Noto Sans';
			font-display: swap;
			font-weight: 400;
			font-style: normal;
			src: url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.woff2) format('woff2'),url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.woff) format('woff'),url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.ttf) format('truetype');
		}

		@font-face {
			font-weight: 600;
			font-style: normal;
			font-family: 'Noto Sans';
			font-display: swap;
			src: url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.woff2) format('woff2'),url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.woff) format('woff'),url(<?php echo $this->Html->url("/"); ?>assets/fonts/noto-sans-v9-latin-regular.ttf) format('truetype');
		}
	</style>
	
	<link rel='stylesheet' id='custome-style-css' href='<?php echo $this->Html->url("/"); ?>css2/custome.css' type='text/css' media='all' />

</head>

<body class="page-template-default page page-id-1133 custom-background theme-Divi woocommerce-no-js et_bloom et_button_no_icon et_pb_button_helper_class et_fullwidth_nav et_fixed_nav et_hide_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_svg_logo et_pb_footer_columns4 et_cover_background et_pb_gutter windows et_pb_gutters3 et_pb_pagebuilder_layout et_no_sidebar et_divi_theme et-db et_minified_js et_minified_css">
	<div id="page-container">
			<header id="main-header" data-height-onload="87">
			<div class="container clearfix et_menu_container">
				<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="">
						<img src="<?php echo $this->Html->url("/"); ?>assets/img/logo4bottom.png" alt="ESL" id="logo" data-height-percentage="44" />
					</a>
				</div>
				
			<div id="et-top-navigation" data-height="87" data-fixed-height="60">
			
			<div id="et_mobile_nav_menu">
				<div class="mobile_nav opened">
					<span class="select_page">Select Page</span>
					<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
					<ul id="mobile_menu" class="et_mobile_menu" style="display: block;">
						<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page 
						menu-item-32 et_first_mobile_item"><a href="index.php/Services">Home</a></li>
						<li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31">
						<a href="#">SignIn</a></li>
					</ul>
				</div>
			</div>



			</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			
		</header> <!-- #main-header -->



		<div id="et-main-area">
		<div id="main-content">
		<article id="post-1133" class="post-1133 page type-page status-publish hentry">
				<div class="entry-content">
					<div id="et-boc" class="et-boc">				
						<div class="et-l et-l--post">
							<div class="et_builder_inner_content et_pb_gutters3">
								<div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular">

									<div id="white-nav" class="et_pb_row et_pb_row_1 custom-nav et_pb_row_fullwidth et_pb_row_1-4_1-2_1-4">

																										
                                       <div style="float:right">

<a style="padding-top: 15px!important; padding-bottom: 15px!important;" class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light alltopbtn"
 href="">Home </a>  
  
<a style="padding-top: 15px!important; padding-bottom: 15px!important; " class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light "
 href="index.php/Services">Sign In</a>


	</div>


										<div class="et_pb_column et_pb_column_1_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_image et_pb_image_0">					
													<a href="/"><span class="et_pb_image_wrap ">
														<img src="<?php echo $this->Html->url("/"); ?>assets/img/logo4.png" alt="" title=""></span></a>
												</div>
											</div> <!-- .et_pb_column -->
	
											<div class="et_pb_column et_pb_column_1_2 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
												<div class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">												
													<div class="et_pb_text_inner"><div>
												</div>
											</div>
											</div> <!-- .et_pb_text -->



										</div> <!-- .et_pb_column -->
	

	
	</div> 
			



<div class="et_pb_row et_pb_row_1 et_pb_equal_columns">
				<div class="et_pb_column et_pb_column_1_1 et_pb_column_14  et_pb_css_mix_blend_mode_passthrough">
				<div class="et_pb_module et_pb_image et_pb_image_2 et-animated et_had_animation" style="">
				
				<span class="et_pb_image_wrap "><img src="<?php echo $this->Html->url("/"); ?>assets/img/Deployment.svg" alt="" title=""></span>
			</div>
			</div> <!-- .et_pb_column -->
			
<div class="et_pb_column et_pb_column_1_1 et_pb_column_15  et_pb_css_mix_blend_mode_passthrough et-last-child">
				
				
			
			<!-- .et_pb_text --><div class="et_pb_module et_pb_text et_pb_text_7  et_pb_text_align_left et_pb_bg_layout_light et_had_animation" style="">
				
				
			<div class="et_pb_text_inner"><h1>Sign Up</h1></div>

				
			</div> <!-- .et_pb_text -->
			<div class="et_pb_module et_pb_divider et_pb_divider_1 et_pb_divider_position_ et_pb_space et_had_animation" style="">
				<div class="et_pb_divider_internal">

				</div>
			</div>
			<div class="et_pb_module et_pb_text et_pb_text_8  et_pb_text_align_left et_pb_bg_layout_light et_had_animation" style="">
				
				
				<div class="et_pb_text_inner">

<div class="et_pb_column et_pb_column_1_1 et_pb_column_3  et_pb_css_mix_blend_mode_passthrough">

	<div id="contact-page-form" class="et_pb_module et_pb_contact_form_0 data-hj-whitelist et_pb_contact_form_container clearfix" data-form_unique_num="0" data-redirect_url="#">
		<div class="et-pb-contact-message"></div>

			<div class="et_pb_contact">

<?php echo $this->Form->create('Customer', array('type'=>'file','class' => 'form-horizontal form-material'));?>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">	
	<span class="spanheader">About You</span>
	<span class="spanpara">  We’ll need some basic information about your company </span>			
</p>


<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">
<label for="txtcompanynametrade" class="et_pb_contact_form_label">First Name</label>
<input type="text" id="txtcompanynametrade"  class="input inputheight" name="data[Customer][company_name]" data-required_mark="required" data-field_type="input" data-original_id="name" placeholder="First Name" required="">
</p>


<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">

<label for="txtagenttradelicenseno" class="et_pb_contact_form_label">Last Name</label>
<input type="text" id="txtagenttradelicenseno" class="input inputheight"  name="data[Customer][last_name]" data-required_mark="required" data-field_type="input" data-original_id="text" placeholder="Last Name" required="">

</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txttaxreg" data-type="input">
<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Corporate Email Address</label>
	<input type="email" id="txtregistrationno" class="input inputheight"  name="data[Customer][corporate_email]" data-required_mark="required" data-field_type="input" data-original_id="name" placeholder="Corporate Email Address" required="">
</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">

<label for="txtagenttradelicenseno" class="et_pb_contact_form_label">Company Name</label>
<input type="text" id="txtcompanyAddress" class="input inputheight"  name="data[Customer][company_name]" data-required_mark="required" data-field_type="input" data-original_id="txtcompanyAddress" placeholder="Company Name" required="">
</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtsitename1" data-type="input">
<!--
														<select id="manufecturesiteoption" width="400px!important" style="width:200px!important;font-size: 14px; font-family: 'Noto Sans'!important;  padding: 6px; border:0.5px solid gray!important " onchange="ReadSiteOpt()">
															<option value="1">Site 1 </option>
															<option value="2">Site 2 </option>
															<option value="3">Site 3 </option>
															<option value="4">Site 4 </option>
														</select>

													</p>
													
<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtsitename1" data-type="input">
-->
<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Country</label>
<?php echo $this->Form->input('country_id',array('label'=>false,"id"=>"manufecturesiteoption","style"=>"width:200px!important;font-size: 14px; font-family: 'Noto Sans'!important;  padding: 6px; border:0.5px solid gray!important ",'required'=>'required'));?>
</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">
<label for="txtAuthSigFullname" class="et_pb_contact_form_label">Phone Number</label>
<input type="text" id="txtAuthSigFullname" class="input inputheight" name="data[Customer][phon_number]" data-required_mark="required" data-field_type="text"  placeholder="Phone Number">
</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="email" data-type="input">
	<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Password</label>
	<input type="password" id="txtcoemail" class="input inputheight"  name="data[Customer][password] " data-required_mark="required" data-field_type="email" data-original_id="email" placeholder="Password" required="">
</p>


<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtWebsite" data-type="input">
<label for="txtWebsite" class="et_pb_contact_form_label">confirm Password</label>
<input type="password" id="txtWebsite" class="input inputheight"  
name="data[Customer][conf_password]" data-required_mark="required" data-field_type="text" data-original_id="txtWebsite" placeholder="confirm Password">

</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="email" data-type="input">
<label for="txtAuthSigFullname" class="et_pb_contact_form_label"></label>
<input type="text" style="visibility:hidden">
</p>


<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtmaincontact" data-type="input">
<span class="spanheader">Main Contact Person</span>
<span class="spanpara"> Next we’ll set up an account for you</span>

</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtcontactpMobile1" data-type="input">
</p>




<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">

	<label class="containerchk" style="padding-right:20px; margin-left:10px; ">MR
		<input type="checkbox" id="txtGenderMr" name="txtGenderMr" onclick="clicmr();" checked="checked">
		<span class="checkmarkchk"></span>
	</label>

	<label class="containerchk" >MS
		<input type="checkbox" id="txtGenderMs" name="txtGenderMs" onclick="clicmis();">
		<span class="checkmarkchk"></span>
	</label>

</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtFirstName" data-type="input">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtLastName" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Fist Name </label>
<input type="text" id="txtFirstName" class="input inputheight"  name="txtFirstName" data-required_mark="required" data-field_type="input" data-original_id="txtFirstName" placeholder="Fist Name">

</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtjobtitle" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Last Name</label>
<input type="text" id="txtLastName" class="input inputheight"  name="txtLastName" data-field_type="input" data-original_id="txtLastName" placeholder="Last Name">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtDepartment" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Emirates ID Number </label>
<input type="text" id="txtemiratesid" class="input inputheight"  name="txtemiratesid" data-required_mark="required" data-field_type="input" data-original_id="txtemiratesid" placeholder="Emirates ID Number ">

</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtlandnumber" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Job Title </label>
<input type="text" id="txtjobtitle" class="input inputheight"  name="txtjobtitle" data-field_type="input" data-original_id="txtjobtitle" placeholder="Job Title">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtMobileNo" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Department </label>
<input type="text" id="txtDepartment" class="input inputheight"  name="txtDepartment" data-required_mark="required" data-field_type="input" data-original_id="txtDepartment" placeholder="Department">

</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtemailaddress" data-type="email">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Landline Number </label>
<input type="text" id="txtlandnumber" class="input inputheight" name="txtlandnumber" data-required_mark="required" data-field_type="input" data-original_id="txtlandnumber" placeholder="Landline Number">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtMobileNo" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Mobile Number </label>
<input type="text" id="txtMobileNo" class="input inputheight"  name="txtMobileNo" data-required_mark="required" data-field_type="input" data-original_id="txtMobileNo" placeholder="Mobile Number">

</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtemiratesid" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Email Address </label>
<input type="email" id="txtemailaddress" class="input inputheight"  name="txtemailaddress" data-required_mark="required" data-field_type="email" data-original_id="txtemailaddress" placeholder="Email Address" required="">
</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtpassword" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label"> Password </label>
<input type="password" id="txtpassword" class="input inputheight"  name="txtpassword" data-required_mark="required" data-field_type="input" data-original_id="txtpassword" placeholder="Password" required="required">
</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtemiratesid" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label"> Re Password </label>
<input type="password" id="txtrepassword" class="input inputheight"  name="txtrepassword" data-required_mark="required" data-field_type="input" data-original_id="txtrepassword" placeholder="Re-enter Password" required="required">
</p>





<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtpassword" data-type="input">

	<span class="spanheader">You can add a contact person</span>	
	<span class="spanpara">Click on <strong style="color:green; font-size:22px">+</strong> button to add a contact person</span>
</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtemiratesid" data-type="input">
</p>




<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half"  data-id="email" data-type="email">
<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">+  </label>
<a id="A1"  onclick="showcontactdiv();" style="text-decoration: none; cursor: pointer; color: green;
float: left; font-size: 38px; border: 2px solid green; border-radius: 10px; padding: 3px 9px 6px 8px;"> + </a> 
</p>


<div id="contacthidediv" style="display:none">

	<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">
	</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtFirstName" data-type="input">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtLastName" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label"> Contact Person First Name  </label>
<input type="text" id="txtcontactpName1" class="input inputheight"  name="txtcontactpName1"  data-field_type="input" data-original_id="txtcontactpName1" placeholder="First Name">
</p>

<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtjobtitle" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Last Name</label>
<input type="text" id="txtcontactLastName1" class="input inputheight"  name="txtcontactLastName1" data-field_type="input" data-original_id="txtcontactLastName1" placeholder="Last Name">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtDepartment" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Emirates ID Number </label>
<input type="text" id="txtcontactemiratesid1" class="input inputheight"  name="txtcontactemiratesid1" data-required_mark="required" data-field_type="input" data-original_id="txttxtcontactemiratesid1" placeholder="Emirates ID Number ">

</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtcontactjobtitle1" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Job Title </label>
<input type="text" id="txtcontactjobtitle1" class="input inputheight"  name="txtcontactjobtitle1" data-field_type="input" data-original_id="txtcontactjobtitle1" placeholder="Job Title">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtMobileNo" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Department </label>
<input type="text" id="txtcontactDepartment1" class="input inputheight"  name="txtcontactDepartment1" data-required_mark="required" data-field_type="input" data-original_id="txtcontactDepartment1" placeholder="Department">

</p>


<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtemailaddress" data-type="email">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Landline Number </label>
<input type="text" id="txtcontactlandnumber1" class="input inputheight" name="txtcontactlandnumber1" data-required_mark="required" data-field_type="input" data-original_id="txtcontactlandnumber1s" placeholder="Landline Number">

</p>

<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtMobileNo" data-type="input">

<label for="et_pb_contact_name_0" class="et_pb_contact_form_label">Mobile Number </label>
<input type="text" id="txtcontactpMobile1" class="input inputheight"  name="txtcontactpMobile1" data-required_mark="required" data-field_type="input" data-original_id="txtcontactpMobile1" placeholder="Mobile Number">

</p>



<p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtcontactpEmail1" data-type="input">

<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Email Address </label>
<input type="email" id="txtcontactpEmail1" class="input inputheight"  name="txtcontactpEmail1" data-required_mark="required" data-field_type="email" data-original_id="txtcontactpEmail1" placeholder="Email Address">
</p>

</div>


<input type="hidden" value="et_contact_proccess" name="et_pb_contactform_submit_0">

	<div class="et_contact_bottom_container"></div>

		<p class="et_pb_contact_field et_pb_contact_field_1" data-id="email" data-type="email">
			<span style="color:#2ba78a!important; font-size:18px">	By singing up, you certify all information is true and correct and you agree to our Confidentiality Terms. </span> 
		</p>
	
		<p class="et_pb_contact_field et_pb_contact_field_1 et_pb_button_module_wrapper et_pb_button_1_wrapper  et_pb_module " data-id="email" data-type="email">
			<label for="et_pb_contact_email_0" class="et_pb_contact_form_label">Sign Up</label>
			<button style="padding-right: 30px!important;padding-left: 30px!important;" type="submit" name="et_builder_submit_button"
			class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light">Sign Up</button>
		</p>





</div> <!-- .et_pb_contact -->

</div> <!-- .et_pb_contact_form_container -->

</div>

					
</div> <!-- .et_pb_row -->


						
		</div>
			</div> <!-- .et_pb_text -->
			</div> <!-- .et_pb_column -->
				
				
			</div>


	</div><!-- .et-l -->
	
			
		</div><!-- #et-boc -->
	</div> <!-- .entry-content -->
			
	</div>

			
	</article> <!-- .et_pb_post -->			

	</div> <!-- #main-content -->

    </div> <!-- #et-main-area -->
</div> <!-- #page-container -->




	<script>
		function showcontactdiv() {
			var getdiplay = document.getElementById("contacthidediv").style.display;
			if (getdiplay == "none") {
				document.getElementById("contacthidediv").style.display = "block";
				document.getElementById("A1").innerHTML = "-";
			}

			if (getdiplay == "block") {
				document.getElementById("contacthidediv").style.display = "none";
				document.getElementById("A1").innerHTML = "+";
			}
		}




			function clicmr()
{

		var chkyesiss = document.getElementById("txtGenderMr").checked;
		if(chkyesiss==false)
		{
				document.getElementById("txtGenderMs").checked = true;
		}

		if(chkyesiss==true)
		{
				document.getElementById("txtGenderMs").checked = false;
		}

}


function clicmis()
{

		var chkyesiss = document.getElementById("txtGenderMs").checked;
		if(chkyesiss==false)
		{
				document.getElementById("txtGenderMr").checked = true;
		}

		if(chkyesiss==true)
		{
				document.getElementById("txtGenderMr").checked = false;
		}

}

	</script>




<script type="text/javascript">
var et_animation_data = [{ "class": "et_pb_text_4", "style": "fade", "repeat": "once", "duration": "1000ms", "delay": "0ms", "intensity": "50%", "starting_opacity": "0%", "speed_curve": "ease-in-out" }, { "class": "et_pb_divider_1", "style": "slideLeft", "repeat": "once", "duration": "1000ms", "delay": "0ms", "intensity": "50%", "starting_opacity": "0%", "speed_curve": "ease-in-out" }, { "class": "et_pb_text_5", "style": "fade", "repeat": "once", "duration": "1000ms", "delay": "0ms", "intensity": "50%", "starting_opacity": "0%", "speed_curve": "ease-in-out" }, { "class": "et_pb_social_media_follow_0", "style": "fade", "repeat": "once", "duration": "1000ms", "delay": "0ms", "intensity": "50%", "starting_opacity": "0%", "speed_curve": "ease-in-out" }, { "class": "et_pb_row_4", "style": "fade", "repeat": "once", "duration": "1000ms", "delay": "0ms", "intensity": "50%", "starting_opacity": "0%", "speed_curve": "ease-in-out" }, { "class": "et_pb_image_1", "style": "zoomBottom", "repeat": "once", "duration": "1000ms", "delay": "0ms", "intensity": "10%", "starting_opacity": "0%", "speed_curve": "ease-in-out" }];
</script>
<script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>assets/js/qppr_frontend_script.min.js?ver=5.1.9'></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/js/core.min.js"></script>
<script src="<?php echo $this->Html->url("/"); ?>assets/js/script.js"></script>


</body>
</html>

			
				

								
																<div class="container text-center text-dark">
					<div class="row">
						<div class="col-lg-12 d-block mx-auto">
							<div class="row">
								<div class="col-xl-12 col-md-12 col-md-12">
									<div class="card">
										<div class="card-body">
											<h3>Register</h3>
											<p class="text-muted">Create New Account</p>
											<?php echo $this->Form->create('Customer', array('type'=>'file','class' => 'form-horizontal form-material'));?>
											<div class="input-group mb-3">
												<input type="text" class="form-control" name="data[Customer][first_name]" placeholder="Enter First Name">
											</div>
											<div class="input-group mb-4">
												<input type="text" class="form-control" name="data[Customer][last_name]" placeholder="Enter Last Name">
											</div>
											<div class="input-group mb-4">
												<input type="email" class="form-control" name="data[Customer][corporate_email]"  placeholder="Enter Corporate Email">
											</div>
											<div class="input-group mb-3">
												<input type="text" class="form-control" name="data[Customer][company_name]" placeholder="Enter Company Name">
											</div>
											<div class="input-group mb-4">
													<?php
											echo $this->Form->input('country_id',array('label'=>false,'class' => 'form-control ','required'=>'required'));?>
											</div>
											<div class="input-group mb-3">
												<input type="text" class="form-control" name="data[Customer][phon_number]" placeholder="Enter Phone Number">
											</div>
											<div class="input-group mb-4">
												<input type="Password" class="form-control" name="data[Customer][password] "placeholder="Enter  Password">
											</div>
											<div class="input-group mb-4">
												<input type="password" class="form-control" name="data[Customer][conf_password]" placeholder=" Confirm Password">
											</div>
											
											<div class="mt-6 btn-list">
												<input type="submit" value="register" class="btn btn-success">
												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>