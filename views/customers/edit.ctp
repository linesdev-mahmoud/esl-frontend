<div class="">

    <div class="et_pb_row et_pb_row_2 et_pb_equal_columns">

        <div class="et_pb_column et_pb_column_1_3 et_pb_column_12  et_pb_css_mix_blend_mode_passthrough">

            <div class="et_pb_module et_pb_text et_pb_text_10  et_pb_text_align_left et_pb_bg_layout_light et_had_animation" style="">

                <div class="et_pb_text_inner">
                    <h1>
                        <?php
                        if ($this->Session->read('Customer.status') == "2" || $this->Session->read('Customer.status') == "3") {
                            echo "Complete Your";
                        } else {
                            echo "Edit";
                        }
                        ?> <span style="color:#2ba78a!important">Profile </span>
                    </h1>
                    <!--<p style="color:black;font-size:18px; display:block;margin-top:20px padding-bottom:10px">
For all organisations applying for ESL's Certificate of Conformity
</p>-->

                </div>

                <p style="color:black!important; font-size:16px"></p>
            </div>

            <!-- .et_pb_text -->

            <div class="et_pb_module et_pb_text et_pb_text_11  et_pb_text_align_left et_pb_bg_layout_light et_had_animation" style="">
                <div class="row">
                    <div class="container">
                        <div class="stepwizard ">

                            <div class="stepwizard-row setup-panel nav nav-bar">

                                <div class="stepwizard-step">

                                    <a href="#step-3"   class="btn btn-primary ">Profile Data</a>
                                    <!--									<a href="#step-3"  class="btn btn-primary  btn-circle stepicons"></a>-->
                                    <!--<p class="stepsmalltxt">Start</p>-->
                                </div>

                                <div class="stepwizard-step">
                                    <a href="#step-2"  class="btn btn-secondary "> Address</a>

                                    <!--									<a href="#step-2"  class="btn btn-default btn-circle stepicons"></a>-->
                                    <!--<p class="stepsmalltxt">Step 1</p>-->
                                </div>

                                <div class="stepwizard-step">
                                    <a href="#step-1"  class="btn btn-secondary "> Banking Details</a>
                                    <!--									<a href="#step-1"  class="btn btn-default btn-circle stepicons"></a>-->
                                    <!--<p class="stepsmalltxt">Step 2</p>-->
                                </div>

                                <div class="stepwizard-step">
                                    <a href="#step-4"  class="btn btn-secondary "> Business Relates</a>
                                    <!--<p class="stepsmalltxt">Step 3</p> -->
                                </div>

                                <div class="stepwizard-step">
                                    <a href="#step-5"  class="btn btn-secondary ">Contact</a>
                                    <!--<p class="stepsmalltxt">Step 4</p>-->
                                </div>
                                <div class="stepwizard-step">
                                    <a href="#step-6"  class="btn btn-secondary ">Customer Document</a>
                                    <!--<p class="stepsmalltxt">Step 4</p>-->
                                </div>

                            </div>
                        </div>


                        <div id="contact-page-form" class="et_pb_module et_pb_contact_form_0 data-hj-whitelist et_pb_contact_form_container clearfix" data-form_unique_num="0" data-redirect_url="#">

                            <div class="et_pb_contact">

                                <!--						address related block-->
                                <div class="row setup-content" id="step-2">

                                    <div class="table-responsive col-lg-12 col-xs-12">
                                        <h3 class="stepbigtxt"> </h3>

                                        <div class="form-group text-right">
                                            <a href="<?= $this->Html->url('/addresses/add') ?>" class="btn  hidden-sm-down btn-success">Add New Address</a>

                                        </div>
                                        <div class="card">
                                            <p class="headerp2"><?php __('Related Addresses'); ?></p>
                                            <?php if (!empty($customer['Address'])): ?>
                                                <div class="card-body">
                                                    <?php
                                                    $i = 0;
                                                    foreach ($customer['Address'] as $address):
                                                        $class = null;
                                                        if ($i++ % 2 == 0) {
                                                            $class = ' class="altrow"';
                                                        }
                                                        ?>

                                                        <div class="panel-group custom_acco" id="accordion1">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#accordion_<?= $address['id'] ?>"><?= $address['state_mirates'] ?><i class="fa fa-arrow-down"></i> 
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="accordion_<?= $address['id'] ?>" class="panel-collapse collapse ">
                                                                    <div class="panel-body">
                                                                        <div class="table-responsive">
                                                                            <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                                                                <tr>
                                                                                    <td><?php __('Phone Country Code'); ?></td>
                                                                                    <td><?php echo $address['phone_country_code']; ?></td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td><?php __('Phone Area Code'); ?></td>
                                                                                    <td><?php echo $address['phone_area_code']; ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><?php __('Phone Number'); ?></td>
                                                                                    <td><?php echo $address['phone_number']; ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><?php __('Po Box'); ?></td>
                                                                                    <td><?php echo $address['po_box']; ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><?php __('State Mirates'); ?></td>
                                                                                    <td><?php echo $address['state_mirates']; ?></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><?php __('Status'); ?></td>
                                                                                    <td>
                                                                                        <?php if ($address['status'] == 0) { ?>
                                                                                            No
                                                                                        <?php } else { ?>
                                                                                            Yes
                                                                                        <?php } ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="actions"><?php __('Actions'); ?></td>
                                                                                    <td class="actions"><?=
                                                                                        $this->Html->link(
                                                                                                'Edit',
                                                                                                ['controller' => 'addresses', 'action' => 'edit', $address['id']],
                                                                                                ['escape' => false, 'class' => 'btn btn-warning text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                                                        )
                                                                                        ?>
                                                                                        <?=
                                                                                        $this->Html->link(
                                                                                                'Delete',
                                                                                                ['controller' => 'addresses', 'action' => 'delete', $address['id']],
                                                                                                ['escape' => false, 'class' => 'btn btn-danger text-white btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Delete'], sprintf(__('Do you want to delete %s?', true), $address['id'])
                                                                                        )
                                                                                        ?>


                                                                                    </td>

                                                                                </tr>

                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>



                                                        </div>
                                                    <?php endforeach; ?>

                                                </div>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                </div>

                                <!--										customer detail blok-->
                                <div class="row setup-content" id="step-3">

                                    <div class="col-md-12">
                                        <?php echo $this->Form->create('Customer', array('type' => 'file', 'url' => '/' . $this->params['url']['url'], 'class' => 'form-horizontal form-material')); ?>
                                        <h3 class="stepbigtxt"> </h3>

                                        <p class="headerp2"> Customer Information</p>
                                        <div>
                                            <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtpassword" data-type="input">
                                                <?php
                                                echo $this->Form->input('first_name', array('label' => "First Name", 'div' => false, 'class' => 'input inputheight', 'required' => 'required'));
                                                ?>
                                            </p>


                                            <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtemiratesid" data-type="input">
                                                <?php echo $this->Form->input('last_name', array('label' => "Last Name", 'div' => false, 'class' => 'input inputheight', 'required' => 'required'));
                                                ?>
                                            </p>

                                        </div>
                                        <div>

                                            <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtsitename1" data-type="input">
                                                <label for="et_pb_contact_email_0" class="et_pb_contact_form_label">

                                                </label>
                                                <?php
                                                echo $this->Form->input('company_name', array('div' => false, 'label' => 'Company Name', 'class' => 'input inputheight', 'required' => 'required'));
                                                ?>
                                            </p>

                                            <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtsiteaddress1" data-type="input">

                                                <?php
                                                echo $this->Form->input('phon_number', array('div' => false, 'label' => 'Phon Number', 'class' => 'input inputheight', 'required' => 'required'));
                                                ?>

                                            </p>
                                        </div>




                                        <div>
                                            

                                            <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtsiteaddress2" data-type="input">
                                                
                                                <?php
                                                echo $this->Form->input('password1', array('div' => false, 'type' => 'password', 'label' => 'Password (Leave password empty if you don\'t change it.)', 'class' => 'input inputheight', 'value' => ""));
                                                echo $this->Form->input('password', array("type" => "hidden"));
                                                
                                                ?>
                                                
                                            
                                            </p>
                                            <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half  et_pb_contact_field_last" data-id="txtsiteaddress2" data-type="input">

                                                <?php
                                                echo $this->Form->input('conf_password', array('div' => false, 'type' => 'password', 'label' => 'Confirm Password', 'class' => 'input inputheight', 'value' => ""));
                                                ?>
                                            </p>
                                        </div>

                                        <div>
                                            <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="txtpassword" data-type="input">
                                                <label>
                                                    Country
                                                </label>

                                                <?php
                                                echo $this->Form->input('country_id', array('label' => false, 'div' => false, "style" => "width:100%!important;font-size: 14px; font-family: 'Noto Sans'!important;  padding: 6px; border:0.5px solid gray!important ", 'class' => 'input inputheight'));
                                                ?>
                                            </p>
                                            <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtMobileNo" data-type="input">

                                                <?php
                                                echo $this->Form->input('corporate_email', array('div' => false, 'label' => 'Corporate Email', 'class' => 'input inputheight', 'required' => 'required'));
                                                ?>
                                            </p>

                                        </div>



                                        <p style="width:45%!important;" class="b_p1 form-group et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half" data-id="name" data-type="input">
                                        </p>

                                        <p class="b_p2 et_pb_contact_field row et_pb_contact_field_2 et_pb_contact_field_half et_pb_contact_field_last" data-id="txtFirstName" data-type="input">

                                            <input type="submit" name="data[Customer][save&submit]" style="color:white!important; width:150px" class=" col-sm-6  et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light pull-right  btn-lg pull-right " value="Save & Review">
                                            <?php if ($customer['Customer']['status'] == 2 || $customer['Customer']['status'] == 3) { ?>
                                                <input type="submit" style="color:white!important; font-size:14px!important; width:120px;margin-right:10px!important; " class=" col-sm-6 et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light pull-right  btn-lg" value="Save">
                                            <?php } ?>


                                        </p>
                                        </form>
                                    </div>


                                </div>



                                <!--			banking related blok-->
                                <div class="row setup-content" id="step-1">
                                    <div class="col-lg-12">
                                        <h3 class="stepbigtxt"> </h3>


                                        <div class="form-group text-right">
                                            <a href="<?= $this->Html->url('/bankingetails/add') ?>" class="btn hidden-sm-down btn-success">Add New Banking details</a>

                                        </div>
                                        <div class="card">
                                            <p class="headerp2"><?php __('Related Banking Details'); ?></p>
                                            <div class="card-body">
                                                <?php
                                                //                                    print_r($customer);die;
                                                $i = 0;
                                                foreach ($customer['Bankingetail'] as $bankingetail):
                                                    $class = null;
                                                    if ($i++ % 2 == 0) {
                                                        $class = ' class="altrow"';
                                                    }
                                                    ?>

                                                    <div class="panel-group custom_acco" id="accordion1">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordion1" href="#accordion_b<?= $bankingetail['id'] ?>"><?= $bankingetail['Bank_Name'] ?><i class="fa fa-arrow-down"></i> 
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion_b<?= $bankingetail['id'] ?>" class="panel-collapse collapse ">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive">
                                                                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                                                            <tr>
                                                                                <td><?php __('IBAN No'); ?></td>
                                                                                <td><?php echo $bankingetail['IBAN_No']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><?php __('Account No'); ?></td>
                                                                                <td><?php echo $bankingetail['Account_No']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><?php __('Bank Name'); ?></td>
                                                                                <td><?php echo $bankingetail['Bank_Name']; ?></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td><?php __('Country'); ?></td>
                                                                                <td><?php echo $bankingetail['country_id']; ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><?php __('Status'); ?></td>
                                                                                <td>
                                                                                    <?php if ($bankingetail['status'] == 0) { ?>
                                                                                        No
                                                                                    <?php } else { ?>
                                                                                        Yes
                                                                                    <?php } ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="actions"><?php __('Actions'); ?></td>
                                                                                <td class="actions">
                                                                                    <?=
                                                                                    $this->Html->link(
                                                                                            'Edit',
                                                                                            ['controller' => 'bankingetails', 'action' => 'edit', $bankingetail['id']],
                                                                                            ['escape' => false, 'class' => 'btn btn-warning text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                                                    )
                                                                                    ?>
                                                                                    <?=
                                                                                    $this->Html->link(
                                                                                            'Delete',
                                                                                            ['controller' => 'bankingetails', 'action' => 'delete', $bankingetail['id']],
                                                                                            ['escape' => false, 'class' => 'btn btn-danger text-white btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Delete'], sprintf(__('Do you want to delete %s?', true), $bankingetail['id'])
                                                                                    )
                                                                                    ?>

                                                                                </td>


                                                                            </tr>

                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>
                                                <?php endforeach; ?>

                                            </div>


                                            <!-- table-wrapper -->
                                        </div>
                                        <!-- section-wrapper -->
                                    </div>
                                </div>


                                <!--	business related blok-->
                                <div class="row setup-content" id="step-4">

                                    <div class="col-md-12 col-lg-12">
                                        <h3 class="stepbigtxt"> </h3>


                                        <div class="form-group text-right">
                                            <a href="<?= $this->Html->url('/businessrelates/add') ?>" class="btn hidden-sm-down btn-success">Add New Business Related</a>

                                        </div>
                                        <div class="card">
                                            <p class="headerp2"><?php __('Related Business Relates'); ?></p>
                                            <div class="card-body">

                                                <?php
                                                $i = 0;
                                                foreach ($customer['Businessrelate'] as $businessrelate):
                                                    $class = null;
                                                    if ($i++ % 2 == 0) {
                                                        $class = ' class="altrow"';
                                                    }
                                                    ?>

                                                    <div class="panel-group custom_acco" id="accordion1">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion1" href="#accordion_bs<?= $businessrelate['id'] ?>" class="collapsed"><?= $businessrelate['website'] ?>
                                                                        <i class="fa fa-arrow-down"></i>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion_bs<?= $businessrelate['id'] ?>" class="panel-collapse collapse ">
                                                                <div class="panel-body">
                                                                    <div class="table-responsive">
                                                                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                                                            <tr>
                                                                                <td><?php __('Business Type'); ?></td>
                                                                                <td><?php echo $businesstypes[$businessrelate['businesstype_id']]; ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><?php __('Website'); ?></td>
                                                                                <td><?php echo $businessrelate['website']; ?></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td><?php __('Status'); ?></td>
                                                                                <td>
                                                                                    <?php if ($businessrelate['status'] == 0) { ?>
                                                                                        No
                                                                                    <?php } else { ?>
                                                                                        Yes
                                                                                    <?php } ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="actions"><?php __('Actions'); ?></td>
                                                                                <td class="actions">
                                                                                    <?=
                                                                                    $this->Html->link(
                                                                                            'Edit',
                                                                                            ['controller' => 'businessrelates', 'action' => 'edit', $businessrelate['id']],
                                                                                            ['escape' => false, 'class' => 'btn btn-warning text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                                                    )
                                                                                    ?>
                                                                                    <?=
                                                                                    $this->Html->link(
                                                                                            'Delete',
                                                                                            ['controller' => 'businessrelates', 'action' => 'delete', $businessrelate['id']],
                                                                                            ['escape' => false, 'class' => 'btn btn-danger text-white btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Delete'], sprintf(__('Do you want to delete %s?', true), $businessrelate['id'])
                                                                                    )
                                                                                    ?>

                                                                                </td>


                                                                            </tr>

                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>
                                                <?php endforeach; ?>

                                            </div>


                                            <!-- table-wrapper -->
                                            <?php //endif;   ?>
                                        </div>

                                        <!-- section-wrapper -->
                                    </div>
                                </div>

                                <!--										contact related blok-->
                                <div class="row setup-content" id="step-5">

                                    <div class="col-md-12 col-lg-12">
                                        <h3 class="stepbigtxt"> </h3>

                                        <div class="form-group text-right">
                                            <a href="<?= $this->Html->url('/contacts/add') ?>" class="btn hidden-sm-down btn-success">Add New Related Contacts</a>

                                        </div>
                                        <div class="card-body">
                                            <p class="headerp2"><?php __('Related Contacts'); ?></p>
                                            <?php
//                                    print_r($customer);die;
                                            $i = 0;
                                            foreach ($customer['Contact'] as $contact):
                                                $class = null;
                                                if ($i++ % 2 == 0) {
                                                    $class = ' class="altrow"';
                                                }
                                                ?>

                                                <div class="panel-group custom_acco" id="accordion1">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion1" class="collapsed" href="#accordion_c<?= $contact['id'] ?>"><?= $contact['title'] ?><i class="fa fa-arrow-down"></i> 
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="accordion_c<?= $contact['id'] ?>" class="panel-collapse collapse ">
                                                            <div class="panel-body">
                                                                <div class="table-responsive">
                                                                    <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                                                        <tr>
                                                                            <td><?php __('Title'); ?></td>
                                                                            <td><?php echo $contact['title']; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php __('First Name'); ?></td>
                                                                            <td><?php echo $contact['first_name']; ?></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td><?php __('Last Name'); ?></td>
                                                                            <td><?php echo $contact['last_name']; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php __('Position'); ?></td>
                                                                            <td><?php echo $contact['position']; ?></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td><?php __('Email'); ?></td>
                                                                            <td><?php echo $contact['email']; ?></td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td class="actions"><?php __('Actions'); ?></td>
                                                                            <td class="actions">
                                                                                <?=
                                                                                $this->Html->link(
                                                                                        'Edit',
                                                                                        ['controller' => 'contacts', 'action' => 'edit', $contact['id']],
                                                                                        ['escape' => false, 'class' => 'btn btn-warning text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                                                )
                                                                                ?>
                                                                                <?=
                                                                                $this->Html->link(
                                                                                        'Delete',
                                                                                        ['controller' => 'contacts', 'action' => 'delete', $contact['id']],
                                                                                        ['escape' => false, 'class' => 'btn btn-danger text-white btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Delete'], sprintf(__('Do you want to delete %s?', true), $contact['id'])
                                                                                )
                                                                                ?>

                                                                            </td>


                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            <?php endforeach; ?>

                                        </div>


                                        <!-- section-wrapper -->

                                        <!-- section-wrapper -->
                                    </div>
                                </div>

                                <!--										customer doc Related block-->
                                <div class="row setup-content" id="step-6">
                                    <div class="col-md-12 col-lg-12">
                                        <h3 class="stepbigtxt"> </h3>


                                        <div class="form-group text-right">
                                            <a href="<?= $this->Html->url('/customersdocs/add') ?>" class="btn hidden-sm-down btn-success">Add
                                                Documents and Information</a>

                                        </div>
                                        <div class="card-body">
                                            <p class="headerp2"><?php __('Customers documents and information'); ?></p>
                                            <?php
                                            $i = 0;
                                            foreach ($customer['Customersdoc'] as $customersdoc):
                                                $class = null;
                                                if ($i++ % 2 == 0) {
                                                    $class = ' class="altrow"';
                                                }
                                                ?>

                                                <div class="panel-group custom_acco" id="accordion1">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" class="collapsed"data-parent="#accordion1" href="#accordion_d<?= $customersdoc['id'] ?>"><?= $documenttypes[$customersdoc['documenttype_id']] ?><i class="fa fa-arrow-down"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="accordion_d<?= $customersdoc['id'] ?>" class="panel-collapse collapse ">
                                                            <div class="panel-body">
                                                                <div class="table-responsive">
                                                                    <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                                                                        <tr>
                                                                            <td><?php __('Documenttype Id'); ?></td>
                                                                            <td><?php echo $documenttypes[$customersdoc['documenttype_id']]; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php __('Issue Date'); ?></td>
                                                                            <td><?php echo $customersdoc['issue_date']; ?></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td><?php __('Expiry Date'); ?></td>
                                                                            <td><?php echo $customersdoc['expiry_date']; ?>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?php __('Issued By'); ?></td>
                                                                            <td><?php echo $customersdoc['issued_by']; ?></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td><?php __('Path'); ?></td>
                                                                            <td><?php echo $customersdoc['path']; ?></td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td class="actions"><?php __('Actions'); ?></td>
                                                                            <td class="actions">
                                                                                <a target="_blank" href="<?=$this->Html->url('/img/uploads/').$customersdoc['path']?>" class="btn btn-success text-white mr-2 btn-sm" data-toggle="tooltip" data-placement="top" data-original-title="View">View</a>
                                                                                <?=
                                                                                $this->Html->link(
                                                                                        'Edit',
                                                                                        ['controller' => 'customersdocs', 'action' => 'edit', $customersdoc['id']],
                                                                                        ['escape' => false, 'class' => 'btn btn-warning text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                                                )
                                                                                ?>
                                                                                <?=
                                                                                $this->Html->link(
                                                                                        'Delete',
                                                                                        ['controller' => 'customersdocs', 'action' => 'delete', $customersdoc['id']],
                                                                                        ['escape' => false, 'class' => 'btn btn-danger text-white btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Delete'], sprintf(__('Do you want to delete %s?', true), $customersdoc['id'])
                                                                                )
                                                                                ?>

                                                                            </td>


                                                                        </tr>

                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                </div>
                                            <?php endforeach; ?>

                                        </div>


                                        <!-- section-wrapper -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div> <!-- .et_pb_contact -->
            </div> <!-- .et_pb_text -->
        </div> <!-- .et_pb_column -->


        <!--
<div class="et_pb_column et_pb_column_1_1 et_pb_column_9  et_pb_css_mix_blend_mode_passthrough et-last-child">

<div class="et_pb_module et_pb_image et_pb_image_2 et-animated et_had_animation" style="">
<span class="et_pb_image_wrap ">
<img src="<?php //echo $this->Html->url("/");   ?>assets/img/applicationback.png" alt="" title=""
id="imgbanner">
</span>
</div>

</div>  
.et_pb_column 
        -->
    </div> <!-- .et_pb_row -->
</div>

</div><!-- .et-l -->


</div><!-- #et-boc -->
</div> <!-- .entry-content -->

</div>

</div>
</article> <!-- .et_pb_post -->

</div> <!-- #main-content -->

</div> <!-- #et-main-area -->

</div> <!-- #page-container -->





<!--

<div class="customers form">
<?php echo $this->Form->create('Customer'); ?>
<fieldset>
<legend><?php __('Edit Customer'); ?></legend>
<?php
echo $this->Form->input('id');
echo $this->Form->input('legal_name');
echo $this->Form->input('channel-id');
echo $this->Form->input('status');
echo $this->Form->input('last_login');
echo $this->Form->input('modified_by');
?>
</fieldset>
<?php echo $this->Form->end(__('Submit', true)); ?>
</div>
<div class="actions">
<h6><?php __('Actions'); ?></h6>
<ul>

<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Customer.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Customer.id'))); ?></li>
<li><?php echo $this->Html->link(__('List Customers', true), array('action' => 'index')); ?></li>
<li><?php echo $this->Html->link(__('List Addresses', true), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Address', true), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
<li><?php echo $this->Html->link(__('List Bankingetails', true), array('controller' => 'bankingetails', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Bankingetail', true), array('controller' => 'bankingetails', 'action' => 'add')); ?> </li>
<li><?php echo $this->Html->link(__('List Businessrelates', true), array('controller' => 'businessrelates', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Businessrelate', true), array('controller' => 'businessrelates', 'action' => 'add')); ?> </li>
<li><?php echo $this->Html->link(__('List Contacts', true), array('controller' => 'contacts', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Contact', true), array('controller' => 'contacts', 'action' => 'add')); ?> </li>
<li><?php echo $this->Html->link(__('List Customersdocs', true), array('controller' => 'customersdocs', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Customersdoc', true), array('controller' => 'customersdocs', 'action' => 'add')); ?> </li>
</ul>
</div>-->
