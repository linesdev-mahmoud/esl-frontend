<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Welcome</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  
      <link rel="stylesheet" href="css/style.css">

  
  
</head>
<style>
    #global-loader {
    position: fixed;
    z-index: 50000;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    background: #fff;
    margin: 0 auto;
    text-align: center;
}

#global-loader img {
    -webkit-animation: rotation2 2s infinite linear;
}
#global-loader img {
    position: absolute;
    left: 0;
    right: 0;
    text-align: center;
    top: 40%;
    margin: 0 auto;
}
#global-loader div {
        position: absolute;
    left: 0;
    right: 0;
    text-align: center;
    top: 48%;
    margin: 0 auto;
    font-size: 25px;
    margin-top: 30px;
}
@-webkit-keyframes rotation2 {
    
		from {
				-webkit-transform: scale(0.9);
		}
		to {
				-webkit-transform: scale(1.4);
		}
}
</style>
<body>
  

<div id="global-loader">
            <img src="<?php echo $this->Html->url("/"); ?>assets/img/fave.png" alt="loader">
            <div>Check Your Email</div>
        </div>

</body>
</html>
