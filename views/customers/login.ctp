<!doctype html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="Sparic– Creative Admin Multipurpose Responsive Bootstrap4 Dashboard HTML Template" name="description">
        <meta content="Spruko Technologies Private Limited" name="author">
        <meta name="keywords" content="html admin template, bootstrap admin template premium, premium responsive admin template, admin dashboard template bootstrap, bootstrap simple admin template premium, web admin template, bootstrap admin template, premium admin template html5, best bootstrap admin template, premium admin panel template, admin template"/>

    
    </head>
    <body class="bg-account">
        <!-- page -->
        <div class="page">

            <!-- page-content -->
            <div class="page-content">
                <div class="container text-center text-dark">
                    <div class="row">
                        <div class="col-lg-4 d-block mx-auto">
                            <div class="row">
                                <div class="col-xl-12 col-md-12 col-md-12">
                                    <?php echo $this->Form->create('Customer');?>

                                    <div class="card">
                                        <div class="card-body">
                                            <h3>Login</h3>
                                            <p class="text-muted">Sign In to your account</p>
                                            <div class="input-group mb-3">
                                                <span class="input-group-addon bg-white"><i class="fa fa-user"></i></span>
                                                <input type="text" class="form-control" placeholder="Corporate Email" name="data[Customer][corporate_email]">
                                            </div>
                                            <div class="input-group mb-4">
                                                <span class="input-group-addon bg-white"><i class="fa fa-unlock-alt"></i></span>
                                                <input type="password" class="form-control" placeholder="Password" name="data[Customer][password]">
                                            </div>
                                            <div class="row">

                                                <div class="col-12">
                                                              <?php echo $this->Session->flash(); ?>
                                                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                                                </div>
                                                <div class="col-12">
                                                    <a href="forgot-password.html" class="btn btn-link box-shadow-0 px-0">Forgot password?</a>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- page-content end -->
        </div>

   
    </body>
</html>