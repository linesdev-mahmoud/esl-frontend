<!--<link rel="stylesheet" type="text/css" href="<?= $this->Html->url('/') ?>new/normalize.css" />-->
<!--		<link rel="stylesheet" type="text/css" href="<?= $this->Html->url('/') ?>new/demo.css" />-->
<link rel="stylesheet" type="text/css" href="<?= $this->Html->url('/') ?>new/component.css" />
<div class="container review_certification" style="margin-top:30px">


    <div class="row">
        <div class="col-sm-7">
            <div class="title_review">Application for review of</div>
            <div class="title_review_color title_review">international conformity certificate</div>
            <div class="after_title">For all organization applying DCD product licensing</div>
            <div class="stepwizard ">

                <div class="stepwizard-row setup-panel nav nav-bar">

                    <div class="stepwizard-step no_margin">
                        <a href="#step-1"   class="btn btn-primary ">1</a>
                        <p class="stepsmalltxt">Step 1</p>
                    </div>

                    <div class="stepwizard-step">
                        <a href="#step-2"  class="btn btn-secondary ">2</a>

                        <p class="stepsmalltxt">Step 2</p>
                    </div>

                    <div class="stepwizard-step">
                        <a href="#step-3"  class="btn btn-secondary ">3</a>
                        <p class="stepsmalltxt">Step 3</p>
                    </div>

                    <div class="stepwizard-step no_after">
                        <a href="#step-4"  class="btn btn-secondary ">4</a>
                        <p class="stepsmalltxt">Step 4</p>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-sm-5">
            <img src="<?= $this->Html->url('/') ?>applicationback.png">
        </div>
        <div class="row">
            <div class="col-sm-9">
                <div class="steps_container container">
                    <div class="row setup-content" id="step-1">
                        <div class="col-sm-6 no_padding_col">
                            <div class="title_review_color title_review step_title">Information Regarding the Application</div>
                            <div class="step_txt">
                                Note: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                            </div>
                            <div class="start_btn">
                                <button style="padding-top: 7px!important;color:white!important; font-size:14px!important; font-weight: 600!important;
                                        padding-right: 30px!important;padding-left: 30px!important;
                                        border-radius: 33px!important; margin-right:10px" class="nextBtn et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light" type="button">
                                    Let’s Go
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="row setup-content" id="step-2">
                        <div class="col-sm-10 no_padding_col">
                        <form>
                            <div class="row input_row">
                                <label class="green">Name and type of product</label>
                                <div class="col-sm-8 no_padding_col">
                                    <input type="text" class="form-control" placeholder="Enter name and type of product(e.g. Fire extinguisher,type Alfa1)">
                                </div>

                            </div>
                            <div class="row input_row">
                                <label class="green">Product manufacturer</label>
                                <div class="col-sm-6 no_padding_col">

                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="col-sm-6 no_padding_col">
                                    <input type="text" class="form-control" placeholder="Address">
                                </div>
                            </div>
                            <div class="row input_row">
                                <label class="green">Product manufacturer</label>
                                <div class="col-sm-6 no_padding_col">

                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="col-sm-6 no_padding_col">
                                    <input type="text" class="form-control" placeholder="Address">
                                </div>
                                <div class="col-sm-6 no_padding_col">
                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="col-sm-6 no_padding_col">
                                    <input type="text" class="form-control" placeholder="Address">
                                </div>
                                <div class="col-sm-12 no_padding_col">
                                    <select>
                                        <option>Issued by</option>
                                        <option>Name1</option>
                                        <option>Name2</option>
                                    </select>
                                </div>

                            </div>
                            <div class="row input_row contain_lang">
                                <label class="green site_label">Product manufacturing site(s) (as indicated in the product certificate)</label>
                                <div class="col-sm-5 no_padding_col">
                                    <label class="default">Site 1</label>
                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="col-sm-5 no_padding_col">
                                    <label class="default"></label>
                                    <input type="text" class="form-control" placeholder="Address">
                                </div>
                                <div class="col-sm-12 no_padding_col">
                                    <div class="row input_row_inner">
                                        <div class="col-sm-5 no_padding_col">
                                            <label class="default">Site 2</label>
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="col-sm-5 no_padding_col">
                                            <label class="default"></label>
                                            <input type="text" class="form-control" placeholder="Address">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 no_padding_col">
                                    <div class="row input_row_inner">
                                        <div class="col-sm-5 no_padding_col">
                                            <label class="default">Site 3</label>
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="col-sm-5 no_padding_col">
                                            <label class="default"></label>
                                            <input type="text" class="form-control" placeholder="Address">
                                        </div>
                                        <div class="col-sm-2 addmore_btn no_padding_col">
                                            <a class="add_more">+</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn_contain">
                                <button type="submit" style=" font-size:14px!important; font-weight: 600!important;padding-right: 20px!important;padding-left: 20px!important;padding-top: 14px!important; padding-bottom: 14px!important;border-radius: 33px!important; margin-right:10px" class="et_pb_button et_pb_button_0 et_pb_bg_layout_light save_btn">Save & Exit</button>
                                <button type="button" style=" font-size:14px!important; font-weight: 600!important; padding-right: 20px!important;padding-left: 20px!important; padding-top: 14px!important; padding-bottom: 14px!important; border-radius: 33px!important; " class="nextBtn et_pb_button et_pb_button_0 et_pb_bg_layout_light save_btn">Continue</button>
                            </div>
                        </form>
                    </div>
                    </div>

                    <div class="row setup-content" id="step-3">
                        <div class="title_review_color title_review step_title" style="text-transform: uppercase;">Mandatory attachments</div>
                        <div class="step_txt">
                            Application attachments
                        </div>
                        <form>
                            <div class="row upload_contain">

                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-2" id="file-2" class="inputfile inputfile-1" />
                                        <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                                <div class="contain_input last_contain">
                                    <div class="col-sm-7 no_padding_col">
                                        <label class="upload_txt" for="upload-1">Product description(product brochure,data sheet,catalogue) and photographs enabling visual identification.</label>
                                    </div>
                                    <div class="col-sm-5 upload_col no_padding_col">
                                        <input type="file" name="file-1" id="file-1" class="inputfile inputfile-1" />
                                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>
                                    </div>
                                </div>
                            </div>
                            <div class="title_review_color title_review step_title2">Note:</div>
                            <div class="step_txt col-sm-10 no_padding_col" style="font-size: 16px;padding-left:0;">
                                Please ensure if you have attached all the required documentation. ESL cannot proceed with your application until all of these items are present. 
                            </div>
                            <div class="btn_contain col-sm-12 no_padding_col">
                                <button type="submit" style=" font-size:14px!important; font-weight: 600!important;padding-right: 20px!important;padding-left: 20px!important;padding-top: 14px!important; padding-bottom: 14px!important;border-radius: 33px!important; margin-right:10px" class="et_pb_button et_pb_button_0 et_pb_bg_layout_light save_btn">Save & Exit</button>
                                <button type="button" style=" font-size:14px!important; font-weight: 600!important; padding-right: 20px!important;padding-left: 20px!important; padding-top: 14px!important; padding-bottom: 14px!important; border-radius: 33px!important; " class="nextBtn et_pb_button et_pb_button_0 et_pb_bg_layout_light save_btn">Continue</button>
                            </div>
                        </form>
                    </div>

                    <div class="row setup-content" id="step-4">
                        <h5 class="main_title">SPECIFIC TERMS OF SERVICE</h5>
                        <div class="col-sm-9 no_padding_col">
                            <!--                                    <div class="title_block title_review_color"></div>-->
                            <div class="txt_block">The Applicant is fully aware of the process of review that will be carried out according to the provisions of the current edition of the applicable scheme, <b class="green_color">“Scheme on review of conformity certificates issued by international certification bodies”</b>, as from time to time amended when required by sole discretion of ESL.</div>
                            <div class="txt_block with_margin">The process of review is initiated based on a correctly filled in application together with a set of obligatory attachments. The Applicant hereby confirms that the information provided in the application and attachments is valid true and correct.</div>
                            <div class="title_block title_review_color last_title">The Applicant shall</div>
                            <div class="txt_block">a) fulfil all requirements resulting from applying for the review, defined in legal regulations in force as well as certificate review requirements defined by ESL.</div>
                            <div class="txt_block" style="margin-top: 20px;">b) make all necessary arrangements for the conduct of the certificate review, including provision for examining documentation and records.</div>
                            <div class="title_block title_review_color last_title">The Applicant declares that</div>
                            <div class="txt_block">a) it understands that ESL has the responsibility to obtain sufficient objective evidence upon which the review could be made. Based on the review of the evidence, in case of sufficient evidence positive review result is made, and in case of not sufficient evidence negative review result is made.</div>
                            <div class="txt_block" style="margin-top: 20px;">b)	it will fulfil the requirements set out by ESL within the scope of the  review and will submit to ESL all information necessary to carry out the review.</div>
                            <div class="txt_block with_margin">The Applicant acknowledges the fact, that the review will be carried out once sufficient and correct product documentation, satisfying review requirements, is submitted to ESL by the Applicant.</div>
                            <form class="agree_form">
                                <label class="container_label">I declare that I am authorized, on behalf of the Applicant, to submit this application.
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>

                                <label class="container_label">I confirm that the information provided in this application is both correct and accurate to the best of my knowledge and belief. 
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>

                                <label class="container_label">I acknowledge that I have read and understood and confirm to comply with the applicable<br><b class="green_color"> “Scheme on review of conformity certificates”</b>
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>

                                <div class="btn_contain col-sm-12 no_padding_col">
                                    <button type="submit" style=" font-size:14px!important; font-weight: 600!important;padding-right: 20px!important;padding-left: 20px!important;padding-top: 14px!important; padding-bottom: 14px!important;border-radius: 33px!important; margin-right:10px" class="et_pb_button et_pb_button_0 et_pb_bg_layout_light save_btn">Save & Exit</button>
                                    <button type="button" style=" font-size:14px!important; font-weight: 600!important; padding-right: 20px!important;padding-left: 20px!important; padding-top: 14px!important; padding-bottom: 14px!important; border-radius: 33px!important; " class="nextBtn et_pb_button et_pb_button_0 et_pb_bg_layout_light save_btn">Continue</button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
                <!--<script src="<?= $this->Html->url('/') ?>new/custom-file-input.js"></script>-->

<style>
    .container_label {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 30px;
        cursor: pointer;
        font-size: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .container_label input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }
    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #fff;
        border: 1px solid;
    }

    /* On mouse-over, add a grey background color */
    .container_label:hover input ~ .checkmark {
        background-color: #fff;
    }

    /* When the checkbox is checked, add a blue background */
    .container_label input:checked ~ .checkmark {
        background-color: #2ba78a;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //alert('aa');
        //languages js
        var maxField = 3; //Input fields increment limitation
        var addButton = $('.add_more'); //Add button selector
        var wrapper = $('.contain_lang'); //Input field wrapper
        //var fieldHTML = '<span class="row rmove-'+l+'" ><div class="col-sm-4 lang-col"><input type="text" name="data[Cv][languages][lang][]" class="form-control" placeholder="Language"/></div><div class="col-sm-4 lang-col"><input type="text" name="data[Cv][languages][proffession][]" class="form-control" placeholder="Proficiency"/></div><div class="col-sm-4 empty-col"><a href="#" class="remove_button">x</a></div></span>'; //New input field html
        var l = 1; //Initial field counter is 1
        
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(l < maxField){ 
                l++; //Increment field counter
                $(wrapper).append('<span data-id="'+l+'" class="row rmove-'+l+'" ><div class="col-sm-12 col-xs-12 lang-col no_padding_col"><div class="row input_row_inner"><div class="col-sm-5 no_padding_col"><label class="default">Site 3</label><input type="text" class="form-control" placeholder="Name"></div><div class="col-sm-5 no_padding_col"><label class="default"></label><input type="text" class="form-control" placeholder="Address"></div><div class="col-sm-2 empty-col no_padding_col"><a href="#" data-id="'+l+'" class="remove_button">x</a></div></div></div></span>'); //Add field html
            }
        });
        
        
                //Once remove button is clicked
                $(wrapper).on('click', '.remove_button', function(e){
                    
                    var input_rm = $(this).attr('data-id');
                    //alert(input_rm);
                    e.preventDefault();
                    $('.rmove-'+input_rm).remove(); //Remove field html
                    l--; //Decrement field counter
                });

        

        

        

        


    });
</script>