<style>
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
<?php 
$style = '';
if(isset($this->params['form']['register'])){
    $style1 = 'display:none!important;';
    $style2 = 'display:block!important;';
    }else{
        $style1 = 'display:block!important;';
    $style2 = 'display:none!important;';
     }?>
<div class="Content login_page new_login_page">
    <div class="container">
        <?php echo $this->Session->flash(); ?>
        <div class="row">
            <div class="col-sm-6 img_content">
                <img src="<?= $this->Html->url('/') ?>assets/img/login.png">
            </div>
            <div class="col-sm-6 form_content login_content" style="<?=$style1?>">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6">
                        <h1><?= __('Sign in') ?></h1>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 show_hide_content">
                        <h3><?= __('or') ?><a class="show_register_btn show_btn" href="#"><?= __(' create an account') ?></a></h3>
                    </div>
                </div>
                <form method="post">
                    <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-12">
                        <input type="email" class="form-control" placeholder="<?= __('Email') ?>" name="email">
                    </div>
                    <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-12">
                        <input type="password" class="form-control" placeholder="<?= __('Password') ?>" name="password">
                    </div>
                    <div class="btn_login col-sm-12 col-xs-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6 checkbox_text">
                                <!--<div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                        <?= __('Remember me') ?>
                                    </label>
                                </div>-->
                            </div>
                            
                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 checkbox_text">
                            <div class="show_hide_content">
                                <h3><?=
                                    $this->Html->link(
                                            '<span>'.__('Forgot your password?',true).'</span>',
                                            ['action' => 'forgotpassword'],
                                            ['escape' => false, 'class' => 'show_btn']
                                    )
                                    ?>
                                </h3> </div>
                        </div>
                            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 login_text">
                                <button type="submit" class="btn btn-primary" name="login">
                                    <span class="arrow arrow-left"></span>
                                    <span class="btn-text"><?= __('Sign in') ?></span>
                                    <span class="arrow arrow-right"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-6 form_content register_content register__form" style="<?=$style2?>">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6">
                        <h1><?= __('Create an account') ?></h1>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 show_hide_content">
                        <h3><?= __('or') ?><a class="show_login_btn show_btn" href="#"><?= __(' sign in') ?></a></h3>
                    </div>
                </div>
                <?php echo $this->Form->create('Customer', array('class' => '')); ?>
                <div class="form-row">

                    <div class="col-sm-6">
                        <?php
                        echo $this->Form->input('first_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('First Name', true), 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        echo $this->Form->input('last_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Last Name', true), 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        echo $this->Form->input('company_name', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Company', true), 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        echo $this->Form->input('country_id', array('label' => false, 'div' => false, 'empty' => __('Country', true), 'class' => 'form-control', 'placeholder' => 'Last Name', 'required' => 'required','onchange'=>'selectcode()'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <label style="float: left;padding-top: 0px;border-right: unset" id="codeid" class="col-4 form-control"></label>
                        <input style="float: left;border-left: unset;padding-left: 0px" id="CustomerPhonNumber" name="data[Customer][phon_number]" type="number" value="<?= @$this->data['Customer']['phon_number']?>"  class= 'form-control col-8'  placeholder = "<?=__('Phone Number', true)?>" required = 'required'>
                        <?php
//                        echo $this->Form->input('phon_number', array('label' => false, 'div' => false, 'class' => 'form-control','maxlength' => '10','minlength' => '10', 'placeholder' => __('Phone Number', true), 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <input id="CustomerCorporateEmail" name="data[Customer][corporate_email]" type="email" value="<?= @$this->data['Customer']['corporate_email']?>"  class= 'form-control'  placeholder = "<?=__('Corporate Email', true)?>" required = 'required'>
                        <?php
                        //echo $this->Form->input('corporate_email', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => __('Corporate Email', true), 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        echo $this->Form->input('password', array('label' => false, 'div' => false,'minlength' => '8', 'class' => 'form-control', 'placeholder' => __('Password', true), 'value' => '', 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        echo $this->Form->input('confirm', array('type' => 'password', 'label' => false,'minlength' => '8', 'div' => false, 'class' => 'form-control', 'placeholder' => __('Confirm Password', true), 'value' => '', 'required' => 'required'));
                        ?>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" required="required" name="capt" placeholder="<?= __('Type The Numbers') ?>" class="form-control">

                    </div>
                    <div class="col-sm-2 capacha">
                        <p style="font-size: 25px;
                           color: #2ba78a"> <?php
                           $rand = rand(1000, 10000);
                           echo ($rand * 2) + 255;
                           ?>
                            <input type="hidden" required="required" name="capt_copy"  value="<?= $rand ?>" >
                        </p>
                    </div>



                </div>

                <div class="btn_login col-sm-12">
                    <button type="submit" class="btn btn-primary" name="register">
                        <span class="arrow arrow-left"></span>
                        <span class="btn-text"><?= __('Sign up') ?></span>
                        <span class="arrow arrow-right"></span>
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>

<script>
     <?php
    $codedd= json_encode($codecountries)
    ?>
         var codevalues=<?=$codedd?>;
         //console.log(codevalues);
       var countryvalue=$('#CustomerCountryId').val();
       if(countryvalue !=''){
           $('#codeid').text(codevalues[countryvalue]);
       }else{
          $('#codeid').text('+00'); 
    }
    
   function selectcode(){
       //var codevalues=<?=$codedd?>;
       //console.log(codevalues);
       var countryvalue=$('#CustomerCountryId').val();
    if(countryvalue =='1'){
        $('#CustomerPhonNumber').val('5');
    }else{
          $('#CustomerPhonNumber').val(' ');
    }
    if(countryvalue !=''){
           $('#codeid').text(codevalues[countryvalue]);
           
       }else{
          $('#codeid').text('+00');
    }
       
       
   }
    //$('.register_content').hide();
    <?php /*if(!empty($this->params['form']['register'])){?>
        $('.register_content').show();
        $('.login_content').hide();
    }else{?>
    $('.register_content').hide();
    $('.login_content').show();
    <?php }*/?>
    $(".show_register_btn").click(function () {
        $('.register_content').show();
        $('.login_content').hide();
    });
    $(".show_login_btn").click(function () {
        $('.register_content').hide();
        $('.login_content').show();
    });
</script>