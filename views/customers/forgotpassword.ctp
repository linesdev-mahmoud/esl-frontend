<div class="Content login_page new_login_page">
    <div class="container">
        <?php echo $this->Session->flash(); ?>
        <div class="row">
            <div class="col-sm-6 img_content">
                <img src="<?= $this->Html->url('/') ?>assets/img/login.png">
            </div>
            <div class="col-sm-6 form_content login_content" style="<?= $style1 ?>">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6">
                        <h1><?= __('Forget password') ?></h1>
                    </div>
                </div>
                <form method="post">
                    <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-12">
                        <input type="email" name='data[Contact][email]' class="form-control" placeholder="Email address">              
                    </div>
                    <div class="btn_login col-sm-12 col-xs-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6 checkbox_text">
                            </div>
                                 <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 checkbox_text">
                                <div class="show_hide_content">
                                    <h3><?=
                                        $this->Html->link(
                                                '<span>' . __('Sign in?', true) . '</span>',
                                                ['action' => 'login_portal'],
                                                ['escape' => false, 'class' => 'show_btn']
                                        )
                                        ?>
                                    </h3> </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 login_text">
                                <button type="submit" class="btn btn-primary" name="login">
                                    <span class="arrow arrow-left"></span>
                                    <span class="btn-text"><?= __('Send') ?></span>
                                    <span class="arrow arrow-right"></span>
                                </button>
                            </div>
                       
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>