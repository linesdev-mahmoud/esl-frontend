<!DOCTYPE html>

<html>
    <head>
        <?php if (!isset($meta) || $meta == "") { ?>
        <title>Emirates Safety Laboratory | ESL </title>
        <?php
} else {
    echo $meta;
}
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick.css" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick-theme.css" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/aos.css" />
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.js"></script>
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.min.js"></script>

        <meta name="viewport" content="width=device-width" />
        <meta name="next-head-count" content="4" />
        <?php if ($controller == 'customers' || $controller == 'apages') { ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <?php } ?>
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/main.css" />

        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/custom<?= $_ar ?>.css" />
        <style data-styled="active" data-styled-version="5.1.1"></style>
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/file8.js"></script>
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/file9.js"></script>
        <link rel="icon" type="image/png" href="<?= $this->Html->url('/') ?>assets/img/fave.png" sizes="32x32">
        <style>
            .slick-list::before{
                content: "<?= $settings_data['home_part1']['value' . $_ar] ?>";
                color: #383838;
                font-size: 40px;
                font-weight: bold;
            }
        </style>
    </head>
    <?php
    $lang_action = '';
              if ($controller == 'pages' && $action == 'display') {
                  $lang_action = 'display';
              }
              $lang_link = 'ara';
              $img_lang = $this->Html->url('/assets/img/ara.png');
              $lang_title = 'العربية';
              if ($this->Session->read('Language.locale') == 'ara') {
                  $lang_link = 'eng';
                  $img_lang = $this->Html->url('/assets/img/eng.png');
                  $lang_title = 'English';
              }
    ?>
    <body class="demo-3" data-ls="1" data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
        <svg class="hidden">
            <defs>
                <filter id="filter-7">
                    <feturbulence type="fractalNoise" baseFrequency="0" numOctaves="5" result="warp"></feturbulence>
                    <fedisplacementmap xChannelSelector="R" yChannelSelector="G" scale="90" in="SourceGraphic" in2="warp"></fedisplacementmap>
                </filter>
            </defs>
        </svg>
        <div id="__next">
            <div id="awwwards" style="position: fixed; z-index: 999; transform: translateY(-50%); top: 50%; right: 0;display:none;">
                <a href="<?= $this->Html->url('/settings/lang_choice/') . $lang_link . '/' . $lang_action ?>" target="_self" class="image-lang">
                    <img src="<?= $img_lang ?>"><span><?= $lang_title ?></span>
                </a>

            </div>
            <div id="mobile-awwwards" style="position: fixed; z-index: 999; transform: translateY(-50%); top: 50%; right: 0;display:none;" wfd-id="39">
                <a href="<?= $this->Html->url('/settings/lang_choice/') . $lang_link . '/' . $lang_action ?>" target="_self" class="image-lang">
                    <img src="<?= $img_lang ?>"><span><?= $lang_title ?></span>
                </a>
            </div>
            <div class="Layout undefined">
                <nav style="background: #fbfbfb;">
                    <div class="flex justify-between container-1">
                        <a href="<?= $this->Html->url('/') ?>">
                            <img class="cursor-pointer logo" src="<?= $this->Html->url('/') ?>assets/img/logo.png" alt="" />
                        </a>
                        <div class="menu menu--line links">

                            <a class="menu__link effect-link nav-effect-link false" href="<?= $this->Html->url('/services') ?>">
                                <span class="menu__link-inner"><?= __('Services') ?></span>
                                <span class="menu__link-deco"></span>
                            </a>
                            <a class="menu__link effect-link nav-effect-link false" href="<?= $this->Html->url('/about') ?>">
                                <span class="menu__link-inner"><?= __('About') ?></span>
                                <span class="menu__link-deco"></span>
                            </a>
                            <a class="menu__link effect-link nav-effect-link false" href="<?= $this->Html->url('/login') ?>">
                                <span class="menu__link-inner"><?= __('Client Portal') ?></span>
                                <span class="menu__link-deco"></span>
                            </a>
                            <a class="menu__link effect-link nav-effect-link false" href="<?= $this->Html->url('/contact-us') ?>">
                                <span class="menu__link-inner"><?= __('Contact us') ?></span>
                                <span class="menu__link-deco"></span>
                            </a>


                        </div>
                    </div>
                </nav>
                <div class="mobile-nav">
                    <div class="hamburger hamburger--demo-3 js-hover">
                        <div class="hamburger__line hamburger__line--01"><div class="hamburger__line-in hamburger__line-in--01"></div></div>
                        <div class="hamburger__line hamburger__line--02"><div class="hamburger__line-in hamburger__line-in--02"></div></div>
                        <div class="hamburger__line hamburger__line--03"><div class="hamburger__line-in hamburger__line-in--03"></div></div>
                        <div class="hamburger__line hamburger__line--cross01"><div class="hamburger__line-in hamburger__line-in--cross01"></div></div>
                        <div class="hamburger__line hamburger__line--cross02"><div class="hamburger__line-in hamburger__line-in--cross02"></div></div>
                    </div>
                    <div class="global-menu">
                        <div class="global-menu__wrap">

                            <a href="<?= $this->Html->url('/services') ?>" class="global-menu__item global-menu__item--demo-3 false"><?= __('Services') ?></a>
                            <a href="<?= $this->Html->url('/about') ?>" class="global-menu__item global-menu__item--demo-3 false"><?= __('About') ?> </a>
                            <a href="<?= $this->Html->url('/login') ?>" class="global-menu__item global-menu__item--demo-3 false"><?= __('Client Portal') ?></a>
                            <a href="<?= $this->Html->url('/contact-us') ?>" class="global-menu__item global-menu__item--demo-3 false"><?= __('Contact us') ?></a>
                        </div>
                    </div>
                    <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
                        <path class="shape-overlays__path"></path>
                        <path class="shape-overlays__path"></path>
                        <path class="shape-overlays__path"></path>
                    </svg>
                    <span class="is-opened-navi is-opened hidden"></span>
                </div>

                <div class="Content login_page new_login_page directory_page">
                    <div class="container" style="max-width:1600px;">
                        <?php echo $this->Session->flash(); ?>
                        <div class="row">
                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-8 form_group_title">
                                <h2 class="search_title"><?=$settings['directory_part1']['value'.$_ar]?></h2>
                            </div>
                            <div class="col-sm-12 form_content login_content">
                                <form method="post" class="search_form">
                                    <div class="row">
                                        <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                            <label for="certifNum"><?= __('Certificate Number') ?>*</label>
                                            <input type="text" id="certifNum" class="form-control" placeholder="<?= __('Enter number') ?>" name="certifNum" required="required">
                                        </div>
                                        <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                            <label for="productNum"><?= __('Product Name & Type') ?>*</label>
                                            <input type="text" id="productNum" class="form-control" placeholder="<?= __('Enter keywords and Brand Name') ?>" name="certifNum" required="required">
                                        </div>
                                        <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                            <label for="productMnu"><?= __('Product Manufacturer') ?>*</label>
                                            <input type="text" id="productMnu" class="form-control" placeholder="<?= __('Enter Name') ?>" name="certifNum" required="required">
                                        </div>
                                        <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                            <label for="techSpec"><?= __('Technical Specifications') ?>*</label>
                                            <input type="text" id="techSpec" class="form-control" placeholder="<?= __('Enter Name') ?>" name="certifNum" required="required">
                                        </div>
                                    </div>
                                    <div class="btn_login col-sm-12 col-xs-12 col-md-12 col-lg-12">

                                        <button type="submit" class="btn btn-primary reset_btn">
                                            <span class="arrow arrow-right reset_btn_right"></span>
                                            <span class="btn-text"><?= __('Search') ?></span>
                                            <span class="arrow arrow-left reset_btn_left"></span>
                                        </button>
                                        <button type="reset" class="btn btn-primary" name="login">
                                            <span class="arrow arrow-left"></span>
                                            <span class="btn-text"><?= __('Reset') ?></span>
                                            <span class="arrow arrow-right"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-12">

                            </div>
                        </div>
                        <div class="row table_row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table style="margin-top:20px;" class="table table-striped table-bordered text-nowrap w-100">

                                        <thead>

                                            <tr>
                                                <th class="wd-15p"><?php __('Product Name'); ?></th>
                                                <th class="wd-15p"><?php __('Certificate Number'); ?></th>
                                                <th class="wd-15p"><?php __('Standard(s)'); ?></th>
                                                <th class="wd-15p"><?php __('Latest Issue Number'); ?></th>
                                                <th class="wd-15p"><?php __('Initially Issue Date'); ?></th>
                                                <th class="wd-20p"><?php __('Lastst Issue Date'); ?></th>
                                                <th class="wd-20p"><?php __('Expiry Date'); ?></th>
                                                <th class="wd-20p"><?php __('Status'); ?></th>

                                            </tr>
                                        </thead>
                                        <tbody>


                                            <tr role="row" class="odd">
                                                <td>-Product Name &amp; Product Type<br style="mso-data-placement:same-cell;"> </td>
                                                <td>000052/2021&nbsp;</td>
                                                <td>-EN 1364-3 (EN 1364-3:2014)<br style="mso-data-placement:same-cell;">-EN 1364-4 (EN 1364-4:2014)<br style="mso-data-placement:same-cell;">-EN 1364-1 (EN 1364-1:2015)<br style="mso-data-placement:same-cell;"></td>
                                                <td>v2&nbsp;</td>
                                                <td>2021-09-06&nbsp;</td>
                                                <td>2021-09-06&nbsp;</td>
                                                <td>2022-09-06&nbsp;</td>
                                                <td>Valid&nbsp;</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>-Product Name &amp; Product Type<br style="mso-data-placement:same-cell;"> </td>
                                                <td>000062/2021&nbsp;</td>
                                                <td>-EN 1364-3 (EN 1364-3:2014)<br style="mso-data-placement:same-cell;">-EN 1364-4 (EN 1364-4:2014)<br style="mso-data-placement:same-cell;">-EN 1364-1 (EN 1364-1:2015)<br style="mso-data-placement:same-cell;"></td>
                                                <td>v1&nbsp;</td>
                                                <td>2021-09-06&nbsp;</td>
                                                <td>2021-09-06&nbsp;</td>
                                                <td>2021-09-06&nbsp;</td>
                                                <td>Expired&nbsp;</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <!--<table class="table table-bordered result_tabel">
<thead>
<tr>
<th scope="col"><?= __('Certificate Number') ?> </th>
<th scope="col"><?= __('Product Manufacturer') ?></th>
<th scope="col"><?= __('Product Name & Type') ?></th>
<th scope="col"><?= __('Technical Specifications') ?></th>
<th scope="col"><?= __('Cert Version') ?></th>
</tr>
</thead>
<tbody>
<tr>
<td>123</td>
<td>Manu1</td>
<td>Product1</td>
<td>Technical1</td>
<td>Version1</td>
</tr>
<tr>

<td>123</td>
<td>Manu2</td>
<td>Product2</td>
<td>Technical2</td>
<td>Version2</td>
</tr>


</tbody>
</table>-->
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="section contact subscribe">
                        <div class="container-2">
                            <div class="flex flex-wrap justify-between">
                                <div class="text">
                                    <div class="text-2 aos-init" data-aos="slide-up"><?=$settings['Directory_subscribe_title']['value'.$_ar]?></div>
                                    <div class="text-6"><?=$settings['Directory_subscribe_content']['value'.$_ar]?></div>
                                </div>
                                <div class="right text-center w-full md:w-auto"><button type="button" class="btn btn-effect btn-dark aos-init" data-toggle="modal" data-target="#exampleModal" data-aos="slide-left"><span class="arrow arrow-left"></span><span class="btn-text"><?=$settings['Directory_subscribe_CTA']['value'.$_ar]?></span><span class="arrow arrow-right"></span></button></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade  aos-init" data-aos="slide-left" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><?=$settings['Directory_subscribe_popup']['value'.$_ar]?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="post" class="form_content">
                                        <div class="row">
                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-12">
                                                <div class="row checkbox_group">
                                                    <?php 
    $i = 1;
foreach($cats as $cat){?>
                                                    <div class="form-check col-sm-6">
                                                        <label class="form-check-label label_container" for="Checkbox_<?=$cat['id']?>">
                                                            <input class="form-check-input" name="newslettercat_id[<?=$i?>]" type="checkbox" id="Checkbox_<?=$cat['id']?>" value="<?=$cat['id']?>" <?php if($this->params['form']['newslettercat_id'][$i] == $cat['id']){echo 'checked';}?>>
                                                            <span class="checkmark"></span>
                                                            <?=$cat['title'.$_ar]?>
                                                        </label>
                                                    </div>
                                                    <?php 

    $i++;
                      }?>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                                <input type="text" class="form-control" placeholder="<?= __('First Name*') ?>" name="first_name" value="<?php if(isset($this->params['form']['first_name'])){echo @$this->params['form']['first_name'];}?>" required="required">
                                            </div>
                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                                <input type="text" class="form-control" placeholder="<?= __('Last Name*') ?>" name="last_name" value="<?php if(isset($this->params['form']['last_name'])){echo @$this->params['form']['last_name'];}?>" required="required">
                                            </div>
                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                                <input type="email" class="form-control" placeholder="<?= __('Corporate email address*') ?>" name="email" value="<?php if(isset($this->params['form']['email'])){echo @$this->params['form']['email'];}?>" required="required">
                                            </div>
                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-6">
                                                <input type="text" class="form-control" placeholder="<?= __('Organization*') ?>" name="organization" value="<?php if(isset($this->params['form']['organization'])){echo @$this->params['form']['organization'];}?>" required="required">
                                            </div>
                                            <div class="form-group col-sm-12 col-xs-12 col-md-12 col-lg-12">
                                                <textarea class="form-control" placeholder="<?= __('Job Function*') ?>" name="job_function" required="required"><?php if(isset($this->params['form']['job_function'])){echo @$this->params['form']['job_function'];}?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-check col-sm-12">

                                            <label class="form-check-label label_container" for="agree_input">
                                                <input class="form-check-input" type="checkbox" id="agree_input" name="agree" value="checked" required="required">
                                                <span class="checkmark"></span>
                                                <?=__('I agree to ESL’s ')?>
                                                <a href="http://20.203.2.65/uat/page/privacy-policy" target="_blank" class="agree_link"><?=__('Privacy Policy')?></a> 
                                                <?=__('and ')?> 
                                                <a href="http://20.203.2.65/uat/page/terms-use" target="_blank" class="agree_link"><?=__('Terms of Use')?></a>
                                            </label>
                                        </div>
                                        <div class="btn_login col-sm-12 col-xs-12 col-md-12 col-lg-12">
                                            <button type="submit" class="btn btn-primary" name="subscribe_btn">
                                                <span class="arrow arrow-left"></span>
                                                <span class="btn-text"><?= __('Sign up') ?></span>
                                                <span class="arrow arrow-right"></span>
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>

                <script>

                    function checkForm(form)
                    {
                        if(!form.agree.checked) {
                            alert("Please indicate that you accept the Terms and Conditions");
                            form.agree.focus();
                            return false;
                        }
                        return true;
                    }

                </script>
                <footer class="">
                    <div class="grid grid-cols-1 md:grid-cols-4">
                        <div class="col">
                            <div class="head"><?= $heads[1]['title' . $_ar] ?></div>
                            <?php foreach ($links[1] as $link) { ?>
                            <a class="effect-link false" href="<?= $link['url'] ?>"><span><?= $link['title' . $_ar] ?></span></a>
                            <?php } ?>

                        </div>
                        <div class="col">
                            <div class="head"><?= $heads[2]['title' . $_ar] ?></div>
                            <?php foreach ($links[2] as $link) { ?>
                            <a class="effect-link false" href="<?= $link['url'] ?>"><span><?= $link['title' . $_ar] ?></span></a>
                            <?php } ?>

                        </div>
                        <div class="col">
                            <div class="head"><?= $heads[3]['title' . $_ar] ?></div>
                            <?php foreach ($links[3] as $link) { ?>
                            <a class="effect-link false" href="<?= $link['url'] ?>"><span><?= $link['title' . $_ar] ?></span></a>
                            <?php } ?>
                        </div>
                        <div class="col">
                            <div class="head"><?= $heads[4]['title' . $_ar] ?></div>
                            <?php foreach ($links[4] as $link) { ?>
                            <a href="<?= $link['url'] ?>"><?= $link['title' . $_ar] ?></a>
                            <?php } ?>


                        </div>
                    </div>
                    <div class="bottom flex-wrap flex justify-between">
                        <a href="<?= $this->Html->url('/') ?>">
                            <img src="<?= $this->Html->url('/') ?>assets/img/logo.png" alt="" />
                        </a>
                        <div class="right">
                            <a href="<?= $this->Html->url('/') ?>">Privacy Policy</a>
                            <span class="copyright">© Copyright 2021 ESL.</span>
                        </div>
                    </div>
                </footer>
            </div>
        </div>





        <!--<script nomodule="" src="./assets/js/polyfills.js"></script>-->
        <script src="<?= $this->Html->url('/') ?>assets/js/main.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/webpack.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/framework.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/file1.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/file2.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/file3.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/file4.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/file5.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/app.js" async=""></script>


        <?php if ($controller == 'apages' && $action == 'contact') { ?>
        <script src="<?= $this->Html->url('/') ?>assets/js/menu_contact.js" async=""></script>
        <?php } else { ?>
        <!--<script src="<?= $this->Html->url('/') ?>assets/js/menu_index.js" async=""></script>-->
        <!-- menu index -->

        <?php } ?>
        <script src="<?= $this->Html->url('/') ?>assets/js/file7.js" async=""></script>

        <script src="<?= $this->Html->url('/') ?>assets/js/buildManifest.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/ssgManifest.js" async=""></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/slick.min.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/aos.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/TweenMax.min.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/ScrollMagic.min.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/animation.gsap.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/jquery-ui.min.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="<?= $this->Html->url('/') ?>assets/js/priceSlider.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/easings.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/demo3.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/common.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
        <?php if ($controller == 'customers' || $controller == 'apages') { ?>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <?php } ?>
        <script>

            setInterval(function () {
                <?php
                $img_lang = $this->Html->url('/assets/img/ara.png');
                $lang_title = 'العربية';
                if ($this->Session->read('Language.locale') == 'ara') {
                    $img_lang = $this->Html->url('/assets/img/eng.png');
                    $lang_title = 'English';
                }
                ?>
                $('.image-lang').html('<img src="<?= $img_lang ?>"><span><?= $lang_title ?></span>');
                //$('.first').html("We are the one-stop shop <span style='color:#2ba78a;'>Testing Laboratory</span> and <span style='color:#2ba78a;'>Certification Body </span>based in Dubai with strong expertise in fire protection requirements.");
                //$('.text_replace').html("Behind successful fire protection products, systems or materials are efficient and robust Testing, Inspection and Certification (TIC) processes. To us, that’s our DNA.<br>We ensure safety, integrity, and quality of your passive or active fire protection systems through conformity assessment and assurance. We provide conformity assessment for Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
                //$('.second').html("We <span style='color:#2ba78a;'>certify</span> , <span style='color:#2ba78a;'>test</span> and provide advisory on development project.<br> Let us help you win the competition, reach your maximum, and score your company goals successfully.  Let’s start");

            }, 500);
        </script>
    </body>
</html>