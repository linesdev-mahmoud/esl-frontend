<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="initial-scale=1.0, width=device-width">
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick.css">
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick-theme.css">
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/aos.css">
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.min.js"></script>
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/insight.min.js"></script>
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.js"></script>
        <script async="" src="<?= $this->Html->url('/') ?>assets/js/gtm.js"></script>
        <script src="<?= $this->Html->url('/') ?>assets/380429773370024" async=""></script>
        <script async="" src="<?= $this->Html->url('/') ?>assets/js/fbevents.js"></script>


        <meta name="viewport" content="width=device-width">
        <title>Emirates Safety Laboratory |  ESL</title>
        <meta name="next-head-count" content="4">
        <link rel="icon" type="image/png" href="<?= $this->Html->url('/') ?>assets/img/fave.png" sizes="32x32">
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/main.css">
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/custom.css" />
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/12.185691b4d5ee03f953fd.js"></script>

        <style data-emotion="css"></style>
    </head>

    <body class="demo-3" data-ls="1" data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0"><svg class="hidden"><defs><filter id="filter-7"><feturbulence type="fractalNoise" baseFrequency="0" numOctaves="5" result="warp"></feturbulence><fedisplacementmap xChannelSelector="R" yChannelSelector="G" scale="90" in="SourceGraphic" in2="warp"></fedisplacementmap></filter></defs></svg>
    <div
        id="__next">
<div id="awwwards" style="display: none;position:fixed;z-index:999;transform:translateY(-50%);top:50%;right:0"><a href="https://www.awwwards.com/inspiration/search?text=Chanceupon" target="_blank"><svg width="53.08" height="171.358"><path class="js-color-bg" fill="black" d="M0 0h53.08v171.358H0z"></path><g class="js-color-text" fill="white"><path d="M20.047 153.665v-1.9h3.888v-4.093h-3.888v-1.9h10.231v1.9h-4.59v4.093h4.59v1.9zM29.898 142.236c-.331.565-.784.997-1.359 1.294s-1.222.446-1.944.446c-.721 0-1.369-.149-1.943-.446a3.316 3.316 0 0 1-1.36-1.294c-.331-.564-.497-1.232-.497-2.002s.166-1.438.497-2.002a3.316 3.316 0 0 1 1.36-1.294c.574-.297 1.223-.445 1.943-.445.723 0 1.369.148 1.944.445a3.307 3.307 0 0 1 1.359 1.294c.331.564.497 1.232.497 2.002s-.166 1.438-.497 2.002m-1.703-3.347c-.435-.33-.967-.496-1.601-.496-.633 0-1.166.166-1.601.496-.433.332-.649.78-.649 1.346 0 .564.217 1.013.649 1.345.435.331.968.497 1.601.497.634 0 1.166-.166 1.601-.497.435-.332.649-.78.649-1.345.001-.566-.214-1.014-.649-1.346M22.911 134.852v-1.813h1.186a3.335 3.335 0 0 1-.951-1.009 2.423 2.423 0 0 1-.352-1.271c0-.682.19-1.229.57-1.645.381-.413.932-.621 1.652-.621h5.262v1.812h-4.721c-.419 0-.727.096-.921.285-.195.19-.292.447-.292.769 0 .302.115.58.35.833.234.254.577.458 1.03.613.454.156.993.234 1.616.234h2.938v1.813h-7.367zM29.898 125.136a3.314 3.314 0 0 1-1.359 1.294c-.575.297-1.222.445-1.944.445-.721 0-1.369-.148-1.943-.445a3.322 3.322 0 0 1-1.36-1.294c-.331-.565-.497-1.232-.497-2.002 0-.771.166-1.438.497-2.003a3.313 3.313 0 0 1 1.36-1.293c.574-.297 1.223-.446 1.943-.446.723 0 1.369.149 1.944.446s1.028.728 1.359 1.293.497 1.232.497 2.003c.001.769-.166 1.436-.497 2.002m-1.703-3.347c-.435-.331-.967-.497-1.601-.497-.633 0-1.166.166-1.601.497-.433.331-.649.778-.649 1.345 0 .564.217 1.013.649 1.344.435.332.968.498 1.601.498.634 0 1.166-.166 1.601-.498.435-.331.649-.779.649-1.344.001-.567-.214-1.014-.649-1.345M22.911 117.75v-1.812h1.199c-.419-.265-.742-.586-.972-.966s-.345-.784-.345-1.213c0-.272.05-.569.146-.892l1.682.336a1.429 1.429 0 0 0-.205.76c0 .576.261 1.048.783 1.418.521.37 1.342.557 2.461.557h2.617v1.812h-7.366zM29.812 111.252c-.391.511-.857.851-1.403 1.016l-.776-1.446c.381-.138.68-.329.893-.577.215-.249.321-.544.321-.885a1.2 1.2 0 0 0-.168-.658c-.112-.175-.294-.263-.548-.263-.225 0-.406.105-.548.313-.142.21-.291.534-.446.973-.019.068-.058.17-.117.307-.224.565-.506 1.004-.848 1.315-.34.313-.779.467-1.314.467-.381 0-.727-.102-1.039-.306a2.185 2.185 0 0 1-.744-.84 2.554 2.554 0 0 1-.279-1.207c0-.497.105-.949.314-1.359.211-.408.506-.725.886-.949l.993 1.082c-.43.292-.644.686-.644 1.184a.84.84 0 0 0 .154.504.471.471 0 0 0 .401.212c.176 0 .338-.103.49-.307.15-.205.334-.604.547-1.199.205-.564.474-1.001.805-1.308.332-.308.756-.46 1.271-.46.721 0 1.299.229 1.732.687s.65 1.057.65 1.797c.001.759-.194 1.396-.583 1.907M35.481 17.006l-4.782 14.969h-3.266l-2.584-9.682-2.584 9.682h-3.268l-4.782-14.969h3.713l2.673 10.276 2.525-10.276h3.445l2.524 10.276 2.674-10.276zM37.978 27.163c1.426 0 2.496 1.068 2.496 2.495 0 1.425-1.07 2.495-2.496 2.495-1.425 0-2.494-1.07-2.494-2.495-.001-1.427 1.069-2.495 2.494-2.495"></path></g></svg></a></div>
            <div
                 id="mobile-awwwards" style="display: none;position:fixed;z-index:999;transform:translateY(-50%);top:50%;right:0" wfd-id="39"><a href="https://www.awwwards.com/inspiration/search?text=Chanceupon" target="_blank"><svg width="53.08" height="171.358"><path class="js-color-bg" fill="#EE762D" d="M0 0h53.08v171.358H0z"></path><g class="js-color-text" fill="#fff"><path d="M20.047 153.665v-2.134l6.387-2.411-6.387-2.413v-2.133h10.23v1.9h-6.314l5.379 1.959v1.374l-5.379 1.958h6.314v1.9zM29.898 141.038a3.319 3.319 0 0 1-1.361 1.294c-.573.297-1.221.445-1.943.445-.721 0-1.368-.148-1.943-.445a3.326 3.326 0 0 1-1.359-1.294c-.331-.565-.497-1.232-.497-2.002 0-.771.166-1.438.497-2.003a3.317 3.317 0 0 1 1.359-1.293c.575-.298 1.223-.446 1.943-.446.723 0 1.37.148 1.943.446a3.31 3.31 0 0 1 1.361 1.293c.33.564.496 1.232.496 2.003.001.77-.165 1.437-.496 2.002m-1.703-3.348c-.435-.33-.968-.496-1.602-.496-.633 0-1.167.166-1.6.496-.435.331-.651.779-.651 1.346 0 .564.217 1.013.651 1.344.433.332.967.498 1.6.498.634 0 1.167-.166 1.602-.498.434-.331.649-.779.649-1.344.001-.566-.215-1.015-.649-1.346M30.117 131.176c-.186.326-.386.548-.6.665h.76v1.812H19.93v-1.812h3.654c-.215-.166-.4-.414-.556-.746a2.422 2.422 0 0 1-.235-1.038c0-1.082.345-1.912 1.031-2.492.688-.579 1.611-.869 2.771-.869s2.083.29 2.771.869c.687.58 1.029 1.41 1.029 2.492 0 .419-.093.792-.278 1.119m-1.871-2.099c-.399-.32-.949-.481-1.651-.481-.711 0-1.265.161-1.659.481-.395.322-.592.731-.592 1.229s.197.904.592 1.22c.395.317.948.476 1.659.476.712 0 1.264-.158 1.659-.476.394-.315.592-.723.592-1.22s-.2-.907-.6-1.229M21.625 124.98c-.225.225-.502.337-.833.337s-.608-.112-.833-.337-.337-.502-.337-.833c0-.332.112-.608.337-.833s.502-.337.833-.337.608.112.833.337.336.501.336.833c0 .332-.111.609-.336.833m1.286-1.739h7.367v1.812h-7.367v-1.812zM19.93 119.389h10.349v1.813H19.93zM29.832 116.189c-.375.531-.853.921-1.433 1.169a4.548 4.548 0 0 1-3.611 0 3.339 3.339 0 0 1-1.433-1.169c-.375-.532-.563-1.196-.563-1.995 0-.77.184-1.413.55-1.93a3.282 3.282 0 0 1 1.381-1.14 4.23 4.23 0 0 1 1.71-.365h.746v5.072a1.798 1.798 0 0 0 1.169-.49c.332-.307.497-.724.497-1.249 0-.41-.094-.753-.278-1.031-.185-.277-.473-.529-.862-.753l.541-1.462c.691.302 1.224.724 1.592 1.265.371.541.557 1.234.557 2.083 0 .799-.188 1.463-.563 1.995m-4.086-3.574c-.408.089-.746.262-1.008.52-.263.258-.395.611-.395 1.06 0 .429.136.784.408 1.067.273.282.604.458.994.526v-3.173zM20.047 102.852v-1.899l10.231-3.464v2.046l-2.412.746v3.244l2.412.745v2.046l-10.231-3.464zm6.065-2.031l-3.552 1.08 3.552 1.082v-2.162zM22.91 97.414v-1.827l5.059-1.316-5.059-1.243v-1.695l5.059-1.242-5.059-1.316v-1.827l7.367 2.354v1.607l-4.763 1.273 4.763 1.27v1.609zM29.811 85.676c-.391.419-.94.628-1.652.628-.73 0-1.314-.277-1.754-.833-.438-.555-.658-1.306-.658-2.251v-1.286h-.32c-.36 0-.632.122-.812.366-.181.243-.271.55-.271.92 0 .633.282 1.058.848 1.272l-.352 1.534a2.316 2.316 0 0 1-1.482-.942c-.375-.513-.563-1.132-.563-1.864 0-.984.261-1.747.782-2.287.521-.541 1.289-.812 2.302-.812h4.4v1.491l-.937.19c.702.575 1.053 1.33 1.053 2.265 0 .654-.195 1.19-.584 1.609m-1.383-3.245c-.276-.331-.645-.497-1.104-.497h-.144v1.213c0 .4.078.709.233.93.156.219.394.327.716.327a.658.658 0 0 0 .52-.226c.132-.151.197-.349.197-.593 0-.439-.14-.822-.418-1.154M22.91 78.179v-1.813h1.199a2.93 2.93 0 0 1-.972-.965 2.324 2.324 0 0 1-.345-1.213c0-.273.05-.57.146-.892l1.682.336a1.452 1.452 0 0 0-.205.76c0 .576.262 1.048.783 1.418s1.342.556 2.461.556h2.617v1.813H22.91zM29.364 71.716c-.687.561-1.61.84-2.771.84-1.158 0-2.082-.279-2.77-.84s-1.029-1.352-1.029-2.375c0-.42.074-.802.226-1.147.151-.347.339-.607.563-.782H19.93v-1.813h10.348v1.813h-.76c.224.117.427.35.607.693.18.348.27.759.27 1.236 0 1.023-.344 1.814-1.031 2.375m-1.11-3.99c-.396-.316-.947-.476-1.66-.476-.711 0-1.264.159-1.657.476a1.493 1.493 0 0 0-.593 1.221c0 .496.197.906.593 1.229.394.32.946.481 1.657.481.703 0 1.252-.161 1.652-.481.4-.322.6-.732.6-1.229 0-.498-.198-.905-.592-1.221M35.48 17.006l-4.783 14.969h-3.265l-2.584-9.682-2.584 9.682h-3.267l-4.782-14.969h3.712L20.6 27.282l2.525-10.276h3.445l2.525 10.276 2.673-10.276zM37.978 27.163c1.425 0 2.495 1.068 2.495 2.495 0 1.425-1.07 2.495-2.495 2.495-1.426 0-2.495-1.07-2.495-2.495-.001-1.427 1.069-2.495 2.495-2.495"></path></g></svg></a></div>

        <div
            class="Layout undefined">
            <nav style="background: #fbfbfb;">
                <div class="flex justify-between container-1">
                    <a href="#">
                        <img class="cursor-pointer logo" src="<?= $this->Html->url('/') ?>assets/img/logo.png" alt="" />
                    </a>
                    <div class="menu menu--line links">
                        <a class="menu__link effect-link nav-effect-link false" href="https://appslink-me.com/esl/index.html">
                            <span class="menu__link-inner">Home</span>
                            <span class="menu__link-deco"></span>
                        </a>
                        <a class="menu__link effect-link nav-effect-link false" href="#">
                            <span class="menu__link-inner">About</span>
                            <span class="menu__link-deco"></span>
                        </a>
                        <a class="menu__link effect-link nav-effect-link false" href="#">
                            <span class="menu__link-inner">Services </span>
                            <span class="menu__link-deco"></span>
                        </a>
                        <a class="menu__link effect-link nav-effect-link false" href="#">
                            <span class="menu__link-inner">Blog</span>
                            <span class="menu__link-deco"></span>
                        </a>
                        <!--<a class="menu__link effect-link nav-effect-link false" href="contact.html">
                            <span class="menu__link-inner">Contact us</span>
                            <span class="menu__link-deco"></span>
                        </a>-->

                        <a href="https://appslink-me.com/esl/register.html" class="c-button--marquee"><span data-text="Get Started">Get Started</span></a>
                    </div>
                </div>
            </nav>
            <div class="mobile-nav">
                <div class="hamburger hamburger--demo-3 js-hover">
                    <div class="hamburger__line hamburger__line--01"><div class="hamburger__line-in hamburger__line-in--01"></div></div>
                    <div class="hamburger__line hamburger__line--02"><div class="hamburger__line-in hamburger__line-in--02"></div></div>
                    <div class="hamburger__line hamburger__line--03"><div class="hamburger__line-in hamburger__line-in--03"></div></div>
                    <div class="hamburger__line hamburger__line--cross01"><div class="hamburger__line-in hamburger__line-in--cross01"></div></div>
                    <div class="hamburger__line hamburger__line--cross02"><div class="hamburger__line-in hamburger__line-in--cross02"></div></div>
                </div>
                <div class="global-menu">
                    <div class="global-menu__wrap">
                        <a href="https://appslink-me.com/esl/index.html" class="global-menu__item global-menu__item--demo-3 false">Home</a>
                        <a href="#" class="global-menu__item global-menu__item--demo-3 false">About</a>
                        <a href="#" class="global-menu__item global-menu__item--demo-3 false">Services </a>
                        <a href="#" class="global-menu__item global-menu__item--demo-3 false">Blog</a>
                        <!--<a href="contact.html" class="global-menu__item global-menu__item--demo-3 false">Contact us</a>-->

                        <a href="https://appslink-me.com/esl/register.html" class="global-menu__item global-menu__item--demo-3">Get Started</a>
                    </div>
                </div>
                <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path class="shape-overlays__path"></path>
                <path class="shape-overlays__path"></path>
                <path class="shape-overlays__path"></path>
                </svg>
                <span class="is-opened-navi is-opened hidden"></span>
            </div>
            <div
                     class="Content">
                    <div class="page page-get-started ">
                        <div class="section hero text-center">
                            <div class="container-2">
                                <h1>Drop us a line</h1>
                            </div>
                            <div class="container-3">
                                <h2>Fill out the form and an ESL representative will contact you as soon as possible.</h2>
                            </div>
                        </div>
                        <div class="section bg-light-grey-two form">
                            <form action="/contact">
                                <div class="form-group container-5">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Types of service</div>
                                        <div class="text-9 form-header__sub">What do you need help with?</div>
                                    </div>
                                    <div class="tab-button">
                                        <button type="button" class="active">Product Certification</button>
                                        
                                        <button type="button" class=" ">Certification Renewal</button>
                                        <button type="button" class=" ">Market Entry</button>
                                        <button type="button" class=" ">Fire Testing</button>
                                    </div>
                                </div>
                                <div class="form-group container-5">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Type of Certification</div>
                                        <div class="text-9 form-header__sub">Which category applies to you?</div>
                                    </div>
                                    <div class="grid grid-cols-1 md:grid-cols-3">
                                        <button type="button" class="">New Product Certificate</button>
                                        <button type="button" class="">Certificate Renewal</button>
                                        <button type="button" class="">Change in existing Certificate</button>
                                    </div>
                                </div>
                                <div class="form-group container-2">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Type of Product or System</div>
                                        <div class="text-9 form-header__sub">What is your product or system? </div>
                                    </div>
                                    <div class="grid grid-cols-2 md:grid-cols-5">
                                        <button type="button" class="">Lorem Ipsum1</button>
                                        <button type="button" class="">Lorem Ipsum2</button>
                                        <button type="button" class="">Lorem Ipsum3</button>
                                        <button type="button" class="">Lorem Ipsum4</button>
                                        <button type="button" class="">Lorem Ipsum5</button>
                                    </div>
                                </div>
                                <div class="form-group container-2">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Type of Application</div>
                                        <div class="text-9 form-header__sub">What are the intended uses for the product or system? </div>
                                    </div>
                                    <div class="grid grid-cols-2 md:grid-cols-5">
                                        <button type="button" class="">Lorem Ipsum1</button>
                                        <button type="button" class="">Lorem Ipsum2</button>
                                        <button type="button" class="">Lorem Ipsum3</button>
                                        <button type="button" class="">Lorem Ipsum4</button>
                                        <button type="button" class="">Lorem Ipsum5</button>
                                    </div>
                                </div>
                                <div class="form-group container-2">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Product Standard</div>
                                        <div class="text-9 form-header__sub">Which standards are you interested in? </div>
                                    </div>
                                    <div class="grid grid-cols-2 md:grid-cols-5">
                                        <button type="button" class="">Lorem Ipsum1</button>
                                        <button type="button" class="">Lorem Ipsum2</button>
                                        <button type="button" class="">Lorem Ipsum3</button>
                                        <button type="button" class="">Lorem Ipsum4</button>
                                        <button type="button" class="">Lorem Ipsum5</button>
                                    </div>
                                </div>
                                <div class="form-group container-5 mb-40p" style="display: none;">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Budget</div>
                                        <div class="text-9 form-header__sub">Move the slider to set your budget range</div>
                                    </div>
                                    <div class="slider-wrapper">
                                        <div class="box">
                                            <div class="values text-center">
                                                <div><span id="first">$500</span></div>-
                                                <div><span id="second">&gt; $100 000</span></div>
                                            </div>
                                            <div class="slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content" data-value-0="#first" data-value-1="#second" data-range="#third" style="--l:-0.0000225098; --r:0.00034502;"><svg viewBox="0 0 613.976 83"><path d="M 0,42 C 0,42 -49.11808,42.00000000000001 0,42 C 49.11808,42 208.75184000000002,42.000000000000014 306.988,42 C 405.22416,42 564.85792,42.00000000000001 613.976,42 C 663.09408,42 613.976,42 613.976,42"></path></svg>
                                                <div
                                                     class="active"><svg viewBox="0 0 613.976 83"><path d="M 0,42 C 0,42 -49.11808,42.00000000000001 0,42 C 49.11808,42 208.75184000000002,42.000000000000014 306.988,42 C 405.22416,42 564.85792,42.00000000000001 613.976,42 C 663.09408,42 613.976,42 613.976,42"></path></svg></div>
                                                <div
                                                     class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"><div></div></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"><div></div></span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group container-5 mb-40p">
                                    <div class="form-header">
                                        <div class="text-5 form-header__header">Personal information<span class="">*</span></div>
                                        <div class="text-9 form-header__sub">Fill in your personal details!</div>
                                    </div>
                                    <div class="grid grid-cols-1 md:grid-cols-2">
                                        <input type="text" id="name" placeholder="First Name" required="">
                                        <input type="text" id="last" placeholder="Last Name" required="">
                                        <input type="text" id="company" placeholder="Company" required="">
                                        
                                        <div class="select-container">
                                            <div class=" css-2b097c-container">
                                                <div class=" css-1sle0x-control">
                                                    <div class=" css-ba109v">
                                                        <div class=" css-1wa3eu0-placeholder">Country</div>
                                                        <div class="css-1g6gooi">
                                                            <div class="" style="display: inline-block;"><input autocapitalize="none" autocomplete="off" autocorrect="off" id="react-select-2-input" spellcheck="false" tabindex="0" type="text" aria-autocomplete="list" value="" style="box-sizing: content-box; width: 2px; background: 0px center; border: 0px; font-size: inherit; opacity: 0; outline: 0px; padding: 0px; color: inherit;">
                                                                <div
                                                                     style="position: absolute; top: 0px; left: 0px; visibility: hidden; height: 0px; overflow: scroll; white-space: pre; font-size: 16px; font-family: SofiaPro-Regular, sans-serif; font-weight: 400; font-style: normal; letter-spacing: normal; text-transform: none;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=" css-1wy0on6"><span class=" css-1jqv6ht-indicatorSeparator"></span>
                                                        <div aria-hidden="true" class=" css-3nlya1-indicatorContainer"><svg height="20" width="20" viewBox="0 0 20 20" aria-hidden="true" focusable="false" class="css-19bqh2r"><path d="M4.516 7.548c0.436-0.446 1.043-0.481 1.576 0l3.908 3.747 3.908-3.747c0.533-0.481 1.141-0.446 1.574 0 0.436 0.445 0.408 1.197 0 1.615-0.406 0.418-4.695 4.502-4.695 4.502-0.217 0.223-0.502 0.335-0.787 0.335s-0.57-0.112-0.789-0.335c0 0-4.287-4.084-4.695-4.502s-0.436-1.17 0-1.615z"></path></svg></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="email" id="email" placeholder="Corporate Email" required="">
                                        <input type="text" id="phone" placeholder="Phone Number" required="">
                                        
                                        <textarea class="md:col-span-2" name="" id="detail" placeholder="What else should we know about your project?"></textarea>
                                    </div>
                                </div>
                                <div class="text-center"><button type="submit" class="btn mx-auto block"><span class="arrow arrow-left"></span><span class="btn-text">Submit</span><span class="arrow arrow-right"></span></button></div>
                            </form>
                        </div>
                    </div>
                </div>
            <script>
            
            _N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [23], {
        Enfu: function(e, t, n) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/get-started", function() {
                return n("kIQt")
            }])
        },
        kIQt: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, "default", (function() {
                return E
            }));
            var a = n("o0o1"),
                r = n.n(a),
                o = n("HaE+"),
                i = n("rePB"),
                s = n("1OyB"),
                l = n("JX7q"),
                u = n("vuIU"),
                c = n("Ji7U"),
                d = n("md7G"),
                p = n("foSv"),
                h = n("q1tI"),
                m = n.n(h),
                f = n("CafY"),
                v = n("8Kt/"),
                g = n.n(v),
                y = n("zS2O"),
                b = (n("YFqc"), m.a.createElement);

            function N(e) {
                var t = e.header,
                    n = e.subHeader,
                    a = e.required;
                return b("div", {
                    className: "form-header"
                }, b("div", {
                    className: "text-5 form-header__header"
                }, t, void 0 !== a && a ? b("span", {
                    className: ""
                }, "*") : ""), b("div", {
                    className: "text-9 form-header__sub"
                }, n))
            }
            var w = n("puUH"),
                O = n("a6RD"),
                k = n.n(O),
                C = n("vDqi"),
                H = n.n(C),
                S = (n("nOHt"), m.a.createElement);

            function x(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function j(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? x(Object(n), !0).forEach((function(t) {
                        Object(i.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : x(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }

            function P(e) {
                var t = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var n, a = Object(p.a)(e);
                    if (t) {
                        var r = Object(p.a)(this).constructor;
                        n = Reflect.construct(a, arguments, r)
                    } else n = a.apply(this, arguments);
                    return Object(d.a)(this, n)
                }
            }
            var T = k()((function() {
                    return Promise.all([n.e(0), n.e(12)]).then(n.bind(null, "IxL5"))
                }), {
                    loading: function() {
                        return S("p", null, "Loading ...")
                    },
                    ssr: !1,
                    loadableGenerated: {
                        webpack: function() {
                            return ["IxL5"]
                        },
                        modules: ["../components/customSelect"]
                    }
                }),
                E = function(e) {
                    Object(c.a)(n, e);
                    var t = P(n);

                    function n(e) {
                        var a;
                        return Object(s.a)(this, n), (a = t.call(this, e)).state = {
                            type: "Hiring",
                            typeOfTalent: "",
                            typeOfHiring: "",
                            noOfTalents: "",
                            duration: "",
                            budget: "",
                            name: "",
                            email: "",
                            country: "",
                            phone: "",
                            detail: "",
                            typeOfProject: "",
                            typeOfSolution: ""
                        }, a.handleLoad = a.handleLoad.bind(Object(l.a)(a)), a
                    }
                    return Object(u.a)(n, null, [{
                        key: "getInitialProps",
                        value: function(e) {
                            return {
                                query: e.query
                            }
                        }
                    }]), Object(u.a)(n, [{
                        key: "componentDidMount",
                        value: function() {
                            "complete" === document.readyState && this.handleLoad(), window.addEventListener("load", this.handleLoad), this.setState({
                                type: this.props.query.type ? this.props.query.type : "Hiring"
                            })
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function() {
                            window.removeEventListener("load", this.handleLoad)
                        }
                    }, {
                        key: "handleLoad",
                        value: function() {
                            var e = this;
                            $(".slider").each((function(t) {
                                var n, a, r = $(this),
                                    o = r.width(),
                                    i = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                                i.setAttribute("viewBox", "0 0 " + o + " 83"), r.html(i), r.append($("<div>").addClass("active").html(i.cloneNode(!0))), r.slider({
                                    range: !0,
                                    values: [500, 1e5],
                                    min: 500,
                                    step: 500,
                                    minRange: 1e3,
                                    max: 1e5,
                                    create: function(e, t) {
                                        r.find(".ui-slider-handle").append($("<div />")), $(r.data("value-0")).html("$" + r.slider("values", 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), 1e5 === r.slider("values", 1) ? $(r.data("value-1")).html("> $" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")) : $(r.data("value-1")).html("$" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), $(r.data("range")).html((r.slider("values", 1) - r.slider("values", 0)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), setCSSVars(r)
                                    },
                                    start: function(e, t) {
                                        $("body").addClass("ui-slider-active"), n = $(t.handle).data("index", t.handleIndex), a = r.find(".ui-slider-handle")
                                    },
                                    change: function(e, t) {
                                        setCSSVars(r)
                                    },
                                    slide: function(t, n) {
                                        var a = r.slider("option", "min"),
                                            o = r.slider("option", "minRange"),
                                            i = r.slider("option", "max");
                                        if (0 == n.handleIndex) {
                                            if (n.values[0] + o >= n.values[1] && r.slider("values", 1, n.values[0] + o), n.values[0] > i - o) return !1
                                        } else if (1 == n.handleIndex && (n.values[1] - o <= n.values[0] && r.slider("values", 0, n.values[1] - o), n.values[1] < a + o)) return !1;
                                        $(r.data("value-0")).html("$" + r.slider("values", 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), 1e5 === r.slider("values", 1) ? $(r.data("value-1")).html("> $" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")) : $(r.data("value-1")).html("$" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), $(r.data("range")).html((r.slider("values", 1) - r.slider("values", 0)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), setCSSVars(r), e.setState({
                                            budget: "".concat(n.values[0], "-").concat(n.values[1])
                                        })
                                    },
                                    stop: function(e, t) {
                                        $("body").removeClass("ui-slider-active");
                                        var a = Elastic.easeOut.config(1.08, .44);
                                        TweenMax.to(n, .6, {
                                            "--y": 0,
                                            ease: a
                                        }), TweenMax.to(s, .6, {
                                            y: 42,
                                            ease: a
                                        }), n = null
                                    }
                                });
                                var s = new Proxy({
                                    x: null,
                                    y: null,
                                    b: null,
                                    a: null
                                }, {
                                    set: function(e, t, n) {
                                        return e[t] = n, null !== e.x && null !== e.y && null !== e.b && null !== e.a && r.find("svg").html(getPath([e.x, e.y], e.b, e.a, o)), !0
                                    },
                                    get: function(e, t) {
                                        return e[t]
                                    }
                                });
                                s.x = o / 2, s.y = 42, s.b = 0, s.a = o, $(document).on("mousemove touchmove", (function(e) {
                                    if (n) {
                                        var t = a.eq(0 == n.data("index") ? 1 : 0),
                                            o = n.position().left,
                                            i = t.position().left,
                                            l = n.outerWidth(),
                                            u = l / 2,
                                            c = e.pageY - n.offset().top - n.outerHeight() / 2,
                                            d = c - 4 >= 0 ? c - 4 : c + 4 <= 0 ? c + 4 : 0,
                                            p = 1;
                                        d = d > 24 ? 24 : d < -24 ? -24 : d, p = (p = 0 == n.data("index") ? (o + u <= 52 ? (o + u) / 52 : 1) * (i - o - l <= 52 ? (i - o - l) / 52 : 1) : (o - (i + 2 * u) <= 52 ? (o - (i + l)) / 52 : 1) * (r.outerWidth() - (o + u) <= 52 ? (r.outerWidth() - (o + u)) / 52 : 1)) > 1 ? 1 : p < 0 ? 0 : p, 0 == n.data("index") ? (s.b = o / 2 * p, s.a = i) : (s.b = i + u, s.a = (r.outerWidth() - o) / 2 + o + u + (r.outerWidth() - o) / 2 * (1 - p)), s.x = o + u, s.y = d * p + 42, n.css("--y", d * p)
                                    }
                                }))
                            }))
                        }
                    }, {
                        key: "setType",
                        value: function(e) {
                            this.setState({
                                type: e
                            })
                        }
                    }, {
                        key: "inputChangeHandler",
                        value: function(e) {
                            this.setState(Object(i.a)({}, "".concat(e.target.id), e.target.value))
                        }
                    }, {
                        key: "selectChangeHandler",
                        value: function(e, t) {
                            this.setState(Object(i.a)({}, "".concat(e), t))
                        }
                    }, {
                        key: "handleSubmit",
                        value: function() {
                            var e = Object(o.a)(r.a.mark((function e(t) {
                                return r.a.wrap((function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return t.preventDefault(), e.next = 3, H.a.post("/api/contact", j({}, this.state));
                                        case 3:
                                            window.location.href = "/submission-complete";
                                        case 4:
                                        case "end":
                                            return e.stop()
                                    }
                                }), e, this)
                            })));
                            return function(t) {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "render",
                        value: function() {
                            var e = this;
                            return S(f.a, null, S("div", {
                                className: "page page-get-started "
                            }, S(g.a, null, S("title", null, "Emirates Safety Laboratory |  ESL"), S("meta", {
                                name: "description",
                                content: "Emirates Safety Laboratory |  ESL"
                            })), S(y.a, {
                                title: "Drop us a line",
                                subtitle: "Fill out the form and an ESL representative will contact you as soon as possible."
                            }), S("div", {
                                className: "section bg-light-grey-two form"
                            }, S("form", {
                                onSubmit: function(t) {
                                    return e.handleSubmit(t)
                                }
                            }, S("div", {
                                className: "form-group container-5"
                            }, S(N, {
                                header: "Types of service",
                                subHeader: "What do you need help with?"
                            }), S("div", {
                                className: "tab-button"
                            }, S("button", {
                                type: "button",
                                className: "Hiring" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Hiring")
                                }
                            }, "Product Certification"), S("button", {
                                type: "button",
                                className: "Co-develop product" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Co-develop product")
                                }
                            }/*, "Fire Testing"), S("button", {
                                type: "button",
                                className: "fire_testing" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Fire testing")
                                }
                            }*/, "Certification Renewal"), S("button", {
                                type: "button",
                                className: "Quick project" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Quick project")
                                }
                            }, "Market Entry"), S("button", {
                                type: "button",
                                className: "test" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("test")
                                }
                            }, "Fire Testing"))), "Hiring" === this.state.type ? S(m.a.Fragment, null, S("div", {
                                className: "form-group container-5"
                            }, S(N, {
                                header: "Type of Certification",
                                subHeader: "Which category applies to you?"
                            }), S("div", {
                                className: "grid grid-cols-1 md:grid-cols-3"
                            }, ["New Product Certificate", "Certificate Renewal", "Change in existing Certificate"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfTalent === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfTalent", t)
                                    }
                                }, t)
                            })))), S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Type of Product or System",
                                subHeader: "What is your product or system? "
                            }), S("div", {
                                className: "grid grid-cols-2 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfHiring === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfHiring", t)
                                    }
                                }, t)
                            })))), S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Type of Application",
                                subHeader: "What are the intended uses for the product or system? "
                            }), S("div", {
                                className: "grid grid-cols-2 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.noOfTalents === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("noOfTalents", t)
                                    }
                                }, t)
                            })))), S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Product Standard",
                                subHeader: "Which standards are you interested in? "
                            }), S("div", {
                                className: "grid grid-cols-2 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.duration === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("duration", t)
                                    }
                                }, t)
                            }))))) : "", "Co-develop product" === this.state.type ? S(m.a.Fragment, null, S("div", {
                                className: "form-group container-5"
                            }, S(N, {
                                header: "Types of project",
                                subHeader: "What does your product need help with?"
                            }), S("div", {
                                className: "grid grid-cols-1 md:grid-cols-3"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfProject === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfProject", t)
                                    }
                                }, t)
                            }))))) : "", "Quick project" === this.state.type ? S(m.a.Fragment, null, S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Types of Solutions",
                                subHeader: "What area does it fall under?"
                            }), S("div", {
                                className: "grid grid-cols-3 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfSolution === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfSolution", t)
                                    }
                                }, t)
                            }))))) : "", S("div", {
                                className: "form-group container-5 mb-40p"
                            }, S(N, {
                                header: "Budget",
                                subHeader: "Move the slider to set your budget range"
                            }), S("div", {
                                className: "slider-wrapper"
                            }, S("div", {
                                className: "box"
                            }, S("div", {
                                className: "values text-center"
                            }, S("div", null, S("span", {
                                id: "first"
                            })), "- ", S("div", null, S("span", {
                                id: "second"
                            }))), S("div", {
                                className: "slider",
                                "data-value-0": "#first",
                                "data-value-1": "#second",
                                "data-range": "#third"
                            })))), S("div", {
                                className: "form-group container-5 mb-40p"
                            }, S(N, {
                                header: "Contact Information",
                                subHeader: "Fill in your personal details!",
                                required: !0
                            }), S("div", {
                                className: "grid grid-cols-1 md:grid-cols-2"
                            }, S("input", {
                                type: "text",
                                id: "name",
                                placeholder: "First Name",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "text",
                                id: "last",
                                placeholder: "Last Name",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "text",
                                id: "company",
                                placeholder: "Company",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("div", {
                                className: "select-container"
                            }, S(T, {
                                options: w.a,
                                placeholder: "Country",
                                parentCallback: function(t) {
                                    return e.setState({
                                        country: t.label
                                    })
                                }
                            })), S("input", {
                                type: "email",
                                id: "email",
                                placeholder: "Email",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "text",
                                id: "phone",
                                placeholder: "Phone Number",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), "Hiring" === this.state.type ? S("textarea", {
                                className: "md:col-span-2",
                                name: "",
                                id: "detail",
                                placeholder: "What else should we know about your project?",
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }) : "", "Co-develop product" === this.state.type ? S("textarea", {
                                className: "md:col-span-2",
                                name: "",
                                id: "detail",
                                placeholder: "What else should we know about your project?",
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }) : "", "Quick project" === this.state.type ? S("textarea", {
                                className: "md:col-span-2",
                                name: "",
                                id: "detail",
                                placeholder: "What else should we know about your project?",
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }) : "")), S("div", {
                                className: "text-center"
                            }, S("button", {
                                type: "submit",
                                className: "btn mx-auto block"
                            }, S("span", {
                                className: "arrow arrow-left"
                            }), S("span", {
                                className: "btn-text"
                            }, "Submit"), S("span", {
                                className: "arrow arrow-right"
                            })))))))
                        }
                    }]), n
                }(h.Component)
        }
    },
    [
        ["Enfu", 1, 0, 2, 3, 5, 10]
    ]
]);</script>
            <footer class="">
                <div class="grid grid-cols-1 md:grid-cols-4">
                    <div class="col">
                        <div class="head">Company</div>
                        <a class="effect-link false" href="#"><span>About</span></a><a class="effect-link false" href="#"><span>Blog</span></a>
                        <a class="effect-link false" href="#"><span>Clients</span></a><a class="effect-link false" href="#"><span>How it works</span></a>
                    </div>
                    <div class="col">
                        <div class="head">Services</div>
                        <a class="effect-link false" href="#"><span>Certification</span></a><a class="effect-link false" href="#"><span>Fire Testing</span></a>
                        <a class="effect-link false" href="#"><span>Reaction to Fire</span></a>
                        <a class="effect-link false" href="#"><span>Fire Resistance</span></a>
                        <a class="effect-link false" href="#"><span>Façade</span></a>
                        <a class="effect-link false" href="#"><span>Market Access </span></a>
                    </div>
                    <div class="col">
                        <div class="head">Get in touch</div>
                        <a class="effect-link false" href="https://appslink-me.com/esl/contact.html"><span>Contact us</span></a>
                        <a class="effect-link false" href="#">
                            <span>Apply as a talent</span>
                        </a>
                        <a class="email" href="mailto:ask@emirateslaboratory.com">ask@emirateslaboratory.com</a>
                    </div>
                    <div class="col">
                        <div class="head">Top Demand</div>
                        <a href="#">Product Certification</a>
                        <a href="#">Fire Testing</a>
                        <a href="#">Façade</a>
                        <a href="#">Reaction to Fire </a>
                        <a href="#">Doors</a>

                    </div>
                </div>
                <div class="bottom flex-wrap flex justify-between">
                    <a href="#">
                        <img src="./assets/img/logo.png" alt="" />
                    </a>
                    <div class="right">
                        <a href="#">Privacy Policy</a>
                        <span class="copyright">© Copyright 2021 ESL.</span>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <!--<script async="" src="./Get started_files/js"></script>-->
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag("js", new Date());
        gtag("config", "UA-177306561-1");
    </script>
    <script></script>
    <script id="__NEXT_DATA__" type="application/json">
        {
        "props": {
        "pageProps": {
        "query": {}
        }
        },
        "page": "/get-started",
        "query": {},
        "buildId": "jY7IilaLBJDSnuzuz9m-U",
        "isFallback": false,
        "gip": true
        }
    </script>
    <script src="<?= $this->Html->url('/') ?>assets/js/main.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/webpack.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/framework.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/28b622a773b12a3bae544346f6679af547b33837.60a5ec64785b3bcadd35.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/1630b8ba2414b4a0748ef6348a6fc6889a3080c1.0f5c44739c26b77ab067.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/d94f8a974fb3043bb063861b915c485f03f8280d.fbe6c10e6310cfd3e0e5.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/file1.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/file2.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/file3.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/file4.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/file5.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/app.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/menu_contact.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/file7.js" async=""></script>
    
    <script src="<?= $this->Html->url('/') ?>assets/js/buildManifest.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/ssgManifest.js" async=""></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/slick.min.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/aos.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/TweenMax.min.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/ScrollMagic.min.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/animation.gsap.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/jquery-ui.min.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/priceSlider.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/easings.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/demo3.js"></script>
    <script src="<?= $this->Html->url('/') ?>assets/js/common.js"></script>

</body>

</html>