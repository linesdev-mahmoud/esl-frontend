<script language="javascript" src="<?php echo $this->Html->url("/js/fckjs/ckeditor_OK/ckeditor.js"); ?>"></script>
<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <?php 
        $post = 'News';
        if($type==1){
            $post = 'Pages';
        }?>
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?php   
                                                        if($type==2){echo $this->Html->url("/apages/posts");}
                                                        else {echo $this->Html->url("/apages");}?>"><?=$post?></a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Edit</h3>
                </div>
                <div class="card-body">
                    <?php echo $this->Form->create('Apage', array('type'=>'file','class' => 'form-horizontal form-material'));?>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">Title</label>
                                <?php
		echo $this->Form->input('id', array('value'=>$id,'type'=>'hidden'));
		echo $this->Form->input('type', array('value'=>$type,'type'=>'hidden'));
		$readonly = '';
		if($type==1){
		    $readonly = 'readonly';
		}
		echo $this->Form->input('title', array('value'=>$this->data['Apage']['title'],'label'=>false,'class' => 'form-control form-control-line','readonly'=>$readonly));
	?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">Title - arabic</label>
                                <?php
		$readonly = '';
		if($type==1){
		    $readonly = 'readonly';
		}
		echo $this->Form->input('title_ar', array('value'=>$this->data['Apage']['title_ar'],'label'=>false,'class' => 'form-control form-control-line','readonly'=>$readonly));
	?>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Content</label>
                                <?php
		echo $this->Form->input('content', array('value'=>$this->data['Apage']['content'],'label'=>false,'class' => 'form-control form-control-line','required'=>'required'));
		echo $fck->load("Apage.content");
	?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Content - arabic</label>
                                
                                <?php
		echo $this->Form->input('content_ar', array('label'=>false,'class' => 'form-control form-control-line'));
		echo $fck->load("Apage.content.ar");
	?> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        
                        
                        <?php if($type == 2){?>
                            <label for="exampleInputnumber">Image</label>
                       <?php     
		echo $this->Form->input('image1', array('label'=>false,'class' => 'form-control form-control-line','type'=>'file'));
		echo $this->Form->input('image', array('type'=>'hidden')); }?>
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                       <input type="submit" class="btn btn-success mt-1" value="Save">
                </div>
            </div>
        </div>
    </div>
    <!-- row end -->

</div>



<!--<div class="page-wrapper">
    <div class="container-fluid">
        <h1><?php __('Edit'); ?></h1>
        <div class="card">
    <div class="card-block">
        <?php echo $this->Form->create('Apage', array('class' => 'form-horizontal form-material','type'=>'file'));?>
        <div class="form-group">
	<?php
		echo $this->Form->input('id', array('value'=>$id,'type'=>'hidden'));
		echo $this->Form->input('type', array('value'=>$type,'type'=>'hidden'));
		$readonly = '';
		if($type==1){
		    $readonly = 'readonly';
		}
		echo $this->Form->input('title', array('value'=>$this->data['Apage']['title'],'label'=>__('Title', true),'class' => 'form-control form-control-line','readonly'=>$readonly));
	?>
        </div>
        
        
      
              <div class="form-group">
	<?php
		$readonly = '';
		if($type==1){
		    $readonly = 'readonly';
		}
		echo $this->Form->input('title_ar', array('value'=>$this->data['Apage']['title_ar'],'label'=>__('Title - arabic', true),'class' => 'form-control form-control-line','readonly'=>$readonly));
	?>
        </div>
        
        
        <div class="form-group">
	<?php
		echo $this->Form->input('content', array('value'=>$this->data['Apage']['content'],'label'=>__('Content', true),'class' => 'form-control form-control-line','required'=>'required'));
		echo $fck->load("Apage.content");
	?>
        </div>
        
        
   <div class="form-group">
	<?php
		echo $this->Form->input('content_ar', array('value'=>$this->data['Apage']['content_ar'],'label'=>__('Content', true),'class' => 'form-control form-control-line','required'=>'required'));
		echo $fck->load("Apage.content.ar");
	?>
        </div>
        
        
        <?php if($type == 2){?>
        <div class="form-group">
            
	<?php
		echo $this->Form->input('image1', array('label'=>__('Image', true),'class' => 'form-control form-control-line','type'=>'file'));
		echo $this->Form->input('image', array('type'=>'hidden'));
	?>
                       
        </div>
        <?php }?>
        
        
        <button style="margin-top:20px;" class="btn btn-success"><?= __('Save') ?></button>
            </div>
        </div>
        
    </div>
</div>-->




<!--<link rel="stylesheet" href="<?php echo $this->Html->url('/css/'); ?>colorbox.css" />
<script src="<?php echo $this->Html->url('/'); ?>js/fckjs/jquery.colorbox.js"></script>

<script src="<?php echo $this->Html->url('/'); ?>js/fckjs/jquery.validation.js"></script>-->







<!--<div class="apages form">
<?php echo $this->Form->create('Apage');?>
	<fieldset>
		<legend><?php __('Edit Apage'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('content');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Apage.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Apage.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Apages', true), array('action' => 'index'));?></li>
	</ul>
</div>