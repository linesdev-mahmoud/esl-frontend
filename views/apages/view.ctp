<div class="Content blog_page post_page">
    <div class="container">    
        <div class="styles__Banner-sc-1k525v7-0 gPRDhv">
            <div class="styles__BannerImage-sc-1k525v7-3 bUfuRq" style="background-image: url(<?=$admin_url.$apage['Apage']['image']?>);">
                
            </div>
        </div>
        <div class="row">
            <?php 
                $date = date("Y-m-d",strtotime($apage['Apage']['created']));
                
                
                ?>
            <div class="col-sm-12 col-xs-12 col-lg-12 styles__Item">
                <p class="styles__Time"><?=$date?></p>
                <div class="styles__Divider"></div>
                <div class="styles__ContentWrapper">
                    <div class="styles__Wrapper-sc-18gpvzo-0 fBEOfW">
                        <?php 
                        $title = str_replace(" ", "-", $apage['Apage']['title']);
                        ?>
                        <a class="share-button social-share-button" href="<?=$tw_share_url.'post/'.$apage['Apage']['id'] . '/' . $title?>">
        <div class="share-button-secondary">
            <div class="share-button-secondary-content">share on twitter</div>
        </div>
        <div class="share-button-primary"><img src="<?=$this->Html->url('/assets/img/twitter.svg')?>" class="share-button-icon" alt="error"></div>
    </a>
    <a class="share-button social-share-button" href="<?=$fb_share_url.'post/'.$apage['Apage']['id'] . '/' . $title?>">
        <div class="share-button-secondary">
            <div class="share-button-secondary-content">share on facebook</div>
        </div>
        <div class="share-button-primary"><img src="<?=$this->Html->url('/assets/img/facebook.svg')?>" class="share-button-icon" alt="error"></div>
    </a>
    <a class="share-button social-share-button" href="<?=$pin_share_url.'post/'.$apage['Apage']['id'] . '/' . $title?>">
        <div class="share-button-secondary">
            <div class="share-button-secondary-content">pin on pinterest</div>
        </div>
        <div class="share-button-primary"><img src="<?=$this->Html->url('/assets/img/pinterest.svg')?>" class="share-button-icon" alt="error"></div>
    </a>
    <a class="share-button social-share-button" href="mailto:?subject=<?=$title?>?&amp;body=<?=$custom_url.'post/'.$apage['Apage']['id'] . '/' . $title?>">
        <div class="share-button-secondary">
            <div class="share-button-secondary-content">email</div>
        </div>
        <div class="share-button-primary"><img src="<?=$this->Html->url('/assets/img/email.svg')?>" class="share-button-icon" alt="error"></div>
    </a>
</div>
                        <div class="title-text">
                            <?=$apage['Apage']['title'.$_ar]?>
                        </div>
                    
                    <div class="styles__Divider"></div>
                    <p class="styles__ShortDescription">
                        <?=$apage['Apage']['content'.$_ar]?>
                    </p>
                </div>
                
            </div>
           
            
            
        </div>
        
    </div>
</div>
