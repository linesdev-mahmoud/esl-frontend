<div class="Content blog_page">
    <div class="container">    
        <div class="styles__Banner-qpvbba-2 hQhVBL">
            <div class="styles__BannerTextWrapper-qpvbba-3 dqpqpY">
                <div class="styles__BannerTitle-qpvbba-5 fQUIsU"><?=__('ESL ')?></div>
                <div class="styles__BannerTitle-qpvbba-5 fQUIsU"><?=__('Insights')?></div>
            </div>
            <div class="styles__BannerImage-qpvbba-4 fejQrV" style="background-image: url(<?= $this->Html->url('/') ?>blog-banner-2.png);">
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($apages as $post) {
                $date = date("Y-m-d", strtotime($post['Apage']['created']));

                $title = str_replace(" ", "-", $post['Apage']['title']);
                ?>
                <div class="col-sm-6 col-xs-12 col-lg-4 styles__Item">
                    <a href="<?= $this->Html->url('/post/') . $post['Apage']['id'] . '/' . $title ?>" class="styles__ImageWrapper">
                        <img src="<?= $admin_url . $post['Apage']['image'] ?>" class="img-responsive" style="width:100%" alt="Image">
                    </a>

                    <p class="styles__Time"><?= $date ?></p>
                    <div class="styles__Divider"></div>
                    <div class="styles__ContentWrapper">
                        <a href="<?= $this->Html->url('/post/') . $post['Apage']['id'] . '/' . $title ?>" class="styles__Title">
                            <div class="title-text">
                                <?= $post['Apage']['title' . $_ar] ?>
                            </div>
                        </a>
                        <p class="styles__ShortDescription">
                            <?= $post['Apage']['short_content' . $_ar] ?>
                        </p>
                    </div>
                    <div class="styles__Profile">
                        <img src="<?= $this->Html->url('/') ?>cu-talent_profile.png" alt="erorr" class="styles__ProfileImage">
                        <div class="styles__ProfileContent">
                            <div class="styles__ProfileName"><?=__('Written by')?></div>
                            <div class="styles__ProfilePosition"><?= $users[$post['Apage']['modified_by']] ?></div>
                        </div>
                    </div>
                    <div class="styles__Divider"></div>
                </div>
            <?php } ?>


        </div>
        <nav aria-label="..." class="news_paginate">
            <div class="paging text-center">
                <div class="custom_paging">
                    <?php echo $this->Paginator->prev('' . __('previous', true), array(), null, array('class' => 'disabled page-item')); ?>
                    <?php echo $this->Paginator->numbers(array('class' => '', 'first' => false, 'last' => false,'separator'=>' ')); ?>

                    <?php echo $this->Paginator->next(__('next', true) . '', array(), null, array('class' => 'disabled page-item')); ?>
                </div>
            </div>
        </nav>
        <!--<nav aria-label="..." class="news_paginate">
            <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item active">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>-->
    </div>
</div>
<script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
