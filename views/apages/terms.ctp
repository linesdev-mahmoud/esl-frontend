<section class="page-title-area" style="background-image: url(assets/img/bg/page-title-bg-01.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <div class="page-title text-center">
                            <h1>Blog Details</h1>
                            <div class="breadcrumb">
                                <ul class="breadcrumb-list">
                                    <li><a href="index.html">Home <i class="far fa-chevron-right"></i></a></li>
                                    <li><a class="active" href="#">Blog Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->

        <!-- blog-area-start -->
        <div class="blog--details-area grey-bg pt-130 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-12">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="assets/img/blog/14.jpg" alt=""></a>
                        </div>
                        <div class="blogs-details-left-area white-bg">
                            <div class="blogs pos-rel">
                                <div class="blog-text">
                                    <span class="blog-tag"><a href="#">design</a></span>
                                    <h3><a href="blog-details.html">Inspire Design Decision With Ott Storch When Idea How To Space And Kern Punctuation</a></h3>
                                    <div class="blog-meta">
                                        <span><i class="far fa-user-circle"></i> <a href="blog-details.html">By Somalia Doe</a></span>
                                        <span><i class="far fa-calendar-alt"></i> <a href="blog-details.html">25 Nov 2020</a></span>
                                        <span><i class="far fa-comments"></i> <a href="blog-details.html">Com(30)</a></span>
                                        <span><i class="far fa-heart"></i> <a href="blog-details.html">Like (1K)</a></span>
                                    </div>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta suntexplicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullacorporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur vel illum qui dolorem </p>
                                </div>
                            </div>
                            <div class="blogs-list-content mb-35">
                                <h4>Why Needs Education</h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem enim ad minima veniam</p>
                                <div class="blogs-list-img mb-40">
                                    <img src="assets/img/blog/15.jpg" alt="">
                                </div>
                                <ul class="list-text">
                                    <li>Inspired Design Decisions With Neville Brody: Design Cannot Remain Neutral</li>
                                    <li>Avoid Keyboard Event-Related Bugs In Browser-Based Transliteration</li>
                                    <li>Embrace The Possibilities (March 2020 Wallpapers)</li>
                                </ul>
                            </div>
                            <div class="blogs-list-content blogs-list-02">
                                <h4>Best Articles For Web Design</h4>
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem enim ad minima veniam</p>
                            </div>
                            <div class="blog-quote blog-quote-details theme-bg2 mb-35">
                                <h4>How To Build An Accessible Front-End Application With Chakra UI And Nuxt.js</h4>
                                <h6 class="line">Norman S. Roberts</h6>
                            </div>
                            <div class="blog-post-tag mb-55">
                                <div class="row tag-border">
                                    <div class="col-xl-6 col-lg-7 col-md-7">
                                        <div class="post-tag-list">
                                            <span>Tags :</span>
                                            <a href="#">Business Cart,</a>
                                            <a href="#">Design & Branding</a>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-5 col-md-5">
                                        <div class="post-share-icon text-right">
                                            <span>Share :</span>
                                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#"><i class="fab fa-twitter"></i></a>
                                            <a href="#"><i class="fab fa-youtube"></i></a>
                                            <a href="#"><i class="fab fa-instagram"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blogs-authors grey-bg mb-60">
                                <div class="authors-avatar"><img src="assets/img/blog/author1.png" alt=""></div>
                                <div class="authors-content">
                                    <h4>Eugene A. Pearson</h4>
                                    <p>But must explain to you how all this mistake idea deounci
                                        asure and praising pain was born and will give you comete
                                        account the system, and expound the actual teach</p>
                                    <div class="author-icon">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="related-post mb-55">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-12 col-md-6">
                                        <div class="post d-flex align-items-center">
                                            <div class="post-thumb"><img src="assets/img/blog/post5.jpg" alt=""></div>
                                            <div class="post-content">
                                             <h6><a href="blog-details.html">Mixing Tangible & Intan
                                                Interfaces Using</a></h6>
                                                <div class="post-meta">
                                                    <span><i class="fal fa-user"></i> Soamlia</span>
                                                    <span><i class="far fa-calendar-alt"></i> 05 Aug 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-12 col-md-6">
                                        <div class="post d-flex align-items-center">
                                            <div class="post-thumb"><img src="assets/img/blog/post6.jpg" alt=""></div>
                                            <div class="post-content">
                                             <h6><a href="blog-details.html">Mixing Tangible & Intan
                                                Interfaces Using</a></h6>
                                                <div class="post-meta">
                                                    <span><i class="fal fa-user"></i> Soamlia</span>
                                                    <span><i class="far fa-calendar-alt"></i> 05 Aug 2020</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post-comments mb-55">
                                <h3 class="post-comments-title mb-35">People’s Comments</h3>
                                <ul class="latest-post">
                                    <li>
                                        <div class="comments-box mb-25">
                                            <div class="commnets-avatar"><img src="assets/img/blog/comment1.png" alt=""></div>
                                            <div class="comments-text fix">
                                                <div class="avatar-name">
                                                    <h6>David Angel Makel</h6>
                                                    <span>IT Consultant</span>
                                                    <a class="reply" href="#">Reply Comments <i class="fal fa-long-arrow-right"></i></a>
                                                </div>
                                                <p>It is a long established fact that a reader will be distracted by the readable content 
                                                    of page looking at its layout. The point of using Lorem Ipsum  normal</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="children">
                                        <div class="comments-box mb-25">
                                            <div class="commnets-avatar"><img src="assets/img/blog/comment2.png" alt=""></div>
                                            <div class="comments-text fix">
                                                <div class="avatar-name">
                                                    <h6>Michel Rason Roy</h6>
                                                    <span>Product Designer</span>
                                                    <a class="reply" href="#">Reply Comments <i class="fal fa-long-arrow-right"></i></a>
                                                </div>
                                                <p>It is a long established fact that a reader will be distracted by the readable content 
                                                    of page looking at its layout. The point of using Lorem Ipsum  normal</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="comments-box">
                                            <div class="commnets-avatar"><img src="assets/img/blog/comment3.png" alt=""></div>
                                            <div class="comments-text fix">
                                                <div class="avatar-name">
                                                    <h6>David Angel Makel</h6>
                                                    <span>IT Consultant</span>
                                                    <a class="reply" href="#">Reply Comments <i class="fal fa-long-arrow-right"></i></a>
                                                </div>
                                                <p>It is a long established fact that a reader will be distracted by the readable content 
                                                    of page looking at its layout. The point of using Lorem Ipsum  normal</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="post-comments-form">
                                <h3 class="post-comments-title pl-15 mb-35">Reply Comments</h3>
                                <form action="" class="subscribe contact-post-form">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="input-text">
                                                <input class="form-control" type="text" placeholder="Full Name Here">
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="input-text email-text">
                                                <input class="form-control" type="text" placeholder="Email Here">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="input-text message-text">
                                                <textarea name="" id="" cols="30" rows="10" placeholder="Comments"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="lg-btn lg-btn-02">
                                                <a class="c-btn" href="#">send comments <i
                                                    class="far fa-long-arrow-alt-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12">
                        <div class="right-blog">
                            <div class="widget-search white-bg mb-30">
                                <h4 class="widget-ttile-02">Keywords</h4>
                                <form class="slider-search-form">
                                    <input type="text" placeholder="Search Courses">
                                    <button type="submit"><i class="far fa-search"></i></button>
                                </form>
                            </div>
                            <div class="widget-list mb-25">
                                <ul class="cat-list">
                                    <li><a href="#">monta ektu thanda hoise <span class="f-right"><i class="far fa-chevron-right"></i></span></a></li>
                                    <li><a href="#">Computer Engineering <span class="f-right"><i class="far fa-chevron-right"></i></span></a></li>
                                    <li><a href="#">Business Studies <span class="f-right"><i class="far fa-chevron-right"></i></span></a></li>
                                    <li><a href="#">Medical & Health <span class="f-right"><i class="far fa-chevron-right"></i></span></a></li>
                                    <li><a href="#">Web Development <span class="f-right"><i class="far fa-chevron-right"></i></span></a></li>
                                    <li><a href="#">Learning English <span class="f-right"><i class="far fa-chevron-right"></i></span></a></li>
                                </ul>
                            </div>
                            <div class="widget-post white-bg mb-25">
                                <h4>Recent News</h4>
                                <ul class="post-list">
                                   <li>
                                       <div class="post d-flex align-items-center">
                                           <div class="post-thumb"><img src="assets/img/blog/post1.jpg" alt=""></div>
                                           <div class="post-content">
                                            <h6><a href="blog-details.html">Smaing Podcast Epis
                                                Ode 15 With Phile</a></h6>
                                                <span><i class="far fa-chevron-right"></i> By Silva Hola</span>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <div class="post d-flex align-items-center">
                                           <div class="post-thumb"><img src="assets/img/blog/post2.jpg" alt=""></div>
                                           <div class="post-content">
                                            <h6><a href="blog-details.html">Smaing Podcast Epis
                                                Ode 15 With Phile</a></h6>
                                                <span><i class="far fa-chevron-right"></i> By Silva Hola</span>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <div class="post d-flex align-items-center">
                                           <div class="post-thumb"><img src="assets/img/blog/post3.jpg" alt=""></div>
                                           <div class="post-content">
                                            <h6><a href="blog-details.html">Smaing Podcast Epis
                                                Ode 15 With Phile</a></h6>
                                                <span><i class="far fa-chevron-right"></i> By Silva Hola</span>
                                           </div>
                                       </div>
                                   </li>
                                   <li>
                                       <div class="post d-flex align-items-center">
                                           <div class="post-thumb"><img src="assets/img/blog/post4.jpg" alt=""></div>
                                           <div class="post-content">
                                               <h6><a href="blog-details.html">Smaing Podcast Epis
                                                Ode 15 With Phile</a></h6>
                                                <span><i class="far fa-chevron-right"></i> By Silva Hola</span>
                                           </div>
                                       </div>
                                   </li>
                                </ul>
                            </div>
                            <div class="widget-post white-bg mb-30">
                                <h4>Photo Gallery</h4>
                                <ul class="instafeed">
                                   <li>
                                       <a href="#">
                                           <img src="assets/img/blog/insta1.jpg" alt="">
                                           <span class="insta-icon"><i class="fab fa-instagram"></i></span>
                                       </a>
                                   </li>
                                   <li>
                                       <a href="#">
                                           <img src="assets/img/blog/insta2.jpg" alt="">
                                           <span class="insta-icon"><i class="fab fa-instagram"></i></span>
                                       </a>
                                   </li>
                                   <li>
                                       <a href="#">
                                           <img src="assets/img/blog/insta3.jpg" alt="">
                                           <span class="insta-icon"><i class="fab fa-instagram"></i></span>
                                       </a>
                                   </li>
                                   <li>
                                       <a href="#">
                                           <img src="assets/img/blog/insta4.jpg" alt="">
                                           <span class="insta-icon"><i class="fab fa-instagram"></i></span>
                                       </a>
                                   </li>
                                   <li>
                                       <a href="#">
                                           <img src="assets/img/blog/insta5.jpg" alt="">
                                           <span class="insta-icon"><i class="fab fa-instagram"></i></span>
                                       </a>
                                   </li>
                                   <li>
                                       <a href="#">
                                           <img src="assets/img/blog/insta6.jpg" alt="">
                                           <span class="insta-icon"><i class="fab fa-instagram"></i></span>
                                       </a>
                                   </li>
                                </ul>
                            </div>
                            <div class="widget-post white-bg mb-30">
                                <h4>Photo Gallery</h4>
                                <div class="cat-tag">
                                   <a href="#">Design</a>
                                   <a href="#">Portfolio</a>
                                   <a href="#">Energy</a>
                                   <a href="#">Resume</a>
                                   <a href="#">Gallery</a>
                                   <a href="#">Renewable</a>
                                </div>
                            </div>
                            <div class="widget-add mb-30">
                               <img src="assets/img/course/add-01.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>