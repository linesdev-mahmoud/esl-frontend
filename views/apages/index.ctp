<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?= $this->Html->url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pages</li>
        </ol><!-- End breadcrumb -->
        <div class="ml-auto">
            <div class="input-group">
                <!--<a href="<?= $this->Html->url('/apages/add') ?>" class="btn btn-secondary padbtn"><i class="fe fe-plus mr-1"></i>Add New</a>-->

            </div>
        </div>
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="card-title">Pages</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table  class="table table-striped table-bordered text-nowrap w-100">
                            <thead>
                                <tr>
                                    <th class="wd-15p"><?php echo $this->Paginator->sort('#', 'id'); ?></th>
                                    <th class="wd-15p"><?php echo $this->Paginator->sort('Title', 'title'); ?></th>
                                    <th class="wd-15p"><?php echo $this->Paginator->sort('Modified by', 'modified_by'); ?></th>
                                    <th class="wd-20p">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($apages as $apage):
                                    $class = null;
                                    if ($i++ % 2 == 0) {
                                        $class = ' class="altrow"';
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $apage['Apage']['id']; ?>&nbsp;</td>
                                        <td><?php echo $apage['Apage']['title']; ?>&nbsp;</td>
                                        <td><?php echo $users[$apage['Apage']['modified_by']]; ?>&nbsp;</td>
                                        <td class="actions">
                                            <div class="input-group">
                                                <?=
                                                $this->Html->link(
                                                        '<span><i class="si si-pencil"></i></span>',
                                                        ['action' => 'edit', $apage['Apage']['id'] . '/' . $apage['Apage']['type']],
                                                        ['escape' => false, 'class' => 'btn btn-yellow text-white mr-2 btn-sm', 'data-toggle' => 'tooltip', 'data-placement' => "top", 'data-original-title' => 'Edit']
                                                )
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
<?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
            <!-- section-wrapper -->
        </div>
    </div>
    <!-- row end -->

</div>
<!-- Right-sidebar-->
