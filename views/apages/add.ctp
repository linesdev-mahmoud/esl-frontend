<script language="javascript" src="<?php echo $this->Html->url("/js/fckjs/ckeditor_OK/ckeditor.js"); ?>"></script>

<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/apages"); ?>">News</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add News</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add News</h3>
                </div>
                <div class="card-body">
                    <?php echo $this->Form->create('Apage', array('type'=>'file','class' => 'form-horizontal form-material'));?>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">Title</label>
                                <?php
		echo $this->Form->input('title', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname">Title - arabic</label>
                                <?php
		echo $this->Form->input('title_ar', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required'));
	?>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Content</label>
                                
                                <?php
		echo $this->Form->input('content', array('label'=>false,'placeholder'=>'Enter Content','class' => 'form-control form-control-line'));
		echo $fck->load("Apage.content");
	?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleInputname1">Content - arabic</label>
                                
                                <?php
		echo $this->Form->input('content_ar', array('label'=>false,'class' => 'form-control form-control-line'));
		echo $fck->load("Apage.content.ar");
	?> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputnumber">Image</label>
                        
                        <?php
		echo $this->Form->input('image', array('label'=>false,'class' => 'form-control form-control-line','type'=>'file'));
	?>
                    </div>
                    
                </div>
                <div class="card-footer text-right">
                   
                       <input type="submit" class="btn btn-success mt-1" value="Save">
                </div>
            </div>
        </div>
    </div>
    <!-- row end -->

</div>




<!--<div class="page-wrapper">
    <div class="container-fluid">
<h1><?php __('Add'); ?></h1>
        <div class="card">
    <div class="card-block">
        
        <?php echo $this->Form->create('Apage', array('type'=>'file','class' => 'form-horizontal form-material'));?>
        
        <div class="form-group">
            
	<?php
		echo $this->Form->input('title', array('label'=>__('Title', true),'class' => 'form-control form-control-line','required'=>'required'));
	?>
                       
        </div>
        
        
        
        <div class="form-group">
            
	<?php
		echo $this->Form->input('title_ar', array('label'=>__('Title - arabic', true),'class' => 'form-control form-control-line','required'=>'required'));
	?>
                       
        </div>
        
        
         <div class="form-group">
            
	<?php
		echo $this->Form->input('content', array('label'=>__('Content', true),'class' => 'form-control form-control-line'));
		echo $fck->load("Apage.content");
	?>            
        </div>
        
        <div class="form-group">
            
	<?php
		echo $this->Form->input('content_ar', array('label'=>__('Content - arabic', true),'class' => 'form-control form-control-line'));
		echo $fck->load("Apage.content.ar");
	?>            
        </div>
        
        
        <div class="form-group">
            
	<?php
		echo $this->Form->input('image', array('label'=>__('Image', true),'class' => 'form-control form-control-line','type'=>'file'));
	?>
                       
        </div>
        
        
        <button style="margin-top:20px;" class="btn btn-success"><?= __('Save') ?></button>
            </div>
        </div>

    </div>
</div>-->












