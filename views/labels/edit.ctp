<div class="labels form">
<?php echo $this->Form->create('Label');?>
	<fieldset>
		<legend><?php __('Edit Label'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('application_id');
		echo $this->Form->input('orientation');
		echo $this->Form->input('size1');
		echo $this->Form->input('size2');
		echo $this->Form->input('quantity');
		echo $this->Form->input('collection_delivery_pain');
		echo $this->Form->input('office_address');
		echo $this->Form->input('full_name');
		echo $this->Form->input('contact_number');
		echo $this->Form->input('status');
		echo $this->Form->input('modified_by');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Label.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Label.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Labels', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Applications', true), array('controller' => 'applications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Application', true), array('controller' => 'applications', 'action' => 'add')); ?> </li>
	</ul>
</div>