<div class="container" style="margin-top:30px">
    <div class="row">
        <div class="col-sm-7">
            <h3>Application for review of</h3>
            <h3>international conformity certificate</h3>

        </div>
        <div class="col-sm-5">
            <img src="<?= $this->Html->url('/') ?>webroot/img/applicationback.png">
        </div>
        <div class="row">
            <div class="col-sm-9">
                <h2>About Me</h2>
                  <?php echo $this->Form->create('Label', array('type'=>'file','class' => 'form-horizontal form-material'));?>

                    <div class="row labfiled">
                        <div class="col-sm-12">
                            <label>Select orientation:</label>
                                <?php
		echo $this->Form->input('orientation', array('div'=>false,'label'=>false,'placeholder'=>'Enter Name','class' => 'form-control selectstyle','options'=>$orintation));
	?>
                            
                        </div>
                        </div>
                    <div class="row labfiled">
                        <div class="col-sm-12">
                            
                            <label>Kindly provide dimensions in millimetre of the product area available to apply the label:</label>
                                <?php
		echo $this->Form->input('size1', array('div'=>false,'label'=>false,'placeholder'=>'Enter Name','class' => 'labelsize','options'=>$numbers));
	?><span>X</span>
                                <?php
		echo $this->Form->input('size2', array('div'=>false,'label'=>false,'placeholder'=>'Enter Name','class' => 'labelsize','options'=>$numbers));
	?><span>mm</span>
                        </div>
                    </div>
                    <div class="row labfiled">
                        <div class="col-sm-12">
                            <label>Where on the product will you apply ESL Certification label ?</label>
                                <?php
		echo $this->Form->input('quantity', array('div'=>false,'label'=>false,'placeholder'=>'Enter Name','class' => 'form-control selectstyle','options'=>$quantity));
	?>
                        </div>
                    </div>
                    <div class="row labfiled">
                        <div class="col-sm-12">
                             <label>Collection-Delivery-Point:</label>
                                <?php
		echo $this->Form->input('collection_delivery_pain', array('label'=>false,'id'=>'colldel','placeholder'=>'Enter Name','class' => 'selectstyle','options'=>$colldelevery,'onchange'=>'CollectionDelivery()'));
	?>
                        </div>
                        </div>
                    <div class="row" id="hiddenfields" style="display:none">
                        <div class="col-sm-6">
                            <label >Enter full office address:</label>
                            <?php
		echo $this->Form->input('office_address', array('label'=>false,'placeholder'=>'Enter Name','class' => 'form-control'));
	?>
                        </div>
                        <div class="col-sm-6">
                            <label >Enter full name:</label>
                            <?php
		echo $this->Form->input('full_name', array('label'=>false,'placeholder'=>'Enter Name','class' => 'form-control'));
	?>
                        </div>
                        <div class="col-sm-6">
                            <label >Enter contact number:</label>
                            <?php
		echo $this->Form->input('contact_number', array('label'=>false,'placeholder'=>'Enter Name','class' => 'form-control'));
	?>
                        </div>
                        <div class="col-sm-6">
                            <label >Applicable shipping fee:</label>
                            <?php
		echo $this->Form->input('Applicable', array('label'=>false,'readonly'=>'readonly','placeholder'=>'Enter Name','class' => 'form-control'));
	?>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <input type="submit" value="Save(Complete data)">
                        </div>
                        </div>
                    
            </div>

        </div>
    </div>
    
    <script>
         if($('#colldel').val()==2){
            $('#hiddenfields').show();
        }
        else{
            $('#hiddenfields').hide();
        }
    function CollectionDelivery(){
        if($('#colldel').val()==2){
            $('#hiddenfields').show();
        }
        else{
            $('#hiddenfields').hide();
        }
    }
    </script>

    <style>
        label{
            color: #2ba78a;
        }
        input.text, input.title, input[type=text], input[type=number], input[type=submit],select, textarea {
            margin: 0;
            border-radius: 45px;
            height: 45px;
            padding-left: 20px;
            background: #eeeeee8f;
            margin-top: 15px;
        }
        .selectstyle{
            margin: 0;
            display: inline;
            width: 100%;
            border: 1px solid #bbb;
            border-radius: 45px;
            height: 45px;
            padding-left: 20px;
            background: #eeeeee8f;
            margin-top: 15px;
        }
        .labelsize{
            margin: 0;
            display: inline;
            width: 46%;
            border: 1px solid #bbb;
            border-radius: 45px;
            height: 45px;
            padding-left: 20px;
            background: #eeeeee8f;
            margin-top: 15px;
        }
        span{
            margin: 6px;
        }
        .labfiled label{
            margin-bottom: 0px
        }
        .labfiled{
                margin-top: 30px;
        }
    </style>