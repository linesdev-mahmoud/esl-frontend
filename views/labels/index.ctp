<div class="labels index">
	<h2><?php __('Labels');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('application_id');?></th>
			<th><?php echo $this->Paginator->sort('orientation');?></th>
			<th><?php echo $this->Paginator->sort('size1');?></th>
			<th><?php echo $this->Paginator->sort('size2');?></th>
			<th><?php echo $this->Paginator->sort('quantity');?></th>
			<th><?php echo $this->Paginator->sort('collection_delivery_pain');?></th>
			<th><?php echo $this->Paginator->sort('office_address');?></th>
			<th><?php echo $this->Paginator->sort('full_name');?></th>
			<th><?php echo $this->Paginator->sort('contact_number');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($labels as $label):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $label['Label']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($label['Application']['title'], array('controller' => 'applications', 'action' => 'view', $label['Application']['id'])); ?>
		</td>
		<td><?php echo $label['Label']['orientation']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['size1']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['size2']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['quantity']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['collection_delivery_pain']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['office_address']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['full_name']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['contact_number']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['status']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['modified_by']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['created']; ?>&nbsp;</td>
		<td><?php echo $label['Label']['modified']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $label['Label']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $label['Label']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $label['Label']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $label['Label']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Label', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Applications', true), array('controller' => 'applications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Application', true), array('controller' => 'applications', 'action' => 'add')); ?> </li>
	</ul>
</div>