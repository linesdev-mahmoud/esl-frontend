<div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb">
            <!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?= $this->Html->url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">faqs</li>
        </ol><!-- End breadcrumb -->
        <div class="ml-auto">
            <div class="input-group">
                <a href="<?= $this->Html->url('/faqs/add') ?>" class="btn pull-right hidden-sm-down btn-success">Add Help Support Answer</a>

            </div>
        </div>
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="card-title">Help Support Answers</div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <form method="post">
                        <table id="example" class="table table-striped table-bordered text-nowrap w-100">
                            <thead>

                                <tr>
                                    <th class="wd-15p">id</th>
                                    <th class="wd-15p">title</th>
                                    <th class="wd-15p">title ar</th>
                                    <th class="wd-15p">answer</th>
                                    <th class="wd-15p">answer_ar</th>
                                    <th class="wd-15p">faqcategory id</th>
                                    <th class="wd-15p">Active?</th>
                                    <th class="wd-15p">created</th>
                                    <th class="wd-15p">modified by</th>
                                    <th class="wd-15p">sorting</th>
                                    <th class="wd-15p">Actions</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
    $i = 0;
                                           foreach ($faqs as $faq):
                                           $class = null;
                                           if ($i++ % 2 == 0) {
                                               $class = ' class="altrow"';
                                           }
                                ?>
                                <tr<?php echo $class;?>>
                                    <td><?php echo $faq['Faq']['id']; ?>&nbsp;</td>
                                    <td><?php echo $faq['Faq']['title']; ?>&nbsp;</td>
                                    <td><?php echo $faq['Faq']['title_ar']; ?>&nbsp;</td>
                                    <td><?php echo $faq['Faq']['answer']; ?>&nbsp;</td>
                                    <td><?php echo $faq['Faq']['answer_ar']; ?>&nbsp;</td>
                                    <td>
                                        <?php echo $this->Html->link($faq['Faqcategory']['title'], array('controller' => 'faqcategories', 'action' => 'view', $faq['Faqcategory']['id'])); ?>
                                    </td>
                                    <td>
                                        <?php if($faq['Faq']['status']==0){?>
                                        <i class="fa fa-times-circle" style="font-size: 30px;color:red"></i>
                                        <?php }else{?>
                                        <i class="fa fa-check-circle" style="font-size: 30px;color:green"></i>
                                        <?php }?>
                                    </td>
                                    <td><?php echo $faq['Faq']['created']; ?>&nbsp;</td>
                                    <td><?php echo $users[$faq['Faq']['modified_by']]; ?>&nbsp;</td>d>
                                    <td>
                                        <select name="sorting[<?=$faq['Faq']['id']?>]">
                                            <?php foreach($sorting as $k=>$v){?>
                                            <option value="<?=$k?>" <?php if($faq['Faq']['sorting']==$v){echo 'selected';}?>><?=$v?></option>
                                            <?php }?>
                                        </select>
                                    </td>
                                    <td class="actions">
                                        <div class="input-group">
<!--
                                            <?= $this->Html->link(
    '<span><i class="fe fe-eye"></i></span>',
    ['action' => 'view', $faq['Faq']['id']],
    ['escape' => false,  'class' => 'btn bg-primary text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'View']
) ?>
-->
                                            <?= $this->Html->link(
    '<span><i class="si si-pencil"></i></span>',
    ['action' => 'edit', $faq['Faq']['id']],
    ['escape' => false,  'class' => 'btn btn-yellow text-white mr-2 btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Edit']
) ?>
                                            <?= $this->Html->link(
    '<span><i class="si si-trash"></i></span>',
    ['action' => 'delete', $faq['Faq']['id']],
    ['escape' => false,  'class' => 'btn bg-danger text-white btn-sm','data-toggle'=>'tooltip','data-placement'=>"top",'data-original-title'=>'Delete'], sprintf(__(' This Item %s Will Be Deleted', true), $faq['Faq']['id'])
) ?>
                                        </div>
                                    </td>

                                    </tr>
                                    <?php endforeach; ?>

                            </tbody>
                        </table>
                        <input type="submit" name="sorting_all" class="btn pull-left hidden-sm-down btn-danger add-custom" value="Sorting" style="margin-bottom: 15px;"> 
                        </form>
                    </div>
                </div>
                <!-- table-wrapper -->
            </div>
            <!-- section-wrapper -->
        </div>
    </div>
    <!-- row end -->

</div>

<!--

<div class="faqs index">
    <h2><?php __('Faqs');?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id');?></th>
            <th><?php echo $this->Paginator->sort('title');?></th>
            <th><?php echo $this->Paginator->sort('title_ar');?></th>
            <th><?php echo $this->Paginator->sort('answer');?></th>
            <th><?php echo $this->Paginator->sort('answer_ar');?></th>
            <th><?php echo $this->Paginator->sort('faqcategory_id');?></th>
            <th><?php echo $this->Paginator->sort('sorting');?></th>
            <th><?php echo $this->Paginator->sort('created');?></th>
            <th><?php echo $this->Paginator->sort('modified_by');?></th>
            <th class="actions"><?php __('Actions');?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($faqs as $faq):
        $class = null;
        if ($i++ % 2 == 0) {
            $class = ' class="altrow"';
        }
        ?>
        <tr<?php echo $class;?>>
            <td><?php echo $faq['Faq']['id']; ?>&nbsp;</td>
            <td><?php echo $faq['Faq']['title']; ?>&nbsp;</td>
            <td><?php echo $faq['Faq']['title_ar']; ?>&nbsp;</td>
            <td><?php echo $faq['Faq']['answer']; ?>&nbsp;</td>
            <td><?php echo $faq['Faq']['answer_ar']; ?>&nbsp;</td>
            <td>
                <?php echo $this->Html->link($faq['Faqcategory']['title'], array('controller' => 'faqcategories', 'action' => 'view', $faq['Faqcategory']['id'])); ?>
            </td>
            <td><?php echo $faq['Faq']['sorting']; ?>&nbsp;</td>
            <td><?php echo $faq['Faq']['created']; ?>&nbsp;</td>
            <td><?php echo $faq['Faq']['modified_by']; ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Html->link(__('View', true), array('action' => 'view', $faq['Faq']['id'])); ?>
                <?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $faq['Faq']['id'])); ?>
                <?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $faq['Faq']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $faq['Faq']['id'])); ?>
            </td>
            </tr>
            <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
        ));
        ?> </p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
        | <?php echo $this->Paginator->numbers();?>
        |
        <?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
    </div>
</div>
<div class="actions">
    <h3><?php __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Faq', true), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Faqcategories', true), array('controller' => 'faqcategories', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Faqcategory', true), array('controller' => 'faqcategories', 'action' => 'add')); ?> </li>
    </ul>
</div>
-->
