<script language="javascript" src="<?php echo $this->Html->url("/js/fckjs/ckeditor_OK/ckeditor.js"); ?>"></script>
    <div class="side-app">

    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb"><!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/faqs"); ?>">Help Support Answers</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Help Support Answers</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add Help Support Answers</h3>
                </div>
                <div class="card-body">
                    <?php echo $this->Form->create('Faq', array('class' => 'form-horizontal form-material'));?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>Title
                                    <?php
                                    echo $this->Form->input('title', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line'));
                                    echo $fck->load("Faq.title");
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>Title
                                    <?php
                                    echo $this->Form->input('title_ar', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line'));
                                    echo $fck->load("Faq.title.ar");
                                    ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>Answer
                                    <?php
                                    echo $this->Form->input('answer', array('label'=>false,'placeholder'=>'Enter Answer','class' => 'form-control form-control-line'));
                                    echo $fck->load("Faq.answer");
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>Answer Arabic
                                    <?php
                                    echo $this->Form->input('answer_ar', array('label'=>false,'placeholder'=>'Enter Answer Arabic','class' => 'form-control form-control-line'));
                                    echo $fck->load("Faq.answer.ar");
                                    ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>Help Support
                                    <?php
                                    echo $this->Form->input('faqcategory_id', array('label'=>false,'placeholder'=>'Enter Help Support','class' => 'form-control form-control-line'));
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>Sorting
                                    <?php
                                    echo $this->Form->input('sorting', array('label'=>false,'placeholder'=>'Enter Title','class' => 'form-control form-control-line','required'=>'required','options'=>$sorting));
                                    ?></label>
                            </div>
                        </div>
                                <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label class="custom-switch form-group">Active?
                                <input type="checkbox" name="data[Faq][status]" class="custom-switch-input" >
                                <span class="custom-switch-indicator"></span>
                            </label>
                            </div>
                        </div>
                        
                    </div>

            </div>
            <div class="card-footer text-right">

                <input type="submit" class="btn btn-success mt-1" value="Save">
            </div>
        </div>
    </div>
</div>
<!-- row end -->

</div>
<!--




<div class="faqs form">
<?php echo $this->Form->create('Faq');?>
<fieldset>
<legend><?php __('Add Faq'); ?></legend>
<?php
echo $this->Form->input('title');
echo $this->Form->input('title_ar');
echo $this->Form->input('answer');
echo $this->Form->input('answer_ar');
echo $this->Form->input('faqcategory_id');
echo $this->Form->input('sorting');
echo $this->Form->input('modified_by');
?>
</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
<h3><?php __('Actions'); ?></h3>
<ul>

<li><?php echo $this->Html->link(__('List Faqs', true), array('action' => 'index'));?></li>
<li><?php echo $this->Html->link(__('List Faqcategories', true), array('controller' => 'faqcategories', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Faqcategory', true), array('controller' => 'faqcategories', 'action' => 'add')); ?> </li>
</ul>
</div>-->
