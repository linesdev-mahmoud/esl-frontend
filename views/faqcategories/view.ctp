<!doctype html>
<html class="hcfe" data-page-type="HOMEPAGE" lang="en">
    <!-- Mirrored from support.google.com/youtube/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Mar 2021 11:42:45 GMT -->
    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

    <head>
        <?php if (!isset($meta) || $meta == "") { ?>
            <title>Emirates Safety Laboratory | ESL </title>
            <?php
        } else {
            echo $meta;
        }
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />



        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/faq_style.css" media="all" type="text/css"/>

        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick.css" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/slick-theme.css" />
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/aos.css" />
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.js"></script>
        <script type="text/javascript" async="" src="<?= $this->Html->url('/') ?>assets/js/analytics.min.js"></script>
        <!--<script type="text/javascript" async="" src="./assets/js/insight.min.js"></script>-->
        <!--<script async="" src="./assets/js/gtm.js"></script>
<script src="./assets/380429773370024" async=""></script>-->
        <script async="" src="<?= $this->Html->url('/') ?>assets/js/fbevents.js"></script>
        <meta name="viewport" content="width=device-width" />
        <meta name="next-head-count" content="4" />
        <?php if ($controller == 'customers' || $controller == 'apages') { ?>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <?php } ?>
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/main.css" />


        <style data-styled="active" data-styled-version="5.1.1"></style>
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/file8.js"></script>
        <script charset="utf-8" src="<?= $this->Html->url('/') ?>assets/js/file9.js"></script>
        <link rel="icon" type="image/png" href="<?= $this->Html->url('/') ?>assets/img/fave.png" sizes="32x32">

        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>etc/designs/help-twitter/public/css/main.css" media="all" type="text/css"/>

        <link href="<?= $this->Html->url('/') ?>etc/designs/help-twitter/public/css/print.css" rel="stylesheet" media="print"/>


        <script type="text/javascript" src="<?= $this->Html->url('/') ?>etc/clientlibs/dtm/help-twitter/c411b4930511/4121f48d5646/launch-aa3ff817aa50.min.js" async></script>
        <link rel="stylesheet" href="<?= $this->Html->url('/') ?>assets/css/custom<?= $_ar ?>.css" />
    </head>


    <?php
    $lang_action = '';
    if ($controller == 'pages' && $action == 'display') {
        $lang_action = 'display';
    }
    $lang_link = 'ara';
    $img_lang = $this->Html->url('/assets/img/ara.png');
    $lang_title = 'العربية';
    if ($this->Session->read('Language.locale') == 'ara') {
        $lang_link = 'eng';
        $img_lang = $this->Html->url('/assets/img/eng.png');
        $lang_title = 'English';
    }
    ?>
    <body class="demo-3 mobile promoted-search-body" class="twtr-theme--blue page home-page twtr-color-bg--extra-extra-light-gray-neutral js-no-scroll" data-ls="1" data-aos-easing="ease" data-aos-duration="400" data-aos-delay="0">
        <svg class="hidden">
        <defs>
    <filter id="filter-7">
        <feturbulence type="fractalNoise" baseFrequency="0" numOctaves="5" result="warp"></feturbulence>
        <fedisplacementmap xChannelSelector="R" yChannelSelector="G" scale="90" in="SourceGraphic" in2="warp"></fedisplacementmap>
    </filter>
    </defs>
    </svg>
    <div id="__next">
        <div id="awwwards" style="display:none;position: fixed; z-index: 999; transform: translateY(-50%); top: 50%; right: 0;">
            <a href="<?= $this->Html->url('/settings/lang_choice/') . $lang_link . '/' . $lang_action ?>" target="_self" class="image-lang">
                <img src="<?= $img_lang ?>"><span><?= $lang_title ?></span>
            </a>

        </div>
        <div id="mobile-awwwards" style="display:none;position: fixed; z-index: 999; transform: translateY(-50%); top: 50%; right: 0;" wfd-id="39">
            <a href="<?= $this->Html->url('/settings/lang_choice/') . $lang_link . '/' . $lang_action ?>" target="_self" class="image-lang">
                <img src="<?= $img_lang ?>"><span><?= $lang_title ?></span>
            </a>
        </div>
        <div class="Layout undefined">
            <nav style="background: #fbfbfb;">
                <div class="flex justify-between container-1">
                    <a href="<?= $this->Html->url('/') ?>">
                        <img class="cursor-pointer logo" src="<?= $this->Html->url('/') ?>assets/img/logo.png" alt="" />
                    </a>
                    <div class="menu menu--line links">

                        <a class="menu__link effect-link nav-effect-link false" href="<?=$this->Html->url('/main-faqs')?>">
                                                        <span class="menu__link-inner"><?=__('Help Center')?></span>
                                                        <span class="menu__link-deco"></span>
                                                    </a>
                                                    <a class="menu__link effect-link nav-effect-link false" href="#">
                                                        <span class="menu__link-inner"><?=__('Help topics')?> </span>
                                                        <span class="menu__link-deco"></span>
                                                    </a>
                                                    <a class="menu__link effect-link nav-effect-link false" href="<?= $this->Html->url('/services') ?>">
                                                        <span class="menu__link-inner"><?=__('Services')?></span>
                                                        <span class="menu__link-deco"></span>
                                                    </a>
                        <!--<a class="menu__link effect-link nav-effect-link false" href="contact.html">
                            <span class="menu__link-inner">Contact us</span>
                            <span class="menu__link-deco"></span>
                        </a>-->

                        
<!--                        <a href="<?= $this->Html->url('/settings/lang_choice/').$lang_link ?>" class="image-lang"><img src="<?=$img_lang?>"></a>-->
                        <a href="<?= $this->Html->url('/login') ?>" class="c-button--marquee"><span data-text="<?= __('Get Started') ?>"><?= __('Get Started') ?></span></a>
                    </div>
                </div>
            </nav>
            <div class="mobile-nav">
                <div class="hamburger hamburger--demo-3 js-hover">
                    <div class="hamburger__line hamburger__line--01"><div class="hamburger__line-in hamburger__line-in--01"></div></div>
                    <div class="hamburger__line hamburger__line--02"><div class="hamburger__line-in hamburger__line-in--02"></div></div>
                    <div class="hamburger__line hamburger__line--03"><div class="hamburger__line-in hamburger__line-in--03"></div></div>
                    <div class="hamburger__line hamburger__line--cross01"><div class="hamburger__line-in hamburger__line-in--cross01"></div></div>
                    <div class="hamburger__line hamburger__line--cross02"><div class="hamburger__line-in hamburger__line-in--cross02"></div></div>
                </div>
                <div class="global-menu">
                    <div class="global-menu__wrap">
                        <a href="#" class="global-menu__item global-menu__item--demo-3 false"><?=__('Help Center')?> </a>
                        <a href="#" class="global-menu__item global-menu__item--demo-3 false"><?=__('Help topics')?></a>
                        <a href="<?= $this->Html->url('/services') ?>" class="global-menu__item global-menu__item--demo-3 false"><?=__('Services')?></a>

                        <a href="<?= $this->Html->url('/login') ?>" class="global-menu__item global-menu__item--demo-3 started_link "><?= __('Get Started') ?></a>
                    </div>
                </div>
                <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
                <path class="shape-overlays__path"></path>
                <path class="shape-overlays__path"></path>
                <path class="shape-overlays__path"></path>
                </svg>
                <span class="is-opened-navi is-opened hidden"></span>
            </div>
            <div class="content-background"></div>
            <div class="hcfe">
                <div class="hcfe-content">
                    <section class="primary-container">
                        <div class="page-width-container">
                            <div class="main-content">
                                <article class="article page no-article-survey" sc-render-smart-button="false" itemscope="">
                                    <nav class="accordion-homepage" data-stats-ve="1">
                                        <?php $i = 0;
                                        foreach ($faqs_data as $faq) { ?>
                                            <section class="parent" data-id="<?= $faq['faqs']['id'] ?>" data-stats-idx="<?= $i ?>,8">
                                                <h2 tabindex="0"><?= $faq['faqs']['title'.$_ar] ?><svg class="up" viewbox="0 0 24 24">
                                                    <path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z"></path>
                                                    </svg><svg class="down" viewbox="0 0 24 24">
                                                    <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z"></path>
                                                    </svg>
                                                </h2>
                                                <div class="overflow">
                                                    <div class="children">
                                                        <div class="child"><a><?= $faq['faqs']['answer'.$_ar] ?></a></div>
                                                    </div>
                                                </div>
                                            </section>

    <?php $i++;
} ?>

                                    </nav>
                                </article>
                            </div>

                        </div>
                    </section>

                    <div class="lb-overlay"></div>
                    <div class="lb-modal">
                        <div class="lb-frame"> <a class="lb-close" href="#" role="button"> <svg viewbox="0 0 48 48">
                                <path d="M38 12.83L35.17 10 24 21.17 12.83 10 10 12.83 21.17 24 10 35.17 12.83 38 24 26.83 35.17 38 38 35.17 26.83 24z"></path>
                                </svg> </a>
                            <div class="lb-content"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div ng-non-bindable="">
                <div class="gb_Hd">Google apps</div>
                <div class="gb_xc">Main menu</div>
            </div>
            <textarea class="csi" name="csi"></textarea>
            <script data-id="common" nonce="fzUJE2P/a/e4J4X2N5Ie6" src="<?= $this->Html->url('/') ?>assets/js/faq_script.js"></script>


            <div data-page-data-key="search_help_center" style="display:none">Search Help Center</div>


            <script data-id="homepage" nonce="fzUJE2Pa/e4J4X2N5Ie6">
                function faa() {
                    this.o = window.sc_scope || document;
                    gaa(this)
                }

                function gaa(a) {
                    for (var b = a.o.querySelectorAll(".accordion-homepage h2"), c = {}, e = 0; e < b.length; c = {
                        oh: c.oh
                    }, e++) {
                        var f = b[e];
                        c.oh = f.parentElement;
                        f.setAttribute("aria-role", "button");
                        nE(c.oh);
                        f.querySelector(".up") && f.querySelector(".up").setAttribute("aria-label", "Expanded menu");
                        f.querySelector(".down") && f.querySelector(".down").setAttribute("aria-label", "Collapsed menu");
                        f.addEventListener("click", function (k) {
                            return function () {
                                return oE(a, k.oh)
                            }
                        }(c));
                        f.addEventListener("keypress", function (k, n) {
                            n = n.which || n.keyCode;
                            13 !== n && 32 !== n || oE(this, k)
                        }.bind(a, c.oh));
                        c.oh.querySelector(".children").addEventListener("transitionend", haa)
                    }
                    var h;
                    (b = ug("topic")) && (h = a.o.querySelector('.accordion-homepage .parent[data-id="' + b + '"]'));
                    h || (h = a.o.querySelector(".accordion-homepage .parent"));
                    h && oE(a, h)
                }

                function nE(a) {
                    var b = a.querySelector(".children");
                    b.style.marginTop = -(b.offsetTop - b.parentElement.offsetTop + b.offsetHeight) + "px";
                    pE(a, !1)
                }

                function pE(a, b) {
                    a.querySelector("h2").setAttribute("aria-expanded", b);
                    a.querySelector(".children").setAttribute("aria-hidden", !b)
                }

                function oE(a, b) {
                    b.querySelector(".children").style.display = "";
                    (a = a.o.querySelector(".accordion-homepage .selected")) && a != b && (qE(a), pE(a, !1));
                    qE(b);
                    a = pg(b, "selected");
                    pE(b, a);
                    vg("topic", a ? b.getAttribute("data-id") : "")
                }

                function qE(a) {
                    kg(a, "selected");
                    if (pg(a, "selected")) {
                        a.querySelector(".children").style.marginTop = 0;
                        var b = 2
                    } else
                        nE(a), b = 3;
                    window.sc_trackStatsEventByElement(a, 1, b)
                }
                window.sc_initHomepage = function () {
                    new faa
                };

                function haa(a) {
                    a = a.target;
                    var b = a.parentElement.parentElement;
                    b && !pg(b, "selected") && (a.style.display = "none")
                }
                ;

            </script>
            <script nonce="fzUJE2Pa/e4J4X2N5Ie6">
                window['sc_initHomepage']();

            </script>

            <div data-page-data-key="initializeSearchTracking" style="display:none">true</div>
            <div data-page-data-key="productEscalationsId" style="display:none">59</div>












            <div class="sc-assertive-live-region" aria-live="assertive"></div>
            <div class="sc-polite-live-region" aria-live="polite"></div>
            <div data-page-data-key="msf__ras" style="display:none">false</div>
            <link rel="stylesheet" href="etc/designs/common-twitter/clientlib-u12-data-protection-notice.min.css" type="text/css">
        <script src="etc/designs/common-twitter/clientlib-promise-polyfill.min.js"></script>
        <script src="etc/designs/common-twitter/clientlib-twtr-profile-adapter.min.js"></script>
        <script src="etc/designs/common-twitter/clientlib-u12-data-protection-notice.min.js"></script>










    <footer class="">
    <div class="grid grid-cols-1 md:grid-cols-4">
        <div class="col">
            <div class="head"><?=$heads[1]['title'.$_ar]?></div>
            <?php foreach($links[1] as $link){?>
            <a class="effect-link false" href="<?=$link['url']?>"><span><?=$link['title'.$_ar]?></span></a>
            <?php }?>
            
        </div>
        <div class="col">
            <div class="head"><?=$heads[2]['title'.$_ar]?></div>
            <?php foreach($links[2] as $link){?>
            <a class="effect-link false" href="<?=$link['url']?>"><span><?=$link['title'.$_ar]?></span></a>
            <?php }?>
            
        </div>
        <div class="col">
            <div class="head"><?=$heads[3]['title'.$_ar]?></div>
            <?php foreach($links[3] as $link){?>
            <a class="effect-link false" href="<?=$link['url']?>"><span><?=$link['title'.$_ar]?></span></a>
            <?php }?>
        </div>
        <div class="col">
            <div class="head"><?=$heads[4]['title'.$_ar]?></div>
            <?php foreach($links[4] as $link){?>
            <a href="<?=$link['url']?>"><?=$link['title'.$_ar]?></a>
            <?php }?>
            

        </div>
    </div>
    <div class="bottom flex-wrap flex justify-between">
        <a href="<?=$this->Html->url('/')?>">
            <img src="<?=$this->Html->url('/')?>assets/img/logo.png" alt="" />
        </a>
        <div class="right">
            <a href="<?=$this->Html->url('/page/privacy-policy')?>"><?=__('Privacy Policy')?></a>
            <span class="copyright"><?=__('© Copyright 2021 ESL.')?></span>
        </div>
    </div>
</footer>
</div>
</div>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag("js", new Date());
    gtag("config", "UA-177306561-1");
</script>


<!--<script nomodule="" src="./assets/js/polyfills.js"></script>-->
<script src="<?= $this->Html->url('/') ?>assets/js/main.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/webpack.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/framework.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/file1.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/file2.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/file3.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/file4.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/file5.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/app.js" async=""></script>
<!-- app js -->


<?php if($controller == 'apages' && $action == 'contact'){?>
    <script src="<?= $this->Html->url('/') ?>assets/js/menu_contact.js" async=""></script>
<?php }else{?>
<!--<script src="<?= $this->Html->url('/') ?>assets/js/menu_index.js" async=""></script>-->
    <!-- menu index -->
    
<?php }?>
<script src="<?= $this->Html->url('/') ?>assets/js/file7.js" async=""></script>

<script src="<?= $this->Html->url('/') ?>assets/js/buildManifest.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/ssgManifest.js" async=""></script>
<script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/slick.min.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/aos.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/TweenMax.min.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/ScrollMagic.min.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/animation.gsap.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/jquery-ui.min.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/jquery.ui.touch-punch.min.js"></script>

<script src="<?= $this->Html->url('/') ?>assets/js/priceSlider.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/easings.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/demo3.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/common.js"></script>
<script src="<?= $this->Html->url('/') ?>assets/js/jquery-3.4.1.min.js"></script>
<?php if($controller == 'customers' || $controller == 'apages'){?>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?php }?>
<script>
                            
    setInterval(function () {
        <?php
        $img_lang = $this->Html->url('/assets/img/ara.png');
        $lang_title = 'العربية';
        if($this->Session->read('Language.locale')=='ara'){
                        $img_lang = $this->Html->url('/assets/img/eng.png');
                        $lang_title = 'English';
                    }?>
                  $('.image-lang').html('<img src="<?=$img_lang?>"><span><?=$lang_title?></span>');                                  
        //$('.first').html("We are the one-stop shop <span style='color:#2ba78a;'>Testing Laboratory</span> and <span style='color:#2ba78a;'>Certification Body </span>based in Dubai with strong expertise in fire protection requirements.");
        //$('.text_replace').html("Behind successful fire protection products, systems or materials are efficient and robust Testing, Inspection and Certification (TIC) processes. To us, that’s our DNA.<br>We ensure safety, integrity, and quality of your passive or active fire protection systems through conformity assessment and assurance. We provide conformity assessment for Lorem Ipsum is simply dummy text of the printing and typesetting industry.");
        //$('.second').html("We <span style='color:#2ba78a;'>certify</span> , <span style='color:#2ba78a;'>test</span> and provide advisory on development project.<br> Let us help you win the competition, reach your maximum, and score your company goals successfully.  Let’s start");

    }, 500);
</script>
</body>
</html>
