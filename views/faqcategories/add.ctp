<script language="javascript" src="<?php echo $this->Html->url("/js/fckjs/ckeditor_OK/ckeditor.js"); ?>"></script>
<div class="side-app">
    <!-- page-header -->
    <div class="page-header">
        <ol class="breadcrumb">
            <!-- breadcrumb -->
            <li class="breadcrumb-item"><a href="<?php echo $this->Html->url("/faqcategories"); ?>">Help Support</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Help Support</li>
        </ol><!-- End breadcrumb -->
    </div>
    <!-- End page-header -->

    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <?php echo $this->Form->create('Faqcategory', array('type'=>'file','url'=>'/'.$this->params['url']['url'],'class' => 'form-horizontal form-material'));?>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Add Help Support</h3>
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Title
                                    <?php
                                    echo $this->Form->input('title', array('label'=>false,'class' => 'form-control form-control-line','required'=>'required' ));
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Title Arabic

                                    <?php
                                    echo $this->Form->input('title_ar',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required'));

                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Header

                                    <?php
                                    echo $this->Form->input('header',array('label'=>false,'class' => 'form-control  form-control-line'));
                                    echo $fck->load("Faqcategory.header");
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Header Arabic

                                    <?php
                                    echo $this->Form->input('header_ar',array('label'=>false,'class' => 'form-control  form-control-line'));
                                    echo $fck->load("Faqcategory.header.ar");
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Footer

                                    <?php
                                    echo $this->Form->input('footer',array('label'=>false,'class' => 'form-control  form-control-line'));
                                    echo $fck->load("Faqcategory.footer");
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Footer Arabic

                                    <?php
                                    echo $this->Form->input('footer_ar',array('label'=>false,'class' => 'form-control  form-control-line'));
                                    echo $fck->load("Faqcategory.footer.ar");
                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Parent

                                    <?php
                                    echo $this->Form->input('parent_id',array('label'=>false,'class' => 'form-control  form-control-line','options'=>$parentFaqcategories));

                                    ?></label>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Sorting

                                    <?php
                                    echo $this->Form->input('sorting',array('label'=>false,'class' => 'form-control  form-control-line','required'=>'required','options'=>$sorting));

                                    ?></label>
                            </div>
                        </div>

                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label class="custom-switch form-group">Active?
                                <input type="checkbox" name="data[Faqcategory][status]" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                            </label>

                        </div>

                        <div class="card-footer text-right col-lg-12 col-md-12">

                            <input type="submit" class="btn btn-success mt-1" value="Save">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row end -->

    </div>



    <!--
<div class="faqcategories form">
<?php echo $this->Form->create('Faqcategory');?>
<fieldset>
<legend><?php __('Add Faqcategory'); ?></legend>
<?php
echo $this->Form->input('title');
echo $this->Form->input('title_ar');
echo $this->Form->input('header');
echo $this->Form->input('header_ar');
echo $this->Form->input('footer');
echo $this->Form->input('footer_ar');
echo $this->Form->input('parent_id');
echo $this->Form->input('status');
echo $this->Form->input('sorting');
?>
</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
<h3><?php __('Actions'); ?></h3>
<ul>

<li><?php echo $this->Html->link(__('List Faqcategories', true), array('action' => 'index'));?></li>
<li><?php echo $this->Html->link(__('List Faqcategories', true), array('controller' => 'faqcategories', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Parent Faqcategory', true), array('controller' => 'faqcategories', 'action' => 'add')); ?> </li>
<li><?php echo $this->Html->link(__('List Faqs', true), array('controller' => 'faqs', 'action' => 'index')); ?> </li>
<li><?php echo $this->Html->link(__('New Faq', true), array('controller' => 'faqs', 'action' => 'add')); ?> </li>
</ul>
</div>-->
