<div class="businessrelates index">
	<h2><?php __('Businessrelates');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('customer_id');?></th>
			<th><?php echo $this->Paginator->sort('businesstype_id');?></th>
			<th><?php echo $this->Paginator->sort('goods');?></th>
			<th><?php echo $this->Paginator->sort('website');?></th>
			<th><?php echo $this->Paginator->sort('supplieraddress');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th><?php echo $this->Paginator->sort('modified_by');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($businessrelates as $businessrelate):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $businessrelate['Businessrelate']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($businessrelate['Customer']['id'], array('controller' => 'customers', 'action' => 'view', $businessrelate['Customer']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($businessrelate['Businesstype']['title'], array('controller' => 'businesstypes', 'action' => 'view', $businessrelate['Businesstype']['id'])); ?>
		</td>
		<td><?php echo $businessrelate['Businessrelate']['goods']; ?>&nbsp;</td>
		<td><?php echo $businessrelate['Businessrelate']['website']; ?>&nbsp;</td>
		<td><?php echo $businessrelate['Businessrelate']['supplieraddress']; ?>&nbsp;</td>
		<td><?php echo $businessrelate['Businessrelate']['status']; ?>&nbsp;</td>
		<td><?php echo $businessrelate['Businessrelate']['created']; ?>&nbsp;</td>
		<td><?php echo $businessrelate['Businessrelate']['modified']; ?>&nbsp;</td>
		<td><?php echo $businessrelate['Businessrelate']['modified_by']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $businessrelate['Businessrelate']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $businessrelate['Businessrelate']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $businessrelate['Businessrelate']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $businessrelate['Businessrelate']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Businessrelate', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Customers', true), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer', true), array('controller' => 'customers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Businesstypes', true), array('controller' => 'businesstypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Businesstype', true), array('controller' => 'businesstypes', 'action' => 'add')); ?> </li>
	</ul>
</div>