<!DOCTYPE html>
<html lang="en-US" class="no-js">

    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Emirates Safety Laboratory | ESL</title>
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png">
        <link rel="canonical" href="https://emirateslaboratory.com/">
        <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="ESL" />
        <meta property="og:title" content="Contact - ESL |" />
        <meta property="og:url" content="https://emirateslaboratory.com/signup" />
        <meta property="og:site_name" content="emirateslaboratory" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Contact - ESL " />
        <meta name="twitter:site" content="@_ESL" />
        <meta name="twitter:creator" content="@_ESL" />

        <!--<link rel='dns-prefetch' href='//fonts.googleapis.com' />-->
        <meta content="Divi Child v.1.0.0" name="generator" />

        <link rel='stylesheet' id='parent-style-css'
              href='<?php echo $this->Html->url("/"); ?>css2/divi/style.css?ver=5.3.2' type='text/css' media='all' />
        <link rel='stylesheet' id='gs-beh-custom-bootstrap-css'
              href='<?php echo $this->Html->url("/"); ?>css2/gs-beh-custom-bootstrap.css?ver=1.0.2' type='text/css'
              media='all' />
        <link rel='stylesheet' id='gs_behance_custom_css-css'
              href='<?php echo $this->Html->url("/"); ?>css2/gs-beh-custom.css?ver=1.0.2' type='text/css' media='all' />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel='stylesheet' id='gs-dribb-custom-bootstrap-css'
              href='<?php echo $this->Html->url("/"); ?>css2/gs-dibb-custom-bootstrap.css?ver=1.1.2' type='text/css'
              media='all' />
        <link rel='stylesheet' id='gs_drib_csutom_css-css'
              href='<?php echo $this->Html->url("/"); ?>css2/gs-drib-custom.css?ver=1.1.2' type='text/css' media='all' />


        <link rel='stylesheet' id='wc-gateway-ppec-frontend-css'
              href='<?php echo $this->Html->url("/"); ?>css2/wc-gateway-ppec-frontend.css?ver=5.3.2' type='text/css'
              media='all' />
        <link rel='stylesheet' id='wp-block-library-css'
              href='<?php echo $this->Html->url("/"); ?>css2/style.min.css?ver=5.3.2' type='text/css' media='all' />
        <link rel='stylesheet' id='wc-block-style-css' href='<?php echo $this->Html->url("/"); ?>css2/style4.css?ver=2.5.11'
              type='text/css' media='all' />
        <link rel='stylesheet' id='contact-form-7-css' href='<?php echo $this->Html->url("/"); ?>css2/styles.css?ver=5.1.6'
              type='text/css' media='all' />

        <link rel='stylesheet' id='et-builder-googlefonts-cached-css'
              href='https://fonts.googleapis.com/css?family=Titillium+Web%3A200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C600%2C600italic%2C700%2C700italic%2C900&#038;ver=5.3.2#038;subset=latin,latin-ext'
              type='text/css' media='all' />
        <link rel='stylesheet' id='dashicons-css'
              href='<?php echo $this->Html->url("/"); ?>css2/dashicons.min.css?ver=5.3.2' type='text/css' media='all' />
        <script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>css2/jquery.js?ver=1.12.4-wp'></script>
        <script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>css2/jquery-migrate.min.js?ver=1.4.1'>
        </script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var et_core_api_spam_recaptcha = {
                "site_key": "",
                "page_action": {
                    "action": "contact"
                }
            };
            /* ]]> */
        </script>
        <script type='text/javascript' src='<?php echo $this->Html->url("/"); ?>css2/recaptcha.js?ver=5.3.2'></script>
        <link rel="alternate" type="application/json+oembed" href="<?php echo $this->Html->url("/"); ?>css2/embed.json" />
        <meta name="referrer" content="always" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
        </noscript>


        <script>
            (function (e, t, n) {
                var r = e.querySelectorAll("html")[0];
                r.className = r.className.replace(/(^|\s)no-js(\s|$)/, "$1js$2")
            })(document, window, 0);
        </script>


        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"
              id="bootstrap-css">
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>


        <!-- Start FontAweseome -->
        <link rel="stylesheet" href="<?php echo $this->Html->url("/"); ?>css2/all.css"
              integrity="" crossorigin="anonymous">

        <!-- End Google Tag Manager -->
        <!-- Reveal Javascript -->
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Hide the div
                jQuery('#reveal').hide();
                jQuery('.rv_button').click(function (e) {
                    e.preventDefault();
                    jQuery("#reveal").slideToggle();
                    jQuery('.rv_button').toggleClass('opened closed');
                });
            });
        </script>
        <!-- End Reveal Javascript -->
        <link rel="icon" href="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-32x32.png" sizes="32x32" />
        <link rel="icon" href="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-192x192.png" sizes="192x192" />
        <link rel="apple-touch-icon-precomposed"
              href="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-180x180.png" />
        <meta name="msapplication-TileImage"
              content="<?php echo $this->Html->url("/"); ?>css2/cropped-Icon-Green-270x270.png" />
        <link rel="stylesheet" id="et-core-unified-1133-cached-inline-styles"
              href="<?php echo $this->Html->url("/"); ?>css2/styles/et-core-unified-1133-158516164351.min4.css"
              onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" />

        <!--Mutipleselect css-->
        <link rel="stylesheet" href="<?php echo $this->Html->url("/"); ?>assets/portal/plugins/multipleselect/multiple-select.css">

        <style media="all">
            @charset "UTF-8";
            @import url(https://fonts.googleapis.com/css?family=Varela+Round&display=swap);

            @font-face {
                font-family: 'Noto Sans';
                font-display: swap;
                font-weight: 700;
                font-style: normal;
                src: url(<?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.woff2) format('woff2'),
                    url(<?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.woff) format('woff'),
                    url(<?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.ttf) format('truetype');
            }

            @font-face {
                font-family: 'Noto Sans';
                font-display: swap;
                font-weight: 400;
                font-style: normal;
                src: url(<?php echo $this->Html->url("/");
?><?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.woff2) format('woff2'),
                    url(><?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.woff) format('woff'),
                    url(<?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.ttf) format('truetype');
            }

            @font-face {
                font-weight: 600;
                font-style: normal;
                font-family: 'Noto Sans';
                font-display: swap;
                src: url(<?php echo $this->Html->url("/");
?><?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.woff2) format('woff2'),
                    url(<?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.woff) format('woff'),
                    url(<?php echo $this->Html->url("/");
?>assets/portal/fonts/noto-sans-v9-latin-regular.ttf) format('truetype');
            }
        </style>

        <link rel='stylesheet' id='custome-style-css' href='<?php echo $this->Html->url("/"); ?>css2/custome2.css'
              type='text/css' media='all' />


        <script>
            $(document).ready(function () {

                var navListItems = $('div.setup-panel div a'),
                        allWells = $('.setup-content'),
                        allNextBtn = $('.nextBtn');

                allWells.hide();

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                            $item = $(this);

                    if (!$item.hasClass('disabled')) {
                        navListItems.removeClass('btn-primary').addClass('btn-default');
                        $item.addClass('btn-primary');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });

                allNextBtn.click(function () {
                    var curStep = $(this).closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next()
                            .children("a"),
                            curInputs = curStep.find("input[type='text'],input[type='url']"),
                            isValid = true;

                    $(".form-group").removeClass("has-error");

                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }



                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');

                    if (curStepBtn == "step-2") {
                        document.getElementById("imgbanner").src =
                                "<?php echo $this->Html->url("/"); ?>assets/portal/img/applicationback2.png";
                    }
                    if (curStepBtn == "step-3") {
                        document.getElementById("imgbanner").src =
                                "<?php echo $this->Html->url("/"); ?>assets/portal/img/applicationback3.png";
                    }

                    if (curStepBtn == "step-4") {
                        var aa = document.getElementById("customFile1").value;
                        if (aa == "") {
                            alert('ayaz code here');
                            // ayaz code here


                        }
                    }


                });

                $('div.setup-panel div a.btn-primary').trigger('click');

                var chkyesiss = document.getElementById("chkchangecertificatescope").checked;
                if (chkyesiss) {
                    document.getElementById("entercertno").style.display = 'block';
                }

                var chkyesiss1 = document.getElementById("chkchangeRecertificate").checked;
                if (chkyesiss1) {
                    document.getElementById("Reentercertno").style.display = 'block';
                }



            });
        </script>




        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

        <script>
            // Add the following code if you want the name of the file appear on select
            $(".custom-file-input").on("change", function () {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });
        </script>

        <script>
            function othercheck() {

                var chkyesiss = document.getElementById("chkissueofcerti").checked;
                if (chkyesiss == false) {
                    document.getElementById("entercertno").style.display = 'block';
                    document.getElementById("chkchangecertificatescope").checked = true;
                }

                if (chkyesiss == true) {
                    document.getElementById("entercertno").style.display = 'none';
                    document.getElementById("chkchangecertificatescope").checked = false;


                    document.getElementById("Reentercertno").style.display = 'none';
                    document.getElementById("chkchangeRecertificate").checked = false;
                }
            }

            function showcertno() {

                var chkyes = document.getElementById("chkchangecertificatescope").checked;
                if (chkyes == true) {
                    document.getElementById("chkissueofcerti").checked = false;
                    document.getElementById("chkchangeRecertificate").checked = false;
                    document.getElementById("Reentercertno").style.display = 'none';
                    document.getElementById("entercertno").style.display = 'block';
                }

                if (chkyes == false) {
                    document.getElementById("entercertno").style.display = 'none';
                    document.getElementById("chkissueofcerti").checked = true;
                }
            }

            function showRecertno() {

                var chkyes = document.getElementById("chkchangeRecertificate").checked;

                if (chkyes == true) {
                    document.getElementById("Reentercertno").style.display = 'block';
                    document.getElementById("entercertno").style.display = 'none';
                    document.getElementById("chkissueofcerti").checked = false;
                    document.getElementById("chkchangecertificatescope").checked = false;
                }

                if (chkyes == false) {
                    document.getElementById("Reentercertno").style.display = 'none';
                    document.getElementById("chkissueofcerti").checked = true;

                }
            }


            function productsamples() {

                var chkyes = document.getElementById("chkregularproduction").checked;

                if (chkyes == true) {
                    document.getElementById("chkprototypes").checked = false;
                }

                if (chkyes == false) {
                    document.getElementById("chkprototypes").checked = true;

                }
            }


            function productsamples2() {

                var chkyes = document.getElementById("chkprototypes").checked;

                if (chkyes == true) {
                    document.getElementById("chkregularproduction").checked = false;
                }

                if (chkyes == false) {
                    document.getElementById("chkregularproduction").checked = true;

                }
            }
        </script>

        <link rel='stylesheet' id='custome-style-css' href='<?php echo $this->Html->url("/"); ?>style_custom.css'
              type='text/css' media='all' />

    </head>

    <body
        class="page-template-default page page-id-1133 custom-background theme-Divi woocommerce-no-js et_bloom et_button_no_icon et_pb_button_helper_class et_fullwidth_nav et_fixed_nav et_hide_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_svg_logo et_pb_footer_columns4 et_cover_background et_pb_gutter windows et_pb_gutters3 et_pb_pagebuilder_layout et_no_sidebar et_divi_theme et-db et_minified_js et_minified_css">
        <div id="page-container">

            <header id="main-header" data-height-onload="87">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="<?php echo $this->Html->url("/"); ?>">
                            <img src="<?php echo $this->Html->url("/"); ?>assets/portal/img/logo.png"
                                 alt="ESL" id="logo" data-height-percentage="44" />
                        </a>
                    </div>

                    <div id="et-top-navigation" data-height="87" data-fixed-height="60">

                        <div id="et_mobile_nav_menu">
                            <div>

                                <a 
                                    class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light "
                                    href="<?php echo $this->Html->url("/dashboard"); ?>" style="margin-right:11px;width:100%">DashBoard </a>
                                <a class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light"
                                   href="<?php echo $this->Html->url("/logout"); ?>" style="margin-right:11px;width:100%">Sign Out </a>



                            </div>

                        </div>
                    </div> <!-- #et-top-navigation -->
                </div> <!-- .container -->

            </header> <!-- #main-header -->


            <div id="et-main-area">
                <div id="main-content">
                    <article id="post-1133" class="post-1133 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div id="et-boc" class="et-boc">
                                <div class="et-l et-l--post">
                                    <div class="et_builder_inner_content et_pb_gutters3">
                                        <div class="et_pb_section et_pb_section_0 et_pb_with_background et_section_regular ">
                                            <div id="white-nav"
                                                 class="header_hid et_pb_row et_pb_row_1 custom-nav et_pb_row_fullwidth et_pb_row_1-4_1-2_1-4">



                                                <div style=" float:right" >

                                                    <a style="width:100%; font-size:14px!important; font-weight: 600!important;padding-right: 20px!important;padding-left: 20px!important;padding-top: 14px!important; padding-bottom: 14px!important;border-radius: 33px!important; margin-right:10px"
                                                       class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light "
                                                       href="<?php echo $this->Html->url("/dashboard"); ?>">DashBoard </a>
                                                    <a style="width:100%; font-size:14px!important; font-weight: 600!important; padding-right: 20px!important;padding-left: 20px!important; padding-top: 14px!important; padding-bottom: 14px!important; border-radius: 33px!important; "
                                                       class="et_pb_button et_pb_button_0 et_hover_enabled et_pb_bg_layout_light"
                                                       href="<?php echo $this->Html->url("/logout"); ?>">Sign Out </a>



                                                </div>


                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_0  et_pb_css_mix_blend_mode_passthrough">
                                                    <div class="et_pb_module et_pb_image et_pb_image_0">
                                                        <a href="<?php echo $this->Html->url("/"); ?>">
                                                            <span class="et_pb_image_wrap ">
                                                                <img src="<?php echo $this->Html->url("/"); ?>assets/portal/img/logo.png"
                                                                     alt="" title="">
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div> <!-- .et_pb_column -->

                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_1  et_pb_css_mix_blend_mode_passthrough">
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">
                                                        <div class="et_pb_text_inner">
                                                            <div>
                                                            </div>
                                                        </div>
                                                    </div> <!-- .et_pb_text -->
                                                </div> <!-- .et_pb_column -->






                                            </div>
                                            <!-- .et_pb_row -->

                                            <h6 style="text-align:center;margin: 0 10%;">
                                                <?php echo $this->Session->flash(); ?>
                                            </h6>



                                            <?php echo $content_for_layout;?>

                                            <script>
                                                function showcontactdiv() {
                                                    var getdiplay = document.getElementById("contacthidediv").style.display;
                                                    if (getdiplay == "none") {
                                                        document.getElementById("contacthidediv").style.display = "block";
                                                        //  document.getElementById("sholnk").innerHTML = "-";
                                                    }

                                                    if (getdiplay == "block") {
                                                        document.getElementById("contacthidediv").style.display = "none";
                                                        /* document.getElementById("sholnk").innerHTML = "+";*/
                                                    }

                                                }
                                            </script>


                                            <script src="<?php echo $this->Html->url("/"); ?>assets/portal/js/custom-file-input.js"></script>

                                            <script type="text/javascript">
                                                                                            jQuery(window).load(function () {
                                                                                                setTimeout(function () {
                                                                                                    var et_header_height = 107;
                                                                                                    jQuery('body').addClass('et_hide_nav');
                                                                                                    jQuery('body').removeClass('et_hide_nav_disabled');
                                                                                                    jQuery('#main-header').css('transform', 'translateY(-' + et_header_height + 'px)');
                                                                                                }, 100)
                                                                                            });
                                            </script>


                                            <script src="<?php echo $this->Html->url("/"); ?>assets/portal/js/core.min.js"></script>
                                            <script src="<?php echo $this->Html->url("/"); ?>assets/portal/js/script.js"></script>

                                            <!--MutipleSelect js-->
                                            <script src="<?php echo $this->Html->url("/"); ?>assets/portal/plugins/multipleselect/multiple-select.js"></script>
                                            <script src="<?php echo $this->Html->url("/"); ?>assets/portal/plugins/multipleselect/multi-select.js"></script>


                                            </body>

                                            </html>