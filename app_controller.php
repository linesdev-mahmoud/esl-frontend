<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       cake
 * @subpackage    cake.app
 */
class AppController extends Controller {
     public function get_meta() {
      $current_url=$this->getUrl();

      if (strpos($current_url, 'https://')!== false) {
//echo $current_url;
      }
      else {
          //die();
      $current_url=str_replace('http:/', 'https:/', $current_url);
      //echo $current_url;
      header("LOCATION:" . $current_url);
      }

      $current_url=rtrim($current_url, "/");

      $current_url=str_replace('www.', '', $current_url);
      //$current_url = str_replace('https', 'http', $current_url);



      $this->loadModel('SeoUrl');
      $seo_url=$this->SeoUrl->query('select seo_urls_attributes.value,seo_attributes.code from '
      . 'seo_urls_attributes inner JOIN seo_urls ON seo_urls_attributes.seo_url_id = seo_urls.id AND (seo_urls.url = "' . $current_url . '" OR seo_urls.url = "' . $current_url . '/") '
      . 'INNER JOIN seo_attributes ON seo_attributes.id = seo_urls_attributes.seo_attribute_id ');
      $meta   ='';
      foreach ($seo_url as $seo_ur) {
      $meta.=str_replace('??', trim($seo_ur['seo_urls_attributes']['value']), $seo_ur['seo_attributes']['code']) . "\n";
      }
      $this->set('meta', $meta);
      } 

/*public function getUrl() {


      $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

      return $url; // Outputs: Full URL
      }*/
     public function getUrl() {


      $uri=$_SERVER['REQUEST_URI'];

      //echo $uri; // Outputs: URI
      $protocol=((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']!= 'off')  || $_SERVER['SERVER_PORT']== 443) ? "https://" : "http://";
      $url     =$protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

      return $url; // Outputs: Full URL
      } 
    public $defaultsenderemail;
    public $SMTPDebug;
    public $Host;
    public $SMTPAuth;
    public $Password;
    public $SMTPSecure;
    public $Port;
    public $emailTitle;
    public $urlportal;
    public $users;
    public $quotationExpirationTime;
    public $databasename;
    public $param_url;
    public $dashboardurl;  
    public $frontend_url;  
    public $portal_url ;
    public function setConf() {
        $this->loadModel("Permission");
        $this->Permission->recursive = -1;
        $configs = $this->Permission->query('SELECT * FROM `configrations`');
        $data = array();
        foreach ($configs as $config) {
            $data[$config["configrations"]["code"]] = $config["configrations"]["value"];
        }
        extract($data);
        $this->defaultsenderemail = $defaultsenderemail;
        $this->SMTPDebug = $SMTPDebug;
        $this->Host = $Host;
        $this->SMTPAuth = $SMTPAuth;
        $this->Password = $Password;
        $this->SMTPSecure = $SMTPSecure;
        $this->Port = $Port;
        $this->emailTitle = $emailTitle;
        $this->urlportal = $urlportal;
        $this->quotationExpirationTime = $quotationExpirationTime;
        $this->databasename = $databasename;
        $this->param_url = $param_url;
        $this->dashboardurl = $dashboardurl;
        $this->frontend_url = $frontend_url;
        $this->portal_url = $urlportal;
    }

    public function beforeFilter() {
        $this->get_meta();
        $this->setConf();
        //phpinfo();die();
        $admin_url = $this->dashboardurl.'/img/uploads/';
        $fb_share_url = "https://www.facebook.com/sharer.php?u=/".$this->frontend_url;
        $tw_share_url = "https://twitter.com/intent/tweet?url=/".$this->frontend_url;
        $pin_share_url = "https://pinterest.com/pin/create/link/?url=/".$this->frontend_url;
        $wh_share_url = "whatsapp://send?text=/".$this->frontend_url;
        $custom_url = $this->frontend_url;
        $this->set('frontend_url',$this->frontend_url);
        $this->set(compact('fb_share_url', 'tw_share_url', 'wh_share_url', 'pin_share_url', 'custom_url'));
        $lang = 'eng';
        if ($this->Session->read('Language.locale')) {
            $lang = $this->Session->read('Language.locale');
        }
        Configure::write('Config.language', $lang);
        //print_r($lang);die();
        $_ar = '';
        if ($lang == 'ara') {
            $_ar = '_ar';
        }
        $find = array(' ');
        $replace = array('-');
        $this->set(compact('find', 'replace'));
        //$this->set(compact('_ar','lang'));
        $controller = $this->params['controller'];
        $action = $this->params['action'];
        $this->set(compact('controller', 'action'));
        //print_r($controller);die();
        $customer_id = $this->Session->read('Customer.id');
        $this->loadModel('Banner');
        $banners = $this->Banner->find('all', array('conditions' => array('Banner.status = 1 ORDER BY Banner.sorting ASC LIMIT 4')));
        $banners_title = '';
        $b = 0;
        foreach ($banners as $banner) {
            $banners_title = $b . ' === n ? bn("div", {className: "custom-slick-dot",key: n}, n + 1, bn("span", null,"' . $banner['Banner']['title' . $_ar] . '"), bn("div", {className: "loader loader' . $banner['Banner']['id'] . '"})):';
            $b++;
            //print_r($banners_title);
        }
        //die();
        //print_r($banners_title);die();
        $this->set(compact('banners_title'));

        //print_r($skuList);die(); 
        $this->loadModel('PageSetting');
        $page_settings = $this->PageSetting->find('all', array('conditions' => array('PageSetting.module = "home" AND PageSetting.status = 1')));
        $settings_data = array();
        foreach ($page_settings as $setting) {
            $settings_data[$setting['PageSetting']['code']] = $setting['PageSetting'];
        }
        $dynamic_blks = $this->PageSetting->query('select * from dynamics where module="home" and status=1');
        $block_data = array();
        $block_array = array();
        foreach ($dynamic_blks as $blk) {
            $block_data[$blk['dynamics']['block_name']][] = $blk['dynamics'];
            $block_array[$blk['dynamics']['block_name']][] = $blk['dynamics'];
            //$block_string .= 'bn("img",{src:"'.$admin_url.$blk['dynamics']['href'].'"';
        }

        // dynamic block 1 in home
        $block_string1 = '';
        foreach ($block_array['Dynamic part1'] as $block) {
            $block_string1 .= 'bn("img",{src:"' . $admin_url . $block['image'] . '",alt:""}),';
            //print_r($block);
        }

        // dynamic block 2 in home
        $block_string2 = '[';
        foreach ($block_array['Dynamic part2'] as $block) {
            $block_string2 .= '{image:"' . $admin_url . $block['image'] . '",title:"' . $block['title' . $_ar] . '",description:"' . $block['content' . $_ar] . '"},';
            //print_r($block);
        }
        $block_string2 .= ']';


        // dynamic block 3 in home
        $block_string3 = '[';
        foreach ($block_array['Dynamic part3'] as $block) {
            $block_string3 .= '{image:"' . $admin_url . $block['image'] . '",title:"' . $block['title' . $_ar] . '",description:"' . $block['content' . $_ar] . '"},';
            //print_r($block);
        }
        $block_string3 .= ']';

        // dynamic block 3 in home
        $block_string4 = '[';
        foreach ($block_array['Dynamic part4'] as $block) {
            $block_string4 .= '{name:"' . $block['title' . $_ar] . '",description:"' . $block['content' . $_ar] . '",image:"' . $admin_url . $block['image'] . '"},';
            //print_r($block);
        }
        $block_string4 .= ']';

        // dynamic block 3 in home
        $block_string5 = '[';
        foreach ($block_array['Dynamic part5'] as $block) {
            $block_string5 .= '{name:"' . $block['title' . $_ar] . '",description:"' . $block['content' . $_ar] . '",image:"' . $admin_url . $block['image'] . '"},';
            //print_r($block);
        }
        $block_string5 .= ']';
        //print_r($block_string4);die();
        $this->set(compact('settings_data', 'block_data', 'block_string1', 'block_string2', 'block_string3', 'block_string4', 'block_string5'));
        $this->loadModel('Setting');
        $settings = $this->Setting->find('all');
        /* $settings_data = array();
          foreach ($settings as $setting){
          $settings_data[$setting['Setting']['code']] = $setting['Setting'];
          } */


        // footer blocks
        $this->loadModel('FooterCat');
        $footer_cats = $this->FooterCat->find('all');
        $heads = array();
        foreach ($footer_cats as $footercat) {
            $heads[$footercat['FooterCat']['id']] = $footercat['FooterCat'];
        }

        $this->loadModel('FooterLink');
        $footer_links = $this->FooterLink->find('all', array('conditions' => array('FooterLink.status = 1')));
        $links = array();

        foreach ($footer_links as $footerlink) {
            $links[$footerlink['FooterLink']['footer_cat_id']][] = $footerlink['FooterLink'];
        }

        $link1 = '';
        foreach ($links[1] as $link) {
            $link1 .= 'u(i.a, {href:"' . $link['url'] . '"}, u("a", {className: "effect-link ",href: "' . $link['url'] . '"}, u("span", null, "' . $link['title' . $_ar] . '"))),';
        }

        $link2 = '';
        foreach ($links[2] as $link) {
            $link2 .= 'u(i.a, {href:"' . $link['url'] . '"}, u("a", {className: "effect-link ",href: "' . $link['url'] . '"}, u("span", null, "' . $link['title' . $_ar] . '"))),';
        }

        $link3 = '';
        foreach ($links[3] as $link) {
            $link3 .= 'u(i.a, {href:"' . $link['url'] . '"}, u("a", {className: "effect-link ",href: "' . $link['url'] . '"}, u("span", null, "' . $link['title' . $_ar] . '"))),';
        }
        $link4 = '';
        foreach ($links[4] as $link) {
            $link4 .= 'u("a", null,"' . $link['title' . $_ar] . '"),';
        }
        /* u("a", null, "Product Certification"), */
        $this->set(compact('link1', 'link2', 'link3', 'link4'));

        //print_r($links);die();
        $orintation = array(
            "Virtical" => "Virtical",
            "Horizontal" => "Horizontal",
        );
        $colldelevery = array(
            1 => "Collection at ESL front desk",
            2 => "Delivery to:",
        );
        $numbers = array();
        for ($i = 0; $i <= 500; $i++) {
            $numbers[$i] = $i;
        }
        $quantity = array();
        $i = 100;
        while ($i <= 3000) {
            $quantity[$i] = $i;
            $i += 100;
        }
        $this->loadModel('Menue');
        $menus = $this->Menue->find('all', array('conditions' => 'Menue.status = 1 ORDER BY Menue.sorting ASC'));
        $arr = '[';
        foreach ($menus as $menu) {
            $arr .= '{href:"' . $menu['Menue']['href'] . '",label:"' . $menu['Menue']['title' . $_ar] . '"},';
        }
        $menu_lang = 'ara';
        $lang_title = 'العربية';
        if ($this->Session->read('Language.locale') == 'ara') {

            $menu_lang = 'eng';
            $lang_title = 'English';
        }
        //$arr .= '{href:"/esl-frontend/settings/lang_choice/' . $menu_lang . '",label:"' . $lang_title . '"},]';
        $arr .= ']';
        //print_r($arr);die();

        $this->loadModel("Metas");
        $allmetas = $this->Metas->query("Select * From metas Where location='website'");
        $metafooter = array();
        $metaheader = array();
        foreach ($allmetas as $allmeta) {
            if ($allmeta['metas']['position'] == 'header') {
                $metaheader[$allmeta['metas']['id']] = $allmeta['metas']['code_to_embed'];
            } else {
                $metafooter[$allmeta['metas']['id']] = $allmeta['metas']['code_to_embed'];
            }
        }

        $this->set(compact('metaheader', 'metafooter', 'admin_url', 'banners', '_ar', 'settings', 'customer_id', 'orintation', 'numbers', 'colldelevery', 'quantity', 'menus', 'arr', 'heads', 'links'));
    }

    //$this->notify('notification from Portal','internal','|5|','chapters/add');
    public function notify($customer_id = null, $content = null, $type = null, $receivers = null, $url = null) {
        $queryBulk = array();
        foreach (array_unique(explode(',', $receivers)) as $recv) {
            if (strlen($recv) > 2 && $recv != "|0|") {
                $queryBulk[] = '(' . $customer_id . ',"customer","' . $content . '",'
                        . '"' . $type . '","' . $recv . '","user","' . date('Y-m-d H:i:s') . '","' . $url . '",0)';
            }
        }

        $this->loadModel("Notification");
        $this->Notification->query('INSERT INTO notifications(senderid,sender_type,content,type,receivers,receiver_type,created,url,status)'
                . ' VALUES' . implode(',', $queryBulk));
    }

    public function prepare_sms($cusNotf_id, $customer_id, $app_id = 0) {
        //$this->layout = "";
        //$this->autoRender = false;
        $this->loadModel('Customernotification');
        //$noti_msg = $this->Customernotification->query('select type,content from customernotifications where id=' . $cusNotf_id);
        //$content_msg = $noti_msg['0']['customernotifications']['content'];
        $content_email = $this->Customernotification->query('select * from customernotifications where id=' . $cusNotf_id)['0']['customernotifications']['content'];
//		die($phone_no);
        preg_match_all('#\{(.*?)\}#', $content_email, $match);
//print_r($content_email);die();

        foreach ($match[1] as $k => $data) {
            $table_name = explode('.', $data)[0];
            $field_name = explode('.', $data)[1];
            $sql_where = "";
            if ($field_name == 'verification_code') {
                $field_name = str_replace('verification_code', 'corporate_email', $field_name);
            }
//this code did untill create the view
            if (strpos($table_name, 'customers') !== false) {
                $sql_where = ' id=' . $customer_id;
            } else if ($table_name == 'applications') {
                $sql_where = ' id=' . $app_id;
            } else {
                $sql_where = ' application_id=' . $app_id;
            }
            $get_data = $this->Customernotification->query('select ' . $field_name . ' from ' . $table_name . ' where ' . $sql_where)[0][$table_name];
            if ($field_name == 'corporate_email') {
                $get_data[$field_name] = base64_encode($get_data[$field_name]);
            }
            if ($field_name == 'password') {
                $get_data[$field_name] = base64_encode($get_data[$field_name]);
            }
            $content_email = str_replace($match[0][$k], $get_data[$field_name], $content_email);
        }
        return $content_email;
    }

    public function insert_msg($email, $title, $content_email) {
        $this->loadModel('Emailmassege');
        $created = date("Y-m-d H:i:s");
        if (!empty($email) && !empty($title) && !empty($content_email)) {
            $insert = $this->Emailmassege->query("INSERT INTO `emailmasseges`(`id`, `title`, `email`, `content`, `is_sent`, `created`) VALUES (null,'" . $title . "','" . $email . "','" . $content_email . "',0,'" . $created . "')");
        }
    }
    public function send_sms($phone_no, $content_msg) {
        $user = "ELUAE"; //your username
        $password = "02300374"; //your password
//$mobilenumbers="201003134944"; //enter Mobile numbers comma seperated
        $mobilenumbers = $phone_no; //enter Mobile numbers comma seperated
        //$message = 'test'; //enter Your Message
        $message = $content_msg; //enter Your Message
        $senderid = "ESLCertTest"; //Your senderid
        $messagetype = "LNG"; //Type Of Your Message
        $DReports = "Y"; //Delivery Reports
        $url = "https://www.smscountry.com/SMSCwebservice_Bulk.aspx";
        $message = urlencode($message);
        $ch = curl_init();
        if (!$ch) {
            die("Couldn't initialize a cURL handle");
        }
        $ret = curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                "User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//If you are behind proxy then please uncomment below line and provide your proxy ip with port.
// $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
        $curlresponse = curl_exec($ch); // execute
        if (curl_errno($ch))
            echo 'curl error : ' . curl_error($ch);
        if (empty($ret)) {
// some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler
            echo $curlresponse; //echo "Message Sent Succesfully" ;
//echo "Message Sent Succesfully" ;
        }

        echo "<br>";
//		die($content_msg);
    }

    public function send($cusnNotf_id, $customer_id, $app_id = 0) {
        
//        $contentNoTags = str_replace("&nbsp;",' ',strip_tags($content_msg));
//        $contentNoTags = str_replace("&rsquo;","'",strip_tags($content_msg));
            //die($contentNoTags);
        //die('ok');
        $this->loadModel('Customernotification');
        $content_msg = $this->prepare_sms($cusnNotf_id, $customer_id, $app_id);
        //$content_email=$this->prepare_sms($cusnNotf_id,$customer_id,$app_id);
        $customer_data = $this->Customernotification->query('select corporate_email,phon_number,country_id from customers where id=' . $customer_id)['0']['customers'];
        $country_code = $this->Customernotification->query('select code from countries where id = "'.$customer_data['country_id'].'"')[0]['countries']['code'];
        //$phone_no = $this->Customernotification->query('select phon_number from customers where id=' . $customer_id)['0']['customers']['phon_number'];
        $phone_no = $country_code.$customer_data['phon_number'];
        //$email = $this->Customernotification->query('select corporate_email from customers where id=' . $customer_id)['0']['customers']['corporate_email'];
        $email = $customer_data['corporate_email'];
        //$email = 'anastaher43@gmail.com';
        $noti_msg = $this->Customernotification->query('select title,type,content from customernotifications where id=' . $cusnNotf_id);
        $title = $noti_msg[0]['customernotifications']['title'];
        if ($noti_msg[0]['customernotifications']['type'] == 0) {
            $contentNoTags = str_replace("&nbsp;",' ',strip_tags($content_msg));
            $contentNoTags = str_replace("&rsquo;","'",$contentNoTags);
            $contentNoTags = str_replace("&#39;","'",$contentNoTags);
            $this->send_sms($phone_no, $contentNoTags);
        } else {
            //$this->send_email($email,$content_msg);
            $this->insert_msg($email, $title, $content_msg);
        }
    }

}
