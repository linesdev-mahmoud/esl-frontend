<?php
class Faq extends AppModel {
	var $name = 'Faq';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Faqcategory' => array(
			'className' => 'Faqcategory',
			'foreignKey' => 'faqcategory_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
