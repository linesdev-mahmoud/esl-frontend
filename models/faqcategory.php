<?php
class Faqcategory extends AppModel {
	var $name = 'Faqcategory';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
    var $actsAs="Tree";
	var $belongsTo = array(
		'ParentFaqcategory' => array(
			'className' => 'Faqcategory',
			'foreignKey' => 'parent_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $hasMany = array(
		'ChildFaqcategory' => array(
			'className' => 'Faqcategory',
			'foreignKey' => 'parent_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Faq' => array(
			'className' => 'Faq',
			'foreignKey' => 'faqcategory_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
