<?php

class ApagesController extends AppController {

    var $name = 'Apages';
    var $components = array('Uploader');
    var $helpers = array('fck', 'Text');

    /*function index() {
        $this->Apage->recursive = 0;
        $this->paginate['Apage']['conditions'] = 'Apage.type = 1';
        $this->set('apages', $this->paginate());
        $users = array();
        $users_array = $this->Apage->query("select id,username from users");
        foreach ($users_array as $array){
            $users[$array['users']['id']] = $array['users']['username'];
        }
        $this->set(compact('users'));
        //print_r($users);die();
    }*/

    function view($id = null, $type = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid apage', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        if ($type == 1) {
            $this->redirect('/');
        }
        $this->set('apage', $this->Apage->read(null, $id));
        //print_r($this->Apage->read(null, $id));die();
    }

    /*function add() {
        if (!empty($this->data)) {
            $this->data['Apage']['modified_by'] = $this->Session->read('Admin.id');
            $this->data['Apage']['type'] = 2;
            if (isset($this->data['Apage']['image']['tmp_name']) && $this->data['Apage']['image']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Apage']['image'], array('photos'));
                $this->data['Apage']['image'] = $files['urls'][0];
            } else {
                $this->data['Apage']['image'] = "";
            }
            $this->Apage->create();
            if ($this->Apage->save($this->data)) {
                $this->Session->setFlash(__('The apage has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect(array('action' => 'posts'));
            } else {
                $this->Session->setFlash(__('The apage could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            }
        }
    }

    function edit($id = null, $type = null) {
        
        if (!$id) {

            $this->Session->setFlash(__('Invalid page', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            //$this->redirect(array('action' => ''));
            $this->redirect('/');
            //$this->referer();
        }
         if(!$type){
          //$this->Session->setFlash(__('Invalid apage', true));
          $this->redirect('/');
          //$this->referer();
          } 
        if (!empty($this->data)) {
            $this->data['Apage']['modified_by'] = $this->Session->read('Admin.id');
            //print_r($this->data);die();
            if (isset($this->data['Apage']['image1']['tmp_name']) && $this->data['Apage']['image1']['tmp_name'] != "") {

                $file = $this->Uploader->upload('img/uploads/', $this->data['Apage']['image1'], array('photos'));

                $this->data['Apage']['image'] = $file['urls'][0];
            } else {
                unset($this->data['Apage']['image1']);
            }

            if ($this->Apage->save($this->data)) {
                $this->Session->setFlash(__('The page has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                if ($this->data['Apage']['type'] == 1) {
                    $this->redirect(array('action' => 'index'));
                }
                if ($this->data['Apage']['type'] == 2) {
                    $this->redirect(array('action' => 'posts'));
                }
            } else {
                $this->Session->setFlash(__('The apage could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
                if ($this->data['Apage']['type'] == 1) {
                    $this->redirect(array('action' => 'edit', $id, $this->data['Apage']['type']));
                }
                if ($this->data['Apage']['type'] == 2) {
                    $this->redirect(array('action' => 'edit', $id, $this->data['Apage']['type']));
                }
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Apage->read(null, $id);
        }
        $this->set(compact('id', 'type'));
    }

    function delete($id = null, $type = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for apage', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect('/');
        }
        if ($this->Apage->delete($id)) {
            $this->Session->setFlash(__('page deleted', true), 'default', ['class' => 'alert alert-success flashstyle']);
            if ($type == 1) {
                $this->redirect(array('action' => 'index'));
            }
            if ($type == 2) {
                $this->redirect(array('action' => 'posts'));
            }
        }
        $this->Session->setFlash(__('page was not deleted', true), 'default', ['class' => 'alert alert-danger flashstyle']);
        if ($type == 1) {
            $this->redirect(array('action' => 'index'));
        }
        if ($type == 2) {
            $this->redirect(array('action' => 'posts'));
        }
    }*/

    function posts() {
        $this->Apage->recursive = 0;
        $this->paginate['Apage']['conditions'] = 'Apage.type = 2';
        $this->paginate = array(
        'limit' => 6,
        'order' => 'Apage.id DESC',  
        'conditions' =>array('Apage.type = 2')   
    );
        $this->set('apages', $this->paginate());
        $this->loadModel('User');
        $users = $this->User->find('list');
        $this->set(compact('users'));
    }
    
    function contact() {
        $this->layout = '';
        $this->Apage->recursive = 0;
        $this->paginate['Apage']['conditions'] = 'Apage.type = 1 AND Apage.id = 1';
        $this->set('apages', $this->paginate());
        //print_r($this->paginate());die();
    }
    function clients() {
        //$this->layout = '';
        $this->Apage->recursive = 0;
        $this->paginate['Apage']['conditions'] = 'Apage.type = 1 AND Apage.id = 32';
        $this->set('apages', $this->paginate());
        //print_r($this->paginate());die();
    }
    
    function test() {
        $this->layout = '';
        $this->Apage->recursive = 0;
        $this->paginate['Apage']['conditions'] = 'Apage.type = 1 AND Apage.id = 1';
        $this->set('apages', $this->paginate());
        //print_r($this->paginate());die();
    }
    function view_apages($title=null) {
    $title_data = $this->Apage->query("select * from apages where UPPER(url)=UPPER('".$title."')");
    if(empty($title_data)){
       $this->redirect('/'); 
    }
    //print_r($title_data);die();
        $this->set('title_data', $title_data);
    }

}
