<?php

class SettingsController extends AppController {

    var $name = 'Settings';

    function index() {
        $this->Setting->recursive = 0;
        $this->set('settings', $this->paginate());
        //print_r($this->paginate());die();
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid setting', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        $this->set('setting', $this->Setting->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Setting->create();
            if ($this->Setting->save($this->data)) {
                $this->Session->setFlash(__('The setting has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The setting could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            }
        }
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid setting', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Setting->save($this->data)) {
                $this->Session->setFlash(__('The setting has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The setting could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Setting->read(null, $id);
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for setting', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Setting->delete($id)) {
            $this->Session->setFlash(__('Setting deleted', true), 'default', ['class' => 'alert alert-success flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Setting was not deleted', true), 'default', ['class' => 'alert alert-danger flashstyle']);
        $this->redirect(array('action' => 'index'));
    }

    function send() {

        if (!empty($this->data)) {
            //print_r($this->data['Setting']['value']);die();

            $msg = str_replace(" ", "+", $this->data['Setting']['value']);
            $msg = str_replace("ء", "$", $msg);
            //print_r($msg);die();
            //$msg = "اهلا+بك+فى+تطبيق+بناء";
            //die("linesdev.com/nafees/push_notifications/android/send_api.php?message=".$msg."&level_id=".$this->data['Setting']['level_id']);
            //print_r($msg);die();
            $url = "linesdev.com/nafees/push_notifications/android/send_api.php?message=" . $msg . "&level_id=" . $this->data['Setting']['level_id'];
            //die($url);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            //$this->Setting->query("insert into notifications(user_id,message,status)Values(0,'".$this->data['Setting']['value']."',0)");
            $this->redirect(array('action' => ''));
        }
//die("select id,title from levels");
        $levels_query = $this->Setting->query("select id,title from levels");
//print_r($levels_query);die();
        $levels = array();
        foreach ($levels_query as $level) {
            $levels[$level['levels']['id']] = $level['levels']['title'];
        }
        $this->set(compact('levels'));
    }
    
    function lang_choice($lang,$action = null){
        $this->autoRender=false;
        $this->layout='';
        $controller = $this->params['controller'];
        //$action = $this->params['action'];

//die($action);
        $this->Session->write('Language.locale',$lang);

        Configure::write('Config.language', $lang);
        //die($this->Session->read('Language.locale'));
        //        die($this->referer().'?lang='.$this->params['form']['langu']);
        //substr referrer to ?
        if($action == 'display'){
            //die('ara');
            $this->redirect('/');
        }else{
            $url =$this->referer();
            $this->redirect($url);
        }
        //$url =$this->referer();
            //$this->redirect($url);
        //$this->redirect('/');
        //       print_r($url['path']);die();
        

    }

}
