<?php

class UploaderComponent extends CakeObject {

/** 
 * uploads files to the server 
 * @params: 
 *      $folder     = the folder to upload the files e.g. 'img/files' 
 *      $formdata   = the array containing the form files 
 *      $itemId     = id of the item (optional) will create a new sub folder 
 * @return: 
 *      will return an array with the success of each file upload 
 */  
// list of permitted file types, this is only images but documents can be added  

 
	

	function upload($folder, $formdataw,$permitted_value, $itemId = null) {  


	$photos = array('image/gif','image/jpeg','image/pjpeg','image/png','image/png','image/jpeg','image/jpeg','image/jpeg','image/gif','image/bmp','image/vnd.microsoft.icon','image/tiff','image/tiff','image/svg+xml','image/svg+xml',
	
	); 
	//test value only for videos - don't use it (you must change it .... azoz)
        $videos = array('application/octet-stream','audio/x-pn-realaudio-plugin','audio/x-pn-realaudio','application/vnd.rn-realmedia','audio/x-pn-realaudio','audio/mid','audio/x-pn-realaudio','video/asf','video/mp4','application/annodex','application/mp4','application/ogg','application/vnd.rn-realmedia','application/x-matroska','video/3gpp','video/3gpp2','video/annodex','video/divx','video/flv','video/h264','video/mp4','video/mp4v-es','video/mpeg','video/mpeg-2','video/mpeg4','video/ogg','video/ogm','video/quicktime','video/ty','video/vdo','video/vivo','video/vnd.rn-realvideo','video/vnd.vivo','video/webm','video/x-bin','video/x-cdg','video/x-divx','video/x-dv','video/x-flv','video/x-la-asf','video/x-m4v','video/x-matroska','video/x-motion-jpeg','video/x-ms-asf','video/x-ms-dvr','video/x-ms-wm','video/x-ms-wmv','video/x-msvideo','video/x-sgi-movie','video/x-tivo','video/avi','video/x-ms-asx','video/x-ms-wvx','video/x-ms-wmx');
	
	$audios =array('audio/mp3','audio/mpeg','video/quicktime','video/quicktime',"audio/mpeg3","audio/wav","audio/x-aac","audio/x-aiff",'video/mp4','application/mp4','video/mp4v-es');
	//$audios =array('audio/mp3','audio/mpeg',"audio/mpeg3");
	
	
	
	//$files = array('application/vnd.ms-excel');
	$files = array('application/pdf', 'application/x-pdf', 'applications/vnd.pdf', 'text/pdf', 'text/x-pdf');
	
	
	$permitted=array();
	foreach($permitted_value as $asd){
	$permitted =array_merge($permitted,$$asd);
	}

    // setup dir names absolute and relative  

    $folder_url = WWW_ROOT.$folder;  

    $rel_url = $folder;  

     

	  

    // create the folder if it does not exist  
	
    if(!is_dir($folder_url)) {  

        mkdir($folder_url);  

    }  

          

    // if itemId is set create an item folder  

    if($itemId) {  

        // set new absolute folder  

        $folder_url = WWW_ROOT.$folder.'/'.$itemId;   

        // set new relative folder  

        $rel_url = $folder.'/'.$itemId;  

        // create directory  

        if(!is_dir($folder_url)) {  

            mkdir($folder_url);  

        }  

    }  

      


	

	

    // loop through and deal with the files  

	

	

	

	$formdata[0]=$formdataw;

	

	

	

    foreach($formdata as $file) {  

        // replace spaces with underscores 

		

		

		

		

        $filename = str_replace(' ', '_', $file['name']); 

       

	   

	    

	   

		$fileext=strtolower(end(explode('.',$filename)));

				

		$filename=$this->genRandomString().".".$fileext;

		

		// assume filetype is false  

        $typeOK = false;  

        // check filetype is ok 
		$file_type	="";
		

        foreach($permitted as $type) {

            if($type == $file['type']) {  

                $typeOK = true;  
				
				if(in_array($file['type'],$photos)){
				$file_type ="photo";
				}
				elseif(in_array($file['type'],$videos)){
					$file_type ="video";
					}
				elseif(in_array($file['type'],$audios)){
					$file_type ="audio";
					}	
				elseif(in_array($file['type'],$files)){
				$file_type ="file";
				}
				else{
				$file_type = $file['type'];
					}
					
                break;  

            }  

        }  

          

        // if file type ok upload the file  

        if($typeOK) {  

            // switch based on error code  

            switch($file['error']) {  

                case 0:  

                    // check filename already exists  

                    if(!file_exists($folder_url.'/'.$filename)) {  

                        // create full filename  

                        $full_url = $folder_url.'/'.$filename;  

                        $url = $rel_url.'/photo'.$filename;  

                        // upload the file  

                        $success = move_uploaded_file($file['tmp_name'], $url); 

						$url = "file".$filename;

						 

                    } else {  

                        // create unique filename and upload file  

                        ini_set('date.timezone', 'Europe/London');  

                        $now = date('Y-m-d-His');  

                        $full_url = $folder_url.'/'.$now.$filename;  

                        $url = $rel_url.'/file'.$now.$filename;

                        $success = move_uploaded_file($file['tmp_name'], $url); 

						$url = $now.$filename;

						 

                    }  

                    // if upload was successful  

                    if($success) {  

                        // save the url of the file  
                        $result['urls'][] = $url;  
						$result['urls'][1] = $file_type;  
                    } else {  

                        $result['errors'][] = "حدث خطأ اثناء رفع الصورة من فضلك حاول مرة اخرى";  

                    }  

                    break;  

                case 3:  

                    // an error occured  

                    $result['errors'][] = "حدث خطأ اثناء رفع الصورة من فضلك حاول مرة اخرى";  

                    break;  

                default:  

                    // an error occured  

                    $result['errors'][] = "حدث خطأ اثناء رفع الصورة من فضلك حاول مرة اخرى";  

                    break;  

            }  

        } elseif($file['error'] == 4) {  

            // no file was selected for upload  

            $result['nofiles'][] = "لا يوجد ملف للرفع";  

        } else {  

            // unacceptable file type  

            $result['errors'][] = " الامتداد الذى تستخدمه غير متاح للاستخدام ";  

        }  

    }  

return $result;  }



	function genRandomString() {

    $length = 10;

    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';

    $string = "";    



    for ($p = 0; $p < $length; $p++) {

        @$string .= $characters[mt_rand(0, strlen($characters)-1)];

    }



    return $string;}





}