<?php

class Uploader2Component extends CakeObject {

/** 
 * uploads files to the server 
 * @params: 
 *      $folder     = the folder to upload the files e.g. 'img/files' 
 *      $formdata   = the array containing the form files 
 *      $itemId     = id of the item (optional) will create a new sub folder 
 * @return: 
 *      will return an array with the success of each file upload 
 */  
// list of permitted file types, this is only images but documents can be added  

 
	

	function upload($folder, $temp_file,$wtrmrk_file,$will_water=0) {
		
		
		$filename = $temp_file['tmp_name'];
		
		list($width, $height) = getimagesize($filename);
		
		$new_width=$width;
		$new_height=$height;
		
		if($width >= $height )
		{
			if($width > 1000)
			{
				$new_width = 1000;
				$new_height = ($height/$width)*1000;
			}
		}
		
		if($height > $width )
		{
			if($height > 1000)
			{
				$new_height = 1000;
				$new_width = ($width/$height)*1000;
			}
		}
		
		
		$image_p = imagecreatetruecolor($new_width, $new_height);
		$image = imagecreatefromjpeg($filename);
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		
		//$fileext=strtolower(end(explode('.',$filename)));
		$fileext="jpg";
		$filename="photo".$this->genRandomString().".".$fileext;
		
		imagejpeg($image_p, $folder."/".$filename, 50);
		
		
		if($will_water == 1){
		$this->ak_img_watermark($folder."/".$filename,$wtrmrk_file, $folder."/".$filename);
                
		}
		
		
		
		$result['urls'][] = $filename;  
		$result['urls'][1] = "photo";  
		$result['urls'][2] = $new_width;
		$result['urls'][3] = $new_height;
		
                
                
                return $result;
		
		}




function ak_img_watermark($target, $wtrmrk_file, $newcopy) { $watermark = imagecreatefrompng($wtrmrk_file); imagealphablending($watermark, false); imagesavealpha($watermark, true); $img = imagecreatefromjpeg($target); $img_w = imagesx($img); $img_h = imagesy($img); $wtrmrk_w = imagesx($watermark); $wtrmrk_h = imagesy($watermark); $dst_x = ($img_w / 2) - ($wtrmrk_w / 2); // For centering the watermark on any image
$dst_y = ($img_h / 2) - ($wtrmrk_h / 2); // For centering the watermark on any image
imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h); imagejpeg($img, $newcopy, 100); imagedestroy($img); imagedestroy($watermark); } 




	function genRandomString() {

    $length = 15;

    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';

    $string = "";    



    for ($p = 0; $p < $length; $p++) {

        @$string .= $characters[mt_rand(0, strlen($characters)-1)];

    }



    return $string;}





}