<?php

class CustomersdocsController extends AppController {

    var $name = 'Customersdocs';
    var $components = array('Uploader');

    /* function index() {
      $this->redirect(array('controller' => 'customers','action' => 'edit'));

      }

      function view($id = null) {
      $this->redirect(array('controller' => 'customers','action' => 'edit'));
      } */

    function add() {
        $this->layout = "portal";
        if (!empty($this->data)) {
            //print_r($this->data);die();
            $this->data['Customersdoc']['issued_by'] = addslashes(strip_tags($this->data['Customersdoc']['issued_by']));
            //$this->data['Customersdoc']['path'] = addslashes(strip_tags($this->data['Customersdoc']['path']));
            if(!empty($this->data['Customersdoc']['url'])){
            $this->data['Customersdoc']['url'] = addslashes(strip_tags($this->data['Customersdoc']['path']));
        }
            if (isset($this->data['Customersdoc']['path']['tmp_name']) && $this->data['Customersdoc']['path']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Customersdoc']['path'], array('files'));
                $this->data['Customersdoc']['path'] = $files['urls'][0];
            } else {
                $this->data['Customersdoc']['path'] = "";
            }
    
            $this->data['Customersdoc']['modified_by'] = $this->Session->read('Customer.id');
            $this->data['Customersdoc']['customer_id'] = $this->Session->read('Customer.id');


            $this->Customersdoc->create();
            if ($this->Customersdoc->save($this->data)) {
                $this->Session->setFlash(__('The customers doc has been saved', true), 'default', array("class" => "btn-primary"));
                $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
            } else {
                $this->Session->setFlash(__('The customers doc could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
            }
        }
        $customers = $this->Customersdoc->Customer->find('list');
        $documenttypes = $this->Customersdoc->Documenttype->find('list');
        $this->set(compact('customers', 'documenttypes'));
    }

    function edit($id = null) {
        $this->layout = "portal";
        $customer_id = $this->Customersdoc->query("SELECT `customer_id` FROM customersdocs WHERE id=" . $id)[0]['customersdocs']['customer_id'];
        if ($customer_id == $this->Session->read('Customer.id')) {
            if (!$id && empty($this->data)) {
                $this->Session->setFlash(__('Invalid customers doc', true));
                $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
            }
            if (!empty($this->data)) {
                //print_r($this->data);die();
                $this->data['Customersdoc']['issued_by'] = addslashes(strip_tags($this->data['Customersdoc']['issued_by']));
                //$this->data['Customersdoc']['path'] = addslashes(strip_tags($this->data['Customersdoc']['path']));
                $this->data['Customersdoc']['url'] = addslashes(strip_tags($this->data['Customersdoc']['path']));
                if (isset($this->data['Customersdoc']['path1']['tmp_name']) && $this->data['Customersdoc']['path1']['tmp_name'] != "") {
                    $files = $this->Uploader->upload('img/uploads/', $this->data['Customersdoc']['path1'], array('files'));
                    //print_r($files);die();
                    $this->data['Customersdoc']['path'] = $files['urls'][0];
                } else {
                    unset($this->data['Customersdoc']['path1']);
                }

                $this->data['Customersdoc']['modified_by'] = $this->Session->read('Customer.id');

                //				print_r($this->data['Customersdoc']);die;
                if ($this->Customersdoc->save($this->data)) {
                    $this->Session->setFlash(__('The customers doc has been saved', true), 'default', array("class" => "btn-primary"));
                    $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
                } else {
                    $this->Session->setFlash(__('The customers doc could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
                }
            }
            if (empty($this->data)) {
                $this->data = $this->Customersdoc->read(null, $id);
            }
            $customers = $this->Customersdoc->Customer->find('list');
            $documenttypes = $this->Customersdoc->Documenttype->find('list');
            $this->set(compact('customers', 'documenttypes'));
        } else {
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for customers doc', true), 'default', array("class" => "btn-primary"));
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
        if ($this->Customersdoc->delete($id)) {
            $this->Session->setFlash(__('Customers doc deleted', true), 'default', array("class" => "btn-primary"));
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
        $this->Session->setFlash(__('Customers doc was not deleted', true), 'default', array("class" => "btn-danger"));
        $this->redirect(array('action' => 'edit'));
    }

}
