<?php
class ContacttypesController extends AppController {

	var $name = 'Contacttypes';

	function index() {
		$this->Contacttype->recursive = 0;
		$this->set('contacttypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid contacttype', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('contacttype', $this->Contacttype->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Contacttype->create();
			if ($this->Contacttype->save($this->data)) {
				$this->Session->setFlash(__('The contacttype has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contacttype could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid contacttype', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Contacttype->save($this->data)) {
				$this->Session->setFlash(__('The contacttype has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contacttype could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Contacttype->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for contacttype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Contacttype->delete($id)) {
			$this->Session->setFlash(__('Contacttype deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Contacttype was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
