<?php
class FaqsController extends AppController {

    var $name = 'Faqs';
    var $helpers = array('fck', 'Text');

    function index() {
        $this->Faq->recursive = 0;
        $this->set('faqs', $this->paginate());
        if(isset($this->params['form']['sorting_all'])){

            foreach($this->params['form']['sorting'] as $k=>$v){
                //                die('update faqs set sequence = "'.$v.'" where id = "'.$k.'"');
                $this->Faq->query('update faqs set sorting = "'.$v.'" where id = "'.$k.'"');
            }
            $this->redirect(array('action' => 'index'));
        }
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid faq', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('faq', $this->Faq->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
//            print_r($this->data);die;
            if($this->data['Faq']['status']=="on"){
                $this->data['Faq']['status']="1";
            }else{
                $this->data['Faq']['status']="0";
            }
            $this->data['Faq']['modified_by']=$this->Session->read('User.id');

            $this->Faq->create();
            if ($this->Faq->save($this->data)) {
                $this->Session->setFlash(__('The faq has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The faq could not be saved. Please, try again.', true));
            }
        }
        $this->loadModel('Faqcategory');
        $faqcategories = $this->Faqcategory->find('list');
        $this->set(compact('faqcategories'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid faq', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if($this->data['Faq']['status']=="on"){
                $this->data['Faq']['status']="1";
            }else{
                $this->data['Faq']['status']="0";
            }
            $this->data['Faq']['modified_by']=$this->Session->read('User.id');

            if ($this->Faq->save($this->data)) {
                $this->Session->setFlash(__('The faq has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The faq could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Faq->read(null, $id);
        }
        $this->loadModel('Faqcategory');
        $faqcategories = $this->Faqcategory->find('list');
        $this->set(compact('faqcategories'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for faq', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Faq->delete($id)) {
            $this->Session->setFlash(__('Faq deleted', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Faq was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
}
