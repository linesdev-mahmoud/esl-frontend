<?php
class BusinesstypesController extends AppController {

	var $name = 'Businesstypes';

	function index() {
		$this->Businesstype->recursive = 0;
		$this->set('businesstypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid businesstype', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('businesstype', $this->Businesstype->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Businesstype->create();
			if ($this->Businesstype->save($this->data)) {
				$this->Session->setFlash(__('The businesstype has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The businesstype could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid businesstype', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Businesstype->save($this->data)) {
				$this->Session->setFlash(__('The businesstype has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The businesstype could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Businesstype->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for businesstype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Businesstype->delete($id)) {
			$this->Session->setFlash(__('Businesstype deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Businesstype was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
