<?php

class ContactsController extends AppController {

    var $name = 'Contacts';

    /* function index() {
      $this->redirect(array('controller' => 'customers','action' => 'edit'));

      }

      function view($id = null) {
      $this->redirect(array('controller' => 'customers','action' => 'edit'));
      } */

    function add() {
        $this->layout = "portal";
        if (!empty($this->data)) {
            $this->data['Contact']['title'] = addslashes(strip_tags($this->data['Contact']['title']));
            $this->data['Contact']['first_name'] = addslashes(strip_tags($this->data['Contact']['first_name']));
            $this->data['Contact']['last_name'] = addslashes(strip_tags($this->data['Contact']['last_name']));
            $this->data['Contact']['position'] = addslashes(strip_tags($this->data['Contact']['position']));
            $this->data['Contact']['mobile_country_code'] = addslashes(strip_tags($this->data['Contact']['mobile_country_code']));
            $this->data['Contact']['mobile_code_no'] = addslashes(strip_tags($this->data['Contact']['mobile_code_no']));
            $this->data['Contact']['mobile_number'] = addslashes(strip_tags($this->data['Contact']['mobile_number']));
            $this->data['Contact']['email'] = addslashes(strip_tags($this->data['Contact']['email']));
            $this->data['Contact']['password'] = addslashes(strip_tags($this->data['Contact']['password']));
            $this->data['Contact']['modified_by'] = $this->Session->read('Customer.id');
            $this->data['Contact']['customer_id'] = $this->Session->read('Customer.id');
            $this->data['Contact']['password'] = md5($this->data['Contact']['password']);
            $this->Contact->create();
            if ($this->Contact->save($this->data)) {
                $this->Session->setFlash(__('The contact has been saved', true), 'default', array("class" => "btn-primary"));
                $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
            } else {
                $this->Session->setFlash(__('The contact could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
            }
        }
        $customers = $this->Contact->Customer->find('list');

        $this->loadModel('Contacttype');
        $contacttypes = $this->Contacttype->find('list');
        ;

        $this->set(compact('customers', 'contacttypes'));
    }

    function edit($id = null) {
        $this->layout = "portal";
        $customer_id = $this->Contact->query("SELECT  `customer_id` FROM contacts WHERE  id=" . $id)[0]['contacts']['customer_id'];
        if ($customer_id == $this->Session->read('Customer.id')) {

            if (!$id && empty($this->data)) {
                $this->Session->setFlash(__('Invalid contact', true), 'default', array("class" => "btn-danger"));
                $this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                $this->data['Contact']['title'] = addslashes(strip_tags($this->data['Contact']['title']));
                $this->data['Contact']['first_name'] = addslashes(strip_tags($this->data['Contact']['first_name']));
                $this->data['Contact']['last_name'] = addslashes(strip_tags($this->data['Contact']['last_name']));
                $this->data['Contact']['position'] = addslashes(strip_tags($this->data['Contact']['position']));
                $this->data['Contact']['mobile_country_code'] = addslashes(strip_tags($this->data['Contact']['mobile_country_code']));
                $this->data['Contact']['mobile_code_no'] = addslashes(strip_tags($this->data['Contact']['mobile_code_no']));
                $this->data['Contact']['mobile_number'] = addslashes(strip_tags($this->data['Contact']['mobile_number']));
                $this->data['Contact']['email'] = addslashes(strip_tags($this->data['Contact']['email']));
                $this->data['Contact']['password'] = addslashes(strip_tags($this->data['Contact']['password']));
                $this->data['Contact']['modified_by'] = $this->Session->read('Customer.id');
                $this->data['Contact']['customer_id'] = $this->Session->read('Customer.id');
                if ($this->data['Contact']['password'] != "") {
                    if (md5($this->data['Contact']['password']) == $this->data['Contact']['password1']) {
                        $this->data['Contact']['password'] = $this->data['Contact']['password1'];
                    } else {

                        $this->data['Contact']['password'] = md5($this->data['Contact']['password']);
                    }
                } else {
                    unset($this->data['Contact']['password']);
                }

                if ($this->Contact->save($this->data)) {
                    $this->Session->setFlash(__('The contact has been saved', true), 'default', array("class" => "btn-danger"));
                    $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
                } else {
                    $this->Session->setFlash(__('The contact could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
                }
            }
            if (empty($this->data)) {
                $this->data = $this->Contact->read(null, $id);
            }
            $customers = $this->Contact->Customer->find('list');

            $this->loadModel('Contacttype');
            $contacttypes = $this->Contacttype->find('list');
            $this->set(compact('customers', 'contacttypes'));
        } else {
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for contact', true), 'default', array("class" => "btn-danger"));
            $this->redirect(array('action' => 'edit'));
        }
        if ($this->Contact->delete($id)) {
            $this->Session->setFlash(__('Contact deleted', true), 'default', array("class" => "btn-primary"));
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
        $this->Session->setFlash(__('Contact was not deleted', true), 'default', array("class" => "btn-danger"));
        $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
    }

}
