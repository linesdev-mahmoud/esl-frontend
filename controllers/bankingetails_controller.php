<?php

class BankingetailsController extends AppController {
    /* function view($id = null) {
      $this->redirect(array('controller' => 'customers','action' => 'edit'));
      } */

    function add() {
        $this->layout = "portal";
        if (!empty($this->data)) {
            $this->data['Bankingetail']['IBAN_No'] = addslashes(strip_tags($this->data['Bankingetail']['IBAN_No']));
            $this->data['Bankingetail']['Account_No'] = addslashes(strip_tags($this->data['Bankingetail']['Account_No']));
            $this->data['Bankingetail']['Bank_Name'] = addslashes(strip_tags($this->data['Bankingetail']['Bank_Name']));

            $this->data['Bankingetail']['modified_by'] = $this->Session->read('Customer.id');
            $this->data['Bankingetail']['customer_id'] = $this->Session->read('Customer.id');


            $this->Bankingetail->create();
            if ($this->Bankingetail->save($this->data)) {
                $this->Session->setFlash(__('The bankingetail has been saved', true));
                $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
            } else {
                $this->Session->setFlash(__('The bankingetail could not be saved. Please, try again.', true), 'default', array("class" => "btn-primary"));
            }
        }
        $customers = $this->Bankingetail->Customer->find('list');
        $countries = $this->Bankingetail->Country->find('list',array('conditions'=>array('Country.status = 1')));
        $this->set(compact('customers', 'countries'));
    }

    function edit($id = null) {
        $this->layout = "portal";
        $customer_id = $this->Bankingetail->query("SELECT customer_id FROM bankingetails where id=" . $id)[0]['bankingetails']['customer_id'];
        if ($customer_id == $this->Session->read('Customer.id')) {
            if (!$id && empty($this->data)) {
                $this->Session->setFlash(__('Invalid bankingetail', true));
                $this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                $this->data['Bankingetail']['IBAN_No'] = addslashes(strip_tags($this->data['Bankingetail']['IBAN_No']));
                $this->data['Bankingetail']['Account_No'] = addslashes(strip_tags($this->data['Bankingetail']['Account_No']));
                $this->data['Bankingetail']['Bank_Name'] = addslashes(strip_tags($this->data['Bankingetail']['Bank_Name']));
                //			print_r($this->data);die;
                $this->data['Bankingetail']['modified_by'] = $this->Session->read('Customer.id');
                $this->data['Bankingetail']['customer_id'] = $this->Session->read('Customer.id');
                //							print_r($this->data);die;

                if ($this->Bankingetail->save($this->data)) {
                    $this->Session->setFlash(__('The banking detail has been saved', true), 'default', array("class" => "btn-primary"));
                    $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
                } else {
                    $this->Session->setFlash(__('The banking detail could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
                }
            }
            if (empty($this->data)) {
                $this->data = $this->Bankingetail->read(null, $id);
            }
            $customers = $this->Bankingetail->Customer->find('list');
            $countries = $this->Bankingetail->Country->find('list',array('conditions'=>array('Country.status = 1')));
            $this->set(compact('customers', 'countries'));
        } else {
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for banking detail', true), 'default', array("class" => "btn-danger"));
        }
        if ($this->Bankingetail->delete($id)) {
            $this->Session->setFlash(__('Bankingetail deleted', true), 'default', array("class" => "btn-primary"));
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
        $this->Session->setFlash(__('Banking detail was not deleted', true), 'default', array("class" => "btn-danger"));
        $this->redirect(array('action' => 'edit'));
    }

}
