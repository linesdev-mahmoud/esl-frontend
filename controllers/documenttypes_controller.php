<?php
class DocumenttypesController extends AppController {

	var $name = 'Documenttypes';

	function index() {
		$this->Documenttype->recursive = 0;
		$this->set('documenttypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid documenttype', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('documenttype', $this->Documenttype->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Documenttype->create();
			if ($this->Documenttype->save($this->data)) {
				$this->Session->setFlash(__('The documenttype has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The documenttype could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid documenttype', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Documenttype->save($this->data)) {
				$this->Session->setFlash(__('The documenttype has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The documenttype could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Documenttype->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for documenttype', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Documenttype->delete($id)) {
			$this->Session->setFlash(__('Documenttype deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Documenttype was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
