<?php

class AddressesController extends AppController {

    public $name = 'Addresses';

    /* public function index()
      {
      $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
      }

      public function view($id = null)
      {
      $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
      } */

    public function add() {
        $this->layout = "portal";
        if (!empty($this->data)) {
            $this->data['Address']['phone_country_code'] = addslashes(strip_tags($this->data['Address']['phone_country_code']));
            $this->data['Address']['phone_area_code'] = addslashes(strip_tags($this->data['Address']['phone_area_code']));
            $this->data['Address']['phone_number'] = addslashes(strip_tags($this->data['Address']['phone_number']));
            $this->data['Address']['addresses'] = addslashes(strip_tags($this->data['Address']['addresses']));
            $this->data['Address']['po_box'] = addslashes(strip_tags($this->data['Address']['po_box']));
            $this->data['Address']['state_mirates'] = addslashes(strip_tags($this->data['Address']['state_mirates']));
            $this->data['Address']['modified_by'] = $this->Session->read('Customer.id');
            $this->data['Address']['customer_id'] = $this->Session->read('Customer.id');
            $this->Address->create();
            if ($this->Address->save($this->data)) {
                $this->Session->setFlash(__('The address has been saved', true), 'default', array("class" => "btn-primary"));
                $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
            } else {
                $this->Session->setFlash(__('The address could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
            }
        }
        $this->loadModel('Customer');
        $customers = $this->Address->Customer->find('list');
        $this->loadModel('Country');
        $countries = $this->Address->Country->find('list',array('conditions'=>array('Country.status = 1')));
        $this->set(compact('customers', 'countries'));
    }

    public function edit($id = null) {
        $this->layout = "portal";
        $customer_id = $this->Address->query("SELECT customer_id FROM `addresses` where id=" . $id)[0]['addresses']['customer_id'];
        if ($customer_id == $this->Session->read('Customer.id')) {
            if (!$id && empty($this->data)) {
                $this->Session->setFlash(__('Invalid address', true));
                $this->redirect(array('action' => 'edit'));
            }
            if (!empty($this->data)) {
                $this->data['Address']['phone_country_code'] = addslashes(strip_tags($this->data['Address']['phone_country_code']));
                $this->data['Address']['phone_area_code'] = addslashes(strip_tags($this->data['Address']['phone_area_code']));
                $this->data['Address']['phone_number'] = addslashes(strip_tags($this->data['Address']['phone_number']));
                $this->data['Address']['addresses'] = addslashes(strip_tags($this->data['Address']['addresses']));
                $this->data['Address']['po_box'] = addslashes(strip_tags($this->data['Address']['po_box']));
                $this->data['Address']['state_mirates'] = addslashes(strip_tags($this->data['Address']['state_mirates']));
                $this->data['Address']['modified_by'] = $this->Session->read('Customer.id');

                if ($this->Address->save($this->data)) {
                    $this->Session->setFlash(__('The address has been saved', true), 'default', array("class" => "btn-primary"));
                    $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
                } else {
                    $this->Session->setFlash(__('The address could not be saved. Please, try again.', true));
                }
            }
            if (empty($this->data)) {
                $this->data = $this->Address->read(null, $id);
            }
            $this->loadModel('Customer');
            $customers = $this->Address->Customer->find('list');
            $this->loadModel('Country');
            $countries = $this->Address->Country->find('list',array('conditions'=>array('Country.status = 1')));
            $this->set(compact('customers', 'countries'));
        } else {
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
    }

    public function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for address', true));
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
        if ($this->Address->delete($id)) {
            $this->Session->setFlash(__('Address deleted', true));
            $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
        }
        $this->Session->setFlash(__('Address was not deleted', true));
        $this->redirect(array('controller' => 'customers', 'action' => 'edit'));
    }

}
