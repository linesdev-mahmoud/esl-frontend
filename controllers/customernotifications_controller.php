<?php
class CustomernotificationsController extends AppController {

	var $name = 'Customernotifications';
        var $grouping = 'Notifications';
        var $helpers = array('fck','Text');

	function index() {
		$this->Customernotification->recursive = 0;
		$this->set('customernotifications', $this->paginate());
                 $this->loadModel('User');
                $usermod = $this->User->find('list');
		$this->set(compact('usermod'));
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid customernotification', true), 'default', ['class' => 'alert alert-danger flashstyle']);
			$this->redirect(array('action' => 'index'));
		}
		$this->set('customernotification', $this->Customernotification->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
                     if($this->data['Customernotification']['status']=="on"){
		      $this->data['Customernotification']['status']="1";
		  }else{
		      $this->data['Customernotification']['status']="0";
		  }
                  $this->data['Customernotification']['modified_by']=$this->Session->read('User.id');
			$this->Customernotification->create();
			if ($this->Customernotification->save($this->data)) {
				$this->Session->setFlash(__('The customernotification has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customernotification could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid customernotification', true), 'default', ['class' => 'alert alert-danger flashstyle']);
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
                      if($this->data['Customernotification']['status']=="on"){
		      $this->data['Customernotification']['status']="1";
		  }else{
		      $this->data['Customernotification']['status']="0";
		  }
                  $this->data['Customernotification']['modified_by']=$this->Session->read('User.id');
			if ($this->Customernotification->save($this->data)) {
				$this->Session->setFlash(__('The customernotification has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customernotification could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Customernotification->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for customernotification', true), 'default', ['class' => 'alert alert-danger flashstyle']);
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Customernotification->delete($id)) {
			$this->Session->setFlash(__('Customernotification deleted', true), 'default', ['class' => 'alert alert-success flashstyle']);
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Customernotification was not deleted', true), 'default', ['class' => 'alert alert-danger flashstyle']);
		$this->redirect(array('action' => 'index'));
	}
}
