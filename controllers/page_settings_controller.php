<?php

class PageSettingsController extends AppController {

    var $name = 'PageSettings';

    function index($module = null) {
        $this->PageSetting->recursive = 0;
        $this->paginate = array(
            'conditions' => array('PageSetting.module = "' . $module . '"')
        );
        $this->set('pageSettings', $this->paginate());
        $this->loadModel('User');
        $usermod = $this->User->find('list');
        $this->set(compact('usermod'));
    }

    /* function view($id = null) {
      if (!$id) {
      $this->Session->setFlash(__('Invalid page setting', true));
      $this->redirect(array('action' => 'index'));
      }
      $this->set('pageSetting', $this->PageSetting->read(null, $id));
      }

      function add() {
      if (!empty($this->data)) {
      $this->PageSetting->create();
      if ($this->PageSetting->save($this->data)) {
      $this->Session->setFlash(__('The page setting has been saved', true));
      $this->redirect(array('action' => 'index'));
      } else {
      $this->Session->setFlash(__('The page setting could not be saved. Please, try again.', true));
      }
      }
      } */

    /* function edit($id = null, $module = null) {
      if (!$id && empty($this->data)) {
      $this->Session->setFlash(__('Invalid page setting', true));
      $this->redirect('/');
      }
      if (!empty($this->data)) {
      if ($this->data['PageSetting']['status'] == "on") {
      $this->data['PageSetting']['status'] = "1";
      } else {
      $this->data['PageSetting']['status'] = "0";
      }
      $this->data['PageSetting']['modified_by'] = $this->Session->read('User.id');
      if ($this->PageSetting->save($this->data)) {
      $this->Session->setFlash(__('The page setting has been saved', true));
      $this->redirect(array('action' => 'index',$this->data['PageSetting']['module']));
      } else {
      $this->Session->setFlash(__('The page setting could not be saved. Please, try again.', true));
      }
      }
      if (empty($this->data)) {
      $this->data = $this->PageSetting->read(null, $id);
      }
      $this->set(compact('module'));
      } */

    /* function delete($id = null) {
      if (!$id) {
      $this->Session->setFlash(__('Invalid id for page setting', true));
      $this->redirect(array('action' => 'index'));
      }
      if ($this->PageSetting->delete($id)) {
      $this->Session->setFlash(__('Page setting deleted', true));
      $this->redirect(array('action' => 'index'));
      }
      $this->Session->setFlash(__('Page setting was not deleted', true));
      $this->redirect(array('action' => 'index'));
      } */

    function about() {
        //$this->redirect($this->frontend_url.'/about');
        $current_url=$this->getUrl();
        //die($current_url);
        /*if($current_url == 'http://20.74.135.106/page_settings/about'){
            $this->redirect('https://20.74.135.106//about');
        }*/
        $page_settings = $this->PageSetting->find('all', array('conditions' => array('PageSetting.module = "about" AND PageSetting.status = 1')));
        $settings = array();
        foreach ($page_settings as $setting) {
            $settings[$setting['PageSetting']['code']] = $setting['PageSetting'];
        }
        $dynamic_blks = $this->PageSetting->query('select * from dynamics where module="about" and status=1');
        $block_data = array();
        foreach ($dynamic_blks as $blk) {
            $block_data[$blk['dynamics']['block_name']][] = $blk['dynamics'];
        }

        //        print_r($settings);die();
        $this->set(compact('settings', 'block_data'));
    }
    function page_about() {
        $this->redirect($this->frontend_url.'/about');
    }

    function clients() {
        $page_settings = $this->PageSetting->find('all', array('conditions' => array('PageSetting.module = "clients" AND PageSetting.status = 1')));
        $settings = array();
        foreach ($page_settings as $setting) {
            $settings[$setting['PageSetting']['code']] = $setting['PageSetting'];
        }
        $dynamic_blks = $this->PageSetting->query('select * from dynamics where module="clients" and status=1');
        $block_data = array();
        foreach ($dynamic_blks as $blk) {
            $block_data[$blk['dynamics']['block_name']][] = $blk['dynamics'];
        }

        //        print_r($settings);die();
        $this->set(compact('settings', 'block_data'));
    }

    function services() {
        $page_settings = $this->PageSetting->find('all', array('conditions' => array('PageSetting.module = "service" AND PageSetting.status = 1')));
        $settings = array();
        foreach ($page_settings as $setting) {
            $settings[$setting['PageSetting']['code']] = $setting['PageSetting'];
        }
        $dynamic_blks = $this->PageSetting->query('select * from dynamics where module="service" and status=1');
        $block_data = array();
        foreach ($dynamic_blks as $blk) {
            $block_data[$blk['dynamics']['block_name']][] = $blk['dynamics'];
        }

        //                print_r($settings);die();
        $this->set(compact('settings', 'block_data'));
    }

    function how_it_works() {
        //$this->redirect($this->frontend_url.'/how-it-works');
        $page_settings = $this->PageSetting->find('all', array('conditions' => array('PageSetting.module = "how_it_works" AND PageSetting.status = 1')));
        $settings = array();
        foreach ($page_settings as $setting) {
            $settings[$setting['PageSetting']['code']] = $setting['PageSetting'];
        }
        $dynamic_blks = $this->PageSetting->query('select * from dynamics where module="how_it_works" and status=1');
        $block_data = array();
        foreach ($dynamic_blks as $blk) {
            $block_data[$blk['dynamics']['block_name']][] = $blk['dynamics'];
        }

//                        print_r($settings);die();
        $this->set(compact('settings', 'block_data'));
    }

    function contact() {
        $this->loadModel('Country');
        $countries = array();
        $codecountries = array();
        $country = $this->Country->find('all', array('conditions' => array('Country.status = 1')));
        foreach ($country as $countrie) {
            $countries[$countrie['Country']['id']] = $countrie['Country']['title'];
            $codecountries[$countrie['Country']['id']] = $countrie['Country']['code'];
        }
        
        if(isset($this->params['form']['send_contact'])){
            //print_r($this->params['form']);die();
            $subject = 'New contact';
            $message = $this->contact_layout($this->params['form']);
            //die($message);
            /*$message = '<html><head>'
                    . '<style>'
                    . '.title{font-size:20px;font-weight:bold;}'
                    . '</style></head><body>';
                    if(!empty($this->params['form']['request_type'])){
                    $message .= '<div class="title">Request for '.$this->params['form']['request_type'].'</div>';
                    }
                    $message .= '</body></html>';*/
            require '../phpmailer/PHPMailerAutoload.php';
        
        //ask@emirateslaboratory.com
            //$emailto = trim('anastaher43@gmail.com');
            $emailto = trim('ask@emirateslaboratory.com');
            $mail = new PHPMailer;
            $mail->SMTPDebug = $this->SMTPDebug;                               // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $this->Host;  // Specify main and backup SMTP servers
            $mail->SMTPAuth = $this->SMTPAuth;                               // Enable SMTP authentication
            $mail->Username = $this->defaultsenderemail;                 // SMTP username
            $mail->Password = $this->Password;                           // SMTP password
            $mail->SMTPSecure = $this->SMTPSecure;                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = $this->Port;                                    // TCP port to connect to
            $mail->setFrom($this->defaultsenderemail, $this->emailTitle);
            $mail->addAddress($emailto);     // Add a recipient
            $mail->addReplyTo($this->defaultsenderemail, $this->emailTitle);
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $message;
            $mail->AltBody = strip_tags($message);
            //$mail->send();
            //print_r($mail->send());
            if ($mail->send()) {
                $this->Session->setFlash(__('Thanks for contact us', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect('/contact-us');
            } else {
                //die('ok');
            }
        }

        
        //print_r($codecountries);
        $this->set(compact('countries', 'codecountries'));
    }
    
    function certification(){
        //die();
        $page_settings = $this->PageSetting->find('all',array('conditions'=>array('PageSetting.module = "certification" AND PageSetting.status = 1')));
        $settings = array();
        foreach($page_settings as $setting){
            $settings[$setting['PageSetting']['code']]=$setting['PageSetting'];
        }
        $dynamic_blks = $this->PageSetting->query('select * from dynamics where module="certification" and status=1');
        $block_data = array();
        foreach($dynamic_blks as $blk){
            $block_data[$blk['dynamics']['block_name']][]=$blk['dynamics'];
        }

//                        print_r($settings);die();
        $this->set(compact('settings','block_data'));
    }
    
    function contact_layout($form){
        $this->layout = '';
        $countries = array();
        $codecountries = array();
        $country = $this->Country->find('all', array('conditions' => array('Country.status = 1')));
        foreach ($country as $countrie) {
            $countries[$countrie['Country']['id']] = $countrie['Country']['title'];
            $codecountries[$countrie['Country']['id']] = $countrie['Country']['code'];
        }
        $this->set(compact('form','countries','codecountries'));
        
        $content = $this->render('/page_settings/contact_layout');
        //die($content);
        return $content;
    }

}
