<?php

class BannersController extends AppController {

    var $name = 'Banners';
    var $grouping = 'CMS';
    var $components = array('SearchPagination.SearchPagination', 'Uploader');
    var $helpers = array('fck','Text');

    /*function index() {
        $this->Banner->recursive = 0;
        $this->set('banners', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid banner', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        $this->set('banner', $this->Banner->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            if($this->data['Banner']['status']=="on"){
		      $this->data['Banner']['status']="1";
		  }else{
		      $this->data['Banner']['status']="0";
		  }
            if (isset($this->data['Banner']['image']['tmp_name']) && $this->data['Banner']['image']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Banner']['image'], array('photos'));
                $this->data['Banner']['image'] = $files['urls'][0];
            } else {
                $this->data['Banner']['image'] = "";
            }
            $this->Banner->create();
            if ($this->Banner->save($this->data)) {
                $this->Session->setFlash(__('The banner has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The banner could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            }
        }
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid banner', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if($this->data['Banner']['status']=="on"){
		      $this->data['Banner']['status']="1";
		  }else{
		      $this->data['Banner']['status']="0";
		  }
            if (isset($this->data['Banner']['image1']['tmp_name']) && $this->data['Banner']['image1']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Banner']['image1'], array('photos'));
                //print_r($files);die();
                $this->data['Banner']['image'] = $files['urls'][0];
            } else {
                unset($this->data['Banner']['image1']);
            }
            if ($this->Banner->save($this->data)) {
                $this->Session->setFlash(__('The banner has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The banner could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Banner->read(null, $id);
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for banner', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Banner->delete($id)) {
            $this->Session->setFlash(__('Banner deleted', true), 'default', ['class' => 'alert alert-success flashstyle']);
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Banner was not deleted', true), 'default', ['class' => 'alert alert-danger flashstyle']);
        $this->redirect(array('action' => 'index'));
    }*/

}
