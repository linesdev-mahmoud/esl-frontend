<?php

class BusinessrelatesController extends AppController {

    var $name = 'Businessrelates';

    /* function index()
      {
      $this->redirect(array(
      'controller' => 'customers',
      'action' => 'edit'
      ));
      }

      function view($id = null)
      {
      $this->redirect(array(
      'controller' => 'customers',
      'action' => 'edit'
      ));
      } */

    function add() {
        $this->layout = "portal";
        
        if (!empty($this->data)) {
            $this->data['Businessrelate']['website'] = addslashes(strip_tags($this->data['Businessrelate']['website']));
            $this->data['Businessrelate']['supplieraddress'] = addslashes(strip_tags($this->data['Businessrelate']['supplieraddress']));
            $this->data['Businessrelate']['goods'] = serialize($this->data['Businessrelate']['goods']);
            $this->data['Businessrelate']['modified_by'] = $this
                    ->Session
                    ->read('Customer.id');

            $this->data['Businessrelate']['customer_id'] = $this
                    ->Session
                    ->read('Customer.id');
            $this
                    ->Businessrelate
                    ->create();
            if ($this
                            ->Businessrelate
                            ->save($this->data)) {
                $this
                        ->Session
                        ->setFlash(__('The businessrelate has been saved', true), 'default', array("class" => "btn-primary"));
                $this->redirect(array(
                    'controller' => 'customers',
                    'action' => 'edit'
                ));
            } else {
                $this
                        ->Session
                        ->setFlash(__('The businessrelate could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
            }
        }
        $this->loadModel('Good');
        $goods = $this
                ->Good
                ->find('list',array('conditions'=>'Good.status = 1'));
        $customers = $this
                ->Businessrelate
                ->Customer
                ->find('list');
        $businesstypes = $this
                ->Businessrelate
                ->Businesstype
                ->find('list');
        $this->set(compact('customers', 'businesstypes', 'goods'));
    }

    function edit($id = null) {
        $this->layout = "portal";
        //		print_r($this->data['Businessrelate']);die;
        //        $this->data['Businessrelate']['goods'] = unserialize($this->data['Businessrelate']['goods']);
        $customer_id = $this
                        ->Businessrelate
                        ->query("SELECT  `customer_id` FROM `businessrelates` WHERE  id=" . $id) [0]['businessrelates']['customer_id'];
        if ($customer_id == $this
                        ->Session
                        ->read('Customer.id')) {

            //					$this->data['Businessrelate']['goods']=unserialize($this->data['Businessrelate']['goods']);
            if (!$id && empty($this->data)) {
                $this
                        ->Session
                        ->setFlash(__('Invalid businessrelate', true), 'default', array("class" => "btn-danger"));
                $this->redirect(array(
                    'action' => 'index'
                ));
            }
            if (!empty($this->data)) {

                $this->data['Businessrelate']['goods'] = serialize($this->data['Businessrelate']['goods']);
                //								die(unserialize($this->data['Businessrelate']['goods']));


                $this->data['Businessrelate']['modified_by'] = $this
                        ->Session
                        ->read('Customer.id');
                $this->data['Businessrelate']['customer_id'] = $this
                        ->Session
                        ->read('Customer.id');
                //			print_r($this->data['Businessrelate']);die;
                if ($this
                                ->Businessrelate
                                ->save($this->data)) {
                    $this
                            ->Session
                            ->setFlash(__('The businessrelate has been saved', true), 'default', array("class" => "btn-primary"));
                    $this->redirect(array(
                        'controller' => 'customers',
                        'action' => 'edit'
                    ));
                } else {
                    $this
                            ->Session
                            ->setFlash(__('The businessrelate could not be saved. Please, try again.', true), 'default', array("class" => "btn-danger"));
                }
            }
            if (empty($this->data)) {
                $this->data = $this
                        ->Businessrelate
                        ->read(null, $id);
            }

            $this->loadModel('Good');
            $goods = $this
                ->Good
                ->find('list',array('conditions'=>'Good.status = 1'));

            $customers = $this
                    ->Businessrelate
                    ->Customer
                    ->find('list');
            $businesstypes = $this
                    ->Businessrelate
                    ->Businesstype
                    ->find('list');
            $this->set(compact('customers', 'businesstypes', 'goods'));
        } else {
            $this->redirect(array(
                'controller' => 'customers',
                'action' => 'edit'
            ));
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this
                    ->Session
                    ->setFlash(__('Invalid id for business relate', true), 'default', array("class" => "btn-danger"));
            $this->redirect(array(
                'controller' => 'customers',
                'action' => 'edit'
            ));
        }
        if ($this
                        ->Businessrelate
                        ->delete($id)) {
            $this
                    ->Session
                    ->setFlash(__('Business relate deleted', true), 'default', array("class" => "btn-primary"));
            $this->redirect(array(
                'controller' => 'customers',
                'action' => 'edit'
            ));
        }
        $this
                ->Session
                ->setFlash(__('Business relate was not deleted', true), 'default', array("class" => "btn-danger"));
        $this->redirect(array(
            'controller' => 'customers',
            'action' => 'edit'
        ));
    }

}
