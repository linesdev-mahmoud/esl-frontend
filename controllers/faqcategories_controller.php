<?php
class FaqcategoriesController extends AppController {

    var $name = 'Faqcategories';
    var $helpers = array('fck', 'Text');
    function index() {
        /*$this->layout = '';
        $this->Faqcategory->recursive = 0;
        $this->set('faqcategories', $this->paginate());*/
        
        $this->layout = "";
        $parent_data=$this->Faqcategory->query("select * from faqcategories where (parent_id=0 or parent_id IS null) and status =1 order by sorting asc");
        $parents_data=array();
        $childs_data=array();
        foreach($parent_data as $parent){
            $parents_data[$parent['faqcategories']['id']]=$parent['faqcategories'];
            $child_data=$this->Faqcategory->query("select * from faqcategories where parent_id='".$parent['faqcategories']['id']."'and status =1 order by sorting asc");
            //            print_r($parent);die($parent['faqcategories']['id']);
            foreach($child_data as $k=>$child){
                $childs_data[$child['faqcategories']['parent_id']][]=$child['faqcategories'];
            }
        }
        //        print_r($childs_data);die;
        $this->loadModel('PageSetting');
        $page_settings = $this->PageSetting->find('all',array('conditions'=>array('PageSetting.module = "faq" AND PageSetting.status = 1')));
        $settings = array();
        foreach($page_settings as $setting){
            $settings[$setting['PageSetting']['code']]=$setting['PageSetting'];
        }
        $this->set(compact('parents_data','childs_data','settings'));
        
    }

    function view($id_child = null) {
//        if (!$id) {
//            $this->Session->setFlash(__('Invalid faqcategory', true));
//            $this->redirect(array('action' => 'index'));
//        }
        $this->set('faqcategory', $this->Faqcategory->read(null, $id_child));
        //print_r($this->Faqcategory->read(null, $id_child));die();
        $this->layout = "";
        $faqs_data=$this->Faqcategory->query("select * from faqs where faqcategory_id=".$id_child." and status =1 order by sorting asc");
        //$get_cat = $this->Faqcategory->query("select title from faqs where faqcategory_id=".$id_child." and status =1 order by sorting asc");
        //                print_r($faqs_data);die("select * from faqs where faqcategory_id=".$id_child." and status =1 order by sorting");
        $this->set(compact('faqs_data'));
    }

    function add() {
        if (!empty($this->data)) {
            if($this->data['Faqcategory']['status']=="on"){
                $this->data['Faqcategory']['status']="1";
            }else{
                $this->data['Faqcategory']['status']="0";
            }
            $this->data['Faqcategory']['modified_by']=$this->Session->read('User.id');

            $this->Faqcategory->create();
            if ($this->Faqcategory->save($this->data)) {
                $this->Session->setFlash(__('The faqcategory has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The faqcategory could not be saved. Please, try again.', true));
            }
        }
        $parentFaqcategories = $this->Faqcategory->generatetreelist(null, null, null, '-');  
        $this->set(compact('parentFaqcategories'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid faqcategory', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if($this->data['Faqcategory']['status']=="on"){
                $this->data['Faqcategory']['status']="1";
            }else{
                $this->data['Faqcategory']['status']="0";
            }
            $this->data['Faqcategory']['modified_by']=$this->Session->read('User.id');

            if ($this->Faqcategory->save($this->data)) {
                $this->Session->setFlash(__('The faqcategory has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The faqcategory could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Faqcategory->read(null, $id);
        }
        $parentFaqcategories = $this->Faqcategory->generatetreelist(null, null, null, '--');  
        $this->set(compact('parentFaqcategories'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for faqcategory', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Faqcategory->delete($id)) {
            $this->Session->setFlash(__('Faqcategory deleted', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Faqcategory was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
    
    function faq_search() {
        $this->layout = "";
        $keyword = $this->params['url']['keyword'];
        if(isset($keyword)){
            //die('ok');
            $data = $this->Faqcategory->find('all',array('conditions'=>'(Faqcategory.title LIKE "%'.$keyword.'%" OR Faqcategory.title_ar LIKE "%'.$keyword.'%") AND Faqcategory.parent_id > 0'));
            //print_r($data);die();
        }
        $this->set(compact('data'));
    }
}
