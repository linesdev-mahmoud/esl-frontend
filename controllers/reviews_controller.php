<?php

class ReviewsController extends AppController {

    var $name = 'Reviews';
    var $components = array('Uploader');

    function index() {
        $this->Review->recursive = 0;
        $this->set('reviews', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid review', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('review', $this->Review->read(null, $id));
    }

    function add() {
        $this->layout = "portal";
        if (!empty($this->data)) {
            //print_r($this->data);die();
            if (isset($this->data['Review']['product_description']['tmp_name']) && $this->data['Review']['product_description']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_description'], array('files'));
                $this->data['Review']['product_description'] = $files['urls'][0];
            } else {
                $this->data['Review']['product_description'] = "";
            }
            if (isset($this->data['Review']['product_manual']['tmp_name']) && $this->data['Review']['product_manual']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_manual'], array('files'));
                $this->data['Review']['product_manual'] = $files['urls'][0];
            } else {
                $this->data['Review']['product_manual'] = "";
            }
            if (isset($this->data['Review']['technecal_description']['tmp_name']) && $this->data['Review']['technecal_description']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['technecal_description'], array('files'));
                $this->data['Review']['technecal_description'] = $files['urls'][0];
            } else {
                $this->data['Review']['technecal_description'] = "";
            }
            if (isset($this->data['Review']['trade_license']['tmp_name']) && $this->data['Review']['trade_license']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['trade_license'], array('files'));
                $this->data['Review']['trade_license'] = $files['urls'][0];
            } else {
                $this->data['Review']['trade_license'] = "";
            }
            if (isset($this->data['Review']['product_conformity_certificate']['tmp_name']) && $this->data['Review']['product_conformity_certificate']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_conformity_certificate'], array('files'));
                $this->data['Review']['product_conformity_certificate'] = $files['urls'][0];
            } else {
                $this->data['Review']['product_conformity_certificate'] = "";
            }
            if (isset($this->data['Review']['product_coc']['tmp_name']) && $this->data['Review']['product_coc']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_coc'], array('files'));
                $this->data['Review']['product_coc'] = $files['urls'][0];
            } else {
                $this->data['Review']['product_coc'] = "";
            }
            if (isset($this->data['Review']['classification_or_product_test_report']['tmp_name']) && $this->data['Review']['classification_or_product_test_report']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['classification_or_product_test_report'], array('files'));
                $this->data['Review']['classification_or_product_test_report'] = $files['urls'][0];
            } else {
                $this->data['Review']['classification_or_product_test_report'] = "";
            }
            if (isset($this->data['Review']['factory_audit_report']['tmp_name']) && $this->data['Review']['factory_audit_report']['tmp_name'] != "") {
                $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['factory_audit_report'], array('files'));
                $this->data['Review']['factory_audit_report'] = $files['urls'][0];
            } else {
                $this->data['Review']['factory_audit_report'] = "";
            }
            $this->Review->query("INSERT INTO `applications`(`id`, `title`) VALUES (null,'new')");
            $application_id = $this->Review->query("select max(id) as id from applications");
            $this->data['Review']['application_id'] = $application_id[0][0]['id'];
            $this->Review->create();
            if ($this->Review->save($this->data)) {

                for ($i = 0; $i < count($this->data['Review']['sites']['site_name']); $i++) {
                    $this->Review->query("INSERT INTO `sites`(`id`, `application_id`, `name`, `address`) VALUES (null,'" . $application_id[0][0]['id'] . "','" . $this->data['Review']['sites']['site_name'][$i] . "','" . $this->data['Review']['sites']['site_address'][$i] . "')");
                }
                //$review_id = $this->Review->query("select max(id) as id from reviews");

                $this->Session->setFlash(__('The review has been saved', true), 'default', ['class' => 'alert alert-success flashstyle']);
                $this->redirect('/dashboard');
            } else {
                $this->Session->setFlash(__('The review could not be saved. Please, try again.', true), 'default', ['class' => 'alert alert-danger flashstyle']);
            }
        }
        $applications = $this->Review->Application->find('list');
        $certificationbodies = $this->Review->Certificationbody->find('list');
        $this->set(compact('applications', 'certificationbodies'));
    }

    function edit($id = null) {
        $this->layout = "portal";
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid review', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Review->save($this->data)) {
                $this->Session->setFlash(__('The review has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The review could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Review->read(null, $id);
        }
        $applications = $this->Review->Application->find('list');
        $certificationbodies = $this->Review->Certificationbody->find('list');
        $this->set(compact('applications', 'certificationbodies'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for review', true));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Review->delete($id)) {
            $this->Session->setFlash(__('Review deleted', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Review was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }

    function insert() {
        $this->data = $_GET['data'];
        //$explode = explode("&",$data);

        $this->layout = "";
        $this->autoRender = false;
        //print_r($this->data);
        if (isset($this->data['Review']['product_description']['tmp_name']) && $this->data['Review']['product_description']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_description'], array('files'));
            $this->data['Review']['product_description'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_description'] = "";
        }
        if (isset($this->data['Review']['product_manual']['tmp_name']) && $this->data['Review']['product_manual']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_manual'], array('files'));
            $this->data['Review']['product_manual'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_manual'] = "";
        }
        if (isset($this->data['Review']['technecal_description']['tmp_name']) && $this->data['Review']['technecal_description']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['technecal_description'], array('files'));
            $this->data['Review']['technecal_description'] = $files['urls'][0];
        } else {
            $this->data['Review']['technecal_description'] = "";
        }
        if (isset($this->data['Review']['trade_license']['tmp_name']) && $this->data['Review']['trade_license']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['trade_license'], array('files'));
            $this->data['Review']['trade_license'] = $files['urls'][0];
        } else {
            $this->data['Review']['trade_license'] = "";
        }
        if (isset($this->data['Review']['product_conformity_certificate']['tmp_name']) && $this->data['Review']['product_conformity_certificate']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_conformity_certificate'], array('files'));
            $this->data['Review']['product_conformity_certificate'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_conformity_certificate'] = "";
        }
        if (isset($this->data['Review']['product_coc']['tmp_name']) && $this->data['Review']['product_coc']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_coc'], array('files'));
            $this->data['Review']['product_coc'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_coc'] = "";
        }
        if (isset($this->data['Review']['classification_or_product_test_report']['tmp_name']) && $this->data['Review']['classification_or_product_test_report']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['classification_or_product_test_report'], array('files'));
            $this->data['Review']['classification_or_product_test_report'] = $files['urls'][0];
        } else {
            $this->data['Review']['classification_or_product_test_report'] = "";
        }
        if (isset($this->data['Review']['factory_audit_report']['tmp_name']) && $this->data['Review']['factory_audit_report']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['factory_audit_report'], array('files'));
            $this->data['Review']['factory_audit_report'] = $files['urls'][0];
        } else {
            $this->data['Review']['factory_audit_report'] = "";
        }
        $this->Review->query("INSERT INTO `applications`(`id`, `title`) VALUES (null,'new')");
        $application_id = $this->Review->query("select max(id) as id from applications");
        $this->Session->write('app.id', $application_id[0][0]['id']);
        //die($this->Session->read('app.id'));
        $this->data['Review']['application_id'] = $application_id[0][0]['id'];
        $this->Review->create();
        if ($this->Review->save($this->data)) {
            $review_id = $this->Review->query("select max(id) as id from reviews");
            $this->Session->write('review.id', $review_id[0][0]['id']);
            for ($i = 0; $i < count($this->data['Review']['sites']['site_name']); $i++) {
                $this->Review->query("INSERT INTO `sites`(`id`, `application_id`, `name`, `address`) VALUES (null,'" . $application_id[0][0]['id'] . "','" . $this->data['Review']['sites']['site_name'][$i] . "','" . $this->data['Review']['sites']['site_address'][$i] . "')");
            }
            //$review_id = $this->Review->query("select max(id) as id from reviews");
        }
    }

    function update() {
        $id = $this->Session->read('review.id');
        
        $this->layout = "";
        $this->autoRender = false;
        //die($id);
        $this->data = $_GET['data'];
        if (isset($this->data['Review']['product_description']['tmp_name']) && $this->data['Review']['product_description']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_description'], array('files'));
            $this->data['Review']['product_description'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_description'] = "";
        }
        if (isset($this->data['Review']['product_manual']['tmp_name']) && $this->data['Review']['product_manual']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_manual'], array('files'));
            $this->data['Review']['product_manual'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_manual'] = "";
        }
        if (isset($this->data['Review']['technecal_description']['tmp_name']) && $this->data['Review']['technecal_description']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['technecal_description'], array('files'));
            $this->data['Review']['technecal_description'] = $files['urls'][0];
        } else {
            $this->data['Review']['technecal_description'] = "";
        }
        if (isset($this->data['Review']['trade_license']['tmp_name']) && $this->data['Review']['trade_license']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['trade_license'], array('files'));
            $this->data['Review']['trade_license'] = $files['urls'][0];
        } else {
            $this->data['Review']['trade_license'] = "";
        }
        if (isset($this->data['Review']['product_conformity_certificate']['tmp_name']) && $this->data['Review']['product_conformity_certificate']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_conformity_certificate'], array('files'));
            $this->data['Review']['product_conformity_certificate'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_conformity_certificate'] = "";
        }
        if (isset($this->data['Review']['product_coc']['tmp_name']) && $this->data['Review']['product_coc']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['product_coc'], array('files'));
            $this->data['Review']['product_coc'] = $files['urls'][0];
        } else {
            $this->data['Review']['product_coc'] = "";
        }
        if (isset($this->data['Review']['classification_or_product_test_report']['tmp_name']) && $this->data['Review']['classification_or_product_test_report']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['classification_or_product_test_report'], array('files'));
            $this->data['Review']['classification_or_product_test_report'] = $files['urls'][0];
        } else {
            $this->data['Review']['classification_or_product_test_report'] = "";
        }
        if (isset($this->data['Review']['factory_audit_report']['tmp_name']) && $this->data['Review']['factory_audit_report']['tmp_name'] != "") {
            $files = $this->Uploader->upload('img/uploads/', $this->data['Review']['factory_audit_report'], array('files'));
            $this->data['Review']['factory_audit_report'] = $files['urls'][0];
        } else {
            $this->data['Review']['factory_audit_report'] = "";
        }
        print_r($this->data);die();
        if ($this->Review->save($this->data)) {
            //$this->Session->setFlash(__('The review has been saved', true));
            //$this->redirect(array('action' => 'index'));
        }
    }

}
