<?php
/**
 * Routes Configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	//Router::connect('/contact-us', array('controller' => 'apages', 'action' => 'contact'));
	Router::connect('/contact-us', array('controller' => 'page_settings', 'action' => 'contact'));
	Router::connect('/certification', array('controller' => 'page_settings', 'action' => 'certification'));
	Router::connect('/login', array('controller' => 'customers', 'action' => 'login_portal'));
	Router::connect('/profile', array('controller' => 'customers', 'action' => 'edit'));
	Router::connect('/dashboard', array('controller' => 'customers', 'action' => 'dashboard'));
	Router::connect('/logout', array('controller' => 'customers', 'action' => 'logout'));
	Router::connect('/review', array('controller' => 'reviews', 'action' => 'add'));
	Router::connect('/blog', array('controller' => 'apages', 'action' => 'posts'));
	Router::connect('/post/*', array('controller' => 'apages', 'action' => 'view'));
        Router::connect('/verification_email/*', array('controller' => 'customers', 'action' => 'confirm_mail_page'));
        Router::connect('/about', array('controller' => 'page_settings', 'action' => 'about'));
        Router::connect('/clients', array('controller' => 'page_settings', 'action' => 'clients'));
        Router::connect('/services', array('controller' => 'page_settings', 'action' => 'services'));
        Router::connect('/how-it-works', array('controller' => 'page_settings', 'action' => 'how_it_works'));
        Router::connect('/page/*', array('controller' => 'apages', 'action' => 'view_apages'));
        Router::connect('/main-faqs', array('controller' => 'faqcategories', 'action' => 'index'));
        Router::connect('/faq/*', array('controller' => 'faqcategories', 'action' => 'view'));
        Router::connect('/faq-search', array('controller' => 'faqcategories', 'action' => 'faq_search'));
        Router::connect('/directory', array('controller' => 'customers', 'action' => 'directory'));

