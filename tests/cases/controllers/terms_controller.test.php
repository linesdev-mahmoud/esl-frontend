<?php
/* Terms Test cases generated on: 2020-08-11 18:55:56 : 1597164956*/
App::import('Controller', 'Terms');

class TestTermsController extends TermsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TermsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.term', 'app.level', 'app.package', 'app.lesson', 'app.section', 'app.chapter', 'app.subject', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.packages_lesson');

	function startTest() {
		$this->Terms =& new TestTermsController();
		$this->Terms->constructClasses();
	}

	function endTest() {
		unset($this->Terms);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
