<?php
/* ChaptersStandards Test cases generated on: 2021-01-26 09:44:30 : 1611650670*/
App::import('Controller', 'ChaptersStandards');

class TestChaptersStandardsController extends ChaptersStandardsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ChaptersStandardsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.chapters_standard', 'app.chapter', 'app.globalcategory', 'app.standard', 'app.price');

	function startTest() {
		$this->ChaptersStandards =& new TestChaptersStandardsController();
		$this->ChaptersStandards->constructClasses();
	}

	function endTest() {
		unset($this->ChaptersStandards);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
