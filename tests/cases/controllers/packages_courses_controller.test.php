<?php
/* PackagesCourses Test cases generated on: 2020-10-12 15:54:43 : 1602510883*/
App::import('Controller', 'PackagesCourses');

class TestPackagesCoursesController extends PackagesCoursesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PackagesCoursesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.packages_course', 'app.package', 'app.section', 'app.lesson', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.students_section', 'app.packages_section', 'app.course');

	function startTest() {
		$this->PackagesCourses =& new TestPackagesCoursesController();
		$this->PackagesCourses->constructClasses();
	}

	function endTest() {
		unset($this->PackagesCourses);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
