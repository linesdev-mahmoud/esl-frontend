<?php
/* Packages Test cases generated on: 2020-08-11 18:50:16 : 1597164616*/
App::import('Controller', 'Packages');

class TestPackagesController extends PackagesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PackagesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.package', 'app.level', 'app.student', 'app.subject', 'app.term', 'app.lesson', 'app.section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.faq', 'app.packages_lesson');

	function startTest() {
		$this->Packages =& new TestPackagesController();
		$this->Packages->constructClasses();
	}

	function endTest() {
		unset($this->Packages);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
