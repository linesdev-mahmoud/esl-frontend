<?php
/* PackagesLessons Test cases generated on: 2020-08-11 18:51:01 : 1597164661*/
App::import('Controller', 'PackagesLessons');

class TestPackagesLessonsController extends PackagesLessonsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PackagesLessonsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.packages_lesson', 'app.package', 'app.level', 'app.student', 'app.subject', 'app.term', 'app.lesson', 'app.section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.faq');

	function startTest() {
		$this->PackagesLessons =& new TestPackagesLessonsController();
		$this->PackagesLessons->constructClasses();
	}

	function endTest() {
		unset($this->PackagesLessons);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
