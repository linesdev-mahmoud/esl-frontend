<?php
/* SeoAttributes Test cases generated on: 2020-12-22 10:54:11 : 1608630851*/
App::import('Controller', 'SeoAttributes');

class TestSeoAttributesController extends SeoAttributesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class SeoAttributesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.seo_attribute', 'app.seo_urls_attribute');

	function startTest() {
		$this->SeoAttributes =& new TestSeoAttributesController();
		$this->SeoAttributes->constructClasses();
	}

	function endTest() {
		unset($this->SeoAttributes);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
