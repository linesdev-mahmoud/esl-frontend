<?php
/* PackagesSections Test cases generated on: 2020-10-05 16:57:04 : 1601909824*/
App::import('Controller', 'PackagesSections');

class TestPackagesSectionsController extends PackagesSectionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PackagesSectionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.packages_section', 'app.package', 'app.section');

	function startTest() {
		$this->PackagesSections =& new TestPackagesSectionsController();
		$this->PackagesSections->constructClasses();
	}

	function endTest() {
		unset($this->PackagesSections);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
