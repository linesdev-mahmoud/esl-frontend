<?php
/* Students Test cases generated on: 2020-08-11 18:53:43 : 1597164823*/
App::import('Controller', 'Students');

class TestStudentsController extends StudentsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StudentsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.student', 'app.level', 'app.package', 'app.term', 'app.lesson', 'app.section', 'app.chapter', 'app.subject', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.faq', 'app.packages_lesson', 'app.transaction');

	function startTest() {
		$this->Students =& new TestStudentsController();
		$this->Students->constructClasses();
	}

	function endTest() {
		unset($this->Students);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
