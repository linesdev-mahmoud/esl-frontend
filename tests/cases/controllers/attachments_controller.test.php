<?php
/* Attachments Test cases generated on: 2020-08-11 18:48:02 : 1597164482*/
App::import('Controller', 'Attachments');

class TestAttachmentsController extends AttachmentsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class AttachmentsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.attachment', 'app.lesson');

	function startTest() {
		$this->Attachments =& new TestAttachmentsController();
		$this->Attachments->constructClasses();
	}

	function endTest() {
		unset($this->Attachments);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
