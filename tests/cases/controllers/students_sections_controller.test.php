<?php
/* StudentsSections Test cases generated on: 2020-10-06 17:02:19 : 1601996539*/
App::import('Controller', 'StudentsSections');

class TestStudentsSectionsController extends StudentsSectionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StudentsSectionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.students_section', 'app.students', 'app.section', 'app.lesson', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.package', 'app.packages_section');

	function startTest() {
		$this->StudentsSections =& new TestStudentsSectionsController();
		$this->StudentsSections->constructClasses();
	}

	function endTest() {
		unset($this->StudentsSections);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
