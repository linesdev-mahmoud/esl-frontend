<?php
/* Prices Test cases generated on: 2021-01-26 09:47:54 : 1611650874*/
App::import('Controller', 'Prices');

class TestPricesController extends PricesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PricesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.price', 'app.chapters_standard', 'app.chapter', 'app.globalcategory', 'app.standard', 'app.standardsversion', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->Prices =& new TestPricesController();
		$this->Prices->constructClasses();
	}

	function endTest() {
		unset($this->Prices);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
