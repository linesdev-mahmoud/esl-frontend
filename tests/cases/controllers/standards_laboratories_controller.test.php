<?php
/* StandardsLaboratories Test cases generated on: 2021-01-26 09:46:25 : 1611650785*/
App::import('Controller', 'StandardsLaboratories');

class TestStandardsLaboratoriesController extends StandardsLaboratoriesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StandardsLaboratoriesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.standards_laboratory', 'app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->StandardsLaboratories =& new TestStandardsLaboratoriesController();
		$this->StandardsLaboratories->constructClasses();
	}

	function endTest() {
		unset($this->StandardsLaboratories);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
