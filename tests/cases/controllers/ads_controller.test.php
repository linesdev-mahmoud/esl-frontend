<?php
/* Ads Test cases generated on: 2020-08-11 18:46:32 : 1597164392*/
App::import('Controller', 'Ads');

class TestAdsController extends AdsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class AdsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.ad');

	function startTest() {
		$this->Ads =& new TestAdsController();
		$this->Ads->constructClasses();
	}

	function endTest() {
		unset($this->Ads);
		ClassRegistry::flush();
	}

}
