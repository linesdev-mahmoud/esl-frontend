<?php
/* Sections Test cases generated on: 2020-08-11 18:53:13 : 1597164793*/
App::import('Controller', 'Sections');

class TestSectionsController extends SectionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class SectionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.section', 'app.chapter', 'app.subject', 'app.lesson', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.students_exam', 'app.faq', 'app.package', 'app.level', 'app.term', 'app.packages_lesson');

	function startTest() {
		$this->Sections =& new TestSectionsController();
		$this->Sections->constructClasses();
	}

	function endTest() {
		unset($this->Sections);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
