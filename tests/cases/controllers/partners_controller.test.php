<?php
/* Partners Test cases generated on: 2021-01-03 15:27:46 : 1609684066*/
App::import('Controller', 'Partners');

class TestPartnersController extends PartnersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class PartnersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.partner');

	function startTest() {
		$this->Partners =& new TestPartnersController();
		$this->Partners->constructClasses();
	}

	function endTest() {
		unset($this->Partners);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
