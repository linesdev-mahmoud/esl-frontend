<?php
/* Lessons Test cases generated on: 2020-08-11 18:49:20 : 1597164560*/
App::import('Controller', 'Lessons');

class TestLessonsController extends LessonsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class LessonsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.lesson', 'app.section', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.students_exam', 'app.faq', 'app.package', 'app.packages_lesson');

	function startTest() {
		$this->Lessons =& new TestLessonsController();
		$this->Lessons->constructClasses();
	}

	function endTest() {
		unset($this->Lessons);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
