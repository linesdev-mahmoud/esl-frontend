<?php
/* Courses Test cases generated on: 2020-10-12 15:51:55 : 1602510715*/
App::import('Controller', 'Courses');

class TestCoursesController extends CoursesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CoursesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.course', 'app.lesson', 'app.section', 'app.package', 'app.packages_section', 'app.student', 'app.faq', 'app.transaction', 'app.exam', 'app.question', 'app.students_exam', 'app.students_section', 'app.attachment', 'app.packages_course');

	function startTest() {
		$this->Courses =& new TestCoursesController();
		$this->Courses->constructClasses();
	}

	function endTest() {
		unset($this->Courses);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
