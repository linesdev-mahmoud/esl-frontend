<?php
/* Questions Test cases generated on: 2020-08-11 18:52:45 : 1597164765*/
App::import('Controller', 'Questions');

class TestQuestionsController extends QuestionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class QuestionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.question', 'app.exam', 'app.lesson', 'app.section', 'app.attachment', 'app.faq', 'app.student', 'app.package', 'app.level', 'app.subject', 'app.term', 'app.packages_lesson', 'app.students_exam');

	function startTest() {
		$this->Questions =& new TestQuestionsController();
		$this->Questions->constructClasses();
	}

	function endTest() {
		unset($this->Questions);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
