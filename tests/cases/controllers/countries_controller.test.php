<?php
/* Countries Test cases generated on: 2020-10-12 15:51:25 : 1602510685*/
App::import('Controller', 'Countries');

class TestCountriesController extends CountriesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class CountriesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.country', 'app.student', 'app.faq', 'app.lesson', 'app.section', 'app.package', 'app.packages_section', 'app.students_section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.transaction');

	function startTest() {
		$this->Countries =& new TestCountriesController();
		$this->Countries->constructClasses();
	}

	function endTest() {
		unset($this->Countries);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
