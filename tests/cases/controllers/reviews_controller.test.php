<?php
/* Reviews Test cases generated on: 2021-02-06 12:08:36 : 1612609716*/
App::import('Controller', 'Reviews');

class TestReviewsController extends ReviewsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ReviewsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.review', 'app.application', 'app.certificationbody');

	function startTest() {
		$this->Reviews =& new TestReviewsController();
		$this->Reviews->constructClasses();
	}

	function endTest() {
		unset($this->Reviews);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
