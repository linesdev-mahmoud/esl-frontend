<?php
/* Laboratories Test cases generated on: 2021-01-26 09:45:03 : 1611650703*/
App::import('Controller', 'Laboratories');

class TestLaboratoriesController extends LaboratoriesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class LaboratoriesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standard', 'app.standards_laboratory');

	function startTest() {
		$this->Laboratories =& new TestLaboratoriesController();
		$this->Laboratories->constructClasses();
	}

	function endTest() {
		unset($this->Laboratories);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
