<?php
/* Standardsversions Test cases generated on: 2021-01-26 09:47:30 : 1611650850*/
App::import('Controller', 'Standardsversions');

class TestStandardsversionsController extends StandardsversionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StandardsversionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.standardsversion', 'app.standard', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->Standardsversions =& new TestStandardsversionsController();
		$this->Standardsversions->constructClasses();
	}

	function endTest() {
		unset($this->Standardsversions);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
