<?php
/* Testimotionals Test cases generated on: 2020-12-20 12:36:16 : 1608464176*/
App::import('Controller', 'Testimotionals');

class TestTestimotionalsController extends TestimotionalsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TestimotionalsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.testimotional');

	function startTest() {
		$this->Testimotionals =& new TestTestimotionalsController();
		$this->Testimotionals->constructClasses();
	}

	function endTest() {
		unset($this->Testimotionals);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
