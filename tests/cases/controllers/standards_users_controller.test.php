<?php
/* StandardsUsers Test cases generated on: 2021-01-26 09:48:29 : 1611650909*/
App::import('Controller', 'StandardsUsers');

class TestStandardsUsersController extends StandardsUsersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StandardsUsersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.standards_user', 'app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role');

	function startTest() {
		$this->StandardsUsers =& new TestStandardsUsersController();
		$this->StandardsUsers->constructClasses();
	}

	function endTest() {
		unset($this->StandardsUsers);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
