<?php
/* Exams Test cases generated on: 2020-08-11 18:48:40 : 1597164520*/
App::import('Controller', 'Exams');

class TestExamsController extends ExamsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ExamsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.exam', 'app.lesson', 'app.question', 'app.student', 'app.students_exam');

	function startTest() {
		$this->Exams =& new TestExamsController();
		$this->Exams->constructClasses();
	}

	function endTest() {
		unset($this->Exams);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
