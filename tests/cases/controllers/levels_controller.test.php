<?php
/* Levels Test cases generated on: 2020-08-11 18:49:39 : 1597164579*/
App::import('Controller', 'Levels');

class TestLevelsController extends LevelsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class LevelsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.level', 'app.package', 'app.student', 'app.subject', 'app.term');

	function startTest() {
		$this->Levels =& new TestLevelsController();
		$this->Levels->constructClasses();
	}

	function endTest() {
		unset($this->Levels);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
