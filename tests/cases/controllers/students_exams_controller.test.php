<?php
/* StudentsExams Test cases generated on: 2020-08-11 18:54:11 : 1597164851*/
App::import('Controller', 'StudentsExams');

class TestStudentsExamsController extends StudentsExamsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StudentsExamsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.students_exam', 'app.student', 'app.level', 'app.package', 'app.term', 'app.lesson', 'app.section', 'app.chapter', 'app.subject', 'app.attachment', 'app.exam', 'app.question', 'app.faq', 'app.packages_lesson', 'app.transaction');

	function startTest() {
		$this->StudentsExams =& new TestStudentsExamsController();
		$this->StudentsExams->constructClasses();
	}

	function endTest() {
		unset($this->StudentsExams);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
