<?php
/* Chapters Test cases generated on: 2021-01-26 09:44:05 : 1611650645*/
App::import('Controller', 'Chapters');

class TestChaptersController extends ChaptersController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class ChaptersControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.chapter', 'app.globalcategory', 'app.standard', 'app.chapters_standard');

	function startTest() {
		$this->Chapters =& new TestChaptersController();
		$this->Chapters->constructClasses();
	}

	function endTest() {
		unset($this->Chapters);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
