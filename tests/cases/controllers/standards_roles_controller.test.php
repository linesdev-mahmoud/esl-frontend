<?php
/* StandardsRoles Test cases generated on: 2021-01-26 09:46:55 : 1611650815*/
App::import('Controller', 'StandardsRoles');

class TestStandardsRolesController extends StandardsRolesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class StandardsRolesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.standards_role', 'app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_user');

	function startTest() {
		$this->StandardsRoles =& new TestStandardsRolesController();
		$this->StandardsRoles->constructClasses();
	}

	function endTest() {
		unset($this->StandardsRoles);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
