<?php
/* Globalcategories Test cases generated on: 2021-01-26 09:43:26 : 1611650606*/
App::import('Controller', 'Globalcategories');

class TestGlobalcategoriesController extends GlobalcategoriesController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class GlobalcategoriesControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.globalcategory', 'app.chapter');

	function startTest() {
		$this->Globalcategories =& new TestGlobalcategoriesController();
		$this->Globalcategories->constructClasses();
	}

	function endTest() {
		unset($this->Globalcategories);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
