<?php
/* Transactions Test cases generated on: 2020-08-11 18:56:49 : 1597165009*/
App::import('Controller', 'Transactions');

class TestTransactionsController extends TransactionsController {
	var $autoRender = false;

	function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

class TransactionsControllerTestCase extends CakeTestCase {
	var $fixtures = array('app.transaction', 'app.student', 'app.level', 'app.package', 'app.term', 'app.lesson', 'app.section', 'app.chapter', 'app.subject', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.faq', 'app.packages_lesson');

	function startTest() {
		$this->Transactions =& new TestTransactionsController();
		$this->Transactions->constructClasses();
	}

	function endTest() {
		unset($this->Transactions);
		ClassRegistry::flush();
	}

	function testIndex() {

	}

	function testView() {

	}

	function testAdd() {

	}

	function testEdit() {

	}

	function testDelete() {

	}

}
