<?php
/* StandardsRole Test cases generated on: 2021-01-26 09:46:53 : 1611650813*/
App::import('Model', 'StandardsRole');

class StandardsRoleTestCase extends CakeTestCase {
	var $fixtures = array('app.standards_role', 'app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_user');

	function startTest() {
		$this->StandardsRole =& ClassRegistry::init('StandardsRole');
	}

	function endTest() {
		unset($this->StandardsRole);
		ClassRegistry::flush();
	}

}
