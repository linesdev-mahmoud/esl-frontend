<?php
/* Standard Test cases generated on: 2021-01-26 09:45:40 : 1611650740*/
App::import('Model', 'Standard');

class StandardTestCase extends CakeTestCase {
	var $fixtures = array('app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->Standard =& ClassRegistry::init('Standard');
	}

	function endTest() {
		unset($this->Standard);
		ClassRegistry::flush();
	}

}
