<?php
/* PackagesLesson Test cases generated on: 2020-08-11 18:50:44 : 1597164644*/
App::import('Model', 'PackagesLesson');

class PackagesLessonTestCase extends CakeTestCase {
	var $fixtures = array('app.packages_lesson', 'app.package', 'app.level', 'app.student', 'app.subject', 'app.term', 'app.lesson', 'app.section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.faq');

	function startTest() {
		$this->PackagesLesson =& ClassRegistry::init('PackagesLesson');
	}

	function endTest() {
		unset($this->PackagesLesson);
		ClassRegistry::flush();
	}

}
