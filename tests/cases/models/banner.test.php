<?php
/* Banner Test cases generated on: 2020-12-23 10:53:22 : 1608717202*/
App::import('Model', 'Banner');

class BannerTestCase extends CakeTestCase {
	var $fixtures = array('app.banner');

	function startTest() {
		$this->Banner =& ClassRegistry::init('Banner');
	}

	function endTest() {
		unset($this->Banner);
		ClassRegistry::flush();
	}

}
