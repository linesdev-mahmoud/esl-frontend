<?php
/* Partner Test cases generated on: 2021-01-03 15:27:31 : 1609684051*/
App::import('Model', 'Partner');

class PartnerTestCase extends CakeTestCase {
	var $fixtures = array('app.partner');

	function startTest() {
		$this->Partner =& ClassRegistry::init('Partner');
	}

	function endTest() {
		unset($this->Partner);
		ClassRegistry::flush();
	}

}
