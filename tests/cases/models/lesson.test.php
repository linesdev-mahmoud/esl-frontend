<?php
/* Lesson Test cases generated on: 2020-08-11 18:49:12 : 1597164552*/
App::import('Model', 'Lesson');

class LessonTestCase extends CakeTestCase {
	var $fixtures = array('app.lesson', 'app.section', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.students_exam', 'app.faq', 'app.package', 'app.packages_lesson');

	function startTest() {
		$this->Lesson =& ClassRegistry::init('Lesson');
	}

	function endTest() {
		unset($this->Lesson);
		ClassRegistry::flush();
	}

}
