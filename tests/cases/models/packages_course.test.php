<?php
/* PackagesCourse Test cases generated on: 2020-10-12 15:54:28 : 1602510868*/
App::import('Model', 'PackagesCourse');

class PackagesCourseTestCase extends CakeTestCase {
	var $fixtures = array('app.packages_course', 'app.package', 'app.section', 'app.lesson', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.students_section', 'app.packages_section', 'app.course');

	function startTest() {
		$this->PackagesCourse =& ClassRegistry::init('PackagesCourse');
	}

	function endTest() {
		unset($this->PackagesCourse);
		ClassRegistry::flush();
	}

}
