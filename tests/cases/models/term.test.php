<?php
/* Term Test cases generated on: 2020-08-11 18:54:54 : 1597164894*/
App::import('Model', 'Term');

class TermTestCase extends CakeTestCase {
	var $fixtures = array('app.term', 'app.level', 'app.package', 'app.lesson', 'app.section', 'app.chapter', 'app.subject', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.packages_lesson');

	function startTest() {
		$this->Term =& ClassRegistry::init('Term');
	}

	function endTest() {
		unset($this->Term);
		ClassRegistry::flush();
	}

}
