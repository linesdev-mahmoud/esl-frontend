<?php
/* Price Test cases generated on: 2021-01-26 09:47:52 : 1611650872*/
App::import('Model', 'Price');

class PriceTestCase extends CakeTestCase {
	var $fixtures = array('app.price', 'app.chapters_standard', 'app.chapter', 'app.globalcategory', 'app.standard', 'app.standardsversion', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->Price =& ClassRegistry::init('Price');
	}

	function endTest() {
		unset($this->Price);
		ClassRegistry::flush();
	}

}
