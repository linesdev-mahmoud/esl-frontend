<?php
/* Student Test cases generated on: 2020-10-12 16:31:17 : 1602513077*/
App::import('Model', 'Student');

class StudentTestCase extends CakeTestCase {
	var $fixtures = array('app.student', 'app.faq', 'app.lesson', 'app.section', 'app.package', 'app.packages_section', 'app.students_section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.transaction');

	function startTest() {
		$this->Student =& ClassRegistry::init('Student');
	}

	function endTest() {
		unset($this->Student);
		ClassRegistry::flush();
	}

}
