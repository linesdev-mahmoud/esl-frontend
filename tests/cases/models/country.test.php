<?php
/* Country Test cases generated on: 2020-10-12 15:51:07 : 1602510667*/
App::import('Model', 'Country');

class CountryTestCase extends CakeTestCase {
	var $fixtures = array('app.country', 'app.student', 'app.faq', 'app.lesson', 'app.section', 'app.package', 'app.packages_section', 'app.students_section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.transaction');

	function startTest() {
		$this->Country =& ClassRegistry::init('Country');
	}

	function endTest() {
		unset($this->Country);
		ClassRegistry::flush();
	}

}
