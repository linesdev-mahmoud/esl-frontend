<?php
/* Exam Test cases generated on: 2020-08-11 18:48:32 : 1597164512*/
App::import('Model', 'Exam');

class ExamTestCase extends CakeTestCase {
	var $fixtures = array('app.exam', 'app.lesson', 'app.question', 'app.student', 'app.students_exam');

	function startTest() {
		$this->Exam =& ClassRegistry::init('Exam');
	}

	function endTest() {
		unset($this->Exam);
		ClassRegistry::flush();
	}

}
