<?php
/* Package Test cases generated on: 2020-10-12 15:53:28 : 1602510808*/
App::import('Model', 'Package');

class PackageTestCase extends CakeTestCase {
	var $fixtures = array('app.package', 'app.section', 'app.lesson', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.students_section', 'app.packages_section');

	function startTest() {
		$this->Package =& ClassRegistry::init('Package');
	}

	function endTest() {
		unset($this->Package);
		ClassRegistry::flush();
	}

}
