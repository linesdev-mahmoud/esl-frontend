<?php
/* Standardsversion Test cases generated on: 2021-01-26 09:47:28 : 1611650848*/
App::import('Model', 'Standardsversion');

class StandardsversionTestCase extends CakeTestCase {
	var $fixtures = array('app.standardsversion', 'app.standard', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->Standardsversion =& ClassRegistry::init('Standardsversion');
	}

	function endTest() {
		unset($this->Standardsversion);
		ClassRegistry::flush();
	}

}
