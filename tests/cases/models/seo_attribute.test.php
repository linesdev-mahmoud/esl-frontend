<?php
/* SeoAttribute Test cases generated on: 2020-12-22 10:54:01 : 1608630841*/
App::import('Model', 'SeoAttribute');

class SeoAttributeTestCase extends CakeTestCase {
	var $fixtures = array('app.seo_attribute', 'app.seo_urls_attribute');

	function startTest() {
		$this->SeoAttribute =& ClassRegistry::init('SeoAttribute');
	}

	function endTest() {
		unset($this->SeoAttribute);
		ClassRegistry::flush();
	}

}
