<?php
/* Laboratory Test cases generated on: 2021-01-26 09:45:01 : 1611650701*/
App::import('Model', 'Laboratory');

class LaboratoryTestCase extends CakeTestCase {
	var $fixtures = array('app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standard', 'app.standards_laboratory');

	function startTest() {
		$this->Laboratory =& ClassRegistry::init('Laboratory');
	}

	function endTest() {
		unset($this->Laboratory);
		ClassRegistry::flush();
	}

}
