<?php
/* StudentsSection Test cases generated on: 2020-10-06 17:21:01 : 1601997661*/
App::import('Model', 'StudentsSection');

class StudentsSectionTestCase extends CakeTestCase {
	var $fixtures = array('app.students_section', 'app.student', 'app.faq', 'app.lesson', 'app.section', 'app.package', 'app.packages_section', 'app.attachment', 'app.exam', 'app.question', 'app.students_exam', 'app.transaction');

	function startTest() {
		$this->StudentsSection =& ClassRegistry::init('StudentsSection');
	}

	function endTest() {
		unset($this->StudentsSection);
		ClassRegistry::flush();
	}

}
