<?php
/* Faq Test cases generated on: 2020-08-11 18:48:53 : 1597164533*/
App::import('Model', 'Faq');

class FaqTestCase extends CakeTestCase {
	var $fixtures = array('app.faq', 'app.lesson', 'app.student');

	function startTest() {
		$this->Faq =& ClassRegistry::init('Faq');
	}

	function endTest() {
		unset($this->Faq);
		ClassRegistry::flush();
	}

}
