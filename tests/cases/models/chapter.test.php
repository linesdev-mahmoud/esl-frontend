<?php
/* Chapter Test cases generated on: 2021-01-26 09:44:04 : 1611650644*/
App::import('Model', 'Chapter');

class ChapterTestCase extends CakeTestCase {
	var $fixtures = array('app.chapter', 'app.globalcategory', 'app.standard', 'app.chapters_standard');

	function startTest() {
		$this->Chapter =& ClassRegistry::init('Chapter');
	}

	function endTest() {
		unset($this->Chapter);
		ClassRegistry::flush();
	}

}
