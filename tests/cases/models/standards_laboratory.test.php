<?php
/* StandardsLaboratory Test cases generated on: 2021-01-26 09:46:23 : 1611650783*/
App::import('Model', 'StandardsLaboratory');

class StandardsLaboratoryTestCase extends CakeTestCase {
	var $fixtures = array('app.standards_laboratory', 'app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role', 'app.standards_user');

	function startTest() {
		$this->StandardsLaboratory =& ClassRegistry::init('StandardsLaboratory');
	}

	function endTest() {
		unset($this->StandardsLaboratory);
		ClassRegistry::flush();
	}

}
