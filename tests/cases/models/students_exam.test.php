<?php
/* StudentsExam Test cases generated on: 2020-08-11 18:54:02 : 1597164842*/
App::import('Model', 'StudentsExam');

class StudentsExamTestCase extends CakeTestCase {
	var $fixtures = array('app.students_exam', 'app.student', 'app.level', 'app.package', 'app.term', 'app.lesson', 'app.section', 'app.chapter', 'app.subject', 'app.attachment', 'app.exam', 'app.question', 'app.faq', 'app.packages_lesson', 'app.transaction');

	function startTest() {
		$this->StudentsExam =& ClassRegistry::init('StudentsExam');
	}

	function endTest() {
		unset($this->StudentsExam);
		ClassRegistry::flush();
	}

}
