<?php
/* PackagesSection Test cases generated on: 2020-10-05 16:56:52 : 1601909812*/
App::import('Model', 'PackagesSection');

class PackagesSectionTestCase extends CakeTestCase {
	var $fixtures = array('app.packages_section', 'app.package', 'app.section');

	function startTest() {
		$this->PackagesSection =& ClassRegistry::init('PackagesSection');
	}

	function endTest() {
		unset($this->PackagesSection);
		ClassRegistry::flush();
	}

}
