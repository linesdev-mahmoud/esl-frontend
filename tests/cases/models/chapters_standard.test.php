<?php
/* ChaptersStandard Test cases generated on: 2021-01-26 09:44:27 : 1611650667*/
App::import('Model', 'ChaptersStandard');

class ChaptersStandardTestCase extends CakeTestCase {
	var $fixtures = array('app.chapters_standard', 'app.chapter', 'app.globalcategory', 'app.standard', 'app.price');

	function startTest() {
		$this->ChaptersStandard =& ClassRegistry::init('ChaptersStandard');
	}

	function endTest() {
		unset($this->ChaptersStandard);
		ClassRegistry::flush();
	}

}
