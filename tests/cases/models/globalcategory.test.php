<?php
/* Globalcategory Test cases generated on: 2021-01-26 09:43:25 : 1611650605*/
App::import('Model', 'Globalcategory');

class GlobalcategoryTestCase extends CakeTestCase {
	var $fixtures = array('app.globalcategory', 'app.chapter');

	function startTest() {
		$this->Globalcategory =& ClassRegistry::init('Globalcategory');
	}

	function endTest() {
		unset($this->Globalcategory);
		ClassRegistry::flush();
	}

}
