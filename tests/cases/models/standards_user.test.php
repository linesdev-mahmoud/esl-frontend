<?php
/* StandardsUser Test cases generated on: 2021-01-26 09:48:18 : 1611650898*/
App::import('Model', 'StandardsUser');

class StandardsUserTestCase extends CakeTestCase {
	var $fixtures = array('app.standards_user', 'app.standard', 'app.standardsversion', 'app.chapter', 'app.globalcategory', 'app.chapters_standard', 'app.laboratory', 'app.country', 'app.address', 'app.customer', 'app.bankingetail', 'app.businessrelate', 'app.businesstype', 'app.contact', 'app.contacttypes', 'app.customersdoc', 'app.documenttype', 'app.standards_laboratory', 'app.role', 'app.approval', 'app.user', 'app.department', 'app.workflow', 'app.users_role', 'app.customernotification', 'app.approvaldocument', 'app.approvalsegment', 'app.permission', 'app.roles_permission', 'app.standards_role');

	function startTest() {
		$this->StandardsUser =& ClassRegistry::init('StandardsUser');
	}

	function endTest() {
		unset($this->StandardsUser);
		ClassRegistry::flush();
	}

}
