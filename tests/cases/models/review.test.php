<?php
/* Review Test cases generated on: 2021-02-06 12:08:35 : 1612609715*/
App::import('Model', 'Review');

class ReviewTestCase extends CakeTestCase {
	var $fixtures = array('app.review', 'app.application', 'app.certificationbody');

	function startTest() {
		$this->Review =& ClassRegistry::init('Review');
	}

	function endTest() {
		unset($this->Review);
		ClassRegistry::flush();
	}

}
