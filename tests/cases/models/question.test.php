<?php
/* Question Test cases generated on: 2020-08-11 18:52:33 : 1597164753*/
App::import('Model', 'Question');

class QuestionTestCase extends CakeTestCase {
	var $fixtures = array('app.question', 'app.exam', 'app.lesson', 'app.section', 'app.attachment', 'app.faq', 'app.student', 'app.package', 'app.level', 'app.subject', 'app.term', 'app.packages_lesson', 'app.students_exam');

	function startTest() {
		$this->Question =& ClassRegistry::init('Question');
	}

	function endTest() {
		unset($this->Question);
		ClassRegistry::flush();
	}

}
