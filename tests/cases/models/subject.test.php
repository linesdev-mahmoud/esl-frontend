<?php
/* Subject Test cases generated on: 2020-08-11 18:54:27 : 1597164867*/
App::import('Model', 'Subject');

class SubjectTestCase extends CakeTestCase {
	var $fixtures = array('app.subject', 'app.level', 'app.package', 'app.term', 'app.lesson', 'app.section', 'app.chapter', 'app.attachment', 'app.exam', 'app.question', 'app.student', 'app.faq', 'app.transaction', 'app.students_exam', 'app.packages_lesson');

	function startTest() {
		$this->Subject =& ClassRegistry::init('Subject');
	}

	function endTest() {
		unset($this->Subject);
		ClassRegistry::flush();
	}

}
