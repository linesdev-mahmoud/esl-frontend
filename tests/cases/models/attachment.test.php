<?php
/* Attachment Test cases generated on: 2020-08-11 18:47:54 : 1597164474*/
App::import('Model', 'Attachment');

class AttachmentTestCase extends CakeTestCase {
	var $fixtures = array('app.attachment', 'app.lesson');

	function startTest() {
		$this->Attachment =& ClassRegistry::init('Attachment');
	}

	function endTest() {
		unset($this->Attachment);
		ClassRegistry::flush();
	}

}
