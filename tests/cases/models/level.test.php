<?php
/* Level Test cases generated on: 2020-08-11 18:49:31 : 1597164571*/
App::import('Model', 'Level');

class LevelTestCase extends CakeTestCase {
	var $fixtures = array('app.level', 'app.package', 'app.student', 'app.subject', 'app.term');

	function startTest() {
		$this->Level =& ClassRegistry::init('Level');
	}

	function endTest() {
		unset($this->Level);
		ClassRegistry::flush();
	}

}
