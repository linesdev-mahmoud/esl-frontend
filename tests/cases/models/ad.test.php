<?php
/* Ad Test cases generated on: 2020-08-11 18:46:19 : 1597164379*/
App::import('Model', 'Ad');

class AdTestCase extends CakeTestCase {
	var $fixtures = array('app.ad');

	function startTest() {
		$this->Ad =& ClassRegistry::init('Ad');
	}

	function endTest() {
		unset($this->Ad);
		ClassRegistry::flush();
	}

}
