<?php
/* Course Test cases generated on: 2020-10-12 15:51:44 : 1602510704*/
App::import('Model', 'Course');

class CourseTestCase extends CakeTestCase {
	var $fixtures = array('app.course', 'app.lesson', 'app.section', 'app.package', 'app.packages_section', 'app.student', 'app.faq', 'app.transaction', 'app.exam', 'app.question', 'app.students_exam', 'app.students_section', 'app.attachment', 'app.packages_course');

	function startTest() {
		$this->Course =& ClassRegistry::init('Course');
	}

	function endTest() {
		unset($this->Course);
		ClassRegistry::flush();
	}

}
