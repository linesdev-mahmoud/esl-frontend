<?php
/* StandardsUser Fixture generated on: 2021-01-26 09:48:18 : 1611650898 */
class StandardsUserFixture extends CakeTestFixture {
	var $name = 'StandardsUser';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'standard_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'sequence' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'purpose' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'modify_by' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'standard_id' => 1,
			'user_id' => 1,
			'sequence' => 1,
			'purpose' => 'Lorem ipsum dolor sit amet',
			'created' => '2021-01-26 09:48:18',
			'modify_by' => 1,
			'status' => 1
		),
	);
}
