<?php
/* StandardsRole Fixture generated on: 2021-01-26 09:46:53 : 1611650813 */
class StandardsRoleFixture extends CakeTestFixture {
	var $name = 'StandardsRole';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'standard_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'role_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'modified_by' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'standard_id' => 1,
			'role_id' => 1,
			'created' => '2021-01-26 09:46:53',
			'modified_by' => 1
		),
	);
}
