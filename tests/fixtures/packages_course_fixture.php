<?php
/* PackagesCourse Fixture generated on: 2020-10-12 15:54:28 : 1602510868 */
class PackagesCourseFixture extends CakeTestFixture {
	var $name = 'PackagesCourse';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'package_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'course_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'package_id' => 1,
			'course_id' => 1
		),
	);
}
