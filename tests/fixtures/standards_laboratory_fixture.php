<?php
/* StandardsLaboratory Fixture generated on: 2021-01-26 09:46:23 : 1611650783 */
class StandardsLaboratoryFixture extends CakeTestFixture {
	var $name = 'StandardsLaboratory';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'standard_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'laboratory_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'comments' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 255),
		'modified_by' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'standard_id' => 1,
			'laboratory_id' => 1,
			'comments' => 1,
			'modified_by' => 1,
			'created' => '2021-01-26 09:46:23'
		),
	);
}
