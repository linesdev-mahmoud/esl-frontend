<?php
/* Review Fixture generated on: 2021-02-06 12:08:35 : 1612609715 */
class ReviewFixture extends CakeTestFixture {
	var $name = 'Review';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'application_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'can_label' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'can_mark' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'product_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'product_type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'product_manufacturer' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'certificate_no' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'issued_on' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'revision_or_version' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'expiry_date' => array('type' => 'date', 'null' => true, 'default' => NULL),
		'certificationbody_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'product_description' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'product_manual' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'technecal_description' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'trade_license' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'product_conformity_certificate' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'product_coc' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'classification_or_product_test_report' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'factory_audit_report' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'modified_by' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'application_id' => 1,
			'can_label' => 1,
			'can_mark' => 1,
			'product_name' => 'Lorem ipsum dolor sit amet',
			'product_type' => 'Lorem ipsum dolor sit amet',
			'product_manufacturer' => 'Lorem ipsum dolor sit amet',
			'certificate_no' => 'Lorem ipsum dolor sit amet',
			'issued_on' => '2021-02-06',
			'revision_or_version' => 'Lorem ipsum dolor sit amet',
			'expiry_date' => '2021-02-06',
			'certificationbody_id' => 1,
			'product_description' => 'Lorem ipsum dolor sit amet',
			'product_manual' => 'Lorem ipsum dolor sit amet',
			'technecal_description' => 'Lorem ipsum dolor sit amet',
			'trade_license' => 'Lorem ipsum dolor sit amet',
			'product_conformity_certificate' => 'Lorem ipsum dolor sit amet',
			'product_coc' => 'Lorem ipsum dolor sit amet',
			'classification_or_product_test_report' => 'Lorem ipsum dolor sit amet',
			'factory_audit_report' => 'Lorem ipsum dolor sit amet',
			'status' => 1,
			'created' => '2021-02-06 12:08:35',
			'modified' => '2021-02-06 12:08:35',
			'modified_by' => 1
		),
	);
}
