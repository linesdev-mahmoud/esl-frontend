<?php
/* PackagesLesson Fixture generated on: 2020-08-11 18:50:44 : 1597164644 */
class PackagesLessonFixture extends CakeTestFixture {
	var $name = 'PackagesLesson';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'package_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'lesson_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'package_id' => 1,
			'lesson_id' => 1
		),
	);
}
