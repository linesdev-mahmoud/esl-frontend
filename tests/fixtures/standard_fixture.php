<?php
/* Standard Fixture generated on: 2021-01-26 09:45:39 : 1611650739 */
class StandardFixture extends CakeTestFixture {
	var $name = 'Standard';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'current_working_edition' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'newsest_edition' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'title_of_test_standard' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'technical_section' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'is_visible_internally' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'is_visible_externally' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'modified_by' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'current_working_edition' => 'Lorem ipsum dolor sit amet',
			'newsest_edition' => 'Lorem ipsum dolor sit amet',
			'title_of_test_standard' => 'Lorem ipsum dolor sit amet',
			'technical_section' => 'Lorem ipsum dolor sit amet',
			'is_visible_internally' => 1,
			'is_visible_externally' => 1,
			'modified_by' => 1,
			'created' => '2021-01-26 09:45:39',
			'status' => 1
		),
	);
}
