<?php
/* StudentsSection Fixture generated on: 2020-10-06 17:20:54 : 1601997654 */
class StudentsSectionFixture extends CakeTestFixture {
	var $name = 'StudentsSection';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'student_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'section_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'is_downloaded' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'last_seen_block' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'precentage' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'price' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'transaction_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'last_video' => array('type' => 'integer', 'null' => true, 'default' => '0'),
		'downloaded_videos' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'seen_date' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'watched' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'student_id' => 1,
			'section_id' => 1,
			'is_downloaded' => 1,
			'last_seen_block' => 1,
			'precentage' => 1,
			'price' => 1,
			'transaction_id' => 1,
			'last_video' => 1,
			'downloaded_videos' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'seen_date' => '2020-10-06 17:20:54',
			'watched' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.'
		),
	);
}
