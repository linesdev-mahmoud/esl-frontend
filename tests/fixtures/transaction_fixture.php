<?php
/* Transaction Fixture generated on: 2020-08-11 18:56:35 : 1597164995 */
class TransactionFixture extends CakeTestFixture {
	var $name = 'Transaction';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'student_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'packages' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 255),
		'price' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'is_paid' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'student_id' => 1,
			'packages' => 1,
			'price' => 1,
			'is_paid' => 1,
			'created' => '2020-08-11 18:56:35',
			'modified' => '2020-08-11 18:56:35'
		),
	);
}
