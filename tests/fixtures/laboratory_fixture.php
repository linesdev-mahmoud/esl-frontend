<?php
/* Laboratory Fixture generated on: 2021-01-26 09:45:01 : 1611650701 */
class LaboratoryFixture extends CakeTestFixture {
	var $name = 'Laboratory';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'full_name' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'main_location' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'other_location1' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'other_location2' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'other_location3' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'other_location4' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'region' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'country_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'contact' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'accreditation_no' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'issued_by' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'valid_till' => array('type' => 'date', 'null' => false, 'default' => NULL),
		'accreditation_standard' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'Identification_of_last_accreditation_scope' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'certification schemes' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'other_applicable_requirements' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'additional_notes' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 11, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL),
		'modify_by' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'full_name' => 'Lorem ipsum dolor sit amet',
			'main_location' => 'Lorem ipsum dolor sit amet',
			'other_location1' => 'Lorem ipsum dolor sit amet',
			'other_location2' => 'Lorem ipsum dolor sit amet',
			'other_location3' => 'Lorem ipsum dolor sit amet',
			'other_location4' => 'Lorem ipsum dolor sit amet',
			'region' => 'Lorem ipsum dolor sit amet',
			'country_id' => 1,
			'contact' => 'Lorem ipsum dolor sit amet',
			'accreditation_no' => 1,
			'issued_by' => 'Lorem ipsum dolor sit amet',
			'valid_till' => '2021-01-26',
			'accreditation_standard' => 'Lorem ipsum dolor sit amet',
			'Identification_of_last_accreditation_scope' => 'Lorem ipsum dolor sit amet',
			'certification schemes' => 'Lorem ipsum dolor sit amet',
			'other_applicable_requirements' => 'Lorem ipsum dolor sit amet',
			'additional_notes' => 'Lorem ips',
			'created' => '2021-01-26 09:45:01',
			'modify_by' => 1,
			'status' => 1
		),
	);
}
