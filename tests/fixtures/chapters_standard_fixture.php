<?php
/* ChaptersStandard Fixture generated on: 2021-01-26 09:44:27 : 1611650667 */
class ChaptersStandardFixture extends CakeTestFixture {
	var $name = 'ChaptersStandard';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'chapter_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'standard_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'price_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'manual_linking_code' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'timestamp', 'null' => false, 'default' => 'current_timestamp()'),
		'status' => array('type' => 'boolean', 'null' => false, 'default' => NULL),
		'modified_by' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'chapter_id' => 1,
			'standard_id' => 1,
			'price_id' => 1,
			'manual_linking_code' => 'Lorem ipsum dolor sit amet',
			'created' => 1611650667,
			'status' => 1,
			'modified_by' => 1
		),
	);
}
