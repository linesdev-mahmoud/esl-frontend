<?php
/* Package Fixture generated on: 2020-08-11 18:49:54 : 1597164594 */
class PackageFixture extends CakeTestFixture {
	var $name = 'Package';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'image' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'level_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'term_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'price' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'image' => 'Lorem ipsum dolor sit amet',
			'level_id' => 1,
			'term_id' => 1,
			'price' => 1
		),
	);
}
