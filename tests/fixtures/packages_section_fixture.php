<?php
/* PackagesSection Fixture generated on: 2020-10-05 16:56:52 : 1601909812 */
class PackagesSectionFixture extends CakeTestFixture {
	var $name = 'PackagesSection';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'package_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'section_id' => array('type' => 'integer', 'null' => false, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'package_id' => 1,
			'section_id' => 1
		),
	);
}
