_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [23], {
        Enfu: function(e, t, n) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/get-started", function() {
                return n("kIQt")
            }])
        },
        kIQt: function(e, t, n) {
            "use strict";
            n.r(t), n.d(t, "default", (function() {
                return E
            }));
            var a = n("o0o1"),
                r = n.n(a),
                o = n("HaE+"),
                i = n("rePB"),
                s = n("1OyB"),
                l = n("JX7q"),
                u = n("vuIU"),
                c = n("Ji7U"),
                d = n("md7G"),
                p = n("foSv"),
                h = n("q1tI"),
                m = n.n(h),
                f = n("CafY"),
                v = n("8Kt/"),
                g = n.n(v),
                y = n("zS2O"),
                b = (n("YFqc"), m.a.createElement);

            function N(e) {
                var t = e.header,
                    n = e.subHeader,
                    a = e.required;
                return b("div", {
                    className: "form-header"
                }, b("div", {
                    className: "text-5 form-header__header"
                }, t, void 0 !== a && a ? b("span", {
                    className: ""
                }, "!") : ""), b("div", {
                    className: "text-9 form-header__sub"
                }, n))
            }
            var w = n("puUH"),
                O = n("a6RD"),
                k = n.n(O),
                C = n("vDqi"),
                H = n.n(C),
                S = (n("nOHt"), m.a.createElement);

            function x(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function j(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? x(Object(n), !0).forEach((function(t) {
                        Object(i.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : x(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }

            function P(e) {
                var t = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var n, a = Object(p.a)(e);
                    if (t) {
                        var r = Object(p.a)(this).constructor;
                        n = Reflect.construct(a, arguments, r)
                    } else n = a.apply(this, arguments);
                    return Object(d.a)(this, n)
                }
            }
            var T = k()((function() {
                    return Promise.all([n.e(0), n.e(12)]).then(n.bind(null, "IxL5"))
                }), {
                    loading: function() {
                        return S("p", null, "Loading ...")
                    },
                    ssr: !1,
                    loadableGenerated: {
                        webpack: function() {
                            return ["IxL5"]
                        },
                        modules: ["../components/customSelect"]
                    }
                }),
                E = function(e) {
                    Object(c.a)(n, e);
                    var t = P(n);

                    function n(e) {
                        var a;
                        return Object(s.a)(this, n), (a = t.call(this, e)).state = {
                            type: "Hiring",
                            typeOfTalent: "",
                            typeOfHiring: "",
                            noOfTalents: "",
                            duration: "",
                            budget: "",
                            name: "",
                            email: "",
                            country: "",
                            phone: "",
                            detail: "",
                            typeOfProject: "",
                            typeOfSolution: ""
                        }, a.handleLoad = a.handleLoad.bind(Object(l.a)(a)), a
                    }
                    return Object(u.a)(n, null, [{
                        key: "getInitialProps",
                        value: function(e) {
                            return {
                                query: e.query
                            }
                        }
                    }]), Object(u.a)(n, [{
                        key: "componentDidMount",
                        value: function() {
                            "complete" === document.readyState && this.handleLoad(), window.addEventListener("load", this.handleLoad), this.setState({
                                type: this.props.query.type ? this.props.query.type : "Hiring"
                            })
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function() {
                            window.removeEventListener("load", this.handleLoad)
                        }
                    }, {
                        key: "handleLoad",
                        value: function() {
                            var e = this;
                            $(".slider").each((function(t) {
                                var n, a, r = $(this),
                                    o = r.width(),
                                    i = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                                i.setAttribute("viewBox", "0 0 " + o + " 83"), r.html(i), r.append($("<div>").addClass("active").html(i.cloneNode(!0))), r.slider({
                                    range: !0,
                                    values: [500, 1e5],
                                    min: 500,
                                    step: 500,
                                    minRange: 1e3,
                                    max: 1e5,
                                    create: function(e, t) {
                                        r.find(".ui-slider-handle").append($("<div />")), $(r.data("value-0")).html("$" + r.slider("values", 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), 1e5 === r.slider("values", 1) ? $(r.data("value-1")).html("> $" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")) : $(r.data("value-1")).html("$" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), $(r.data("range")).html((r.slider("values", 1) - r.slider("values", 0)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), setCSSVars(r)
                                    },
                                    start: function(e, t) {
                                        $("body").addClass("ui-slider-active"), n = $(t.handle).data("index", t.handleIndex), a = r.find(".ui-slider-handle")
                                    },
                                    change: function(e, t) {
                                        setCSSVars(r)
                                    },
                                    slide: function(t, n) {
                                        var a = r.slider("option", "min"),
                                            o = r.slider("option", "minRange"),
                                            i = r.slider("option", "max");
                                        if (0 == n.handleIndex) {
                                            if (n.values[0] + o >= n.values[1] && r.slider("values", 1, n.values[0] + o), n.values[0] > i - o) return !1
                                        } else if (1 == n.handleIndex && (n.values[1] - o <= n.values[0] && r.slider("values", 0, n.values[1] - o), n.values[1] < a + o)) return !1;
                                        $(r.data("value-0")).html("$" + r.slider("values", 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), 1e5 === r.slider("values", 1) ? $(r.data("value-1")).html("> $" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")) : $(r.data("value-1")).html("$" + r.slider("values", 1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), $(r.data("range")).html((r.slider("values", 1) - r.slider("values", 0)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&thinsp;")), setCSSVars(r), e.setState({
                                            budget: "".concat(n.values[0], "-").concat(n.values[1])
                                        })
                                    },
                                    stop: function(e, t) {
                                        $("body").removeClass("ui-slider-active");
                                        var a = Elastic.easeOut.config(1.08, .44);
                                        TweenMax.to(n, .6, {
                                            "--y": 0,
                                            ease: a
                                        }), TweenMax.to(s, .6, {
                                            y: 42,
                                            ease: a
                                        }), n = null
                                    }
                                });
                                var s = new Proxy({
                                    x: null,
                                    y: null,
                                    b: null,
                                    a: null
                                }, {
                                    set: function(e, t, n) {
                                        return e[t] = n, null !== e.x && null !== e.y && null !== e.b && null !== e.a && r.find("svg").html(getPath([e.x, e.y], e.b, e.a, o)), !0
                                    },
                                    get: function(e, t) {
                                        return e[t]
                                    }
                                });
                                s.x = o / 2, s.y = 42, s.b = 0, s.a = o, $(document).on("mousemove touchmove", (function(e) {
                                    if (n) {
                                        var t = a.eq(0 == n.data("index") ? 1 : 0),
                                            o = n.position().left,
                                            i = t.position().left,
                                            l = n.outerWidth(),
                                            u = l / 2,
                                            c = e.pageY - n.offset().top - n.outerHeight() / 2,
                                            d = c - 4 >= 0 ? c - 4 : c + 4 <= 0 ? c + 4 : 0,
                                            p = 1;
                                        d = d > 24 ? 24 : d < -24 ? -24 : d, p = (p = 0 == n.data("index") ? (o + u <= 52 ? (o + u) / 52 : 1) * (i - o - l <= 52 ? (i - o - l) / 52 : 1) : (o - (i + 2 * u) <= 52 ? (o - (i + l)) / 52 : 1) * (r.outerWidth() - (o + u) <= 52 ? (r.outerWidth() - (o + u)) / 52 : 1)) > 1 ? 1 : p < 0 ? 0 : p, 0 == n.data("index") ? (s.b = o / 2 * p, s.a = i) : (s.b = i + u, s.a = (r.outerWidth() - o) / 2 + o + u + (r.outerWidth() - o) / 2 * (1 - p)), s.x = o + u, s.y = d * p + 42, n.css("--y", d * p)
                                    }
                                }))
                            }))
                        }
                    }, {
                        key: "setType",
                        value: function(e) {
                            this.setState({
                                type: e
                            })
                        }
                    }, {
                        key: "inputChangeHandler",
                        value: function(e) {
                            this.setState(Object(i.a)({}, "".concat(e.target.id), e.target.value))
                        }
                    }, {
                        key: "selectChangeHandler",
                        value: function(e, t) {
                            this.setState(Object(i.a)({}, "".concat(e), t))
                        }
                    }, {
                        key: "handleSubmit",
                        value: function() {
                            var e = Object(o.a)(r.a.mark((function e(t) {
                                return r.a.wrap((function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return t.preventDefault(), e.next = 3, H.a.post("/api/contact", j({}, this.state));
                                        case 3:
                                            window.location.href = "/submission-complete";
                                        case 4:
                                        case "end":
                                            return e.stop()
                                    }
                                }), e, this)
                            })));
                            return function(t) {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "render",
                        value: function() {
                            var e = this;
                            return S(f.a, null, S("div", {
                                className: "page page-get-started "
                            }, S(g.a, null, S("title", null, "Emirates Safety Laboratory |  ESL"), S("meta", {
                                name: "description",
                                content: "Emirates Safety Laboratory |  ESL"
                            })), S(y.a, {
                                title: "Drop us a line",
                                subtitle: "Fill out the form and an ESL representative will contact you as soon as possible."
                            }), S("div", {
                                className: "section bg-light-grey-two form"
                            }, S("form", {
                                onSubmit: function(t) {
                                    return e.handleSubmit(t)
                                }
                            }, S("div", {
                                className: "form-group container-5"
                            }, S(N, {
                                header: "Types of service",
                                subHeader: "What do you need help with?"
                            }), S("div", {
                                className: "tab-button"
                            }, S("button", {
                                type: "button",
                                className: "Hiring" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Hiring")
                                }
                            }, "Product Certification"), S("button", {
                                type: "button",
                                className: "Co-develop product" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Co-develop product")
                                }
                            }, "Certification Renewal"), S("button", {
                                type: "button",
                                className: "Quick project" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("Quick project")
                                }
                            }, "Market Entry"), S("button", {
                                type: "button",
                                className: "test" === this.state.type ? "active" : " ",
                                onClick: function() {
                                    return e.setType("test")
                                }
                            }, "Fire Testing"))), "Hiring" === this.state.type ? S(m.a.Fragment, null, S("div", {
                                className: "form-group container-5"
                            }, S(N, {
                                header: "Type of Certification",
                                subHeader: "Which category applies to you?"
                            }), S("div", {
                                className: "grid grid-cols-1 md:grid-cols-3"
                            }, ["New Product Certificate", "Certificate Renewal", "Change in existing Certificate"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfTalent === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfTalent", t)
                                    }
                                }, t)
                            })))), S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Type of Product or System",
                                subHeader: "What is your product or system? "
                            }), S("div", {
                                className: "grid grid-cols-2 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfHiring === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfHiring", t)
                                    }
                                }, t)
                            })))), S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Type of Application",
                                subHeader: "What are the intended uses for the product or system? "
                            }), S("div", {
                                className: "grid grid-cols-2 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.noOfTalents === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("noOfTalents", t)
                                    }
                                }, t)
                            })))), S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Product Standard",
                                subHeader: "Which standards are you interested in? "
                            }), S("div", {
                                className: "grid grid-cols-2 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.duration === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("duration", t)
                                    }
                                }, t)
                            }))))) : "", "Co-develop product" === this.state.type ? S(m.a.Fragment, null, S("div", {
                                className: "form-group container-5"
                            }, S(N, {
                                header: "Types of project",
                                subHeader: "What does your product need help with?"
                            }), S("div", {
                                className: "grid grid-cols-1 md:grid-cols-3"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfProject === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfProject", t)
                                    }
                                }, t)
                            }))))) : "", "Quick project" === this.state.type ? S(m.a.Fragment, null, S("div", {
                                className: "form-group container-2"
                            }, S(N, {
                                header: "Types of Solutions",
                                subHeader: "What area does it fall under?"
                            }), S("div", {
                                className: "grid grid-cols-3 md:grid-cols-5"
                            }, ["Lorem Ipsum1", "Lorem Ipsum2", "Lorem Ipsum3", "Lorem Ipsum4", "Lorem Ipsum5"].map((function(t) {
                                return S("button", {
                                    type: "button",
                                    key: t,
                                    className: e.state.typeOfSolution === t ? "active" : "",
                                    onClick: function() {
                                        return e.selectChangeHandler("typeOfSolution", t)
                                    }
                                }, t)
                            }))))) : "", S("div", {
                                className: "form-group container-5 mb-40p"
                            }, S(N, {
                                header: "Budget",
                                subHeader: "Move the slider to set your budget range"
                            }), S("div", {
                                className: "slider-wrapper"
                            }, S("div", {
                                className: "box"
                            }, S("div", {
                                className: "values text-center"
                            }, S("div", null, S("span", {
                                id: "first"
                            })), "- ", S("div", null, S("span", {
                                id: "second"
                            }))), S("div", {
                                className: "slider",
                                "data-value-0": "#first",
                                "data-value-1": "#second",
                                "data-range": "#third"
                            })))), S("div", {
                                className: "form-group container-5 mb-40p register_form"
                            }, S(N, {
                                header: "Move your business forward Let’s start ",
                                subHeader: "Fill in your personal details!",
                                required: !0
                            }), S("div", {
                                className: "grid grid-cols-1 md:grid-cols-2"
                            }, S("input", {
                                type: "text",
                                id: "name",
                                placeholder: "First Name",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "text",
                                id: "last",
                                placeholder: "Last Name",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "text",
                                id: "company",
                                placeholder: "Company",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("div", {
                                className: "select-container"
                            }, S(T, {
                                options: w.a,
                                placeholder: "Country",
                                parentCallback: function(t) {
                                    return e.setState({
                                        country: t.label
                                    })
                                }
                            })), S("input", {
                                type: "email",
                                id: "email",
                                placeholder: "Email",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "text",
                                id: "phone",
                                placeholder: "Phone Number",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "password",
                                id: "password",
                                placeholder: "Password",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), S("input", {
                                type: "password",
                                id: "confirm",
                                placeholder: "Confirm Password",
                                required: !0,
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }), "Co-develop product" === this.state.type ? S("textarea", {
                                className: "md:col-span-2",
                                name: "",
                                id: "detail",
                                placeholder: "What else should we know about your project?",
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }) : "", "Quick project" === this.state.type ? S("textarea", {
                                className: "md:col-span-2",
                                name: "",
                                id: "detail",
                                placeholder: "What else should we know about your project?",
                                onChange: function(t) {
                                    return e.inputChangeHandler(t)
                                }
                            }) : ""), S("button", {
                                type: "submit",
                                className: "btn mx-auto block"
                            }, S("span", {
                                className: "arrow arrow-left"
                            }), S("span", {
                                className: "btn-text"
                            }, "Submit"), S("span", {
                                className: "arrow arrow-right"
                            }))), S("div", {
                                className: "img"
                            }, S("img", {
                                
                                className: "image"
                            })), S("div", {
                                className: "text-center"
                            })))))
                        }
                    }]), n
                }(h.Component)
        }
    },
    [
        ["Enfu", 1, 0, 2, 3, 5, 10]
    ]
]);