_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [25, 33], {
        "2qu3": function(n, e, t) {
            "use strict";
            var i = t("oI91"),
                a = t("/GRZ"),
                r = t("i2R6");

            function o(n, e) {
                var t = Object.keys(n);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(n);
                    e && (i = i.filter((function(e) {
                        return Object.getOwnPropertyDescriptor(n, e).enumerable
                    }))), t.push.apply(t, i)
                }
                return t
            }

            function s(n) {
                for (var e = 1; e < arguments.length; e++) {
                    var t = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? o(Object(t), !0).forEach((function(e) {
                        i(n, e, t[e])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(n, Object.getOwnPropertyDescriptors(t)) : o(Object(t)).forEach((function(e) {
                        Object.defineProperty(n, e, Object.getOwnPropertyDescriptor(t, e))
                    }))
                }
                return n
            }

            function l(n, e) {
                var t;
                if ("undefined" === typeof Symbol || null == n[Symbol.iterator]) {
                    if (Array.isArray(n) || (t = function(n, e) {
                            if (!n) return;
                            if ("string" === typeof n) return c(n, e);
                            var t = Object.prototype.toString.call(n).slice(8, -1);
                            "Object" === t && n.constructor && (t = n.constructor.name);
                            if ("Map" === t || "Set" === t) return Array.from(n);
                            if ("Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t)) return c(n, e)
                        }(n)) || e && n && "number" === typeof n.length) {
                        t && (n = t);
                        var i = 0,
                            a = function() {};
                        return {
                            s: a,
                            n: function() {
                                return i >= n.length ? {
                                    done: !0
                                } : {
                                    done: !1,
                                    value: n[i++]
                                }
                            },
                            e: function(n) {
                                throw n
                            },
                            f: a
                        }
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }
                var r, o = !0,
                    s = !1;
                return {
                    s: function() {
                        t = n[Symbol.iterator]()
                    },
                    n: function() {
                        var n = t.next();
                        return o = n.done, n
                    },
                    e: function(n) {
                        s = !0, r = n
                    },
                    f: function() {
                        try {
                            o || null == t.return || t.return()
                        } finally {
                            if (s) throw r
                        }
                    }
                }
            }

            function c(n, e) {
                (null == e || e > n.length) && (e = n.length);
                for (var t = 0, i = new Array(e); t < e; t++) i[t] = n[t];
                return i
            }
            e.__esModule = !0, e.default = void 0;
            var d, u = (d = t("q1tI")) && d.__esModule ? d : {
                    default: d
                },
                p = t("8L3h"),
                f = t("jwwS");
            var m = [],
                g = [],
                h = !1;

            function v(n) {
                var e = n(),
                    t = {
                        loading: !0,
                        loaded: null,
                        error: null
                    };
                return t.promise = e.then((function(n) {
                    return t.loading = !1, t.loaded = n, n
                })).catch((function(n) {
                    throw t.loading = !1, t.error = n, n
                })), t
            }

            function b(n) {
                var e = {
                        loading: !1,
                        loaded: {},
                        error: null
                    },
                    t = [];
                try {
                    Object.keys(n).forEach((function(i) {
                        var a = v(n[i]);
                        a.loading ? e.loading = !0 : (e.loaded[i] = a.loaded, e.error = a.error), t.push(a.promise), a.promise.then((function(n) {
                            e.loaded[i] = n
                        })).catch((function(n) {
                            e.error = n
                        }))
                    }))
                } catch (i) {
                    e.error = i
                }
                return e.promise = Promise.all(t).then((function(n) {
                    return e.loading = !1, n
                })).catch((function(n) {
                    throw e.loading = !1, n
                })), e
            }

            function x(n, e) {
                return u.default.createElement(function(n) {
                    return n && n.__esModule ? n.default : n
                }(n), e)
            }

            function y(n, e) {
                var t = Object.assign({
                        loader: null,
                        loading: null,
                        delay: 200,
                        timeout: null,
                        render: x,
                        webpack: null,
                        modules: null
                    }, e),
                    i = null;

                function a() {
                    if (!i) {
                        var e = new w(n, t);
                        i = {
                            getCurrentValue: e.getCurrentValue.bind(e),
                            subscribe: e.subscribe.bind(e),
                            retry: e.retry.bind(e),
                            promise: e.promise.bind(e)
                        }
                    }
                    return i.promise()
                }
                if (!h && "function" === typeof t.webpack) {
                    var r = t.webpack();
                    g.push((function(n) {
                        var e, t = l(r);
                        try {
                            for (t.s(); !(e = t.n()).done;) {
                                var i = e.value;
                                if (-1 !== n.indexOf(i)) return a()
                            }
                        } catch (o) {
                            t.e(o)
                        } finally {
                            t.f()
                        }
                    }))
                }
                var o = function(n, e) {
                    a();
                    var r = u.default.useContext(f.LoadableContext),
                        o = (0, p.useSubscription)(i);
                    return u.default.useImperativeHandle(e, (function() {
                        return {
                            retry: i.retry
                        }
                    }), []), r && Array.isArray(t.modules) && t.modules.forEach((function(n) {
                        r(n)
                    })), u.default.useMemo((function() {
                        return o.loading || o.error ? u.default.createElement(t.loading, {
                            isLoading: o.loading,
                            pastDelay: o.pastDelay,
                            timedOut: o.timedOut,
                            error: o.error,
                            retry: i.retry
                        }) : o.loaded ? t.render(o.loaded, n) : null
                    }), [n, o])
                };
                return o.preload = function() {
                    return a()
                }, o.displayName = "LoadableComponent", u.default.forwardRef(o)
            }
            var w = function() {
                function n(e, t) {
                    a(this, n), this._loadFn = e, this._opts = t, this._callbacks = new Set, this._delay = null, this._timeout = null, this.retry()
                }
                return r(n, [{
                    key: "promise",
                    value: function() {
                        return this._res.promise
                    }
                }, {
                    key: "retry",
                    value: function() {
                        var n = this;
                        this._clearTimeouts(), this._res = this._loadFn(this._opts.loader), this._state = {
                            pastDelay: !1,
                            timedOut: !1
                        };
                        var e = this._res,
                            t = this._opts;
                        e.loading && ("number" === typeof t.delay && (0 === t.delay ? this._state.pastDelay = !0 : this._delay = setTimeout((function() {
                            n._update({
                                pastDelay: !0
                            })
                        }), t.delay)), "number" === typeof t.timeout && (this._timeout = setTimeout((function() {
                            n._update({
                                timedOut: !0
                            })
                        }), t.timeout))), this._res.promise.then((function() {
                            n._update({}), n._clearTimeouts()
                        })).catch((function(e) {
                            n._update({}), n._clearTimeouts()
                        })), this._update({})
                    }
                }, {
                    key: "_update",
                    value: function(n) {
                        this._state = s(s({}, this._state), {}, {
                            error: this._res.error,
                            loaded: this._res.loaded,
                            loading: this._res.loading
                        }, n), this._callbacks.forEach((function(n) {
                            return n()
                        }))
                    }
                }, {
                    key: "_clearTimeouts",
                    value: function() {
                        clearTimeout(this._delay), clearTimeout(this._timeout)
                    }
                }, {
                    key: "getCurrentValue",
                    value: function() {
                        return this._state
                    }
                }, {
                    key: "subscribe",
                    value: function(n) {
                        var e = this;
                        return this._callbacks.add(n),
                            function() {
                                e._callbacks.delete(n)
                            }
                    }
                }]), n
            }();

            function k(n) {
                return y(v, n)
            }

            function j(n, e) {
                for (var t = []; n.length;) {
                    var i = n.pop();
                    t.push(i(e))
                }
                return Promise.all(t).then((function() {
                    if (n.length) return j(n, e)
                }))
            }
            k.Map = function(n) {
                if ("function" !== typeof n.render) throw new Error("LoadableMap requires a `render(loaded, props)` function");
                return y(b, n)
            }, k.preloadAll = function() {
                return new Promise((function(n, e) {
                    j(m).then(n, e)
                }))
            }, k.preloadReady = function() {
                var n = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [];
                return new Promise((function(e) {
                    var t = function() {
                        return h = !0, e()
                    };
                    j(g, n).then(t, t)
                }))
            }, window.__NEXT_PRELOADREADY = k.preloadReady;
            var O = k;
            e.default = O
        },
        "5pYx": function(n, e, t) {
            "use strict";
            t.d(e, "a", (function() {
                return r
            }));
            var i = t("q1tI"),
                a = t.n(i).a.createElement;

            function r(n) {
                var e = n.title,
                    t = n.subtitle;
                return a("div", {
                    className: "mb-80p"
                }, a("p", {
                    className: "text-2 text-center section__title",
                    dangerouslySetInnerHTML: {
                        __html: e
                    },
                    "data-aos": "fade-up"
                }), t ? a("p", {
                    className: "text-6 text-center section__sub-title",
                    dangerouslySetInnerHTML: {
                        __html: t
                    },
                    "data-aos": "fade-up"
                }) : "")
            }
        },
        "7SI0": function(n, e, t) {
            "use strict";
            t.d(e, "a", (function() {
                return s
            }));
            var i = t("q1tI"),
                a = t.n(i),
                r = (t("YFqc"), t("5pYx")),
                o = a.a.createElement;

            function s(n) {
                var e = n.background,
                    t = n.title,
                    i = n.items;
                return o("div", {
                    className: "section get-started-steps " + e
                }, o("div", {
                    className: "container-2"
                }, o(r.a, {
                    title: t
                }), o("div", {
                    className: "items grid grid-cols-1 md:grid-cols-4"
                }, i.map((function(n, e) {
                    return o("div", {
                        className: "item",
                        key: n.name,
                        "data-aos": "fade-left",
                        "data-aos-delay": 300 * e
                    }, o("div", {
                        className: "name"
                    }, n.name), o("div", {
                        className: "description"
                    }, n.description))
                })))))
            }
        },
        HSIl: function(n, e, t) {
            "use strict";
            t.d(e, "b", (function() {
                return c
            })), t.d(e, "a", (function() {
                return d
            }));
            var i = t("h4VS"),
                a = t("vOnD");

            function r() {
                var n = Object(i.a)(["\n  .slick-dots {\n    bottom: -50px;\n    display: flex !important;\n    justify-content: flex-start;\n    align-items: center;\n\n    li {\n      display: flex;\n      width: auto;\n      height: auto;\n      margin: 0;\n\n      &:first-child {\n        &.slick-active {\n          .custom-slick-dot {\n            .loader1:after {\n              animation: ", " 12s ease;\n            }\n          }\n        }\n        .custom-slick-dot{\n          padding: 0;\n        }\n      }\n\n      &:nth-child(2) {\n        &.slick-active {\n          .custom-slick-dot {\n            .loader2:after {\n              animation: ", " 12s ease infinite;\n            }\n          }\n        }\n      }\n\n      &:last-child {\n        .custom-slick-dot{\n          padding: 0;\n        }\n      }\n    }\n  }\n\n  .custom-slick-dot {\n    display: flex;\n    font-size: 35px;\n    font-weight: bold;\n    color: #e1e0d5;\n    font-family: \"Noto Sans\";\n    justify-content: center;\n    align-items: center;\n\n    @media (max-width: 1024px) {\n      font-size: 25px;\n    }\n\n    span {\n      font-family: Noto Sans;\n      font-size: 20px;\n      margin-left: 7px;\n\n      @media (max-width: 1024px) {\n        font-size: 11px;\n      }\n\n      @media (max-width: 768px) {\n        display: none;\n      }\n    }\n\n    .loader {\n      margin: 0 25px;\n      width: 90px;\n      height: 2px;\n      background-color: #e1e0d5;\n      position: relative;\n\n      &:after {\n        content: '';\n        background: #ffe300;\n        height: 100%;\n        position: absolute;\n        left: 0;\n        right: 100%;\n      }\n    }\n  }\n\n  .slick-active {\n    .custom-slick-dot {\n      color: #ffe300;\n\n      span {\n        color: #474747;\n      }\n\n      &:after {\n        background: #ffe300 !important;\n      }\n\n      &:before {\n        background: #ffe300 !important;\n      }\n    }\n  }\n\n  @media (max-width: 1024px) {\n    .slick-dots {\n      bottom: -30px;\n    }\n  }\n"]);
                return r = function() {
                    return n
                }, n
            }

            function o() {
                var n = Object(i.a)(["\n  .slick-slider {\n    z-index: 20;\n\n    .slick-list {\n      padding: 0;\n    }\n\n    .slick-slide {\n      padding: 0 10px;\n      > div {\n        outline: none !important;\n      }\n\n      &:first-child {\n        padding-right: 20px;\n        padding-left: 0;\n      }\n\n      &:last-child {\n        padding-right: 0;\n        padding-left: 20px;\n      }\n    }\n  }\n\n  .content {\n    min-height: 224px;\n  }\n\n  .slick-dots {\n    bottom: -30px;\n    display: flex !important;\n    justify-content: center;\n\n    li {\n      width: 30px;\n      display: flex;\n      justify-content: center;\n\n      button {\n        margin-top: 10px;\n        width: 100%;\n        height: 2px;\n        display: flex;\n        background: #474747;\n        padding: 0;\n        &:before {\n          display: none;\n        }\n      }\n    }\n\n    li.slick-active button {\n      background: #ffe300;\n    }\n  }\n\n  @media (max-width: 768px) {\n    .slick-slide {\n      padding: 0 10px;\n\n      .item {\n        padding: 0 10px;\n      }\n    }\n\n    margin-bottom: 50px;\n  }\n"]);
                return o = function() {
                    return n
                }, n
            }

            function s() {
                var n = Object(i.a)(["\n  0% {\n    left: 0;\n    right: 100%;\n  }\n\n  50% {\n    left: 0;\n    right: 0;\n  }\n\n  100% {\n    left: 100%;\n    right: 0;\n  }\n"]);
                return s = function() {
                    return n
                }, n
            }
            var l = Object(a.d)(s()),
                c = a.c.div(o()),
                d = a.c.div(r(), l, l)
        },
        Jevo: function(n, e, t) {
            "use strict";
            t.d(e, "a", (function() {
                return o
            }));
            var i = t("q1tI"),
                a = t.n(i),
                r = a.a.createElement;

            function o(n) {
                var e = n.title;
                return r(a.a.Fragment, null, r("div", {
                    className: "text-4 text-center mb-60p"
                }, e), r("div", {
                    className: "grid grid-cols-3 md:grid-cols-6 mb-80p"
                }, [{
                    image: "/svg/logo - service - jwt (b&w).svg"
                }, {
                    image: "/svg/logo - service - lytepay (b&w).svg"
                }, {
                    image: "/svg/logo - service - fairprice (b&w).svg"
                }, {
                    image: "/svg/logo - service - mccann (b&w).svg"
                }, {
                    image: "/svg/logo - service - havas (b&w).svg"
                }, {
                    image: "/svg/logo - service - leoburnett (b&w).svg"
                }].map((function(n) {
                    return r("div", {
                        key: n.image
                    }, r("img", {
                        src: n.image,
                        alt: ""
                    }))
                }))))
            }
        },
        RNiq: function(n, e, t) {
            "use strict";
            t.r(e), t.d(e, "__N_SSP", (function() {
                return jn
            })), t.d(e, "default", (function() {
                return On
            }));
            var i = t("1OyB"),
                a = t("vuIU"),
                r = t("JX7q"),
                o = t("Ji7U"),
                s = t("md7G"),
                l = t("foSv"),
                c = t("q1tI"),
                d = t.n(c),
                u = t("sQcB"),
                p = t("8Kt/"),
                f = t.n(p),
                m = t("7SI0"),
                g = t("kJgk"),
                h = t("5pYx"),
                v = t("YFqc"),
                b = t.n(v),
                x = t("CafY"),
                y = t("a3/r"),
                w = t("a6RD"),
                k = t.n(w),
                j = t("h4VS"),
                O = t("vOnD");
            t("sr+r");

            function N() {
                var n = Object(j.a)(["\n  font-size: 18px;\n  font-family: Noto Sans;\n"]);
                return N = function() {
                    return n
                }, n
            }

            function S() {
                var n = Object(j.a)(["\n  font-size: 18px;\n  width: 100%;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n"]);
                return S = function() {
                    return n
                }, n
            }

            function _() {
                var n = Object(j.a)(["\n  display: flex;\n  flex-direction: column;\n  margin-left: 15px;\n  overflow: hidden;\n"]);
                return _ = function() {
                    return n
                }, n
            }

            function E() {
                var n = Object(j.a)(["\n  width: 55px;\n  height: 55px;\n  border-radius: 50%;\n"]);
                return E = function() {
                    return n
                }, n
            }

            function P() {
                var n = Object(j.a)(["\n  width: 100%;\n  display: flex;\n  align-items: center;\n"]);
                return P = function() {
                    return n
                }, n
            }

            function T() {
                var n = Object(j.a)(["\n  display: flex;\n  flex-direction: column;\n  flex: 1 0 auto;\n  overflow: hidden;\n  margin-bottom: 25px;\n  height: 270px;\n"]);
                return T = function() {
                    return n
                }, n
            }

            function I() {
                var n = Object(j.a)(["\n  width: 100%;\n  height: 1px;\n  background: #cccccc;\n  margin: 20px 0 25px 0;\n"]);
                return I = function() {
                    return n
                }, n
            }

            function z() {
                var n = Object(j.a)(["\n  width: auto;\n  font-size: 14px;\n  color: #474747;\n  font-family: SofiaPro-Regular;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.43;\n  margin-top: 20px;\n"]);
                return z = function() {
                    return n
                }, n
            }

            function D() {
                var n = Object(j.a)(["\n  font-size: 20px;\n  width: 100%;\n  color: #474747;\n  font-family: SofiaPro-Regular;\n  line-height: 1.6;\n  margin-top: 5px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n\n  @media (max-width: 767px) {\n    font-size: 18px;\n  }\n\n  -webkit-line-clamp: ", ";\n"]);
                return D = function() {
                    return n
                }, n
            }

            function L() {
                var n = Object(j.a)(["\n  display: none;\n  width: auto;\n  padding: 4px 15px;\n  background-image: linear-gradient(251deg, #474747, #313131);\n  color: #ffe300;\n  font-size: 18px;\n  justify-content: center;\n  align-items: center;\n  position: absolute;\n  bottom: 0;\n  font-family: SofiaPro-Regular;\n"]);
                return L = function() {
                    return n
                }, n
            }

            function R() {
                var n = Object(j.a)(["\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n"]);
                return R = function() {
                    return n
                }, n
            }

            function M() {
                var n = Object(j.a)(["\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  margin-top: 50px;\n  margin-right: 0;\n\n  @media (min-width: 1025px) and (max-width: 1280px) {\n    width: 300px;\n  }\n\n  @media (max-width: 1024px){\n    width: 360px;\n    margin: 0 20px;\n    width: auto;\n  }\n\n  @media (max-width: 767px) {\n    margin: 0 10px;\n  }\n"]);
                return M = function() {
                    return n
                }, n
            }

            function q() {
                var n = Object(j.a)(["\n    @media (min-width: 1440px){\n      .slick-slide {\n        padding: 0 20px;\n\n        &:first-child {\n          padding-left: 0;\n          padding-right: 40px;\n        }\n\n        &:last-child {\n          padding-right: 0;\n          padding-left: 40px;\n        }\n      }\n    }\n\n\n    @media (max-width: 1440px) {\n      .slick-slide {\n        padding: 0 10px;\n\n        &:first-child {\n          padding-left: 0;\n          padding-right: 20px;\n        }\n\n        &:last-child {\n          padding-right: 0;\n          padding-left: 20px;\n        }\n      }\n    }\n  "]);
                return q = function() {
                    return n
                }, n
            }

            function A() {
                var n = Object(j.a)(["\n    width: 70%;\n\n    @media (max-width: 1024px) {\n      width: 100%;\n    }\n  "]);
                return A = function() {
                    return n
                }, n
            }

            function C() {
                var n = Object(j.a)(["\n  width: 100%;\n  display: block;\n\n  ", "\n\n  .slick-dots {\n    bottom: -15px;\n    display: flex !important;\n    justify-content: center;\n\n    li {\n      width: 30px;\n      display: flex;\n      justify-content: center;\n\n      button {\n        margin-top: 10px;\n        width: 100%;\n        height: 2px;\n        display: flex;\n        background: #474747;\n        padding: 0;\n        &:before {\n          display: none;\n        }\n      }\n    }\n\n    li.slick-active button {\n      background: #ffe300;\n    }\n  }\n\n\n  ", "\n\n  .slick-slide {\n    &:hover {\n      ", " {\n        transform: scale(0.95);\n      }\n\n      .title-text {\n        background-size: 100% 100%;\n      }\n    };\n  }\n"]);
                return C = function() {
                    return n
                }, n
            }

            function B() {
                var n = Object(j.a)(["\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -webkit-line-clamp: ", ";\n  margin-bottom: 5px;\n  line-height: 1.18;\n  font-size: 34px;\n\n  .title-text {\n    display: inline;\n    font-size: 34px;\n    font-family: Noto Sans;\n    width: 100%;\n    margin-bottom: 10px;\n    color: #474747;\n    line-height: 1.18;\n    transition: background-size .4s ease;\n    background: linear-gradient(to bottom, transparent 65%, #ffe300) center center/0% 75% no-repeat;\n    cursor: pointer;\n  }\n\n  z-index: 1;\n  position: relative;\n"]);
                return B = function() {
                    return n
                }, n
            }

            function U() {
                var n = Object(j.a)(["\n  width: 100%;\n  height: 250px;\n  display: flex;\n  position: relative;\n  transition: transform .2s;\n\n  img {\n    width: 100%;\n    object-fit: cover;\n    height: 100%;\n  }\n"]);
                return U = function() {
                    return n
                }, n
            }

            function H() {
                var n = Object(j.a)(["\n  text-align: center;\n  font-size: 2.2916666667vw;\n  font-family: Noto Sans;\n  width: 100%;\n  font-weight: bold;\n  margin-bottom: 10px;\n  color: #474747;\n  line-height: 1.14;\n  letter-spacing: 0.53px;\n\n  @media (max-width: 1024px) {\n    font-size: 28px;\n    margin-bottom: 40px;\n  }\n"]);
                return H = function() {
                    return n
                }, n
            }

            function F() {
                var n = Object(j.a)(["\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n\n  @media (max-width: 1024px) {\n    width: 100%;\n    padding: 0 40px;\n  }\n\n  @media (max-width: 768px) {\n    width: 100%;\n    padding: 0 20px;\n  }\n"]);
                return F = function() {
                    return n
                }, n
            }

            function Y() {
                var n = Object(j.a)(["\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-flow: wrap row;\n  padding: 50px 0;\n"]);
                return Y = function() {
                    return n
                }, n
            }
            var G = O.c.div(Y()),
                V = O.c.div(F()),
                W = O.c.div(H()),
                J = O.c.div(U()),
                X = O.c.div(B(), (function(n) {
                    return n.number
                })),
                Q = O.c.div(C(), (function(n) {
                    return 2 === n.totalItem && Object(O.b)(A())
                }), (function(n) {
                    return 3 === n.totalItem && Object(O.b)(q())
                }), J),
                $ = O.c.div(M()),
                K = O.c.div(R()),
                Z = O.c.div(L()),
                nn = O.c.div(D(), (function(n) {
                    return n.number
                })),
                en = O.c.div(z()),
                tn = O.c.div(I()),
                an = O.c.div(T()),
                rn = O.c.div(P()),
                on = O.c.img(E()),
                sn = O.c.div(_()),
                ln = O.c.div(S()),
                cn = O.c.div(N()),
                dn = d.a.createElement;

            function un(n, e) {
                var t = window.getComputedStyle(n, null),
                    i = n.offsetHeight,
                    a = parseInt(t.getPropertyValue("font-size")),
                    r = parseInt(t.getPropertyValue("line-height"));
                t.getPropertyValue("box-sizing");
                return isNaN(r) && (r = a * e), Math.ceil(i / r)
            }
            var pn = function(n) {
                    var e = n.item,
                        t = Object(c.useState)({
                            title: 5,
                            description: 5
                        }),
                        i = t[0],
                        a = t[1],
                        r = Object(c.useRef)(),
                        o = Object(c.useRef)(),
                        s = Object(c.useRef)(1);
                    return Object(c.useEffect)((function() {
                        if (s.current < 6) {
                            var n = un(r.current, 1.18),
                                e = un(o.current, 1.6);
                            (n = n % 2 !== 0 ? parseInt(n) + 1 : parseInt(n)) < 7 && (e = 7 - n), a({
                                title: n,
                                description: parseInt(e)
                            }), s.current += 1
                        }
                    }), [i]), dn(K, null, dn($, null, dn(b.a, {
                        href: "#".concat(e.slug)
                    }, dn("a", {
                        href: "#".concat(e.slug)
                    }, dn(J, null, dn("img", {
                        src: e.image,
                        alt: "error"
                    }), dn(Z, null, e.featuredPost)))), dn(en, null, "".concat(e.category.text, " - ").concat(e.readTimes)), dn(tn, null), dn(an, null, dn(b.a, {
                        href: "#".concat(e.slug)
                    }, dn("a", {
                        href: "#".concat(e.slug)
                    }, dn(X, {
                        number: i.title
                    }, dn("span", {
                        ref: r,
                        className: "title-text"
                    }, e.title)))), dn(nn, {
                        ref: o,
                        number: i.description
                    }, e.shortDescription)), dn(rn, null, dn(on, {
                        src: e.profileImage,
                        alt: "erorr"
                    }), dn(sn, null, dn(ln, null, e.profileName), dn(cn, null, e.profilePosition))), dn(tn, null)))
                },
                fn = t("OS56"),
                mn = t.n(fn),
                gn = d.a.createElement,
                hn = function(n) {
                    var e = n.items;
                    return 0 === e.length ? null : gn(G, null, gn(V, {
                        className: "container-2"
                    }, gn(W, null, "ESL"), gn(Q, {
                        totalItem: e.length
                    }, gn(mn.a, {
                        arrows: !1,
                        dots: !1,
                        slidesToShow: e.length,
                        slidesToScroll: e.length,
                        responsive: [{
                            breakpoint: 768,
                            settings: {
                                autoplaySpeed: 3e3,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                dots: !0,
                                arrows: !1,
                                autoplay: !0
                            }
                        }, {
                            breakpoint: 1024,
                            settings: {
                                autoplaySpeed: 3e3,
                                slidesToShow: e.length > 2 ? 2 : e.length,
                                slidesToScroll: e.length > 2 ? 2 : e.length,
                                dots: !0,
                                autoplay: !0,
                                arrows: !1
                            }
                        }]
                    }, e.map((function(n) {
                        return gn(pn, {
                            item: n,
                            totalItem: e.length
                        })
                    })))), gn(b.a, {
                        href: "#"
                    }, gn("a", {
                        href: "#",
                        className: "btn btn-effect",
                        style: {
                            marginTop: 40
                        },
                        "data-aos": "fade-up"
                    }, gn("span", {
                        className: "arrow arrow-left"
                    }), gn("span", {
                        className: "btn-text"
                    }, "View more"), gn("span", {
                        className: "arrow arrow-right"
                    })))))
                },
                vn = t("HSIl"),
                bn = d.a.createElement;

            function xn(n, e) {
                var t;
                if ("undefined" === typeof Symbol || null == n[Symbol.iterator]) {
                    if (Array.isArray(n) || (t = function(n, e) {
                            if (!n) return;
                            if ("string" === typeof n) return yn(n, e);
                            var t = Object.prototype.toString.call(n).slice(8, -1);
                            "Object" === t && n.constructor && (t = n.constructor.name);
                            if ("Map" === t || "Set" === t) return Array.from(n);
                            if ("Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t)) return yn(n, e)
                        }(n)) || e && n && "number" === typeof n.length) {
                        t && (n = t);
                        var i = 0,
                            a = function() {};
                        return {
                            s: a,
                            n: function() {
                                return i >= n.length ? {
                                    done: !0
                                } : {
                                    done: !1,
                                    value: n[i++]
                                }
                            },
                            e: function(n) {
                                throw n
                            },
                            f: a
                        }
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }
                var r, o = !0,
                    s = !1;
                return {
                    s: function() {
                        t = n[Symbol.iterator]()
                    },
                    n: function() {
                        var n = t.next();
                        return o = n.done, n
                    },
                    e: function(n) {
                        s = !0, r = n
                    },
                    f: function() {
                        try {
                            o || null == t.return || t.return()
                        } finally {
                            if (s) throw r
                        }
                    }
                }
            }

            function yn(n, e) {
                (null == e || e > n.length) && (e = n.length);
                for (var t = 0, i = new Array(e); t < e; t++) i[t] = n[t];
                return i
            }

            function wn(n) {
                var e = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Date.prototype.toString.call(Reflect.construct(Date, [], (function() {}))), !0
                    } catch (n) {
                        return !1
                    }
                }();
                return function() {
                    var t, i = Object(l.a)(n);
                    if (e) {
                        var a = Object(l.a)(this).constructor;
                        t = Reflect.construct(i, arguments, a)
                    } else t = i.apply(this, arguments);
                    return Object(s.a)(this, t)
                }
            }
            var kn = k()((function() {
                    return Promise.all([t.e(9), t.e(13)]).then(t.bind(null, "a0BU"))
                }), {
                    loading: function() {
                        return bn("p", null, "Loading ...")
                    },
                    ssr: !1,
                    loadableGenerated: {
                        webpack: function() {
                            return ["a0BU"]
                        },
                        modules: ["../components/testimonials"]
                    }
                }),
                jn = !0,
                On = function(n) {
                    Object(o.a)(t, n);
                    var e = wn(t);

                    function t() {
                        var n;
                        return Object(i.a)(this, t), (n = e.call(this)).handleLoad = n.handleLoad.bind(Object(r.a)(n)), n
                    }
                    return Object(a.a)(t, [{
                        key: "componentDidMount",
                        value: function() {
                            "complete" === document.readyState && this.handleLoad(), window.addEventListener("load", this.handleLoad)
                        }
                    }, {
                        key: "handleLoad",
                        value: function() {
                            var n, e = document.getElementsByClassName("accordion"),
                                t = document.getElementsByClassName("accordion-image");
                            for (n = 0; n < e.length; n++) {
                                //console.log(e.length);
                                if (0 === n) {
                                    var i, a = xn(t);
                                    //console.log(xn(t));
                                    try {
                                        for (a.s(); !(i = a.n()).done;) {
                                            i.value.style.display = "none"
                                        }
                                    } catch (o) {
                                        a.e(o)
                                    } finally {
                                        a.f()
                                    }
                                    t[n].style.display = "block", e[n].classList.add("active"), e[n].nextElementSibling.style.display = "block"
                                }
                                e[n].addEventListener("click", (function() {
                                    var i;
                                    for (n = 0; n < e.length; n++) e[n].isSameNode(this) ? i = n : (e[n].classList.remove("active"), e[n].nextElementSibling.style.display = "none");
                                    var a, r = xn(t);
                                    try {
                                        for (r.s(); !(a = r.n()).done;) {
                                            a.value.style.display = "none"
                                        }
                                    } catch (o) {
                                        r.e(o)
                                    } finally {
                                        r.f()
                                    }
                                    //console.log(t[i]);
                                    t[i].style.display = "block", this.classList.toggle("active");
                                    var s = this.nextElementSibling;
                                    "block" === s.style.display ? s.style.display = "none" : s.style.display = "block"
                                }))
                            }
                            var r = new ScrollMagic.Controller({
                                globalSceneOptions: {
                                    triggerHook: "onEnter",
                                    duration: "200%"
                                }
                            });
                            window.matchMedia("(min-width: 767px)").matches && (new ScrollMagic.Scene({
                                triggerElement: "#meet-and-hire"
                            }).setTween(".meet-and-hire-item:nth-child(2n-1)", {
                                y: "60px",
                                ease: Linear.easeNone
                            }).addTo(r), new ScrollMagic.Scene({
                                triggerElement: "#meet-and-hire"
                            }).setTween(".meet-and-hire-item:nth-child(2n)", {
                                y: "-30px",
                                ease: Linear.easeNone
                            }).addTo(r), new ScrollMagic.Scene({
                                triggerElement: "#no-secret"
                            }).setTween("#logo-text", {
                                y: "-200px",
                                ease: Linear.easeNone
                            }).addTo(r))
                        }
                    }, {
                        key: "render",
                        value: function() {
                            var n = {
                                customPaging: function(n) {
                                    return 0 === n ? bn("div", {
                                        className: "custom-slick-dot",
                                        key: n
                                    }, n + 1, bn("span", null, "Certification"), bn("div", {
                                        className: "loader loader1"
                                    })) : 1 === n ? bn("div", {
                                        className: "custom-slick-dot",
                                        key: n
                                    }, n + 1, " ", bn("span", null, "Fire Testing"), bn("div", {
                                        className: "loader loader2"
                                    })) : 2 === n ? bn("div", {
                                        className: "custom-slick-dot",
                                        key: n
                                    }, n + 1, " ", bn("span", null, "Project Development"), bn("div", {
                                        className: "loader loader3"
                                    })): 3 === n ? bn("div", {
                                        className: "custom-slick-dot",
                                        key: n
                                    }, n + 1, " ", bn("span", null, "Market Access")) : null
                                },
                                arrows: !1,
                                dots: !0,
                                infinite: !0,
                                autoplaySpeed: 4e3,
                                autoplay: !0,
                                slidesToShow: 1,
                                slidesToScroll: 1
                            };
                            //console.log(n);
                            return bn(x.a, null, bn(f.a, null, bn("title", null, "Emirates Safety Laboratory |  ESL"), bn("meta", {
                                name: "description",
                                content: "We are the one-stop shop Testing Laboratory and Certification Body with strong expertise in fire protection requirements."
                            })), bn("div", {
                                className: "page page-home main",
                                id: "fullpage"
                            }, bn("section", {
                                "data-anchor": "Page 1",
                                className: "container-1 hero flex pt-10 relative"
                            }, bn("div", {
                                className: "left"
                            }, bn(vn.a, {
                                className: "home-slick"
                            }, bn(mn.a, n, bn("h1", {
                                id: "effect",
                                className: "first"
                            }, "We are the one-stop shop Testing Laboratory and Certification Body with strong expertise in fire protection requirements."), bn("h1", {
                                id: "effect2",
                                className: "second"
                            }, "We certify, test and provide advisory on development project. Let us help you win the competition, reach your maximum, and score your company goals successfully.  Let’s start"), bn("h1", {
                                id: "effect3",
                                className: "first"
                            }, "We are the one-stop shop Testing Laboratory and Certification Body with strong expertise in fire protection requirements."), bn("h1", {
                                id: "effect4",
                                className: "second"
                            }, "We certify, test and provide advisory on development project. Let us help you win the competition, reach your maximum, and score your company goals successfully.  Let’s start")))), bn("div", {
                                className: "right"
                            }, bn("img", {
                                srcSet: "/assets/img/1.png, /assets/img/1.png 2x, assets/img/1.png 3x",
                                src: "/assets/img/1.png",
                                alt: "illustration character"
                            })), bn("div", {
                                id: "wrapper",
                                className: "absolute"
                            }, bn("div", {
                                id: "wrapper-inner"
                            }, bn("div", {
                                className: "text-center"
                            }, bn("span", {
                                id: "scroll-title",
                                className: "transform rotate-90"
                            }, "scroll")), bn("div", {
                                id: "scroll-down"
                            })))), bn("section", {
                                "data-anchor": "Page 2",
                                className: "section bg-yellow no-secret relative",
                                id: "no-secret",
                                "data-aos": "zoom-in"
                            }, bn("div", {
                                className: "container-3"
                            }, bn("p", {
                                className: "text-3"
                            }, "It is no secret that fire protection products/systems and building material must be tested and certified before introduction to market. To us, that is our expertise.", bn("br", null), bn("br", null), "Yet the reality is that most companies face challenges to navigate fire codes, standards and building regulations and don’t optimize their resources to meet fire safety requirements to ensure best positioning in the market  ̶ that’s where we come in.  ", bn("br", null), bn("br", null), "We facilitate market access to companies, guarantee products or materials integrity and accelerate project development in the construction sector. We analyse, certify and test in Dubai according to most relevant international and national standards for your business.", bn("br", null), bn("br", null), "And you do not have to delay your project to get there. "), bn("p", {
                                className: "text-5 text-center",
                                "data-aos": "fade-up"
                            }, "Trusted by leading multinational corporations, start-ups, SMBs, government entities, architects and more"), bn("div", {
                                className: "clients grid grid-cols-3 md:grid-cols-4",
                                "data-aos": "fade-right"
                            }, bn("img", {
                                src: "./assets/img/logo-2.png",
                                alt: ""
                            }), bn("img", {
                                src: "./assets/img/logo-1.png",
                                alt: ""
                            }), bn("img", {
                                src: "./assets/img/logo-3.png",
                                alt: ""
                            }), bn("img", {
                                src: "./assets/img/logo-4.png",
                                alt: ""
                            })), bn("p", {
                                className: "text-5 text-center",
                                "data-aos": "fade-up second_txt"
                            }, "Together, we shape safer city for a sustainable future")), bn("img", {
                                src: "/svg/cu logo text.svg",
                                id: "logo-text",
                                className: "absolute",
                                alt: ""
                            })), bn("section", {
                                "data-anchor": "Page 3",
                                className: "section bg-yellow meet-and-hire clearfix",
                                id: "meet-and-hire"
                            }, bn("div", {
                                className: "container-2 flex flex-wrap justify-between"
                            }, bn("div", {
                                className: "col-398 left",
                                style: {}
                            }, bn("p", {
                                className: "text-2",
                                "data-aos": "fade-right"
                            }, "Our multi-disciplinary fire protection experts are ready to help."), bn("p", {
                                className: "text-6",
                                "data-aos": "fade-right text_replace"
                            }, "Behind successful fire protection products, systems or materials are efficient and robust Testing, Inspection and Certification (TIC) processes. To us, that’s our DNA."),/* bn("p", {
                                className: "text-6",
                                "data-aos": "fade-right"
                            }, "We ensure safety, integrity, and quality of your passive or active fire protection systems through conformity assessment and assurance. We provide conformity assessment for Lorem Ipsum is simply dummy text of the printing and typesetting industry."),*/ bn("p", {
                                className: "text-6",
                                "data-aos": "fade-right"
                            }, "Our expertise spans a broad range of fields to support you at any stage of your development. We work along your team to understand your requirements and provide you with the most relevant solutions to accelerate your access to market."), bn(y.a, {
                                className: "mb-10 md:mb-0",
                                text: "How it works",
                                link: "https://google.com/",
                                dark: !0
                            })), bn("div", {
                                className: " grid right"
                            }, [ {
                                image: "/png/icon-sv-ondemand@3x.png",
                                title: "Reaction to Fire ",
                                description: "Evaluating how your material or system responds to fire. We measure the contribution of your material or system to a fire to which it is exposed. We analyse the combustibility and ignitibility of building materials or system to help you meet project requirement.  "
                            }, {
                                image: "/png/icon-sv-ondemand@3x.png",
                                title: "Fire Resistance ",
                                description: "Measuring the performance of your product in containing a fire and preventing it from spreading elsewhere. We test for fire-resistance rating of passive fire protection system, i.e. the duration for which it can withstand a standard fire resistance test."
                            }, {
                                image: "/png/icon-sv-specialist@3x.png",
                                title: "Façade Testing",
                                description: "Helping you understand the fire behaviour of a building façade based on the overall façade system’s performance and ensuring specified façade requirements are met. "
                            }, {
                                image: "/png/icon-sv-ondemand@3x.png",
                                title: "Fire Protection System Testing and Inspection ",
                                description: "Assessing the efficiency and effectiveness of your fire detection, protection, and extinguishing systems and ensuring they are compliant through testing and inspection. "
                            }, {
                                image: "/png/icon-sv-developer@3x.png",
                                title: "Fire Doors Testing and Certification",
                                description: "Helping you meet expected fire rating and specified fire door requirements."
                            },{
                                image: "/png/icon-sv-designer-2@3x.png",
                                title: "Product Certification",
                                description: "Gain recognition and trust in the market by ensuring the performance of your product over its lifetime."
                            }].map((function(n, e) {
                                return bn("div", {
                                    key: n.title,
                                    id: "meet-and-hire-col" + (e + 1),
                                    className: "meet-and-hire-item item bg-white col-398"
                                }, bn("div", {
                                    className: "image-container image-left"
                                }, bn("img", {
                                    src: n.image,
                                    alt: ""
                                })), bn("div", {
                                    className: "text-7 name"
                                }, n.title), bn("div", {
                                    className: "text-8 description"
                                }, n.description), bn(b.a, {
                                    href: "/our-talents"
                                }, bn("a", {
                                    href: "/our-talents",
                                    className: "block read-more text-7"
                                }, bn("span", null, "Read more"))))
                            }))))), bn(g.a, {
                                title: "When would you need us?",
                                subtitle: "As a Testing, Inspection and Certification (TIC) entity, we cater to diverse range of industry sectors and a variety of standards and regulations, therefore, we play a critical role in all economic fields related to active and passive fire protection systems, façade, [Lorem Ipsum].We take care of your quality and safety controls through conformity assessments with product certification, manufacturing site inspection and product testing. These are the services we offer to move your business forward.",
                                background: "bg-white",
                                buttonText: "Get started",
                                buttonUrl: "https://appslink-me.com/esl/register.html",
                                items: [{
                                    image: "/png/co-develop-product@3x.png",
                                    title: "Product Certification",
                                    description: "Gain trust and recognition with our certificate"
                                }, {
                                    image: "/png/new-quick-project@3x.png",
                                    title: "Fire Testing",
                                    description: "Test your products according to international standards"
                                }, {
                                    image: "/png/on-demand-talents@3x.png",
                                    title: "Façade Performance",
                                    description: "Assess the performance of your façade system"
                                }, {
                                    image: "/png/full-time@3x.png",
                                    title: "Inspection",
                                    description: "Lorem Ipsum is simply dummy text of the printing"
                                }, {
                                    image: "/png/full-time@3x.png",
                                    title: "Certification Renewal",
                                    description: "Lorem Ipsum is simply dummy text of the printing"
                                }, {
                                    image: "/png/full-time@3x.png",
                                    title: "Introduction to market",
                                    description: "Accelerate your entry to market with efficient approach"
                                }, {
                                    image: "/png/full-time@3x.png",
                                    title: "Regulations",
                                    description: "Understand the market fire protection requirements to accelerate your projects"
                                }, {
                                    image: "/png/full-time@3x.png",
                                    title: "Product Development",
                                    description: "Test your product or system during its development process"
                                }]
                            }), bn(u.a, {
                                title: "Move your business forward.",
                                subtitle: "Leverage fire safety expertise to deliver maximum value to your business and customers.",
                                link: "/get-started"
                            }), bn("section", {
                                className: "section services",
                                "data-aos": "slide-up"
                            }, bn("div", {
                                className: "container-2"
                            }, bn(h.a, {
                                title: "How do we add value?"
                            }), bn("div", {
                                className: "row-1 flex justify-between flex-wrap"
                            }, bn("div", {
                                className: "accordion-container",
                                "data-aos": "fade-right"
                            }, [{
                                name: "Manufacturers",
                                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.",
                                image: "/png/startups.png"
                            }, {
                                name: "Distributors, Agents",
                                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.",
                                image: "/png/smes.png"
                            }, {
                                name: "Architects",
                                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.",
                                image: "/png/corporations.png"
                            },{
                                name: "Contractors",
                                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.",
                                image: "/png/startups.png"
                            }, {
                                name: "Investors",
                                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.",
                                image: "/png/smes.png"
                            }, {
                                name: "Regulators",
                                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been.",
                                image: "/png/corporations.png"
                            }].map((function(n, e) {
                                return bn("div", {
                                    key: n.name,
                                    className: "item"
                                }, bn("div", {
                                    className: "accordion name ".concat(0 === e ? "active" : "")
                                }, n.name, bn("img", {
                                    className: "arrow float-right",
                                    src: "/svg/ic - arrow left_2.svg",
                                    alt: ""
                                })), bn("div", {
                                    className: "panel description",
                                    style: {
                                        display: 0 === e ? "block" : "none"
                                    }
                                }, n.description))
                            }))), bn("div", {
                                className: "image-container"
                            }, bn("img", {
                                className: "accordion-image",
                                src: "/png/startups.png",
                                style: {
                                    display: "block"
                                },
                                alt: ""
                            }), bn("img", {
                                className: "accordion-image",
                                src: "/png/smes.png",
                                style: {
                                    display: "none"
                                },
                                alt: ""
                            }), bn("img", {
                                className: "accordion-image",
                                src: "/png/corporations.png",
                                alt: "",
                                style: {
                                    display: "none"
                                }
                            }), bn("img", {
                                className: "accordion-image",
                                src: "/png/startups.png",
                                style: {
                                    display: "none"
                                },
                                alt: ""
                            }), bn("img", {
                                className: "accordion-image",
                                src: "/png/smes.png",
                                style: {
                                    display: "none"
                                },
                                alt: ""
                            }), bn("img", {
                                className: "accordion-image",
                                src: "/png/corporations.png",
                                alt: "",
                                style: {
                                    display: "none"
                                }
                            }))), bn("div", {
                                className: "link-container"
                            }, bn(y.a, {
                                text: "Our services",
                                link: "/service"
                            }))))/*, bn(kn, {
                                title: "Trusted by leading organisations",
                                background: "bg-white"
                            }), bn(hn, {
                                items: this.props.data
                            })*/, bn(m.a, {
                                background: "bg-light-grey-two",
                                title: "Let\u2019s get started.",
                                items: [{
                                    image: "/svg/step.svg",
                                    name: "Step 1",
                                    description: "Tell us about what services you require and why."
                                }, {
                                    image: "/svg/step.svg",
                                    name: "Step 2",
                                    description: "Let us prepare a plan for you."
                                }, {
                                    image: "/svg/step.svg",
                                    name: "Step 3",
                                    description: "Let us test and certify your product or system."
                                }, {
                                    image: "/svg/step.svg",
                                    name: "Step 4",
                                    description: "Access the market and move your business ahead."
                                }]
                            }), bn(u.a, {
                                title: "Gain recognition and trust. Move your business forward.",
                                subtitle: "Accelerating access to market through efficient conformity assessment.",
                                link: "https://appslink-me.com/esl/contact.html"
                            })))
                        }
                    }]), t
                }(c.Component)
        },
        "a3/r": function(n, e, t) {
            "use strict";
            t.d(e, "a", (function() {
                return l
            }));
            var i = t("q1tI"),
                a = t.n(i),
                r = t("YFqc"),
                o = t.n(r),
                s = a.a.createElement;

            function l(n) {
                var e = n.link,
                    t = n.text,
                    i = n.dark,
                    a = n.className,
                    r = void 0 === a ? "" : a,
                    l = n.effect,
                    c = void 0 === l ? "fade-up" : l;
                return e.startsWith("/get-started") ? s("a", {
                    href: e,
                    className: "btn btn-effect" + (i ? " btn-dark" : "") + " " + r,
                    "data-aos": c
                }, s("span", {
                    className: "arrow arrow-left"
                }), s("span", {
                    className: "btn-text"
                }, t), s("span", {
                    className: "arrow arrow-right"
                })) : s(o.a, {
                    href: e
                }, s("a", {
                    href: e,
                    className: "btn btn-effect" + (i ? " btn-dark" : "") + " " + r,
                    "data-aos": c
                }, s("span", {
                    className: "arrow arrow-left"
                }), s("span", {
                    className: "btn-text"
                }, t), s("span", {
                    className: "arrow arrow-right"
                })))
            }
        },
        a6RD: function(n, e, t) {
            "use strict";
            var i = t("oI91");

            function a(n, e) {
                var t = Object.keys(n);
                if (Object.getOwnPropertySymbols) {
                    var i = Object.getOwnPropertySymbols(n);
                    e && (i = i.filter((function(e) {
                        return Object.getOwnPropertyDescriptor(n, e).enumerable
                    }))), t.push.apply(t, i)
                }
                return t
            }

            function r(n) {
                for (var e = 1; e < arguments.length; e++) {
                    var t = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? a(Object(t), !0).forEach((function(e) {
                        i(n, e, t[e])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(n, Object.getOwnPropertyDescriptors(t)) : a(Object(t)).forEach((function(e) {
                        Object.defineProperty(n, e, Object.getOwnPropertyDescriptor(t, e))
                    }))
                }
                return n
            }
            e.__esModule = !0, e.noSSR = d, e.default = function(n, e) {
                var t = s.default,
                    i = {
                        loading: function(n) {
                            n.error, n.isLoading;
                            return n.pastDelay, null
                        }
                    };
                n instanceof Promise ? i.loader = function() {
                    return n
                } : "function" === typeof n ? i.loader = n : "object" === typeof n && (i = r(r({}, i), n));
                if (i = r(r({}, i), e), "object" === typeof n && !(n instanceof Promise) && (n.render && (i.render = function(e, t) {
                        return n.render(t, e)
                    }), n.modules)) {
                    t = s.default.Map;
                    var a = {},
                        o = n.modules();
                    Object.keys(o).forEach((function(n) {
                        var e = o[n];
                        "function" !== typeof e.then ? a[n] = e : a[n] = function() {
                            return e.then((function(n) {
                                return n.default || n
                            }))
                        }
                    })), i.loader = a
                }
                i.loadableGenerated && delete(i = r(r({}, i), i.loadableGenerated)).loadableGenerated;
                if ("boolean" === typeof i.ssr) {
                    if (!i.ssr) return delete i.ssr, d(t, i);
                    delete i.ssr
                }
                return t(i)
            };
            var o = l(t("q1tI")),
                s = l(t("2qu3"));

            function l(n) {
                return n && n.__esModule ? n : {
                    default: n
                }
            }
            var c = !1;

            function d(n, e) {
                if (delete e.webpack, delete e.modules, !c) return n(e);
                var t = e.loading;
                return function() {
                    return o.default.createElement(t, {
                        error: null,
                        isLoading: !0,
                        pastDelay: !1,
                        timedOut: !1
                    })
                }
            }
        },
        jwwS: function(n, e, t) {
            "use strict";
            var i;
            e.__esModule = !0, e.LoadableContext = void 0;
            var a = ((i = t("q1tI")) && i.__esModule ? i : {
                default: i
            }).default.createContext(null);
            e.LoadableContext = a
        },
        kJgk: function(n, e, t) {
            "use strict";
            t.d(e, "a", (function() {
                return c
            }));
            t("YFqc");
            var i = t("q1tI"),
                a = t.n(i),
                r = t("5pYx"),
                o = t("Jevo"),
                s = t("a3/r"),
                l = a.a.createElement;

            function c(n) {
                var e = n.title,
                    t = n.subtitle,
                    i = n.items,
                    a = n.buttonText,
                    c = n.buttonUrl,
                    d = n.background,
                    u = n.titleTextSize,
                    p = void 0 === u ? "text-5" : u,
                    f = n.cssClass,
                    m = void 0 === f ? "" : f,
                    g = n.imageLeft,
                    h = void 0 !== g && g,
                    v = n.companyList,
                    b = void 0 !== v && v;
                return l("section", {
                    className: "section multi-columns " + d + " " + m
                }, l("div", {
                    className: "container-2"
                }, l(r.a, {
                    title: e,
                    subtitle: t
                }), l("div", {
                    className: "items grid grid-cols-1 md:grid-cols-" + i.length
                }, i.map((function(n) {
                    return l("div", {
                        className: "text-center item",
                        key: n.title
                    }, l("div", {
                        className: "flex items-end image-container " + (h ? "image-left" : ""),
                        "data-aos": "slide-down"
                    }, l("img", {
                        src: n.image,
                        alt: ""
                    })), l("p", {
                        className: "title " + p,
                        "data-aos": "fade-up"
                    }, n.title), l("p", {
                        className: "description text-6",
                        dangerouslySetInnerHTML: {
                            __html: n.description
                        },
                        "data-aos": "fade-up"
                    }))
                }))), b ? l(o.a, {
                    title: "We're proud to work with some of the best teams"
                }) : "", c ? l("div", {
                    className: "text-center"
                }, l(s.a, {
                    link: c,
                    text: a
                })) : ""))
            }
        },
        sQcB: function(n, e, t) {
            "use strict";
            t.d(e, "a", (function() {
                return s
            }));
            var i = t("q1tI"),
                a = t.n(i),
                r = t("a3/r"),
                o = a.a.createElement;

            function s(n) {
                var e = n.title,
                    t = n.subtitle,
                    i = n.link,
                    a = void 0 === i ? "https://appslink-me.com/esl/contact.html" : i,
                    s = n.buttonText,
                    l = void 0 === s ? "Contact us" : s,
                    c = n.applyAsATalentButton,
                    d = void 0 !== c && c;
                return o("div", {
                    className: "section contact"
                }, o("div", {
                    className: "container-2"
                }, o("div", {
                    className: "flex flex-wrap justify-between"
                }, o("div", {
                    className: "text"
                }, o("div", {
                    className: "text-2",
                    "data-aos": "slide-up"
                }, e), t ? o("div", {
                    className: "text-6"
                }, t) : ""), o("div", {
                    className: "right text-center w-full md:w-auto"
                }, d ? o(r.a, {
                    link: "/apply-as-a-talent",
                    text: "Apply as talent",
                    effect: "slide-left",
                    className: "btn-talent-apply",
                    dark: !0
                }) : "", o(r.a, {
                    link: a,
                    text: l,
                    effect: "slide-left",
                    dark: !0
                })))))
            }
        },
        "sr+r": function(n, e, t) {
            "use strict";
            t.d(e, "h", (function() {
                return g
            })), t.d(e, "e", (function() {
                return h
            })), t.d(e, "a", (function() {
                return v
            })), t.d(e, "c", (function() {
                return b
            })), t.d(e, "b", (function() {
                return x
            })), t.d(e, "d", (function() {
                return y
            })), t.d(e, "f", (function() {
                return w
            })), t.d(e, "g", (function() {
                return k
            }));
            var i = t("h4VS"),
                a = t("vOnD");

            function r() {
                var n = Object(i.a)(["\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  cursor: pointer;\n  width: 180px;\n  height: 57px;\n  display: flex;\n  border: solid 3px #e1e0d5;\n  background: ", ";\n  margin-right: 20px;\n  font-family: Noto Sans;\n  font-size: 18px;\n  line-height: 1.56;\n\n  &:last-child {\n    margin-right: 0;\n  }\n\n  &:hover {\n    background: #ffe300;\n  }\n\n  @media (max-width: 767px) {\n    width: 140px;\n    height: 45px;\n    margin-top: 20px;\n    margin-right: 0;\n  }\n"]);
                return r = function() {
                    return n
                }, n
            }

            function o() {
                var n = Object(i.a)(["\n  width: 100%;\n  display: flex;\n  margin-top: 50px;\n  margin-bottom: 40px;\n\n  @media (max-width: 767px) {\n    flex-flow: wrap row;\n    justify-content: space-around;\n    margin: 20px 0;\n  }\n"]);
                return o = function() {
                    return n
                }, n
            }

            function s() {
                var n = Object(i.a)(["\n  font-size: 20px;\n  font-family: SofiaPro-Regular;\n  line-height: 1.6;\n  letter-spacing: normal;\n  margin-top: 25px;\n\n  @media (min-width: 1140px) and (max-width: 1280px) {\n    font-size: 18px;\n  }\n\n  @media (min-width: 1024px) and (max-width: 1140px) {\n    font-size: 16px;\n  }\n\n  @media (max-width: 768px) {\n    font-size: 14px;\n  }\n\n  @media (max-width: 767px) {\n    margin-top: 10px;\n  }\n"]);
                return s = function() {
                    return n
                }, n
            }

            function l() {
                var n = Object(i.a)(["\n  font-size: 90px;\n  font-weight: bold;\n  font-family: Noto Sans;\n  line-height: 1;\n\n  @media (min-width: 1140px) and (max-width: 1280px) {\n    font-size: 70px;\n  }\n\n  @media (max-width: 1140px) {\n    font-size: 50px;\n  }\n"]);
                return l = function() {
                    return n
                }, n
            }

            function c() {
                var n = Object(i.a)(['\n    background-image: url("', '");\n  ']);
                return c = function() {
                    return n
                }, n
            }

            function d() {
                var n = Object(i.a)(["\n  width: 100%;\n  height: 500px;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: 130% 120%;\n\n  ", "\n\n  @media (max-width: 1024px) {\n    background-size: 130% 100%;\n    height: 400px;\n  }\n\n  @media (max-width: 767px) {\n    background-position: -80px;\n    background-size: 100% 100%;\n  }\n\n  @media (max-width: 414px) {\n    background-position: -80px;\n    background-size: 86% 100%;\n    width: 200%;\n  }\n"]);
                return d = function() {
                    return n
                }, n
            }

            function u() {
                var n = Object(i.a)(["\n  position: absolute;\n  width: 45%;\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n\n  @media (max-width: 767px) {\n    width: 100%;\n    position: relative;\n    margin-top: 20px;\n  }\n"]);
                return u = function() {
                    return n
                }, n
            }

            function p() {
                var n = Object(i.a)(["\n  position: relative;\n  width: 100%;\n  height: 500px;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n\n  @media (max-width: 1140px) {\n    height: 400px;\n  }\n\n  @media (max-width: 768px) {\n    height: 350px;\n  }\n\n  @media (max-width: 767px) {\n    flex-direction: column;\n    height: 400px;\n  }\n\n\n  @media (max-width: 414px) {\n    margin-top: 20px;\n  }\n"]);
                return p = function() {
                    return n
                }, n
            }

            function f() {
                var n = Object(i.a)(["\n  width: 1240px;\n  display: flex;\n  flex-direction: column;\n\n  @media (min-width: 1140px) and (max-width: 1280px) {\n    width: 1024px;\n  }\n\n  @media (min-width: 1024px) and (max-width: 1140px) {\n    width: 970px;\n  }\n\n  @media (max-width: 1024px) {\n    width: 100%;\n    padding: 0 40px;\n  }\n\n  @media (max-width: 768px) {\n    width: 100%;\n    padding: 0 20px;\n  }\n"]);
                return f = function() {
                    return n
                }, n
            }

            function m() {
                var n = Object(i.a)(["\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  padding: 0;\n  background: transparent;\n"]);
                return m = function() {
                    return n
                }, n
            }
            var g = a.c.div(m()),
                h = a.c.div(f()),
                v = a.c.div(p()),
                b = a.c.div(u()),
                x = a.c.div(d(), (function(n) {
                    var e = n.imageUrl;
                    return Object(a.b)(c(), e)
                })),
                y = a.c.div(l()),
                w = (a.c.div(s()), a.c.div(o())),
                k = a.c.div(r(), (function(n) {
                    return n.active ? "#ffe300" : "transparent"
                }))
        },
        vlRD: function(n, e, t) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/", function() {
                return t("RNiq")
            }])
        }
    },
    [
        ["vlRD", 1, 0, 2, 3, 4, 7]
    ]
]);
