_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [21], {
        GIHZ: function(n, t, e) {
            "use strict";
            e.r(t), e.d(t, "__N_SSP", (function() {
                return Kn
            }));
            var i = e("q1tI"),
                r = e.n(i),
                a = e("8Kt/"),
                o = e.n(a),
                c = e("sr+r"),
                l = e("YFqc"),
                u = e.n(l),
                d = e("h4VS"),
                f = e("vOnD");

            function s() {
                var n = Object(d.a)(["\n  width: 100%;\n  display: flex;\n  align-items: center;\n  margin-bottom: 30px;\n\n  &:last-child {\n    margin-bottom: 0;\n  }\n\n  a {\n    width: 100%;\n    display: flex;\n    align-items: center;\n    height: 100%;\n\n    @media (max-width: 1023px) {\n      flex-direction: column;\n    }\n  }\n\n  @media (max-width: 1023px) {\n    margin-bottom: 25px;\n  }\n\n  &:hover {\n    ", " {\n      transform: scale(0.95);\n    }\n\n    .title-text {\n      background-size: 100% 100%;\n    }\n  };\n"]);
                return s = function() {
                    return n
                }, n
            }

            function p() {
                var n = Object(d.a)(["\n    width: 100% !important;\n    flex-flow: wrap row;\n    justify-content: space-between;\n\n    ", " {\n      width: calc(50% - 20px);\n\n      @media (max-width: 767px) {\n        width: 100%;\n      }\n\n    }\n  "]);
                return p = function() {
                    return n
                }, n
            }

            function m() {
                var n = Object(d.a)(["\n  width: calc(50% - 20px);\n  display: flex;\n  flex-direction: column;\n\n  @media (max-width: 1023px) {\n    width: 40%;\n  }\n\n  @media (max-width: 767px) {\n    width: 100%;\n    margin-top: 30px;\n  }\n\n  ", "\n"]);
                return m = function() {
                    return n
                }, n
            }

            function x() {
                var n = Object(d.a)(["\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -webkit-line-clamp: 3;\n  margin-bottom: 5px;\n  line-height: 1.18;\n  font-size: 28px;\n\n  .title-text {\n    display: inline;\n    font-size: 28px;\n    font-family: SofiaPro-Bold;\n    width: 100%;\n    margin-bottom: 10px;\n    color: #474747;\n    line-height: 1.18;\n    transition: background-size .4s ease;\n    background: linear-gradient(to bottom, transparent 65%, #00af66) center center/0% 75% no-repeat;\n    cursor: pointer;\n  }\n\n  z-index: 1;\n  position: relative;\n\n  @media (max-width: 1280px) {\n    font-size: 24px;\n\n    .title-text {\n      font-size: 24px;\n    }\n  }\n\n  @media (max-width: 1023px) {\n    -webkit-line-clamp: 2;\n  }\n"]);
                return x = function() {
                    return n
                }, n
            }

            function h() {
                var n = Object(d.a)(["\n  display: flex;\n  width: calc(100% - 270px);\n  flex-direction: column;\n  margin-left: 25px;\n\n  ", " {\n    margin: 0;\n  }\n\n  @media (max-width: 1280px) {\n    width: calc(100% - 240px);\n  }\n\n  @media (max-width: 1023px) {\n    width: 100%;\n    margin-top: 10px;\n    margin-left: 0;\n  }\n\n  @media (max-width: 767px) {\n    margin-top: 20px;\n  }\n"]);
                return h = function() {
                    return n
                }, n
            }

            function g() {
                var n = Object(d.a)(["\n  width: 245px;\n  display: flex;\n  transition: transform .2s;\n\n  img {\n    width: 100%;\n    height: 160px;\n    object-fit: cover;\n  }\n\n  @media (max-width: 1280px) {\n    width: 215px;\n\n    img {\n      height: 140px;\n    }\n  }\n\n  @media (max-width: 1023px) {\n    width: 100%;\n  }\n\n  @media (max-width: 767px) {\n    img {\n      height: 160px;\n    }\n  }\n"]);
                return g = function() {
                    return n
                }, n
            }

            function b() {
                var n = Object(d.a)(["\n  font-size: 18px;\n  font-family: SofiaPro-Bold;\n"]);
                return b = function() {
                    return n
                }, n
            }

            function w() {
                var n = Object(d.a)(["\n  font-size: 18px;\n"]);
                return w = function() {
                    return n
                }, n
            }

            function v() {
                var n = Object(d.a)(["\n  display: flex;\n  flex-direction: column;\n  margin-left: 15px;\n"]);
                return v = function() {
                    return n
                }, n
            }

            function y() {
                var n = Object(d.a)(["\n  width: 55px;\n  height: 55px;\n  border-radius: 50%;\n"]);
                return y = function() {
                    return n
                }, n
            }

            function j() {
                var n = Object(d.a)(["\n  width: 100%;\n  display: flex;\n  align-items: center;\n"]);
                return j = function() {
                    return n
                }, n
            }

            function O() {
                var n = Object(d.a)(["\n  font-size: 20px;\n  width: 100%;\n  margin-bottom: 25px;\n  color: #474747;\n  font-family: SofiaPro-Regular;\n  line-height: 1.6;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-line-clamp: 3;\n  -webkit-box-orient: vertical;\n\n  @media (max-width: 1023px) {\n    -webkit-line-clamp: 7;\n  }\n"]);
                return O = function() {
                    return n
                }, n
            }

            function k() {
                var n = Object(d.a)(["\n  display: flex;\n  flex-direction: column;\n  height: auto;\n  overflow: hidden;\n"]);
                return k = function() {
                    return n
                }, n
            }

            function z() {
                var n = Object(d.a)(["\n  width: auto;\n  font-size: 14px;\n  margin: 20px 0 25px 0;\n  color: #474747;\n  font-family: SofiaPro-Regular;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.43;\n"]);
                return z = function() {
                    return n
                }, n
            }

            function P() {
                var n = Object(d.a)(["\n  width: auto;\n  padding: 4px 15px;\n  background-image: linear-gradient(251deg, #474747, #313131);\n  color: #00af66;\n  font-size: 18px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  position: absolute;\n  bottom: 0;\n  font-family: SofiaPro-Regular;\n"]);
                return P = function() {
                    return n
                }, n
            }

            function N() {
                var n = Object(d.a)(["\n  width: calc(50% - 20px);\n  display: flex;\n  flex-direction: column;\n\n  @media (max-width: 1023px) {\n    width: calc(60% - 20px);\n  }\n\n  @media (max-width: 767px) {\n    width: 100%;\n  }\n\n\n  &:hover {\n    ", " {\n      transform: scale(0.95);\n    }\n\n    .title-text {\n      background-size: 100% 100%;\n    }\n  };\n"]);
                return N = function() {
                    return n
                }, n
            }

            function S() {
                var n = Object(d.a)(["\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -webkit-line-clamp: 4;\n  margin-bottom: 5px;\n  line-height: 1.18;\n  font-size: 34px;\n\n  .title-text {\n    display: inline;\n    font-size: 34px;\n    font-family: SofiaPro-Bold;\n    width: 100%;\n    margin-bottom: 10px;\n    color: #474747;\n    line-height: 1.18;\n    transition: background-size .4s ease;\n    background: linear-gradient(to bottom, transparent 65%, #00af66) center center/0% 75% no-repeat;\n    cursor: pointer;\n  }\n\n  z-index: 1;\n  position: relative;\n"]);
                return S = function() {
                    return n
                }, n
            }

            function E() {
                var n = Object(d.a)(["\n  width: 100%;\n  height: 350px;\n  display: flex;\n  position: relative;\n  transition: transform .2s;\n\n  img {\n    width: 100%;\n    object-fit: cover;\n    height: 100%;\n  }\n\n  @media (max-width: 1023px) {\n    height: 300px;\n  }\n\n  @media (max-width: 767px) {\n    height: 250px;\n  }\n"]);
                return E = function() {
                    return n
                }, n
            }

            function I() {
                var n = Object(d.a)(["\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n  font-family: SofiaPro-Regular;\n  margin-top: 20px;\n  margin-bottom: 50px;\n\n  @media (max-width: 767px) {\n    flex-direction: column;\n  }\n"]);
                return I = function() {
                    return n
                }, n
            }
            var _ = f.c.div(I()),
                R = f.c.div(E()),
                B = f.c.div(S()),
                q = f.c.div(N(), R),
                T = f.c.div(P()),
                C = f.c.div(z()),
                A = f.c.div(k()),
                L = f.c.div(O()),
                V = f.c.div(j()),
                D = f.c.img(y()),
                H = f.c.div(v()),
                Y = f.c.div(w()),
                F = f.c.div(b()),
                G = f.c.div(g()),
                J = f.c.div(h(), C),
                K = f.c.div(x()),
                M = f.c.div(m(), (function(n) {
                    return n.mainEmpty && Object(f.b)(p(), Q)
                })),
                Q = f.c.div(s(), G),
                U = r.a.createElement,
                X = function(n) {
                    var t = n.data,
                        e = n.mainItem;
                    return e || 0 !== t.length ? U(_, null, e && U(q, null, U(u.a, {
                        href: "/blog/".concat(e.slug)
                    }, U("a", {
                        href: "/blog/".concat(e.slug)
                    }, U(R, null, U("img", {
                        src: e.image,
                        alt: "error"
                    }), U(T, null, e.featuredPost)))), U(C, null, "".concat(e.category.text, " - ").concat(e.readTimes)), U(A, null, U(u.a, {
                        href: "/blog/".concat(e.slug)
                    }, U("a", {
                        href: "/blog/".concat(e.slug)
                    }, U(B, null, U("span", {
                        className: "title-text"
                    }, e.title)))), U(L, null, e.shortDescription)), U(V, null, U(D, {
                        src: e.profileImage,
                        alt: "erorr"
                    }), U(H, null, U(Y, null, e.profileName), U(F, null, e.profilePosition)))), U(M, {
                        mainEmpty: !e
                    }, t.map((function(n) {
                        return U(Q, {
                            key: n.id
                        }, U(u.a, {
                            href: "/blog/".concat(n.slug)
                        }, U("a", {
                            href: "/blog/".concat(n.slug)
                        }, U(G, null, U("img", {
                            src: n.image,
                            alt: "error"
                        })), U(J, null, U(C, null, "".concat(n.category.text, " - ").concat(n.readTimes)), U(K, null, U("div", {
                            className: "title-text"
                        }, n.title))))))
                    })))) : null
                };

            function Z() {
                var n = Object(d.a)(["\n  cursor: pointer;\n  margin-top: 40px;\n"]);
                return Z = function() {
                    return n
                }, n
            }

            function W() {
                var n = Object(d.a)(["\n  font-size: 18px;\n  font-family: SofiaPro-Bold;\n"]);
                return W = function() {
                    return n
                }, n
            }

            function $() {
                var n = Object(d.a)(["\n  font-size: 18px;\n  width: 100%;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n"]);
                return $ = function() {
                    return n
                }, n
            }

            function nn() {
                var n = Object(d.a)(["\n  display: flex;\n  flex-direction: column;\n  margin-left: 15px;\n  overflow: hidden;\n"]);
                return nn = function() {
                    return n
                }, n
            }

            function tn() {
                var n = Object(d.a)(["\n  width: 55px;\n  height: 55px;\n  border-radius: 50%;\n"]);
                return tn = function() {
                    return n
                }, n
            }

            function en() {
                var n = Object(d.a)(["\n  width: 100%;\n  display: flex;\n  align-items: center;\n"]);
                return en = function() {
                    return n
                }, n
            }

            function rn() {
                var n = Object(d.a)(["\n  font-size: 20px;\n  width: 100%;\n  color: #474747;\n  font-family: SofiaPro-Regular;\n  line-height: 1.6;\n  margin-top: 5px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -webkit-line-clamp: ", ";\n\n  @media (max-width: 767px) {\n    font-size: 18px;\n  }\n"]);
                return rn = function() {
                    return n
                }, n
            }

            function an() {
                var n = Object(d.a)(["\n  display: flex;\n  flex-direction: column;\n  height: 270px;\n  overflow: hidden;\n  margin-bottom: 25px;\n"]);
                return an = function() {
                    return n
                }, n
            }

            function on() {
                var n = Object(d.a)(["\n  width: 100%;\n  height: 1px;\n  background: #cccccc;\n  margin: 20px 0 25px 0;\n"]);
                return on = function() {
                    return n
                }, n
            }

            function cn() {
                var n = Object(d.a)(["\n  width: auto;\n  font-size: 14px;\n  color: #474747;\n  font-family: SofiaPro-Regular;\n  font-stretch: normal;\n  font-style: normal;\n  line-height: 1.43;\n  margin-top: 20px;\n"]);
                return cn = function() {
                    return n
                }, n
            }

            function ln() {
                var n = Object(d.a)(["\n  display: none;\n  width: auto;\n  padding: 4px 15px;\n  background-image: linear-gradient(251deg, #474747, #313131);\n  color: #00af66;\n  font-size: 18px;\n  justify-content: center;\n  align-items: center;\n  position: absolute;\n  bottom: 0;\n  font-family: SofiaPro-Regular;\n"]);
                return ln = function() {
                    return n
                }, n
            }

            function un() {
                var n = Object(d.a)(["\n  width: 370px;\n  display: flex;\n  flex-direction: column;\n  margin-top: 50px;\n  margin-right: 65px;\n\n  @media(min-width: 1280px) {\n    &:nth-child(3n) {\n      margin-right: 0;\n    }\n  }\n\n  @media (max-width: 1280px) {\n    width: calc(50% - 30px);\n    margin-right: 0;\n  }\n\n  @media (max-width: 1024px) {\n    width: calc(50% - 15px);\n    margin-right: 0;\n  }\n\n  @media (max-width: 767px) {\n    width: 100%;\n    margin-right: 0;\n  }\n\n  &:hover {\n    ", " {\n      transform: scale(0.95);\n    }\n\n    .title-text {\n      background-size: 100% 100%;\n    }\n  };\n"]);
                return un = function() {
                    return n
                }, n
            }

            function dn() {
                var n = Object(d.a)(["\n  width: 100%;\n  height: 250px;\n  display: flex;\n  position: relative;\n  transition: transform .2s;\n\n  img {\n    width: 100%;\n    object-fit: cover;\n    height: 100%;\n  }\n"]);
                return dn = function() {
                    return n
                }, n
            }

            function fn() {
                var n = Object(d.a)(["\n  overflow: hidden;\n  text-overflow: ellipsis;\n  display: -webkit-box;\n  -webkit-box-orient: vertical;\n  -webkit-line-clamp: ", ";\n  margin-bottom: 5px;\n  line-height: 1.18;\n  font-size: 34px;\n\n  .title-text {\n    display: inline;\n    font-size: 34px;\n    font-family: SofiaPro-Bold;\n    width: 100%;\n    margin-bottom: 10px;\n    color: #474747;\n    line-height: 1.18;\n    transition: background-size .4s ease;\n    background: linear-gradient(to bottom, transparent 65%, #00af66) center center/0% 75% no-repeat;\n    cursor: pointer;\n  }\n\n  z-index: 1;\n  position: relative;\n"]);
                return fn = function() {
                    return n
                }, n
            }

            function sn() {
                var n = Object(d.a)(["\n  width: 100%;\n  display: flex;\n  flex-flow: wrap row;\n\n  @media (max-width: 1280px) {\n    justify-content: space-between;\n  }\n"]);
                return sn = function() {
                    return n
                }, n
            }

            function pn() {
                var n = Object(d.a)(["\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  margin-bottom: 70px;\n"]);
                return pn = function() {
                    return n
                }, n
            }
            var mn = f.c.div(pn()),
                xn = f.c.div(sn()),
                hn = f.c.div(fn(), (function(n) {
                    return n.number
                })),
                gn = f.c.div(dn()),
                bn = f.c.div(un(), gn),
                wn = f.c.div(ln()),
                vn = f.c.div(cn()),
                yn = f.c.div(on()),
                jn = f.c.div(an()),
                On = f.c.div(rn(), (function(n) {
                    return n.number
                })),
                kn = f.c.div(en()),
                zn = f.c.img(tn()),
                Pn = f.c.div(nn()),
                Nn = f.c.div($()),
                Sn = f.c.div(W()),
                En = f.c.a(Z()),
                In = r.a.createElement;

            function _n(n, t) {
                var e = window.getComputedStyle(n, null),
                    i = n.offsetHeight,
                    r = parseInt(e.getPropertyValue("font-size")),
                    a = parseInt(e.getPropertyValue("line-height"));
                e.getPropertyValue("box-sizing");
                return isNaN(a) && (a = r * t), Math.ceil(i / a)
            }
            var Rn = function(n) {
                    var t = n.item,
                        e = Object(i.useState)({
                            title: 5,
                            description: 5
                        }),
                        r = e[0],
                        a = e[1],
                        o = Object(i.useRef)(),
                        c = Object(i.useRef)();
                    return Object(i.useEffect)((function() {
                        var n = _n(o.current, 1.18),
                            t = _n(c.current, 1.6);
                        (n = n % 2 !== 0 ? parseInt(n) + 1 : parseInt(n)) < 7 && (t = 7 - n), a({
                            title: n,
                            description: parseInt(t)
                        })
                    }), []), In(bn, null, In(u.a, {
                        href: "/blog/".concat(t.slug)
                    }, In("a", {
                        href: "/blog/".concat(t.slug)
                    }, In(gn, null, In("img", {
                        src: t.image,
                        alt: "error"
                    }), In(wn, null, t.featuredPost)))), In(vn, null, "".concat(t.category.text, " - ").concat(t.readTimes)), In(yn, null), In(jn, null, In(u.a, {
                        href: "/blog/".concat(t.slug)
                    }, In("a", {
                        href: "/blog/".concat(t.slug)
                    }, In(hn, {
                        number: r.title
                    }, In("div", {
                        ref: o,
                        className: "title-text"
                    }, t.title)))), In(On, {
                        ref: c,
                        number: r.description
                    }, t.shortDescription)), In(kn, null, In(zn, {
                        src: t.profileImage,
                        alt: "erorr"
                    }), In(Pn, null, In(Nn, null, t.profileName), In(Sn, null, t.profilePosition))), In(yn, null))
                },
                Bn = e("/MKj"),
                qn = e("nOHt"),
                Tn = e.n(qn),
                Cn = e("lfRO"),
                An = r.a.createElement,
                Ln = function(n) {
                    var t = n.data,
                        e = Object(Bn.b)(),
                        r = Object(qn.useRouter)(),
                        a = Object(i.useState)({
                            items: t.items,
                            totalPage: t.totalPage,
                            currentPage: t.currentPage
                        }),
                        o = a[0],
                        c = a[1],
                        l = o.items,
                        u = (o.loading, o.totalPage),
                        d = o.currentPage,
                        f = function(n) {
                            c({
                                items: l.concat(n.items),
                                totalPage: n.totalPage,
                                currentPage: n.currentPage
                            })
                        },
                        s = Object(i.useCallback)((function(n, t) {
                            return e(Object(Cn.b)(n, t))
                        }), [e]);
                    return Object(i.useEffect)((function() {
                        c({
                            items: t.items,
                            totalPage: t.totalPage,
                            currentPage: t.currentPage
                        })
                    }), [t]), 0 === l.length ? null : An(mn, null, An(xn, null, l.map((function(n) {
                        return An(Rn, {
                            item: n,
                            key: n.id
                        })
                    }))), u > 1 && d < u && An(En, {
                        className: "btn btn-effect",
                        onClick: function() {
                            return s({
                                type: r.query.type || "all",
                                page: d + 1
                            }, f)
                        }
                    }, An("span", {
                        className: "arrow arrow-left"
                    }), An("span", {
                        className: "btn-text"
                    }, "Load more"), An("span", {
                        className: "arrow arrow-right"
                    })))
                },
                Vn = r.a.createElement,
                Dn = function() {
                    var n = Object(qn.useRouter)(),
                        t = Object(i.useState)(n.query.type || "all"),
                        e = t[0],
                        r = t[1],
                        a = function(n) {
                            return Tn.a.push({
                                pathname: "/blogs",
                                query: {
                                    type: n
                                }
                            })
                        };
                    return Object(i.useEffect)((function() {
                        r(n.query.type || "all")
                    }), [n.query]), Vn(c.f, null, Vn(c.g, {
                        active: "all" === e,
                        onClick: function() {
                            return a("all")
                        }
                    }, "All"), Vn(c.g, {
                        active: "articles" === e,
                        onClick: function() {
                            return a("articles")
                        }
                    }, "Articles"))
                },
                Hn = r.a.createElement,
                Yn = function(n) {
                    var t = n.data;
                    return Hn(c.h, null, Hn(c.e, null, Hn(c.a, null, Hn(c.c, null, Hn(c.d, null, "Chanceupon"), Hn(c.d, null, "Insights")), Hn(c.b, {
                        imageUrl: "/png/blog-banner-1.png"
                    })), Hn(Dn, null), Hn(X, {
                        data: t.mainItems,
                        mainItem: t.firstItem
                    }), Hn(Ln, {
                        data: t
                    })))
                },
                Fn = e("CafY"),
                Gn = e("sQcB"),
                Jn = r.a.createElement,
                Kn = !0;
            t.default = function(n) {
                var t = n.data;
                return Jn(r.a.Fragment, null, Jn(o.a, null, Jn("title", null, "Blogs")), Jn(Fn.a, null, Jn(Yn, {
                    data: t
                }), Jn(Gn.a, {
                    title: "Connect with your team today!",
                    subtitle: "You will be astonished by what you could assemble.",
                    applyAsATalentButton: !1
                })))
            }
        },
        LLLT: function(n, t, e) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/blogs", function() {
                return e("GIHZ")
            }])
        },
        "a3/r": function(n, t, e) {
            "use strict";
            e.d(t, "a", (function() {
                return l
            }));
            var i = e("q1tI"),
                r = e.n(i),
                a = e("YFqc"),
                o = e.n(a),
                c = r.a.createElement;

            function l(n) {
                var t = n.link,
                    e = n.text,
                    i = n.dark,
                    r = n.className,
                    a = void 0 === r ? "" : r,
                    l = n.effect,
                    u = void 0 === l ? "fade-up" : l;
                return t.startsWith("/get-started") ? c("a", {
                    href: t,
                    className: "btn btn-effect" + (i ? " btn-dark" : "") + " " + a,
                    "data-aos": u
                }, c("span", {
                    className: "arrow arrow-left"
                }), c("span", {
                    className: "btn-text"
                }, e), c("span", {
                    className: "arrow arrow-right"
                })) : c(o.a, {
                    href: t
                }, c("a", {
                    href: t,
                    className: "btn btn-effect" + (i ? " btn-dark" : "") + " " + a,
                    "data-aos": u
                }, c("span", {
                    className: "arrow arrow-left"
                }), c("span", {
                    className: "btn-text"
                }, e), c("span", {
                    className: "arrow arrow-right"
                })))
            }
        },
        sQcB: function(n, t, e) {
            "use strict";
            e.d(t, "a", (function() {
                return c
            }));
            var i = e("q1tI"),
                r = e.n(i),
                a = e("a3/r"),
                o = r.a.createElement;

            function c(n) {
                var t = n.title,
                    e = n.subtitle,
                    i = n.link,
                    r = void 0 === i ? "/get-started" : i,
                    c = n.buttonText,
                    l = void 0 === c ? "Contact us" : c,
                    u = n.applyAsATalentButton,
                    d = void 0 !== u && u;
                return o("div", {
                    className: "section contact"
                }, o("div", {
                    className: "container-2"
                }, o("div", {
                    className: "flex flex-wrap justify-between"
                }, o("div", {
                    className: "text"
                }, o("div", {
                    className: "text-2",
                    "data-aos": "slide-up"
                }, t), e ? o("div", {
                    className: "text-6"
                }, e) : ""), o("div", {
                    className: "right text-center w-full md:w-auto"
                }, d ? o(a.a, {
                    link: "/apply-as-a-talent",
                    text: "Apply as talent",
                    effect: "slide-left",
                    className: "btn-talent-apply",
                    dark: !0
                }) : "", o(a.a, {
                    link: r,
                    text: l,
                    effect: "slide-left",
                    dark: !0
                })))))
            }
        },
        "sr+r": function(n, t, e) {
            "use strict";
            e.d(t, "h", (function() {
                return x
            })), e.d(t, "e", (function() {
                return h
            })), e.d(t, "a", (function() {
                return g
            })), e.d(t, "c", (function() {
                return b
            })), e.d(t, "b", (function() {
                return w
            })), e.d(t, "d", (function() {
                return v
            })), e.d(t, "f", (function() {
                return y
            })), e.d(t, "g", (function() {
                return j
            }));
            var i = e("h4VS"),
                r = e("vOnD");

            function a() {
                var n = Object(i.a)(["\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  cursor: pointer;\n  width: 180px;\n  height: 57px;\n  display: flex;\n  border: solid 3px #e1e0d5;\n  background: ", ";\n  margin-right: 20px;\n  font-family: SofiaPro-Bold;\n  font-size: 18px;\n  line-height: 1.56;\n\n  &:last-child {\n    margin-right: 0;\n  }\n\n  &:hover {\n    background: #00af66;\n  }\n\n  @media (max-width: 767px) {\n    width: 140px;\n    height: 45px;\n    margin-top: 20px;\n    margin-right: 0;\n  }\n"]);
                return a = function() {
                    return n
                }, n
            }

            function o() {
                var n = Object(i.a)(["\n  width: 100%;\n  display: flex;\n  margin-top: 50px;\n  margin-bottom: 40px;\n\n  @media (max-width: 767px) {\n    flex-flow: wrap row;\n    justify-content: space-around;\n    margin: 20px 0;\n  }\n"]);
                return o = function() {
                    return n
                }, n
            }

            function c() {
                var n = Object(i.a)(["\n  font-size: 20px;\n  font-family: SofiaPro-Regular;\n  line-height: 1.6;\n  letter-spacing: normal;\n  margin-top: 25px;\n\n  @media (min-width: 1140px) and (max-width: 1280px) {\n    font-size: 18px;\n  }\n\n  @media (min-width: 1024px) and (max-width: 1140px) {\n    font-size: 16px;\n  }\n\n  @media (max-width: 768px) {\n    font-size: 14px;\n  }\n\n  @media (max-width: 767px) {\n    margin-top: 10px;\n  }\n"]);
                return c = function() {
                    return n
                }, n
            }

            function l() {
                var n = Object(i.a)(["\n  font-size: 90px;\n  font-weight: bold;\n  font-family: EB Garamond;\n  line-height: 1;\n\n  @media (min-width: 1140px) and (max-width: 1280px) {\n    font-size: 70px;\n  }\n\n  @media (max-width: 1140px) {\n    font-size: 50px;\n  }\n"]);
                return l = function() {
                    return n
                }, n
            }

            function u() {
                var n = Object(i.a)(['\n    background-image: url("', '");\n  ']);
                return u = function() {
                    return n
                }, n
            }

            function d() {
                var n = Object(i.a)(["\n  width: 100%;\n  height: 500px;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: 130% 120%;\n\n  ", "\n\n  @media (max-width: 1024px) {\n    background-size: 130% 100%;\n    height: 400px;\n  }\n\n  @media (max-width: 767px) {\n    background-position: -80px;\n    background-size: 100% 100%;\n  }\n\n  @media (max-width: 414px) {\n    background-position: -80px;\n    background-size: 86% 100%;\n    width: 200%;\n  }\n"]);
                return d = function() {
                    return n
                }, n
            }

            function f() {
                var n = Object(i.a)(["\n  position: absolute;\n  width: 45%;\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n\n  @media (max-width: 767px) {\n    width: 100%;\n    position: relative;\n    margin-top: 20px;\n  }\n"]);
                return f = function() {
                    return n
                }, n
            }

            function s() {
                var n = Object(i.a)(["\n  position: relative;\n  width: 100%;\n  height: 500px;\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n\n  @media (max-width: 1140px) {\n    height: 400px;\n  }\n\n  @media (max-width: 768px) {\n    height: 350px;\n  }\n\n  @media (max-width: 767px) {\n    flex-direction: column;\n    height: 400px;\n  }\n\n\n  @media (max-width: 414px) {\n    margin-top: 20px;\n  }\n"]);
                return s = function() {
                    return n
                }, n
            }

            function p() {
                var n = Object(i.a)(["\n  width: 1240px;\n  display: flex;\n  flex-direction: column;\n\n  @media (min-width: 1140px) and (max-width: 1280px) {\n    width: 1024px;\n  }\n\n  @media (min-width: 1024px) and (max-width: 1140px) {\n    width: 970px;\n  }\n\n  @media (max-width: 1024px) {\n    width: 100%;\n    padding: 0 40px;\n  }\n\n  @media (max-width: 768px) {\n    width: 100%;\n    padding: 0 20px;\n  }\n"]);
                return p = function() {
                    return n
                }, n
            }

            function m() {
                var n = Object(i.a)(["\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  padding: 0;\n  background: transparent;\n"]);
                return m = function() {
                    return n
                }, n
            }
            var x = r.c.div(m()),
                h = r.c.div(p()),
                g = r.c.div(s()),
                b = r.c.div(f()),
                w = r.c.div(d(), (function(n) {
                    var t = n.imageUrl;
                    return Object(r.b)(u(), t)
                })),
                v = r.c.div(l()),
                y = (r.c.div(c()), r.c.div(o())),
                j = r.c.div(a(), (function(n) {
                    return n.active ? "#00af66" : "transparent"
                }))
        }
    },
    [
        ["LLLT", 1, 0, 2, 3, 4, 5, 6, 11]
    ]
]);