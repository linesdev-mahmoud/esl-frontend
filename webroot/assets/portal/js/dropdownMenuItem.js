// E1 Menu constants for accessing translated strings used for accessibility
// These must first be defined in OWResource.java and DropdownInitializationTag.java
var ACCESSIBILITY_EXPANDED = 0, ACCESSIBILITY_COLLAPSED = 1,
    ACCESS_E1_MENU_APP_TASK = 2, ACCESS_E1_MENU_REPORT_TASK = 3, ACCESSIBILITY_E1_MENU_ACTION = 4,
    ACCESS_E1_MENU_CRYS_REP_TASK = 5, ACCESSIBILITY_E1_MENU_URL_TASK = 6,
    ACCESSIBILITY_E1_MENU_URL_IS = 7, ACCESSIBILITY_E1_MENU_FOR = 8,
    ACCESS_E1_MENU_OVR_TASK = 9;
function jdeMItm(shortId, indentLevel, hasChildren, label, tmTaskNM, id, taskId, parentTaskId, taskView, seqNum, role, objId, formId, versionId, menuSysCode, formDSTmpl, formDSData, appMode, RID, menuNodeInstanceId) {

    jdeMItm.prototype.shortId = shortId;
    jdeMItm.prototype.indentLevel = indentLevel;
    jdeMItm.prototype.hasChildren = hasChildren;
    jdeMItm.prototype.label = label;
    jdeMItm.prototype.tmTaskNM = tmTaskNM;
    jdeMItm.prototype.id = id;
    jdeMItm.prototype.taskId = taskId;
    jdeMItm.prototype.parentTaskId = parentTaskId;
    jdeMItm.prototype.taskView = taskView;
    jdeMItm.prototype.seqNum = seqNum;
    jdeMItm.prototype.role = role;
    jdeMItm.prototype.objId = objId;
    jdeMItm.prototype.formId = formId;
    jdeMItm.prototype.versionId = versionId;
    jdeMItm.prototype.menuSysCode = menuSysCode;
    jdeMItm.prototype.formDSTmpl = formDSTmpl;
    jdeMItm.prototype.formDSData = formDSData;
    jdeMItm.prototype.appMode = appMode;
    jdeMItm.prototype.spacerWidth = indentLevel * 12;
    jdeMItm.prototype.RID = RID;
    jdeMItm.prototype.menuNodeInstanceId = menuNodeInstanceId;
    jdeMItm.prototype.isOVRTask = false;

    var userAgent = navigator.userAgent.toUpperCase();
    jdeMItm.prototype.isWebKit = userAgent.indexOf("WEBKIT") > -1;
    jdeMItm.prototype.isTouchEnabled = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) ||
    (userAgent.indexOf("IPHONE") > -1) || (userAgent.indexOf("ANDROID") > -1);
    jdeMItm.prototype.hasWebkitTouchSupport = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) ||
    (userAgent.indexOf("IPHONE") > -1) || (userAgent.indexOf("ANDROID") > -1);
    jdeMItm.prototype.flyoutSupported = true;
    jdeMItm.prototype.flyoutCount = 0;
    jdeMItm.prototype.flyoutIndex = 0;
    jdeMItm.prototype.popupLength = 0;
    jdeMItm.prototype.s1 = "";
    jdeMItm.prototype.submenuStyle = "showMenu";
    jdeMItm.prototype.hidemenuStyle = "hideMenu";
    jdeMItm.prototype.flyoutStyle = "customNodeFlyOut";
    jdeMItm.prototype.flyoutHiddenStyle = "customNodeFlyOutHidden";
    if (jdeMenuParent.isRtlEnabled) {
        this.flyoutStyle += "_rtl";
        this.flyoutHiddenStyle += "_rtl";
    }

    jdeMItm.prototype.nodeHead = function (appType) {
        var sb = new PSStringBuffer();
        var userDefinedFolderID = "";
        var myAppType = appType;
        if (myAppType == "APP_OVR") {
            this.isOVRTask = true;
            myAppType = "APP";
        }
        if (myAppType == "") {
            var taskIDLength = this.tmTaskNM.length;
            userDefinedFolderID = this.tmTaskNM.substring(taskIDLength - 3, taskIDLength);
        }

        sb.append("<div id=\"head");
        sb.append(this.shortId);

        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append("\" aria-hidden=\"false\" role=\"menu");
        }

        sb.append("\">\n<table class=\"MenuNormal\" id=\"menuItem");
        sb.append(this.shortId);
        sb.append("\" cellpadding=0 cellspacing=0 tabIndex=0");

        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(" role=\"menuitem\" aria-labelledby=\"fldnode");
            sb.append(this.shortId);
            sb.append("\"");
            if (myAppType != null) {
                if ((myAppType == "" && this.hasChildren) || myAppType == "FOLDER" || userDefinedFolderID == "UDF") {
                    sb.append(" aria-haspopup=\"true\"");
                }
                else if (myAppType == "APP" || myAppType == "UBE") {
                    sb.append(" aria-describedby=\"flddescnode");
                    sb.append(this.shortId);
                    sb.append("\"");
                }
            }
        }

        sb.append(" onmouseover=\"menuItemHover(this,");
        sb.append(this.indentLevel + ");\"");
        sb.append("onmouseout=\"menuItemUnhover(this,");
        sb.append(this.indentLevel + ");\"");
        sb.append("onblur=\"menuItemUnhover(this,");
        sb.append(this.indentLevel + ");\"");
        sb.append("onfocus=\"menuItemHover(this);\"");

        sb.append("onClick=\"");
        sb.append(this.getNodeActivate() + ";" + "menuItemClick(this,");
        sb.append(this.indentLevel + ",event);");
        sb.append("showSubMenu(event,'");
        sb.append("treechild");
        sb.append(this.shortId);
        sb.append("',");
        sb.append(this.indentLevel);
        sb.append(",'");
        sb.append(this.shortId);
        sb.append("submenu");
        sb.append("'");
        sb.append(");\"");

        sb.append("oncontextmenu=\"displayTaskContextMenu(event,'");
        sb.append(this.shortId);
        sb.append("pop',");
        sb.append(this.popupLength);
        sb.append(", 'FlyOut");
        sb.append(this.shortId);
        sb.append("');\"");

        if (this.isTouchEnabled) {
            sb.append(" ontouchstart=\"DROPDOWNMENUTOUCH.touchMenuStart(event,DROPDOWNMENUTOUCH.TASK,'");
            sb.append(this.shortId);
            sb.append("pop',");
            sb.append(this.popupLength);
            sb.append(", 'FlyOut");
            sb.append(this.shortId);
            sb.append("');\"");

            sb.append(" ontouchmove=\"DROPDOWNMENUTOUCH.touchMenuMove(event);\"");
            sb.append(" ontouchend=\"DROPDOWNMENUTOUCH.touchMenuEnd(event);\"");
            sb.append(" ontouchcancel=\"DROPDOWNMENUTOUCH.touchMenuCancel(event);\"");
        }

        sb.append("onkeydown=\"menuItemKeyDown(event,this,");
        sb.append(this.indentLevel + "); showSubMenu(event,'");
        sb.append("treechild");
        sb.append(this.shortId);
        sb.append("',");
        sb.append(this.indentLevel);
        sb.append(",'");
        sb.append(this.shortId);
        sb.append("submenu");
        sb.append("'");
        sb.append(");");
        sb.append(this.getNodeActivate() + ";" + "this.className='HoverMenuItem';return doMenuKeyDown(this,event,false,'");
        sb.append("treechild");
        sb.append(this.shortId)
        sb.append("');\"");

        sb.append("><tbody><tr><td></td>");
        sb.append("<td>");

        if (myAppType != null) {
            if (myAppType == "APP") {
                if (this.isOVRTask) {
                    sb.append("<span class=\"GenericOVRIcon\"><img src=\"");
                    sb.append(window["E1RES_share_spacer_gif"]);
                    sb.append("\" alt=\"\" width=16 height=16></span>");
                }
                else {
                    sb.append("<span class=\"GenericAppIcon\"><img src=\"");
                    sb.append(window["E1RES_share_spacer_gif"]);
                    sb.append("\" alt=\"\" width=16 height=16></span>");
                }
            }
            else if (myAppType == "UBE") {
                sb.append("<span class=\"GenericReportIcon\"><img src=\"");
                sb.append(window["E1RES_share_spacer_gif"]);
                sb.append("\" alt=\"\" width=16 height=16></span>");
            }
            else if (myAppType == "FOLDER" || userDefinedFolderID == "UDF") {
                sb.append("<span><img src=\"");
                sb.append(window["E1RES_share_spacer_gif"]);
                sb.append("\" alt=\"\" width=16 height=16></span>");
            }
            else {
                if (!this.hasChildren) {
                    sb.append("<span class=\"treebulletother\"><img src=\"");
                    sb.append(window["E1RES_share_spacer_gif"]);
                    sb.append("\" alt=\"\" width=16 height=16></span>");
                }
            }
        }
        sb.append("</td><td>&nbsp;</td>");
        if (jdeMenuParent.isRtlEnabled) {
            sb.append("<td align=\"right\"");
        }
        else {
            sb.append("<td align=\"left\"");
        }
        sb.append(" width=\"100%\"")
        return sb.toString();
    }

    jdeMItm.prototype.taskNodeManageFavorites = function (label) {
        var sb = new PSStringBuffer();
        sb.append("<table cellpadding=0 cellspacing=0 ");
        if (!this.isTouchEnabled)
            sb.append("onclick");
        else
            sb.append("ontouchend");

        sb.append("=\"javascript:manageFavorites()\" class=\"MenuNormal\" id=\"DROP_MANAGE_FAVORITES\" reorganize=\"false\" ");
        sb.append("onmouseover=\"menuItemHover(this,0);\" onmouseout=\"menuItemUnhover(this,0);\" onfocus=\"menuItemHover(this,0);\" onblur=\"menuItemUnhover(this,0);\" ");
        sb.append("onkeydown=\"javascript:manageFavMenuKeyDown(event,this);\" role=\"menuitem\" aria-labelledby=\"ManageFav\" tabindex=0><tbody>");
        sb.append("<tr>");
        sb.append("<td id=\"ManageFavTD\">");
        sb.append("<span><img id=\"ManageFavIcon\" class=\"ManageFavIcon\" src=\"");
        sb.append(window["E1RES_share_spacer_gif"]);
        sb.append("\" alt=\"\" width=16 height=16></span>");
        sb.append("</td>");
        if (jdeMenuParent.isRtlEnabled) {
            sb.append("<td id=\"ManageFavTD2\" align=\"right\"");
        }
        else {
            sb.append("<td id=\"ManageFavTD2\" align=\"left\"");
        }
        sb.append(" width=\"100%\">")
        sb.append("<span id=\"ManageFav\">")
        sb.append(label);
        sb.append("</span></td>");
        sb.append("</tr>");
        sb.append("</tbody></table>");
        return sb.toString();
    }

    jdeMItm.prototype.taskNodeAddToFavorites = function (label, nameSpace) {
        var showAddFormToFav = false;
        var e1AppFrame = top.document.getElementById('e1menuAppIframe');
        if (e1AppFrame != null && e1AppFrame.contentWindow.showAddFormToFav == true)//iframe doesn't exist, or the variable on the form is false
        {
            showAddFormToFav = true;
        }
        var sb = new PSStringBuffer();
        sb.append("<table cellpadding=0 cellspacing=0 ");
        if (showAddFormToFav) {
            sb.append(" style=\"display:table\"");
        }
        else {
            sb.append(" style=\"display:none\"");
        }
        if (!this.isTouchEnabled)
            sb.append("onclick");
        else
            sb.append("ontouchend");

        sb.append("=\"javascript:addFormToFavorites('").append(nameSpace).append("')\" id=\"ADD_TO_FAVORITES\" reorganize=\"false\" class=\"MenuNormal\" aria-labelledby=\"AddToFav\" ");
        sb.append("onmouseover=\"menuItemHover(this,0);\" onmouseout=\"menuItemUnhover(this,0);\" onfocus=\"menuItemHover(this,0);\" onblur=\"menuItemUnhover(this,0);\" ");
        sb.append("onkeydown=\"javascript:manageFavMenuKeyDown(event,this,'");
        sb.append(nameSpace);
        sb.append("');\" role=\"menuitem\" tabindex=0><tbody>");
        sb.append("<tr>");

        sb.append("<td id=\"AddToFavTD\">");
        sb.append("<span><img id=\"AddToFavIcon\" class=\"AddToFavIcon\" src=\"");
        sb.append(window["E1RES_share_spacer_gif"]);
        sb.append("\" alt=\"\" width=16 height=16></span>");
        sb.append("</td>");
        if (jdeMenuParent.isRtlEnabled) {
            sb.append("<td id=\"AddToFavTD2\" align=\"right\"");
        }
        else {
            sb.append("<td id=\"AddToFavTD2\" align=\"left\"");
        }
        sb.append(" width=\"100%\">")
        sb.append("<span id=\"AddToFav\">");
        sb.append(label);
        sb.append("</span></td>");
        sb.append("</tr>");
        sb.append("</tbody></table>");
        return sb.toString();
    }

    jdeMItm.prototype.taskNodeActions = function (label) {
        var sb = new PSStringBuffer();

        sb.append("<div id=\"actionshead\"");
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(" aria-hidden=\"false\" role=\"menu\"");
        }
        sb.append(">");
        sb.append("<table style=\"white-space: nowrap\" class=\"MenuNormal\" id=\"ActionsSubMenuTable\" onclick=\"taskNodeActionsClick(this,event);\"");
        sb.append(" onkeydown=\"taskNodeActionsClick(this,event);return doMenuKeyDown(this,event,false,'');\" onmouseover=\"actionsMenuHover(this);\" onmouseout=\"actionsMenuUnhover(this);\" align=\"left\" id=\"actionsMenuTable\" reorganize=\"false\" onfocus=\"this.className='HoverMenuItem';\" onblur=\"this.className='MenuNormal';\" tabindex=\"0\" title=\"");
        sb.append(label + "\""); //Actions Tooltip
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(" aria-haspopup=\"true\" aria-labelledby=\"actionsfld\" role=\"menuitem\"");
        }
        if (this.isTouchEnabled) {
            sb.append(" ontouchstart=\"DROPDOWNMENUTOUCH.touchMenuStart(event,DROPDOWNMENUTOUCH.TASK,'");
            sb.append(this.shortId);
            sb.append("pop',");
            sb.append(this.popupLength);
            sb.append(", 'FlyOut");
            sb.append(this.shortId);
            sb.append("');\"");

            sb.append(" ontouchmove=\"DROPDOWNMENUTOUCH.touchMenuMove(event);\"");
            sb.append(" ontouchend=\"DROPDOWNMENUTOUCH.touchMenuEnd(event);\"");
            sb.append(" ontouchcancel=\"DROPDOWNMENUTOUCH.touchMenuCancel(event);\"");
        }
        sb.append(">");
        sb.append("<tr>");
        if (jdeMenuParent.isRtlEnabled) {
            sb.append("<td align=\"right\"");
        }
        else {
            sb.append("<td align=\"left\"");
        }
        sb.append(" width=\"100%\">")
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append("<span id=\"actionsfld\">");
        }
        sb.append(label);
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append("</span>");
        }
        sb.append("</td>");
        sb.append("<td style=\"white-space: nowrap\" valign=\"middle\"><table cellpadding=0 cellspacing=0><tr><td style=\"white-space: nowrap;width:5px;\" align=\"right\">");
        sb.append("<img id=\"I");
        sb.append(this.shortId);
        sb.append("\" style=\"cursor:pointer;\" align=\"absmiddle\" border=0 SRC=\"");
        if (jdeMenuParent.isRtlEnabled) {
            sb.append(window["E1RES_share_images_open_submenu_ena_rtl_png"]);
        }
        else {
            sb.append(window["E1RES_share_images_open_submenu_ena_png"]);
        }
        sb.append("\">");
        sb.append("</td>");
        sb.append("</tr></table> </td></tr>");
        sb.append("</table>");
        return sb.toString();
    }

    jdeMItm.prototype.taskNodeEmptyChildren = function (label, indentLevel) {
        var sb = new PSStringBuffer();
        sb.append("<table  id=\"emptyChildNodeTable\" class=\"MenuNormal\" reorganize=\"false\"");
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(" onfocus=\"this.className='HoverMenuItem';\" onblur=\"this.className='MenuNormal';\" tabindex=\"0\" aria-labelledby=\"emptyfld\" role=\"menuitem\" onkeydown=\"");
            sb.append("menuItemKeyDown(event,this,");
            sb.append(indentLevel).append(");");
            sb.append(" return doMenuKeyDown(this,event)\"");
        }
        else {
            sb.append("onfocus=\"\"");
        }
        sb.append("><tbody>");
        sb.append("<tr>");
        sb.append("<td id=\"emptyChildNode\" class=\"ContextMenuAnchorItem\">");
        sb.append("-- ");
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append("<span id=\"emptyfld\">");
        }
        sb.append(label);
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append("</span>");
        }
        sb.append(" --");
        sb.append("</td>");
        sb.append("</tr>");
        sb.append("<tbody></table>");
        sb.append("<table height=\"3px\"><tbody>");
        sb.append("<tr>");
        sb.append("</tr>");
        sb.append("<tbody></table>");
        return sb.toString();
    }

    jdeMItm.prototype.nodeTail = function () {
        var sb = new PSStringBuffer();
        if (this.hasChildren) {
            sb.append("</tr></table>");
        }
        if (this.hasChildren && !this.flyoutSupported) {
            sb.append("</td></div>");
        } else {
            sb.append("</td><td>&nbsp;</td></tr></tbody></table></div>");
        }
        return sb.toString();
    }

    jdeMItm.prototype.taskNodeHead = function (flyOutSupported, labelType, toolTip, action, promptPO, appType, popUpLength, nodeId) {
        var sb = new PSStringBuffer();
        this.popupLength = popUpLength;
        this.flyoutSupported = flyOutSupported;
        sb.append(this.nodeHead(appType));
        sb.append(">");
        if (flyOutSupported) {
            sb.append("<table width=\"100%\" cellpadding=0 cellspacing=0 style=\"white-space: nowrap\" title=\"");
            sb.append(toolTip);
            sb.append("\"><tr><td style=\"cursor: pointer;\">");
            if (this.hasChildren) {
                sb.append("<table cellpadding=0 cellspacing=0 style=\"white-space: nowrap\"");
                sb.append("><tr><td");
            }
            else {
                sb.append("<table cellpadding=0 cellspacing=0><tr><td");
            }
            sb.append(">");
        }

        switch (labelType) {
            case 1: // Folder Node without tooltip
                sb.append("<span id=\"fld");
                sb.append(nodeId);
                sb.append("\">");
                sb.append(this.label);
                sb.append("</span>");
                break;
            case 2: // Folder Node with tooltip
                sb.append("<span id=\"fld");
                sb.append(nodeId);
                sb.append("\"");
                sb.append(" title=\"");
                sb.append(toolTip);
                sb.append("\">");
                sb.append(this.label);
                sb.append("</span>");
                break;
            case 3: // Run application without tooltip

                sb.append("<a id=\"fld");
                sb.append(nodeId);
                sb.append("\" ");
                if (this.hasWebkitTouchSupport) {
                    sb.append("-webkit-touch-callout:none;");
                }
                sb.append("\"");
                sb.append(this.getNavExtras());
                sb.append(" href=");
                sb.append(this.runNewAppHref(action, promptPO, appType, this.label, nodeId));
                sb.append(">");
                sb.append(this.label);
                sb.append("</a>");
                break;

            case 4: // Run application with tootip
                sb.append("<a id=\"fld");
                sb.append(nodeId);
                sb.append("\" ");

                if (this.hasWebkitTouchSupport) {
                    sb.append("-webkit-touch-callout:none;");
                }

                sb.append("\"");
                sb.append(this.getNavExtras());
                sb.append(" href=");
                sb.append(this.runNewAppHref(action, promptPO, appType, this.label, nodeId));
                sb.append(" title=\"");
                sb.append(toolTip);
                sb.append("\">");
                sb.append(this.label);
                if (jdeMenuParent.isAccessibilityEnabled) {
                    sb.append("<span id=\"flddesc");
                    sb.append(nodeId);
                    sb.append("\"");
                    sb.append(" class='accessibility'>");
                    // For a user with accessibility mode set to yes
                    // specify if the task is APP or REPORT 
                    sb.append(toolTip);
                    sb.append(". ");
                    if (appType == "APP") {
                        sb.append(jdeMenuParent.accessibilityText[ACCESS_E1_MENU_APP_TASK]);
                    }
                    else if (appType == "UBE") {
                        sb.append(jdeMenuParent.accessibilityText[ACCESS_E1_MENU_REPORT_TASK]);
                    }
                    sb.append("</span>");
                }
                sb.append("</a>");
                break;
            case 5: // Crystal Report task
                sb.append("<a id=\"fld");
                sb.append(nodeId);
                sb.append("\" ");
                sb.append(this.getNavExtras());
                sb.append(" href=\"javascript:teDoRunCrystal('");
                sb.append(this.id);
                sb.append("');\" title=\"");
                sb.append(toolTip);
                sb.append("\">");
                sb.append(this.label);
                sb.append("</a>");

                if (jdeMenuParent.isAccessibilityEnabled) {
                    sb.append("<span id=\"flddesc");
                    sb.append(nodeId);
                    sb.append("\"");
                    sb.append(" class='accessibility'>");
                    sb.append(toolTip);
                    sb.append(". ");
                    sb.append(jdeMenuParent.accessibilityText[ACCESS_E1_MENU_CRYS_REP_TASK]);
                    sb.append("</span>");
                }
                break;
            case 6: // URL task
                sb.append("<a id=\"fld");
                sb.append(nodeId);
                sb.append("\" ");
                sb.append(this.getNavExtras());
                sb.append(" href=\"javascript:RunUrl('");
                sb.append(this.objId);
                sb.append("');\" title=\"");
                sb.append(toolTip);
                sb.append("\">");
                sb.append(this.label);
                sb.append("</a>");

                if (jdeMenuParent.isAccessibilityEnabled) {
                    sb.append("<span id=\"flddesc");
                    sb.append(nodeId);
                    sb.append("\"");
                    sb.append(" class='accessibility'>");
                    sb.append(toolTip);
                    sb.append(". ");
                    sb.append(jdeMenuParent.accessibilityText[ACCESSIBILITY_E1_MENU_URL_TASK]);
                    sb.append(". ");
                    sb.append(jdeMenuParent.accessibilityText[ACCESSIBILITY_E1_MENU_URL_IS]);
                    sb.append(" ");
                    sb.append("</span>");
                }
                break;
            case 7: // One View Report task
                sb.append("<a id=\"fld");
                sb.append(nodeId);
                sb.append("\" ");
                sb.append(this.getNavExtras());
                sb.append(" href=\"javascript:teDoRunOvrTask('");
                sb.append(this.id);
                sb.append("');\" title=\"");
                sb.append(toolTip);
                sb.append("\">");
                sb.append(this.label);
                sb.append("</a>");

                if (jdeMenuParent.isAccessibilityEnabled) {
                    sb.append("<span id=\"flddesc");
                    sb.append(nodeId);
                    sb.append("\"");
                    sb.append(" class='accessibility'>");
                    sb.append(toolTip);
                    sb.append(". ");
                    sb.append(jdeMenuParent.accessibilityText[ACCESS_E1_MENU_OVR_TASK]);
                    sb.append("</span>");
                }
                break;
            case 8: // ADF Application task

                sb.append("<a id=\"fld");
                sb.append(nodeId);
                sb.append("\" ");
                sb.append(this.getNavExtras());
                sb.append(" href=\"javascript:teDoRunExternalAppTask('");
                sb.append(this.id);
                sb.append("','");
                sb.append(this.menuNodeInstanceId);
                sb.append("');\"");
                sb.append(" onclick='checkHrefKeyPress(this, event, null)'");
                sb.append(" title=\"");
                sb.append(toolTip);
                sb.append("\">");
                sb.append(this.label);
                sb.append("</a>");

                if (jdeMenuParent.isAccessibilityEnabled) {
                    sb.append("<span id=\"flddesc");
                    sb.append(nodeId);
                    sb.append("\"");
                    sb.append(" style=\"display: none;\">");
                    sb.append(toolTip);
                    sb.append(". ");
                    //sb.append(jdeMenuParent.accessibilityText[ACCESSIBILITY_E1_MENU_URL_IS]);    
                    sb.append("E1 Menu External App Task"); //TODO: See OVR fix in Staging to prevent English bleed through
                    sb.append("</span>");
                }
                break;
        }

        if (flyOutSupported) {
            sb.append("</td>");
            //rendering the submenu 
            if (this.hasChildren) {
                sb.append("<td style=\"white-space: nowrap\"><table id=\"");
                sb.append(this.shortId);
                sb.append("submenu");
                sb.append("\"");
                sb.append("width=\"100%\" class=\"hideMenu\" align=\"right\" cellpadding=0 cellspacing=0 style=\"white-space: nowrap\"><tbody><tr><td>");
                sb.append("<div id=\"treechild");
                sb.append(this.shortId);
                sb.append("\" style=\"color: #000000;\"></div>");
                sb.append("</td></tr></tbody></table></td>");
            }
            sb.append("<td>&nbsp;<div id=\"");
            sb.append(this.shortId);
            sb.append("pop\" style=\"display: none;\" role=\"presentation\"><table cellpadding=0 cellspacing=0><tr><td>");
        }
        return sb.toString();
    }


    jdeMItm.prototype.taskNodeTail = function () {
        var sb = new PSStringBuffer();
        if (this.flyoutSupported) {
            sb.append("</td></tr></table></td>");
            sb.append("</tr></table></td></tr></table></td>");

            //if the TaskNode has Children, render Right Arrow image in right
            sb.append("<td style=\"white-space: nowrap\" valign=middle><table cellpadding=0 cellspacing=0><tr><td style=\"white-space: nowrap;width:5px;\" align=\"right\">");
            if (this.hasChildren) {
                sb.append("<img id=\"I");
                sb.append(this.shortId);
                sb.append("\" style=\"cursor: pointer;\" alt=\"\" align=absmiddle border=0 SRC=\"");
                if (jdeMenuParent.isRtlEnabled) {
                    sb.append(window["E1RES_share_images_open_submenu_ena_rtl_png"]);
                }
                else {
                    sb.append(window["E1RES_share_images_open_submenu_ena_png"]);
                }
                sb.append("\" ");
                /* This wasn't working, so commented out. */
                /* It looks like it was supposed to open the sub menu on mouseover of the image, */
                /* but it needs more - or different - code for that to work. */
                //sb.append("onmouseover=\"");
                //sb.append(this.getNodeActivate() + "\">");
            }
            sb.append("</td>");
        }

        if (!this.hasChildren) {
            sb.append("</table>");
        }

        return sb.toString();
    }

    jdeMItm.prototype.runNewAppHref = function (action, promptPO, appType, taskName, nodeId) {
        var sb = new PSStringBuffer();
        taskName = taskName.replace(/\\/g, "\\\\");
        sb.append("javascript:RunNewApp(\"");
        sb.append(action);
        sb.append("\",\"");
        sb.append(promptPO);
        sb.append("\",\"");
        sb.append(this.objId);
        sb.append("\",\"");
        sb.append(appType);
        sb.append("\",\"");
        sb.append(this.formId);
        sb.append("\",\"");
        sb.append(this.versionId);
        sb.append("\",\"");
        sb.append(this.appMode);
        sb.append("\",\"");
        sb.append(this.menuSysCode);
        sb.append("\",\"");
        if (document.documentMode !== undefined) // IE
        {
            sb.append(escape(taskName));
        } else {
            sb.append(window.encodeURIComponent(taskName));
        }
        sb.append("\",\"");
        sb.append(this.formDSTmpl);
        sb.append("\",\"");
        sb.append(this.formDSData);
        sb.append("\",\"\",\"\",\"");
        sb.append(nodeId);
        sb.append("\",\"\",\"");
        sb.append(this.RID);
        sb.append("\",\"");
        sb.append(this.menuNodeInstanceId);
        sb.append("\") onclick='checkHrefKeyPress(this, event, null)'");

        return sb.toString();
    }

    jdeMItm.prototype.getNavExtras = function () {
        var sb = new PSStringBuffer();
        sb.append("arrownav=\"yes\" onkeydown=\"return doMenuKeyDown(this,event)\"");
        if (this.hasChildren) {
            sb.append(" expandScript=\"");
            sb.append(this.getLoadScript("expand"));
            sb.append("\"");
        }

        return sb.toString();
    }

    jdeMItm.prototype.getLoadScript = function (toggleMode) {
        var sb = new PSStringBuffer();
        sb.append("doTreeIframeWebGui('");
        sb.append(this.id);
        sb.append("',");
        sb.append(this.indentLevel);
        sb.append(",'.','");
        sb.append(toggleMode);
        sb.append("','')");

        return sb.toString();
    }

    jdeMItm.prototype.getNodeActivate = function () {
        var sb = new PSStringBuffer();
        var parent = (this.hasChildren) ? this.id : this.parentNodeId;
        if (parent == this.id) {
            parent = ".";
        }
        parent = "";
        var myIndentLevel = this.indentLevel + 1;
        sb.append("doActivateNodeWebGui('");
        sb.append(this.shortId);
        sb.append("','");
        sb.append(parent);
        sb.append("','");
        sb.append(myIndentLevel);
        sb.append("');");

        if (this.hasChildren) {
            sb.append(this.getLoadScript("expand"));
        }

        return sb.toString();
    }

    jdeMItm.prototype.flyoutApp = function (action, promptPO, appType, taskName, label, nodeId) {
        var sb = new PSStringBuffer();
        sb.append(this.addTableHeader("App"));
        sb.append("RunNewApp(\"");
        sb.append(action);
        sb.append("\",\"");
        sb.append(promptPO);
        sb.append("\",\"");
        sb.append(this.objId);
        sb.append("\",\"");
        sb.append(appType);
        sb.append("\",\"");
        sb.append(this.formId);
        sb.append("\",\"");
        sb.append(this.versionId);
        sb.append("\",\"");
        sb.append(this.appMode);
        sb.append("\",\"");
        sb.append(this.menuSysCode);
        sb.append("\",\"");
        if (document.documentMode !== undefined) // IE
        {
            sb.append(escape(taskName));
        } else {
            sb.append(window.encodeURIComponent(taskName));
        }
        sb.append("\",\"");
        sb.append(this.formDSTmpl);
        sb.append("\",\"");
        sb.append(this.formDSData);
        sb.append("\",\"\",\"\",\"");
        sb.append(nodeId);
        sb.append("\",\"\",\"");
        sb.append(this.RID);
        sb.append("\",\"");
        sb.append(this.menuNodeInstanceId);
        sb.append("\")");
        sb.append(">");
        sb.append(jdeMenuParent.flyoutLabels[label]);
        sb.append(this.addTableEnd());

        return sb.toString();
    }

    jdeMItm.prototype.flyoutDoc = function (label, nodeId) {
        var sb = new PSStringBuffer();
        sb.append(this.addTableHeader("Doc"));
        sb.append("teDoDocWindow(\"");
        sb.append(this.tmTaskNM);
        sb.append("\",\"");
        sb.append(this.taskView);
        sb.append("\",\"");
        sb.append(this.taskId);
        sb.append("\",\"");
        sb.append(this.parentTaskId);
        sb.append("\",\"");
        sb.append(this.seqNum);
        sb.append("\",\"");
        sb.append(this.role);
        sb.append("\",\"\")");
        sb.append(">");
        sb.append(jdeMenuParent.flyoutLabels[label]);
        sb.append(this.addTableEnd());

        return sb.toString();
    }

    jdeMItm.prototype.flyoutProps = function (label, nodeId) {
        var sb = new PSStringBuffer();
        sb.append(this.addTableHeader("prop"));
        sb.append("teProps(\"");
        sb.append(this.taskView);
        sb.append("\",\"");
        sb.append(this.taskId);
        sb.append("\",\"");
        sb.append(this.parentTaskId);
        sb.append("\",\"");
        sb.append(this.seqNum);
        sb.append("\",\"");
        sb.append(this.role);
        sb.append("\",\"\")");
        sb.append(">");
        sb.append(jdeMenuParent.flyoutLabels[label]);
        sb.append(this.addTableEnd());

        return sb.toString();
    }

    jdeMItm.prototype.flyoutFav = function (favMode, label, nodeId) {
        var sb = new PSStringBuffer();
        sb.append(this.addTableHeader("Fav"));
        sb.append("doFavoritesAction(\"");
        sb.append(this.taskId);
        sb.append("\",\"");
        sb.append(this.parentTaskId);
        sb.append("\",\"");
        sb.append(this.taskView);
        sb.append("\",\"");
        sb.append(this.seqNum);
        sb.append("\",\"");
        sb.append(favMode);
        sb.append("\",\"");
        if (document.documentMode !== undefined) // IE
        {
            sb.append(escape(this.label));
        } else {
            sb.append(window.encodeURIComponent(this.label));
        }
        sb.append("\")");
        sb.append(">");
        sb.append(jdeMenuParent.flyoutLabels[label]);
        sb.append(this.addTableEnd());

        return sb.toString();
    }
    jdeMItm.prototype.createAbout = function (url, hoverText, label) {
        var sb = this.nodeHead("APP");
        sb += "\"><a arrownav=\"yes\" onkeydown=\"return doMenuKeyDown(this,event)\" href=\"javascript:;\" onclick=\"window.open('";
        sb += url;
        sb += "','teAboutWindow','height=400,width=600,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1');";
        sb += "\" id=\"fldnode";
        sb += this.shortId;
        sb += "\" title=\"";
        sb += hoverText;
        // For a user with accessibility mode set to yes
        // specify the context info for the action task
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb += ". ";
            sb += jdeMenuParent.accessibilityText[ACCESSIBILITY_E1_MENU_ACTION];
        }
        sb += "\">";
        sb += label;
        sb += "</a>";
        sb += this.nodeTail();
        return sb;
    }

    jdeMItm.prototype.createHelp = function (url, hoverText) {
        var sb = new PSStringBuffer();
        sb.append(this.nodeHead("APP"));
        sb.append("\"><a arrownav=\"yes\" onkeydown=\"return doMenuKeyDown(this,event)\" href=\"javascript:;\"");
        sb.append(" id=\"fldnode");
        sb.append(this.shortId);
        sb.append("\"");
        sb.append(" onclick=\"window.open('");
        sb.append(url);
        sb.append("','OneWorldHelp','toolbar,location,status,menubar,scrollbars,resizable,width=740,height=540');");
        sb.append("\" title=\"");
        sb.append(hoverText);
        sb.append("\">");
        sb.append(hoverText);
        sb.append("</a>");
        sb.append(this.nodeTail());

        return sb.toString();
    }

    jdeMItm.prototype.createRIHome = function (url, hoverText) {
        var sb = new PSStringBuffer();
        sb.append(this.nodeHead("APP"));
        sb.append("\"><a arrownav=\"yes\" onkeydown=\"return doMenuKeyDown(this,event)\" href=\"javascript:;\" onclick=\"window.open('");
        sb.append(url);
        sb.append("','BeehiveHome','toolbar,location,status,menubar,scrollbars,resizable');");
        sb.append("\" title=\"");
        sb.append(hoverText);
        // For a user with accessibility mode set to yes
        // specify the context info for the action task
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(". ");
            sb.append(jdeMenuParent.accessibilityText[ACCESSIBILITY_E1_MENU_ACTION]);
        }
        sb.append("\">");
        sb.append(hoverText);
        sb.append("</a>");
        sb.append(this.nodeTail());

        return sb.toString();
    }

    jdeMItm.prototype.createLink = function (action, promptPO, appType, taskName, hoverText) {
        var sb = new PSStringBuffer();
        sb.append(this.nodeHead(appType));
        sb.append("\"><a arrownav=\"yes\" onkeydown=\"return doMenuKeyDown(this,event)\"");
        sb.append(" id=\"fldnode");
        sb.append(this.shortId);
        sb.append("\"");
        sb.append(" href=");
        sb.append(this.runNewAppHref(action, promptPO, appType, taskName, "0"));
        sb.append(" title=\"");
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(taskName);
            sb.append(". ");
        }
        sb.append(hoverText);
        sb.append("\"");
        sb.append("alt=\"");
        sb.append(appType);
        sb.append("\">");
        sb.append(taskName);
        sb.append("</a>");
        sb.append(this.nodeTail());

        return sb.toString();

    }

    jdeMItm.prototype.addTableHeader = function (mName) {
        var sb = new PSStringBuffer();
        var menuTabId = "ContextMenuTab" + mName + this.shortId + ":" + this.flyoutIndex;
        sb.append("<TABLE width=\"160px;\" id=\'").append(menuTabId).append("' tabIndex=\"0\" class=MenuNormal onmouseover=\"this.className='HoverMenuItem';\"");
        sb.append("onmouseout=\"this.className='MenuNormal';\" onfocus=\"this.className='HoverMenuItem';\" onblur=\"this.className='MenuNormal';\" cellspacing=0 cellpadding=0");
        if (jdeMenuParent.isAccessibilityEnabled) {
            sb.append(" role=\"menuitem\" aria-labelledby=\"fo");
            sb.append(this.shortId);
            sb.append(":");
            sb.append(this.flyoutIndex++);
            sb.append("\"");
        }
        sb.append(" onkeydown=\"return doMenuKeyDown(this,event,true);\"");
        sb.append(">");
        sb.append("<TR width=\"160px;\">");
        sb.append("<TD align=\"left\">");
        sb.append("<a class=ContextMenuAnchorItem");
        sb.append(" role=\"presentation\" tabindex=-1 id=\"fo");
        sb.append(this.shortId);
        sb.append(":");
        sb.append(this.flyoutCount++);
        sb.append("\" href=javascript:");

        return sb.toString();
    }

    jdeMItm.prototype.addTableEnd = function () {
        return "</a></TD></TR></TABLE>";
    }
    function PSStringBuffer() {
        this.maxStreamLength = (document.documentMode !== undefined ? 100 : 10000);
        this.data = new Array(100);
        this.iStr = 0;
        this.append = function (obj) {
            this.data[this.iStr++] = obj;
            if (this.data.length > this.maxStreamLength) {
                this.data = [this.data.join("")];
                this.data.length = 100;
                this.iStr = 1;
            }
            return this;
        };
        this.toString = function () {
            return this.data.join("");
        };
    }

    PSStringBuffer._joinFunc = Array.prototype.join;
    PSStringBuffer.concat = function () {
        arguments.join = this._joinFunc;
        return arguments.join("")
    }
}