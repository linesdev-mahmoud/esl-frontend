/*
* This file is intended to hold any utility functions of general usefulness that can be reused
* in other javascript code.  Generally speaking, this file is intended to hold functions that
* do not specifically pertain to EnterpriseOne.  A good rule of thumb is:
* 
*  - Functions that are specific to a particular project or area of code should go in the 
*    file for that project
*  - Functions that are useful across several projects/modules but are very much tied into
*    the E1 framework should go someplace like webgui.js or e1.js.
*  - Functions of a general usefulness in the language of javascript but that are not tied
*    in (at least, not tightly tied in) with the E1 framework should go here (utils.js).
*/

var UTIL =
{
    clamp: function (value, min, max) {
        return Math.max(min, Math.min(value, max));
    },

    createElement: function (type, cssClass, id) {
        var newHtmlElement = document.createElement(type);
        if (cssClass) {
            newHtmlElement.className = cssClass;
        }
        if (id) {
            newHtmlElement.setAttribute('id', newHtmlElement.id = id);
        }

        return newHtmlElement;
    },

    forceRedraw: function (obj) {
        /* IE does not update styles immediately when changed.  It delays the redraw hoping to 
        group multiple changes together.  The following workaround forces IE to update the
        rendering by reading a property (in this case, offsetHeight) that depends on the style
        changes, thereby forcing it to rerender.  
           
        The check that the browser is IE is not strictly required.  Doing this on other browsers
        will not cause any problems; it is simply unnecessary.  
           
        Sadly, due to needing to restructure in a convoluted way in order to work-around IE bugs,
        it has now become necessary to do this on chrome as well.  
        */

        var savedDisplayString = obj.style.display || '';

        obj.style.display = 'none';
        var hiddenHeight = obj.offsetHeight

        obj.style.display = savedDisplayString;
        var finalHeight = obj.offsetHeight;

        /* Currently, on IE it is sufficient to read the property.  It is possible that
        * future dead-code elimination will require that we actually use the property. 
        * and/or read it in between changes to obj.style.display.  Chrome seems to need
        * soemthing to happen between setting display to none and restoring its original
        * value. */
    },

    forceRedrawIEOnly: function (obj) {
        (navigator && navigator.appName == "Microsoft Internet Explorer") ? UTIL.forceRedraw(obj) : { /* No-op */
    }
},

getEventTarget: function (event) {
    // Determine the element that was actually clicked
    event = event || window.event;
    if (event) {
        var target = event.target || event.srcElement;
        if (target) {
            return target;
        }
    }
    return null;
},

isMouseOutsideWindow: function (event) {
    var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);
    return (eClientX < 0) || (eClientX > CARO.pageWidth()) ||
                (eClientY < 0) || (eClientY > CARO.pageHeight());
},

isWebkit: function () {
    return (navigator.userAgent.toLowerCase().indexOf("webkit") > -1);
},

pageHeight: function () {
    return window.innerHeight || document.documentElement.clientHeight;
},

pageWidth: function () {
    if (window.innerWidth !== undefined)
        return window.innerWidth; // firefox, chrome, safari, non-IE8
    return document.documentElement.clientWidth; // IE8
},

htmlDecoder: function (encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    if (div.firstChild != null)
        return div.firstChild.nodeValue;
    else
        return "";
},

sign: function (n) {
    if (n == 0) return 0;
    if (n > 0) return 1;
    return -1;
},

stopEventBubbling: function (event) {
    event = event || window.event;
    if (event.stopPropagation) {
        event.stopPropagation();
        event.cancelBubble = true;
        if (event.bubbles)
            event.bubbles = false;
    }
    else {
        event.cancelBubble = true;
    }
},

addEvent: function (elem, eventType, eventFunction) {
    if (elem.addEventListener) //can use the "addEventListener" syntax
    {
        //Firefox
        this.addEvent = this.addEventWithListener;
    }
    else {
        //IE
        this.addEvent = this.addEventWithAttach;
    }

    this.addEvent(elem, eventType, eventFunction);
},

addEventWithAttach: function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.attachEvent('on' + eventType, eventFunction);
},

addEventWithListener: function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.addEventListener(eventType, eventFunction, false); //false = capture on bubbling
}
};

/* TODO: Currently, this is (essentially) a duplicate of the RIUTIL.sendXMLReq logic.  It is included here
*   so that the AJAX framework will be available without having to import a 5000+ line JS library (RIView)
*   just for that purpose.  Ultimately, we should replace the RIUTIL/RIView version with a wrapper around 
*   this one so there is not dual-maintenance, but that is too risky to do during 9.1.3 regression.
*/
var E1AJAX =
{
    sendXMLReq: function (method, containerId, action, handler, doc) {
        // code for IE 7, FireFox, Safari, etc.
        if (window.XMLHttpRequest) {
            var xmlReqObject = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            var xmlReqObject = new ActiveXObject("MSXML2.XMLHTTP");
        }

        if (xmlReqObject != null) {
            xmlReqObject.open(method, action, true);
            var handlerFunction = E1AJAX.getReadyStateHandler(xmlReqObject, handler, containerId);
            xmlReqObject.onreadystatechange = handlerFunction;
            xmlReqObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            //Webkit browsers through Javascript error while setting connection header
            if (!UTIL.isWebkit()) {
                xmlReqObject.setRequestHeader("Connection", "close");
            }
            try {
                xmlReqObject.send(doc);
            }
            catch (e) { } // SSO logout from web center may cause interaction of E1 failure
        }

    },

    getReadyStateHandler: function (req, responseHandler, containerId) {
        // Return an anonymous function that listens to the 
        // XMLHttpRequest instance
        return function () {
            // If the request's status is "complete"
            if (req.readyState == 4) {
                // Check that a successful server response was received
                switch (req.status) {
                    case 200:
                        // Pass the XML payload of the response to the 
                        // handler function
                        if (responseHandler)
                            responseHandler(containerId, req.responseText);
                        break;

                    case 122: // SSO logout already from web center but E1 is still opened
                        E1AJAX.timeout();
                        break;

                    case 404: // possible XSS attack 
                        if (window.RIUTIL && RIUTIL.CONSTANTS && RIUTIL.CONSTANTS.RI_EXIT_FOR_XSS) {
                            E1AJAX.timeout(RIUTIL.CONSTANTS.RI_EXIT_FOR_XSS);
                        }
                        else {
                            E1AJAX.timeout();
                        }
                        break;

                    // Safari intermittently encounters status 0 states. 
                    // Bypass the alert error in the default below with a case for this 
                    // state with no specific logic for it. 
                    case 0:
                        break;

                    default:
                        // An HTTP problem has occurred
                        alert("HTTP error: " + req.status);
                }
            }
        }
    },

    timeout: function (message) {
        if (message)
            alert(message);
        top.location.replace(top.location.href);
    },

    sendSyncXMLReq: function (method, action) {
        // code for IE 7, FireFox, Safari, etc.
        if (window.XMLHttpRequest) {
            var xmlReqObject = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            var xmlReqObject = new ActiveXObject("MSXML2.XMLHTTP");
        }

        if (xmlReqObject != null) {
            xmlReqObject.open(method, action, false);
            xmlReqObject.send(null);
            if (xmlReqObject.status === 200) {
                try {
                    return xmlReqObject.responseText;
                }
                catch (e) { }
            }
        }
    }
};

/* Modifications to core API's */


/* If the native String type does not have a suitable trim function defined (IE8), then define one in the prototype for the class */
if (typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
    };
}