﻿///////////////////////////////////////
// Singleton Grid Control Message Handler
///////////////////////////////////////
var gesturedebug = false;
var GCMH;
if (GCMH == null) {
    GCMH = new function () {
        this.curGridIndex = 0;
        this.grids = new Array();
        this.gridIds = new Array();
    }
}

GCMH.registerGrid = function (gridId, gridObj) {
    this.grids[gridId] = gridObj;
    this.gridIds[this.gridIds.length] = gridId;
}

GCMH.findGrid = function (gridId) {
    var gridObj = null;

    if (gridId)  //it's possible that the attribute was not passed when calling here
    {
        gridObj = this.grids[gridId];
        if (!gridObj) {
            //this should never never happen, but just to be ABSOLUTELY safe
            gridObj = window["jdeGrid" + gridId];
            GCMH.registerGrid(gridObj.gridId, gridObj);
        }
    }
    return gridObj;
}

GCMH.getJdeDtaFactory = function () {
    if (this.dtaFactory == undefined || this.dtaFactory == null) {
        //minimize number of unnecessary resolutions of the global
        //variable JDEDTAFactory
        this.dtaFactory = (JDEDTAFactory == undefined) ? null : JDEDTAFactory;
    }
    return this.dtaFactory;
}

GCMH.getDtaInstance = function (namespace) {
    return this.getJdeDtaFactory().getInstance(namespace);
}

GCMH.getGridCellRenderFactory = function (gridObj) {
    //control access to singleton object
    if (!this.cellRenderFactory) {
        this.cellRenderFactory = new JSGridCellRenderFactory();
    }

    //just in case multiple portals with multiple grids are defined on the same
    //page, just make sure that getting a reference to the cell render factory
    //has all prototype information correctly defined for the 2nd and subsequent
    //references

    this.cellRenderFactory.initializePrototypes(gridObj)

    return this.cellRenderFactory;
}

GCMH.getGridRowRenderer = function (gridObj) {
    //control access to singleton object
    if (!this.rowRenderer) {
        this.rowRenderer = new JSGridRowRender(gridObj);
    }

    return this.rowRenderer;
}
///////////////////////////////////////////////////////////
//  Grid Message Handler - EVENTS
//  The following methods are the event "hooks" to which 
//  grid-interaction events will initially resolve.
//
//  IMPORTANT NOTE:
//  (1) In all cases, the argument signature is the event,
//  itself (which will always be null on IE, requiring the
//  interrogation of "window.event" instead)
//  (2) In all cases, and at each method entry, the "this" 
//  keyword will *always* refer to the DOM element that 
//  triggered the event and NOT to the singleton GCMH 
//  object instance.
///////////////////////////////////////////////////////////
GCMH.onPCExpandNodeEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onExpandNode(infoObj.rowIndex);

        // we do not want the event bubbling up to trigger edit capbable cell click and change edit row.
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

GCMH.onPCCollapseNodeEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onCollapseNode(infoObj.rowIndex);

        // we do not want the event bubbling up to trigger edit capbable cell click and change edit row.
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

GCMH.onGridSelKeyDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            if (gridObj.onRowSelectorKeyDown(keyInfo, infoObj.rowIndex)) {
                gridObj.renderFreezeMode = false;
                return gridObj.msgHandler.stopEventBubbling(e);
            }
            gridObj.renderFreezeMode = false;
        }
        else if (gridObj.onRowSelectorKeyDown(keyInfo, infoObj.rowIndex)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onPCNodeClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj != null) {
        gridObj.onNodeClick(infoObj.rowIndex, e);
    }
    // This is to avoid posting to the default link url 'javascript:'.
    return false;
}

GCMH.onPCNodeBmpClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj != null) {
        gridObj.onNodeBmpClick(infoObj.rowIndex, e);
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

GCMH.onQBEFocusEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj != null) {
        var isTouchEnabled = gridObj.isTouchEnabled;
        if (isTouchEnabled && self && self.parent && !gridObj.focusManager.callBackFocus) {
            if (self.parent.CARO)
                self.parent.CARO.disableTouchEvents();
            var e1PaneForm = document.getElementById("E1PaneForm");
            if (e1PaneForm && FORMTOUCH)
                FORMTOUCH.disableTouchEvents(e1PaneForm);
            var appFrame = self.parent.document.getElementById("e1AppFrameContainer");
            if (appFrame && self.parent.FRAMETOUCH)
                self.parent.FRAMETOUCH.disableTouchEvents(appFrame);
        }
        var colIndex = infoObj.colIndex;
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            if (gridObj.onQBEFocus(this, colIndex)) {
                gridObj.renderFreezeMode = false;
                return gridObj.msgHandler.stopEventBubbling(e);
            }
            gridObj.renderFreezeMode = false;
        }
        else if (gridObj.onQBEFocus(this, colIndex)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onQBEChangeEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var colIndex = infoObj.colIndex;
        gridObj.onQBEChange(this, colIndex, this.value);
    }
}

GCMH.onQBEKeyDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            if (gridObj.onQBEKeyDown(keyInfo, infoObj.colIndex, this, true)) {
                return gridObj.msgHandler.stopEventBubbling(e);
            }
        }
        else if (gridObj.onQBEKeyDown(keyInfo, infoObj.colIndex, this, false)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onQBEKeyUpEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);

        if (gridObj.onQBEKeyUp(keyInfo, infoObj.colIndex, this)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onVAMouseOverEvent = function (e) {
    if (this != null && this.tagName == 'IMG') {
        var imgSrc = this.getAttribute('va_mo');
        if (imgSrc != null && imgSrc != this.src) {
            this.src = imgSrc;
        }
    }
}

GCMH.onVAMouseOutEvent = function (e) {
    if (this != null && this.tagName == 'IMG') {
        var imgSrc = this.getAttribute('va_img');
        if (imgSrc != null && imgSrc != this.src) {
            this.src = imgSrc;
        }
    }
}

GCMH.onVAMouseDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            gridObj.onCellVAFromImage(infoObj.rowIndex, infoObj.colIndex, this);
            gridObj.renderFreezeMode = false;
        }
        else {
            gridObj.onCellVAFromImage(infoObj.rowIndex, infoObj.colIndex, this);
        }
    }
}

GCMH.onDropdownMouseDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            gridObj.onDropdownImage(infoObj.rowIndex, infoObj.colIndex, this);
            gridObj.renderFreezeMode = false;
        }
        else {
            gridObj.onDropdownImage(infoObj.rowIndex, infoObj.colIndex, this);
        }
    }
}


GCMH.onIconCellMouseoverEvent = function (e) {
    var iconMouseover = this.getAttribute('iconMouseover');
    if (iconMouseover) {
        this.src = JSIconCell.prototype.locateIconImage(iconMouseover);

    }
    var gridObj = GUTIL.retrieveGridObject(this);

    // We only need to use our own tooltip for Safari and Firefox. IE is using
    // the ALT value to show the default the tooltip implemented by the browser. 
    if (gridObj && !gridObj.isIE) {
        gridObj.onTooltipMouseoverEvent(this.getAttribute('tooltip'), this, e);
    }
}

GCMH.onIconCellMouseoutEvent = function (e) {
    var iconMouseout = this.getAttribute('iconMouseout');
    if (iconMouseout) {
        this.src = JSIconCell.prototype.locateIconImage(iconMouseout);

    }
    var gridObj = GUTIL.retrieveGridObject(this);
    if (gridObj && !gridObj.isIE) {
        gridObj.onTooltipMouseoutEvent(this.getAttribute('tooltip'), this, e);
    }
}

GCMH.onSelectCellMouseoverEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var dta = gridObj.getDtaInstance();
    if (gridObj) {
        this.title = gridObj.constants[gridObj.ROW_STR] + ":" + gridObj.getVisibleRowIndexAllSections(infoObj.rowIndex);
    }
    if (gridObj.isPopupAssociated) {
        if (gridObj.isDefaultPopup) {

            // if the ShowPopup system function parameter param is default, 
            //get all the visible columns in the currenly hovered row and format them in 
            //2X2 header value format and create tools popup
            clearTimeout(dta.displayPopup);
            gridRowMouseStop = function () {
                var gridDataDiv = document.createElement('DIV');
                gridDataDiv.id = 'jdeGriddiv' + gridObj.gridId;
                gridDataDiv.className = 'JSGridBorder';

                var gridDataTable = gridObj.agent.newSimpleTable();
                gridDataTable.id = 'jdeGridData' + gridObj.gridId;
                gridDataTable.border = 0;
                gridDataTable.width = "100%";

                gridDataDiv.appendChild(gridDataTable);
                var maxColWidth = 0;
                var visibleColCount = 0;
                var colWidth = 0;
                var columns = gridObj.columns;
                var isLastRowIncomp = false;
                for (var cols = gridObj.columns, i = 0, totCols = cols.length, col; col = cols[i]; i++) {
                    if ((!col.isHidden()) && (col.cellType != JSGridCellRenderFactory.prototype.GCR_SELECT)
                && (col.cellType != JSGridCellRenderFactory.prototype.GCR_ATTACH)
                && (col.cellType != JSGridCellRenderFactory.prototype.GCR_TREENODE)
                ) {
                        var cellElem = gridObj.getDataCell(infoObj.rowIndex, col.colIndex);
                        if (visibleColCount % 2 == 0) {
                            var tr = document.createElement("TR");
                            var tdHeader = document.createElement("TD");
                            tdHeader.className = "JSGridHeaderCell";
                            gridDataTable.appendChild(tr);
                            tdHeader.innerHTML = col.label;
                            tr.appendChild(tdHeader);
                            var dataTD = document.createElement("TD");
                            dataTD.setAttribute("align", "left");
                            dataTD.className = "JSGridCell textModifier";
                            if (cellElem) {
                                var alignDiv = cellElem.getElementsByTagName("DIV");
                                var cloneData = alignDiv[0].cloneNode(true);
                                cloneData.style.textAlign = "left";
                                // if the grid is editable get the value from the control and render in readonly mode.
                                if (!gridObj.isEditable) {
                                    dataTD.appendChild(cloneData);
                                }
                                else {
                                    if (!cellElem.isDisabled && (cellElem.getElementsByTagName("INPUT")[0])) {
                                        cloneData.innerHTML = cellElem.getElementsByTagName("INPUT")[0].value;
                                        dataTD.appendChild(cloneData);
                                    }
                                    else {
                                        dataTD.appendChild(cloneData);
                                    }
                                }

                                colWidth = cellElem.offsetWidth;
                                if (colWidth > maxColWidth) {
                                    maxColWidth = colWidth;
                                }
                            }
                            else {
                                var cellData;
                                if (col.colIndex) {
                                    cellData = GCMH.getCellDataFromGridObj(col, infoObj, cols, i);
                                }
                                dataTD.innerHTML = cellData ? cellData : "&nbsp;";
                            }
                            tr.appendChild(dataTD);
                            isLastRowIncomp = true;
                        }
                        else {
                            var tdHeader = document.createElement("TD");
                            tdHeader.innerHTML = col.label;
                            tdHeader.className = "JSGridHeaderCell";
                            tr.appendChild(tdHeader);
                            var dataTD = document.createElement("TD");
                            dataTD.setAttribute("align", "left");
                            dataTD.className = "JSGridCell textModifier";
                            if (cellElem) {
                                var alignDiv = cellElem.getElementsByTagName("DIV");
                                var cloneData = alignDiv[0].cloneNode(true);
                                cloneData.style.textAlign = "left";
                                if (!gridObj.isEditable) {
                                    dataTD.appendChild(cloneData);
                                }
                                else {
                                    if (!cellElem.isDisabled && (cellElem.getElementsByTagName("INPUT")[0])) {
                                        cloneData.innerHTML = cellElem.getElementsByTagName("INPUT")[0].value;
                                        dataTD.appendChild(cloneData);
                                    }
                                    else {
                                        dataTD.appendChild(cloneData);
                                    }
                                }

                                colWidth = cellElem.offsetWidth;
                                if (colWidth > maxColWidth) {
                                    maxColWidth = colWidth;
                                }
                            }
                            else {
                                var cellData;
                                if (col.colIndex) {
                                    cellData = GCMH.getCellDataFromGridObj(col, infoObj, cols, i);
                                }
                                dataTD.innerHTML = cellData ? cellData : "&nbsp;";
                            }
                            tr.appendChild(dataTD);
                            isLastRowIncomp = false;
                        }
                        visibleColCount++;
                    }
                }
                // if we have ODD number of visible cols, create dummy header and value for the better visibility
                if (isLastRowIncomp) {
                    var rows = gridDataTable.getElementsByTagName('tr');
                    var lastrow = rows[rows.length - 1];
                    var tds = lastrow.getElementsByTagName('td');
                    lasttd = tds[tds.length - 1];
                    var td = document.createElement("TD");
                    td.innerHTML = "&nbsp;";
                    td.className = "JSGridHeaderCell";
                    lastrow.appendChild(td);
                    var dataTD = document.createElement("TD");
                    dataTD.setAttribute("align", "right");
                    dataTD.className = "JSGridCell textModifier";
                    dataTD.innerHTML = "&nbsp;";
                    lastrow.appendChild(dataTD);
                }
                var rowSel = gridObj.getRowSelector(infoObj.rowIndex);
                var clientHeight = document.body.clientHeight;
                var thisRow = gridObj.getDataRowElem(infoObj.rowIndex);
                var popupHeight = (thisRow.offsetHeight * visibleColCount / 2);
                var popupWinHeight = 0;
                var popupDivHeight = 0;
                if (popupHeight > clientHeight) {
                    popupDivHeight = clientHeight - thisRow.offsetHeight;
                    popupWinHeight = clientHeight;
                }
                else {
                    popupDivHeight = popupHeight + thisRow.offsetHeight;
                    if (!gridObj.isEditable) {
                        popupWinHeight = popupHeight + thisRow.offsetHeight + 47;
                    }
                    else {
                        popupWinHeight = popupHeight + thisRow.offsetHeight;
                    }
                }
                var popupWidth = (maxColWidth * 4);
                gridDataDiv.style.height = popupDivHeight + "px";
                gridDataDiv.style.width = popupWidth + "px";
                gridDataDiv.style.overflow = "auto";
                var gridDataDivWin = document.createElement('DIV');
                gridDataDivWin.id = 'jdeGriddivWin' + gridObj.gridId;
                gridDataDivWin.appendChild(gridDataDiv);

                createPopup("popupWinRow" + infoObj.rowIndex,
                                SHOW_POPUP,
                                true,
                                true,
                                true,
                                true,
                                '',
                                null,
                                null,
                                null,
                                null,
                                null,
                                getAbsoluteLeftPos(rowSel),
                                getAbsoluteTopPos(rowSel),
                                popupWidth + 5,
                                popupWinHeight,
                                rowSel,
                                gridDataDivWin.innerHTML);
            };
            dta.displayPopup = setTimeout(gridRowMouseStop, 1000);
        }
        else {
            var ctrlId = "popupWinRow" + infoObj.rowIndex;
            if (window[ctrlId]) {
                var rowSel = gridObj.getRowSelector(infoObj.rowIndex);
                clearTimeout(dta.displayPopup);
                gridRowMouseStop = function () {
                    window[ctrlId].show(rowSel);
                };
                dta.displayPopup = setTimeout(gridRowMouseStop, 1000);

            }
            else {
                clearTimeout(dta.displayPopup);
                if (window[gridObj.prevRow]) {
                    clearInterval(window.intervalIdForOpenedPopup);
                    window.openedPopup.hide();
                    window.openedPopup = null;
                    window.intervalIdForOpenedPopup = null;
                }
                gridRowMouseStop = function () {
                    // format of the controlId: "gce"+gridId+"."+row+"."+col
                    if (gridObj.isPopupAssociated) {
                        gridObj.postNotificationInteractive('mouseHover', "gridMouseHoverRow" + gridObj.gridId + "." + infoObj.rowIndex, '', null);
                    }
                };
                dta.displayPopup = setTimeout(gridRowMouseStop, 1000);
                dta.popupPos[0] = getAbsoluteLeftPos(this) + this.offsetWidth;
                dta.popupPos[1] = getAbsoluteTopPos(this);
            }
        }
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

GCMH.onSelectCellMouseoutEvent = function (e) {
    this.title = "";
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var dta = gridObj.getDtaInstance();
    clearTimeout(dta.displayPopup);
    var ctrlId = "popupWinRow" + infoObj.rowIndex;
    var evt = e ? e : window.event;
    var targObjName = evt.relatedTarget || evt.toElement;

    var parentName;
    var closePopup = true;
    while (targObjName != null && targObjName.tagName != 'BODY') {
        if (targObjName.tagName == 'DIV' && (targObjName.id == ctrlId || (targObjName.id.indexOf(ctrlId + "-shadow") === 0))) {
            closePopup = false;
        }
        targObjName = targObjName.parentNode;
    }
    if (closePopup && (window.openedPopup) && window.openedPopup.type != SHOW_HOVER) {
        clearInterval(window.intervalIdForOpenedPopup);
        window.openedPopup.hide();
        window.openedPopup = null;
        window.intervalIdForOpenedPopup = null;
    }
    return gridObj.msgHandler.stopEventBubbling(e);
}

GCMH.onRowHeaderClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var agent = window.GUTIL;
    var srcElem = agent.getEventSource(e);
    var title = srcElem.title;
    if (gridObj) {
        gridObj.clearCopyMarking();
        gridObj.onRowHeaderClick(infoObj.rowIndex, title);
    }
}

GCMH.onColHeaderClickEvent = function (e) {
    // code to prevent click event on resizeReorderAnchor begins  bug 23586420

    if (!e) var e = window.event;
    var target = e.target;
    var srcElement = e.srcElement;
    if (target) {
        if (target.className == "resizeReorderAnchor") return false;
    }
    else if (srcElement) {
        if (srcElement.className == "resizeReorderAnchor") return false;
    }
    // code to prevent click event on resizeReorderAnchor ends

    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.clearCopyMarking();
        gridObj.onColHeaderClick(infoObj.colIndex);
    }
}

GCMH.onColHeaderRightClickEvent = function (e) {
    jdeWebGUIPopupMenuEventHandler(e);
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var gridId = gridObj.gridId;

        if (((gridObj.isFirefox || gridObj.isFirefoxMac || gridObj.isWebKit) && e.button == 2) || gridObj.isIE) {
            gridObj.closeFrzHidePopUp();
            var itemHeight = 70, itemWidth = 80, htmlContent;
            var mousePos = gridObj.getCursorPosition(e);
            var formDiv = document.getElementById('e1formDiv');
            if (formDiv && formDiv.scrollTop > 0 && (formDiv.clientHeight < formDiv.scrollHeight)) {
                mousePos[1] -= formDiv.scrollTop;
            }
            var topPos = getAbsoluteTopPos(this, true);
            var hiddenColsCount = 0, frzHiddenColsCount = 0, freezColIndex = -1, isColFrozen = false, colObj;
            if (gridObj.isFreezeMode && gridObj.freezColIndex != -1)
                freezColIndex = gridObj.freezColIndex;
            else if (gridObj.isEditable && gridObj.isFreezeModeEditGrid && gridObj.freezeEditColIndex != -1)
                freezColIndex = gridObj.freezeEditColIndex;
            if (freezColIndex != -1 && infoObj.colIndex <= freezColIndex)
                isColFrozen = true;
            for (i = 0; i < gridObj.dataColumnCount && !isColFrozen; i++) {
                colObj = gridObj.getColumnByColIndex(i);
                if (colObj && colObj._isHidden) {
                    if (colObj.isUserHideCol) {
                        hiddenColsCount++;
                        if (frzHiddenColsCount > 0) {
                            break;
                        }
                    }
                    else if (colObj.isUserFreezeHideCol) {
                        frzHiddenColsCount++;
                        if (hiddenColsCount > 0) {
                            break;
                        }
                    }
                }
            }

            var popUpTableContent = "<table width=\"100%\" height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td id=\"FreezeTD\" valign=\"middle\">";

            var hideDivTag = "<div id=\"HideDiv\"";
            // Enable the Hide option ONLY if the column is not required entry
            if (!gridObj.getColumnByColIndex(infoObj.colIndex).isRequiredEntry && !isColFrozen) {
                hideDivTag += " style=\"CURSOR: pointer;\"";
                hideDivTag += " onmouseout=\"this.style.backgroundColor='#FFFFFF';\" onmouseover=\"this.style.backgroundColor='#c1e6ff';\"";
                hideDivTag += " onclick='javascript:JDEDTAFactory.getInstance(\"" + gridObj.namespace + "\").doAsynPostByIdWithDelayProcIndicator(\"colHide\", \"" + gridId + '.' + infoObj.colIndex + "\", null); ";
                hideDivTag += " document.getElementById(\"saveFmtButton" + gridId + "\").style.display = \"block\";";
                hideDivTag += " var gridObj = GCMH.findGrid(\"" + gridId + "\");";
                hideDivTag += " gridObj.closeFrzHidePopUp();";
                hideDivTag += " gridObj.removeDocClickEvent();";
                hideDivTag += " gridObj.getColumnByColIndex(\"" + infoObj.colIndex + "\").isUserHideCol = true;'>";
            }
            else {
                hideDivTag += " disabled=true class=\"DisabledFreezeMenuItem\">";
            }
            hideDivTag += "&nbsp;&nbsp;&nbsp;&nbsp;" + gridObj.newConstants[gridObj.HIDE_STR] + " </div>";
            htmlContent = hideDivTag;
            itemWidth = Math.max(itemWidth, gridObj.newConstants[gridObj.HIDE_STR].length * 8);

            var unhideDivTag = "<div id=\"UnHideDiv\"";
            // Enable the UnHide option ONLY if there is atleast one user hidden column
            if (hiddenColsCount > 0 && !isColFrozen) {
                unhideDivTag += " style=\"CURSOR: pointer;\"";
                unhideDivTag += " onmouseout=\"this.style.backgroundColor='#FFFFFF';\" onmouseover=\"this.style.backgroundColor='#c1e6ff';\"";
                unhideDivTag += " onclick='javascript: var gridObj = GCMH.findGrid(\"" + gridId + "\"); ";
                unhideDivTag += " gridObj.closeFrzHidePopUp();";
                unhideDivTag += " gridObj.removeDocClickEvent();";
                unhideDivTag += " gridObj.showUnhideCols(\"" + (mousePos[0] - 10) + "\", \"" + (topPos + 15) + "\", \"" + itemWidth + "\", \"" + itemHeight + "\", this);";
                unhideDivTag += " if(document.all) gridObj.msgHandler.stopEventBubbling();";
                unhideDivTag += " else gridObj.msgHandler.stopEventBubbling(event);'>";
            }
            else {
                unhideDivTag += " disabled=true class=\"DisabledFreezeMenuItem\">";
            }
            unhideDivTag += "&nbsp;&nbsp;&nbsp;&nbsp;" + gridObj.newConstants[gridObj.UNHIDE_STR] + " </div>";
            htmlContent += unhideDivTag;
            itemWidth = Math.max(itemWidth, gridObj.newConstants[gridObj.UNHIDE_STR].length * 8);

            // Enable the Freeze option ONLY if below conditions are met
            var showFrzForBrowseGrid = false, showFrzForEditGrid = false;
            if (!gridObj.isEditable && frzHiddenColsCount == 0 && !gridObj.isFreezeMode && gridObj.sizeManager.gridHasHScroll
             && gridObj.virtVertical && gridObj.virtHorizontal) {
                showFrzForBrowseGrid = true;
            }
            if (gridObj.isEditable && !gridObj.isFreezeModeEditGrid && gridObj.sizeManager.gridHasHScroll
            && gridObj.virtVertical && gridObj.virtHorizontal) {
                showFrzForEditGrid = true;
            }
            var freezeDivTag = "<div id=\"FreezeDiv\"";
            if ((showFrzForBrowseGrid || showFrzForEditGrid) && !isColFrozen) {
                freezeDivTag += " style=\"CURSOR: pointer;\"";
                freezeDivTag += " onmouseout=\"this.style.backgroundColor='#FFFFFF';\" onmouseover=\"this.style.backgroundColor='#c1e6ff';\"";
                freezeDivTag += " onclick='javascript: var gridObj = GCMH.findGrid(\"" + gridId + "\"); ";
                freezeDivTag += " gridObj.freezeCol(" + infoObj.colIndex + "); ";
                freezeDivTag += " gridObj.closeFrzHidePopUp(); ";
                freezeDivTag += " gridObj.removeDocClickEvent();'>";
            }
            else {
                freezeDivTag += " disabled=true class=\"DisabledFreezeMenuItem\">";
            }
            freezeDivTag += "&nbsp;&nbsp;&nbsp;&nbsp;" + gridObj.newConstants[gridObj.FREEZE_STR] + " </div>";
            htmlContent += freezeDivTag;
            itemWidth = Math.max(itemWidth, gridObj.newConstants[gridObj.FREEZE_STR].length * 8);

            var unfreezeDivTag = "<div id=\"UnFreezeDiv\"";
            // Enable the UnFreeze option ONLY if below conditions are met
            if ((gridObj.isFreezeMode && !gridObj.isEditable) || (gridObj.isFreezeModeEditGrid && gridObj.isEditable)) {
                unfreezeDivTag += " style=\"CURSOR: pointer;\"";
                unfreezeDivTag += " onmouseout=\"this.style.backgroundColor='#FFFFFF';\" onmouseover=\"this.style.backgroundColor='#c1e6ff';\"";
                unfreezeDivTag += " onclick='javascript: var gridObj = GCMH.findGrid(\"" + gridId + "\"); ";
                unfreezeDivTag += " gridObj.unFreezeCol(" + infoObj.colIndex + "); ";
                unfreezeDivTag += " gridObj.closeFrzHidePopUp();";
                unfreezeDivTag += " gridObj.removeDocClickEvent();'>";
            }
            else {
                unfreezeDivTag += " disabled=true class=\"DisabledFreezeMenuItem\">";
            }
            unfreezeDivTag += "&nbsp;&nbsp;&nbsp;&nbsp;" + gridObj.newConstants[gridObj.UNFREEZE_STR] + " </div>";
            htmlContent += unfreezeDivTag;
            itemWidth = Math.max(itemWidth, gridObj.newConstants[gridObj.UNFREEZE_STR].length * 8);

            if (htmlContent) {
                if (document.all) {
                    document.attachEvent("onclick", jdeWebGUIPopupMenuEventHandler);
                }
                else {
                    document.addEventListener("click", jdeWebGUIPopupMenuEventHandler, false);
                }
                popUpTableContent += htmlContent + "</td></tr></tbody></table>";
                createPopup('popupFrzWin' + gridId + '.' + infoObj.colIndex, FREEZE_COL, false, false, true, false,
                         '', null, null, null, null, null, mousePos[0], mousePos[1], itemWidth, itemHeight, this,
                         popUpTableContent);
            }
            else {
                return true; // To show the default right click menu
            }
            return gridObj.msgHandler.stopEventBubbling(e); // To Suppress the default right click menu
        }
    }
}

//This event handler initializes the column for 'reorder' drag and attaches mousemove and mouseup
//handlers on the fly to facilitate dragging. Finally ait calls a cleanup function to detach them and reset
//other datas.
GCMH.onColHeaderMouseDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var agent = window.GUTIL;
    // Do not execute the logic in this function for mouse right click since it shows the column name when we right click
    if (window.event && window.event.button == 2)
        return true;
    if (gridObj) {
        //Ensure grid is in clean state before anything
        if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLORDER) {
            gridObj.performPostFormattingCleanup();
        }
        else if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLSIZE) {
            gridObj.gridHeaderBack.onmouseup();
        }

        //A representation of the column header that moves along with mouse
        var colDragClone = this.getElementsByTagName('div')[0].cloneNode(true);
        colDragClone.className = 'colDragClone';
        colDragClone.style.position = 'absolute';
        colDragClone.setAttribute('id', 'colDragClone');
        document.body.appendChild(colDragClone);

        //Changes background color of the dragged column header
        gridObj.updateColDragStatus(infoObj.colIndex, true);
        //disable default selection
        agent.setHTMLDragSelection(gridObj.gridHeaderBack, false);
        //Start format Change
        gridObj.formatChanging = gridObj.FORMAT_TYPE_COLORDER;
        //Add auto scroll handlers
        gridObj.gridHeaderBack.onmousemove = function (e) {
            gridObj.onColDragMouseMove(e);
            var curpos = gridObj.getCursorPosition(e);
            //Take into account the scroll positions on the 'e1formDiv' also (after HFE - lock toolbar project)
            var formDiv = document.getElementById('e1formDiv');
            var formDivScrollTop = 0;
            var formDivScrollLeft = 0;
            if (formDiv) {
                formDivScrollTop = formDiv.scrollTop;
                formDivScrollLeft = formDiv.scrollLeft;
            }
            colDragClone.style.left = curpos[0] - formDivScrollLeft + "px";
            colDragClone.style.top = curpos[1] + 25 - formDivScrollTop + "px";
        }
        //Set the startcol position on gridObj
        gridObj.reOrderOldPos = infoObj.colIndex;
        gridObj.reOrderNewPos = infoObj.colIndex;

        gridObj.gridHeaderBack.onmouseup = function (e) {
            if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLORDER) {
                if (gridObj.reOrderNewPos != -1 && (gridObj.reOrderOldPos != gridObj.reOrderNewPos)) {
                    gridObj.postNotificationInteractive('colReorder', gridObj.gridId + '.' + gridObj.reOrderOldPos + '.' + gridObj.reOrderNewPos, null, null);
                }
                else {
                    gridObj.updateColDragStatus(infoObj.colIndex, false);
                }
                gridObj.performPostFormattingCleanup();
            }
        }
    }
}

GCMH.disableGridTouchEvents = function (gridId) {
    if (gridId) {
        var gridBack = document.getElementById("jdeGridBack" + gridId);
        if (gridBack)
            gridBack.ontouchstart = null;
    }
}

GCMH.enableGridTouchEvents = function (gridId) {
    if (gridId) {
        var gridBack = document.getElementById("jdeGridBack" + gridId);
        if (gridBack)
            gridBack.ontouchstart = GCMH.onGridTouchStartEvent;
    }
}

GCMH.doubleTapTimer = null;
// This function attaches and processes touch events for iOS and other touch OSs.
GCMH.onGridTouchStartEvent = function (e) {
    var tFingers = e.touches.length;
    if (tFingers == 3) {
        e.preventDefault();
    }
    else if (tFingers == 2 || tFingers == 1) {
        // Let it go through
    }
    else {
        return;
    }

    var messageHandler = GCMH;
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;

    if (gridObj) {
        gridObj.nbrFingers = tFingers;
        gridObj.moveXArray = [];
        gridObj.moveYArray = [];
        gridObj.doubleTapDeltaTime = 700;

        this.ontouchmove = function (e) {
            return messageHandler.onGridTouchMoveEvent(e, this);
        }

        this.ontouchcancel = function (e) {
            var infoObj = GUTIL.retrieveGridAttributes(this);
            return messageHandler.onGridTouchClean(infoObj.gridObj);
        }

        this.ontouchend = function (e) {
            return messageHandler.onGridTouchEndEvent(e, this);
        }
    }
}

GCMH.onGridTouchMoveEvent = function (e, obj) {
    var infoObj = GUTIL.retrieveGridAttributes(obj);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var tFingers = gridObj.nbrFingers;
        // add 3 finger touch events to the touch array
        if (tFingers == 3) {
            gridObj.moveXArray.push(e.touches[0].pageX);
            gridObj.moveYArray.push(e.touches[0].pageY);
        }
        // 2 finger touch events are clicks only. If a move is detected then throw away the event and let
        // the device handle the event natively
        else if (tFingers == 2) {
            gridObj.nbrFingers = 0;
        }
    }

}

GCMH.onGridTouchClean = function (gridObj, clearTimer) {
    gridObj.nbrFingers = 0;
    gridObj.moveXArray = [];
    gridObj.moveYArray = [];
    TouchLib.clean();
    if (clearTimer) {
        GCMH.doubleTapTimer = null;
    }
}

GCMH.onGridTouchEndEvent = function (e, obj) {
    var infoObj = GUTIL.retrieveGridAttributes(obj);
    var gridObj = infoObj.gridObj;
    var tFingers = gridObj.nbrFingers;
    // Only process 2 or 3 finger events
    if (tFingers != 3 && tFingers != 2) {
        this.onGridTouchClean(gridObj);
        return;
    }
    // 3 finger events need to move a certain distance before acceptance
    if (tFingers == 3 && gridObj.moveXArray.length < 2) {
        this.onGridTouchClean(gridObj);
        return;
    }
    // 2 finger events are a single touch only (i.e. 2 finger 'click')
    // simple row exit display
    if (tFingers == 2) {
        e.preventDefault();

        return this.tap(gridObj, infoObj, e);
    }
    // determine the gesture for the 3 fingered event
    else if (tFingers == 3) {
        e.preventDefault();
        TouchLib.init(gridObj.moveXArray, gridObj.moveYArray);
        var gesture = TouchLib.determineGesture(TouchLib.DIRECTION_SENSITIVITY_MEDIUM);
        this.onGridTouchClean(gridObj);

        switch (gesture) {
            case TouchLib.UP:
                if (isSubForm) {
                    var subFormNextParId = GCMH.getSubFormNext(e, gridObj);
                    var subFormNextParElem = document.getElementById(subFormNextParId);
                    if (subFormNextParElem) {
                        var subFormNextButton = subFormNextParElem.firstChild;
                        if (subFormNextButton)
                            subFormNextButton.onclick(e);
                    }
                }
                else {
                    var button = document.getElementById("NEXT" + gridObj.gridId);
                    if (button) {
                        JDEDTAFactory.getInstance('').post('gNP' + gridObj.gridId);
                    }
                }
                return gridObj.msgHandler.stopEventBubbling(e);
                break;
            case TouchLib.DOWN:
                if (isSubForm) {
                    var subFormFindId = GCMH.getSubFormFind(e, gridObj);
                    var subFormFindButton = document.getElementById(subFormFindId);
                    if (subFormFindButton)
                        subFormFindButton.onclick(e);
                }
                else {
                    TouchLib.processGesture(gesture, window, document, e);
                }
                return gridObj.msgHandler.stopEventBubbling(e);
                break;
        }
    }
    this.onGridTouchClean(gridObj);
}
GCMH.getSubFormFind = function (e, gridObj) {
    var parElem = '';
    var parElemId = '';
    var subFormFindId = 'C';
    if (gridObj && gridObj.gridOuter)
        parElem = GUTIL.findParentTag(gridObj.gridOuter, 'DIV');

    if (parElem) {
        parElemId = parElem.id;
        var firChildId = parElem.firstChild.id;
        subFormFindId = subFormFindId + firChildId.substr(5);
    }
    return subFormFindId;
}
GCMH.getSubFormNext = function (e, gridObj) {
    var gridElem = '';
    var gridElemId = '';
    var subFormNextId = 'NEXT';
    if (gridObj && gridObj.gridOuter)
        gridElem = gridObj.gridOuter;

    if (gridElem) {
        gridElemId = gridElem.id.substr(5);
        subFormNextId = subFormNextId + gridElemId;
    }
    return subFormNextId;
}

GCMH.singleTap = function (gridObj, infoObj, event) {
    gridObj.nbrFingers = 0;
    var rowExit = getExitMenuLaunchElement(EXIT_TYPE.ROW_EXIT);

    this.onGridTouchClean(gridObj, true);
    if (rowExit != null) {
        if (GCMH.isRightClickPrefCheckEnabled() || gridObj.isTouchEnabled) {
            rowExit.onclick(event);
        }
        GCMH.selectRow(gridObj, infoObj);
    }
    return gridObj.msgHandler.stopEventBubbling(event);
}

GCMH.doubleTap = function (gridObj) {
    this.onGridTouchClean(gridObj, true);
    if (self.parent) {
        self.parent.goHome();
    }
}

GCMH.tap = function (gridObj, infoObj, event) {
    var gTimer = GCMH.doubleTapTimer;
    if (gTimer == null) {
        // First tap, we wait X ms to the second tap
        GCMH.doubleTapTimer = setTimeout(function () { GCMH.singleTap(gridObj, infoObj, event) }, gridObj.doubleTapDeltaTime);
        gridObj.msgHandler.stopEventBubbling(event);
    } else {
        // Second tap
        clearTimeout(gTimer);
        GCMH.doubleTap(gridObj);
    }
    this.onGridTouchClean(gridObj);
}
//This event handler triggers during dynamic reorder of columns when mouse moves over a column
//This function determines the potential destination positions (by highlighting anchors) for the dragged column
GCMH.onColDragMouseOverEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var agent = window.GUTIL;
    if (gridObj) {
        //Ensure anchors are highlighted only during reorder       
        if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLORDER) {
            //Before highlighting the column's anchor, hide the previously highlighted anchor
            if (gridObj.currentAnchor != undefined)
                gridObj.currentAnchor.style.borderColor = 'transparent';

            if (infoObj.colIndex > gridObj.reOrderOldPos) {
                var anchor = document.getElementById('rAnchor' + gridObj.gridId + '.' + infoObj.colIndex);
                anchor.style.borderColor = '#8C8C8C';
            }
            else if (infoObj.colIndex < gridObj.reOrderOldPos) {
                var anchor = document.getElementById('lAnchor' + gridObj.gridId + '.' + infoObj.colIndex);
                anchor.style.borderColor = '#8C8C8C';
            }
            gridObj.reOrderNewPos = infoObj.colIndex;
            gridObj.currentAnchor = anchor;
        }
    }
}

GCMH.saveDynamicGridFormat = function (gridId, isNewFmt, title, defaultName) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj == null)
        return;

    if (!isNewFmt) {
        gridObj.postNotificationInteractive('saveDynamicFormat', gridObj.gridId, '', null);
        document.getElementById('saveFmtButton' + gridId).style.display = 'none';
        return;
    }
    var titleInfo = {
        titleIcon: "",
        titleText: title,
        titleTextColor: "#343434",
        hasClose: true,
        hasAbout: false,
        aboutText: ""
    };

    var eventHandler = {};
    eventHandler.onClose = function (e) {
        if (blockPane != null)
            blockPane._hideBlockingDiv(document);
    }
    var pw = new PopupWindow('0_1', false, titleInfo, eventHandler);

    var okScript = "javascript: var gridObj = GCMH.findGrid('" + gridId + "'); gridObj.postNotificationInteractive('saveDynamicFormat', gridObj.gridId, form.gridtabformatName.value, null);";
    var keyDownScript = "javascript: var gridObj = GCMH.findGrid('" + gridId + "'); if (event.keyCode == 13) {document.getElementById('ok').click(); if (gridObj.isIE) {return gridObj.msgHandler.stopEventBubbling();} else {return gridObj.msgHandler.stopEventBubbling(event);}}";
    pw.setContent('<form><INPUT class=textfield size=16 name=gridtabformatName id=gridtabformatName MAXLENGTH=30 onkeydown ="' + keyDownScript + '" value="' + defaultName + '" aria-label="' + title + '"></input><input type="button" id="ok" value="OK" onClick="' + okScript + '";></input></form>');
    //Protect Under Form
    blockPane = new UIBlockingPane();
    blockPane.blockUI(document);

    var gtab = document.getElementById('gtab' + gridId);
    var saveButton = document.getElementById('saveFmtButton' + gridId);
    var top, left;
    //Chris updates
    if (gtab != null) {
        top = getAbsoluteTopPos(gtab) + gtab.offsetHeight;
        left = getAbsoluteLeftPos(gtab);
        pw.display(left, top, 175, 40);
    }
    else {
        top = getAbsoluteTopPos(saveButton) + saveButton.offsetHeight;
        left = getAbsoluteLeftPos(saveButton);
        pw.display(left - 175, top, 175, 40);
    }

    document.getElementById('gridtabformatName').select();
}


GCMH.onTreeNodeHeaderMouseMoveEvent = function (e) {
    var gridObj = (GCMH.gridObjForMouseDown) ? GCMH.gridObjForMouseDown : GUTIL.retrieveGridObject(this);
    if (gridObj) {
        //this event requires the clientX value for where the mouse is
        gridObj.onTreeNodeHeaderMouseMove(GUTIL.getPageX(e));
    }
}

GCMH.onTreeNodeHeaderMouseDownEvent = function (e) {
    var gridObj = GUTIL.retrieveGridObject(this);
    if (gridObj) {
        //this event requires the pageX value for where the mouse is
        gridObj.onTreeNodeHeaderMouseDown(this, GUTIL.getPageX(e));
    }
    return false; // prevent the text selection during drag and drop
}

GCMH.onTreeNodeHeaderMouseUpEvent = function (e) {
    var gridObj = (GCMH.gridObjForMouseDown) ? GCMH.gridObjForMouseDown : GUTIL.retrieveGridObject(this);
    if (gridObj) {
        gridObj.onTreeNodeHeaderMouseUp(GUTIL.getPageX(e));
    }
}

GCMH.onGridSelFocusEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            gridObj.onGridSelFocus(infoObj.rowIndex);
            gridObj.renderFreezeMode = false;
        }
        else {
            gridObj.onGridSelFocus(infoObj.rowIndex);
        }
    }
}

GCMH.onGridSelBlurEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            gridObj.onGridSelBlur(infoObj.rowIndex, this);
            gridObj.renderFreezeMode = false;
        }
        else {
            gridObj.onGridSelBlur(infoObj.rowIndex, this);
        }
    }
}

GCMH.onGridSelClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.clearCopyMarking();
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            //This will select the Frozen Data Row
            gridObj.onGridSelClick(infoObj.rowIndex, this, e);
            gridObj.renderFreezeMode = false;
            //This will select the Original grid row
            gridObj.setRowSelection(infoObj.rowIndex, this.checked);

            var ctrlVal = (this.type == "radio" || this.checked) ? "select" : "unselect";
            /* During the un-selection of a row, need to sync-up the Select All element b/w Frozen Vs Orig. grid
            when Select All is already checked */
            if (ctrlVal == "unselect") {
                var origSelAllElem = document.getElementById(JSSelectHeaderCell.prototype.DATA_ID_FMT.format(gridObj.gridId));
                var frzSelAllElem = document.getElementById(JSSelectHeaderCell.prototype.FRZ_DATA_ID_FMT.format(gridObj.gridId));
                //some grids have no "Select All" element
                if (origSelAllElem && frzSelAllElem && (frzSelAllElem.checked != origSelAllElem.checked)) {
                    gridObj.setSelectAll(this.checked);
                }
            }
        }
        else {
            gridObj.onGridSelClick(infoObj.rowIndex, this, e);
            if (gridObj.isFreezeMode) {
                gridObj.renderFreezeMode = true;
                //This will select/un-select the Frozen Data Row
                gridObj.setRowSelection(infoObj.rowIndex, this.checked);

                var ctrlVal = (this.type == "radio" || this.checked) ? "select" : "unselect";
                /* During the un-selection of a row, need to sync-up the Select All element b/w Frozen Vs Orig. grid
                when Select All is already checked */
                if (ctrlVal == "unselect") {
                    var origSelAllElem = document.getElementById(JSSelectHeaderCell.prototype.DATA_ID_FMT.format(gridObj.gridId));
                    var frzSelAllElem = document.getElementById(JSSelectHeaderCell.prototype.FRZ_DATA_ID_FMT.format(gridObj.gridId));
                    //some grids have no "Select All" element
                    if (origSelAllElem && frzSelAllElem && (frzSelAllElem.checked != origSelAllElem.checked)) {
                        gridObj.setSelectAll(this.checked);
                    }
                }
                gridObj.renderFreezeMode = false;
            }
            if (gridObj.isMac)  //Grid focus is lost when we press Cmd + C and need to set it explicitly
            {
                document.getElementById('copyPad' + gridObj.gridId).focus();
            }
        }
    }
}

GCMH.onGridRowCursorClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onGridRowCursorClick(infoObj.rowIndex);
    }
}

GCMH.onGridSelDblClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.clearCopyMarking();
        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            gridObj.onGridSelDblClick(infoObj.rowIndex);
            gridObj.renderFreezeMode = false;
        }
        else {
            gridObj.onGridSelDblClick(infoObj.rowIndex);
        }
    }
}

GCMH.onRowEditStartClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onRowEditStartClick(infoObj.rowIndex);
    }
}

GCMH.onRowEditFinishClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onRowEditFinishClick(infoObj.rowIndex);
    }
}

GCMH.onRowEditFinishKeyDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        gridObj.onRowEditFinishKeyDown(keyInfo, infoObj.rowIndex);
    }
}

GCMH.onRowEditStartKeyDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        gridObj.onRowEditStartKeyDown(keyInfo, infoObj.rowIndex);
    }
}

GCMH.onCheckBoxCellClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var isEvt = false;
        if (window.appAutoTestingOn) {
            if (e)
                isEvt = true;
        }
        else {
            isEvt = true;
        }
        if (isEvt) {
            var keyInfo = GUTIL.retrieveKeystrokeInfo(e);
            if (!keyInfo.isCtrlDown && keyInfo.isShiftDown && !keyInfo.isAltDown) {
                gridObj.copyMarking = true;
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
                return gridObj.msgHandler.stopEventBubbling(e);
            }
            else {
                gridObj.clearCopyMarking();
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
                gridObj.copyMarking = false;
                gridObj.onCheckBoxCellClick(infoObj.rowIndex, infoObj.colIndex, this);
            }
        }
        else {
            gridObj.onCheckBoxCellClick(infoObj.rowIndex, infoObj.colIndex, this);
        }
    }
}

GCMH.onHyperLinkCellClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var isEvt = false;
        if (window.appAutoTestingOn) {
            if (e)
                isEvt = true;
        }
        else {
            isEvt = true;
        }
        if (isEvt) {
            var keyInfo = GUTIL.retrieveKeystrokeInfo(e);
            if (!keyInfo.isCtrlDown && keyInfo.isShiftDown && !keyInfo.isAltDown) {
                gridObj.copyMarking = true;
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
                return gridObj.msgHandler.stopEventBubbling(e);
            }
            else {
                gridObj.clearCopyMarking();
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
                gridObj.copyMarking = false;
                gridObj.onHyperLinkCellClick(infoObj.rowIndex, infoObj.colIndex);
                if (gridObj.isLowInteractivity()) {
                    return gridObj.msgHandler.stopEventBubbling(e);
                }
            }
        }
        else {
            gridObj.onHyperLinkCellClick(infoObj.rowIndex, infoObj.colIndex);
            if (gridObj.isLowInteractivity()) {
                return gridObj.msgHandler.stopEventBubbling(e);
            }
        }
    }
    return false;
}

GCMH.onEditCapableCellClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        // To hide the Frozen area in Edit grid and re-set the h-scroll position to the left most
        var freezeDiv = document.getElementById("freezeGrid" + gridObj.gridId);
        if (freezeDiv && (infoObj.colIndex <= gridObj.freezeEditColIndex)) {
            freezeDiv.style.display = 'none';
            if (!gridObj.isRtlLayout) {
                gridObj.gridBack.scrollLeft = -1;
                gridObj.scrollLeft = -1;
            }
            else {
                gridObj.gridBack.scrollLeft = gridObj.visibleDataWidth;
                gridObj.scrollLeft = gridObj.visibleDataWidth;
            }
        }
        var isEvt = false;
        if (window.appAutoTestingOn) {
            if (e)
                isEvt = true;
        }
        else {
            isEvt = true;
        }
        if (isEvt) {
            var keyInfo = GUTIL.retrieveKeystrokeInfo(e);
            if (!keyInfo.isCtrlDown && keyInfo.isShiftDown && !keyInfo.isAltDown) {
                gridObj.copyMarking = true;
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
                return gridObj.msgHandler.stopEventBubbling(e);
            }
            else {
                gridObj.clearCopyMarking();
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
                gridObj.copyMarking = false;
                gridObj.onEditCapableCellClick(infoObj.rowIndex, infoObj.colIndex);
            }
        }
        else {
            gridObj.onEditCapableCellClick(infoObj.rowIndex, infoObj.colIndex);
        }
    }
}

GCMH.onEditCellClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if (gridObj.isTouchEnabled) {
            gridObj.cellClicked = true;
        }
        gridObj.clearCopyMarking();
        // To hide the Frozen area in Edit grid and re-set the h-scroll position to the left most
        var dataCellElem, frozenCell = false;
        var freezeDiv = document.getElementById("freezeGrid" + gridObj.gridId);
        if (freezeDiv && (infoObj.colIndex <= gridObj.freezeEditColIndex) && ("none" != freezeDiv.style.display)) {
            dataCellElem = gridObj.getDataCellElem(infoObj.rowIndex, infoObj.colIndex);
            if (!dataCellElem) {
                gridObj.focusManager.verifyVirtualCellRange(infoObj.rowIndex, infoObj.colIndex);
                dataCellElem = gridObj.getDataCellElem(infoObj.rowIndex, infoObj.colIndex);
            }
            frozenCell = true;
            freezeDiv.style.display = 'none';
            if (!gridObj.isRtlLayout) {
                gridObj.gridBack.scrollLeft = -1;
                gridObj.scrollLeft = -1;
            }
            else {
                gridObj.gridBack.scrollLeft = gridObj.visibleDataWidth;
                gridObj.scrollLeft = gridObj.visibleDataWidth;
            }
        }
        /* When the focus need tobe set on the Frozen area edit cell,
        we need to pass-in the corresponding edit cell in Original grid where the focus will be set */
        if (!frozenCell) {
            gridObj.onEditCellClick(infoObj.rowIndex, infoObj.colIndex, e, this);
        }
        else if (dataCellElem) {
            gridObj.onEditCellClick(infoObj.rowIndex, infoObj.colIndex, e, dataCellElem);
        }

        var dtaFactory = gridObj.msgHandler.getJdeDtaFactory();
        if (dtaFactory && dtaFactory.captureStatus) {
            return;
        }
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

/*This event handler triggers on mousedown for all cells except edit cell.
* This will 1)Mark start cell
*           2)Disable default browser highlighting (what you see when you click and drag mouse)
*           3)Attach mousemove and mouseover handlers to gridBack
*/
GCMH.onCopyDragMouseDownEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if (gridObj.allowCopy != true)
            return;

        //disable default highlighting
        gridObj.disableDOMHighlighting();
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e);
        if (keyInfo.isShiftDown && !keyInfo.isCtrlDown && !keyInfo.isAltDown) {
            if (gridObj.copyMarked) {
                gridObj.copyMarking = true;
                gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
            }
        }
        else {
            //Clear previous marking
            gridObj.clearCopyMarking();
            if (gridObj.sizeManager.gridHasHScroll)
                gridObj.focusManager.setFocusOnGridControl();
            else {
                var focusedOnRowSelector = gridObj.focusManager.setFocusOnRowSelector(infoObj.rowIndex);
                if (focusedOnRowSelector == false) {
                    gridObj.focusManager.setFocusOnGridControl();
                }
            }

            gridObj.disableDOMHighlighting();

            //Mark start cell
            gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);

            //Handler to trigger auto scroll
            gridObj.gridBack.onmousemove = function (event) {
                gridObj.onCopyDragMouseMove(event);
            };

            //During scroll the mouseover should not be propagated to document element, which
            //will clear the scroll Intervals
            gridObj.gridBack.onmouseover = function (event) {
                return gridObj.msgHandler.stopEventBubbling(event);
            };
        }
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

/*This event handler triggers on mouseup for all cells.
* This will 1)Mark end cell
*           2)Cleanup the transient event handlers attached to gridBack and reenable default browser highlighting
*/
GCMH.onCopyDragMouseUpEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e);
        if (!gridObj.allowCopy || !gridObj.copyMarked) //Proceed only if grid is copyMarked.
            return;

        if (!gridObj.copyMarking && !keyInfo.isShiftDown)
            return;

        if (!keyInfo.isShiftDown) {
            gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
            gridObj.performPostCopyMarkCleanUp();
        }
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}
GCMH.onMouseHoverEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var cellElem = gridObj.getDataCell(infoObj.rowIndex, infoObj.colIndex);
    var imgTag = cellElem.getElementsByTagName("img")[0];
    imgTag.src = window["E1RES_img_popup_indicator_ovr_png"];
    imgTag.style.position = "absolute";
}

GCMH.onMouseHoverClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var dta = gridObj.getDtaInstance();
    var ctrlId = "popupWinCol" + infoObj.colIndex + "_" + infoObj.rowIndex;
    var cellElem = gridObj.getDataCell(infoObj.rowIndex, infoObj.colIndex);
    if (window.openedPopup) {
        if ((window.openedPopup.id != ctrlId)) {
            clearInterval(window.intervalIdForOpenedPopup);
            window.openedPopup.hide();
            window.openedPopup = null;
            window.intervalIdForOpenedPopup = null;
        }
        else {
            if (!gridObj.isEditable) {
                return;
            }
            else {
                var valueChanged = false;
                var cellValue = null;
                var colDataIdx = gridObj.getDataIdxForColIndex(infoObj.colIndex);
                var dataRow = gridObj.gridData[infoObj.rowIndex];
                cellValue = this.parentNode.parentNode.getElementsByTagName("INPUT")[0].cellPrevValue;
                if (cellValue)
                    valueChanged = (dataRow[colDataIdx] != cellValue);
                if (valueChanged)
                    this.parentNode.parentNode.getElementsByTagName("INPUT")[0].cellPrevValue = dataRow[colDataIdx];
                else
                    return;
            }
        }
    }

    var colObj = gridObj.getColumnByColIndex(infoObj.colIndex);
    var idToError = colObj.cellRenderer.CELL_NAME_FORMAT.format(gridObj.gridId, infoObj.rowIndex, infoObj.colIndex);
    //Remove the gce,ecb from the grid ID
    idToError = idToError.substring(3);
    var error = null;
    if (gridObj.inyfeObj) {
        error = gridObj.inyfeObj.checkAndreturnErrorString(cellElem, idToError);
    }
    var isErrorSet = false;
    if (null != error && error != undefined && error.length > 0) {
        isErrorSet = true;
    }
    var cellData = gridObj.getValueAt(infoObj.rowIndex, infoObj.colIndex);
    if (cellData && cellData != " " && cellData != "*" && !isErrorSet) {
        clearTimeout(dta.displayPopup);
        gridCellMouseStop = function () {
            gridObj.postNotificationInteractive('mouseHover', "gridMouseHoverCol" + gridObj.gridId + "." + infoObj.rowIndex + "." + infoObj.colIndex, '', null);
        };
        dta.displayPopup = setTimeout(gridCellMouseStop, 1);
        dta.popupPos[0] = getAbsoluteLeftPos(cellElem) + cellElem.offsetWidth;
        dta.popupPos[1] = getAbsoluteTopPos(cellElem);
        dta.popupPos[2] = infoObj.rowIndex;
    }

    // for hyperlink we do not want click event of hover form bubble to link action, so the user has to use link list to act link for hover and hyper combination.
    // but for other type of cell we have to let the event bubble to div to cause edit mode change, without other workaround.
    if (colObj.isHyperlink)
        return gridObj.msgHandler.stopEventBubbling(e);
}

GCMH.onMouseOutEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    var ctrlId = "popupWinCol" + infoObj.colIndex + "_" + infoObj.rowIndex;
    var cellElem = gridObj.getDataCell(infoObj.rowIndex, infoObj.colIndex);
    if (cellElem) {
        var imgTag = cellElem.getElementsByTagName("img")[0];
        imgTag.src = window["E1RES_img_popup_indicator_png"];
    }
    if ((window.openedPopup) && (window.openedPopup.id == ctrlId) && window.openedPopup.type != SHOW_HOVER) {

        var evt = e ? e : window.event;
        var targObjName = evt.relatedTarget || evt.toElement;

        var parentName;
        var closePopup = true;
        while (targObjName != null && targObjName.tagName != 'BODY') {
            if ((cellElem === targObjName) || (targObjName.tagName == 'DIV' && (targObjName.id == ctrlId || (targObjName.id.indexOf(ctrlId + "-shadow") === 0)))) {
                closePopup = false;
            }
            targObjName = targObjName.parentNode;
        }
        if (closePopup) {
            clearInterval(window.intervalIdForOpenedPopup);
            window.openedPopup.hide();
            window.openedPopup = null;
            window.intervalIdForOpenedPopup = null;
        }
    }
    return gridObj.msgHandler.stopEventBubbling(e);
}

//Event handler used for IE to dynamically mark the cells during copyDrag
GCMH.onCopyDragMouseEnterEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        if (!gridObj.allowCopy || !gridObj.copyMarking)
            return;

        //This call runs the 'endcell' click handler
        gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
        //Reset the state to 'copyMarking' as we are not done with marking yet
        gridObj.copyMarking = true;

        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

//Event handler used for FF to dynamically mark the cells during copyDrag
GCMH.onCopyDragMouseOverEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        //Unlike mouseenter, mouseover bubbles. To avoid marking the same cell multiple times the below check is made.
        if (!gridObj.allowCopy || !gridObj.copyMarking || (infoObj.rowIndex == gridObj.prevRowIdx && infoObj.colIndex == gridObj.prevColIdx))
            return;

        gridObj.prevRowIdx = infoObj.rowIndex;
        gridObj.prevColIdx = infoObj.colIndex;

        //This call runs the 'endcell' click handler
        gridObj.onCopyClick(infoObj.rowIndex, infoObj.colIndex, this);
        //Reset the state to 'copyMarking' as we are not done with marking yet        
        gridObj.copyMarking = true;

        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

//Event handler used to trigger auto scroll during copyDrag
GCMH.onCopyDragMouseMoveEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onCopyDragMouseMove();
        return gridObj.msgHandler.stopEventBubbling(e);
    }
}

GCMH.onFocusCellEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var colObj = gridObj.getColumnByColIndex(infoObj.colIndex);
        if (colObj.isNumberInput && window.showNumericKeyboard) {
            this.setAttribute('type', 'number');
        }

        if (isTouchEnabled && self && self.parent && !gridObj.focusManager.callBackFocus) {
            if (self.parent.CARO)
                self.parent.CARO.disableTouchEvents();

            GCMH.disableGridTouchEvents(gridObj.gridId);

            var e1PaneForm = document.getElementById("E1PaneForm");
            if (e1PaneForm && FORMTOUCH)
                FORMTOUCH.disableTouchEvents(e1PaneForm);

            var appFrame = self.parent.document.getElementById("e1AppFrameContainer");
            if (appFrame && self.parent.FRAMETOUCH)
                self.parent.FRAMETOUCH.disableTouchEvents(appFrame);
        }
        if (gridObj.onFocusCell(infoObj.rowIndex, infoObj.colIndex, this)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onCellChangeEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onCellChange(infoObj.rowIndex, infoObj.colIndex, this);
    }
}

GCMH.onBlurCellEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var isTouchEnabled = gridObj.isTouchEnabled;
        if (isTouchEnabled) {
            if (self && self.parent && !gridObj.focusManager.callBackFocus) {
                if (self.parent.CARO)
                    self.parent.CARO.enableTouchEvents();

                GCMH.enableGridTouchEvents(gridObj.gridId);

                var e1PaneForm = document.getElementById("E1PaneForm");
                if (e1PaneForm && FORMTOUCH)
                    FORMTOUCH.enableTouchEvents(e1PaneForm);

                var appFrame = self.parent.document.getElementById("e1AppFrameContainer");
                if (appFrame && self.parent.FRAMETOUCH)
                    self.parent.FRAMETOUCH.enableTouchEvents(appFrame);
            }
            if (gridObj.focusManager.callBackFocus) {
                gridObj.focusManager.callBackFocus = false;
            }
        }
        gridObj.onBlurCell(infoObj.rowIndex, infoObj.colIndex, this);
    }
}

GCMH.onBlurQBEEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var isTouchEnabled = gridObj.isTouchEnabled;
        if (isTouchEnabled) {
            if (self && self.parent && !gridObj.focusManager.callBackFocus) {
                if (self.parent.CARO)
                    self.parent.CARO.enableTouchEvents();

                var e1PaneForm = document.getElementById("E1PaneForm");
                if (e1PaneForm && FORMTOUCH)
                    FORMTOUCH.enableTouchEvents(e1PaneForm);

                var appFrame = self.parent.document.getElementById("e1AppFrameContainer");
                if (appFrame && self.parent.FRAMETOUCH)
                    self.parent.FRAMETOUCH.enableTouchEvents(appFrame);
            }
            if (gridObj.focusManager.callBackFocus)
                gridObj.focusManager.callBackFocus = false;
        }

        if ((gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)
            && (infoObj.colIndex <= gridObj.freezColIndex)) {
            gridObj.renderFreezeMode = true;
            gridObj.onBlurQBE(infoObj.colIndex, this);
            gridObj.renderFreezeMode = false;
        }
        else {
            gridObj.onBlurQBE(infoObj.colIndex, this);
        }
    }
}

GCMH.onTouchCellEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj && gridObj.isTouchEnabled) {
        gridObj.onTouchCell(infoObj.rowIndex, infoObj.colIndex, this);
    }
}

GCMH.onTouchDivCellEvent = function (e) {
    var divCellElement = e.target;
    if (divCellElement) {
        var inputTag = divCellElement.firstChild;
        if (inputTag) {
            var infoObj = GUTIL.retrieveGridAttributes(this);
            var gridObj = infoObj.gridObj;
            if (gridObj && gridObj.isTouchEnabled) {
                gridObj.onTouchCell(infoObj.rowIndex, infoObj.colIndex, inputTag);
            }
        }
    }
}

GCMH.onTouchTdCellEvent = function (e) {
    var tdCellElement = e.target;
    if (tdCellElement) {
        var qbeRowId = tdCellElement.parentNode.id;
        var divCellElement = tdCellElement.firstChild;
        if (divCellElement) {
            var inputTag = divCellElement.firstChild;
            if (qbeRowId && qbeRowId.indexOf('qbeRow') != -1)
                inputTag = divCellElement.firstChild.firstChild;
            if (inputTag) {
                var infoObj = GUTIL.retrieveGridAttributes(inputTag);
                var gridObj = infoObj.gridObj;
                if (gridObj && gridObj.isTouchEnabled) {
                    gridObj.onTouchCell(infoObj.rowIndex, infoObj.colIndex, inputTag);
                }
            }
        }
    }
}

GCMH.onKeyDownCellEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        // need clearCopyMarking before moving to next cell
        gridObj.clearCopyMarking();
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        if (gridObj.onCellKeyDown(keyInfo, infoObj.rowIndex, infoObj.colIndex, e)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onKeyUpECBEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        if (gridObj.onECBKeyUp(keyInfo, infoObj.rowIndex, infoObj.colIndex, e)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onKeyUpCellInputEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        if (gridObj.onCellKeyUp(keyInfo, infoObj.rowIndex, infoObj.colIndex, e, this)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

GCMH.onKeyPressCellEvent = function (e) {
    //currently only used by Firefox on Mac, as keystroke event bubbling will
    //not stop keyPress and keyUp events from being fired, which allows TAB
    //key to be interpreted by browser, removing focus on grid control
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);

        // Disable default ENTER behavior
        if (keyInfo.keyCode == 13 && typeAheadData.isTypeAheadWindowVisible()) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
        else if (keyInfo.keyCode == 9) // TAB or SHIFT TAB
        {
            typeAheadData.resetTypeAheadInternal();

            if (gridObj.onCellKeyPress(keyInfo, infoObj.rowIndex, infoObj.colIndex)) {
                return gridObj.msgHandler.stopEventBubbling(e);
            }
        }
        else {
            if (gridObj.onCellKeyPress(keyInfo, infoObj.rowIndex, infoObj.colIndex)) {
                return gridObj.msgHandler.stopEventBubbling(e);
            }
        }
    }
}

GCMH.onGridKeyDownEvent = function (e) {
    var gridObj = GUTIL.retrieveGridObject(this);
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        var browserCopy = false;
        // window.getSelection returns TRUE for Firefox ,Chrome and IE11
        if ((gridObj.isFirefox || gridObj.isFirefoxMac || gridObj.isWebKit || gridObj.isIE)
             && window.getSelection) {
            var selText = '';
            if (document.activeElement
                && document.activeElement.type == "text"
                && document.activeElement.className.indexOf("JSTextfield") > -1)//Bug 21048888 FAILED TO COPY A SINGLE CELL IN A GRID USING CTRL+C WHEN AUTO SUGGEST IS ENABLED
            {
                selText = document.activeElement.value;
                var selStart = document.activeElement.selectionStart;
                var selEnd = document.activeElement.selectionEnd;
                selText = selText.substr(selStart, selEnd - selStart);
                if ('' != selText && " " != selText && selText.length > 0)
                    browserCopy = true;
            }
        }
        // document.selection returns TRUE for IE10 and below
        else if (gridObj.isIE && document.selection) {
            var selText = '';
            if (document.activeElement
                && document.activeElement.type == "text"
                && document.activeElement.className.indexOf("JSTextfield") > -1) {
                selText = document.selection.createRange().text;
                if ('' != selText && " " != selText && selText.length > 0)
                    browserCopy = true;
            }
        }
        if (!browserCopy)
            if (keyInfo.keyCode == 67) {
                if ((keyInfo.isCtrlDown || (gridObj.isMac && e.metaKey)) && !keyInfo.isShiftDown && (!keyInfo.isAltDown || gridObj.isMac)) {
                    gridObj.captureCopyMarkedData();
                    gridObj.clearCopyMarking();
                    return;
                }
            }
            else if (keyInfo.keyCode == 86) {
                if ((keyInfo.isCtrlDown || (gridObj.isMac && e.metaKey)) && !keyInfo.isShiftDown && (!keyInfo.isAltDown || gridObj.isMac)) {
                    gridObj.pasteClipboardData();
                    return;
                }
            }
        if (gridObj.handleGridKeys(keyInfo)) {
            return gridObj.msgHandler.stopEventBubbling(e);
        }
    }
}

// set the focus on the grid div for FireFox browse grid if there is no focus element yet
GCMH.onGridClickEvent = function (e) {
    if (!document.activeElement) {
        GUTIL.setFocus(this, false);
    }
}

GCMH.onEditFrzGridClickEvent = function (e) {
    if (document.activeElement) {
        var infoObj = GUTIL.retrieveGridAttributes(document.activeElement);
        var gridObj = infoObj.gridObj;
        // To hide the Frozen area in Edit grid and re-set the h-scroll position to the left most
        var freezeDiv = document.getElementById("freezeGrid" + gridObj.gridId);
        if (freezeDiv) {
            freezeDiv.style.display = 'none';
            if (!gridObj.isRtlLayout) {
                gridObj.gridBack.scrollLeft = -1;
                gridObj.scrollLeft = -1;
            }
            else {
                gridObj.gridBack.scrollLeft = gridObj.visibleDataWidth;
                gridObj.scrollLeft = gridObj.visibleDataWidth;
            }
        }
    }
}

GCMH.onGridContextMenuCellEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;

    if (gridObj) {
        //if the Form does not have ROW EXIT Don't do grid row autosection
        if ((gridObj.isFirefox && e.button == 2) || (gridObj.isFirefoxMac && e.button == 2) || gridObj.isIE || (gridObj.isWebKit && e.button == 2)) {
            var rowExit = document.getElementById('ROW_EXIT_BUTTON');
            if (rowExit != null) {
                GCMH.selectRow(gridObj, infoObj);
            }
        }
    }
}

GCMH.selectRow = function (gridObj, infoObj) {
    if (gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid) {
        gridObj.renderFreezeMode = true;
        // To select the Frozen grid row
        gridObj.setRowSelection(infoObj.rowIndex, true);
        gridObj.renderFreezeMode = false;
        // To select the Original grid row
        gridObj.setRowSelection(infoObj.rowIndex, true);
    }
    else {
        gridObj.setRowSelection(infoObj.rowIndex, true);
    }
    // Update Server
    gridObj.postRowSelectNotification(infoObj.rowIndex, "select");
}

/*
This routine is to check whether right mouse click enabled on Grid 
*/
GCMH.isRightClickPrefCheckEnabled = function () {
    var rightClickPrefCheck = true;
    try {
        if (window.modalSSIndex != undefined && modalSSIndex != 0) {
            rightClickPrefCheck = parent.parent.isPreferenceChecked("rowExitPreference");
        }
        // please don't reorder this condition as used in case of WSRP
        else if (prefObject.isWSRP) {
            rightClickPrefCheck = prefObject.rowExitpref;
        }
        else if (parent.prefObj) {
            rightClickPrefCheck = parent.isPreferenceChecked("rowExitPreference");
        }
        //app opened in New window     
        else if (self.parent.opener != null && self.parent.opener != undefined) {
            if (!self.parent.opener.closed)
                rightClickPrefCheck = self.parent.opener.isPreferenceChecked("rowExitPreference");
        }
    }
    catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
    }

    return rightClickPrefCheck;
}

GCMH.onGridContextMenuEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;

    if (GCMH.isRightClickPrefCheckEnabled() == false) {
        return;
    }

    if (gridObj) {
        /* Note:IE returns useful values in event.button only on mousedown and mouseup events.
        It is not possible to identify the mouse button for a click, dblclick or contextmenu event because event
        button will be zero regardless of which button was pressed.But Firefox returns the Correct value on mouse rightClick
        */
        if ((gridObj.isFirefox && e.button == 2) || (gridObj.isFirefoxMac && e.button == 2) || gridObj.isIE || (gridObj.isWebKit && e.button == 2)) {
            var rowExit = document.getElementById('ROW_EXIT_BUTTON');
            if (rowExit != null) {
                //we need to pass the Mouse Event, otherwise Firefox won't get the Mouse Event
                rowExit.onclick(e);
                gridObj.msgHandler.stopEventBubbling(e);
                return false;
            }
        }
    }
}

GCMH.onSelectAllClickEvent = function (e) {
    var gridObj = GUTIL.retrieveGridObject(this);
    if (gridObj) {
        if (gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid) {
            var origSelAllElem = document.getElementById(JSSelectHeaderCell.prototype.DATA_ID_FMT.format(gridObj.gridId));
            if (origSelAllElem && (origSelAllElem.checked != this.checked)) {
                origSelAllElem.click();
            }
            gridObj.onSelectAllClick();
        }
        else {
            gridObj.onSelectAllClick();
        }
        var frzSelAllElem = document.getElementById(JSSelectHeaderCell.prototype.FRZ_DATA_ID_FMT.format(gridObj.gridId));
        //some grids have no "Select All" element
        if (frzSelAllElem && (frzSelAllElem.checked != this.checked)) {
            frzSelAllElem.click();
        }
    }
}

GCMH.showMotion = function (e) {
    if (this != null && this.tagName == 'IMG') {//is IMG test still necessary?
        showMotion(this);
    }
}

GCMH.showMotion_rtl = function (e) {
    if (this != null && this.tagName == 'IMG') {//is IMG test still necessary?
        showMotion_rtl(this);
    }
}

GCMH.removeMotion = function (e) {
    if (this != null && this.tagName == 'IMG') {//is IMG test still necessary?
        removeMotion(this);
    }
}

GCMH.removeMotion_rtl = function (e) {
    if (this != null && this.tagName == 'IMG') {//is IMG test still necessary?
        removeMotion_rtl(this);
    }
}

GCMH.onHelpEvent = function (event) {
    GUTIL.popupHelpForGridCell(this);
}

GCMH.updateHelpCursorEvent = function (e) {
    var gridObj = GUTIL.retrieveGridObject(this);
    if (gridObj) {
        window.updateHelpCursor(gridObj.namespace, this);
    }
}

/**
WARNING: The following SCROLL events require a slightly different interpretation 
of the triggering-element resolution technique.  Since they are added or 
deleted to owning elements via the addEvent/addEventListener methods, the 
"this" context-reference object is NOT the originating source element, but 
is the global window object instead - this means that the originating element 
must be resolved indirectly through the event reference instead.
**/
GCMH.onScrollDataEvent = function (e) {
    //This event invokes the main virtualization methods as a reaction to user
    //scrolling.  When virtualization is turned off, it merely records the
    //current gridObj.scrollLeft/scrollTop values.

    var agent = window.GUTIL;
    var srcElem = agent.getEventSource(e);
    var gridObj = agent.retrieveGridObject(srcElem);
    if (gridObj) {
        if (gridObj.virtHorizontal) {
            // To show/hide the Frozen area in Edit grid based on the h-scroll position
            var freezeDiv = document.getElementById("freezeGrid" + gridObj.gridId);
            var gridDataFreezDiv = document.getElementById("gridDataFreezDiv" + gridObj.gridId);
            if (freezeDiv && gridObj.isEditable && (gridObj.isFreezeMode || gridObj.isFreezeModeEditGrid)) {
                if (!gridObj.isRtlLayout) {
                    if (gridObj.gridBack.scrollLeft > 1) {
                        freezeDiv.style.display = '';
                        gridDataFreezDiv.style.width = freezeDiv.clientWidth + "px";
                    }
                    else if (gridObj.gridBack.scrollLeft <= 1) {
                        freezeDiv.style.display = 'none';
                    }
                }
                else {
                    var sizeMgr = gridObj.sizeManager;
                    var viewportEdge = gridObj.gridBack.scrollLeft + (sizeMgr.dataWidthAvailable - (sizeMgr.gridHasVScroll ? sizeMgr.SB_SIZE + 2 : 0));
                    if (viewportEdge >= gridObj.visibleDataWidth) {
                        freezeDiv.style.display = 'none';
                    }
                    else if (viewportEdge < gridObj.visibleDataWidth) {
                        freezeDiv.style.display = '';
                        gridDataFreezDiv.style.width = freezeDiv.clientWidth + "px";
                    }
                }
            }
        }
        gridObj.onScrollData();
        if (!document.activeElement) {
            agent.setFocus(srcElem, false);
        }
    }
}

GCMH.onMouseWheelHandle = function (e) 		 // Mouse Wheel Scroll event 
{
    var agent = window.GUTIL;
    var srcElem = agent.getEventSource(e);
    var gridObj = agent.retrieveGridObject(srcElem);
    if (gridObj && gridObj.gridBack)
        gridObj.gridBack.focus();
}

//This event handler triggers when mouse down on a right anchor (column right edge)
//This function initializes the 'drag to resize' process and attaches mousemove and mouseup handlers
//on the fly to facilitate dragging and releasing the mouse to finalize the width. Finally it calls a cleanup
//function that detaches these on-the-fly handlers.
GCMH.onAnchorMouseDownEvent = function (e) {
    var agent = window.GUTIL;
    var srcElem = agent.getEventSource(e);
    var gridId = srcElem.getAttribute('gridId');
    var gridObj = GCMH.findGrid(gridId);

    if (gridObj) {
        //Ensure grid is in clean state before anything
        if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLORDER) {
            gridObj.performPostFormattingCleanup();
        }
        else if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLSIZE) {
            gridObj.gridHeaderBack.onmouseup();
        }

        var col = srcElem.colObj;
        gridObj.formatChanging = gridObj.FORMAT_TYPE_COLSIZE;

        //temporarily remove qbe cell
        if (gridObj.isQBEVisible) {
            gridObj.removeQBECellInputTag(col.colIndex);
        }

        agent.setHTMLDragSelection(gridObj.gridHeaderBack, false);

        gridObj.gridHeaderBack.onmousemove = document.body.onmousemove = function (e) {
            if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLSIZE) {
                //Keep the right anchor highlighted while dragging
                agent.setOpacity(srcElem, 1);

                //Calculate the extent of drag
                var curpos = gridObj.getCursorPosition(e);
                var divpos = getAbsoluteLeftPos(srcElem) - gridObj.scrollLeft;
                var diff = curpos[0] - divpos;

                //Update the width of the DOM of column header dynamically
                var headerDiv = srcElem.headerDiv;
                var newWidth = col.getWidth() + diff;
                if (newWidth > 30) //Minimum cap on width
                {
                    headerDiv.style.width = newWidth + "px";
                    col.setWidth(newWidth);
                }
                if (gridObj.isFreezeModeEditGrid || gridObj.isFreezeMode) {
                    var gridHeader = document.getElementById("jdeGridHeaderData" + gridObj.gridId);
                    var gridHeaderFreezDiv = document.getElementById("gridHeaderFreezDiv" + gridObj.gridId);
                    if (gridHeader && gridHeaderFreezDiv) {
                        var frzHeaderRowHeight = gridHeaderFreezDiv.lastChild.lastChild.lastChild.style.height;
                        var hdrRowHeight = gridHeader.lastChild.lastChild.clientHeight;
                        if (frzHeaderRowHeight != hdrRowHeight + 'px')
                            gridHeaderFreezDiv.lastChild.lastChild.lastChild.style.height = gridHeader.lastChild.lastChild.clientHeight + "px";
                    }
                }
            }
        }
        gridObj.gridHeaderBack.onmouseup = function (e) {
            if (gridObj.formatChanging == gridObj.FORMAT_TYPE_COLSIZE) {
                //redraw qbe cell
                if (gridObj.isQBEVisible) {
                    col.redrawQBECell();
                }

                var newWidth = col.getWidth();
                col.resizeWidth(newWidth);
                //Post update to server
                gridObj.postNotificationInteractive('colResize', gridId + '.' + col.colIndex, newWidth, null);
                gridObj.resizeGrid();
                gridObj.redrawGrid();
                document.getElementById('saveFmtButton' + gridId).style.display = 'block';
                //Remove highlighting from anchor
                agent.setOpacity(srcElem, 0);
                gridObj.performPostFormattingCleanup();

            }
        }
    }
    return gridObj.msgHandler.stopEventBubbling(e);
}


GCMH.onScrollSyncEvent = function (e) {
    //This event only invokes the code responsible for synchronizing the grid
    //header scrollLeft position with the new scrolled position of the grid 
    //data's scrollLeft.  It will be turned off (removed) during a complete 
    //redraw to prevent excess header synchronization events.

    var agent = window.GUTIL;
    var srcElem = agent.getEventSource(e);
    var gridObj = GUTIL.retrieveGridObject(srcElem);
    if (gridObj) {
        gridObj.onScrollSync();
    }
}

GCMH.onScrollHeaderEvent = function (e) {
    //This event invokes the code responsible for synchronizing the grid
    //data's scrollLeft position with the new scrolled position of the grid 
    //header (usually only required because of QBE focus auto-scrolling through
    //header QBE fields bringing off-page elements into view).  It will be 
    //turned off (removed) during a complete redraw to prevent excess header 
    //synchronization events and possible dataElem scrollLeft feedback effects.

    var agent = window.GUTIL;
    var srcElem = agent.getEventSource(e);
    var gridObj = GUTIL.retrieveGridObject(srcElem);
    if (gridObj) {
        gridObj.onScrollHeader();
    }
}

// accessibility support for archor to convert user key strock space and enter key into the click event of the link.
GCMH.onAccKeyDownEventForLink = function (e) {
    var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
    if (keyInfo.keyCode == 32 || keyInfo.keyCode == 13) {
        keyInfo.srcElement.click();
        return GCMH.stopEventBubbling(e);
    }
}

// accessibility support for click event on QBE TD tag, so that we can set focus to input field using space in JAWS for IE. (not need for FF)
GCMH.onAccClickEventForTagEncloseInput = function (e) {
    var srcElem = window.GUTIL.getEventSource(e);
    var inputs = srcElem.getElementsByTagName("INPUT");
    if (inputs && inputs[0]) {
        var input = inputs[0];
        input.focus();
    }
}

///////////////////////////////////////////////////////////
// Grid Message Handler - CONTROLLER & UTILITY functions
// The following methods are controller and utility methods
// used within the Message Handler or as grid object entry
// points for external processes.
///////////////////////////////////////////////////////////
GCMH.adjustWidthForTreeNodeColumn = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj != null)
        gridObj.adjustWidthForTreeNodeColumn();
}

GCMH.stopEventBubbling = function (e) {
    if (e == null) //meaning isIE
    {
        window.event.cancelBubble = true;
        window.event.returnValue = false;
    }
    else {
        e.stopPropagation();
        e.preventDefault();

        if (e.cancelBubble) {
            e.cancelBubble = true;
        }
    }
    return false;
}

////////////////////////////////////////////////////////////
// Grid Message Handler - external EVENTS
// The following methods are called from code generated in
// VTGridRenderAdaptee and are associated with grid-related
// events triggered outside the grid object itself (mostly
// through the toolbar).  As such, they follow a different
// signature paradigm, and being triggered through inline
// HTML assignment will have only the GCMH context as "this".
////////////////////////////////////////////////////////////
GCMH.onGridKeyDown = function (gridId, namespace, e) {
    var gridObj = GCMH.findGrid(gridId);
    var handled = false;
    if (gridObj) {
        var keyInfo = GUTIL.retrieveKeystrokeInfo(e, this);
        if (gridObj.handleGridKeys(keyInfo)) {
            return gridObj.msgHandler.setEentBubbling(e);
        }
    }
}

GCMH.onPCCut = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onCut();
    }
}

GCMH.onPCCopy = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onCopy();
    }
}

GCMH.onPCPaste = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onPaste();
    }
}

GCMH.onPCIndent = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onIndent();
    }
}

GCMH.onPCOutdent = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onOutdent();
    }
}

GCMH.onPCMoveUp = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onMoveUp();
    }
}

GCMH.onPCMoveDown = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onMoveDown();
    }
}

GCMH.onPCExpandAll = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onExpandAll();
    }
}

GCMH.onPCCollapseAll = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.onCollapseAll();
    }
}

GCMH.setGridScrollbars = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.setScrollbars();
    }
}

GCMH.adjustFormBelowGrid = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.adjustFormBelowGrid();
    }
}

GCMH.onGridMaximize = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.getDtaInstance().post('gMaximize' + gridId);
    }
}

GCMH.onGridRestore = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.getDtaInstance().post('gRestore' + gridId);
    }
}

GCMH.switchGridTab = function (gridId, value) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.switchGridTab(value);
    }
}

GCMH.onSPDataReceived = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (gridObj) {
        gridObj.mlEditDTA.onSPDataReceived();
    }
}

GCMH.onDataElemResize = function (e) {
    //a special event for gridObj.dataElem, 
    var gridId = this.getAttribute('gridId');
    if (gridId) {
        var gridObj = GCMH.findGrid(gridId);
        if (gridObj) {
            gridObj.onDataElemResize();
        }
    }
}

// Public API to return name of currently focused cell, when required.
// Currently only applies to DTA calls in low interactivity.
GCMH.getGridCellName = function (elem) {
    // may be grid cell (which has no name or id) - check, and if so, return
    if (typeof (JSGridCellRender) != "undefined") {
        //grid cell elements have no name, so we might want to "fake" one up
        var gridInfo = GUTIL.retrieveGridAttributes(elem);
        if (gridInfo == null || gridInfo.gridId == null) {
            return null;
        }
        else {
            if (!isNaN(gridInfo.colIndex) && gridInfo.colIndex >= 0) {
                var gridObj = this.findGrid(gridInfo.gridId);
                if (gridObj) {
                    return gridObj.getCellNameForServer(gridInfo.realRow, gridInfo.colIndex);
                }
            }
        }
    }
}

GCMH.onAggregateClickEvent = function (e) {
    var infoObj = GUTIL.retrieveGridAttributes(this);
    var gridObj = infoObj.gridObj;
    if (gridObj) {
        gridObj.onAggregateClickEvent(infoObj.colIndex, this);
    }
}
///////////////////////////////////////
// Singleton Grid Control Utility Wrangler
///////////////////////////////////////
var GUTIL;
if (GUTIL == null) {
    GUTIL = new function () {
        this.isIE = (navigator.appName == "Microsoft Internet Explorer") || !!navigator.userAgent.match(/Trident\/7.0/); // include all IE versions up to IE 11
        this.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
        this.isSafari = false;
        this.isWebKit = (navigator.userAgent.toUpperCase().indexOf("WEBKIT") > -1);

        if (this.isIE) {
            this.hasAttribute = function (object, arg) { return (object.attributes[arg] != null); };
        }
        else {
            this.hasAttribute = function (object, arg) { return object.hasAttribute(arg); };
        }
    }
}

GUTIL._createCheckbox = function (isIE, checked) {
    return GUTIL._createCheckboxOrRadio(isIE, 'checkbox', checked);
}

GUTIL._createRadio = function (isIE, checked) {
    return GUTIL._createCheckboxOrRadio(isIE, 'radio', checked);
}

GUTIL._createCheckboxOrRadio = function (isIE, selectType, checked) {
    // if no select type then do not create the element
    if (selectType == null) {
        return;
    }

    var newSelect = null;
    if (!isIE8OrEarlier || (isIE8OrEarlier && ((newSelect = GUTIL._attemptIECreateCheckboxOrRadio(selectType, checked)) == null))) {
        //W3C standard method of creating checkbox or radio button programatically
        newSelect = document.createElement('INPUT');
        newSelect.type = selectType;
        newSelect.checked = checked;
    }
    return newSelect;
}

//IE8 has "bug" with creating INPUT fields with a checkbox or radio type; setting
//this value will not be honored after creation.  Similarly even a newly created
//checkbox will not be rendered in the "checked" state through "checked" attribute 
//until user interaction, which means we must use non-W3C-compliant method of 
//creating a new node using HTML input tag attributes, instead of just the tag name.
GUTIL._attemptIECreateCheckboxOrRadio = function (selectType, checked) {
    var newSelect = null;
    try {
        if (checked) {
            newSelect = document.createElement('<INPUT type=\"' + selectType + '\" CHECKED />');
        } else {
            newSelect = document.createElement('<INPUT type=\"' + selectType + '\" />');
        }
    } catch (excp) {
    }
    return newSelect;
}

GUTIL._addQuickElement = function (elementType, parent) {
    return GUTIL._addElementAndReparent(document.createElement(elementType), parent);
}

GUTIL._addElementAndReparent = function (child, parent) {
    parent.appendChild(child);
    return child;
}

GUTIL.moveUpToParent = function (tagName, child) {
    var parent = document.createElement(tagName)
    parent.appendChild(child);
    return parent;
}


GUTIL.newSimpleTableStructure = function () {
    //Keep a "template" object structure handy for cloning, since
    //it's much cheaper than creating new document elements from scratch

    if (this.simpleTableStructure == null) {
        var thisElement = document.createElement('TABLE');
        setAccHide(thisElement);
        thisElement.cellPadding = 0;
        thisElement.cellSpacing = 0;
        this.simpleTableStructure = thisElement;

        thisElement = GUTIL._addQuickElement('TBODY', thisElement);
        setAccHide(thisElement);
        thisElement = GUTIL._addQuickElement('TR', thisElement);
        setAccHide(thisElement);
        thisElement = GUTIL._addQuickElement('TD', thisElement);
        setAccHide(thisElement);
    }

    return this.simpleTableStructure.cloneNode(true);
}

GUTIL.newSimpleTable = function () {
    //Keep a "template" object structure handy for cloning, since
    //it's much cheaper than creating new document elements from scratch

    if (this.simpleTable == null) {
        var thisElement = document.createElement('TABLE');
        setAccHide(thisElement);
        thisElement.cellPadding = 0;
        thisElement.cellSpacing = 0;
        this.simpleTable = thisElement;
        thisElement = GUTIL._addQuickElement('TBODY', thisElement);
        setAccHide(thisElement);
    }

    return this.simpleTable.cloneNode(true);
}

GUTIL.findParentTag = function (elm, tagName) {
    while (elm != null && elm.nodeName != tagName) //Tip: nodeName return more information than tagName and it is better chose.
    {
        elm = elm.parentNode;
    };

    return elm;
}

// Collect all the attributes (gridId, rowIndex, colIndex) by traversing the DOM from elm up to TD and TR tag
// As a generic rule for types of cell, the TD tag contains colIndex and TR tag contains realRow and gridId.
// Given that in every case, the very least that's required is to resolve the grid object to which the 
// event-triggering element is attached, also find the gridObj as part of the returned information object.
GUTIL.retrieveGridAttributes = function (elm) {
    var infoObj = null;
    var tdTag = GUTIL.findParentTag(elm, 'TD');
    var trTag = GUTIL.findParentTag(tdTag, 'TR');

    if (tdTag == null || trTag == null) {
        //not looking at a grid element, so return null
        return null;
    }

    var gridId = trTag.getAttribute('gridId');
    if (!gridId) {
        var divTag = GUTIL.findParentTag(elm, 'DIV');
        if (divTag)
            gridId = divTag.getAttribute('gridId');
    }
    if (gridId) {
        var realRow = parseInt(trTag.getAttribute('realRow'));
        var gridObj = GCMH.findGrid(gridId);
        infoObj = {
            colIndex: parseInt(tdTag.getAttribute('colIndex')),
            realRow: realRow,
            rowIndex: realRow - gridObj.baseIndex,
            gridId: gridId,
            gridObj: gridObj
        };
    }
    return infoObj;
}

//Some of the events require only a quick reference to the grid object itself
//This method is a specially truncated version of the above method, designed
//to only search for the one piece of information required for some events
GUTIL.retrieveGridObject = function (elm) {
    var gridId = null;

    if (elm != null && elm.getAttribute != null && ((gridId = elm.getAttribute('gridId')) == null)) {
        var trTag = GUTIL.findParentTag(elm, 'TR');
        if (trTag == null) {
            return null;
        }
        gridId = trTag.getAttribute('gridId');
    }
    return GCMH.findGrid(gridId);
}
// Create a single object that encapsulates the information about active keys that triggered
// the designated event - this should make onKey processing more efficient, and minimize the
// penalty of running through event handling and FormKeyboardProcessor handling.
// NOTE: a reference to the originating source element is kept for a small number of keystroke
//       handling methods for which keeping this information is cheaper than trying to 
//       re-determine it again programmatically.

GUTIL.retrieveKeystrokeInfo = function (keyEvent, srcElement) {
    //
    if (!keyEvent) {
        keyEvent = window.event;
    }
    var keyHandler = window.FormKeyboardHandler;
    var keyObj = {
        keyCode: keyEvent.keyCode,
        isCtrlDown: keyHandler.isCtrlDown(keyEvent),
        isShiftDown: keyHandler.isShiftDown(keyEvent),
        isAltDown: keyHandler.isAltDown(keyEvent),
        srcElement: srcElement
    };

    if (this.isSafari) {
        if (keyEvent.keyCode == 25 && keyEvent.charCode == 25) {
            keyObj.keyCode = 9; //horrible Shift+Tab hack for Safari only!!!!
        }
    }

    return keyObj;
}

// Trigger the help popup given the target dom cell element for the help event
// return false if the elm is not really in the grid but in form header
GUTIL.popupHelpForGridCell = function (elm) {
    //this.dumpGlobalVariables();
    var infoObj = GUTIL.retrieveGridAttributes(elm);
    if (infoObj) {
        var gridId = infoObj.gridId;
        var realRow = infoObj.realRow;
        var colIndex = infoObj.colIndex;
        if (gridId && !isNaN(colIndex)) // if elm is gridBackDiv, we will get realRow and colIndex as NaN
        {
            if (isNaN(realRow)) // QBE does not have realRow
            {
                var objectId = JSGridCellRender.prototype.QBE_NAME_FORMAT.format(gridId, colIndex);
            }
            else // Data cell has the both realRow and colIndex
            {
                var objectId = JSGridCellRender.prototype.CELL_NAME_FORMAT.format(gridId, realRow, colIndex);
            }
            var gridObj = GCMH.findGrid(gridId);
            eval(gridObj.namespace + "hp(objectId)");
            return true;
        }
    }
    return false;
}

// for debug only, to dump the global variables to check if we do it intentionally.
GUTIL.dumpGlobalVariables = function () {
    var names = "Gloable Variables:<br>";
    for (var name in window)
        names += name + "<br>";
    GUTIL.logError(names);
}
/**
* Encapsulates common, cross-browser implementation of DOM element enablement:
* enable = "true"  --> remove 'disabled' attribute, if present
* enable = "false" --> set 'disabled' attribute to true
*/
GUTIL.setElemEnablement = function (element, enable) {
    if (enable) {
        if (this.hasAttribute(element, 'disabled')) {
            element.removeAttribute('disabled');
        }
    }
    else {
        element.setAttribute('disabled', true);
    }
}

/**
* Encapsulates common, cross-browser implementation of DOM Anchor element enablement:
* enable = "true"  --> remove 'disabled' and 'href' attribute, if present
* enable = "false" --> set 'disabled' attribute to true and 'herf'
*/
GUTIL.setArchorEnablement = function (element, enable) {
    this.setElemEnablement(element, enable)
    if (enable) {
        element.href = 'javascript:';
    }
    else {
        element.removeAttribute("href");
    }
}

GUTIL.updateClassInfo = function (element, updateClass, addClass) {
    var className = element.className;
    if (addClass) {
        if (className.indexOf(updateClass) == -1)
            element.className = className + " " + updateClass;
    }
    else {
        element.className = className.replace(updateClass, '');
    }
}

// Public method to set opacity ( from 0 to 1)
// 0 is for transparent.
GUTIL.setOpacity = function (element, opacity) {
    if (this.isIE) {
        opacity = (opacity == null) ? "" : ("alpha(opacity=" + (opacity * 100) + ")");
        element.style.filter = opacity;
    }
    else {
        opacity = (opacity == null) ? "" : opacity;
        element.style.opacity = opacity; // for firefox and new version of Safari   
        if (this.isSafari) // for Safari
        {
            element.style.KhtmlOpacity = opacity; // for old version of Safari which the customer is using
        }
    }
}

// Public method to enable/disable default html highlighting during mouse drag
// set flag to true to enable, false to disable
GUTIL.setHTMLDragSelection = function (element, flag) {
    if (this.isIE) {
        element.onselectstart = function () { return flag }
    }
    else {
        if (this.isWebKit) {
            if (element.style.webkitUserSelect == "none" && flag == true)
                element.style.webkitUserSelect = "text";
            else if (flag == false)
                element.style.webkitUserSelect = "none";
        } else {
            if (element.style.MozUserSelect == "none" && flag == true)
                element.style.MozUserSelect = "text";
            else if (flag == false)
                element.style.MozUserSelect = "none";
        }
    }
}

// Do busy wait for millis seconds - only use it for testing
GUTIL.pause = function (millis) {
    date = new Date();
    var curDate = null;

    do { var curDate = new Date(); }
    while (curDate - date < millis);
}

// Add a new cellElem into rowElem at the position of tagIndex
// we do not use inserCell API since it cause crash in IE.
GUTIL.insertCellAt = function (rowElem, tagIndex, cellElem, gridObj) {
    if (!gridObj.renderFreezeMode) {
        if (rowElem.cells.length == tagIndex) // insert at end
        {
            rowElem.appendChild(cellElem);
        }
        else // insert into the middle
        {
            rowElem.insertBefore(cellElem, rowElem.cells[tagIndex]);
        }
    }
    else {
        if (rowElem.childNodes.length == tagIndex) // insert at end
        {
            rowElem.appendChild(cellElem);
        }
        else // insert into the middle
        {
            rowElem.insertBefore(cellElem, rowElem.childNodes[tagIndex]);
        }
    }
}

//Helper method to generically set focus on cell or grid
GUTIL.setFocus = function (elem, needActive) {
    if (elem != null && elem.focus && !elem.disabled && elem.style.visibility != "hidden" && !isIOS) {
        //can only focus on visible, enabled focusable control
        this.performFocus(elem, needActive);
    }
}

GUTIL.performFocus = function (elem, needActive) {
    if (this.isIE) {
        this.performFocus = this.performFocusIE;
    }
    else if (this.isSafari) {

    }
    // Chrome and Firefox now shared the method previously applicable only to Firefox.   
    else {
        this.performFocus = this.performFocusFirefox;
    }

    return this.performFocus(elem, needActive);
}

GUTIL.performFocusIE = function (elem, needActive) {
    var gridObj;
    var isResizing;
    var infoObj = GUTIL.retrieveGridAttributes(elem);
    if (infoObj) {
        var gridId = infoObj.gridId;
        gridObj = GCMH.findGrid(gridId);
    }

    if (gridObj) {
        var inyfeObj = window["inyfeHandler" + gridObj.namespace];
        if (inyfeObj) {
            isResizing = inyfeObj.isResizing();
        }
    }

    //clear current selection, if one exists
    if (window.getSelection) {
        window.getSelection().removeAllRanges();
    }
    else if (document.selection) // IE8 or earlier
    {
        document.selection.empty();
    }

    try {
        elem.focus();
    }
    catch (e)
    { };

    if (needActive && document.activeElement != elem) {
        elem.setActive();  //strange focus events sometimes happen that prevent active
    }

    if (elem.select) {
        if (elem.disableSelection) {
            elem.disableSelection = false;
            if (elem.type != "checkbox" && elem.type != "radio") {
                if (isResizing || (document.activeElement == elem)) {
                    elem.select();
                }
            }
        }
        else {
            elem.select();
        }
        elem.disableSelection = true;
    }
}

GUTIL.performFocusFirefox = function (elem, needActive) {
    /*
    * The following code was added for Webkit processing back with the introduction of such
    * support (August 2011).  It seemingly is unncessary code.  But, surprisingly, it is completely
    * necessary.  It appears to reset the tabindex property via the tabIndex setter function.  Doing
    * so permits copy and paste operations in Webkit grid instances (when the gridback is the focus).
    */
    if (this.isWebKit && elem.tabIndex == -1) {
        elem.tabIndex = -1;
    }

    var gridObj;
    var isResizing;
    var infoObj = GUTIL.retrieveGridAttributes(elem);
    if (infoObj) {
        var gridId = infoObj.gridId;
        gridObj = GCMH.findGrid(gridId);
    }

    if (gridObj) {
        var inyfeObj = window["inyfeHandler" + gridObj.namespace];
        if (inyfeObj) {
            isResizing = inyfeObj.isResizing();
        }
    }

    //clear current selection, if one exists
    if (window.getSelection) {
        window.getSelection().removeAllRanges();
    }

    elem.focus();

    if (elem.select) {
        if (elem.disableSelection) {
            elem.disableSelection = false;
            if (elem.type != "checkbox") {
                if (isResizing)
                    elem.select();
                else if (elem.createTextRange) // it may not exist for some element even in Firefox 4
                {
                    var range = elem.createTextRange(); // Firefox only implementation
                    var selectionStart = (elem.value) ? elem.value.length : 0;
                    elem.focus();
                    elem.setSelectionRange(selectionStart, selectionStart);
                }
                else if (this.isChrome) {
                    elem.select();
                    // the try catch block is to handle any uncaught exception for html elements which does not support selectionStart or selectionEnd.
                    try {
                        // currently the html elements text, search, password, url and telephone supports selection
                        if (elem.type === 'text' ||
                            elem.type === 'search' ||
                            elem.type === 'password' ||
                            elem.type === 'url' ||
                            elem.type === 'tel') {
                            elem.selectionStart = elem.selectionEnd;
                        }
                    }
                    catch (err) {
                        //console.log(err.message);
                    }
                }
            }
        }
        else {
            elem.select();
        }
        elem.disableSelection = true;
    }
}

GUTIL.setAltAndTitle = function (elem, text) {
    elem.setAttribute('alt', text);
    elem.setAttribute('title', text);
}

// get crossbrowser pageX for the both LTR and RTL for IE and FireFox
GUTIL.getPageX = function (e) {
    if (!e || e.pageX === undefined) {
        // IE8
        return this.getPageXIE(e);
    }

    return e.pageX;
}

GUTIL.getPageXIE = function (e) {
    e = window.event;
    var elm = e.srcElement
    var x = e.offsetX;
    while (elm.offsetParent != null) {
        x += elm.offsetLeft;
        elm = elm.offsetParent;
    }
    x += elm.offsetLeft;
    return x;
}

GUTIL.addEvent = function (elem, eventType, eventFunction) {
    if (elem.addEventListener) //can use the "addEventListener" syntax
    {
        //Firefox
        this.addEvent = this.addEventWithListener;
    }
    else {
        //IE
        this.addEvent = this.addEventWithAttach;
    }

    this.addEvent(elem, eventType, eventFunction);
}

// This method is the addEvent redirection for IE only
GUTIL.addEventWithAttach = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.attachEvent('on' + eventType, eventFunction);
}

//This method is the addEvent redirection for Firefox
GUTIL.addEventWithListener = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.addEventListener(eventType, eventFunction, false); //false = capture on bubbling
}

GUTIL.removeEvent = function (elem, eventType, eventFunction) {
    if (elem.removeEventListener) //can use the "addEventListener" syntax
    {
        //Firefox
        this.removeEvent = this.removeEventWithListener;
    }
    else {
        //IE
        this.removeEvent = this.removeEventWithDetach;
    }

    this.removeEvent(elem, eventType, eventFunction);
}

// This method is the removeEvent redirection for IE
GUTIL.removeEventWithDetach = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.detachEvent('on' + eventType, eventFunction);
}

// This method is the removeEvent redirection for Firefox
GUTIL.removeEventWithListener = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.removeEventListener(eventType, eventFunction, false); //false = capture on bubbling
}

GUTIL.getEventSource = function (event) {
    if (window.event) {
        //IE has simple method to access originating element from event
        this.getEventSource = function () {
            if (window.event != undefined)
                return window.event.srcElement;
        };
    }
    else {
        //Firefox needs a slightly more complex technique
        this.getEventSource = this.getEventSourceW3C;
    }

    return this.getEventSource(event);
}

// This method is the getEventSource redirection for non-IE browsers
GUTIL.getEventSourceW3C = function (event) {
    var evtSource = event.currentTarget;

    if (evtSource && evtSource.nodeType == 3) {
        return evtSource.parentNode;
    }
    else {
        return evtSource;
    }
}

// Public utility method to recursively delete child nodes from parent
GUTIL.removeAllChildren = function (parentElem) {
    if (parentElem && parentElem.lastChild) {
        while (parentElem.lastChild.lastChild) {
            this.removeAllChildren(parentElem.lastChild);
        }
        parentElem.removeChild(parentElem.lastChild);
    }
}

// Public method to show alert error only when JSMointor.enable is true, and does nothing otherwise.
GUTIL.logError = function (errorStr) {
    JSMonitor.log("JSGrid", errorStr);
}

/********************************
The following methods are extensions to the standard javascript environment
(all browsers) that provide standardized helper functions
*********************************/
String.prototype.format = function () {
    var args = String.prototype.format.arguments;
    var fmtStr = this;

    for (var i = args.length - 1, regex; i >= 0; --i) {
        regex = new RegExp("\\{" + i + "\\}", "g");
        fmtStr = fmtStr.replace(regex, args[i]);
    }

    return fmtStr;
}

//Function to carefully round float to integer, specifically catering
//for the exact value of 0.5 to be rounded consistently (inc. negatives) 
//NOTE: "Math" is a collection of functions, not an object, and therefore
//extending it requires simply adding a function, not extending prototype
Math.roundToInt = function (num) {
    return Math.abs(num % 1) != 0.5 ? Math.round(num) :
    num > 0 ? 2 * Math.round(num / 2) : -2 * Math.round(-num / 2)
}

Array.prototype.getItem = function (n) {
    if (n != null) {
        return this[n];
    }
}

/*Gesture debugger
Can also be used to debug form.js gestures.
Adds a div at right bottom of the page and when debugLog("some message"); is called, the message is appended to innerHTML.
*/
if (gesturedebug) {
    var debugWindow = document.createElement('div');
    debugWindow.id = "gridDebug";
    debugWindow.setAttribute('style', "position:absolute; z-index:1000; overflow:auto; -webkit-overflow-scrolling:touch; width:200px;height:200px; border:solid 1px; opacity:0.6; bottom:0; right:0;");
    document.body.appendChild(debugWindow);
}

function debugLog(msg) {
    if (gesturedebug) {
        debugWindow.innerHTML += "- " + msg + "<br>";
    }
}


GCMH.getCellDataFromGridObj = function (col, infoObj, cols, i) {
    var rowDataArray;
    var cellData;
    var colObj = cols[col.colIndex];
    if (colObj) {
        rowDataArray = colObj.gridObj.gridData[infoObj.rowIndex];
    }
    if (rowDataArray) {
        cellData = rowDataArray[i];
    }
    return cellData;
}