
var WebObjectUtil;
if (WebObjectUtil == null) {
    WebObjectUtil = new function () { }
    var bUserAgent = navigator.userAgent.toLowerCase();
    WebObjectUtil.isWebkit = (bUserAgent.indexOf("webkit") > -1);
    WebObjectUtil.isIOS = (bUserAgent.indexOf("ipad") > -1) || (bUserAgent.indexOf("ipod") > -1) || (bUserAgent.indexOf("iphone") > -1);
    WebObjectUtil.isFirefox = (bUserAgent.indexOf("firefox") > -1);
    WebObjectUtil.isRTL = (document.documentElement.dir == "rtl"); // find out if it is Right to left layout
    WebObjectUtil.namespace = "";
    WebObjectUtil.DISP_PROC_IND = 1;
    WebObjectUtil.NO_DISP_PROC_IND = 0;
    WebObjectUtil.ID_PROC_IND = "procIndFloatLyr";
    WebObjectUtil.procIndStatus = 0;
    WebObjectUtil.BAD_CHAR_SET = "~`!@#$%^&*()+={[}]|\\;:'\"<,>.?/";  //some char like " can not be used in name

}

WebObjectUtil.isBlankOrEmptyName = function (name) {
    // do not allow blank or empty name
    if (name == null || name.Trim().length == 0)
        return true;
    else
        return false;
}

// make sure the name does not contain forbiden chars. return true if it is ok.
WebObjectUtil.isValidName = function (name, errMsg) {
    // do not allow to use a certain set of characters in the name
    for (var i = 0, len = WebObjectUtil.BAD_CHAR_SET.length; i < len; i++) {
        if (name.indexOf(WebObjectUtil.BAD_CHAR_SET.charAt(i)) != -1) {
            alert(errMsg);
            return false;
        }
    }
    return true;
}



// Private: Ajax send request
WebObjectUtil.sendXMLReq = function (method, containerId, action, handler, async) {
    // code for IE 7, FireFox, Safari, etc.
    if (window.XMLHttpRequest) {
        var xmlReqObject = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        var xmlReqObject = new ActiveXObject("MSXML2.XMLHTTP");
    }

    if (xmlReqObject != null) {
        xmlReqObject.open(method, action, ((async != null) && (async == false)) ? false : true);
        var handlerFunction = WebObjectUtil.getReadyStateHandler(xmlReqObject, handler, containerId);
        xmlReqObject.onreadystatechange = handlerFunction;
        xmlReqObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        //Webkit browsers through Javascript error while setting connection header
        if (!WebObjectUtil.isWebkit) {
            xmlReqObject.setRequestHeader("Connection", "close");
        }
        try {
            xmlReqObject.send(null);
        }
        catch (e) { } // SSO logout from web center may cause interaction of E1 failure
    }
}

// Private: generic callback handler for ajax
WebObjectUtil.getReadyStateHandler = function (req, responseHandler, containerId) {
    // Return an anonymous function that listens to the 
    // XMLHttpRequest instance
    return function () {
        // If the request's status is "complete"
        if (req.readyState == 4) {
            // Check that a successful server response was received
            switch (req.status) {
                case 200:
                    // Pass the XML payload of the response to the 
                    // handler function
                    if (responseHandler)
                        responseHandler(containerId, req.responseText);
                    break;

                case 122: // SSO logout already from web center but E1 is still opened
                    WebObjectUtil.timeout();
                    break;

                case 404: // possible XSS attack 
                    WebObjectUtil.timeout(RIUTIL.CONSTANTS.RI_EXIT_FOR_XSS)
                    break;

                // Safari intermittently encounters status 0 states. 
                // Bypass the alert error in the default below with a case for this 
                // state with no specific logic for it. 
                case 0:
                    break;

                default:
                    // An HTTP problem has occurred
                    alert("HTTP error: " + req.status);
            }
        }
    }
}

// Public: generic ajax request handler
WebObjectUtil.handlerAjaxRequest = function (containerId, retStr) {
    //try 
    {
        if (retStr && retStr.substring(0, 1) == "<")// when SSO logout or timeout, we may get the html login page back
            WebObjectUtil.timeout();
        else
            eval(retStr);
    }
    //catch (e)
    {
        //alert("handlerAjaxRequest failed:" + e.name + ":" + retStr + ":" + e.message);
    }
}

// Public: call by server when detect session timeout in ajax or page controler.
// refreshing the whole window will bring user back to login window.
WebObjectUtil.timeout = function (message) {
    if (message)
        alert(message);
    top.location.replace(top.location.href);
}

// Private: Ajax send request
WebObjectUtil.sendFile = function (form, method, containerId, action, handler, async) {
    // code for IE 7, FireFox, Safari, etc.
    if (window.XMLHttpRequest) {
        var xmlReqObject = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        var xmlReqObject = new ActiveXObject("MSXML2.XMLHTTP");
    }

    if (xmlReqObject != null) {
        xmlReqObject.open(method, action, ((async != null) && (async == false)) ? false : true);
        var handlerFunction = WebObjectUtil.getReadyStateHandler(xmlReqObject, handler, containerId);
        xmlReqObject.onreadystatechange = handlerFunction;
        //        var formData = new FormData(form);

        if (!WebObjectUtil.isWebkit) {
            xmlReqObject.setRequestHeader("Connection", "close");
        }
        try {
            //xmlReqObject.send(file);
            xmlReqObject.send(form);
        }
        catch (e) { } // SSO logout from web center may cause interaction of E1 failure
    }
}


WebObjectUtil.sendFileIE = function (form, method, containerId, action, handler, async) {
    var iframe = document.createElement("iframe");
    iframe.setAttribute("id", "hiddeniframe");
    iframe.setAttribute("name", "hiddeniframe");
    iframe.setAttribute("width", "0");
    iframe.setAttribute("height", "0");
    iframe.setAttribute("border", "0");
    iframe.setAttribute("style", "width: 0; height: 0; border: none;");

    // Add to document...
    form.parentNode.appendChild(iframe);
    window.frames['hiddeniframe'].name = "hiddeniframe";

    iframeId = document.getElementById("hiddeniframe");

    // Add event...
    var eventHandler = function () {
        if (iframeId != null && iframeId.parentNode != null) {
            var respDoc = null;
            // Message from server...
            if (iframeId.contentDocument) {
                respDoc = iframeId.contentDocument.body;
            }
            else if (iframeId.contentWindow) {
                respDoc = iframeId.contentWindow.document.body;
            }
            else if (iframeId.document) {
                respDoc = iframeId.document.body;
            }
            if (respDoc != null && respDoc.innerHTML != null) {
                var content = respDoc.innerHTML;
                handler("", content);
            }

            if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
            else iframeId.removeEventListener("onload", eventHandler, false);
            // Del the iframe...
            //setTimeout('iframeId.parentNode.removeChild(iframeId)', 250);
            iframeId.parentNode.removeChild(iframeId);
        }
    }

    if (iframeId.addEventListener) iframeId.addEventListener("load", eventHandler, true);
    if (iframeId.attachEvent) iframeId.attachEvent("onload", eventHandler);

    // Set properties of form...
    form.setAttribute("target", "hiddeniframe");
    form.setAttribute("action", action);
    form.setAttribute("method", "post");
    form.setAttribute("enctype", "multipart/form-data");
    form.setAttribute("encoding", "multipart/form-data");

    // Submit the form...
    form.submit();
}

WebObjectUtil.setProcessingIndicator = function (state) {
    if (state == WebObjectUtil.DISP_PROC_IND) {
        var procInd = top.document.getElementById(WebObjectUtil.ID_PROC_IND);
        if (procInd == null || procInd == undefined) {
            procInd = document.createElement("div");
            procInd.id = WebObjectUtil.ID_PROC_IND;
            procInd.className = "ProcInd";
            procInd.style.display = "block";
            procInd.style.position = "absolute";
            procInd.style.top = "20px";
            procInd.style.right = "65px";
            procInd.style.height = "30px";
            procInd.style.zIndex = 2000000000;
            procInd.noWrap = "true";
            procInd.innerHTML = PSStringBuffer.concat("<table cellpadding=0 cellspacing=0 border=0><tr nowrap>",
                     "<td class='af_progressIndicator_indeterminate'>",
                     "</td>",
                     "</tr></table>");
        }
        var sidePanel = top.document.getElementById("E1PSidePanel");
        sidePanel.parentNode.appendChild(procInd);
        WebObjectUtil.procIndStatus = WebObjectUtil.DISP_PROC_IND;
    }
    else {
        var procInd = top.document.getElementById(WebObjectUtil.ID_PROC_IND);
        if (WebObjectUtil.procIndStatus == WebObjectUtil.DISP_PROC_IND && procInd != null && procInd != undefined)
            procInd.parentNode.removeChild(procInd);
    }
}

WebObjectUtil.setProcInd = function (namespace) {
    WebObjectUtil.setProcessingIndicator(WebObjectUtil.DISP_PROC_IND);
}

WebObjectUtil.clearProcInd = function (namespace) {
    WebObjectUtil.setProcessingIndicator(WebObjectUtil.NO_DISP_PROC_IND);
}

WebObjectUtil.getElementByAttribute = function (attr, value, root) {
    root = root || document.body;
    if ((root.getAttribute(attr) != undefined) && root.getAttribute(attr) == value) {
        return root;
    }
    var children = root.children,
        element;
    for (var i = children.length; i--; ) {
        element = WebObjectUtil.getElementByAttribute(attr, value, children[i]);
        if (element) {
            return element;
        }
    }
    return null;
}


WebObjectUtil.jsonPath = function (obj, expr, arg) {
    var P = {
        resultType: arg && arg.resultType || "VALUE",
        result: [],
        normalize: function (expr) {
            var subx = [];
            return expr.replace(/[\['](\??\(.*?\))[\]']/g, function ($0, $1) { return "[#" + (subx.push($1) - 1) + "]"; })
                    .replace(/'?\.'?|\['?/g, ";")
                    .replace(/;;;|;;/g, ";..;")
                    .replace(/;$|'?\]|'$/g, "")
                    .replace(/#([0-9]+)/g, function ($0, $1) { return subx[$1]; });
        },
        asPath: function (path) {
            var x = path.split(";"), p = "$";
            for (var i = 1, n = x.length; i < n; i++)
                p += /^[0-9*]+$/.test(x[i]) ? ("[" + x[i] + "]") : ("['" + x[i] + "']");
            return p;
        },
        store: function (p, v) {
            if (p) P.result[P.result.length] = P.resultType == "PATH" ? P.asPath(p) : v;
            return !!p;
        },
        trace: function (expr, val, path) {
            if (expr) {
                var x = expr.split(";"), loc = x.shift();
                x = x.join(";");
                if (val && val.hasOwnProperty(loc))
                    P.trace(x, val[loc], path + ";" + loc);
                else if (loc === "*")
                    P.walk(loc, x, val, path, function (m, l, x, v, p) { P.trace(m + ";" + x, v, p); });
                else if (loc === "..") {
                    P.trace(x, val, path);
                    P.walk(loc, x, val, path, function (m, l, x, v, p) { typeof v[m] === "object" && P.trace("..;" + x, v[m], p + ";" + m); });
                }
                else if (/,/.test(loc)) { // [name1,name2,...]
                    for (var s = loc.split(/'?,'?/), i = 0, n = s.length; i < n; i++)
                        P.trace(s[i] + ";" + x, val, path);
                }
                else if (/^\(.*?\)$/.test(loc)) // [(expr)]
                    P.trace(P.eval(loc, val, path.substr(path.lastIndexOf(";") + 1)) + ";" + x, val, path);
                else if (/^\?\(.*?\)$/.test(loc)) // [?(expr)]
                    P.walk(loc, x, val, path, function (m, l, x, v, p) { if (P.eval(l.replace(/^\?\((.*?)\)$/, "$1"), v[m], m)) P.trace(m + ";" + x, v, p); });
                else if (/^(-?[0-9]*):(-?[0-9]*):?([0-9]*)$/.test(loc)) // [start:end:step]  phyton slice syntax
                    P.slice(loc, x, val, path);
            }
            else
                P.store(path, val);
        },
        walk: function (loc, expr, val, path, f) {
            if (val instanceof Array) {
                for (var i = 0, n = val.length; i < n; i++)
                    if (i in val)
                        f(i, loc, expr, val, path);
            }
            else if (typeof val === "object") {
                for (var m in val)
                    if (val.hasOwnProperty(m))
                        f(m, loc, expr, val, path);
            }
        },
        slice: function (loc, expr, val, path) {
            if (val instanceof Array) {
                var len = val.length, start = 0, end = len, step = 1;
                loc.replace(/^(-?[0-9]*):(-?[0-9]*):?(-?[0-9]*)$/g, function ($0, $1, $2, $3) { start = parseInt($1 || start); end = parseInt($2 || end); step = parseInt($3 || step); });
                start = (start < 0) ? Math.max(0, start + len) : Math.min(len, start);
                end = (end < 0) ? Math.max(0, end + len) : Math.min(len, end);
                for (var i = start; i < end; i += step)
                    P.trace(i + ";" + expr, val, path);
            }
        },
        eval: function (x, _v, _vname) {
            try { return $ && _v && eval(x.replace(/@/g, "_v")); }
            catch (e) { throw new SyntaxError("jsonPath: " + e.message + ": " + x.replace(/@/g, "_v").replace(/\^/g, "_a")); }
        }
    };

    var $ = obj;
    if (expr && obj && (P.resultType == "VALUE" || P.resultType == "PATH")) {
        P.trace(P.normalize(expr).replace(/^\$;/, ""), obj, "$");
        return P.result.length ? P.result : false;
    }
}


WebObjectUtil.sortObjectByValue = function (p, obj) {
    return obj.slice(0).sort(function (a, b) {
        return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
    });
}


WebObjectUtil.putCursorAtEnd = function (parameter) {
    return parameter.each(function () {

        // Cache references
        var $el = $(this),
        el = this;

        // Only focus if input isn't already
        if (!$el.is(":focus")) {
            $el.focus();
        }

        // If this function exists... (IE 9+)
        if (el.setSelectionRange) {

            // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
            var len = $el.val().length * 2;

            // Timeout seems to be required for Blink
            setTimeout(function () {
                el.setSelectionRange(len, len);
            }, 1);

        } else {

            // As a fallback, replace the contents with itself
            // Doesn't work in Chrome, but Chrome supports setSelectionRange
            $el.val($el.val());

        }

        // Scroll to the bottom, in case we're in a tall textarea
        // (Necessary for Firefox and Chrome)
        this.scrollTop = 999999;

    });

};

// Numeric only control handler
WebObjectUtil.ForceNumericOnly = function (inputBox) {
    return inputBox.each(function () {
        $(inputBox).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end and numpad decimal
            return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                (key === 65 && (key === true || e.metaKey === true)) ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};