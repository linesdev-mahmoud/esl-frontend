// voiceCommands - Javascript code for Voice Commands Functionality
if (!VC) {
    var VC = new Object();
    VC.DVCList = null;
}

VC.loadDVCList = function () {
    VC.DVCList = { command:
                [
                    { "vName": VC.SELECT, "elemId": "hc_Select" },
                    { "vName": VC.FIND, "elemId": "hc_Find" },
                    { "vName": VC.ADD, "elemId": "hc_Add" },
                    { "vName": VC.CANCEL, "elemId": ["hc_Cancel", "hc_Close", "hc2", "BUTTONCANCEL"] },
                    { "vName": VC.CLOSE, "elemId": ["hc_Close", "hc_Cancel", "hc2", "BUTTONCANCEL"] },
                    { "vName": VC.OK, "elemId": ["hc_OK", "hc1", "BUTTONSAVEEXIT"] },
                    { "vName": VC.SAVE, "elemId": ["hc_OK", "hc1", "BUTTONSAVEEXIT"] },
                    { "vName": VC.DELETE, "elemId": ["hc_Delete", "BUTTONDELETE"] },
                    { "vName": VC.BACK, "elemId": "BUTTONSAVEEXIT" },
                    { "vName": VC.CONTINUE, "elemId": "hc1" },
                    { "vName": VC.COPY, "elemId": "hc_Copy" },
                    { "vName": VC.FORM_EXIT, "elemId": "FORM_EXIT_BUTTON" },
                    { "vName": VC.ROW_EXIT, "elemId": "ROW_EXIT_BUTTON" },
                    { "vName": VC.LINE_EXIT, "elemId": "ROW_EXIT_BUTTON" },
                    { "vName": VC.REPORT_EXIT, "elemId": "REPORT_EXIT_BUTTON" },
                    { "vName": VC.TOOLS_EXIT, "elemId": "TOOLS_EXIT_BUTTON" },
                    { "vName": VC.ONE_VIEW, "elemId": "BIP_EXIT_BUTTON" },
                    { "vName": VC.EXPORT, "elemId": "jdehtmlExportData" },
                    { "vName": VC.XSPORT, "elemId": "jdehtmlExportData" },
                    { "vName": VC.IMPORT, "elemId": "jdehtmlImportData" },
                    { "vName": VC.SIGN_OUT, "elemId": "e1LogoutLink" },
                    { "vName": VC.MANAGE_QUERY, "elemId": "AQDesignIcon" },
                    { "vName": VC.CLOSE_QUERY, "elemId": "SidePanelClose" },
                    { "vName": VC.MANAGE_WATCH_LIST, "elemId": "WLDesignIcon" },
                    { "vName": VC.CLOSE_WATCH_LIST, "elemId": "SidePanelClose" },
                    { "vName": VC.NEXT_RECORD, "elemId": "hc_Next" },
                    { "vName": VC.PREVIOUS_RECORD, "elemId": "hc_Previous" },
                    { "vName": VC.NEXT_PAGE, "elemId": "NEXT0_1" },
                    { "vName": VC.GO_TO_END, "elemId": "GOTOLAST0_1" },
                    { "vName": VC.ADD_TEXT, "elemId": "BUTTONADDTEXT" },
                    { "vName": VC.ADD_FILE, "elemId": "BUTTONADDIMAGE" },
                    { "vName": VC.ADD_URL, "elemId": "BUTTONADDURL" },
                    { "vName": VC.NEW_TEMPLATE, "elemId": "BUTTONTEMPLATE" },
                    { "vName": VC.HOME, "elemId": "drop_home" },
                    { "vName": VC.NAVIGATOR, "elemId": "drop_mainmenu" },
                    { "vName": VC.OPEN_APPLICATIONS, "elemId": "drop_openapps" },
                    { "vName": VC.RECENT_REPORTS, "elemId": "drop_reportmenu" },
                    { "vName": VC.FAVORITES, "elemId": "drop_fav_menus" },
                    { "vName": VC.WATCH_LIST, "elemId": "drop_watchlist_menus" },
                    { "vName": VC.SYSTEM_INFORMATION, "elemId": "MainPanelAbout" },
                    { "vName": VC.SUBMIT, "elemId": "hc0" },
                    { "vName": VC.HOT_KEYS, "elemId": "" },
                    { "vName": VC.NEW_WINDOW, "elemId": "" },
                    { "vName": VC.CLICK, "elemId": "" },
                    { "vName": VC.STOP, "elemId": "STOP" },
                    { "vName": VC.CHECK, "elemId": "" },
                    { "vName": VC.UNCHECK, "elemId": "" }
               ]
    };
}

VC.getContentDoc = function () {
    VC.e1MenuAppIframe = document.getElementById("e1menuAppIframe");
    if (VC.e1MenuAppIframe.contentDocument != null) {
        return VC.e1MenuAppIframe.contentDocument;
    }
    else {
        return VC.e1MenuAppIframe.contentWindow.document;
    }
}

VC.loadControls = function () {
    VC.ContentDoc = VC.getContentDoc();
    VC.inputElems = VC.ContentDoc.getElementsByTagName("INPUT");
    VC.comboBoxElements = VC.ContentDoc.getElementsByTagName("SELECT");
    VC.buttonTagElements = VC.ContentDoc.getElementsByTagName("BUTTON");
    VC.e1Form = VC.ContentDoc.getElementById("FormAboveGrid");
    VC.imageElements = VC.ContentDoc.images;
    VC.anchorElements = VC.ContentDoc.anchors;

    VC.buttonAndImageElements = [];
    VC.textElements = [];
    VC.radioElements = [];
    VC.checkBoxElements = [];
    VC.allInputElements = [];
    var vInputElementsLength = VC.inputElems.length;
    var vImageElementsLength = VC.imageElements.length;

    for (i = 0; i < vInputElementsLength; i++) {
        if (VC.inputElems[i].type == "text") {
            VC.textElements.add(VC.inputElems[i]);
            VC.allInputElements.add(VC.inputElems[i]);
        }
        else if (VC.inputElems[i].type == "button" || VC.inputElems[i].type == "submit" || VC.inputElems[i].type == "reset") {
            VC.buttonAndImageElements.add(VC.inputElems[i]);
            VC.allInputElements.add(VC.inputElems[i]);
        }
        else if (VC.inputElems[i].type == "checkbox") {
            VC.checkBoxElements.add(VC.inputElems[i]);
            VC.allInputElements.add(VC.inputElems[i]);
        }
        else if (VC.inputElems[i].type == "radio") {
            VC.radioElements.add(VC.inputElems[i]);
            VC.allInputElements.add(VC.inputElems[i]);
        }
    }

    for (i = 0; i < vImageElementsLength; i++) {
        if (VC.imageElements[i].parentNode.tagName == "A" && VC.imageElements[i].title != "") {
            VC.buttonAndImageElements.add(VC.imageElements[i]);
            VC.allInputElements.add(VC.imageElements[i]);
        }
    }

    if (VC.buttonTagElements.length > 0) {
        VC.buttonAndImageElements.addAll(VC.buttonTagElements);
        VC.allInputElements.addAll(VC.buttonTagElements);
    }

    if (VC.comboBoxElements.length > 0) {
        VC.allInputElements.addAll(VC.comboBoxElements);
    }
    // make the VC.ContentDoc reference to null
    VC.ContentDoc = null;
}

VC.processVC = function () {
    var vCommandElem = document.getElementById('vtext');
    var vInput = vCommandElem.value;
    if (vInput.length > 0) {
        VC.processDVC(vInput); // process Default Voice Commands
    }
}

// Function to process the Default Voice Commands
VC.processDVC = function (vInput) {
    VC.loadControls();
    var firstSpaceIndex = vInput.indexOf(' ');
    var vInputInLower = vInput.toLowerCase();
    var vInputArray = vInputInLower.split(" ");
    var vInputArrayLength = vInputArray.length;
    VC.vCmd = null;
    VC.cmdParam = null;
    if (firstSpaceIndex != -1) {
        VC.vCmd = vInputInLower.substr(0, firstSpaceIndex);
        VC.cmdParamActual = vInput.substr(firstSpaceIndex + 1);
        VC.cmdParam = VC.cmdParamActual.toLowerCase();
    }
    else {
        VC.processMiscCmds(vInputInLower);
        return;
    }

    switch (VC.vCmd) {
        case VC.FOCUS:
            {
                if (VC.cmdParam == VC.GRID) {
                    VC.processMiscCmds(vInputInLower);
                }
                else {
                    VC.currActiveElem = VC.getElementByTitle(VC.allInputElements);
                    if (VC.currActiveElem != null) {
                        VC.currActiveElem.focus();
                    }
                }
                break;
            }
        case VC.TYPE:
            {
                if (VC.currActiveElem != null) {
                    if (VC.currActiveElem.type == 'text') {
                        VC.currActiveElem.value = VC.cmdParamActual;
                    }
                }
                break;
            }
        case VC.CHECK:
        case VC.UNCHECK:
            {
                VC.currActiveElem = VC.getElementByTitle(VC.checkBoxElements);
                if (VC.currActiveElem != null && VC.currActiveElem.type == 'checkbox') {
                    if ((VC.vCmd == VC.CHECK && !VC.currActiveElem.checked) || (VC.vCmd == VC.UNCHECK && VC.currActiveElem.checked)) {
                        VC.triggerEvent('mouse', VC.currActiveElem, 'click');
                    }
                }
                break;
            }
        case VC.LAUNCH: //for fastpath xxxx command
            {
                VC.doFastPath(VC.cmdParam);
                break;
            }
        case VC.CLICK:
            {
                var currElem = VC.getElementByTitle(VC.buttonAndImageElements);
                if (currElem != null) {
                    currElem.click();
                }
                break;
            }
        case VC.OPEN:
            {
                if (vInputArrayLength > 1) {
                    if ((vInputArray[1] == VC.TAB) && (vInputArray[2] != null)) {
                        var currElem = VC.getTabElement(vInputArray[2]);
                        if (currElem != null) {
                            currElem.click();
                        }
                    }
                    else {
                        VC.processMiscCmds(vInputInLower)
                    }
                }
                break;
            }
        default:
            {
                VC.processMiscCmds(vInputInLower);
            }
    }
}

VC.processMiscCmds = function (vCommand) {
    VC.currCommand = VC.getCommandDetails(vCommand);
    if (VC.currCommand == null) {
        document.getElementById('unknownCmd').play();  //Play the unrecognized command audio if the command is not available in the system
        return;
    }

    var cmdName = VC.currCommand.vName;
    var elem = null;
    VC.elementId = VC.currCommand.elemId;
    switch (cmdName) {
        case VC.HOT_KEYS:
            {
                window["LaunchHotKeysPage"]();
                break;
            }
        case VC.NEW_WINDOW:
            {
                launchNewWindow();
                break;
            }
        case VC.HOME:
            {
                goHome();
                break;
            }
        case VC.NAVIGATOR:
            {
                showMenu('e1MMenuRoot', 'click');
                break;
            }
        case VC.OPEN_APPLICATIONS:
            {
                showMenu('OPENAPPTABLE', 'click');
                break;
            }
        case VC.RECENT_REPORTS:
            {
                showRecRptsMenu('recRptsDiv', 'click');
                break;
            }
        case VC.FAVORITES:
            {
                showMenu('e1MMenuFav', 'click');
                break;
            }
        case VC.WATCH_LIST:
            {
                showMenu('e1MMenuWatchlists', 'click');
                break;
            }
        case VC.CLICK:
            {
                if (VC.currActiveElem != null && (VC.currActiveElem.type == "checkbox" || VC.currActiveElem.type == "radio")) {
                    VC.triggerEvent('mouse', VC.currActiveElem, 'click');
                }
                break;
            }
        case VC.CHECK:
        case VC.UNCHECK:
            {
                if (VC.currActiveElem != null && (VC.currActiveElem.type == 'checkbox' || VC.currActiveElem.type == 'radio')) {
                    if ((cmdName == VC.CHECK && !VC.currActiveElem.checked) || (cmdName == VC.UNCHECK && VC.currActiveElem.checked)) {
                        VC.triggerEvent('mouse', VC.currActiveElem, 'click');
                    }
                }
                break;
            }
        default:
            {
                if (VC.elementId instanceof Array) {
                    VC.elementIdLength = VC.elementId.length;
                    elem = VC.getElementFromElementId();
                }
                else {
                    elem = VC.getContentDoc().getElementById(VC.elementId);
                    if (elem == null) {
                        elem = document.getElementById(VC.elementId);
                    }
                }
                if (elem != null) {
                    // next and goto end
                    if (cmdName == VC.NEXT_PAGE || cmdName == VC.GO_TO_END) {
                        elem.firstChild.click();
                    }
                    else {
                        elem.click();
                    }
                }
            }
    }
}

VC.getElementFromElementId = function () {
    var element = null;
    for (i = 0; i < VC.elementIdLength; i++) {
        element = VC.getContentDoc().getElementById(VC.elementId[i]);
        if (element == null) {
            element = document.getElementById(VC.elementId[i]);
        }
        if (element != null) {
            return element;
        }
    }
    return element;
}

VC.triggerEvent = function (eventType, targetElem, action, ctrlKeyPressed, shiftKeypressed, eventKeyCode, updateActiveElem) {
    switch (eventType) {
        case "mouse":
            {
                var custEvent = new MouseEvent(action);
                targetElem.dispatchEvent(custEvent);
                custEvent.stopPropagation();
                break;
            }
    }
}

VC.getElementByTitle = function (vInputElement) {
    var currElem = null;
    var vInputElementLenght = vInputElement.length;
    for (i = 0; i < vInputElementLenght; i++) {
        if ((vInputElement[i].title).trim().toLowerCase() == VC.cmdParam) {
            currElem = vInputElement[i];
            return currElem;
        }
        else if (vInputElement[i].tagName == "BUTTON" || vInputElement[i].type == "button"
                 || vInputElement[i].type == "submit" || vInputElement[i].type == "reset") {
            if ((vInputElement[i].textContent).trim().toLowerCase() == VC.cmdParam
               || (vInputElement[i].value).trim().toLowerCase() == VC.cmdParam) {
                currElem = vInputElement[i];
                return currElem;
            }
        }
    }
    return currElem;
}

VC.getTabElement = function (tabNum) {
    var currElem = null;
    var currentTabNum = null;
    var tabElements = [];
    if (VC.e1Form != null) {
        var allTablinkElements = VC.e1Form.getElementsByClassName("tablink");
        var listOfAllTabLinkElements = allTablinkElements.length;
        for (i = 0; i < listOfAllTabLinkElements; i++) {
            if (allTablinkElements[i].tagName == "A") {
                tabElements.add(allTablinkElements[i]);
            }
        }
    }
    var vTabElementsLength = tabElements.length;
    for (i = 0; i < vTabElementsLength; i++) {
        currentTabNum = tabElements[i].name;
        var lastIndex = currentTabNum.lastIndexOf(".");
        if (lastIndex != -1) {
            if (currentTabNum.charAt(lastIndex + 1) == tabNum - 1) {
                currElem = tabElements[i];
                return currElem;
            }
        }
    }
    return currElem;
}

VC.getCommandDetails = function (vInput) {
    var currCommand = null;
    var vCommandsLength = VC.DVCList.command.length;
    for (i = 0; i < vCommandsLength; i++) {
        if (VC.DVCList.command[i].vName.toLowerCase() == vInput) {
            currCommand = VC.DVCList.command[i];
            return currCommand;
        }
    }
    return currCommand;
}

VC.doFastPath = function (vfpAppName) {
    var elem = document.getElementById("TE_FAST_PATH_BOX");
    if (elem != null) {
        elem.value = vfpAppName;
        document.getElementById('e1MFastpathForm').submit();
    }
}