/*
externalAppsHandler
*/

var ExternalAppsHandler = new function () {
    //Tracekr for currently open ADF apps. Uses instanceId(stackId on JAS) as key. Stores [mafLoopURL:, mafCloseURL:, namespace:]
    this.openADFApps = {};

    //JAS Target for External App action
    this.externAppAction = null;

    //External App Commands for JAS
    this.CMD_CLOSE_EXTERNAL_APP = "clsextapp";

    //External App Update Events
    this.REFRESH = 1;

    this.EXTERNAL_APP_APPTYPE = "ADFAPP";

    this.setExternalAppAction = function (action) {
        this.externAppAction = action;
    }

    this.addADFOpenApp = function (instanceId, mafLoopURL, mafCloseURL, namespace) {
        ExternalAppsHandler.openADFApps[instanceId] = { "mafLoopURL": mafLoopURL, "mafCloseURL": mafCloseURL, "namespace": namespace };
    }

    this.getGlobalRID = function () {
        var RID = document.getElementsByName("globalRID");
        if (RID.length > 0) {
            RID = RID[0].value;
            return RID;
        }
        return ""; //RID not found
    }

    this.closeExternalApplication = function (instanceId, stateId) {
        this.postExternalAppEvent(this.CMD_CLOSE_EXTERNAL_APP, instanceId, stateId);
    }

    this.postExternalAppEvent = function (cmd, instanceId, stateId) {
        var postDoc = new Array();
        postDoc.push("jdemafjasLinkTarget=");
        postDoc.push(self.name);
        postDoc.push("&RID=");
        postDoc.push(this.getGlobalRID());
        postDoc.push("&stackId=");
        postDoc.push(instanceId);
        postDoc.push("&");
        postDoc.push("guiChangeOnly=1");

        //pass the state id if it was passed in
        if (stateId != null && stateId != "null") {
            postDoc.push("&stateId=");
            postDoc.push(stateId);
        }
        else {
            postDoc.push("&stateId=0");
        }
        postDoc.push("&cmd=");
        postDoc.push(window.encodeURIComponent(cmd));
        var requestData = postDoc.join("");
        this.sendXMLReq("POST", this.externAppAction, requestData, instanceId);

        this.showE1MenuAppIframe();
    }

    this.sendXMLReq = function (method, action, doc, instanceId) {
        var xmlReqObject = new XMLHttpRequest();

        if (xmlReqObject != null) {
            xmlReqObject.open(method, action, true);
            var handlerFunction = this.getReadyStateHandler(xmlReqObject, ExternalAppsHandler.processExternalAppUpdateEvent, instanceId); //function(response){console.log(response)};
            xmlReqObject.onreadystatechange = handlerFunction;
            xmlReqObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            try {
                xmlReqObject.send(doc);
            }
            catch (ex) {
                //TODO: Refer JDEDTA for this part
            }
        }
    }

    this.getReadyStateHandler = function (req, responseXmlHandler, instanceId) {
        return function () {
            if (req.readyState == 4) {
                responseXmlHandler(req.responseText, req, instanceId);
            }
        }
    }

    this.processExternalAppUpdateEvent = function (response, req, instanceId) {
        var events = eval(response);
        if (events == null || events.length == 0) {
            return;
        }
        for (var i = 0; i < events.length; i++) {
            var e = events[i],
            eventType = e.EVT;
            switch (eventType) {
                case ExternalAppsHandler.REFRESH:
                    if (e.removeSB == "true") {
                        var sbFrame = document.getElementById('sbFrame');
                        if (sbFrame != null) {
                            top.cleanUpInlineHeightWidthForIFrame();
                        }
                    }

                    var divtemp = document.createElement("DIV");
                    var docUsed = document.getElementById("e1menuAppIframe").contentWindow.document;
                    docUsed.body.appendChild(divtemp);
                    var instanceInfo = ExternalAppsHandler.openADFApps[instanceId],
                    namespace = instanceInfo["namespace"],
                    loopURL = instanceInfo["mafLoopURL"],
                    closeURL = instanceInfo["mafCloseURL"];
                    if (loopURL != undefined) {
                        e.REFRESHFORM = e.REFRESHFORM.replace('{loopurl}', loopURL);
                    }
                    if (closeURL != undefined) {
                        e.REFRESHFORM = e.REFRESHFORM.replace('{closeurl}', closeURL);
                    }
                    divtemp.innerHTML = e.REFRESHFORM;

                    try {
                        f1 = docUsed.getElementById("MafRefreshForm");
                    }
                    catch (ex) {
                        alert("error name = " + ex.name + ", error message = " + ex.message);
                    }

                    if (window.adfpp_formSub) {
                        f1.submit = window.adfpp_formSub;
                    }
                    if (window.adfpp_handleLocationChange)
                        f1.portalContainerType.value = "WebCenter";
                    else
                        f1.portalContainerType.value = "Non-WebCenter";
                    f1.RID.value = ExternalAppsHandler.getGlobalRID();
                    f1.submit();
                    break;

                default:
                    break;
            }
        }
    }

    // This function subordinates the display of the e1ExternalAppIframe to that of the e1menuAppIframe.
    this.showE1MenuAppIframe = function () {
        this.showFrame(document.getElementById("e1menuAppIframe"));
        var innerRCUX = document.getElementById("innerRCUX");
        innerRCUX.style.overflow = "auto";
        var externalAppFrame = document.getElementById("e1ExternalAppIframe");
        if (externalAppFrame) {
            var hideOccurred = this.hideFrame(externalAppFrame);

            // There's an issue with the external app iFrame flashing when its source value
            // changes. As a workaround solution, we do an intermediate reset of that src
            // prior to updating it to its new value.
            if (hideOccurred == true) {
                externalAppFrame.src = "";
            }
        }
    }

    /*
    * This function subordinates the display of the e1menuAppIframe to that of the e1ExternalAppIframe.
    * Note that the iFrame will have a initial display style of 'none' with HTML5 that we account for
    * with doExternalAppLaunch() below.
    */
    this.showE1ExternalAppIframe = function () {
        this.showFrame(document.getElementById("e1ExternalAppIframe"));
        this.hideFrame(document.getElementById("e1menuAppIframe"));

        var innerRCUX = document.getElementById("innerRCUX");
        innerRCUX.style.overflow = "hidden";
        // hide dropdown menu for edit layout or manage page for independent ADF App
        top.RIUTIL.cleanupPreferenceMenu();
    }

    /*
    * For the hiding/showing of frames, we rely on the hidden attribute for most browsers.  Touch devices at times
    * and IE10 always will also use the display style attribute.  The visibility attribute is not in play.
    *
    * As we hide frames, we'll save off the value of the display property prior to the hide.  Should we later show the
    * frame, we'll restore that saved off value.
    */
    this.hideFrame = function (frame) {
        var hideOccurred = false;
        if (frame) {
            var isHidden = frame.getAttribute('hidden');
            if (isHidden == null || (isHidden && isHidden != "true"))  // Hide it if it's not already hidden.
            {
                frame.setAttribute('hidden', true);
                hideOccurred = true;
            }

            if (frame.style) {
                var currentDisplayValue = frame.style.display;
                /*
                * We won't force the use of the display style unless we're IE10 (and "hidden" is not supported) or a
                * display style was already used.
                */
                if (isIE10 || (currentDisplayValue && currentDisplayValue.length > 0)) // Hide it if it's not already hidden.
                {
                    if (currentDisplayValue != "none") {
                        // Tweak the existing style attribute.           
                        frame.style.display = 'none';
                        frame.setAttribute('lastDisplayValue', currentDisplayValue);
                        hideOccurred = true;
                    }
                }
            }
        }

        return hideOccurred;
    }

    this.showFrame = function (frame) {
        if (frame) {
            var isHidden = frame.getAttribute('hidden');
            if (isHidden && isHidden == "true") // Show it if it's not already shown.
            {
                frame.removeAttribute('hidden');
            }

            if (frame.style) {
                var currentDisplayValue = frame.style.display;

                if (currentDisplayValue && currentDisplayValue == "none") // Show it if it's not already shown.
                {
                    var lastDisplayValue = frame.getAttribute('lastDisplayValue');
                    if (lastDisplayValue) {
                        frame.style.display = lastDisplayValue;
                        frame.removeAttribute('lastDisplayValue');
                    }
                    else {
                        frame.style.display = '';
                    }
                }
            }
        }
    }

    this.doExternalAppLaunch = function (launchUrl) {
        this.doPreLaunchCleanup();

        // Launch with hide/show of e1menuAppIframe/e1ExternalAppIframe.
        var theE1ExternalAppIframe = document.getElementById('e1ExternalAppIframe');
        if (theE1ExternalAppIframe) {
            if (!document.all && theE1ExternalAppIframe.style.display) // Init of iFrame may have assigned a hidden style regardless of HTML5.
            {
                theE1ExternalAppIframe.style.display = '';
            }

            this.showE1ExternalAppIframe();
            var newSrc = launchUrl + "&jasServer=" + window.location.origin;
            theE1ExternalAppIframe.src = newSrc;
        }
    }


    this.isAnExternalAppInstance = function (instanceId) {
        var isAnExternalApp = false;

        if (instanceId && this.openADFApps && this.openADFApps[instanceId] != undefined) {
            isAnExternalApp = true;
        }
        return isAnExternalApp;
    }

    this.doPreLaunchCleanup = function () {
        // Pre-launch cleanup
        if (OpenAppIndicator.getInstance() != null) {
            OpenAppIndicator.stopOpenAppIndicator();
        }
        if (ProcessingIndicator.getInstance() != null) {
            ProcessingIndicator.stopProcessingIndicator();
        }
    }
}