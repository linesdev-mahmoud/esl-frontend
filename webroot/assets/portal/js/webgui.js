﻿
//////////////////////////////////////////////////////////////////////
// COPYRIGHT (C) 1999-2004 PeopleSoft(R), Inc.  All Rights Reserved //
//////////////////////////////////////////////////////////////////////
// We will stop render of portlets as soon as we detect there are portlets from more than one release on the same page.
var userAgent = navigator.userAgent.toUpperCase();
var isSafari = (userAgent.indexOf("SAFARI") != -1);
var isFirefox = (userAgent.indexOf("GECKO/") > -1) || (userAgent.indexOf("FIREFOX") > -1);
var isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
var isAndroid = userAgent.indexOf("ANDROID") > -1;
var isTouchEnabled = (isIOS == true) || (isAndroid == true);
var isIE = (document.documentMode !== undefined);
var isEdge = (userAgent.indexOf("EDGE") > -1); //for handling edge browser
var isIE8OrEarlier = (isIE && document.documentMode <= 8);
var isIE10 = (isIE && document.documentMode == 10);
var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
var testStyle = document.documentElement.style;
var supportTransform = testStyle.webkitTransform !== undefined || testStyle.MozTransform !== undefined || testStyle.OTransform !== undefined || testStyle.MsTransform !== undefined || testStyle.transform !== undefined

// code for auto scroll begins 
var favIntervalScroll = 0;
var iScrollFactor = 10;
var dragElementMenuID;
//code for auto scroll ends

//Global array to store the MaskFrame
var maskFrameStack = new Array();

//endsWith not supported until version 12 in IE 
if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function (str) {
        return str.length > 0 && this.substring(this.length - str.length, this.length) === str;
    };
}

function setAndValidateVersion(currentVersion) {

    if (window.storedVersion && window.storedVersion != currentVersion) {
        var message = "Portlets from different releases are not supported on the same page. Please contact your administrator to put these portlets in separate pages.";
        alert(message);
        if (isIE) // stop script processing in IE
            document.execCommand('Stop');
        else {
            // stop script processing in FF. We have to clear the inner HTML since
            // otherwise Firefox by default renders all portlets from the first release
            // and stops only at the first portlet of the second release.
            document.body.innerHTML = "";
            window.stop();

        }
    }
    window.storedVersion = currentVersion;
}
setAndValidateVersion("9.1.4");

/*This function is used to add the MaskFrame to the
maskFrameStack Array*/
function addmaskFrameStack(maskFrame) {
    //update maskFrameStack Array
    maskFrameStack.push(maskFrame);
}

/*This function removes the MaskFrame from the maskFrameStack Array
and call removeItemShadow to detach the MaskFrame for the Elem*/
function removeMaskFrame(floatDiv, omitShadow) {
    //remove the mask Frame in IE
    if (isIE && !isWSRPContainer()) {
        var maskFrameId = floatDiv.id + "-maskframe";
        for (k = maskFrameStack.length - 1; k >= 0; k--) {
            if (maskFrameStack[k].defaultId == maskFrameId) {
                removeItemShadow(floatDiv, maskFrameStack[k], omitShadow);
                maskFrameStack.splice(k, 1);
                break;
            }
        }
    }
}

/* This function checks is the Container is WSRP or Not*/
function isWSRPContainer() {
    if (typeof gContainerId != 'undefined') {
        var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);
        return isWSRP;
    }
    return false;
}

var webguiDebug = false;
function webguiDebugToggle() {
    webguiDebug = !webguiDebug;
    if (webguiDebug)
        alert("WebGUI debug is enabled.");
    else
        alert("WebGUI debug is disabled.");
}

///////////////////////////////////////////////////////////////////////
// START OF SCRIPTS FOR COLLAPSIBLE TREES

function jdeWebGuiToggleTreeNode(domObjectID, toggleType/*unused*/, overrideExpandedURL, overrideCollapsedURL) {
    var theToggledObject = document.getElementById(domObjectID);
    var theToggleButton = document.getElementById(domObjectID + "_BUTTON");
    if (theToggledObject.style.display == "none") {
        theToggledObject.style.display = "block";
        if (overrideExpandedURL)
            theToggleButton.src = overrideExpandedURL;
        else
            theToggleButton.src = window["WEBGUIRES_images_treeexpanded_gif"];
    }
    else {
        theToggledObject.style.display = "none";
        if (overrideCollapsedURL)
            theToggleButton.src = overrideCollapsedURL;
        else {
            var treeCollapsedImg = window["WEBGUIRES_images_treecollapsed_gif"];
            if (document.documentElement.dir == "rtl")
                treeCollapsedImg = window["WEBGUIRES_images_treecollapsed_rtl_gif"];
            theToggleButton.src = treeCollapsedImg;
        }
    }
}

// END OF SCRIPTS FOR COLLAPSIBLE TREES
//////////////////////////////////
// START OF SCRIPTS FOR INTERACTIVE TREES

self.webGuiInteractiveTree = new function () {
    this.isFocusableElement = function (node) {
        return ((node != null) && node.getAttribute && (node.getAttribute("arrownav") == "yes"));
    };
    this.getFirstFocusableElement = function (nodesToExamine) {
        for (var i = 0; i < nodesToExamine.length; i++) {
            var currentNode = nodesToExamine[i];
            if (this.isFocusableElement(currentNode)) {
                return currentNode; // we found it!
            }
            var firstFocusableElementInChild = this.getFirstFocusableElement(currentNode.childNodes);
            if (firstFocusableElementInChild != null) {
                return firstFocusableElementInChild;
            }
        }
        return null; // didn't find the element
    };
    this.getLastFocusableElement = function (nodesToExamine) {
        for (var i = nodesToExamine.length - 1; i >= 0; i--) {
            var currentNode = nodesToExamine[i];
            if (this.isFocusableElement(currentNode)) {
                return currentNode; // we found it!
            }
            var lastFocusableElementInChild = this.getLastFocusableElement(currentNode.childNodes);
            if (lastFocusableElementInChild != null) {
                return lastFocusableElementInChild;
            }
        }
        return null; // didn't find the element
    };
    this.getDeepestChildNode = function (ancestorNode) {
        if (ancestorNode.hasChildNodes()) {
            return this.getDeepestChildNode(ancestorNode.lastChild);
        }
        else {
            return ancestorNode;
        }
    };
    this.getPreviousFocusableElement = function (currentNode, overallParentNode) {
        if (currentNode == overallParentNode) {
            return null;
        }
        var potentialNode;
        if (currentNode.previousSibling) {
            potentialNode = this.getDeepestChildNode(currentNode.previousSibling);
            if (this.isFocusableElement(potentialNode)) {
                return potentialNode;
            }
            return this.getPreviousFocusableElement(potentialNode, overallParentNode);
        }
        else if (currentNode.parentNode) {
            potentialNode = currentNode.parentNode;
            if (this.isFocusableElement(potentialNode)) {
                return potentialNode;
            }
            return this.getPreviousFocusableElement(potentialNode, overallParentNode);
        }
        return null;
    };
    this.getFirstFocusableChild = function (parent) {
        if (parent.hasChildNodes()) {
            var childNodes = parent.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var child = childNodes[i];
                if (this.isFocusableElement(child)) {
                    return child; // we found it!
                }
                var firstFocusableChild = this.getFirstFocusableChild(child);
                if (firstFocusableChild != null)
                    return firstFocusableChild;
            }
        }
        return null;
    };
    this.getNextFocusableElement = function (currentNode, overallParentNode) {
        // examine children
        var potentialNode = this.getFirstFocusableChild(currentNode);
        if (potentialNode != null) {
            return potentialNode; // we found it!
        }

        // for each next sibling (examine it and its children)
        potentialNode = currentNode;
        while (potentialNode.nextSibling) {
            potentialNode = potentialNode.nextSibling;
            if (this.isFocusableElement(potentialNode)) {
                return potentialNode; // we found it!
            }
            var firstFocusableChild = this.getFirstFocusableChild(potentialNode);
            if (firstFocusableChild != null) {
                return firstFocusableChild;
            }
        }

        // if no more siblings, go up to parents and and then use it's next siblings
        var parentNode = currentNode;
        while (parentNode.parentNode) {
            parentNode = parentNode.parentNode;
            if (parentNode == overallParentNode) {
                return null;
            }
            var sibling = parentNode;
            while (sibling.nextSibling) {
                sibling = sibling.nextSibling;
                potentialNode = this.getNextFocusableElement(sibling, overallParentNode);
                if (potentialNode != null) {
                    return potentialNode;
                }
            }
        }

        return null;
    };
}
function doWebInteractiveKeyDown(eventElement, event, parentContainerID) {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    var parentContainer = document.getElementById(parentContainerID);
    var leftArrow = 37;
    var rightArrow = 39;
    if (document.documentElement.dir == "rtl") {
        leftArrow = 39;
        rightArrow = 37;
    }
    switch (keyCode) {
        case 40: /*down arrow*/
            self.currentFound = false;
            var treeRootNodes = parentContainer.childNodes;
            var foundElement = self.webGuiInteractiveTree.getNextFocusableElement(eventElement, parentContainer);
            if (foundElement == null) {
                foundElement = self.webGuiInteractiveTree.getFirstFocusableElement(treeRootNodes);
            }
            if (foundElement != null) {
                try {
                    foundElement.focus();
                }
                catch (problem) {
                    window.status = problem;
                }
            }
            return false;
        case 38: /*up arrow*/
            self.previousNode = null;
            var treeRootNodes = parentContainer.childNodes;
            var foundElement = self.webGuiInteractiveTree.getPreviousFocusableElement(eventElement, parentContainer);
            self.previousNode = null;
            if (foundElement == null) {
                foundElement = self.webGuiInteractiveTree.getLastFocusableElement(treeRootNodes);
            }
            if (foundElement != null) {
                try {
                    foundElement.focus();
                }
                catch (problem) {
                    window.status = problem;
                }
            }
            return false;
        case leftArrow:
            if (eventElement.getAttribute) {
                var collapseScript = eventElement.getAttribute("collapseScript");
                if (collapseScript != "null")
                    eval(collapseScript);
            }
            try {
                event.returnValue = false;
                event.cancelBubble = true;
            }
            catch (e) { }
            return false;
        case rightArrow:
            if (eventElement.getAttribute) {
                var expandScript = eventElement.getAttribute("expandScript");
                if (expandScript != "null")
                    eval(expandScript);
            }
            try {
                event.returnValue = false;
                event.cancelBubble = true;
            }
            catch (e) { }
            return false;
    }
    return true;
}

function doActivateNodeWebGUI(nodeId, parentNodeId, indentLevel, activeNodeRememberedInCookie, treeId) {
    // these 2 global values are in text fields so the content can easily be transferred between frames
    var currentActiveTreeSectionField = document.getElementById("currentActiveTreeSection" + treeId);
    var currentActiveTreeSection = currentActiveTreeSectionField.value;
    var currentActiveTreeItemField = document.getElementById("currentActiveTreeItem" + treeId);
    var currentActiveTreeItem = currentActiveTreeItemField.value;

    if (currentActiveTreeSection != "") {
        try {
            document.getElementById(currentActiveTreeSection).className = "";
        }
        catch (problem) { }
    }
    if (currentActiveTreeItem != "") {
        try {
            document.getElementById(currentActiveTreeItem).className = "";
        }
        catch (problem) { }
    }
    if (parentNodeId == "") {
        currentActiveTreeSection = "";
        currentActiveTreeSectionField.value = currentActiveTreeSection;
    }
    else {
        currentActiveTreeSection = "node" + treeId + parentNodeId; // NODE_DIV_PREFIX
        currentActiveTreeSectionField.value = currentActiveTreeSection;
    }
    if (currentActiveTreeSection != "") {
        try {
            if (parentNodeId == nodeId)
                document.getElementById(currentActiveTreeSection).className = "treeactivesectionbottomonly";
            else
                document.getElementById(currentActiveTreeSection).className = "treeactivesection";
        }
        catch (problem) {
            currentActiveTreeSection = "";
            currentActiveTreeSectionField.value = currentActiveTreeSection;
        }
    }
    currentActiveTreeItem = "nodeHead" + treeId + nodeId; // NODE_HEAD_DIV_PREFIX
    currentActiveTreeItemField.value = currentActiveTreeItem;
    try {
        document.getElementById(currentActiveTreeItem).className = "treeactiveitem";
    }
    catch (problem) {
        currentActiveTreeItem = "";
        currentActiveTreeItemField.value = currentActiveTreeItem;
    }

    if (activeNodeRememberedInCookie) {
        jdeWebGUIsetExpirableSubCookie("webguiinteractivetree", treeId + "ActiveNodeId", nodeId, "");
        jdeWebGUIsetExpirableSubCookie("webguiinteractivetree", treeId + "ActiveParent", parentNodeId, "");
        jdeWebGUIsetExpirableSubCookie("webguiinteractivetree", treeId + "ActiveIndent", indentLevel, "");
    }
}

function doTreeIframeWebGUI(nodeId, indentLevel, nodeIconId, collapsedIconURL, expandedIconURL, toggleMode, activeNodeRememberedInCookie, treeId) {
    doActivateNodeWebGUI(nodeId, nodeId/*parent*/, indentLevel, activeNodeRememberedInCookie, treeId);
    var childDiv = document.getElementById("treechild" + treeId + nodeId); // CHILD_DIV_PREFIX
    var expanded = (childDiv.style.display == "block");
    if (expanded && (toggleMode == 'expand')) {
        return;
    }
    else if (expanded) {
        var theToggleIcon = document.getElementById(nodeIconId);
        theToggleIcon.src = collapsedIconURL;
        childDiv.innerHTML = "";
        childDiv.style.display = "none";
    }
    else if (toggleMode == 'collapse') {
        return;
    }
    else {
        var theToggleIcon = document.getElementById(nodeIconId);
        theToggleIcon.src = expandedIconURL;
        childDiv.style.display = "block";
        var loadingIndent = indentLevel * 12; // INDENT_SIZE
        var loadingMessage = document.getElementById("loadingMessage" + treeId).value;
        childDiv.innerHTML = "<table cellpadding=0 cellspacing=0><tr><td><img src='" + window["WEBGUIRES_images_spacer_gif"] + "' alt=\"\" width=" + loadingIndent + " height=3></td><td>&nbsp;" + loadingMessage + "</td></tr></table>";
    }
    var iframeDiv = document.getElementById("treeiframediv" + treeId); // IFRAME_DIV_ID
    iframeDiv.style.display = "";
    var callBackUrlPrefix = document.getElementById("callBackUrlPrefix" + treeId).value;
    var nodeIdParam = document.getElementById("nodeIdParam" + treeId).value;
    var indentLevelParam = document.getElementById("indentLevelParam" + treeId).value;
    iframeDiv.innerHTML = "<iframe tabindex=\"-1\" width=1 height=1 scrolling=no frameborder=0 src='" + callBackUrlPrefix + nodeIdParam + "=" + escape(nodeId) + "&" + indentLevelParam + "=" + escape(indentLevel) + "'></iframe>";
}

function doLoadActiveNodeWebGUI(activeNodeRememberedInCookie, treeId) {
    try {
        var nodeId = jdeWebGUIgetSubCookie("webguiinteractivetree", treeId + "ActiveNodeId");
        var parentNodeId = jdeWebGUIgetSubCookie("webguiinteractivetree", treeId + "ActiveParent");
        var indentLevel = jdeWebGUIgetSubCookie("webguiinteractivetree", treeId + "ActiveIndent");
        var indentLevelInt = 0;
        if (indentLevel != "")
            indentLevelInt = parseInt(indentLevel);
        if (nodeId != "")
            doActivateNodeWebGUI(nodeId, parentNodeId, indentLevelInt, activeNodeRememberedInCookie, treeId);
    }
    catch (problem) { }
}

// END OF SCRIPTS FOR INTERACTIVE TREES
//////////////////////////////////
// START OF SCRIPTS FOR MULTISELECTS

function jdeWebGuiMultiSelectCategory(category_id, category_desc) {
    this.category_id = category_id;
    this.category_desc = category_desc;
}

function jdeWebGuiMultiSelectItem(item_id, item_name, item_desc, category_id) {
    this.item_id = item_id;
    this.item_name = item_name;
    this.item_desc = item_desc;
    this.category_id = category_id;
}

// END OF SCRIPTS FOR MULTISELECTS
//////////////////////////////////
// START OF SCRIPTS FOR MENUS
var g_jdeWebGUIMenuShown = false;
var g_PrevFocusedElement = null;
var g_caroTabContextMenuId = 'caroTabContextMenu';

/*
* menuPlaceholderObject has 1) a boolean indicating whether we'll
* add a placeholder to a submenu ("[Empty]") and 2) a reference
* to a disabled menu item where the placeholder can be written.
*/
var menuPlaceholderObject =
{
    isRequired: true,
    lastDisabledID: null
};

/*
* emptyString is the placeholder text string ("[Empty]").  Its
* initialization value is padded with spaces--seeing those spaces means
* English bleed through is occurring.
*/
var emptyString = "[ Empty ]";
try {
    if (top != null && top.CARO_STRING_CONSTS != null) {
        emptyString = "[" + top.CARO_STRING_CONSTS.empty + "]";
    }
} catch (err) {
    //This try/catch was added to handle forms embedded in iFrames 
    //that don�t have access to the parent or top document. 
}

function jdeWebGUISaveFocusedElement(evt) {
    g_PrevFocusedElement = (evt.target === undefined) ? document.activeElement : evt.target;
}

function jdeWebGUICheckToRestorePrevFocus() {
    if (g_jdeWebGUIMenuShown == false && g_PrevFocusedElement) {
        try {
            g_PrevFocusedElement.focus();
        }
        catch (error) {
        }
        g_PrevFocusedElement = null;
    }
}

function jdeWebGUIUpdateMenuShownFlag() {
    if (jdeWebGUIGetMenuStackDepth() > 0)
        g_jdeWebGUIMenuShown = true;
    else
        g_jdeWebGUIMenuShown = false;
}

function jdeWebGUIOnMenuUpKey() {
    var activeMenuItem = jdeWebGUIGetActiveMenuItem();
    if (activeMenuItem != null) {
        var nextElement = jdeWebGUIGetPreviousVisibleElementWithFlag(activeMenuItem.parentNode, "jdeMenuHighlightable", "yes");
        if (nextElement != null) {
            jdeWebGUIHighlightMenuItem(nextElement);
            // set the focus on the menu item
            if (nextElement.parentNode)
                nextElement.parentNode.focus();
            return true;
        }
    }
    return false;
}

function jdeWebGUIOnMenuDownKey() {
    var activeMenuItem = jdeWebGUIGetActiveMenuItem();
    if (activeMenuItem != null) {
        var nextElement = jdeWebGUIGetNextVisibleElementWithFlag(activeMenuItem.parentNode, "jdeMenuHighlightable", "yes");
        if (nextElement != null) {
            jdeWebGUIHighlightMenuItem(nextElement);
            // set the focus on the menu item
            if (nextElement.parentNode)
                nextElement.parentNode.focus();
            return true;
        }
    }
    return false;
}

function jdeWebGUIOnMenuExpandKey(evt, activeMenuItem) {
    if (activeMenuItem != null) {
        var subMenuId = activeMenuItem.getAttribute("id");
        //truncate the substring "-show" to get the ID of the submenu
        subMenuId = subMenuId.substring(0, subMenuId.lastIndexOf("-"));
        jdeWebGUIdoToggleSubMenuOnKeyDown(activeMenuItem, subMenuId, false, evt);
        return true;
    }
    return false;
}

function jdeWebGUIOnMenuEnterKey(evt) {
    var activeMenuItem = jdeWebGUIGetActiveMenuItem();
    if (activeMenuItem != null) {
        if (jdeWebGUIIsElementAttributeOn(activeMenuItem.parentNode, "subMenu", "yes")) {
            jdeWebGUIOnMenuExpandKey(evt, activeMenuItem.parentNode);
        }
        else {
            var menuItemId = activeMenuItem.parentNode.id;
            //strip out the prefix "outer"
            menuItemId = menuItemId.substring(5);
            jdeWebGUIhideAllMenus(null, null, null);
            jdeWebGUICheckToRestorePrevFocus();
            menuItem = document.getElementById(menuItemId);
            if (menuItem)
                menuItem.onclick();
        }
        return true;
    }
    return false;
}

function jdeWebGUIOnDefaultKeyDown(e) {
    var event = e ? e : window.event;
    jdeWebGUIhideAllMenus(null, null, null);
    jdeWebGUICheckToRestorePrevFocus();
    return true;
}

function jdeWebGUIOnMenuKeyDown(e) {
    var event = e ? e : window.event;
    var cancelBubbling = false;
    var leftArrow = 37;
    var rightArrow = 39;
    if (document.documentElement.dir == "rtl") {
        leftArrow = 39;
        rightArrow = 37;
    }

    if (g_jdeWebGUIMenuShown == false)
        return false;

    if (event.keyCode == 38) //Up Arrow
    {
        jdeWebGUIOnMenuUpKey();
        cancelBubbling = true;
    }
    else if (event.keyCode == 40) //Down Arrow
    {
        jdeWebGUIOnMenuDownKey();
        cancelBubbling = true;
    }
    else if (event.keyCode == rightArrow) //right arrow key
    {
        var activeMenuItem = jdeWebGUIGetActiveMenuItem();
        if (activeMenuItem != null) {
            if (jdeWebGUIIsElementAttributeOn(activeMenuItem.parentNode, "subMenu", "yes")) {
                jdeWebGUIOnMenuExpandKey(event, activeMenuItem.parentNode);
            }
        }
        cancelBubbling = true;
    }
    else if (event.keyCode == leftArrow) //left arrow
    {
        if (jdeWebGUIGetMenuStackDepth() > 1) {
            jdeWebGUIHideLastLevelMenu();
        }
        cancelBubbling = true;
    }
    else if (event.keyCode == 13 || event.keyCode == 32) {  //Enter key
        jdeWebGUIOnMenuEnterKey(event)
        cancelBubbling = true;
    }
    else {
        jdeWebGUIOnDefaultKeyDown(event)
        if (event.keyCode != 17 && event.keyCode != 18) //For Ctrl and Alt key, leave it to document level
            cancelBubbling = true;
    }
    if (cancelBubbling) {
        if (isIE) {
            event.cancelBubble = true;
            event.returnValue = false;
        }
        else {
            event.stopPropagation();
            event.preventDefault();
        }
    }
    return cancelBubbling;
}

function jdeWebGUIexecuteLinkInFrame(linkurl, linktarget) {
    // linktarget must be a valid frame or iframe name or nothing will happen ("_new" will not work)
    eval(linktarget + ".location=\"" + linkurl + "\"");
}

function jdeWebGUIstartMenu(menuID) {
    initializeMenuPlaceholderObject();
    writeString = "<div ID=\"" + menuID + "\" ALIGN=LEFT jdeWebGUIPopupMenu=\"yes\" style=\"display:none;position:absolute;z-index:9;\" class=MenuDropdownBack";
    if (document.documentElement.dir == "rtl")
        writeString += "_rtl";
    writeString += " onclick=\"jdeWebGUIhideAllMenus(this,'" + menuID + "', event);\">\n";
    if (document.documentElement.dir == "rtl")
        writeString += "<table ID=\"" + menuID + "_rtl\" cellpadding=0 cellspacing=0><tr><td>";

    writeString += "<table cellpadding=0 cellspacing=0><tr><td>\n";
}

function jdeWebGUIstartSubMenu(menuID) {
    initializeMenuPlaceholderObject();
    writeString = "<div ID=\"" + menuID + "\" align=left style=\"display:none;position:absolute;z-index:9;\" class=MenuDropdownBack";
    if (document.documentElement.dir == "rtl")
        writeString += "_rtl";
    writeString += " jdeWebGUIPopupMenu=\"yes\" onclick=\"jdeWebGUIhideAllMenus(this, '" + menuID + "', event);\">\n";
    if (document.documentElement.dir == "rtl")
        writeString += "<table ID=\"" + menuID + "_rtl\" cellpadding=0 cellspacing=0><tr><td>";

    writeString += "<table cellpadding=0 cellspacing=0><tr><td>\n";
}

/*
* This function has two code paths depending on the input parameter's value.  Note
* that when writing the menu (returnOnly false), placeholders for empty menus are
* also written.  With returnOnly true, this step does not occur because the document
* elements exist only in a string vs. in the document.  Rather than manipulate that
* return string, we acknowledge that empty strings are not applicable to the
* returnOnly true scenario (e.g., One View Reporting and the Carousel's context menu).
*/
function jdeWebGUIwriteMenu(returnOnly) {
    writeString += "</td></tr></table>";
    if (document.documentElement.dir == "rtl") // workaround for Mozilla in RTL
        writeString += "</td></tr></table>";
    writeString += "</div>";
    if (returnOnly) {
        var retValue = writeString;
        writeString = "";
        return retValue;
    }
    else {
        document.writeln(writeString);
        writeString = ""; // free up the memory
    }
}

function jdeWebGUIaddMenuItem(MenuID, MenuItemStr, MenuItemHref, Type, MenuItemID, menuAltDesc, blankAltDesc, accessibilityMode, menuType, menuItemChecked, dummy, namespace) {
    if (Type.indexOf("DISABLEDSUB") != -1 && MenuItemID.indexOf('FavoritesLabel') == -1 && MenuItemID.indexOf('FormRowLabels') == -1) {
        writeString += "<div jdeWebGUIPopupMenu=\"yes\" onclick=\"jdeWebGUIpreventHideAll=true;\" ontouchend=\"jdeWebGUIpreventHideAll=true;\"><table class=\"MenuNormal\" jdeWebGUIPopupMenu=\"yes\" cellpadding=0 cellspacing=0><tr jdeWebGUIPopupMenu=\"yes\"><td jdeWebGUIPopupMenu=\"yes\"><nobr jdeWebGUIPopupMenu=\"yes\">&nbsp;<IMG jdeWebGUIPopupMenu=\"yes\" align=absmiddle border=0 width=9 height=10 SRC='" + window["WEBGUIRES_images_menuitemblank_gif"] + "' alt=\"" + blankAltDesc + "\">&nbsp;</nobr></td><td jdeWebGUIPopupMenu=\"yes\" width=100% class=MenuItemDisabled><span class=MenuItemDisabled><nobr jdeWebGUIPopupMenu=\"yes\">" + MenuItemStr + "&nbsp;&nbsp;&nbsp;</nobr></span></td><td jdeWebGUIPopupMenu=\"yes\" align=right><nobr jdeWebGUIPopupMenu=\"yes\"><IMG jdeWebGUIPopupMenu=\"yes\" align=absmiddle border=0 width=9 height=10 SRC='";
        if (document.documentElement.dir == "rtl") {
            writeString += window["WEBGUIRES_images_submenuitem_disabled_rtl_gif"];
        }
        else {
            writeString += window["WEBGUIRES_images_submenuitem_disabled_gif"];
        }
        writeString += "' alt=\"" + menuAltDesc + "\">&nbsp;</nobr></td></tr></table></div>\n";
    }
    else if (Type.indexOf("SUBMENU") != -1 && MenuItemID.indexOf('FavoritesLabel') == -1 && MenuItemID.indexOf('FormRowLabels') == -1) {
        writeString += "<div jdeWebGUIPopupMenu=\"yes\" subMenu=\"yes\"" +
            " ID='" + MenuID + "-" + MenuItemID + "-Show' tabindex='0'";
        if (accessibilityMode) {
            writeString += " role='menuitem' aria-label='" + decodeEntities(MenuItemStr) + " " + jdeWebGUIStaticStrings.ACCESS_SUB_MENU + "'";
            // TODO: writeString += " aria-describedby='subMenuDesc'";  (want to add this description somewhere: use jdeWebGUIStaticStrings.ACCESS_SUB_MENU_DESC)
        }
        writeString += "><table class=\"MenuNormal\" jdeMenuHighlightable=\"yes\" jdeWebGUIPopupMenu=\"yes\" cellpadding=0 cellspacing=0" +
            " onmouseover=\"jdeWebGUIUnhighlightAllItemsInCurrentMenu(this);jdeWebGUIHighlightMenuItemById('" + MenuID + "-" + MenuItemID + "-Show');jdeWebGUIpreventHideAll=false;jdeWebGUIdoToggleSubMenu(this, '" + MenuID + "-" + MenuItemID + "', event);\"" +
            " onclick=\"jdeWebGUIpreventHideAll=true;jdeWebGUIdoToggleSubMenu(this, '" + MenuID + "-" + MenuItemID + "', event);\"";
        if (isTouchEnabled) {
            writeString += " ontouchstart=\"jdeWebGUIUnhighlightMenuItem(jdeWebGUIGetActiveItemInMenu(this.parentNode.parentNode));jdeWebGUIHighlightMenuItemById('" + MenuID + "-" + MenuItemID + "-Show');jdeWebGUIpreventHideAll=false;jdeWebGUIdoToggleSubMenu(this, '" + MenuID + "-" + MenuItemID + "', event);\"" +
            " ontouchend=\"jdeWebGUIpreventHideAll=true;jdeWebGUIdoToggleSubMenu(this, '" + MenuID + "-" + MenuItemID + "', event);\"";
        }
        writeString += "> <tr jdeWebGUIPopupMenu=\"yes\"> " +
            "<td jdeWebGUIPopupMenu=\"yes\"><nobr jdeWebGUIPopupMenu=\"yes\">&nbsp;<IMG jdeWebGUIPopupMenu=\"yes\" align=absmiddle border=0 width=9 height=10 SRC='" + window["WEBGUIRES_images_menuitemblank_gif"] + "' alt=\"" + blankAltDesc + "\">&nbsp;</nobr></td>" +
            "<td jdeWebGUIPopupMenu=\"yes\" class=MenuItemSubMenu ID=\"SubMenu_" + MenuItemID + "\"><span class=MenuItemSubMenu><nobr style=\"WIDTH: 100%\" jdeWebGUIPopupMenu=\"yes\">" + MenuItemStr + "&nbsp;&nbsp;&nbsp;</nobr></span><nobr><LABEL FOR=\"SubMenu_" + MenuItemID + "\"><span STYLE=\"display:none;\">&nbsp;" + Type + "&nbsp;</span></LABEL></nobr></td> " +
            "<td jdeWebGUIPopupMenu=\"yes\" align=right><nobr jdeWebGUIPopupMenu=\"yes\"><IMG jdeWebGUIPopupMenu=\"yes\" align=absmiddle border=0 width=9 height=10 SRC='";
        if (document.documentElement.dir == "rtl") {
            writeString += window["WEBGUIRES_images_submenuitem_rtl_gif"];
        }
        else {
            writeString += window["WEBGUIRES_images_submenuitem_gif"];
        }
        writeString += "' alt=\"" + menuAltDesc + "\">&nbsp;</nobr></td></tr></table></div>\n";

        // No need for an empty placeholder when there is a submenu on the menu.
        menuPlaceholderObject.isRequired = false;
        menuPlaceholderObject.lastDisabledID = null;
    }
    else if (Type.indexOf("SEPARATOR") != -1 && MenuItemID.indexOf('FavoritesLabel') == -1 && MenuItemID.indexOf('FormRowLabels') == -1) {
        writeString += "<table class=\"MenuSep\" cellpadding=0 cellspacing=0><tr><td jdeWebGUIPopupMenu=\"yes\" onclick=\"jdeWebGUIpreventHideAll=true;\" ontouchend=\"jdeWebGUIpreventHideAll=true;\"><HR onclick=\"jdeWebGUIpreventHideAll=true;\"  jdeWebGUIPopupMenu=\"yes\" class=HR1 SIZE=1></td></tr></table>\n";
    }
    else {
        jdeWebGUIsetNamespace(namespace);
        // add the Favorite menuItem to the fav array
        if (MenuItemID.indexOf('FEFA') != -1) {
            addFormExitFavMenuStack(MenuItemID);
        }
        else if (MenuItemID.indexOf('REFA') != -1) {
            addRowExitFavMenuStack(MenuItemID);
        }

        writeString += "<div id=\"outer" + MenuItemID + "\" tabindex='0'";
        // Align the Default Favorites, ROW & FORM Labels to left
        if (MenuItemID.indexOf('FavoritesLabel') == -1 && MenuItemID.indexOf('FormRowLabels') == -1) {
            if (document.documentElement.dir == "rtl") {
                writeString += " align=right";
            }
            else {
                writeString += " align=left";
            }


            if (MenuItemID == "AutoSuggest") {
                if (Type == "CHECKED" || Type == "DISABLEDCHECKED")
                    writeString += " AutoSuggestChecked=\"yes\"";
                else
                    writeString += " AutoSuggestChecked=\"no\"";
            }
        }
        if (accessibilityMode) {
            writeString += " role='menuitem' aria-label=\"" + decodeEntities(MenuItemStr);
            if (Type.indexOf("CHECKED") != -1)
                writeString += " " + menuItemChecked;  // this is the passed in, translated, accessibility string
            writeString += "\"";
        }
        writeString += ">";
        if (Type.indexOf("DISABLED") == -1) {
            writeString += getWebGUIMenuItemInnerHTML(MenuID, MenuItemStr, MenuItemHref, Type, MenuItemID, accessibilityMode, menuType, menuItemChecked, dummy);

            /*
            * Enabled, non-labeled items are our concern.
            * If we found one, we won't need a placeholder.
            */
            if (MenuItemID.indexOf('FavoritesLabel') == -1 &&
               MenuItemID.indexOf('FormRowLabels') == -1) {
                menuPlaceholderObject.isRequired = false;
                menuPlaceholderObject.lastDisabledID = null;
            }
        }
        else {
            // Capture the non-label item--the potential placeholder will go there.
            // isRequired is initialized to true. The expectation is it remains that
            // way unless we updated it when we encountered an enabled item.
            if (MenuItemID.indexOf('FavoritesLabel') == -1 &&
               MenuItemID.indexOf('FormRowLabels') == -1 &&
               menuPlaceholderObject.isRequired == true) {
                menuPlaceholderObject.lastDisabledID = MenuItemID;
            }
        }
        writeString += "</div>\n";
    }
}

function jdeWebGUIChangeDisplayStyle(theElementID, newDisplayStyle) {
    var theElement = document.getElementById(theElementID);
    if (theElement) {
        if (newDisplayStyle) // true means to show it
            theElement.style.display = "";
        else // false means to hide it
            theElement.style.display = "none";
    }
}

function hideSubMenuToggle(Parent, MenuID, event) {
    if (jdeWebGUIGetMenuStackDepth() > 1)
        jdeWebGUIhideSubMenu(Parent, jdeWebGUIGetLastLevelMenuID(), event);
}

function getWebGUIMenuItemInnerHTML(MenuID, MenuItemStr, MenuItemHref, Type, MenuItemID, accessibilityMode, menuType, menuItemChecked, dummy) {
    var writeString = "";
    var onClick = "";
    var onTouchEnd = "";
    writeString = "<table class=\"MenuNormal\" jdeWebGUIPopupMenu=\"yes\" ID=\"" + MenuItemID + "\" cellpadding=0 cellspacing=0 width=100%";
    if (Type.indexOf("DISABLED") == -1 && MenuItemID.indexOf('FavoritesLabel') == -1 && MenuItemID.indexOf('FormRowLabels') == -1) {
        writeString += " jdeMenuHighlightable=\"yes\"";
        writeString += " onmouseover=\"jdeWebGUIUnhighlightAllItemsInCurrentMenu(this);jdeWebGUIHighlightMenuItemById('outer" + MenuItemID + "'); hideSubMenuToggle(this, '" + MenuItemID + "', event);\" ";
        writeString += " onmouseout=\"jdeWebGUIUnhighlightMenuItem(this);\" ";
        writeString += " onmousedown=\"menuItemMouseDown('outer" + MenuItemID + "', event);\" ";
        if (isTouchEnabled)
            writeString += " ontouchstart=\"jdeWebGUIHighlightMenuItemById('outer" + MenuItemID + "');\" ";
        if (MenuItemHref.indexOf("javascript:") == -1) // not a JavaScript call
        {
            if (!isTouchEnabled) {
                onClick = "onclick=\"self.location='";
                var hrefDelimitedItems = MenuItemHref.split("'");
                var i = 0;
                for (; i < hrefDelimitedItems.length - 1; i++)
                    onClick += hrefDelimitedItems[i] + "\\'";
                onClick += hrefDelimitedItems[i];
                onClick += "';\"";
            }
            else {
                onTouchEnd = " ontouchend=\"self.location='";
                var hrefDelimitedItems = MenuItemHref.split("'");
                var i = 0;
                for (; i < hrefDelimitedItems.length - 1; i++)
                    onTouchEnd += hrefDelimitedItems[i] + "\\'";
                onTouchEnd += hrefDelimitedItems[i];
                onTouchEnd += "';\"";
            }
        }
        else // is a JavaScript call, immediately evaluate it when clicked
        {
            if (!isTouchEnabled) {
                onClick = "onclick=\"eval('";
                var hrefDelimitedItems = MenuItemHref.split("'");
                var i = 0;
                for (; i < hrefDelimitedItems.length - 1; i++)
                    onClick += hrefDelimitedItems[i] + "\\'";
                onClick += hrefDelimitedItems[i];
                onClick += "');\"";
            }
            else {
                onTouchEnd = "ontouchend=\"eval('";
                var hrefDelimitedItems = MenuItemHref.split("'");
                var i = 0;
                for (; i < hrefDelimitedItems.length - 1; i++)
                    onTouchEnd += hrefDelimitedItems[i] + "\\'";
                onTouchEnd += hrefDelimitedItems[i];
                onTouchEnd += "');\"";
            }
        }
    }
    else {
        if (!isTouchEnabled) {
            onClick += " onclick=\"jdeWebGUIpreventHideAll=true;\"";
        }
        else {
            onTouchEnd += " ontouchend=\"jdeWebGUIpreventHideAll=true;\"";
        }
    }

    if (!isTouchEnabled) {
        writeString += onClick;
    }
    else {
        writeString += onTouchEnd;
    }
    writeString += "><tr jdeWebGUIPopupMenu=\"yes\"><td jdeWebGUIPopupMenu=\"yes\"><nobr jdeWebGUIPopupMenu=\"yes\">&nbsp;";
    var imgString = "<IMG jdeWebGUIPopupMenu=\"yes\" align=absmiddle border=0 width=9 height=10 SRC='";

    if (Type == "DISABLEDCHECKED")
        imgString += window["WEBGUIRES_images_menuitemcheckdisabled_gif"] + "' alt=''>";
    else if (Type == "CHECKED")
        imgString += window["WEBGUIRES_images_menuitemcheck_gif"] + "' alt=''>";
    else
        imgString += window["WEBGUIRES_images_menuitemblank_gif"] + "' alt=''>";

    writeString += imgString;

    writeString += "&nbsp;</nobr></td><td jdeWebGUIPopupMenu=\"yes\" style=\"width: 100%;\" class=";
    if (Type.indexOf("DISABLED") != -1)
        writeString += "MenuItemDisabled";
    else if (MenuItemID.indexOf('FavoritesLabel') != -1)
        writeString += "FavoritesLabel";
    else if (MenuItemID.indexOf('FormRowLabels') != -1)
        writeString += "FormRowLabels";
    else
        writeString += "MenuItem";
    writeString += "><span jdeWebGUIPopupMenu=\"yes\" CLASS=";
    if (Type.indexOf("DISABLED") != -1) {
        if (dummy == true) {
            writeString += "accessibility";
        }
        else {
            writeString += "MenuItemDisabled";
        }
    }
    else {
        writeString += "MenuItem";
    }
    //For Accessibilty Mode render text equivalent of Disabled class 'MenuItemDisabled'
    if (Type.indexOf("DISABLED") != -1 && accessibilityMode == true) {
        MenuItemStr += "<span class=\"accessibility\"> Status : Disabled </span>";
    }
    writeString += "><nobr jdeWebGUIPopupMenu=\"yes\">" + MenuItemStr + "&nbsp;</nobr>";
    writeString += "</span>";
    writeString += "</td><td jdeWebGUIPopupMenu=\"yes\" align=right> ";
    if (accessibilityMode == false) {
        writeString += " <img src='" + window["WEBGUIRES_images_spacer_gif"] + "' alt=\"\" width=10 height=1>";
    }

    writeString += "</td></tr></table>";
    return writeString;
}

var jdeWebGUIcurrentMenu = null;
var jdeWebGUIcurrentSubMenu = null;

var jdeWebGUIPopupMenuEventHandlerEnabled = false;
var jdeWebGUIPopupMenuEventHandlerParent = null;
var jdeWebGUIPopupMenuEventHandlerMenuID = null;
function jdeWebGUIPopupMenuEventHandler(event) {

    var closePopup = true;
    // do not hide the AutoSuggest pop up window - MZ5649378
    if (window.openedPopup && window.openedPopup.id != "typeAheadWindow" && window.openedPopup.id != "savePrompt") {
        // Check if we can close Freeze / Hide Context menu
        if (window.openedPopup.id.indexOf('popupFrzWin') != -1) {
            var srcElementID;
            if (document.all) {
                srcElementID = window.event.srcElement.id;
            }
            else {
                srcElementID = event.currentTarget.id;
                // In FF/Chrome, get the original target when there is no srcElementID
                if (!srcElementID || "" == srcElementID) {
                    if (event.target)
                        srcElementID = event.target.id;
                }
            }
            if (srcElementID &&
               (srcElementID.indexOf('popupFrzWin') != -1
                || srcElementID.indexOf('FreezeTD') != -1
                || srcElementID.indexOf('HideDiv') != -1
                || srcElementID.indexOf('UnHideDiv') != -1
                || srcElementID.indexOf('FreezeDiv') != -1
                || srcElementID.indexOf('UnFreezeDiv') != -1)) {
                // If user clicks anywhere inside the Freeze context menu, then don't close it
                closePopup = false;
            }
        }
        else if (window.openedPopup.id.indexOf('popupUnHideWin') != -1) {
            var srcElement, srcElementID;
            if (document.all) {
                srcElement = window.event.srcElement;
                if (srcElement)
                    srcElementID = srcElement.id;
            }
            else {
                srcElement = event.currentTarget;
                if (srcElement)
                    srcElementID = srcElement.id;
                // In FF/chrome, the 'srcElement' is the HTMLDoc on the Top level form. Hence check for 'target', which is the original target
                if (!srcElementID || "" == srcElementID) {
                    if (event.target)
                        srcElementID = event.target.id;
                    if ((!srcElementID || "" == srcElementID) && event.target.getAttribute("showunhidepopup") == "true")
                        closePopup = false;
                }
            }
            // If user clicks anywhere inside the UnHide pop-up, then don't close it
            if (srcElementID &&
               (srcElementID.indexOf('popupUnHideWin') != -1
                || srcElementID.indexOf('UnHideDiv') != -1
                || srcElementID.indexOf('unHide') != -1
                || srcElementID.indexOf('hideCol') != -1)) {
                closePopup = false;
            }
            else if (srcElement && srcElement.showUnhidePopup) {
                closePopup = false;
            }
        }
        if (closePopup) {
            clearInterval(window.intervalIdForOpenedPopup);
            window.openedPopup.hide();
            window.openedPopup = null;
            window.intervalIdForOpenedPopup = null;
        }
    }
    if (isIE) {
        if (jdeWebGUIPopupMenuEventHandlerEnabled) {
            // check to see if we need to hide the menus or not
            if (window.event.srcElement.jdeWebGUIPopupMenu != "yes" || jdeWebGUIPopupMenuEventHandlerMenuID == g_caroTabContextMenuId) {
                jdeWebGUIhideAllMenus(jdeWebGUIPopupMenuEventHandlerParent, jdeWebGUIPopupMenuEventHandlerMenuID, window.event);
            }
        }
    }
    else {
        if (jdeWebGUIPopupMenuEventHandlerEnabled) {
            // check to see if we need to hide the menus or not
            if (event.currentTarget.jdeWebGUIPopupMenu != "yes") {
                if (event.target.parentNode.jdeWebGUIPopupMenu != "yes") //work around for Safari
                    jdeWebGUIhideAllMenus(jdeWebGUIPopupMenuEventHandlerParent, jdeWebGUIPopupMenuEventHandlerMenuID, event);
            }
        }
    }
    if (jdeWebGUIPopupMenuEventHandlerMenuID && jdeWebGUIPopupMenuEventHandlerMenuID == g_caroTabContextMenuId) {
        jdeWebGUIPopupMenuEventHandlerEnabled = false;
        jdeWebGUIPopupMenuEventHandlerParent = null;
        jdeWebGUIPopupMenuEventHandlerMenuID = null;
    }
    else {
        jdeWebGUIPopupMenuEventHandlerEnabled = true;
    }
}

function jdeWebGUIdoMenu(Parent, MenuID, event) {
    // do not hide the AutoSuggest pop up window - MZ5649378
    if (window.openedPopup && window.openedPopup.id != "typeAheadWindow") {
        clearInterval(window.intervalIdForOpenedPopup);
        window.openedPopup.hide();
        window.openedPopup = null;
        window.intervalIdForOpenedPopup = null;
    }
    var thisMenu = document.getElementById(MenuID);

    // register the custom onclick trapping method
    jdeWebGUIPopupMenuEventHandlerEnabled = false;
    jdeWebGUIPopupMenuEventHandlerParent = Parent;
    jdeWebGUIPopupMenuEventHandlerMenuID = MenuID;

    if (document.attachEvent) {
        document.attachEvent('onclick', jdeWebGUIPopupMenuEventHandler);
    }
    else // Netscape's version
    {
        document.addEventListener("click", jdeWebGUIPopupMenuEventHandler, false);
        document.addEventListener("touchend", jdeWebGUIPopupMenuEventHandler, false);
    }

    var offsetLeftAdj, x, y;
    var E1PaneDIV = document.getElementById("E1PaneDIV");

    // IE9/10 are adding pixels to the offsetLeft calculation of menu text labels.
    offsetLeftAdj = (document.all && !isIE8OrEarlier && Parent.tagName.indexOf('A') == 0) ? -Parent.offsetLeft : 0;

    // Set dropdown menu display position
    x = Parent.offsetLeft + document.body.offsetLeft + offsetLeftAdj;
    y = Math.max(Parent.offsetTop, 0) + document.body.offsetTop;

    /*
    * Browser specific adjustments for the dropdown to appear just below the toolbar.
    * Applicable to standard apps, apps launched through the shortcut launcher, and ones
    * displayed as modal forms.
    */
    if (Parent.tagName.indexOf('IMG') != -1) {
        if (isFirefox)
            y += 1;
    }
    else {
        if (isFirefox || isIE)
            y -= 2;
        else
            y -= 1;
    }

    var vaParent = Parent.offsetParent;
    if (vaParent) {
        if (Parent.tagName.indexOf('IMG') != -1 && vaParent.tagName == 'BODY') {
            y += Parent.clientHeight;
        }
        while (vaParent && vaParent.id != 'E1PaneDIV') {
            if (vaParent.id == 'WebMenuBar') {
                y += vaParent.clientHeight;
            }
            else {
                y += vaParent.offsetTop;
                x += vaParent.offsetLeft;
            }
            vaParent = vaParent.offsetParent;
        }
    }

    if (y < 3) {
        y += Parent.offsetHeight;
    }

    //adjust by the tab on modeless forms
    var modelessTabDiv = document.getElementById("modelessTabDiv");
    if (modelessTabDiv != null) {
        y -= 25;
    }

    //lock toolbar
    if (modelessTabDiv != null) {
        //top banner
        var topBanner = document.getElementById("topimagecell");
        if (topBanner != null) {
            //adjust with topbanner height plus its border and padding
            y -= (topBanner.offsetHeight + 8);
        }

        if (document.getElementById("jdeFormTitle") != null) {
            var jdeFormTitle = document.getElementById("jdeFormTitle");
            y -= jdeFormTitle.offsetHeight + 12
        }
        else {
            //adjust by the form title height
            var formtitle = document.getElementById("formTitle0");
            if (formtitle == null)
                formtitle = document.getElementById("formTitle");
            if (formtitle != null)
                y -= formtitle.offsetHeight;
        }
    }

    var isRTL = document.documentElement.dir == "rtl";
    thisMenu.style.top = y + 'px';
    if (isIOS && isRTL) {
    }
    else {
        thisMenu.style.left = x + 'px';
    }

    // Execute the Row Contextmenu Logic only when the event.type == "contextmenu"
    var e1formdiv = document.getElementById("e1formDiv");
    var mousepos = getCursorPosition(event);
    if (event.type == "contextmenu") {
        // discount the offset of the E1PaneDiv from CafeOne framework and possible scrolling of e1formdiv
        thisMenu.style.top = mousepos[1] - (E1PaneDIV ? getAbsoluteTopPos(E1PaneDIV) + e1formdiv.scrollTop : getAbsoluteTopPos(thisMenu.parentNode)) + 'px';
        thisMenu.style.left = mousepos[0] - (E1PaneDIV ? getAbsoluteLeftPos(E1PaneDIV) + e1formdiv.scrollLeft : 0) + 'px';
        thisMenu.setAttribute('contextmenu', 'yes');

        jdeWebGUIPopupMenuEventHandlerEnabled = true;
    }

    //OPS WSRP in IE has an issue with the E1Menu dropdowns
    var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);
    if (document.all && isWSRP) {
        thisMenu.style.top = (mousepos[1] - getAbsoluteTopPos(thisMenu.parentNode)) + 'px';
    }

    thisMenu.style.display = "block";

    if (event.type == "contextmenu" && UTIL) {
        // we only check context menu to see if it extends below window height
        // this fixes context menu issue on bottom carousel
        var newMenuTop = mousepos[1];
        var windowHeight = UTIL.pageHeight();
        var newMenuBottom = newMenuTop + thisMenu.offsetHeight;
        if (newMenuBottom > windowHeight) {
            newMenuTop = newMenuTop - (newMenuBottom - windowHeight) - 10; // 10 is arbitrary - so it does not touch the bottom edge
            if (newMenuTop < 0) newMenuTop = 0;
        }
        thisMenu.style.top = newMenuTop + "px";
    }

    thisMenu.style.zIndex = 20000; // assuming minimum 32-bit integer
    //Check for caroTabContextMenu so we can change the opacity
    if (jdeWebGUIPopupMenuEventHandlerMenuID == g_caroTabContextMenuId) {
        webguiSetOpacity(thisMenu, 95);
    }
    // This is a hack to force IE display high zindex div correctly in portal
    // by setting the same zindex on the parent table element as well. TABLE-TBODY-TR-TD-DIV
    if (document.all && jdeWebGUIPopupMenuEventHandlerMenuID != g_caroTabContextMenuId) // TODO: add javascript check of portal, since we only need this hack for IE portal
    {
        thisMenu.parentNode.parentNode.parentNode.parentNode.style.zIndex = thisMenu.style.zIndex;
    }

    if (isRTL) {
        var rtlWrapper = document.getElementById(MenuID + "_rtl");
        var theMenuWidth = rtlWrapper.offsetWidth;
        thisMenu.style.width = theMenuWidth;
        var menuTable = document.getElementById("WebMenuBar");
        if (menuTable != null) {
            x = (menuTable.offsetLeft + x + Parent.offsetParent.offsetWidth) - theMenuWidth;
        }
        thisMenu.style.left = x + 'px';
        if (menuTable != null && event.type == "contextmenu") {
            thisMenu.style.left = (mousepos[0] - (x + menuTable.offsetLeft + thisMenu.offsetWidth + 30)) + 'px';
        }
    }

    var availableWidth, availableHeight;
    //HFE - lock toolbar. If the menu is outside the window width, need show it in the left side
    if (E1PaneDIV) {
        availableWidth = E1PaneDIV.offsetWidth;
        availableHeight = E1PaneDIV.offsetHeight - 27;
    }
    else {
        var bodyElem = document.body;

        if (window.innerWidth !== undefined) {
            availableWidth = window.innerWidth;
            availableHeight = window.innerHeight - 52;
        }
        // IE8
        else {
            availableWidth = bodyElem.offsetWidth;
            availableHeight = bodyElem.offsetHeight - 52;
        }
    }

    var windowOffsetLeft = E1PaneDIV ? getAbsoluteLeftPos(E1PaneDIV) : 0;
    if (!isRTL && ((x - windowOffsetLeft + thisMenu.offsetWidth) > availableWidth)) // when menu bleed out of the container window of iframe, need to shift left to fit the window.
    {
        x = availableWidth - thisMenu.offsetWidth + windowOffsetLeft;
        thisMenu.style.left = x + 'px';
    }

    hideOSDrawnControls();
    if (jdeWebGUIcurrentMenu != null) // get rid of any old menu if present
    {
        jdeWebGUIcurrentMenu.style.display = "none";
        removeItemShadow(jdeWebGUIcurrentMenu);
    }
    jdeWebGUIcurrentMenu = thisMenu;
    showItemShadow(thisMenu);

    if (thisMenu.scrollHeight > availableHeight) {
        thisMenu.style.overflowY = "scroll";
        thisMenu.style.height = availableHeight + "px";
    }
    else {
        thisMenu.style.overflowY = "";
        thisMenu.style.height = "";
    }

    //if the RowExit Context menu is outside window width, need to show in top
    var windowOffsetTop = E1PaneDIV ? getAbsoluteTopPos(E1PaneDIV) + e1formdiv.scrollTop : 0;
    if (event.type == "contextmenu" && !isRTL && ((mousepos[1] - windowOffsetTop + thisMenu.offsetHeight) > availableHeight) && (jdeWebGUIPopupMenuEventHandlerMenuID != g_caroTabContextMenuId)) {
        var outsideMenuHeight = availableHeight - (mousepos[1] + thisMenu.offsetHeight);
        thisMenu.style.top = mousepos[1] + outsideMenuHeight + 'px';
    }

    if (isTouchEnabled && event.type == 'touchend')
        jdeWebGUIPopupMenuEventHandler(event);
}

function jdeWebGUIshowMenu() {
    if (jdeWebGUIcurrentMenu != null)
        jdeWebGUIcurrentMenu.style.clip = "rect(auto auto auto auto)";
}

function jdeWebGUIshowSubMenu() {
    if (jdeWebGUIcurrentSubMenu != null)
        jdeWebGUIcurrentSubMenu.style.clip = "rect(auto auto auto auto)";
}

//This function Check is the ActiveX Control available in the Current E1Page.
function isActiveXAvailable() {

    var iframe = document.getElementById('e1menuAppIframe');
    //For the Hover popup iframe is null, need to check the window.parent
    try {
        if (iframe == null)
            iframe = window.parent.document.getElementById('e1menuAppIframe');
    }
    catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
        iframe = null;
    }

    if (iframe != null) {
        var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
        if (innerDoc != null && innerDoc != undefined) {
            var e1formDivElem = innerDoc.getElementById("e1formDiv");
        }

        if (e1formDivElem != null) {
            //Check is the ActiveX Control/ embeded SVG Viewer available in DOM
            var activeXObj = e1formDivElem.getElementsByTagName("OBJECT")[0] || e1formDivElem.getElementsByTagName("embed")[0];
            //check for the moItemview <Iframe> tag
            var moItemvIframe = e1formDivElem.getElementsByTagName("Iframe")[0];
            //check for RIFramework Element
            var frameworkElem = innerDoc.getElementById("RIFramework");
            if (frameworkElem)
                var subContainers = frameworkElem.getElementsByTagName("Iframe");
            //if activeXObj available return true
            if ((activeXObj) || (moItemvIframe != null && ((moItemvIframe.id == "moItemView") || (moItemvIframe.id == "moPDFViewer")))
            || (subContainers != null && subContainers.length > 0)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    return false;
}


/*
showItemShadow() adds the <Iframe> and six Divs to
get the shadow affect. omitShadow is an optional parameter.
pass true to omitShadow, if you don't want to display the
shadow.popupType is also Optional Parameter.
*/
function showItemShadow(el, maskFrame, omitShadow, popupType) {
    // IE has problems with comboboxes and native controls. Putting an iframe underneath fixes it.
    if (isIE && isActiveXAvailable())  //IE
    {
        if (maskFrame != null) {
            maskFrame.attachMask(el);
        }
        else {
            createMaskFrameForElement(el)
        }
    }

    if (!omitShadow) {
        var elmID = el.id + "-shadow";
        var elmBorder = document.getElementById(elmID + 0);
        if (elmBorder == null) {
            if ((el.style.zIndex == undefined) || ((isIOS ? new Number(el.style.zIndex).toFixed(0) : parseInt(el.style.zIndex)) <= 2)) {
                el.style.zIndex = 3;
            }
            var elTop = parseInt(el.style.top);
            var elLeft = parseInt(el.style.left);
            var elWidth = el.offsetWidth;
            var elHeight = el.offsetHeight;
            var isRtl = (document.documentElement.dir == "rtl");
            var isSafari = false;
            if (popupType) {
                var ssTop = elTop + 15;
                var ssLeft = elLeft + 8;
            }
            else {
                var ssTop = elTop + 1;
                var ssLeft = elLeft;
            }
            for (var i = 0; i < 6; i++) {
                var shade = document.createElement('div');
                var ss = shade.style;
                ss.position = "absolute";
                ss.left = ssLeft;
                ss.top = ssTop;
                if (i % 2 == 0) {
                    if (isRtl) {
                        if (popupType)
                            ssLeft++;
                        else
                            ssLeft--;
                    }
                    else {
                        ssLeft++;
                    }
                }
                else {
                    if (popupType)
                        ssTop--;
                    else
                        ssTop++;
                }

                if (popupType) {
                    ss.width = elWidth - 9;
                    ss.height = elHeight - 13;
                }
                else {
                    ss.width = elWidth;
                    ss.height = elHeight;
                }

                if (!isSafari && (null != el.style.zIndex))
                    ss.zIndex = parseInt(el.style.zIndex) - 1;
                else
                    ss.zIndex = 2;
                ss.backgroundColor = "#000000";

                if (popupType)
                    webguiSetOpacity(shade, 9);
                else
                    webguiSetOpacity(shade, 10);

                if (el.nextSibling)
                    el.parentNode.insertBefore(shade, el.nextSibling);
                else
                    el.parentNode.appendChild(shade);
                shade.id = elmID + i;
            }
            if (el.id != g_caroTabContextMenuId) {
                webguiSetOpacity(el, 99);
            }
        }
    }
}

function webguiSetOpacity(el, opacityPercent) {
    el.style.opacity = (opacityPercent / 100);
    el.style.filter = "alpha(opacity=" + opacityPercent + ")";
    el.style.MozOpacity = (opacityPercent / 100);
}

function moveItemShadow(el) {
    if (isIE)  //IE
    {
        var maskframe = document.getElementById(el.id + "-maskframe");
        if (null != maskframe) {
            maskframe.style.top = parseInt(el.style.top) + "px";
            maskframe.style.left = parseInt(el.style.left) + "px";
            maskframe.style.width = el.offsetWidth + "px";
            //Adjust the Iframe Height for Preference POPUP
            if (el.id == "UI_Pref")
                maskframe.style.height = el.offsetHeight - 4 + "px";
            else
                maskframe.style.height = el.offsetHeight + "px";
        }
    }

    var elmID, elmBorder;
    elmID = el.id + "-shadow";
    var elTop = parseInt(el.style.top);
    var elLeft = parseInt(el.style.left);
    var elWidth = el.offsetWidth;
    var elHeight = el.offsetHeight;
    var isRtl = (document.documentElement.dir == "rtl");
    var ssTop = elTop + 1;
    var ssLeft = elLeft;
    for (var i = 0; i < 6; i++) {
        elmBorder = document.getElementById(elmID + i);
        if (elmBorder != null) {
            var ss = elmBorder.style;
            ss.left = ssLeft;
            ss.top = ssTop;
            if (i % 2 == 0) {
                if (isRtl)
                    ssLeft--;
                else
                    ssLeft++;
            }
            else
                ssTop++;
            ss.width = elWidth;
            ss.height = elHeight;
        }
    }
}

/*
This function detach/remove the maskIFrame
added to the Element.omitShadow is a Optional Parameter.
*/
function removeItemShadow(el, maskFrame, omitShadow) {
    if (isIE)  //IE
    {
        var itemMask = (maskFrame != null) ? maskFrame : window.GlobalMaskFrame;
        itemMask.detachMask();
    }

    if (!omitShadow) {
        var elmID, elmBorder;
        elmID = el.id + "-shadow";
        for (var i = 0; i < 6; i++) {
            elmBorder = document.getElementById(elmID + i);
            if (elmBorder != null) {
                var elmBorderParent = elmBorder.parentNode;
                elmBorderParent.removeChild(elmBorder);
            }
        }
    }

    removeMaskForElement(el);
}

var jdeWebGUIpreventHideAll = false;
var jdeWebGUImenuStack = new Array(50);
var formexitfavstackSize = 0;
var rowexitfavstackSize = 0;
var formexitfavmenuStack = new Array();
var rowexitfavmenuStack = new Array();
// the following two variables are used to keep track of focus for keyboard access
var prevFocusedMenuItems = new Array();
var prevMenuItemIdx = 0;

function jdeWebGUIdoToggleSubMenuOnKeyDown(Parent, MenuID, isBaseMenu, evt) {
    jdeWebGUIdoToggleSubMenu(Parent, MenuID, evt, isBaseMenu);
    jdeWebGUIPopupMenuEventHandlerEnabled = true;
}

function jdeWebGUIHighlightAndFocus(menuElement, Parent) {
    var hilightableMenuItemOuterElement = jdeWebGUIGetFirstVisibleDescendentWithFlag(menuElement, "jdeMenuHighlightable", "yes");
    if (hilightableMenuItemOuterElement != null) {
        jdeWebGUIHighlightMenuItem(hilightableMenuItemOuterElement);
    }

    // set the focus on the menu item
    if (prevMenuItemIdx >= 0)
        prevFocusedMenuItems[prevMenuItemIdx] = Parent;
    if (hilightableMenuItemOuterElement && hilightableMenuItemOuterElement.parentNode)
        hilightableMenuItemOuterElement.parentNode.focus();
}

function jdeWebGUIHighlightMenuItem(element) {
    jdeWebGUIUnhighlightAllItemsInCurrentMenu(element);
    element.className = "HoverMenuItem";
    element.setAttribute("highlighted", "yes");
}

function jdeWebGUIUnhighlightAllItemsInCurrentMenu(element) {
    var siblingNodes = element.parentNode.parentNode.childNodes;
    for (var i = 0; i < siblingNodes.length; i++) {
        var node = siblingNodes[i];
        if (node.tagName == "DIV" || node.tagName == "SPAN") {
            node = node.firstChild;
            if (node != null && node.className == "HoverMenuItem") {
                jdeWebGUIUnhighlightMenuItem(node);
            }
        }
    }
}

function jdeWebGUIHighlightMenuItemById(Id) {
    var element = document.getElementById(Id).childNodes[0];
    if (element)
        jdeWebGUIHighlightMenuItem(element);
}

function jdeWebGUIUnhighlightMenuItem(menuItemElement) {
    if (menuItemElement != null) {
        menuItemElement.className = "MenuNormal";
        menuItemElement.removeAttribute("highlighted");
    }
}

function menuItemMouseDown(ID, e) {
    var menuID;
    var isRTL = document.documentElement.dir == "rtl";

    var element = document.getElementById(ID);
    if (isIE) {
        if (!isRTL) {
            menuID = element.parentNode.id;
        }
        else {
            menuID = element.ownerDocument.activeElement.id;
        }
    }
    //FF Check
    if (!isIE) {
        // Firefox 3 begin to support HTML5 include document.getElementsByClassName
        // The easier way to distiguish FF2 and FF3 is to check document.getElementsByClassName.
        if (!document.getElementsByClassName) // firefox version < 3
        {
            menuID = element.parentNode.parentNode.parentNode.parentNode.parentNode.id;
        }
        else {
            // FF3
            if (element.ownerDocument.activeElement.firstChild != undefined)
                menuID = element.ownerDocument.activeElement.firstChild.id;
            //if the Context menu is on Form/Grid,need to get the MenuID from DOM differently in FF3.6/FF3.0
            if (menuID == null)
                menuID = element.parentNode.parentNode.parentNode.parentNode.parentNode.id;
        }
    }

    //Drag N drop feature is not available in ToolsMenu/Report_Exit/View_Exit
    if (menuID == "ToolsMenu" || menuID.indexOf("Report_Exit") > 0 || menuID.indexOf("View_Exit") > 0) {
        if (isIE) {
            e.cancelBubble = true;
            e.returnValue = false;
        }
        else {
            e.stopPropagation();
            e.preventDefault();
        }
        return;
    }
    //disable the highlight of menuitem
    if (element) {
        var origItem = element.childNodes[0];
        jdeWebGUIUnhighlightMenuItem(origItem);

        //A representation of the menuItem that moves along with mouse
        var menuItemDragClone = element.cloneNode(true);
        menuItemDragClone.childNodes[0].className = "";
        menuItemDragClone.className = 'menuItemDragClone';
        menuItemDragClone.setAttribute('id', 'menuItemDragClone');

        menuItemDragClone.style.backgroundColor = "#D9F4FA";
        menuItemDragClone.style.height = origItem.offsetHeight + "px";
        menuItemDragClone.style.width = origItem.offsetWidth + "px";

        document.body.appendChild(menuItemDragClone);
        dragDrop.initElement(element);
    }
}

//dragdrop object
dragDrop = {
    draggedObject: undefined,
    initElement: function (element) {
        element.onmousedown = dragDrop.startDragMouse;
    },
    startDragMouse: function (e) {
        dragDrop.startDrag(this);
        var evt = e || window.event;
        addMouseEvent(document, 'mousemove', dragDrop.dragMouse);
        addMouseEvent(document, 'mouseup', dragDrop.releaseElement);
        return false;
    },
    startDrag: function (obj) {
        if (dragDrop.draggedObject)
            dragDrop.releaseElement();
        dragDrop.draggedObject = obj;
        obj.className += ' dragged';
        //code for autoscroll starts
        var isRTL = document.documentElement.dir == "rtl";

        if (!isRTL) {
            dragElementMenuID = dragDrop.draggedObject.parentNode.parentNode.parentNode.parentNode.parentNode.id;
        }
        else {
            dragElementMenuID = getHyperExitMenuId(dragDrop.draggedObject);  // RTL
        }

        // code to check the multilevel dropdown starts
        if (dragElementMenuID) {
            var elem = document.getElementById(dragElementMenuID);
            if (elem.parentNode.children[1].id && elem.parentNode.children[1].id !== dragElementMenuID)
                dragElementMenuID = elem.parentNode.children[1].id;
        }

        // code to check the multilevel dropdown ends
        // code for autoscroll ends
    },
    dragMouse: function (e) {
        if (dragDrop.draggedObject) {
            var evt = e || window.event;

            var curpos = getCursorPosition(e);

            //Take into account the scroll positions on the 'e1formDiv' also (after HFE - lock toolbar project)
            var formDiv = document.getElementById('e1formDiv');
            var formDivScrollTop = 0;
            var formDivScrollLeft = 0;

            if (formDiv) {
                formDivScrollTop = formDiv.scrollTop;
                formDivScrollLeft = formDiv.scrollLeft;
            }

            if (menuItemDragClone.style != null) {
                menuItemDragClone.style.left = curpos[0] + 1 - formDivScrollLeft + "px";
                menuItemDragClone.style.top = curpos[1] - 18 - formDivScrollTop + "px";
                //set zIndex to display the Drag Clone on top of the page
                menuItemDragClone.style.zIndex = 30000;
                //code to auto scroll the scrollbar begins   
                if (dragElementMenuID) {
                    // console.log('dragElementMenuID = '+dragElementMenuID);
                    var divIdObj = document.getElementById(dragElementMenuID);
                    var scrollTopValue = divIdObj.scrollTop;
                    if (parseInt(menuItemDragClone.style.top) < 30)  // scroll up 
                    {
                        if (scrollTopValue > 0) {
                            favIntervalScroll = setInterval(favDivScroll(dragElementMenuID, iScrollFactor * -1), 50); //scroll up
                        }
                        else {
                            clearIntervalScroll();
                        }
                    }
                }
                //code to auto scroll the scrollbar ends
            }

            return false;
        }
    },
    releaseElement: function (e) {
        var dragObjMenuID;
        var isRTL = document.documentElement.dir == "rtl";
        var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);
        //code for autoscroll starts
        clearIntervalScroll();
        //code for autoscroll ends
        if (!isRTL) {
            dragObjMenuID = dragDrop.draggedObject.parentNode.parentNode.parentNode.parentNode.parentNode.id;
        }
        else {
            dragObjMenuID = getHyperExitMenuId(dragDrop.draggedObject);  // RTL
        }

        if (dragObjMenuID.indexOf("Form_Exit") == -1 || dragObjMenuID.indexOf("Row_Exit") == -1) {
            var menuType = getMenuType(dragObjMenuID);
        }

        if (menuType == "FORM_EXIT_TYPE") {
            var dragObjectClone = dragDrop.draggedObject.cloneNode(true);
            // Favorites menuItem ID starts with outerFEFA
            if (dragObjectClone.id.indexOf("outerFEFA") == 0) {
                var selectedElement = getSelectedElement();

                // Check if the favmenu item is Drag N dropped on another fav menuitem.Don't allow the invalid Drag N Drop
                if (selectedElement != undefined && dragObjectClone.id != selectedElement.id && selectedElement.id.indexOf("outerFEFA") != 0) {
                    removeFormExitFavMenuStack(dragObjectClone.id.substring(5));
                    getDtaInstance().doAsynPostById("formExitFavRemove", null, dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") + 1));
                }

                // If the selectedElement menuItem ID starts with outerFEFA, It means the menuItem Drag and dropped on the another favorite menuItem
                //Note: Below if loop code has logic to re-order the favorites
                else if (selectedElement != undefined && dragObjectClone.id != selectedElement.id && selectedElement.id.indexOf("outerFEFA") == 0) {
                    var selectedElmID = selectedElement.id.substring(5);
                    var dragCloneID = dragObjectClone.id.substring(5);

                    // get the OldPos, NewPos of the favorites menuitems from the formExitarray
                    var favOldPos = formexitfavmenuStack.findIndex(dragCloneID);
                    var favNewPos = formexitfavmenuStack.findIndex(selectedElmID);
                    if (selectedElmID != undefined && favNewPos != -1) {
                        getDtaInstance().doAsynPostById("formExitFavReorder", null, favOldPos + '|' + dragObjectClone.id.substring(11) + '|' + favNewPos + '|' + selectedElement.id.substring(11));
                    }
                }
                else {
                    // remove dragObjectClone
                    dragObjectClone = null;
                }
            }
            else {
                var origDragCloneID = dragObjectClone.id;

                // ID comes with prefix outerHE- Replace the HE with FA[Favorites]
                // to maintain unique ID's for the FormExit/RowExit Fav MenuItems
                dragObjectClone.setAttribute('id', dragObjectClone.id.replace("HE", "FEFA"));

                //replace all Occurrences of HE with FA
                dragObjectClone.innerHTML = dragObjectClone.innerHTML.replace(/HE/g, "FEFA");

                // in webClient the DragCloneID starts with prefix outerFEFA+FullQulifiedID of menuItem(ex:0_89)
                // In WSRP we get the namespace appended to MenuID, remove the namespace from the ID to ensure
                // that it works similarly as Normal webClient
                if (isWSRP) {
                    var favFormPrefix = dragObjectClone.id.substring(0, dragObjectClone.id.indexOf("_"));
                    dragObjectClone.id = favFormPrefix.concat(dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") - 1));
                }

                //if it's a New Formexit favorite
                if (isFavoritesSection(e) && isNewFormExitFavorite(dragObjectClone.id)) {
                    if (isWSRP) {
                        addFormExitFavMenuStack(favFormPrefix.concat(dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_"))));
                        getDtaInstance().doAsynPostById("formExitFavAdd", null, dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") + 1));
                    }
                    else {
                        addFormExitFavMenuStack(dragObjectClone.id.substring(5));
                        getDtaInstance().doAsynPostById("formExitFavAdd", null, dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") + 1));
                    }
                }
                else {
                    dragObjectClone = null;
                }
            }
        }
        else if (menuType == "ROW_EXIT_TYPE") {
            var dragObjectClone = dragDrop.draggedObject.cloneNode(true);

            //Favorite Row Exit menuitem ID comes with the prefix -outerREFA
            if (dragObjectClone.id.indexOf("outerREFA") == 0) {
                var selectedElement = getSelectedElement();

                // check if the favmenu item is droped on another fav menuitem (OR) User did not do a proper Drag n drop,don't allow
                if (selectedElement != undefined && dragObjectClone.id != selectedElement.id && selectedElement.id.indexOf("outerREFA") != 0) {
                    //remove the Fav menuItem from the rowexitfavmenu Array
                    removeRowExitFavMenuStack(dragObjectClone.id.substring(5));
                    getDtaInstance().doAsynPostById("rowExitFavRemove", null, dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") + 1));
                }

                // If the selectedElement menuItem ID starts with outerREFA, It means the menuItem Drag and dropped on the
                // another favorite menuItem
                else if (selectedElement != undefined && dragObjectClone.id != selectedElement.id && selectedElement.id.indexOf("outerREFA") == 0) {
                    var selectedElmID = selectedElement.id.substring(5);
                    var dragCloneID = dragObjectClone.id.substring(5);
                    // get the OldPos, NewPos of the favorites menuitems from the formExitarray
                    var favOldPos = rowexitfavmenuStack.findIndex(dragCloneID);
                    var favNewPos = rowexitfavmenuStack.findIndex(selectedElmID);
                    if (selectedElmID != undefined && favNewPos != -1) {
                        getDtaInstance().doAsynPostById("rowExitFavReorder", null, favOldPos + '|' + dragObjectClone.id.substring(11) + '|' + favNewPos + '|' + selectedElement.id.substring(11));
                    }
                }
                else {
                    dragObjectClone = null;
                }
            }
            else {
                var origDragCloneID = dragObjectClone.id;
                // ID comes with prefix outerHE- Replace the HE with FA[Favorites]
                // to maintain unique ID's for the FormExit/RowExit Fav MenuItems
                dragObjectClone.setAttribute('id', dragObjectClone.id.replace("HE", "REFA"));

                //replace all Occurrences of HE with FA
                dragObjectClone.innerHTML = dragObjectClone.innerHTML.replace(/HE/g, "REFA");

                if (isWSRP) {
                    var favRowPrefix = dragObjectClone.id.substring(0, dragObjectClone.id.indexOf("_"));
                    dragObjectClone.id = favRowPrefix.concat(dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") - 1));
                }

                if (isFavoritesSection(e) && isNewRowExitFavorite(dragObjectClone.id)) {
                    if (isWSRP) {
                        addRowExitFavMenuStack(favRowPrefix.concat(dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_"))));
                        getDtaInstance().doAsynPostById("rowExitFavAdd", null, dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") + 1));
                    }
                    else {
                        addRowExitFavMenuStack(dragObjectClone.id.substring(5));
                        getDtaInstance().doAsynPostById("rowExitFavAdd", null, dragObjectClone.id.substring(dragObjectClone.id.lastIndexOf("_") + 1));
                    }
                }
                else {
                    // remove dragObjectClone
                    dragObjectClone = null;
                }
            }
        }

        //reset the Color back
        dragDrop.draggedObject.style.backgroundColor = 'transparent';
        if (dragObjectClone != null)
            dragObjectClone.style.backgroundColor = 'transparent';

        //Reset the JIT handlers
        removeMouseEvent(document, 'mousemove', dragDrop.dragMouse);
        removeMouseEvent(document, 'mouseup', dragDrop.releaseElement);
        dragDrop.draggedObject.className = dragDrop.draggedObject.className.replace(/dragged/, '');
        dragDrop.draggedObject = null;

        //Remove the dragged 'clone'
        var menuItemDragClone = document.getElementById('menuItemDragClone');
        if (menuItemDragClone != null)
            document.body.removeChild(menuItemDragClone);
    }
}

function isFavoritesSection(e) {
    var srcElement = isIE ? event.srcElement : e.target;
    while (srcElement.parentNode) {
        srcElement = srcElement.parentNode;
        // stop search when we find a accend who is row exit or form exit fravorite or className is FavoritesLabel
        if (srcElement.id == "FavoritesLabel1" || srcElement.id == "FavoritesLabel2" || (srcElement.id && (srcElement.id.indexOf('REFA') != -1 || srcElement.id.indexOf('FEFA') != -1)))
            return true;
    }

    return false;
}

function isNewFormExitFavorite(dragObjectCloneID) {
    var newFormExitFav = true;
    var formexitfavstackSize = 0;

    // Check is the menuItem is a New FormExit Favorite Item or Not
    if ((formexitfavmenuStack[formexitfavstackSize] != null) && (formexitfavmenuStack[formexitfavstackSize] != "")) {
        for (i = 0; i < formexitfavmenuStack.length; i++) {
            var dragObjId = dragObjectCloneID.substring(5);
            if (window.namespace)
                dragObjId = dragObjId.replace(namespace, "");
            if (formexitfavmenuStack[i] == dragObjId) {
                newFormExitFav = false;
            }
        }
    }
    return newFormExitFav;
}


function getHyperExitMenuId(currentNode) {
    // if more siblings, go up to parentNode and get Root Elem
    var parentNode = currentNode;
    while (parentNode.parentNode) {
        parentNode = parentNode.parentNode;
        if (parentNode.nodeName == "DIV" && parentNode.className == "MenuDropdownBack_rtl") {
            return parentNode.id;
        }
    }
}

function isNewRowExitFavorite(dragObjectCloneID) {
    var newRowExitFav = true;
    var rowexitfavstackSize = 0;

    // Check is the menuItem is a New RowExit Favorite Item or Not
    if ((rowexitfavmenuStack[rowexitfavstackSize] != null) && (rowexitfavmenuStack[rowexitfavstackSize] != "")) {
        for (i = 0; i < rowexitfavmenuStack.length; i++) {
            var dragObjId = dragObjectCloneID.substring(5);
            if (window.namespace)
                dragObjId = dragObjId.replace(namespace, "");
            if (rowexitfavmenuStack[i] == dragObjId) {
                newRowExitFav = false;
            }
        }
    }

    return newRowExitFav;
}

function getMenuType(dragObjMenuID) {
    var menuType;

    if (dragObjMenuID.indexOf("Form_Exit") > 0 || dragObjMenuID.indexOf("FORM_EXIT_BUTTON") == 0) {
        menuType = "FORM_EXIT_TYPE";
    }
    else if (dragObjMenuID.indexOf("Row_Exit") > 0 || dragObjMenuID.indexOf("ROW_EXIT_BUTTON") == 0) {
        menuType = "ROW_EXIT_TYPE";
    }
    return menuType;

}

function getDtaInstance() {

    var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);

    if (isIE || isWSRP) {
        return JDEDTAFactory.getInstance(this.namespace);
    }
    else {
        return JDEDTAFactory.getInstance("");
    }
}

function addMouseEvent(obj, evt, fn) {
    if (obj.addEventListener)
        obj.addEventListener(evt, fn, false);
    else if (obj.attachEvent)
        obj.attachEvent('on' + evt, fn);
}

function removeMouseEvent(obj, evt, fn) {
    if (obj.removeEventListener)
        obj.removeEventListener(evt, fn, false);
    else if (obj.detachEvent)
        obj.detachEvent('on' + evt, fn);
}

function addFormExitFavMenuStack(favId) {
    //update the formexit Favorite Array and increment the favstacksize
    formexitfavmenuStack[formexitfavstackSize] = favId;
    while ((formexitfavmenuStack[formexitfavstackSize] != null) && (formexitfavmenuStack[formexitfavstackSize] != "") && (formexitfavstackSize < formexitfavmenuStack.length)) {
        formexitfavstackSize++;
    }
}
function addRowExitFavMenuStack(favId) {
    //update the rowexit Favorite Array and increment the favstacksize
    rowexitfavmenuStack[rowexitfavstackSize] = favId;
    while ((rowexitfavmenuStack[rowexitfavstackSize] != null) && (rowexitfavmenuStack[rowexitfavstackSize] != "") && (rowexitfavstackSize < rowexitfavmenuStack.length)) {
        rowexitfavstackSize++;
    }
}
function removeFormExitFavMenuStack(favId) {
    for (j = 0; j < formexitfavstackSize; j++) {
        if (formexitfavmenuStack[j] == favId) {
            formexitfavmenuStack.splice(j, 1);
            formexitfavstackSize--;
        }
    }
}

function removeRowExitFavMenuStack(favId) {
    for (j = 0; j < rowexitfavstackSize; j++) {
        if (rowexitfavmenuStack[j] == favId) {
            rowexitfavmenuStack.splice(j, 1);
            rowexitfavstackSize--;
        }
    }
}

function jdeWebGUIsetNamespace(namespace) {
    this.namespace = namespace;
}

function getSelectedElement() {
    var thisMenu = document.getElementById(jdeWebGUImenuStack[0]);
    var activeItem = jdeWebGUIGetActiveItemInMenu(thisMenu);

    // get the highlighted MenuItem Element
    if (activeItem != null)
        var selectedElement = activeItem.parentNode;

    return selectedElement;
}

function jdeWebGUIdoToggleSubMenu(Parent, MenuID, event, isBaseMenu) {
    if (MenuID != g_caroTabContextMenuId) {
        if (EUR && MenuID == EUR.MENU_ID_ONE_VIEW && EUR.isListDirty()) {
            /*To avoid object corruption if OVR object launched through LcmEdit 
            onclick of OneView Menu needs to close side panel if already opened only for the first time*/
            //Bug 18562073 - LCM: OV - OVR EDITOR BECOME UNRESPONSIVE AFTER OPENING OVR MENU DROPDOWN 
            JSSidePanelRender.close();
            EUR.goLoadReportList();
        }
    }

    // get the real number of items in this array
    var stackSize = jdeWebGUIGetMenuStackDepth();
    prevMenuItemIdx = stackSize - 1;
    // determine if the stack contains this menu (so we know whether to show it or not)
    var needToShow = true;
    var menuLevel = 0;
    if (MenuID == "") {
        needToShow = false;
    }
    else {
        for (i = 0; i < stackSize; i++) {
            if (jdeWebGUImenuStack[i] == MenuID)
                needToShow = false;
        }
        menuLevel = jdeWebGUIgetMenuLevel(MenuID);
    }
    needToShow = true;
    // hide all submenus that are at the same level as this one or deeper
    for (j = stackSize - 1; j >= 0; j--) {
        if (jdeWebGUIgetMenuLevel(jdeWebGUImenuStack[j]) >= menuLevel) {
            if (isBaseMenu)
                jdeWebGUIhideMenu(Parent, jdeWebGUImenuStack[j], event);
            else
                jdeWebGUIhideSubMenu(Parent, jdeWebGUImenuStack[j], event);

            jdeWebGUImenuStack[j] = "";
        }
    }
    if (needToShow) {
        stackSize = 0;
        while ((jdeWebGUImenuStack[stackSize] != null) && (jdeWebGUImenuStack[stackSize] != "") && (stackSize < jdeWebGUImenuStack.length)) {
            stackSize++;
        }
        if (isBaseMenu)
            jdeWebGUIdoMenu(Parent, MenuID, event);
        else
            jdeWebGUIdoSubMenu(Parent, MenuID, event);
        jdeWebGUImenuStack[stackSize] = MenuID;
    }
    showOSDrawnControls();
    jdeWebGUIUpdateMenuShownFlag();

    var menuElement = document.getElementById(MenuID);
    if (menuElement != null) {
        jdeWebGUIHighlightAndFocus(menuElement, Parent);
    }
}
function jdeWebGUIhideAllMenus(Parent, MenuID, event) {
    if (jdeWebGUIpreventHideAll && event != null) {
        jdeWebGUIpreventHideAll = false;
        event.stopPropagation();
        event.preventDefault();
        return;
    }

    // get the real number of items in this array
    var stackSize = 0;
    while ((jdeWebGUImenuStack[stackSize] != null) && (jdeWebGUImenuStack[stackSize] != "") && (stackSize < jdeWebGUImenuStack.length)) {
        stackSize++;
    }
    // hide all submenus that are at the same level as this one or deeper
    for (j = stackSize - 1; j >= 0; j--) {
        try {
            var thisMenu = document.getElementById(jdeWebGUImenuStack[j]);
            jdeWebGUIUnhighlightMenuItem(jdeWebGUIGetActiveItemInMenu(thisMenu));
            thisMenu.style.display = "none";
            removeItemShadow(thisMenu);
        }
        catch (problem) {
            // the object no longer exists on the page so let's just remove it from the stack
        }
        jdeWebGUImenuStack[j] = "";
    }

    if (jdeWebGUIcurrentMenu != null) {
        jdeWebGUIcurrentMenu.style.display = "none";
        removeItemShadow(jdeWebGUIcurrentMenu);
        jdeWebGUIcurrentMenu = null;
    }
    showOSDrawnControls();
    jdeWebGUIUpdateMenuShownFlag();


    // following the same code style JDEGrid.prototype.removeDocClickEvent 
    if (document.detachEvent) {
        document.detachEvent('onclick', jdeWebGUIPopupMenuEventHandler);
    }
    else {
        document.removeEventListener("touchend", jdeWebGUIPopupMenuEventHandler, false);
        document.removeEventListener("click", jdeWebGUIPopupMenuEventHandler, false);
    }
}

function jdeWebGUIgetMenuLevel(MenuID) {
    var level = MenuID.split("_").length;
    // The ToolsMenu IDs are not set up the same way as the Row_Exit and Form_Exit.
    // Because of this, we need to force the level up so the menu stack hides properly.
    if (MenuID.slice(0, 5) == "Tools")
        level += 3;
    return level;
}

function jdeWebGUIdoSubMenu(Parent, MenuID, event) {
    var thisMenu = document.getElementById(MenuID);
    var parString = MenuID.substring(0, MenuID.lastIndexOf("-"));
    var ParentMenu = document.getElementById(parString);
    var x, y;

    // Reset dropdown menu
    jdeWebGUIcurrentSubMenu = thisMenu;

    // Set dropdown menu display position
    x = parseInt(ParentMenu.style.left) + ParentMenu.offsetWidth;

    y = Parent.offsetTop + document.body.offsetTop;

    var vaParent = Parent.offsetParent;
    while (vaParent && vaParent.id != "WebMenuBar") {
        y += (vaParent.offsetTop - vaParent.scrollTop);
        vaParent = vaParent.offsetParent;
    }

    //lock toolbar -menubar table relative position adjust in IE

    //top banner
    var topBanner = document.getElementById("topimagecell");
    // dont do this for the Collaborate SubMenu under Tools Menu since it is messing up the allignment
    if (topBanner != null && MenuID != "ToolsMenu-ToolsMenu_Collaborate") {
        //adjust with topbanner height plus its border and padding
        y -= (topBanner.offsetHeight + 8);
    }

    //adjust by the tab on modeless forms
    var modelessTabDiv = document.getElementById("modelessTabDiv");
    // dont do this for the Collaborate SubMenu under Tools Menu since it is messing up the allignment
    if (modelessTabDiv != null && MenuID != "ToolsMenu-ToolsMenu_Collaborate") {
        y -= 25;
    }
    var isRTL = (document.documentElement.dir == "rtl");
    thisMenu.style.top = y + "px";
    if (isIOS && isRTL) {
    }
    else {
        thisMenu.style.left = x + "px";
    }

    thisMenu.style.display = "block";
    thisMenu.style.zIndex = 20001; // assuming minimum 32-bit integer

    if (isRTL) {
        var rtlWrapper = document.getElementById(MenuID + "_rtl");
        var theMenuWidth = rtlWrapper.offsetWidth;
        thisMenu.style.width = theMenuWidth + "px";

        x = getAbsoluteLeftPos(Parent.offsetParent);
        // flip menus by changing their left positions
        if (x > theMenuWidth)
            x = x - theMenuWidth;
        else {
            var menuTable = document.getElementById("WebMenuBar");
            if (menuTable != null) {
                x = (menuTable.offsetLeft + x + Parent.offsetParent.offsetWidth) - theMenuWidth;
            }
        }

        thisMenu.style.left = x + "px";
    }

    //HFE - lock toolbar. If the menu is outside the window width, need show it in the left side
    var bodyElem = document.body;
    var availableWidth, availableHeight;

    if (window.innerWidth !== undefined) {
        availableWidth = window.innerWidth;
        availableHeight = window.innerHeight;
    }
    // IE8
    else {
        availableWidth = bodyElem.offsetWidth;
        availableHeight = bodyElem.offsetHeight;
    }

    if ((x + thisMenu.offsetWidth) > availableWidth) {
        x = parseInt(ParentMenu.style.left) - thisMenu.offsetWidth;
        thisMenu.style.left = Math.max(0, x) + "px";
    }

    //if the submenu is outside window, need to show in top
    if ((y + thisMenu.scrollHeight) > availableHeight) {
        var extraHeight = availableHeight - (y + thisMenu.scrollHeight);
        var menuTop = y + extraHeight - 36;
        if (menuTop < 0) {
            thisMenu.style.top = "1px";
            thisMenu.style.overflowY = "scroll";
            thisMenu.style.height = (thisMenu.scrollHeight + menuTop) + "px";
        }
        else {
            thisMenu.style.top = menuTop + "px";
        }
    }

    if (isIE) {
        thisMenu.setActive();
        thisMenu.focus();
    }
    showItemShadow(thisMenu);

    // set the focus on the menu, so that JAWS can read it 
    if (window.userAccessibility)
        thisMenu.focus();

}

function jdeWebGUIhideMenu(Parent, MenuID, event) {
    var thisMenu = document.getElementById(MenuID);
    if (thisMenu != null) {
        var cX, cY;
        var bHideMenu = true;
        //If pass in null event, that is a keydown command, hide it always
        if (event != null) {
            if (window && window.pageXOffset !== undefined) {
                cX = event.clientX + window.pageXOffset;
                cY = event.clientY + window.pageYOffset;
            }
            else {
                // IE8
                cX = event.clientX + document.body.scrollLeft;
                cY = event.clientY + document.body.scrollTop;
            }

            var x, x2, y, y2;

            // Get menu position, width and height
            if (document.all) {
                x = thisMenu.style.posLeft;
                y = thisMenu.style.posTop;
            }
            else {
                x = parseInt(thisMenu.style.left);
                y = parseInt(thisMenu.style.top);
            }

            x2 = x + thisMenu.offsetWidth;
            y2 = y + thisMenu.offsetHeight;

            if (cY > y - 2 && cY < y2 - 2) {
                if (cX > x && cX < x2 - 2) bHideMenu = false;
            }
            // this prevents it from opening multiple exit menus.  32 is the size of the bitmap.
            if (cX > x + 32 && cX < x2) {
                if (cY < y) bHideMenu = true;
            }

            // see if we have popped up a child menu and we are jumping over to it
            if (jdeWebGUIcurrentSubMenu != null) {
                if (document.all) {
                    if ((cX >= jdeWebGUIcurrentSubMenu.style.posLeft && cX <= (jdeWebGUIcurrentSubMenu.style.posLeft + jdeWebGUIcurrentSubMenu.offsetWidth)) &&
                      (cY >= jdeWebGUIcurrentSubMenu.style.posTop && cY <= (jdeWebGUIcurrentSubMenu.style.posTop + jdeWebGUIcurrentSubMenu.offsetHeight))) {
                        bHideMenu = false;
                    }
                }
                else {
                    x = parseInt(jdeWebGUIcurrentSubMenu.style.left);
                    y = parseInt(jdeWebGUIcurrentSubMenu.style.top);

                    if ((cX >= x && cX <= (x + jdeWebGUIcurrentSubMenu.offsetWidth)) &&
                        (cY >= y && cY <= (y + jdeWebGUIcurrentSubMenu.offsetHeight))) {
                        bHideMenu = false;
                    }
                }
            }
        }
        if (!bHideMenu) {
            return;
        }
        jdeWebGUIUnhighlightMenuItem(jdeWebGUIGetActiveItemInMenu(thisMenu));
        if (jdeWebGUIcurrentMenu != null) {
            jdeWebGUIcurrentMenu.style.display = "none";
            removeItemShadow(jdeWebGUIcurrentMenu);
            jdeWebGUIcurrentMenu = null;
        }
        thisMenu.style.display = "none";
        removeItemShadow(thisMenu);
        showOSDrawnControls();
    }

    if (!document.all && document.body.style.overflow == "auto")
        document.body.style.overflow == "hidden";
}

function jdeWebGUIhideSubMenu(Parent, MenuID, event) {
    if (Parent) {
        var menuToBeShown = Parent.parentNode.id;
        // if the parent of this element is a div with "-Show" at the end of the ID
        // then we are already in the menu we want to be showing.
        if (menuToBeShown == MenuID + "-Show") {
            return;
        }

        // otherwise, find the nearest parent that has jdewebguipopupmenu="yes"
        // and if that id is the menu we want to show, we're in the right place.
        else if (menuToBeShown.slice(0, 5) == "outer") {
            var elem = Parent.parentNode;
            while (elem && (elem.getAttribute("jdewebguipopupmenu") != "yes")) {
                elem = elem.parentNode;
            }
            if (elem) {
                if (MenuID == elem.id) {
                    return;
                }
            }
        }
    }

    // neither of the above conditions existed, so we need to hide the menu.
    var thisMenu = document.getElementById(MenuID);

    if (thisMenu == null) return;
    jdeWebGUIUnhighlightMenuItem(jdeWebGUIGetActiveItemInMenu(thisMenu));
    jdeWebGUIcurrentSubMenu = null;
    thisMenu.style.display = "none";
    removeItemShadow(thisMenu);

    if (!document.all && document.body.style.overflow == "auto")
        document.body.style.overflow == "hidden";
}

function jdeWebGUIHideLastLevelMenu() {
    var lastLevelMenuID = jdeWebGUIGetLastLevelMenuID();
    var menuDepth = jdeWebGUIGetMenuStackDepth();
    if (menuDepth == 1) {
        jdeWebGUIhideMenu(null, lastLevelMenuID, null);
    }
    else if (menuDepth > 1) {
        jdeWebGUIhideSubMenu(null, lastLevelMenuID, null);
        prevFocusedMenuItems[menuDepth - 2].focus();
    }
    jdeWebGUImenuStack[menuDepth - 1] = "";
    jdeWebGUIUpdateMenuShownFlag();
    if (g_jdeWebGUIMenuShown == false)
        showOSDrawnControls();
    jdeWebGUICheckToRestorePrevFocus();
    return true;
}

function jdeWebGUIGetMenuStackDepth() {
    var stackSize = 0;
    while ((jdeWebGUImenuStack[stackSize] != null) && (jdeWebGUImenuStack[stackSize] != "") && (stackSize < jdeWebGUImenuStack.length)) {
        stackSize++;
    }
    return stackSize;
}

function jdeWebGUIGetLastLevelMenuID() {
    var menuDepth = jdeWebGUIGetMenuStackDepth();
    if (menuDepth > 0)
        return jdeWebGUImenuStack[menuDepth - 1];
    else
        return null;
}

function jdeWebGUIGetActiveMenuItem() {
    var menuDepth = jdeWebGUIGetMenuStackDepth();
    var activeItem = null;
    if (menuDepth > 0) {
        var menuElement = document.getElementById(jdeWebGUIGetLastLevelMenuID());
        activeItem = jdeWebGUIGetActiveItemInMenu(menuElement);
    }
    return activeItem;
}

function jdeWebGUIGetActiveItemInMenu(menuElement) {
    if (menuElement != null) {
        return jdeWebGUIGetFirstVisibleDescendentWithFlag(menuElement, "highlighted", "yes");
    }
    return null;
}

function jdeWebGUIRecurseHideMenu(MenuID, event) {
    var thisMenu = document.getElementById(MenuID);
    var Parent = document.getElementById(MenuID + '-Show');
    var bDoNotHide = false;

    if (thisMenu != null) {
        var cX, cY;
        if (window && window.pageXOffset !== undefined) {
            cX = event.clientX + window.pageXOffset;
            cY = event.clientY + window.pageYOffset;
        }
        else {
            // IE8
            cX = event.clientX + document.body.scrollLeft;
            cY = event.clientY + document.body.scrollTop;
        }

        // discount the offset of the E1PaneDiv (created from CafeOne project)
        var e1PaneDiv = document.getElementById("E1PaneDIV");
        if (e1PaneDiv) {
            cX = cX - getAbsoluteLeftPos(e1PaneDiv);
            cY = cY - getAbsoluteTopPos(e1PaneDiv);
        }

        var x, x2, y, y2;
        var bHideMenu = true;
        var lastIndex = -1;
        var tempStr = MenuID;
        var tempMenu;

        // Get menu position, width and height
        if (document.all) {
            x = thisMenu.style.posLeft;
            y = thisMenu.style.posTop;
        }
        else {
            x = parseInt(thisMenu.style.left);
            y = parseInt(thisMenu.style.top);
        }

        x2 = x + thisMenu.offsetWidth;
        y2 = y + thisMenu.offsetHeight;

        if (cY < (y + Parent.offsetHeight - 2) && cY > y) {
            if (cX >= (x2 - 2) && cX <= (x2 + Parent.offsetWidth - 2)) bHideMenu = false;
        }

        if (!bHideMenu) {
            return;
        }

        if (cY > y + 2 && cY < y2 - 2) {
            if (cX > (x - 3) && cX < x2 - 2) bHideMenu = false;
        }

        // see if we have popped up a child menu and we arejumping over to it
        if (jdeWebGUIcurrentSubMenu != null && jdeWebGUIcurrentSubMenu != thisMenu) {
            if (!document.all) {
                if ((cX >= jdeWebGUIcurrentSubMenu.style.posLeft && cX <= (jdeWebGUIcurrentSubMenu.style.posLeft + jdeWebGUIcurrentSubMenu.offsetWidth)) &&
                     (cY >= jdeWebGUIcurrentSubMenu.style.posTop && cY <= (jdeWebGUIcurrentSubMenu.style.posTop + jdeWebGUIcurrentSubMenu.offsetHeight))) {
                    bHideMenu = false;
                }
            }
            else {
                x = parseInt(jdeWebGUIcurrentSubMenu.style.left);
                y = parseInt(jdeWebGUIcurrentSubMenu.style.top);

                if ((cX >= x && cX <= (x + jdeWebGUIcurrentSubMenu.offsetWidth)) &&
                    (cY >= y && cY <= (y + jdeWebGUIcurrentSubMenu.offsetHeight))) {
                    bHideMenu = false;
                }
            }
        }

        if (!bHideMenu) {
            return;
        }

        jdeWebGUIcurrentSubMenu = null;
        thisMenu.style.display = "none";
        removeItemShadow(thisMenu);
        // now recursively start hiding the parent menus
        while (true) {
            lastIndex = tempStr.lastIndexOf("_");
            if (lastIndex == -1)
                break;
            // get the previous menu
            tempStr = tempStr.substring(0, lastIndex);
            tempMenu = document.getElementById(tempStr);

            // if the mouse is on the previous menu dont hide it
            if (document.all) {
                if ((cX >= tempMenu.style.posLeft && cX <= (tempMenu.style.posLeft + tempMenu.offsetWidth)) &&
                     (cY >= tempMenu.style.posTop && cY <= (tempMenu.style.posTop + tempMenu.offsetHeight))) {
                    bDoNotHide = true;
                    break;
                }
            }
            else {
                x = parseInt(tempMenu.style.left);
                y = parseInt(tempMenu.style.top);
                if ((cX >= x && cX <= (x + tempMenu.offsetWidth)) &&
                    (cY >= y && cY <= (y + tempMenu.offsetHeight))) {
                    bDoNotHide = true;
                    break;
                }
            }
            // just hide the menu
            tempMenu.style.display = "none";
            removeItemShadow(tempMenu);
            showOSDrawnControls();
        }

        if (!bDoNotHide) {
            jdeWebGUIcurrentMenu = null;
        }
    }
}

/////Menu Key Handling

//Check if the DOM element has the passed attribute with the passed in value
function jdeWebGUIIsElementAttributeOn(node, attribute, value) {
    return ((node != null) && node.getAttribute && (node.getAttribute(attribute) == value));
}

function jdeWebGUIGetFirstVisibleDescendentWithFlag(parent, attribute, value) {
    if (parent.hasChildNodes()) {
        var childNodes = parent.childNodes;
        for (var i = 0; i < childNodes.length; i++) {
            var child = childNodes[i];
            if (child.style && child.style.display != "none") {
                if (jdeWebGUIIsElementAttributeOn(child, attribute, value)) {
                    return child;
                }
            }
            var result = jdeWebGUIGetFirstVisibleDescendentWithFlag(child, attribute, value);
            if (result != null)
                return result;
        }
    }
    return null;
}

function jdeWebGUIGetPreviousVisibleElementWithFlag(currentNode, attribute, value) {
    // for each previous sibling (examine its first child)
    var potentialNode = currentNode;
    do {
        if (potentialNode.previousSibling)
            potentialNode = potentialNode.previousSibling;
        else {
            potentialNode = potentialNode.parentNode.lastChild;
        }
        if (potentialNode == currentNode)
            break;

        if (potentialNode.style && potentialNode.style.display == "none")
            continue;
        if (potentialNode.firstChild && jdeWebGUIIsElementAttributeOn(potentialNode.firstChild, attribute, value)) {
            return potentialNode.firstChild; // we found it!
        }
    } while (potentialNode != currentNode);
    return null;
}


function jdeWebGUIGetNextVisibleElementWithFlag(currentNode, attribute, value) {
    // for each next sibling (examine its first child)
    var potentialNode = currentNode;
    do {
        if (potentialNode.nextSibling)
            potentialNode = potentialNode.nextSibling;
        else {
            potentialNode = potentialNode.parentNode.firstChild;
        }
        if (potentialNode == currentNode)
            break;

        if (potentialNode.style && potentialNode.style.display == "none")
            continue;
        if (potentialNode.firstChild && jdeWebGUIIsElementAttributeOn(potentialNode.firstChild, attribute, value)) {
            return potentialNode.firstChild; // we found it!
        }
    } while (potentialNode != currentNode);
    return null;
}


/////End Menu Key handling

function declareOSDrawnControl(anotherControlID) {
}

function temporarilyExcludeOSDrawnControl(controlID) {
}

function hideOSDrawnControls() {
}

function showOSDrawnControls() {

}

// Reset the menuPlaceholderObject back to its initial state.
function initializeMenuPlaceholderObject() {
    // Initialize the placeholder.
    menuPlaceholderObject.isRequired = true;
    menuPlaceholderObject.lastDisabledID = null;
}

/*
* The function that follows essentially mimics getWebGUIMenuItemInnerHTML() but is streamlined
* for inner HTML with no event handling and a purpose that is merely to display placeholder
* text (e.g., "[Empty]").
*/
function getWebGUIMenuItemEmptyInnerHTML(MenuItemID) {
    var writeString = "";
    writeString = "<table jdeWebGUIPopupMenu=\"yes\" ID=\"" + MenuItemID + "\" cellpadding=0 cellspacing=0 width=100% style=\"cursor: default;\"";
    writeString += "><tr jdeWebGUIPopupMenu=\"yes\"><td jdeWebGUIPopupMenu=\"yes\"><nobr jdeWebGUIPopupMenu=\"yes\">&nbsp;";
    var imgString = "<IMG jdeWebGUIPopupMenu=\"yes\" align=absmiddle border=0 width=9 height=10 SRC='";
    //Display the image only for normal mode.
    imgString += window["WEBGUIRES_images_menuitemblank_gif"] + "' alt=''>";
    writeString += imgString;
    writeString += "&nbsp;</nobr></td><td jdeWebGUIPopupMenu=\"yes\" style=\"width: 100%;\" class=";
    writeString += "MenuItem";
    writeString += "><span jdeWebGUIPopupMenu=\"yes\" CLASS=";
    writeString += "MenuItem";
    writeString += "><nobr jdeWebGUIPopupMenu=\"yes\">" + emptyString + "&nbsp;</nobr>";
    writeString += "</span>";
    writeString += "</td><td jdeWebGUIPopupMenu=\"yes\" align=right> ";
    writeString += " <img src='" + window["WEBGUIRES_images_spacer_gif"] + "' alt=\"\" width=10 height=1>";
    writeString += "</td></tr></table>";
    return writeString;
}

/*
* This function examines a disabled menu item and its siblings.  If none of the
* siblings are enabled, the disabled menu item is altered to contain a
* placeholder signifying that the submenu it is in has nothing in it.
*/
function jdeWebGUIAlterMenuForEmptyContent(theElement, hyperExitID) {
    try {
        if (theElement != null && hyperExitID != null) {
            var currentElement = getNextSiblingOfElement(theElement);
            var continueSearch = true;

            // Look at the siblings after us--any single, non-label one enabled?
            while (continueSearch == true && currentElement != null) {
                // Looking for a jdemenuhighlightable attribute NOT related to a label.
                if ((currentElement.getAttribute("jdeMenuHighlightable") == "yes") &&
                    (currentElement.id.indexOf("FormRowLabel") == -1) &&
                    (currentElement.id.indexOf("FavoritesLabel") == -1)) {
                    continueSearch = false;
                    break;
                }
                currentElement = getNextSiblingOfElement(currentElement);
            }

            // Look at the siblings before us--any single, non-label one enabled?
            currentElement = getPreviousSiblingOfElement(theElement);
            while (continueSearch == true && currentElement != null) {
                if ((currentElement.getAttribute("jdeMenuHighlightable") == "yes") &&
                    (currentElement.id.indexOf("FormRowLabel") == -1) &&
                    (currentElement.id.indexOf("FavoritesLabel") == -1)) {
                    continueSearch = false;
                    break;
                }
                currentElement = getPreviousSiblingOfElement(currentElement);
            }

            // We never found an enabled sibling item.  Add the placeholder.
            if (continueSearch == true) {
                theElement.innerHTML = getWebGUIMenuItemEmptyInnerHTML("outerHE" + hyperExitID);
            }
        }
    }
    catch (exception) {
        // No specific error handling--"try" is primarily a safety net over null references.
    }
}

/*
* This function is the cleanup utility for any changes made by the
* alterMenuForEmptyContent function.  Here, we're enabling a menu item
* and need to remove any placeholder in our menu when it contained only
* disabled menu items.  Note: Ideally we could use an event's PARENTMENUIDKEY
* to examine the parent then find an existing placeholder within that parent.
* But, unfortunately, in the case of items within a user-defined submenu, the
* PARENTMENUIDKEY does not point to the submenu.  As such, we need to examine
* the item's siblings.
*/
function jdeWebGUIRemoveEmptyContentPlaceholder(theElement) {
    try {
        if (theElement != null) {
            /*
            * Remove an "[Empty]" placeholder we've previously added in now that we
            * no longer are empty. A potential risk is "[Empty]" isn't reserved for
            * our context only.
            */
            var currentElement = theElement;
            var continueSearch = true;

            // Look at ourself first, then the siblings after us.
            // We're done the moment we find the placeholder.
            do {
                if (currentElement.innerHTML.indexOf(emptyString) != -1) {
                    currentElement.innerHTML = "";
                    continueSearch = false;
                    break;
                }
                currentElement = getNextSiblingOfElement(currentElement);
            }
            while (continueSearch == true && currentElement != null);

            // Look at the siblings before us.
            // We're done the moment we find the placeholder.
            currentElement = getPreviousSiblingOfElement(theElement);
            while (continueSearch == true && currentElement != null) {
                if (currentElement.innerHTML.indexOf(emptyString) != -1) {
                    currentElement.innerHTML = "";
                    continueSearch = false;
                    break;
                }
                currentElement = getPreviousSiblingOfElement(currentElement);
            }
        }
    }
    catch (exception) {
        // No specific error handling--"try" is primarily a safety net over null references.
    }
}

function hasElementTraversalSupport() {
    // For JDE E1 Tools 9.1.3+, the mtr for client browsers all offer support for Element Traversal.
    // The lone exception is IE8.
    var hasSupport = true;
    if (isIE8OrEarlier) {
        hasSupport = false;
    }

    return hasSupport;
}

function getNextSiblingOfElement(theElement) {
    var currentElement = null;
    if (hasElementTraversalSupport()) {
        currentElement = theElement.nextElementSibling;
    }
    else {
        do {
            currentElement = theElement.nextSibling;
        }
        // Continue until we find an ELEMENT_NODE.
        while (currentElement && currentElement.nodeType != 1);
    }

    return currentElement;
}

function getPreviousSiblingOfElement(theElement) {
    var currentElement = null;
    if (hasElementTraversalSupport()) {
        currentElement = theElement.previousElementSibling;
    }
    else {
        do {
            currentElement = theElement.previousSibling;
        }
        // Continue until we find an ELEMENT_NODE.
        while (currentElement && currentElement.nodeType != 1);
    }

    return currentElement;
}

// END OF SCRIPTS FOR MENUS
///////////////////////////
// START OF SCRIPTS FOR COOKIES

function jdeWebGUIsetSubCookie(cookiename, subcookiename, setting) {
    var expiration = "";
    var expire = new Date();
    expire.setTime(expire.getTime() + (180 * 24 * 60 * 60 * 1000)); // expire in 180 days
    jdeWebGUIsetExpirableSubCookie(cookiename, subcookiename, setting, expire.toGMTString());
}
function jdeWebGUIsetExpirableSubCookie(cookiename, subcookiename, setting, expiration, path) {
    var bigcookievalue = "";
    var oldbigcookie = jdeWebGUIgetCookie(cookiename, false);
    var subarray = oldbigcookie.split("&");
    for (i = 0; i < subarray.length; i++) {
        var curVal = subarray[i].split("=");
        if (curVal.length > 1) {
            var curName = curVal[0];
            var curVal = unescape(curVal[1]);
            if (curName != subcookiename)
                bigcookievalue += curName + "=" + escape(curVal) + "&";
        }
    }

    bigcookievalue += subcookiename + "=" + escape(setting);

    var theCookie = cookiename + "=" + bigcookievalue + ";";
    if (!path)
        theCookie += " path=/;";
    else
        theCookie += " path=" + path + ";";

    theCookie += " " + expiration;
    document.cookie = theCookie;
}
function jdeWebGUIgetSubCookie(cookiename, subcookiename) {
    var bigcookie = jdeWebGUIgetCookie(cookiename, false);
    var subarray = bigcookie.split("&");
    for (i = 0; i < subarray.length; i++) {
        var curVal = subarray[i].split("=");
        if (curVal.length > 1) {
            var curName = curVal[0];
            var curVal = unescape(curVal[1]);
            if (curName == subcookiename)
                return curVal;
        }
    }
    return "";
}
function jdeWebGUIresetCookie(cookiename) {
    var expire = new Date();
    expire.setTime(expire.getTime() + 1); // expire in 1 millisecond
    document.cookie = cookiename + "=" + escape("zzz") + "; path=/; expires=" + expire.toGMTString();
}
function jdeWebGUIsetCookie(cookiename, setting) {
    var expire = new Date();
    expire.setTime(expire.getTime() + (180 * 24 * 60 * 60 * 1000)); // expire in 180 days
    document.cookie = cookiename + "=" + escape(setting) + "; path=/; expires=" + expire.toGMTString();
}
function jdeWebGUIgetCookie(cookiename, unescapeResult) {
    var arg = cookiename + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg) {
            var endstr = document.cookie.indexOf(";", j);
            if (endstr == -1)
                endstr = document.cookie.length;
            if (unescapeResult)
                return unescape(document.cookie.substring(j, endstr));
            else
                return document.cookie.substring(j, endstr);
        }
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return "";
}

// END OF SCRIPTS FOR COOKIES
//////////////////////////////////

///////////////////////////////////////////////////////////////
// START OF CALENDAR SCRIPTS

function jdeWebGUIshowCalendar(updateFieldId, dateSeparator, dateFormatEnum, timeFormatEnum) {
    var dateFormat = "MDE";
    if (dateFormatEnum == "1")
        dateFormat = "EMD";
    else if (dateFormatEnum == "2")
        dateFormat = "DME";

    var utcEnabled = false;
    var utc24 = false;
    if (timeFormatEnum == "1")
        utcEnabled = true;
    else if (timeFormatEnum == "2") {
        utcEnabled = true;
        utc24 = true;
    }
    var windowHeight = 210;
    if (utcEnabled)
        windowHeight = 310;

    var minYear = 1500;
    var maxYear = 2500;

    var calWindow = window.open("", "jdeCalendarWindow" + updateFieldId, "height=" + windowHeight + ",width=240,toolbar=0,location=0,directories=0,status=0,menuBar=0,scrollBars=1,resizable=1");

    // build the calendar window content
    var writeString;
    if (document.documentElement.dir == "rtl")
        writeString = "<html dir=rtl>";
    else
        writeString = "<html>";
    writeString += "\n<head>\n";
    writeString += "<title>" + jdeWebGUIcalendarStrings[0] + "</title>\n";
    writeString += "<SCRIPT LANGUAGE=\"JavaScript\" SRC='" + jdeWebGUIjavaScriptPath + "'></SCRIPT>\n";
    writeString += "<style>\n";
    writeString += "BODY {\n";
    writeString += "font-size: 9pt;\n";
    writeString += "margin-left: 0;\n";
    writeString += "margin-top: 0;\n";
    writeString += "margin-right: 0;\n";
    writeString += "margin-bottom: 0;\n";
    writeString += "font-family: Arial,Helvetica,Verdana,Sans-Serif;\n";
    writeString += "color: #000000;\n";
    writeString += "background-color: #FFFFFF;\n";
    writeString += "}\n";
    writeString += "TABLE TD {\n";
    writeString += "font-size: 9pt;\n";
    writeString += "}\n";
    writeString += "INPUT {\n";
    writeString += "font-size: 8pt;\n";
    writeString += "}\n";
    writeString += "SELECT {\n";
    writeString += "font-size: 9pt;\n";
    writeString += "}\n";
    writeString += ".textfield {\n";
    writeString += "font-size: 9pt;\n";
    writeString += "color: #000000;\n";
    writeString += "background-color: #FFFFFF;\n";
    writeString += "border-width: 1px;\n";
    writeString += "border-style: solid;\n";
    writeString += "border-top-color: #999999;\n";
    writeString += "border-bottom-color: #CCCCCC;\n";
    writeString += "border-left-color: #999999;\n";
    writeString += "border-right-color: #CCCCCC;\n";
    writeString += "}\n";
    writeString += "</style>\n";
    writeString += "</head>\n";
    if (document.documentElement.dir == "rtl")
        writeString += "<body dir=rtl";
    else
        writeString += "<body";
    writeString += " onload=\"jdeWebGUIinitToday();jdeWebGUIbuildCaln();\">\n";
    writeString += "<div id=\"calendar\" align=\"center\"></div>\n";
    writeString += "<script>\n";
    writeString += "var jdeWebGUIdateseparator = \"" + dateSeparator + "\";\n";
    writeString += "var jdeWebGUIdateformat = \"" + dateFormat + "\";\n";
    writeString += "var jdeWebGUIisUTimeAMPM = !" + utc24 + ";\n";
    writeString += "var jdeWebGUIisUTime = " + utcEnabled + ";\n";
    writeString += "var jdeWebGUIcalendarTextFieldId = \"" + updateFieldId + "\";\n";
    writeString += "var jdeWebGUIcalendarStrings = new Array(";
    for (var i = 0; i < jdeWebGUIcalendarStrings.length; i++) {
        if (i > 0)
            writeString += ", ";
        writeString += "\"" + jdeWebGUIcalendarStrings[i] + "\"";
    }
    writeString += ");\n";
    writeString += "var jdeWebGUI_MINYEAR = " + minYear + ";\n";
    writeString += "var jdeWebGUI_MAXYEAR = " + maxYear + ";\n";
    writeString += "</script>\n";
    writeString += "</body>\n</html>\n";

    try {
        calWindow.focus();
    }
    catch (problem) {
        // Netscape 7 may not allow this
    }
    calWindow.document.write(writeString);
    calWindow.document.close(); // close the layout stream
}

var jdeWebGUImonthdays = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var jdeWebGUItodayDate;
var jdeWebGUIcurday;
var jdeWebGUIcurmonth;
var jdeWebGUIcurdate;
var jdeWebGUIcuryear;
var jdeWebGUIstartspaces;
var jdeWebGUIdays;
var jdeWebGUIcurDateObj = new Date();

function jdeWebGUIinitToday() {
    jdeWebGUItodayDate = new Date();
    jdeWebGUIcurmonth = jdeWebGUItodayDate.getMonth();
    jdeWebGUIcurdate = jdeWebGUItodayDate.getDate();
    jdeWebGUIcuryear = jdeWebGUItodayDate.getFullYear();

    // Abbreviate the days
    jdeWebGUIdays = new Array(jdeWebGUIcalendarStrings[13], jdeWebGUIcalendarStrings[14], jdeWebGUIcalendarStrings[15], jdeWebGUIcalendarStrings[16], jdeWebGUIcalendarStrings[17], jdeWebGUIcalendarStrings[18], jdeWebGUIcalendarStrings[19]);
    for (var i = 0; i < 7; i++) {
        var startChar = jdeWebGUIdays[i].charAt(0);
        var need2Day = false;
        for (var j = 0; j < 7; j++) {
            if (i != j) {
                if (startChar == jdeWebGUIdays[j].charAt(0)) {
                    need2Day = true;
                    continue;
                }
            }
        }
        if (need2Day)
            jdeWebGUIdays[i] = jdeWebGUIdays[i].substring(0, 2);
        else
            jdeWebGUIdays[i] = startChar;
    }
}

function jdeWebGUIcalcCurrentValues() {
    jdeWebGUIcurDateObj.setDate(jdeWebGUIcurdate);
    jdeWebGUIcurDateObj.setYear(jdeWebGUIcuryear);
    jdeWebGUIcurDateObj.setMonth(jdeWebGUIcurmonth);

    jdeWebGUIcurday = jdeWebGUIcurDateObj.getDay();

    if (((jdeWebGUIcuryear % 4 == 0) && !(jdeWebGUIcuryear % 100 == 0)) || (jdeWebGUIcuryear % 400 == 0))
        jdeWebGUImonthdays[1] = 29;
    else
        jdeWebGUImonthdays[1] = 28;

    jdeWebGUIstartspaces = jdeWebGUIcurdate;

    while (jdeWebGUIstartspaces > 7)
        jdeWebGUIstartspaces -= 7;

    jdeWebGUIstartspaces = jdeWebGUIcurday - jdeWebGUIstartspaces + 1;
    if (jdeWebGUIstartspaces < 0)
        jdeWebGUIstartspaces += 7;
}

function jdeWebGUIprevMonth() {
    jdeWebGUIcurmonth--;

    if (jdeWebGUIcurmonth < 0) {
        jdeWebGUIcurmonth = 11;
        jdeWebGUIcuryear--;
        if (jdeWebGUIcuryear < jdeWebGUI_MINYEAR)
            jdeWebGUIcuryear = jdeWebGUI_MAXYEAR;
    }
    jdeWebGUIbuildCaln();
}

function jdeWebGUInextMonth() {
    jdeWebGUIcurmonth++;

    if (jdeWebGUIcurmonth > 11) {
        jdeWebGUIcurmonth = 0;
        jdeWebGUIcuryear++;
        if (jdeWebGUIcuryear > jdeWebGUI_MAXYEAR)
            jdeWebGUIcuryear = jdeWebGUI_MINYEAR;
    }
    jdeWebGUIbuildCaln();
}

function jdeWebGUIprevYear() {
    jdeWebGUIcuryear--;
    if (jdeWebGUIcuryear < jdeWebGUI_MINYEAR)
        jdeWebGUIcuryear = jdeWebGUI_MAXYEAR;

    jdeWebGUIbuildCaln();
}

function jdeWebGUInextYear() {
    jdeWebGUIcuryear++;
    if (jdeWebGUIcuryear > jdeWebGUI_MAXYEAR)
        jdeWebGUIcuryear = jdeWebGUI_MINYEAR;

    jdeWebGUIbuildCaln();
}

function jdeWebGUIhighlightSel(field) {
    field.style.color = "red";
}

function jdeWebGUIremHighlight(field, isWeekend) {
    if (isWeekend)
        field.style.color = "black"; //"#666666";
    else
        field.style.color = "black";
}

function jdeWebGUIremHighlightBtn(field) {
    field.style.borderColor = "gray";
}

function jdeWebGUIsetCurDate(dateval) {
    jdeWebGUIcurdate = dateval;
    jdeWebGUIbuildCaln();
}

function jdeWebGUIformatDate(strVal) {
    var strDay = "" + jdeWebGUIcurDateObj.getDate(), strMonth = "", strYear = "";

    if (jdeWebGUIcurDateObj.getDate() < 10)
        strDay = "0" + jdeWebGUIcurDateObj.getDate();

    if (jdeWebGUIcurDateObj.getMonth() < 9)
        strMonth = "0" + (jdeWebGUIcurDateObj.getMonth() + 1);
    else
        strMonth += (jdeWebGUIcurDateObj.getMonth() + 1);

    var yrVal = jdeWebGUIcurDateObj.getYear();
    if (!document.all)
        yrVal = jdeWebGUIcurDateObj.getFullYear();
    if (jdeWebGUIdateformat.indexOf("E") != -1)
        strYear += yrVal;
    else {
        var yrTmp = yrVal % 100;
        if (yrTmp < 10)
            strYear += "0" + yrTmp;
        else
            strYear += yrTmp;
    }
    if (jdeWebGUIdateformat == "DME" || jdeWebGUIdateformat == "DMY") {
        strVal = strDay + jdeWebGUIdateseparator + strMonth + jdeWebGUIdateseparator + strYear;
    }
    else if (jdeWebGUIdateformat == "EMD" || jdeWebGUIdateformat == "YMD") {
        strVal = strYear + jdeWebGUIdateseparator + strMonth + jdeWebGUIdateseparator + strDay;
    }
    else if (jdeWebGUIdateformat == "MDE" || jdeWebGUIdateformat == "MDY") {
        strVal = strMonth + jdeWebGUIdateseparator + strDay + jdeWebGUIdateseparator + strYear;
    }
    //UTime stuff
    if (jdeWebGUIisUTime) {
        var hrVal = new Number(document.getElementById("utimeHR").value);
        if (jdeWebGUIisUTimeAMPM) {
            var ampmField = document.getElementById("utimeAMPM");
            if (ampmField.innerHTML == jdeWebGUIcalendarStrings[28]) // PM
                hrVal += 12;
        }
        var theHours = "" + hrVal;
        if (theHours == "")
            theHours = "00";
        var theMinutes = "" + document.getElementById("utimeMIN").value;
        if (theMinutes == "")
            theMinutes = "00";
        var theSeconds = "" + document.getElementById("utimeSEC").value;
        if (theSeconds == "")
            theSeconds = "00";
        strVal += " " + theHours;
        strVal += ":" + theMinutes;
        strVal += ":" + theSeconds;
        strVal += " " + document.getElementById("utimeTZ").options[document.getElementById("utimeTZ").selectedIndex].text;
    }

    return strVal;
}

function jdeWebGUIdoAMPM(amString, pmString) {
    var ampmField = document.getElementById("utimeAMPM");
    if (ampmField.innerHTML == amString)
        ampmField.innerHTML = pmString;
    else
        ampmField.innerHTML = amString;
}

function jdeWebGUIbuildCaln() {
    jdeWebGUIcalcCurrentValues();
    writeString = "<table dir=ltr><tr><td>";
    var rtlEnabled = false;
    if (document.documentElement.dir == "rtl") {
        rtlEnabled = true;
    }
    writeString += "<table border=0 cellpadding=0 cellspacing=0>\n";
    writeString += "<tr valign=MIDDLE>\n";
    if (rtlEnabled) {
        writeString += "<td align=left>";
        writeString += "<A ID=va_caln_cancel title=\"" + jdeWebGUIcalendarStrings[21] + "\" href=\"javascript:jdeWebGUIonCancelCaln()\"><IMG onmouseover=\"this.src='" + window["WEBGUIRES_images_tiny_cancel_mo_gif"] + "'\" onmouseout=\"this.src='" + window["WEBGUIRES_images_tiny_cancel_gif"] + "'\" src=\"" + window["WEBGUIRES_images_tiny_cancel_gif"] + "\" alt=\"" + jdeWebGUIcalendarStrings[21] + "\" border=0></A>";
        writeString += "<A ID=va_caln_ok title=\"" + jdeWebGUIcalendarStrings[20] + "\" href=\"javascript:jdeWebGUIonOKCaln()\"><IMG onmouseover=\"this.src='" + window["WEBGUIRES_images_tiny_ok_mo_gif"] + "'\" onmouseout=\"this.src='" + window["WEBGUIRES_images_tiny_ok_gif"] + "'\" src=\"" + window["WEBGUIRES_images_tiny_ok_gif"] + "\" alt=\"" + jdeWebGUIcalendarStrings[20] + "\" border=0></A>";
        writeString += "</td>\n";
        writeString += "<td align=right><b>" + jdeWebGUIcalendarStrings[0] + "</b></td>\n";
    }
    else {
        writeString += "<td align=left><b>" + jdeWebGUIcalendarStrings[0] + "</b></td>\n";
        writeString += "<td align=right>";
        writeString += "<A ID=va_caln_ok title=\"" + jdeWebGUIcalendarStrings[20] + "\" href=\"javascript:jdeWebGUIonOKCaln()\"><IMG onmouseover=\"this.src='" + window["WEBGUIRES_images_tiny_ok_mo_gif"] + "'\" onmouseout=\"this.src='" + window["WEBGUIRES_images_tiny_ok_gif"] + "'\" src=\"" + window["WEBGUIRES_images_tiny_ok_gif"] + "\" alt=\"" + jdeWebGUIcalendarStrings[20] + "\" border=0></A>";
        writeString += "<A ID=va_caln_cancel title=\"" + jdeWebGUIcalendarStrings[21] + "\" href=\"javascript:jdeWebGUIonCancelCaln()\"><IMG onmouseover=\"this.src='" + window["WEBGUIRES_images_tiny_cancel_mo_gif"] + "'\" onmouseout=\"this.src='" + window["WEBGUIRES_images_tiny_cancel_gif"] + "'\" src=\"" + window["WEBGUIRES_images_tiny_cancel_gif"] + "\" alt=\"" + jdeWebGUIcalendarStrings[21] + "\" border=0></A>";
        writeString += "</td>\n";
    }
    writeString += "</tr>\n";
    writeString += "<tr valign=top>\n";
    writeString += "<td colspan=2 align=center><HR></td>\n";
    writeString += "</tr>\n";
    writeString += "<tr><td colspan=2 align=center><table border=0 cellpadding=0 cellspacing=0>\n";
    writeString += "<tr valign=MIDDLE>\n";
    writeString += "<td colspan=7><table width=100% border=0 cellpadding=0 cellspacing=0><tr>";
    if (rtlEnabled) {
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUInextYear()\" title=\"" + jdeWebGUIcalendarStrings[25] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><img src='" + window["WEBGUIRES_images_tiny_moveallleft_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[25] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveallleft_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveallleft_gif"] + "'\" border=0 width=20 height=20></td>\n";
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUInextMonth()\" title=\"" + jdeWebGUIcalendarStrings[24] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><nobr><img src='" + window["WEBGUIRES_images_tiny_moveleft_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[24] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveleft_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveleft_gif"] + "'\" border=0 width=20 height=20 align=absmiddle>&nbsp;</nobr></td>\n";
        writeString += "<td width=120 nowrap align=center><b>" + jdeWebGUIcalendarStrings[jdeWebGUIcurmonth + 1] + " " + jdeWebGUIcuryear + "</b></td>\n";
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUIprevMonth()\" title=\"" + jdeWebGUIcalendarStrings[23] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><nobr>&nbsp;<img src='" + window["WEBGUIRES_images_tiny_moveright_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[23] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveright_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveright_gif"] + "'\" border=0 width=20 height=20 align=absmiddle></nobr></td>\n";
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUIprevYear()\" title=\"" + jdeWebGUIcalendarStrings[22] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><img src='" + window["WEBGUIRES_images_tiny_moveallright_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[22] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveallright_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveallright_gif"] + "'\" border=0 width=20 height=20></td>\n";
    }
    else {
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUIprevYear()\" title=\"" + jdeWebGUIcalendarStrings[22] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><img src='" + window["WEBGUIRES_images_tiny_moveallleft_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[22] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveallleft_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveallleft_gif"] + "'\" border=0 width=20 height=20></td>\n";
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUIprevMonth()\" title=\"" + jdeWebGUIcalendarStrings[23] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><nobr>&nbsp;<img src='" + window["WEBGUIRES_images_tiny_moveleft_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[23] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveleft_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveleft_gif"] + "'\" border=0 width=20 height=20 align=absmiddle></nobr></td>\n";
        writeString += "<td width=120 nowrap align=center><b>" + jdeWebGUIcalendarStrings[jdeWebGUIcurmonth + 1] + " " + jdeWebGUIcuryear + "</b></td>\n";
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUInextMonth()\" title=\"" + jdeWebGUIcalendarStrings[24] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><nobr><img src='" + window["WEBGUIRES_images_tiny_moveright_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[24] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveright_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveright_gif"] + "'\" border=0 width=20 height=20 align=absmiddle>&nbsp;</nobr></td>\n";
        writeString += "<td style=\"cursor:pointer\" onclick=\"javascript:jdeWebGUInextYear()\" title=\"" + jdeWebGUIcalendarStrings[25] + "\" onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlightBtn(this)\"><img src='" + window["WEBGUIRES_images_tiny_moveallright_gif"] + "' alt=\"" + jdeWebGUIcalendarStrings[25] + "\" onMouseOver=\"this.src='" + window["WEBGUIRES_images_tiny_moveallright_mo_gif"] + "'\" onMouseOut=\"this.src='" + window["WEBGUIRES_images_tiny_moveallright_gif"] + "'\" border=0 width=20 height=20></td>\n";
    }
    writeString += "</tr></table></td>\n"
    writeString += "</tr>\n";
    writeString += "<tr valign=top>\n";
    writeString += "<td colspan=7 align=center><HR></td>\n";
    writeString += "</tr>\n";
    writeString += "<tr style=\"text-decoration: underline\" valign=MIDDLE>\n";
    for (var i = 0; i < 7; ++i)
        writeString += "<td width=25 align=center>" + jdeWebGUIdays[i] + "</td>\n";
    writeString += "</tr>\n";
    writeString += "<tr valign=MIDDLE>\n";
    for (s = 0; s < jdeWebGUIstartspaces; s++)
        writeString += "<td> </td>\n";

    x = jdeWebGUIstartspaces; count = 1;
    var rowsDrawn = 0;

    while (count <= jdeWebGUImonthdays[jdeWebGUIcurmonth]) {
        for (b = jdeWebGUIstartspaces; b < 7; b++) {
            linktrue = false;

            writeString += "<td align=right style='cursor:pointer;";
            if ((((count + x) % 7) == 0) || (((count + x) % 7) == 1)) // is weekend
            {
                writeString += "font-style:italic;'";

                if (count <= jdeWebGUImonthdays[jdeWebGUIcurmonth]) {
                    if (count == jdeWebGUIcurdate) // if this day is selected, next click shall select it
                        writeString += " onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlight(this, true)\" onclick=\"javascript:jdeWebGUIonOKCaln()\"";
                    else // a single click will highlight it
                        writeString += " onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onDblClick=\"javascript:jdeWebGUIonOKCaln()\" onmouseout=\"javascript:jdeWebGUIremHighlight(this, true)\" onclick=\"javascript:jdeWebGUIsetCurDate(" + count + ")\"";
                }
            }
            else {
                writeString += "'";

                if (count <= jdeWebGUImonthdays[jdeWebGUIcurmonth]) {
                    if (count == jdeWebGUIcurdate) // if this day is selected, next click shall select it
                        writeString += " onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onmouseout=\"javascript:jdeWebGUIremHighlight(this, false)\" onclick=\"javascript:jdeWebGUIonOKCaln()\"";
                    else // a single click will highlight it
                        writeString += " onmouseover=\"javascript:jdeWebGUIhighlightSel(this)\" onDblClick=\"javascript:jdeWebGUIonOKCaln()\" onmouseout=\"javascript:jdeWebGUIremHighlight(this, false)\" onclick=\"javascript:jdeWebGUIsetCurDate(" + count + ")\"";
                }
            }

            writeString += ">";

            if (count == jdeWebGUIcurdate)
                writeString += "<font color='#FF0000'><strong>";

            if (count <= jdeWebGUImonthdays[jdeWebGUIcurmonth])
                writeString += count;
            else
                writeString += " ";

            if (count == jdeWebGUIcurdate)
                writeString += "</strong></font>";

            writeString += "&nbsp;&nbsp;</td>\n";
            count++;
        }
        writeString += "</tr>\n";
        if (count <= jdeWebGUImonthdays[jdeWebGUIcurmonth])
            writeString += "<tr valign=MIDDLE>\n";
        rowsDrawn++;
        jdeWebGUIstartspaces = 0;
    }
    while (rowsDrawn++ < 6) // months can have from 4 to 6 actual rows in the calendar
        writeString += "<tr valign=MIDDLE><td>&nbsp;</td></tr>";

    writeString += "</table></td></tr>\n";
    if (jdeWebGUIisUTime) {
        writeString += "<tr valign=MIDDLE><td colspan=7 align=center><HR></td></tr>";
        writeString += "<tr valign=MIDDLE><td colspan=7>";
        if (rtlEnabled)
            writeString += "<table align=right dir=rtl><tr>";
        else
            writeString += "<table><tr>";
        writeString += "<td width=20>&nbsp;</td><td><b>" + jdeWebGUIcalendarStrings[26] + "</b></td>";
        writeString += "<td width=30>&nbsp;</td><td width=35>&nbsp;</td>";
        writeString += "<td colspan=3><b>+/- UTC</b></td>";
        writeString += "</tr></table>";
        writeString += "</td></tr>";
        writeString += "<tr valign=MIDDLE><td colspan=7 align=center><HR></td></tr>";
        writeString += "<tr valign=MIDDLE><td colspan=7 align=center>";

        if (rtlEnabled)
            writeString += "<table dir=rtl><tr>";
        else
            writeString += "<table><tr>";

        var utimeHrs = jdeWebGUItodayDate.getHours();
        var isPM = false;
        if (jdeWebGUIisUTimeAMPM && utimeHrs >= 12) {
            utimeHrs -= 12;
            isPM = true;
        }

        if (utimeHrs < 10)
            utimeHrs = "0" + utimeHrs;

        var utimeMin = jdeWebGUItodayDate.getMinutes();
        if (utimeMin < 10)
            utimeMin = "0" + utimeMin;

        var utimeSec = jdeWebGUItodayDate.getSeconds();
        if (utimeSec < 10)
            utimeSec = "0" + utimeSec;

        if (rtlEnabled) {
            writeString += "<td><nobr><input type=text class='textfield' style='width:20' maxlength=2 id=\"utimeSEC\" value=" + utimeSec + ">:</td>";
            writeString += "<td><nobr><input type=text class='textfield' style='width:20' maxlength=2 id=\"utimeMIN\" value=" + utimeMin + ">:</nobr></td>"
            writeString += "<td><nobr><input type=text class='textfield' style='width:20' maxlength=2 id=\"utimeHR\" value=" + utimeHrs + "></nobr>"
            if (jdeWebGUIisUTimeAMPM) {
                writeString += "<td><A href=\"javascript:jdeWebGUIdoAMPM('" + jdeWebGUIcalendarStrings[27] + "', '" + jdeWebGUIcalendarStrings[28] + "')\" id=\"utimeAMPM\">";
                if (isPM)
                    writeString += jdeWebGUIcalendarStrings[28]; // PM
                else
                    writeString += jdeWebGUIcalendarStrings[27]; // AM
                writeString += "</A>";
            }
            writeString += "</td>";
        }
        else {
            writeString += "<td><nobr><input type=text class='textfield' style='width:20' maxlength=2 id=\"utimeHR\" value=" + utimeHrs + ">:</nobr></td>"
            writeString += "<td><nobr><input type=text class='textfield' style='width:20' maxlength=2 id=\"utimeMIN\" value=" + utimeMin + ">:</nobr></td>"
            writeString += "<td><input type=text class='textfield' style='width:20' maxlength=2 id=\"utimeSEC\" value=" + utimeSec + ">";
            if (jdeWebGUIisUTimeAMPM) {
                writeString += "<td><A href=\"javascript:jdeWebGUIdoAMPM('" + jdeWebGUIcalendarStrings[27] + "', '" + jdeWebGUIcalendarStrings[28] + "')\" id=\"utimeAMPM\">";
                if (isPM)
                    writeString += jdeWebGUIcalendarStrings[28]; // PM
                else
                    writeString += jdeWebGUIcalendarStrings[27]; // AM
                writeString += "</A>";
            }
            writeString += "</td>";
        }

        writeString += "<td width=20>&nbsp;</td>";
        writeString += "<td><select size=1 id=\"utimeTZ\">";
        writeString += "<option>UTC </option>";
        writeString += "<option>UTC-12:00</option>";
        writeString += "<option>UTC-11:00</option>";
        writeString += "<option>UTC-10:00</option>";
        writeString += "<option>UTC-09:00</option>";
        writeString += "<option>UTC-08:00</option>";
        writeString += "<option>UTC-07:00</option>";
        writeString += "<option>UTC-06:00</option>";
        writeString += "<option>UTC-05:00</option>";
        writeString += "<option>UTC-04:00</option>";
        writeString += "<option>UTC-03:00</option>";
        writeString += "<option>UTC-02:00</option>";
        writeString += "<option>UTC-01:00</option>";
        writeString += "<option>UTC+01:00</option>";
        writeString += "<option>UTC+02:00</option>";
        writeString += "<option>UTC+03:00</option>";
        writeString += "<option>UTC+03:30</option>";
        writeString += "<option>UTC+04:00</option>";
        writeString += "<option>UTC+04:30</option>";
        writeString += "<option>UTC+05:00</option>";
        writeString += "<option>UTC+05:30</option>";
        writeString += "<option>UTC+05:45</option>";
        writeString += "<option>UTC+06:00</option>";
        writeString += "<option>UTC+06:30</option>";
        writeString += "<option>UTC+07:00</option>";
        writeString += "<option>UTC+08:00</option>";
        writeString += "<option>UTC+09:00</option>";
        writeString += "<option>UTC+09:30</option>";
        writeString += "<option>UTC+10:00</option>";
        writeString += "<option>UTC+11:00</option>";
        writeString += "<option>UTC+12:00</option>";
        writeString += "<option>UTC+13:00</option>";
        writeString += "</select></td>\n";

        writeString += "</tr></table></td>";
        writeString += "</tr>\n";
    }
    writeString += "</table>\n"; // close of above UTime

    writeString += "</table>\n"; // close of dates
    writeString += "</td></tr></table>\n"; // close of internal layout
    writeString += "</td></tr></table>\n"; // close of outer border
    document.getElementById("calendar").innerHTML = writeString;
}

function jdeWebGUIonOKCaln() {
    var strVal = jdeWebGUIformatDate();
    jdeWebGUIupdateOpenerFieldValue(jdeWebGUIcalendarTextFieldId, strVal, true);
}

function jdeWebGUIonCancelCaln() {
    self.close();
}

// END OF SCRIPTS FOR CALENDAR
//////////////////////////////////

///////////////////////////////////////////////////////////////
// START OF UTILITY SCRIPTS

function isCSSLevel1Supported() {
    try { return jdeWebGUICSS1; } catch (problem) { return false; }
}

function isDOMLevel1Supported() {
    try { return jdeWebGUIDOM1; } catch (problem) { return false; }
}

function isDOMLevel2Supported() {
    try { return jdeWebGUIDOM2; } catch (problem) { return false; }
}

function isDynamicMenusSupported() {
    try { return jdeWebGUIDynamicMenus; } catch (problem) { return false; }
}

function isDynamicOptionsSupported() {
    try { return jdeWebGUIDynamicOptions; } catch (problem) { return false; }
}

function isEventOffsetCoordsSupported() {
    try { return jdeWebGUIEventOffsetCoords; } catch (problem) { return false; }
}

function isSmallScreenSupported() {
    try { return jdeWebGUISmallScreen; } catch (problem) { return false; }
}

/* The function returns the element which has click event associated with it to display exit menu(row/form) irrespective of the platform. */
var EXIT_TYPE = {};
EXIT_TYPE.ROW_EXIT = 0;
EXIT_TYPE.FORM_EXIT = 1;
function getExitMenuLaunchElement(exitType) {
    var simplifiedExitId = "exitMenuTab_",
        regularExitId = "_EXIT_BUTTON";

    switch (exitType) {
        case EXIT_TYPE.ROW_EXIT:
            simplifiedExitId = simplifiedExitId + "Row";
            regularExitId = "ROW" + regularExitId;
            break;

        case EXIT_TYPE.FORM_EXIT:
            simplifiedExitId = simplifiedExitId + "Form";
            regularExitId = "FORM" + regularExitId;
            break;

        default:
            //Not yet supported. Add support for other exits if needed.
            return;
    }

    //Simplified exit menu has higher precendence
    var exitElement = document.getElementById(simplifiedExitId);
    if (exitElement) {
        //debugLog("Simple:"+exitElement.id);
        return exitElement;
    }

    //No simplified menu present, try to find regular exits
    exitElement = document.getElementById(regularExitId);
    if (exitElement) {
        if (exitElement.getAttribute("launchmenuid") && exitElement.onclick) {
            //debugLog("Regular:"+exitElement.id);
            return exitElement;
        }
        else {
            //This case arises for the tablet style WebMenu Buttons(9.1.4) where onclick event is associated with one of the ancestors
            var i = 0;
            while (!(exitElement.getAttribute("launchmenuid") && exitElement.onclick)) {
                exitElement = exitElement.parentNode;
                if (i++ >= 15) //Arbitrary constant to avoid infinite loop. Ideally should never have to be used.
                {
                    break;
                }
            }
            //debugLog("Tablet:"+exitElement.id);
            return exitElement;
        }
    }
}

function getAbsoluteLeftPos(item, adjustScrollbars, ownerDoc) {
    var isRTL = document.documentElement.dir == "rtl";
    ownerDoc = ownerDoc == null ? document : ownerDoc;
    var itemLeft = item.offsetLeft + ownerDoc.body.offsetLeft;
    var itemParent = item.offsetParent;
    var scr = item;

    if (!isRTL) {
        while (itemParent) {
            itemLeft += itemParent.offsetLeft;
            if (true == adjustScrollbars && itemParent != ownerDoc.body) {
                itemLeft -= itemParent.scrollLeft;
            }
            itemParent = itemParent.offsetParent;
        }
    }
    else {
        while (itemParent) {
            if (itemParent.offsetLeft != undefined) {
                itemLeft += itemParent.offsetLeft;
            }

            itemParent = itemParent.offsetParent;
        }
        if (true == adjustScrollbars) {
            while ((scr = scr.parentNode) && scr != ownerDoc.body) {
                if (scr.scrollLeft != undefined) {
                    if (isIE || isFirefox)
                        itemLeft += Math.abs(scr.scrollLeft);
                    else// webkit
                        if (scr.scrollWidth > scr.offsetWidth)
                            itemLeft += (scr.scrollWidth - scr.offsetWidth - scr.scrollLeft);
                }
            }
        }
    }
    return itemLeft;
}

function getAbsoluteTopPos(item, adjustScrollbars, ownerDoc) {
    var isRTL = (document.documentElement.dir == "rtl");
    ownerDoc = ownerDoc == null ? document : ownerDoc;
    var itemTop = item.offsetTop + ownerDoc.body.offsetTop;
    try {
        if (!isRTL) {
            var itemParent = item.offsetParent;
            while (itemParent) {
                itemTop += itemParent.offsetTop;
                if (true == adjustScrollbars && itemParent != ownerDoc.body) {
                    itemTop -= itemParent.scrollTop;
                }

                itemParent = itemParent.offsetParent;
            }
        }
        else {
            var scr = item;
            var itemParent = item.offsetParent;
            while (itemParent) {
                itemTop += itemParent.offsetTop;
                itemParent = itemParent.offsetParent;
            }

            if (true == adjustScrollbars) {
                while ((scr = scr.parentNode) && scr != ownerDoc.body) {
                    if (scr.scrollTop != undefined)
                        itemTop -= scr.scrollTop;
                }
            }
        }
    }
    catch (problem) {
        // OPS gets an unspecified exception on a refresh.
    }

    return itemTop;
}

function showMotion(elem) {
    if (elem.src.indexOf('ena.png') != -1)  // Alta
    {
        var dot = elem.src.lastIndexOf('ena.png');
        if (dot != -1)
            elem.src = elem.src.substring(0, dot) + "hov.png";
    }
    else {
        if (elem.name) {
            // if element.name exists then use it and create the url using _e1URLFactory
            var dot = elem.name.indexOf('.gif');
            if (dot != -1 && !elem.name.endsWith("mo.gif"))
                elem.name = elem.name.substring(0, dot) + "mo.gif";
            e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
            e1UrlCreator.setURI(elem.name);
            elem.src = e1UrlCreator.toString();
        }
        else {
            var dot = elem.src.indexOf('.gif');
            if (dot != -1 && !elem.src.endsWith("mo.gif"))
                elem.src = elem.src.substring(0, dot) + "mo.gif";
        }
    }
}

function removeMotion(elem) {
    if (elem.src.indexOf('hov.png') != -1)  // Alta
    {
        var dot = elem.src.lastIndexOf('hov.png');
        if (dot != -1)
            elem.src = elem.src.substring(0, dot) + "ena.png";
    }
    else {
        if (elem.name) {
            // if element.name exists then use it and create the url using _e1URLFactory
            var dot = elem.name.indexOf('mo.gif');
            if (dot != -1)
                elem.name = elem.name.substring(0, dot) + ".gif";
            e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
            e1UrlCreator.setURI(elem.name);
            elem.src = e1UrlCreator.toString();
        }
        else {
            var dot = elem.src.indexOf('mo.gif');
            if (dot != -1)
                elem.src = elem.src.substring(0, dot) + ".gif";
        }
    }
}

// This function Completely equivilant to "showMotion" but it deals with RTL images.
// N.B Some images fliped in RTL, then we add _rtl to the end of the image file name
function showMotion_rtl(elem) {
    if (elem.src.indexOf('.png'))  // Alta
    {
        showMotion(elem);
    }
    else {
        if (elem.name) {
            // if element.name exists then use it and create the url using _e1URLFactory
            var dot = elem.name.indexOf('_rtl.gif');
            if (dot != -1)
                elem.name = elem.name.substring(0, dot) + "mo_rtl.gif";
            e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
            e1UrlCreator.setURI(elem.name);
            elem.src = e1UrlCreator.toString();
        }
        else {
            var dot = elem.src.indexOf('_rtl.gif');
            if (dot != -1)
                elem.src = elem.src.substring(0, dot) + "mo_rtl.gif";
        }
    }
}

// This function completely equivilant to "removeMotion" but it deals with RTL images.
function removeMotion_rtl(elem) {
    if (elem.src.indexOf('.png'))  // Alta
    {
        removeMotion(elem);
    }
    else {
        if (elem.name) {
            // if element.name exists then use it and create the url using _e1URLFactory
            var dot = elem.name.indexOf('mo_rtl.gif');
            if (dot != -1)
                elem.name = elem.name.substring(0, dot) + "_rtl.gif";
            e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
            e1UrlCreator.setURI(elem.name);
            elem.src = e1UrlCreator.toString();
        }
        else {
            var dot = elem.src.indexOf('mo_rtl.gif');
            if (dot != -1)
                elem.src = elem.src.substring(0, dot) + "_rtl.gif";
        }
    }
}

function jdeWebGUIgetLocationParameter(parameterName) {
    // @todo handle multiple params of the same name
    // @todo lookup the parameter name with a leading "?" and leading "&" so other params that contain the name will not be mistakenly received
    var myURL = "" + self.location;
    var index = myURL.lastIndexOf(parameterName + "=");
    var parameterValue = "";
    if (index > 1) {
        myURL = myURL.substr(index + parameterName.length + 1, myURL.length);
        index = myURL.indexOf("&");
        if (index == -1)
            parameterValue = myURL;
        else
            parameterValue = myURL.substring(0, index);
    }
    return parameterValue;
}

function jdeWebGUIupdateOpenerFieldValue(documentFieldId, theValue, closeSelf) {
    window.opener.document.getElementById(documentFieldId).value = theValue;
    if (closeSelf) {
        self.close();
    }
}

function jdeWebGUIReplaceSubstring(source, pattern, replace) {
    return source.replace(new RegExp(pattern, "g"), replace);
}

function PSStringBuffer() {
    this.maxStreamLength = (isIE ? 100 : 10000);
    this.data = new Array(100);
    this.iStr = 0;
    this.append = function (obj) {
        this.data[this.iStr++] = obj;
        if (this.data.length > this.maxStreamLength) {
            this.data = [this.data.join("")];
            this.data.length = 100;
            this.iStr = 1;
        }
        return this;
    };
    this.toString = function () {
        return this.data.join("");
    };
}

PSStringBuffer._joinFunc = Array.prototype.join;
PSStringBuffer.concat = function () {
    arguments.join = this._joinFunc;
    return arguments.join("")
}

var PI_MOTION_TIMER = isIE ? 500 : 100;
var piStopTimout;

function ProcessingIndicator() {
}

ProcessingIndicator.createInstance = function (procText, maxWidth, height, topOffset, rightOffset) {
    ProcessingIndicator.theInstance = new ProcessingIndicator();
    ProcessingIndicator.theInstance.PIText = procText;
    ProcessingIndicator.theInstance.isProcIndDisplayed = false;
    ProcessingIndicator.theInstance.procIndicatorElem = null;
    ProcessingIndicator.theInstance.movePIIndicatorTimer = null;
    ProcessingIndicator.theInstance.height = height;
    ProcessingIndicator.theInstance.maxWidth = maxWidth;
    ProcessingIndicator.theInstance.topOffset = topOffset;
    ProcessingIndicator.theInstance.rightOffset = rightOffset;
    return ProcessingIndicator.theInstance;
}

ProcessingIndicator.getInstance = function () {
    return ProcessingIndicator.theInstance;
}

ProcessingIndicator.positionProcessingIndicator = function () {
    if (ProcessingIndicator.theInstance != null)
        ProcessingIndicator.theInstance.position();
}
ProcessingIndicator.stopProcessingIndicator = function () {
    if (ProcessingIndicator.theInstance != null) {
        ProcessingIndicator.theInstance.display(false);

        var procIndicatorDiv = document.getElementById("procIndicatorFloatLyr");

        //remove all Children
        removeChildren(procIndicatorDiv);

        //Remove the procIndicatorFloatLyr div
        document.body.removeChild(procIndicatorDiv);

        //delete ProcessingIndicator.theInstance;
        ProcessingIndicator.theInstance = null;
    }
    if (piStopTimout != null) {
        window.clearTimeout(piStopTimout);
        piStopTimout = null;
    }
}

// Create the processing indicator element
ProcessingIndicator.prototype.createPIElem = function () {
    var elem = document.createElement("div");
    var leftPos = document.body.clientWidth + document.body.scrollLeft - this.maxWidth - this.rightOffset;
    if (document.documentElement.dir == "rtl")
        leftPos = this.rightOffset;

    document.body.appendChild(elem);
    elem.id = "procIndicatorFloatLyr";
    elem.noWrap = "true";
    elem.innerHTML = PSStringBuffer.concat("<table cellpadding=0 cellspacing=0 border=0><tr nowrap>",
                     "<td class='af_progressIndicator_indeterminate'>",
                     "</td>",
                     "</tr></table>");
    elem.style.display = "none";
    elem.style.left = leftPos + "px";
    if (this.topOffset != null)  // if 'null' is passed in for topOffset, CSS will determine top
        elem.style.top = this.topOffset + "px";
    elem.className = "ProcInd";

    return elem;
}


ProcessingIndicator.prototype.position = function () {
    var oldY = document.all ? this.procIndicatorElem.style.pixelTop : this.procIndicatorElem.style.top;
    var newY = document.body.scrollTop + this.topOffset;
    var posChanged = false;
    //in Mozilla/Firefox/Safari, the oldY is in the format of "123px", so strip the px
    if (!document.all)
        oldY = oldY.substring(0, oldY.length - 2);

    if (oldY != newY) {
        if (document.all)
            this.procIndicatorElem.style.pixelTop = newY;
        else
            this.procIndicatorElem.style.top = newY + "px";
        posChanged = true;
    }

    var procIndWidth = this.procIndicatorElem.offsetWidth;
    if (isIOS && procIndWidth && procIndWidth > this.maxWidth)
        this.maxWidth = procIndWidth;

    var oldX = document.all ? this.procIndicatorElem.style.pixelLeft : this.procIndicatorElem.style.left;
    var newX = document.body.clientWidth + document.body.scrollLeft - this.maxWidth - this.rightOffset;
    if (document.documentElement.dir == "rtl")
        newX = document.body.scrollLeft + this.rightOffset;
    if (!document.all)
        oldX = oldX.substring(0, oldX.length - 2);
    if (oldX != newX) {
        if (document.all)
            this.procIndicatorElem.style.pixelLeft = newX;
        else
            this.procIndicatorElem.style.left = newX + "px";
        posChanged = true;
    }

}

ProcessingIndicator.prototype.display = function (show, overrideText) {
    this.procIndicatorElem = document.getElementById("procIndicatorFloatLyr");

    if (this.procIndicatorElem == null) {
        this.procIndicatorElem = this.createPIElem();
        if (isIE)  //IE
        {
            this.procIndMask = new MaskFrame("procIndMask", this.procIndicatorElem);
        }
        else {
            this.procIndMask = null;
        }
    }

    // bug 18902331 - procIndicatorTextSpan was removed for Alta. We will have to do something else if override text is needed.
    //        else
    //        {
    //            if (overrideText)
    //            {
    //                document.getElementById("procIndicatorTextSpan").innerHTML = this.PIText + "...&nbsp;&nbsp;";
    //            }
    //        }
    if (this.procIndicatorElem != null) {
        if (show && !this.isProcIndDisplayed) {
            this.procIndicatorElem.style.display = "block";
            this.movePIIndicatorTimer = window.setInterval("ProcessingIndicator.positionProcessingIndicator()", PI_MOTION_TIMER);
            this.isProcIndDisplayed = true;
        }
        else {
            this.procIndicatorElem.style.display = "none";
            if (this.isProcIndDisplayed) { // clear our timer
                window.clearInterval(this.movePIIndicatorTimer);
            }
            this.isProcIndDisplayed = false;
        }
    }
}

// Since IFRAME masking techniques for floating DIVs are only required to address
// a zIndex bug in IE, don't even define this object if using Firefox
if (isIE)  //IE
{
    function MaskFrame(defaultId, fixedParentElem) {
        var maskFrame = document.createElement('iframe')
        maskFrame.src = "javascript:false;"
        maskFrame.style.display = "none";
        maskFrame.style.position = "absolute";
        //set frameborder off, by default Border on("1")
        maskFrame.frameBorder = "0";
        //set scrolling to no
        maskFrame.scrolling = "no";
        maskFrame.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";

        maskFrame.id = this.defaultId = defaultId;
        this.domElement = maskFrame;
        if (fixedParentElem) {
            this.setFixedParent(fixedParentElem);
        }
    }

    /**
    The setFixedParent method is ONLY used by non-shared Masks (currently this
    is only the ProcessingInd mask).  It is used to stop resetting the
    parentNode for the IFRAME element repeatedly, which appears to be doing
    nasty things to the DOM.  Once the parent is fixed to the IFRAME, it can
    never be reset using the attach or detach methods.
    **/
    MaskFrame.prototype.setFixedParent = function (elem) {
        if (!elem) {
            return;
        }

        elem.parentNode.appendChild(this.domElement);

        //now repoint the attach/detach methods to just show/hide instead
        //this allows an "owned" maskFrame item to maintain it's place in
        //the DOM without being added and removed incessently.
        this.attachMask = this.showMask;
        this.detachMask = this.hideMask;
    }

    MaskFrame.prototype.attachMask = function (elem) {
        if (!elem) {
            return;
        }

        var maskFrame = this.domElement;

        if (maskFrame.parentNode != null)  //already attached to something
        {
            this.detachMask();
        }
        elem.parentNode.appendChild(maskFrame);
        this.showMask(elem);
    }

    MaskFrame.prototype.showMask = function (elem) {
        if (!elem) {
            return;
        }

        var maskFrame = this.domElement;

        if (elem.style.top != "")
            maskFrame.style.top = parseInt(elem.style.top) + "px";

        if (elem.style.left != "")
            maskFrame.style.left = parseInt(elem.style.left) + "px";

        if (elem.className == "showMenu") {
            maskFrame.style.width = elem.offsetWidth - 2 + "px";
            maskFrame.style.height = elem.offsetHeight - 2 + "px";
        }
        else if (elem.className == "MenuDropdownBack") // for E1 toolbar menu dropdowna
        {
            maskFrame.style.width = elem.offsetWidth + 2 + "px";
            maskFrame.style.height = elem.offsetHeight + 2 + "px";
        }
        else {
            maskFrame.style.width = elem.offsetWidth + "px";
            //<Iframe> is going out of Preference popup window.Adjust height
            if (elem.id == "UI_Pref")
                maskFrame.style.height = elem.offsetHeight - 4 + "px";
            else
                maskFrame.style.height = elem.offsetHeight + "px";
        }
        maskFrame.style.zIndex = elem.style.zIndex - 1;
        maskFrame.style.display = "block";
        maskFrame.id = elem.id + "-maskframe";
    }

    MaskFrame.prototype.detachMask = function () {

        this.hideMask();

        var maskFrame = this.domElement;

        //test parent first, in case detach called twice consecutively
        if (maskFrame.parentNode != null) {
            maskFrame.parentNode.removeChild(maskFrame);
        }
    }

    MaskFrame.prototype.hideMask = function () {
        var maskFrame = this.domElement;

        maskFrame.style.display = "none";
        maskFrame.id = this.defaultId;
    }
    /** Create a single multi-use masking IFRAME for generic floating DIV needs **/
    var GlobalMaskFrame = new MaskFrame("globalMask");
}

function PopupWindow(id, resizable, titleInfo, eventListener, depth, serverCall, type) {
    if (type == ADVANCED_VA)
        this.id = id + depth;
    else
        this.id = id;
    this.resizable = resizable;
    this.eventListener = eventListener;
    this.depth = depth;
    this.serverCall = serverCall;
    if (!serverCall && depth != null && depth > 1) {
        this.document = window.parent.document;
        window.parent[this.id] = this;
    }
    else {
        this.document = document;
        window[this.id] = this;
    }
    this.modalMaximized = false;

    this.titleInfo = titleInfo;
    this.minWidth = 188;
    this.minHeight = 60;
    this.windowElem = null;
    this.contentHTML = null;
    this.namespace = null;

    //variables used for moving the window
    this.titleElem = null;
    this.titleHasCapture = false;
    this.initWindowX = this.initWindowY = this.initClientX = this.initClientY = 0;

    //variables used for resizing the window
    this.containerHasCapture = false;
    this.initDirection = "";
    this.initWindowWidth = this.initWindowHeight = 0;
    this.type = type;
    this.windowElem = this.document.getElementById(this.id);
    if (this.windowElem == null) {
        this._createPopupElem(this.id);
        this.windowElem = this.document.getElementById(this.id);
    }
}

PopupWindow.prototype.RESIZE_TOLERANCE = 8;
PopupWindow.prototype.currentInstance = null;
//this should be larger than the layer(z-index) defined as
//UIBLOCKING_LAYER_ZINDEX for UIBlocking layer in UIBlocking.js
var MSGFORM_LAYER_ZINDEX = 100002;
var isNativeContainer = navigator.userAgent.toUpperCase().indexOf("JDECONTAINER") > -1;
PopupWindow.prototype.setMinSize = function (width, height) {
    this.minWidth = width;
    this.minHeight = height;
}

PopupWindow.prototype.resize = function (width, height) {
    if (width != null)
        this.windowElem.style.width = width + "px";
    if (height != null)
        this.windowElem.style.height = height + "px";
}

PopupWindow.prototype._createPopupElem = function (id) {
    var userAccessibilityMode = false;
    if (localStorage.getItem("userAccessibilityMode"))
        userAccessibilityMode = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
    var elem = this.document.createElement("div");
    this.document.body.appendChild(elem);
    elem.id = id;
    if (userAccessibilityMode && (this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF)) {
        elem.setAttribute("tabIndex", 0);
    }
    if (this.depth != null)
        elem.style.zIndex = MSGFORM_LAYER_ZINDEX * this.depth;
    else
        elem.style.zIndex = MSGFORM_LAYER_ZINDEX;
    elem.style.position = 'absolute';
    elem.style.overflow = 'hidden';
}

PopupWindow.prototype.setContent = function (htmlContent) {
    this.contentHTML = htmlContent;
}

PopupWindow.prototype.setNameSpace = function (namespace) {
    this.namespace = namespace;
}

PopupWindow.prototype.getWidth = function () {
    return parseInt(this.windowElem.style.width);
}

PopupWindow.prototype.getHeight = function () {
    return parseInt(this.windowElem.style.height);
}

PopupWindow.prototype._buildHTML = function () {
    var userAccessibilityMode = false;
    if (localStorage.getItem("userAccessibilityMode")) {
        userAccessibilityMode = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
    }
    var sb = new PSStringBuffer();
    if (this.type == ADVANCED_VA) {
        sb.append("<div class='InnerBorder'>");
    }
    sb.append("<table border=0 cellspacing=0 cellpadding=0 width=100%");
    if (this.type != TYPE_AHEAD) {
        // do not use height=100% for TYPE_AHEAD - this allows window to resize automatically.
        sb.append(" height=100%");
    }
    sb.append(">");

    if (this.titleInfo) {
        sb.append("<tbody><tr><td id='popupTitleBar' class='popupTitleBar'><table width=100% height=100% border=0 cellspacing=0 cellpadding=0>");
        sb.append("<tbody><tr><td>");

        if (this.titleInfo.titleIcon) {
            sb.append("<img border=0 hspace=3 src='");
            sb.append(this.titleInfo.titleIcon).append("' alt=\"").append(this.titleInfo.titleText).append("\">");
        }
        else {
            sb.append("&nbsp;");
        }

        var titleTextColor = this.titleInfo.titleTextColor ? this.titleInfo.titleTextColor : "#ffffff";
        sb.append("</td><td ");
        if (this.type == UNHIDE_COL) {
            sb.append("showUnhidePopup=true ");
        }
        sb.append("><nobr><div class='popupTitle' id=popupWindowTitle").append(this.id).append(">");
        sb.append(this.titleInfo.titleText);
        sb.append("</div></nobr></td>");
        var nameSpace = this.namespace == null ? "" : this.namespace;

        if (this.type == ADVANCED_VA) {
            sb.append("<td nowrap>");
            // Item help is null as it does not exist for the current dialog.
            sb.append(buildHelpOptionsTable(this.depth, nameSpace, this.titleInfo.aboutText, this.titleInfo.contextID, null, this.titleInfo.wideContextID, this.titleInfo.helpText, null, true));
            sb.append("</td>");
        }
        else {
            if (this.titleInfo.aboutText != null && this.titleInfo.aboutText) {
                sb.append("<td ");
                if (this.type == UNHIDE_COL) {
                    sb.append("showUnhidePopup=true ");
                }
                sb.append("align=right>");
                sb.append("<div class='imageDiv16'><img onclick=javascript:" + nameSpace + "about() alt=\"").append(this.titleInfo.aboutText).append("\" title = \"").append(this.titleInfo.aboutText).append("\"");
                sb.append(" src='").append(window["E1RES_img_jdeabout_ena_png"]).append("' name='img/jdeabout_ena.png'");
                if (!isTouchEnabled) {
                    sb.append(" onmouseOver=showMotion(this) onmouseOut=removeMotion(this)");
                }
                sb.append("></div></td>");
            }
            if (this.titleInfo.helpText != null) {
                sb.append("<td align=right>");
                sb.append("<div class='imageDiv16'><img onclick=javascript:" + nameSpace + "showHelp('" + this.titleInfo.contextID + "',null,'" + this.titleInfo.wideContextID + "') alt=\"").append(this.titleInfo.helpText).append("\" title = \"").append(this.titleInfo.helpText).append("\"");
                sb.append(" src='").append(window["E1RES_img_jdehelp_ena_png"]).append("' name='img/jdehelp_ena.png'");
                if (!isTouchEnabled) {
                    sb.append(" onmouseOver=showMotion(this) onmouseOut=removeMotion(this)");
                }
                sb.append("></div></td>");
            }
        }

        //maximize/restore icon for Modal Window
        if (this.type == ADVANCED_VA) {
            var titleText = this.modalMaximized ? window.restoreModalForm : window.maximizeModalForm;
            sb.append("<td align=right><div class='imageDiv16'><img id='modalmaxres").append(this.depth).append("' onclick=\"window['" + this.id + "'].toggleModalState()\" alt=\"");
            sb.append(titleText).append("\" title= \"").append(titleText).append("\"");
            sb.append(" src='").append(window["E1RES_img_jdemodalmax_ena_png"]).append("' name='img/jdemodalmax_ena.png'");
            if (!isTouchEnabled) {
                sb.append(" onmouseOver=showMotion(this) onmouseOut=removeMotion(this)");
            }
            sb.append("></div></td>");
        }

        if (this.titleInfo.hasClose) {
            sb.append("<td align=right><div class='imageDiv16'>");
            // If accessibility mode is set to true and popup is for Watchlist or Advanced Query Name
            // then put the Close Image in an Anchor so that it is Accessible through Insert + F7 in JAWS
            sb.append("<a tabindex=0 role=button aria-label=\"").append(jdeWebGUIStaticStrings.JH_CLOSE).append(" ").append(jdeWebGUIStaticStrings.RI_HOVER_TEXT_POPUP).append(" ").append(this.titleInfo.titleText).append("\" style=\"text-decoration: none\" href=\"javascript:void(0)\" onclick=\"javascript:window['").append(this.id).append("'].onClose()\" onKeyPress=\"if(event.keyCode ==32 || event.keyCode == 13){this.click()};\">");
            sb.append("<img id=Close_Prompt");
            sb.append(" src='");
            sb.append(window["E1RES_img_jdeclose_ena_png"]).append("' title='Close' alt='Close' name='img/jdeclose_ena.png' ");
            if (!isTouchEnabled) {
                sb.append(" onmouseOver=showMotion(this) onmouseOut=removeMotion(this)");
            }
            sb.append(">");
            sb.append("</a>");
            sb.append("</div></td>");
        }
        sb.append("</tr></tbody></table></td></tr>");
    }
    var tempID = "modalIframetd";
    if (this.type == ADVANCED_VA || this.type == SHOW_HOVER) {
        tempID = tempID + this.depth;
    }
    sb.append("<tr><td id='").append(tempID).append("' type='" + this.type + "'  style='width:100%;height:100%'");
    if (this.type == UNHIDE_COL) {
        sb.append(" showUnhidePopup=true");
    }
    sb.append(">");
    sb.append(this.contentHTML);
    sb.append("</td></tr>");
    sb.append("</tbody></table>");
    if (this.type == ADVANCED_VA) {
        sb.append("</div>");
    }
    this.windowElem.innerHTML = sb.toString();
    delete this.contentHTML;
}

PopupWindow.prototype._buildHTMLForHoverPopup = function () {
    var userAccessibilityMode = false;
    if (localStorage.getItem("userAccessibilityMode")) {
        userAccessibilityMode = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
    }
    var sb = new PSStringBuffer();
    var isRTL = (document.documentElement.dir == "rtl");
    sb.append("<div>");
    sb.append("<div id='popupTitleBar' class='popupTitleBar'>");

    sb.append("<div class='popupTitle' id=popupWindowTitle").append(this.id).append(">");
    sb.append("<span style=\"width:90% ; overflow: hidden; float:left; white-space:nowrap;text-overflow: ellipsis;\">");
    sb.append(this.titleInfo.titleText);
    sb.append("</span>");
    sb.append("<div class='imageDiv16'><img role=button tabindex=0 ")
    if (userAccessibilityMode) {
        sb.append("aria-label=\"")
        sb.append(jdeWebGUIStaticStrings.JH_CLOSE).append(" ").append(jdeWebGUIStaticStrings.RI_HOVER_TEXT_POPUP);
        sb.append(" ");
        sb.append(this.titleInfo.titleText).append("\"");
    }

    sb.append(" onclick=\"javascript:window['").append(this.id).append("'].onClose()\" onkeydown=\"if(event.keyCode ==32 || event.keyCode == 13){this.click()};\"");
    // Image tweak for the native container for image consistency across apps.
    if (isNativeContainer) {
        sb.append(" src='").append(window["E1RES_img_NC_close_png"]).append("' alt=\"").append(this.titleInfo.titleText).append("\" name='img/close.gif' hspace=3 border=0>");
    }
    else {
        sb.append(" src='").append(window["E1RES_img_jdeclose_ena_png"]).append("' title=\"").append(this.titleInfo.titleText).append("\" alt=\"").append(this.titleInfo.titleText).append("\" name='img/jdeclose_ena.png' onmouseOver=showMotion(this) onmouseOut=removeMotion(this) hspace=3 border=0>");
    }
    sb.append("</div>"); // img
    sb.append("</div>"); // popupTitle
    sb.append("</div>"); // popupTitleBar

    sb.append("<div>");
    sb.append(this.contentHTML);
    sb.append("</div>");

    sb.append("</div>");
    this.windowElem.innerHTML = sb.toString();
    delete this.contentHTML;
    if (this.type == SHOW_POPUP && typeof preformatHoverForms == 'function')  // this is only defined for hover forms (in PopupTabs.js).
    {
        try { preformatHoverForms(); } catch (e) { }
    }
}

PopupWindow.prototype.display = function (left, top, width, height) {
    if (this.type == SHOW_HOVER) {
        this.windowElem.style.height = height + "px";
        if (!this.serverCall || this.depth == 1) {
            window.openedPopup = this;
        }
        else//page re-rendered, hover is displayed on a modal form, set the openedPopup to the previous window
        {
            document.getElementById("modalIframe" + (this.depth - 1)).contentWindow.openedPopup = this;
        }
    }
    else if (this.type != ADVANCED_VA)//VA form is closed explicitly by the end user
    {
        window.openedPopup = this;
    }
    if (this.type == SHOW_POPUP || this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF) {
        this._buildHTMLForHoverPopup();
    }
    else {
        this._buildHTML();
    }
    this.titleElem = this.document.getElementById("popupWindowTitle" + this.id);
    if (this.titleInfo)
        this.windowElem.title = this.titleInfo.titleText;

    this.windowElem.style.left = left + "px";
    this.windowElem.style.top = top + "px";
    this.windowElem.style.width = width + "px";
    // try letting the height adjust on it's own...  this.windowElem.style.height = height + "px";
    if (this.type == FREEZE_COL) {
        this.windowElem.className = "FreezeMenu";
    }
    else  //Try to use this default to make most popup windows consistent.
    {
        this.windowElem.className = "popupWindow";
    }
    this.windowElem.style.display = "block";

    // If title bar is larger than the passed in width, we need to adjust.
    // This happens mostly with INYFE windows where error message is the title.
    var titleBar = this.titleElem;
    if (titleBar) {
        var titlePadding = this.getTitlePadding();
        if (titleBar && titleBar.offsetWidth + titlePadding > width) {
            this.windowElem.style.width = (titleBar.offsetWidth + titlePadding) + "px";
        }
    }

    if (this.type == TYPE_AHEAD) {
        var typeAheadWin = document.getElementById('typeAheadWindow');
        typeAheadWin.style.height = typeAheadWin.children[0].clientHeight + "px";
    }

    if (this.resizable) {
        if (!this.serverCall && this.depth != null && this.depth > 1) {
            this.windowElem.onmousedown = new Function("window.parent['" + this.id + "'].mouseDownContainer(arguments[0])");
            this.windowElem.onmousemove = new Function("window.parent['" + this.id + "'].mouseMoveContainer(arguments[0])");
            this.windowElem.onmouseup = new Function("window.parent['" + this.id + "'].mouseUpContainer(arguments[0])");
        }
        else {
            if (this.type != SHOW_POPUP) {
                this.windowElem.onmousedown = new Function("window['" + this.id + "'].mouseDownContainer(arguments[0])");
                this.windowElem.onmouseup = new Function("window['" + this.id + "'].mouseUpContainer(arguments[0])");
            }

            this.windowElem.onmousemove = new Function("window['" + this.id + "'].mouseMoveContainer(arguments[0])");
        }
    }
    this.windowElem.onkeydown = new Function("window['" + this.id + "'].keyDownContainer(arguments[0])");
    if (this.autoHide)
        this.windowElem.onmouseout = new Function("window['" + this.id + "'].mouseOutContainer(arguments[0])");

    if (this.titleElem != null) {
        if (this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF || this.type == ENHANCED_ATTACHMENT
            || this.type == UDO || this.type == UNHIDE_COL || this.type == ADVANCED_QUERY) {
            this.titleElem.onmousedown = new Function("window['" + this.id + "'].mouseDownTitle(arguments[0])");
            this.titleElem.onmousemove = new Function("window['" + this.id + "'].mouseMoveFavPrefTitle(arguments[0])");
            this.titleElem.onmouseup = new Function("window['" + this.id + "'].mouseUpTitle(arguments[0])");
        }
        else if (!this.serverCall && this.depth != null && this.depth > 1) {
            this.titleElem.onmousedown = new Function("window.parent['" + this.id + "'].mouseDownTitle(arguments[0])");
            this.titleElem.onmousemove = new Function("window.parent['" + this.id + "'].mouseMoveTitle(arguments[0])");
            this.titleElem.onmouseup = new Function("window.parent['" + this.id + "'].mouseUpTitle(arguments[0])");
            if (this.type == ADVANCED_VA) {
                this.titleElem.ondblclick = new Function("window.parent['" + this.id + "'].toggleModalState()");
            }
        }
        else if (this.type == ADVANCED_VA) {
            this.titleElem.onmousedown = new Function("window['" + this.id + "'].mouseDownTitle(arguments[0])");
            this.titleElem.onmousemove = new Function("window['" + this.id + "'].mouseMoveTitle(arguments[0])");
            this.titleElem.onmouseup = new Function("window['" + this.id + "'].mouseUpTitle(arguments[0])");
            this.titleElem.ondblclick = new Function("window['" + this.id + "'].toggleModalState()");
        }
    }
    this.adjustForAutoCenter();
    if (this.type == SHOW_POPUP || this.type == SHOW_UI_PREF) {
        showItemShadow(this.windowElem, null, false, true);
        moveItemShadow(this.windowElem);
    }
    else if (this.type == SHOW_POPUP_FAV) {
        if (!isWSRPContainer() && isIE) {
            showItemShadow(this.windowElem, null, false, true);
            moveItemShadow(this.windowElem);
        }
        else {
            showItemShadow(this.windowElem, null, false, true);
        }
    }
    else if (this.type != ADVANCED_VA && this.type != SHOW_HOVER) {
        showItemShadow(this.windowElem, null);
    }
    var userAccessibilityMode = false;
    if (localStorage.getItem("userAccessibilityMode"))
        userAccessibilityMode = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
    if (userAccessibilityMode && (this.type == SHOW_POPUP || this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF))
        setFocusonPopups(this.windowElem);   // Setting focus on the popup window
}

function setFocusonPopups(elem) {
    elem.focus();
}

PopupWindow.prototype.getImgOffsetOnTitle = function () {
    if (this.type == ADVANCED_VA)
        return 96;
    var count = 0;
    if (this.titleInfo) {
        if (this.titleInfo.aboutText)
            count++;
        if (this.titleInfo.helpText)
            count++;
        if (this.titleInfo.hasClose)
            count++;
    }
    return count * 25;
}

// include blank before title and close icon after title
PopupWindow.prototype.getTitlePadding = function () {
    if (this.id == "CONTENT_PALETTE" || this.id == "CONTENT_FACTORY")
        return 0;
    return this.getImgOffsetOnTitle() + 17;
}

PopupWindow.prototype.hide = function () {
    if (this.type == ADVANCED_VA) {
        return;
    }
    else if (this.type == SHOW_HOVER) {
        window.openedPopup = null;
        this.onClose();
    }
    else {
        this.windowElem.style.display = "none";
        removeItemShadow(this.windowElem, null, false);
        // Clean up the DOM for the Hover Form
        // by removing all Children if they are present
        // otherwise the DOM is not refreshing with latest content
        if (this.type == SHOW_POPUP)
            removeAllChildren(this.windowElem);
    }
}

PopupWindow.prototype.destory = function () {
    if (this.type != ADVANCED_VA)
        removeItemShadow(this.windowElem, null);
    this.windowElem.parentNode.removeChild(this.windowElem);
}


// show a previously hidden popup window based the reference control
PopupWindow.prototype.show = function (ctrl) {
    if (this.type != ADVANCED_VA)//VA form is closed explicitly by the end user
        window.openedPopup = this;
    this.focusedControl = ctrl;
    this.windowElem.style.display = "block";
    this.adjustForAutoCenter();
    if (this.type != ADVANCED_VA) {
        showItemShadow(this.windowElem, null);
        moveItemShadow(this.windowElem);
    }
}

PopupWindow.prototype.moveTo = function (left, top) {
    this.windowElem.style.left = left + "px";
    this.windowElem.style.top = top + "px";
    if (this.type != ADVANCED_VA)
        moveItemShadow(this.windowElem);
}

PopupWindow.prototype.onClose = function () {

    if (document.getElementById("UI_Pref") != null) {
        clearBreadcrumbHistory = false;
    }
    if (this.type == ADVANCED_VA) {
        var iframeWindow = null;
        if (!this.serverCall && this.depth != null && this.depth > 1) {
            iframeWindow = window.parent.document.getElementById("modalIframe" + this.depth).contentWindow;
        }
        else {
            iframeWindow = document.getElementById("modalIframe" + this.depth).contentWindow;
        }
        var rootContainer = iframeWindow.JSCompMgr.getRootContainer(this.namespace);
        rootContainer.processHotkey(this.namespace, null, 76, true, true, false); // trigger Ctrl_Alt_L
    }
    else if (this.type == SHOW_HOVER) {
        if (!this.serverCall && this.depth != null && this.depth > 1) {
            iframeWindow = window.parent.document.getElementById("HoverFrame").contentWindow;
        }
        else {
            iframeWindow = document.getElementById("HoverFrame").contentWindow;
        }
        iframeWindow.JDEDTAFactory.getInstance(this.namespace).doAsynPostWithoutProcIndicator("closePopup");
    }
    else {
        if (this.eventListener && this.eventListener.onClose)
            this.eventListener.onClose();
        this.hide();
    }
}

PopupWindow.prototype.mouseDownTitle = function (e) {

    // clearInterval(window.intervalIdForOpenedPopup);
    if (this.eventListener && this.eventListener.mouseDown)
        this.eventListener.mouseDown();
    if (!this.serverCall && this.depth != null && this.depth > 1)
        var evt = e ? e : window.parent.event;
    else
        var evt = e ? e : window.event;
    // set/releaseCapture is functionality unique to IE.
    // Checking for IE's existence because feature detection is failing with Firefox thinking it supports the feature.
    if (isIE) {
        this.titleElem.setCapture();
    }
    else {
        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
        this.oldMouseUp = this.document["onmouseup"];
        this.document["onmouseup"] = this.mouseUpTitle;

        // Special circumstances for onmousemove to mirror logic when popups display and the appropriate events there.
        this.oldMouseMove = this.document["onmousemove"];
        if ((this.titleElem != null && this.type) && (this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF)) {
            this.document["onmousemove"] = this.mouseMoveFavPrefTitle;
        }
        else {
            this.document["onmousemove"] = this.mouseMoveTitle;
        }
    }
    this.initWindowX = parseInt(this.windowElem.style.left);
    this.initWindowY = parseInt(this.windowElem.style.top);
    this.initClientX = (evt.pageX === undefined) ? evt.clientX : evt.pageX;
    this.initClientY = (evt.pageY === undefined) ? evt.clientY : evt.pageY;
    this.titleHasCapture = true;
    PopupWindow.prototype.currentInstance = this;
    if (isIE) {
        evt.cancelBubble = true;
        evt.returnValue = false;
    }
    else {
        if (evt.cancelable) {
            evt.stopPropagation();
            evt.preventDefault();
        }
    }
}

PopupWindow.prototype.mouseMoveFavPrefTitle = function (e) {
    var popupWin = this;
    var evt = document.all ? window.parent.event : e;
    if (!document.all && this != PopupWindow.prototype.currentInstance &&
        PopupWindow.prototype.currentInstance != null) {
        popupWin = PopupWindow.prototype.currentInstance;
    }
    if (!popupWin.titleHasCapture)
        return;
    if (evt.pageX !== undefined) {
        popupWin.windowElem.style.left = popupWin.initWindowX + (evt.pageX - popupWin.initClientX) + "px";
        popupWin.windowElem.style.top = popupWin.initWindowY + (evt.pageY - popupWin.initClientY) + "px";
    }
    else {
        // IE8
        popupWin.windowElem.style.left = popupWin.initWindowX + (evt.clientX - popupWin.initClientX) + "px";
        popupWin.windowElem.style.top = popupWin.initWindowY + (evt.clientY - popupWin.initClientY) + "px";
    }

    moveItemShadow(popupWin.windowElem);

    if (popupWin.eventListener && popupWin.eventListener.movedTo)
        popupWin.eventListener.movedTo(popupWin.windowElem.style.left, popupWin.windowElem.style.top);

}

PopupWindow.prototype.mouseMoveTitle = function (e) {
    var popupWin = this;
    if (!document.all && this != PopupWindow.prototype.currentInstance &&
        PopupWindow.prototype.currentInstance != null) {
        popupWin = PopupWindow.prototype.currentInstance;
    }
    if (!popupWin.titleHasCapture)
        return;
    if (!popupWin.serverCall && popupWin.depth != null && popupWin.depth > 1) {
        var evt = e ? e : window.parent.event;
        var clientWidth = window.parent.document.body.clientWidth;
        var clientHeight = window.parent.document.body.clientHeight;
    }
    else {
        var evt = e ? e : window.event;
        var clientWidth = getWindowWidth();
        var clientHeight = getWindowHeight();
    }

    var newLeft = popupWin.initWindowX + evt.clientX - popupWin.initClientX;
    var newTop = popupWin.initWindowY + evt.clientY - popupWin.initClientY;
    if (newLeft < (clientWidth - popupWin.windowElem.offsetWidth))
        popupWin.windowElem.style.left = Math.max(0, newLeft) + "px";
    else
        popupWin.windowElem.style.left = Math.min(clientWidth - popupWin.windowElem.offsetWidth, newLeft) + "px";
    if (newTop < (clientHeight - popupWin.windowElem.offsetHeight))
        popupWin.windowElem.style.top = Math.max(0, newTop) + "px";
    else
        popupWin.windowElem.style.top = Math.min(clientHeight - popupWin.windowElem.offsetHeight, newTop) + "px";
    if (this.type != ADVANCED_VA)
        moveItemShadow(popupWin.windowElem);

    if (popupWin.eventListener && popupWin.eventListener.movedTo)
        popupWin.eventListener.movedTo(popupWin.windowElem.style.left, popupWin.windowElem.style.top);
}

PopupWindow.prototype.mouseUpTitle = function (e) {
    var popupWin = this;
    if (!document.all && this != PopupWindow.prototype.currentInstance &&
    PopupWindow.prototype.currentInstance != null) {
        popupWin = PopupWindow.prototype.currentInstance;
    }

    PopupWindow.prototype.currentInstance = null;
    if (true == popupWin.titleHasCapture) {
        // set/releaseCapture is functionality unique to IE.
        // Checking for IE's existence because feature detection is failing with Firefox thinking it supports the feature.
        if (isIE) {
            popupWin.titleElem.releaseCapture();
        }
        else {
            window.releaseEvents(Event.MOUSEMOVE | Event.MOUSEUP);
            this.document["onmousemove"] = popupWin.oldMouseMove;
            this.document["onmouseup"] = popupWin.oldMouseUp;
        }
        popupWin.titleHasCapture = false;
    }
}

PopupWindow.prototype.getContainerMoveDirection = function (e, containerElm) {
    var direction = "";
    if (!this.serverCall && this.depth != null && this.depth > 1)
        var evt = e ? e : window.parent.event;
    else
        var evt = e ? e : window.event;
    var container = document.all ? evt.srcElement : e.target;
    var x, y, windowX, windowY;

    if (evt.pageX !== undefined) {
        x = evt.pageX;
        y = evt.pageY;
    }
    else {
        // IE8
        x = evt.x + this.document.body.scrollLeft;
        y = evt.y + this.document.body.scrollTop;
    }

    windowX = getAbsoluteLeftPos(containerElm);
    windowY = getAbsoluteTopPos(containerElm);

    if ((y - windowY) < PopupWindow.prototype.RESIZE_TOLERANCE)
        direction += "";
    else if ((y - windowY) > containerElm.offsetHeight - PopupWindow.prototype.RESIZE_TOLERANCE)
        direction += "s";
    if ((x - windowX) < PopupWindow.prototype.RESIZE_TOLERANCE)
        direction += "w";
    else if ((x - windowX) > containerElm.offsetWidth - PopupWindow.prototype.RESIZE_TOLERANCE)
        direction += "e";
    return direction;
}

PopupWindow.prototype.keyDownContainer = function (event) {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    if (keyCode == 27) // escape key closes the popup
    {
        window[this.id].onClose();
    }
}

PopupWindow.prototype.mouseDownContainer = function (e) {
    if (!this.serverCall && this.depth != null && this.depth > 1)
        clearInterval(window.parent.intervalIdForOpenedPopup);
    else
        clearInterval(window.intervalIdForOpenedPopup);
    if (this.eventListener && this.eventListener.mouseDown)
        this.eventListener.mouseDown();

    if (!this.serverCall && this.depth != null && this.depth > 1)
        var evt = e ? e : window.parent.event;
    else
        var evt = e ? e : window.event;
    var container = document.all ? evt.srcElement : e.target;

    this.initDirection = this.getContainerMoveDirection(e, this.windowElem);
    if (this.initDirection == "") {
        return;
    }
    this.containerHasCapture = true;
    // set/releaseCapture is functionality unique to IE.
    // Checking for IE's existence because feature detection is failing with Firefox thinking it supports the feature.
    if (isIE) {
        this.windowElem.setCapture();
    }
    else {
        window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
        this.oldMouseMove = this.document["onmousemove"];
        this.document["onmousemove"] = this.mouseMoveContainer;
        this.oldMouseUp = this.document["onmouseup"];
        this.document["onmouseup"] = this.mouseUpContainer;
    }
    this.initWindowX = parseInt(this.windowElem.offsetLeft);
    this.initWindowY = parseInt(this.windowElem.offsetTop);
    this.initClientX = evt.clientX;
    this.initClientY = evt.clientY;
    this.initWindowWidth = parseInt(this.windowElem.offsetWidth);
    this.initWindowHeight = parseInt(this.windowElem.offsetHeight);
    PopupWindow.prototype.currentInstance = this;
    if (isIE) {
        evt.cancelBubble = true;
        evt.returnValue = false;
    }
    else {
        if (evt.cancelable) {
            evt.stopPropagation();
            evt.preventDefault();
        }
    }
}

PopupWindow.prototype.mouseMoveContainer = function (e) {
    var popupWin = this;

    if (!document.all && this != PopupWindow.prototype.currentInstance && PopupWindow.prototype.currentInstance != null) {
        popupWin = PopupWindow.prototype.currentInstance;
    }

    if (!popupWin.serverCall && popupWin.depth != null && popupWin.depth > 1) {
        clearInterval(window.parent.intervalIdForOpenedPopup);
    }
    else {
        clearInterval(window.intervalIdForOpenedPopup);
    }

    var modalIframetd = document.getElementById("modalIframetd");
    if (!popupWin.serverCall && popupWin.depth != null && popupWin.depth > 1) {
        var evt = e ? e : window.parent.event;
        if (this.type == ADVANCED_VA) {
            modalIframetd = window.parent.document.getElementById("modalIframetd" + this.depth);
        }
    }
    else {
        var evt = e ? e : window.event;
        if (this.type == ADVANCED_VA) {
            modalIframetd = document.getElementById("modalIframetd" + this.depth);
        }
    }

    if (!popupWin.containerHasCapture) {
        var direction = popupWin.getContainerMoveDirection(e, popupWin.windowElem);
        if (this.type == SHOW_POPUP || this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF || this.type == FREEZE_COL) {
            popupWin.windowElem.style.cursor = "default";
        }
        else {
            if (direction == "") {
                popupWin.windowElem.style.cursor = "default";
            }
            else {
                popupWin.windowElem.style.cursor = direction + "-resize";
            }
        }
        return;
    }

    if (popupWin.initDirection.indexOf("e") != -1) {
        popupWin.windowElem.style.width = Math.max(popupWin.minWidth, popupWin.initWindowWidth + evt.clientX - popupWin.initClientX) + "px";
    }
    if (popupWin.initDirection.indexOf("s") != -1) {
        popupWin.windowElem.style.height = Math.max(popupWin.minHeight, popupWin.initWindowHeight + evt.clientY - popupWin.initClientY) + "px";
        if (modalIframetd) {
            if (!isIOS) {
                modalIframetd.style.height = parseInt(popupWin.windowElem.style.height) - 4 - 23 + "px";
            }
        }
    }
    if (popupWin.initDirection.indexOf("w") != -1) {
        popupWin.windowElem.style.left = Math.min(popupWin.initWindowX + evt.clientX - popupWin.initClientX,
        popupWin.initWindowX + popupWin.initWindowWidth - popupWin.minWidth) + "px";
        popupWin.windowElem.style.width = Math.max(popupWin.minWidth,
        popupWin.initWindowWidth - evt.clientX + popupWin.initClientX) + "px";
    }
    if (popupWin.initDirection.indexOf("n") != -1) {
        popupWin.windowElem.style.top = Math.min(popupWin.initWindowY + evt.clientY - popupWin.initClientY,
        popupWin.initWindowY + popupWin.initWindowHeight - popupWin.minHeight) + "px";
        popupWin.windowElem.style.height = Math.max(popupWin.minHeight,
        popupWin.initWindowHeight - evt.clientY + popupWin.initClientY) + "px";
        if (modalIframetd) {
            if (!isIOS) {
                modalIframetd.style.height = parseInt(popupWin.windowElem.style.height) - 4 - 23 + "px";
            }
        }
    }
    if (this.type != ADVANCED_VA) {
        moveItemShadow(popupWin.windowElem);
    }
    if (this.titleElem) {
        if (this.type == SHOW_POPUP || this.type == SHOW_POPUP_FAV || this.type == SHOW_UI_PREF) {
            this.titleElem.style.width = parseInt(popupWin.windowElem.style.width) - 50 + "px";
        }
        else {
            this.titleElem.style.width = parseInt(popupWin.windowElem.style.width) - this.getTitlePadding() + "px";
        }
    }

    if (this.type == UNHIDE_COL) {
        var unHideColsListDiv = document.getElementById('unHideColsList');
        if (unHideColsListDiv) {
            unHideColsListDiv.style.height = Math.max(popupWin.minHeight, popupWin.initWindowHeight + evt.clientY - popupWin.initClientY) - 60 + "px";
        }
    }

    if (popupWin.eventListener && popupWin.eventListener.resizedTo) {
        popupWin.eventListener.resizedTo(popupWin.windowElem.style.width, popupWin.windowElem.style.height);
    }
}

PopupWindow.prototype.mouseUpContainer = function (e) {
    var popupWin = this;
    if (!document.all && this != PopupWindow.prototype.currentInstance &&
        PopupWindow.prototype.currentInstance != null) {
        popupWin = PopupWindow.prototype.currentInstance;
    }

    if (!popupWin.serverCall && popupWin.depth != null && popupWin.depth > 1) {
        clearInterval(window.parent.intervalIdForOpenedPopup);
    }
    else {
        clearInterval(window.intervalIdForOpenedPopup);
    }

    PopupWindow.prototype.currentInstance = null;

    if (true == popupWin.containerHasCapture) {
        // set/releaseCapture is functionality unique to IE.
        // Checking for IE's existence because feature detection is failing with Firefox thinking it supports the feature.
        if (isIE) {
            popupWin.windowElem.releaseCapture();
        }
        else {
            window.releaseEvents(Event.MOUSEMOVE | Event.MOUSEUP);
            this.document["onmousemove"] = popupWin.oldMouseMove;
            this.document["onmouseup"] = popupWin.oldMouseUp;
        }
        popupWin.containerHasCapture = false;
    }
}

PopupWindow.prototype.mouseOutContainer = function (e) {
    if (window.openedPopup)
        window.intervalIdForOpenedPopup = setInterval(this.decidePopupWindowVisibility, 500);
}

PopupWindow.prototype.decidePopupWindowVisibility = function (e) {
    if (window.openedPopup && window.intervalIdForOpenedPopup) {
        clearInterval(window.intervalIdForOpenedPopup);
        window.openedPopup.hide();
        window.openedPopup = null;
        window.intervalIdForOpenedPopup = null;
    }
}

// The passed in position and demention is the a small reference area, which has to
// be inside of new area of popup window for ADVANCED_QUERY, ADVANCED_VA
// but has to be nearest around to the reference area for TYPE_AHEAD, ADVANCED_VA
PopupWindow.prototype.adjustForAutoCenter = function () {
    if (!this.autoCenter)
        return;

    var ctrl = this.focusedControl;
    var ownerDoc = null;
    var parentDiv = null;
    if (this.depth > 1) {
        if (this.serverCall) {
            if (this.type == ADVANCED_VA) {
                ownerDoc = document.getElementById("modalIframe" + (this.depth - 1)).contentWindow.document;
                parentDiv = document.getElementById("ModalSS" + (this.depth - 1));
            }
            else if (this.type == SHOW_HOVER) {
                ownerDoc = document.getElementById("HoverFrame").contentWindow.document;
                parentDiv = document.getElementById("Hover");
            }
        }
        else {
            if (this.type == ADVANCED_VA) {
                ownerDoc = window.parent.document.getElementById("modalIframe" + (this.depth - 1)).contentWindow.document;
                parentDiv = window.parent.document.getElementById("ModalSS" + (this.depth - 1));
            }
            else if (this.type == SHOW_HOVER) {
                ownerDoc = window.parent.document.getElementById("HoverFrame").contentWindow.document;
                parentDiv = window.parent.document.getElementById("Hover");
            }
        }
    }
    if (ctrl == null && this.type != ADVANCED_VA && this.type != UDO)
        return;
    if (ctrl != null) {
        var refTop = getAbsoluteTopPos(ctrl, true, ownerDoc);
        if (parentDiv) {
            refTop = refTop + parentDiv.offsetTop;
        }
        var refLeft = getAbsoluteLeftPos(ctrl, true, ownerDoc);
        if (parentDiv) {
            refLeft = refLeft + parentDiv.offsetLeft;
        }
        var refWidth = ctrl.offsetWidth;
        var refHeight = ctrl.offsetHeight;
    }

    var windowElem = this.windowElem;
    if (this.type == ADVANCED_VA) {
        var left = 80;
        var top = 80;
    }
    else {
        var left = windowElem.offsetLeft;
        var top = windowElem.offsetTop;
    }
    if (this.type == ADVANCED_VA || this.type == SHOW_HOVER || this.type == UDO) {
        var width = this.minWidth;
        var height = this.minHeight;
    }
    else {
        var width = windowElem.offsetWidth;
        var height = windowElem.offsetHeight;
    }
    if (this.depth > 1 && !this.serverCall) {
        var clientWidth = window.parent.document.documentElement.clientWidth;
        var clientHeight = window.parent.document.documentElement.clientHeight;
    }
    else {
        var clientWidth = document.documentElement.clientWidth;
        var clientHeight = document.documentElement.clientHeight;
    }
    try {
        var innerRCUXElem = self.parent.document.getElementById('innerRCUX');
    }
    catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
        var innerRCUXElem = null;
    }
    if (isIOS && innerRCUXElem) {
        var clientWidth = innerRCUXElem.offsetWidth;
        var clientHeight = innerRCUXElem.offsetHeight;
    }
    switch (this.type) {
        //For Modal Window, display the window next to the parent control, adjust the y position so the whole form could fit within the height 
        //of the browser, shift the x position and set the size of the window to fit within the browser. 
        case ADVANCED_VA:
            var ctrlOutofViewArea = (refTop === refLeft === refWidth === refHeight == 0) ? true : false;
            var newLeft = left;
            var newTop = top;
            var newWidth = width + 8;  // adding a few pixels gets rid of extra scroll bars on ADV VA popup
            var newHeight = height + 8;

            if (ctrl != null && (refLeft + refWidth) <= clientWidth && refLeft >= 0 && !this.modalMaximized && !ctrlOutofViewArea) {
                if ((clientWidth - (refLeft + refWidth)) > refLeft && ((clientWidth - (refLeft + refWidth)) >= (width + 10)))//there's more space on the right, show to the right of the control
                {
                    newLeft = refLeft + refWidth;
                    newWidth = Math.min(width, clientWidth - newLeft);
                }
                else if (refLeft >= (width + 10))// show to the left of the control
                {
                    newLeft = Math.max(0, refLeft - width);
                    newWidth = Math.min(refLeft, width);
                }
                else {
                    if (clientWidth <= width + 10) {
                        newWidth = clientWidth - 10;
                        newLeft = 5;
                    }
                    else {
                        newLeft = (clientWidth - width) / 2;
                    }
                }
                if (clientHeight > (height + 28))// down shift but never go out of bandary if there is enough space
                {
                    if ((clientHeight - refTop) >= height) {
                        // down shift
                        newTop = refTop;
                    } else if (refTop - height > 0) {
                        //up shift
                        newTop = refTop - height;
                    } else {
                        newTop = Math.max((clientHeight - height) / 2, 0);
                    }
                }
                else // up shift but never go out of bandary if there is enough space
                {
                    newTop = 23;
                    newHeight = clientHeight - 5;
                }
            }
            else//if the parent control is not available, display the form in the middle
            {
                if (this.modalMaximized) {
                    if (this.depth == 1) {
                        newTop = 40;
                        newLeft = 30;
                        newHeight = clientHeight - 70;
                        newWidth = clientWidth - 60;
                    }
                    else// indent nested modal forms
                    {
                        newTop = 40 + 24 * (this.depth - 1);
                        newLeft = 30 + 24 * (this.depth - 1);
                        newHeight = clientHeight - (70 + 24 * (this.depth - 1));
                        newWidth = clientWidth - (60 + 24 * (this.depth - 1));
                    }
                }
                else {
                    if (clientHeight <= (height + 28)) {
                        newHeight = clientHeight - 5;
                        newTop = 23;
                    }
                    else {
                        newTop = (clientHeight - height) / 2;
                    }
                    if (clientWidth <= width + 10) {
                        newWidth = clientWidth - 10;
                        newLeft = 5;
                    }
                    else {
                        newLeft = (clientWidth - width) / 2;
                    }
                }
            }
            windowElem.style.left = newLeft + "px";
            windowElem.style.top = newTop + "px";
            windowElem.style.height = newHeight + "px";
            windowElem.style.width = newWidth + "px";
            this.titleElem.style.width = newWidth - this.getImgOffsetOnTitle() + "px";
            var modalIframetd = document.getElementById("modalIframetd" + this.depth);
            if (!this.serverCall && this.depth != null && this.depth > 1) {
                modalIframetd = window.parent.document.getElementById("modalIframetd" + this.depth);
            }
            if (modalIframetd) {
                if (!isIOS) {
                    modalIframetd.style.height = newHeight - 4 - 23 + "px"; //Required for IE<=10
                }
            }
            break;
        case ADVANCED_QUERY:
        case TYPE_AHEAD:
        case SHOW_POPUP:
        case SHOW_POPUP_FAV:
        case SHOW_UI_PREF:
        case SHOW_HOVER:
            var newLeft;
            var newTop;
            if (clientWidth / 2 > refLeft + refWidth / 2)// right shift
            {
                newLeft = Math.min(refLeft, clientWidth - width);
            }
            else // left shift
            {
                newLeft = Math.max(refLeft + refWidth - width, 0);
            }

            if (clientHeight / 2 > refTop + refHeight / 2)// down shift but never overlapping with reference area
            {
                newTop = refTop + refHeight;
            }
            else // up shift but never overlapping with reference area
            {
                newTop = refTop - height;
            }
            windowElem.style.left = newLeft + "px";
            windowElem.style.top = newTop + "px";
            if (this.type == SHOW_HOVER) {
                windowElem.style.height = height + "px";
                windowElem.style.width = width + "px";
                var modalIframetd = document.getElementById("modalIframetd");
                if (!this.serverCall && this.depth != null && this.depth > 1) {
                    modalIframetd = window.parent.document.getElementById("modalIframetd" + this.depth);
                }
                if (modalIframetd) {
                    if (!isIOS) {
                        modalIframetd.style.height = height - 4 + "px"; //Required for IE<=10
                    }
                }
            }
            break;
        case UDO:
            if (clientHeight <= (height + 28)) {
                height = clientHeight - 5;
                top = 23;
            }
            else {
                top = (clientHeight - height) / 2;
            }
            if (clientWidth <= width + 10) {
                width = clientWidth - 10;
                left = 5;
            }
            else {
                left = (clientWidth - width) / 2;
            }
            windowElem.style.left = left + "px";
            windowElem.style.top = top + "px";
            windowElem.style.height = height + "px";
            windowElem.style.width = width + "px";
            break;
    }
}

PopupWindow.prototype.toggleModalState = function () {
    this.modalMaximized = !this.modalMaximized;

    //swap the image
    var img = document.getElementById("modalmaxres" + this.depth);
    if (!this.serverCall && this.depth != null && this.depth > 1) {
        img = window.parent.document.getElementById("modalmaxres" + this.depth);
    }
    if (this.modalMaximized) {
        img.src = window["E1RES_img_jdemodalrestore_ena_png"];
        img.alt = window.restoreModalForm;
        img.title = window.restoreModalForm;
        img.name = 'img/jdemodalrestore_ena.png';
    }
    else {
        img.src = window["E1RES_img_jdemodalmax_ena_png"];
        img.alt = window.maximizeModalForm;
        img.title = window.maximizeModalForm;
        img.name = 'img/jdemodalmax_ena.png';
    }

    this.adjustForAutoCenter();

    //notify server the new state
    var baseWindow = document.getElementById("modalIframe" + this.depth);
    if (!this.serverCall && this.depth != null && this.depth > 1) {
        baseWindow = parent.window.document.getElementById("modalIframe" + this.depth);
    }

    baseWindow.contentWindow.JDEDTAFactory.getInstance(this.namespace).doAsynPostWithoutProcIndicator("toggleModalFormState");
}

/*Native Container(iPad) Exit Menu Code*/
var nativeExitMenu;
if (!nativeExitMenu) {
    nativeExitMenu = new Object();
}

function initExitMenu() {
    //Change dom structure here
    var endEvents = ['transitionend', 'webkitTransitionEnd'];
    for (i = 0; i < endEvents.length; i++) {
        addMouseEvent(document.getElementById('exitMenuTable'), endEvents[i], function () {
            if (nativeExitMenu.prevVisibleDiv) {
                nativeExitMenu.prevVisibleDiv.style.display = 'none';
            }
        });
    }

    if (!nativeExitMenu) {
        nativeExitMenu = new Object();
    }

    nativeExitMenu.isShowing = true;
    nativeExitMenu.menuWidth = 0;
    nativeExitMenu.isOpen = false;

    nativeExitMenu.exitMenuDiv = null;
    nativeExitMenu.exitMenuDivSansTitle = null;
    nativeExitMenu.currVisibleDiv = null;
    nativeExitMenu.prevVisibleDiv = null;

    nativeExitMenu.exitMenuDivToStart = null;
    if (exitMenuDivInitial) {
        nativeExitMenu.exitMenuDivToStart = exitMenuDivInitial;
    }

    nativeExitMenu.resetPreventionTimer = null;
    nativeExitMenu.preventFavAct = false;
    nativeExitMenu.favConfirmationDialogDisplayed = false;

    nativeExitMenu.longPress = false;
    nativeExitMenu.actionExecuted = false;
    nativeExitMenu.timer = null;
    nativeExitMenu.longPressThreshold = 2000;
    nativeExitMenu.selectedElement = null;

    nativeExitMenu.maxHeight = 535; //This height is considering only iPad container
    nativeExitMenu.maxHeightSansTitleDivAdjustment = -45; // Educated guess pixel tweak to make things just right.

    if (exitMenuItemIdPrefix) {
        //This prefix is passed down from server; Dropdown menus and exit menu both have same source of data and this prefix helps differentiating between visual elements in DOM from both
        nativeExitMenu.ITEM_ID_PREFIX = exitMenuItemIdPrefix;
    }
    else {
        nativeExitMenu.ITEM_ID_PREFIX = "exitMenu_";
    }
}

/*
* Creates a new div for given submenu
* Also adds two children nodes to it
* 1. titleDiv: Holds the submenu title
* 2. MenuID-sansTitleDiv: Holds the actual menu items in it
*/
function startExitMenu(MenuLevel, MenuID, MenuTitle) {

    //Creates a new div with MenuID as its ID
    nativeExitMenu.exitMenuDiv = document.createElement('div');
    nativeExitMenu.exitMenuDiv.setAttribute('id', MenuID);
    if (!nativeExitMenu.exitMenuDivToStart) {
        nativeExitMenu.exitMenuDivToStart = MenuID;
    }
    nativeExitMenu.exitMenuDiv.setAttribute('style', 'display:none;');
    nativeExitMenu.exitMenuDiv.setAttribute('class', 'exitMenuWithTitleDiv');

    var titleDiv = document.createElement('div');
    titleDiv.setAttribute('class', 'exitMenuTitleDiv');
    titleDiv.innerHTML = MenuTitle;
    nativeExitMenu.exitMenuDiv.appendChild(titleDiv);

    nativeExitMenu.exitMenuDivSansTitle = document.createElement('div');
    nativeExitMenu.exitMenuDivSansTitle.setAttribute('id', MenuID + '-sansTitle');
    nativeExitMenu.exitMenuDivSansTitle.setAttribute('class', 'exitMenuSansTitleDiv');
    addScrollEventListener(nativeExitMenu.exitMenuDivSansTitle);

    nativeExitMenu.exitMenuDivSansTitleTable = document.createElement('table');
    nativeExitMenu.exitMenuDivSansTitleTable.setAttribute('class', 'exitMenuDivSansTitleTable');
    nativeExitMenu.exitMenuDivSansTitle.appendChild(nativeExitMenu.exitMenuDivSansTitleTable);

    nativeExitMenu.exitMenuDiv.appendChild(nativeExitMenu.exitMenuDivSansTitle);

    if (MenuLevel == 0 && MenuID.indexOf("Form_Exit") >= 0) {
        var exitMenuTabForm = document.getElementById("exitMenuTab_Form");
        if (exitMenuTabForm) {
            exitMenuTabForm.setAttribute("onclick", "goTo('" + MenuID + "',this.className);");
        }
    }
    else if (MenuLevel == 0 && MenuID.indexOf("Row_Exit") >= 0) {
        var exitMenuTabRow = document.getElementById("exitMenuTab_Row");
        if (exitMenuTabRow) {
            exitMenuTabRow.setAttribute("onclick", "goTo('" + MenuID + "',this.className);");
        }
    }
}


/*
* This function builds up each individual menu item which is eventually added to SansTitleDiv.
* Menu items are divided into 3 major categories.
* a) Labels: This item is a simple text node with label text. e.g. Form, Row, Favorites etc.
* b) Separators: This item is currently just a horizontal rule element.
* c) Menu option: This node has two child nodes.
* 		1. img: This node holds the menu item icon and also the submenu indicator in case the item is a submenu again.
* 		2. Label: This node holds the menu item title.
* Note: This function reuses the logic part of jdeWebGUIAddMenuItem() function above in webgui.js
*/
function addExitMenuItem(MenuItemTitle, MenuItemAction, MenuItemType, MenuItemID, AccessibilityMode, MenuType, MenuItemChecked, Namespace, MenuItemIcon) {
    var isRTL = document.documentElement.dir == "rtl";
    var itemDiv = document.createElement('table');
    var itemDivTbody = document.createElement('tbody');
    var itemDivTrow = document.createElement('tr');
    itemDivTbody.appendChild(itemDivTrow);
    itemDiv.appendChild(itemDivTbody);
    var favIconSrc;
    var favHoverIconSrc;

    var iconDiv = document.createElement('div');
    itemDiv.setAttribute('id', 'exitMenuOuter' + MenuItemID);
    if ((MenuItemID.indexOf('FavoritesLabel') != -1 || MenuItemID.indexOf('FormRowLabels') != -1) && MenuItemType.indexOf("DISABLED") == -1) {
        itemDiv.setAttribute('class', 'exitMenuLabelItemDiv');
        var itemCol = document.createElement('td');
        var labelDiv = document.createElement('div');
        labelDiv.appendChild(document.createTextNode(MenuItemTitle));
        labelDiv.style.fontSize = "13px";
        itemCol.appendChild(labelDiv);
        itemDivTrow.appendChild(itemCol);
    }
    else if (MenuItemType.indexOf('SEPARATOR') != -1 && MenuItemType.indexOf("DISABLED") == -1) {
        /*No separators in this menu right now*/
        itemDiv.style.display = 'none';
    }
    else {
        itemDiv.setAttribute('class', 'exitMenuExitItemDiv RIClickable');
        itemDiv.setAttribute('type', 'button');

        var menuItemIcon = document.createElement('img');
        menuItemIcon.setAttribute('class', 'exitMenuExitItemIcon');
        menuItemIcon.setAttribute('src', MenuItemIcon);

        iconDiv.setAttribute('style', 'height:34px;');    //CHANGE: Removed:position
        iconDiv.appendChild(menuItemIcon);

        var textDiv = document.createElement('div');

        if (isRTL)
            textDiv.setAttribute('class', 'exitMenuExitItemText e1TextAlignRight');
        else
            textDiv.setAttribute('class', 'exitMenuExitItemText');
        textDiv.setAttribute('style', 'width:78%;word-wrap: break-word;');
        textDiv.innerHTML = MenuItemTitle;

        if (MenuItemType.indexOf("SUBMENU") != -1) {
            itemDiv.setAttribute('onclick', MenuItemAction);

            /*For non-touch devices*/
            /*itemDiv.setAttribute('onmousedown','highlightExitMenuItem(this);');
            itemDiv.setAttribute('onmouseup','clearExitMenuItemHighlight(this);');*/

            /*For touch enabled devices*/
            itemDiv.setAttribute('ontouchstart', 'highlightExitMenuItem(this);');
            itemDiv.setAttribute('ontouchend', 'clearExitMenuItemHighlight(this);');
        }
        else {
            if (MenuItemID.indexOf('FEFA') != -1) {
                addFormExitFavMenuStack(MenuItemID);
                favIconSrc = window["E1RES_img_FAV_deletefavorites_ena_png"];
                favHoverIconSrc = window["E1RES_img_FAV_deletefavorites_ovr_png"];
            }
            else if (MenuItemID.indexOf('REFA') != -1) {
                addRowExitFavMenuStack(MenuItemID);
                favIconSrc = window["E1RES_img_FAV_deletefavorites_ena_png"];
                favHoverIconSrc = window["E1RES_img_FAV_deletefavorites_ovr_png"];
            }
            else {
                favIconSrc = window["E1RES_img_FAV_addfavorites_ena_png"];
                favHoverIconSrc = window["E1RES_img_FAV_addfavorites_ovr_png"];
            }

            itemDiv.setAttribute('onclick', "if(!nativeExitMenu.longPress && !nativeExitMenu.actionExecuted){" + MenuItemAction + "}nativeExitMenu.longPress=false;nativeExitMenu.actionExecuted=false;");
            if (isTouchEnabled) {
                /*For touch enabled devices*/
                itemDiv.ontouchstart = nativeExitMenu.itemPressed;
                itemDiv.ontouchend = nativeExitMenu.itemReleased;
            }
            else // set a flag for no IPAD version, so that we can search it in the event handler
                itemDiv.isDeskTopVersion = true;
        }

        var itemCol1 = document.createElement('td');
        itemCol1.setAttribute('style', 'width:34px;');
        itemCol1.appendChild(iconDiv);
        itemDivTrow.appendChild(itemCol1);

        var itemCol2 = document.createElement('td');
        itemCol2.setAttribute('style', 'vertical-align:middle;width:70%');
        itemCol2.appendChild(textDiv);
        itemDivTrow.appendChild(itemCol2);

        // add/delete favorite icons for menu item
        if (!isTouchEnabled && favIconSrc) {
            var favIcon = document.createElement('img');


            JSSidePanelRender.setMotionImage(favIcon, favIconSrc, favHoverIconSrc);
            favIcon.className += ' exitMenuFavImg';
            favIcon.onclick = onClickFavEvent;
            var itemCol3 = document.createElement('td');
            itemCol3.setAttribute('style', 'vertical-align:middle;width:15%');
            webguiSetOpacity(favIcon, 0);
            itemCol3.appendChild(favIcon);

            itemDivTrow.appendChild(itemCol3);
            itemDivTrow.favIcon = favIcon;

            // attach hover event
            addMouseEvent(itemDivTrow, 'mouseover', onMouseOverMenuItemEvent);
            addMouseEvent(itemDivTrow, 'mouseout', onMouseOutMenuItemEvent);
        }
    }

    if (MenuItemType.indexOf("DISABLED") != -1) {
        itemDiv.style.display = 'none';
    }

    var itemRow = nativeExitMenu.exitMenuDivSansTitleTable.insertRow(-1);
    var itemCell = itemRow.insertCell(-1);
    itemCell.setAttribute('class', 'exitMenuDivSansTitleCell');
    itemCell.appendChild(itemDiv);
}

function onMouseOverMenuItemEvent(e) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "favIcon");
    webguiSetOpacity(elem.favIcon, 100);
}

function onMouseOutMenuItemEvent(e) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "favIcon");
    webguiSetOpacity(elem.favIcon, 0);
}

function onClickFavEvent(e) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "isDeskTopVersion");
    nativeExitMenu.selectedElement = elem;
    nativeExitMenu.longPressAction();
    return RIUTIL.stopEventBubbling(e);
}

function getFlyInMenuOffset() {
    return nativeExitMenu.menuWidth - (document.all && window.modalSSIndex > 0 ? 7 : 0);
}

/*
* This function takes care of adding the div element created in startExitMenu() to the poroper cell in exitMenuTable
*/
function endExitMenu(MenuLevel) {
    //Checks if there is a <td> at MenuLevel; if not creates one
    var exitMenuTable = document.getElementById('exitMenuTable');
    //For use during grid rendering
    if (nativeExitMenu.menuWidth == 0) {
        nativeExitMenu.menuWidth = exitMenuTable.parentNode.offsetWidth;
    }
    var colNum = exitMenuTable.rows[0].cells.length;
    if (colNum < MenuLevel + 1) {
        var tableColumn = document.createElement('td');
        tableColumn.className = 'exitMenuTableColumn';
        exitMenuTable.rows[0].appendChild(tableColumn);
    }
    exitMenuTable.rows[0].cells[MenuLevel].appendChild(nativeExitMenu.exitMenuDiv);
    //Insert back button in the div if required
    if (MenuLevel > 0) {
        var substrs = nativeExitMenu.exitMenuDiv.id.split("-");
        substrs.pop();
        var prevId = substrs.join("-");
        insertBackButton(nativeExitMenu.exitMenuDiv.id, prevId);
    }
}

function finishExitMenu() {
    var e1formDivTable = document.getElementById('nativeExitMenuTable');
    if (e1formDivTable) {
        if (document.documentElement.dir == "rtl") {
            e1formDivTable.style.left = -1 * getFlyInMenuOffset() + 'px';
        }
        else {
            e1formDivTable.style.right = -1 * getFlyInMenuOffset() + 'px';
        }
    }
    var exitMenuTabForm = document.getElementById("exitMenuTab_Form");
    if (exitMenuTabForm && exitMenuTabForm.getAttribute('onclick') == null) {
        exitMenuTabForm.style.display = 'none';
    }
    var exitMenuTabRow = document.getElementById("exitMenuTab_Row");
    if (exitMenuTabRow && exitMenuTabRow.getAttribute('onclick') == null) {
        exitMenuTabRow.style.display = 'none';
    }
    if (parseFloat(e1formDivTable.parentNode.style.opacity) <= 0) {
        setTimeout(setExitMenuTopValue, 500);
    }
}

function setExitMenuTopValue(topValue) {
    if (!topValue) {
        topValue = 0;
    }

    // prevent the user make menu stub out of the view port of E1 Form by drag and drop or scrolling the e1FormDiv
    var e1FormDiv = document.getElementById("e1formDiv");
    var exitMenuTabsHolder = document.getElementById('exitMenuTabsHolder');
    if (!exitMenuTabsHolder)
        return;

    topValue = Math.min(topValue, e1FormDiv.scrollTop + e1FormDiv.offsetHeight - exitMenuTabsHolder.offsetHeight - 20);
    topValue = Math.max(topValue, e1FormDiv.scrollTop);

    var menuDismissDiv = document.getElementById('nativeExitMenuDismissDiv');
    var e1formDivTableHolder = document.getElementById('nativeExitMenuTableHolder');
    var formTitleTable = document.getElementById('formTitleTable');
    var modelessTabTable = document.getElementById('modelessTabTable');
    var spTabStub = document.getElementById('SPTabStub');
    var spMenuBarTable = document.getElementById('SPMenuBarTable');

    if (menuDismissDiv) {
        menuDismissDiv.style.top = topValue;
        menuDismissDiv.style.height = "100%"; // make sure the dissmiss div has full cover of the screen especially important for CafeOne.
        menuDismissDiv.style.visibility = "hidden";
    }

    if (e1formDivTableHolder) {
        e1formDivTableHolder.style.top = topValue + "px";
        var e1FormDiv = document.getElementById(RIUTIL.ID_E1_FORM_DIV);
        if (e1FormDiv) {
            var hasHScrollbar = e1FormDiv.clientWidth < e1FormDiv.scrollWidth;
            menuHeight = Math.max(0, Math.min(nativeExitMenu.maxHeight, e1FormDiv.clientHeight - (hasHScrollbar ? 20 : 0))); // discount scrollbar height if H-scrollbar present in e1FormDiv
            e1formDivTableHolder.style.height = menuHeight + "px";
        }

        webguiSetOpacity(e1formDivTableHolder, 100); // this causes chrome to repaint

        var exitMenuTabsHolder = document.getElementById('exitMenuTabsHolder');

        if (exitMenuTabsHolder) {
            exitMenuTabsHolder.style.top = topValue;
            exitMenuTabsHolder.style.pointerEvents = 'all';
            exitMenuTabsHolder.parentNode.style.pointerEvents = 'all';
        }
    }
}

var lastY = 0;
function touchToMoveTabsStarted(e) {
    lastY = e.targetTouches[0].clientY;
    var tabHolder = document.getElementById('exitMenuTabsHolder');
}

// Despite the name, this function handles both touch and mouse movements.
function touchToMoveTabsMoved(e) {
    e.preventDefault();
    thisY = (isTouchEnabled ? e.targetTouches[0].clientY : e.clientY);
    var transY = thisY - lastY;
    lastY = thisY;
    var maxVal = nativeExitMenu.maxHeight;

    var e1FormDiv = document.getElementById(RIUTIL.ID_E1_FORM_DIV);
    if (e1FormDiv) {
        maxVal = Math.min(maxVal, e1FormDiv.clientHeight);
    }
    maxVal += getModelessTabPixelAdjustment();

    var tabHolder = document.getElementById('exitMenuTabsHolder');
    if (tabHolder) {
        var currTop = parseFloat(tabHolder.style.top);
        if (transY > 0) {
            tabHolder.style.top = Math.min(maxVal - tabHolder.clientHeight, currTop + transY) + "px";
        }
        else {
            tabHolder.style.top = Math.max(1, currTop + transY) + "px";
        }
    }
}

function touchToMoveTabsEnded(e) {
    var tabHolder = document.getElementById('exitMenuTabsHolder');
    getDtaInstance().doAsynPostById("exitMenuTabTopChanged", null, parseFloat(tabHolder.style.top));
}

// event handlers for drag an drop the fly in menu tab
function dragToMoveTabsStarted(e) {
    lastY = e.clientY;
    nativeExitMenu.isDragging = false;
    var position = RIUTIL.getMousePosition(e);
    var e1FormDiv = document.getElementById("e1formDiv");
    nativeExitMenu.mouseDownY = position.y;
    var e1formDivTableHolder = document.getElementById('nativeExitMenuTableHolder');
    nativeExitMenu.offsetY = e1formDivTableHolder.offsetTop - position.y;
    RIUTIL.addEvent(e1FormDiv, "mouseup", dragToMoveTabsEnded);
    RIUTIL.addEvent(e1FormDiv, "mousemove", dragToMoveTabsMoved);
    return RIUTIL.stopEventBubbling(e);
}

function dragToMoveTabsMoved(e) {
    var position = RIUTIL.getMousePosition(e);
    // when moving away from the start point beyond a certain threshold, we treat it as a drag and drop action.
    nativeExitMenu.isDragging = Math.abs(nativeExitMenu.mouseDownY - position.y) > 5;
    // change top position
    touchToMoveTabsMoved(e);
    return RIUTIL.stopEventBubbling(e);
}

function dragToMoveTabsEnded(e) {
    var position = RIUTIL.getMousePosition(e);
    var e1FormDiv = document.getElementById("e1formDiv");
    RIUTIL.removeEvent(e1FormDiv, "mouseup", dragToMoveTabsEnded);
    RIUTIL.removeEvent(e1FormDiv, "mousemove", dragToMoveTabsMoved);
    return RIUTIL.stopEventBubbling(e);
}

function moveExitMenuOutofView(divEle) {
    if (!nativeExitMenu.isOpen)  // prevents animation when first rendering form
        return;

    var e1formDivTable = document.getElementById('nativeExitMenuTable');
    var menuDismissDiv = document.getElementById('nativeExitMenuDismissDiv');

    if (document.documentElement.dir == "rtl") {
        ANIM.animate(e1formDivTable, 'sinSlideX', 0, (-1) * nativeExitMenu.menuWidth, 500);
    }
    else {
        ANIM.animate(e1formDivTable, 'sinSlideXright', 0, (-1) * nativeExitMenu.menuWidth, 500);
    }
    if (menuDismissDiv) {
        menuDismissDiv.style.pointerEvents = 'none';
        menuDismissDiv.style["background"] = 'none';
        menuDismissDiv.style.visibility = "hidden";
    }
    nativeExitMenu.isOpen = false;
    highlightSelectedTab();
}

function moveExitMenuIntoView(divEle) {
    var e1formDivTable = document.getElementById('nativeExitMenuTable');
    var menuDismissDiv = document.getElementById('nativeExitMenuDismissDiv');

    if (document.documentElement.dir == "rtl") {
        ANIM.animate(e1formDivTable, 'sinSlideX', (-1) * nativeExitMenu.menuWidth, 0, 500);
    }
    else {
        ANIM.animate(e1formDivTable, 'sinSlideXright', (-1) * nativeExitMenu.menuWidth, 0, 500);
    }

    if (menuDismissDiv) {
        menuDismissDiv.style.pointerEvents = 'all';
        menuDismissDiv.style["background"] = 'rgba(150,150,150,0.25)';
        menuDismissDiv.style.visibility = "visible";
        var e1FormDiv = document.getElementById(RIUTIL.ID_E1_FORM_DIV);
        if (!isIE8OrEarlier && !isIOS && !isAndroid)
            addMouseEvent(e1FormDiv, isFirefox ? "DOMMouseScroll" : "mousewheel", mouseWheelForE1FormDiv, false);
    }
    nativeExitMenu.isOpen = true;
}

// This event handler is attached to E1formdiv only when fly in menu is opened (for IE9, IE10, IE11, Firefox, Chrome only)
// The function implements the wheel scrolling behavior for menu div and prevent mousewheel event from bubbling up to E1FormDiv to cause malfunction of dismissing fly in menu.
function mouseWheelForE1FormDiv(e) {
    var elem = getChildWithMatchingId(nativeExitMenu.currVisibleDiv, 'sansTitle');
    elem.scrollTop = Math.min(Math.max(0, elem.scrollTop - getWheelDistance(e)), elem.scrollHeight - elem.clientHeight); window.status = elem.scrollTop;
    return RIUTIL.stopEventBubbling(e);
}

// crossing browser wheel delta distance in pixel  
function getWheelDistance(e) {
    var e = window.event || e;
    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))) * 30;
    return delta
};


/*
* This function shows the menuItem passed to it.
* Note: This comes into play when previously DISABLED(and hence not shown) item is enabled and needs to be displayed.
*/
function showExitMenuItem(menuItem) {
    menuItem.style.display = 'block';
    adjustAfterMenuItemStatusChanged(menuItem);
}

/*
* This function hides the menuItem passed to it.
*/
function hideExitMenuItem(menuItem) {
    menuItem.style.display = 'none';
    adjustAfterMenuItemStatusChanged(menuItem);
}

function highlightExitMenuItem(item) {
    if (item) {
        webguiSetOpacity(item, 70);
    }
}

function clearExitMenuItemHighlight(item) {
    if (item) {
        webguiSetOpacity(item, 100);
    }
}

/*
* Supplementary function for above two functions.
*/
function adjustAfterMenuItemStatusChanged(menuItem) {
    //menuItem -> itemInSansTitle -> SansTitle -> WithTitle
    var divWithTitle = menuItem.parentNode.parentNode;
    var dispChanged = false;
    if (divWithTitle.style.display == 'none') {
        //Temporarily for height calculations
        divWithTitle.style.display = 'block';
        dispChanged = true;
    }
    adjustHeightsToFit(divWithTitle);
    if (dispChanged) {
        divWithTitle.style.display = 'none';
    }
}

/*
* This function can be called if the already created exit menu needs to be hidden.
*/
function hideExitMenu() {
    var elem = document.getElementById('nativeExitMenuTable');
    elem.style.display = 'none';
    elem.style.width = '0px';
    nativeExitMenu.isShowing = false;
    nativeExitMenu.menuWidth = 0;
}

/*
* Called when a menu item is touched
*/
nativeExitMenu.itemPressed = function (e) {
    if (nativeExitMenu.selectedElement == null) {
        nativeExitMenu.selectedElement = e.currentTarget;
        highlightExitMenuItem(nativeExitMenu.selectedElement);
        nativeExitMenu.timer = setTimeout(nativeExitMenu.longPressAction, nativeExitMenu.longPressThreshold);
    }
}

/*
* Added to improve menu behavior by avoiding unintentional item clicks
*/
nativeExitMenu.itemTouchMoved = function (e) {
    if (nativeExitMenu.selectedElement && nativeExitMenu.selectedElement != e.currentTarget) {
        nativeExitMenu.preventFavAct = true;

        exitMenuItemReleased();

        if (nativeExitMenu.resetPreventionTimer)
            clearTimeout(nativeExitMenu.resetPreventionTimer);
        nativeExitMenu.resetPreventionTimer = setTimeout(allowFavoriteAction, 500);
    }
}

/*
* Called when menu item is touched long enough
*/
nativeExitMenu.longPressAction = function () {
    clearExitMenuItemHighlight(nativeExitMenu.selectedElement);
    if (!nativeExitMenu.preventFavAct) {
        nativeExitMenu.longPress = true;
        var selectedId = nativeExitMenu.selectedElement.id;
        var selectedObjectTitle = nativeExitMenu.selectedElement.children[0].children[0].children[1].children[0].innerHTML;
        var confirmVal = false;
        nativeExitMenu.favConfirmationDialogDisplayed = true;
        if ((selectedId.indexOf('exitMenuOuterREFA') != -1) || (selectedId.indexOf('exitMenuOuterFEFA') != -1)) {
            //selectedElement is an item in Favorites section which is to be removed
            confirmVal = confirm("\"" + selectedObjectTitle + "\": " + removeFavoriteConfirmMessage);
        }
        else {
            confirmVal = confirm("\"" + selectedObjectTitle + "\": " + addFavoriteConfirmMessage);
        }
        if (confirmVal == true) {
            updateFavoritesSection();
            nativeExitMenu.favConfirmationDialogDisplayed = false;
        }
        else {
            exitMenuItemReleased();
            nativeExitMenu.favConfirmationDialogDisplayed = false;
        }
    }
}

/*
* Called when menu item is released
*/
nativeExitMenu.itemReleased = function (e) {
    clearTimeout(nativeExitMenu.timer);
    clearExitMenuItemHighlight(nativeExitMenu.selectedElement);

    if (nativeExitMenu.selectedElement && e.currentTarget == nativeExitMenu.selectedElement && !nativeExitMenu.preventFavAct)//This check is to prevent actions happening when scrolling
    {
        nativeExitMenu.selectedElement.onclick.apply(nativeExitMenu.selectedElement);
        nativeExitMenu.actionExecuted = true;
    }
    if (!nativeExitMenu.favConfirmationDialogDisplayed) {
        nativeExitMenu.selectedElement = null;
    }
}

function exitMenuItemReleased(e) {
    clearTimeout(nativeExitMenu.timer);
    if (nativeExitMenu.selectedElement) {
        clearExitMenuItemHighlight(nativeExitMenu.selectedElement);
    }
    nativeExitMenu.selectedElement = null;
}

function highlightSelectedTab(selectedTab) {
    var exitMenuTabForm = document.getElementById("exitMenuTab_Form");
    var exitMenuTabRow = document.getElementById("exitMenuTab_Row");
    var rowVal = "false";
    var formVal = "false";
    if (selectedTab == "Row") {
        rowVal = "true";
    }
    else if (selectedTab == "Form") {
        formVal = "true";
    }
    exitMenuTabRow.setAttribute('selected', rowVal);
    exitMenuTabForm.setAttribute('selected', formVal);
}

/*
* This function does the job of transitioning from current visible submenu to submenu with targetDivID.
* The sourceClassName parameter is used to distinguish tab positioning events from click events on the tabs or
* other elements.  
*/
function goTo(targetDivID, sourceClassName) {
    if (nativeExitMenu.isDragging && sourceClassName && sourceClassName.indexOf("exitMenuTab") != -1) {
        return;
    }

    var targetDiv = document.getElementById(targetDivID);
    if (targetDiv) {
        var isTargetRowExitMenu = targetDivID.indexOf("Row_Exit");
        if (isTargetRowExitMenu != -1) {
            highlightSelectedTab("Row");
        }
        else {
            highlightSelectedTab("Form");
        }

        var exitMenuWasOpen = nativeExitMenu.isOpen;
        if (!nativeExitMenu.isOpen) {
            moveExitMenuIntoView();
            if (nativeExitMenu.currVisibleDiv && targetDivID == nativeExitMenu.currVisibleDiv.id) {
                adjustHeightsToFit(targetDiv);
                return;
            }
        }
        else if (nativeExitMenu.isOpen && nativeExitMenu.currVisibleDiv && targetDivID == nativeExitMenu.currVisibleDiv.id) {
            moveExitMenuOutofView();
            return;
        }

        var parentCell = targetDiv.parentNode; //parentCell is a column in the table
        var targetTdIndex = parentCell.cellIndex;
        nativeExitMenu.prevVisibleDiv = nativeExitMenu.currVisibleDiv;
        var prevTdIndex = 0;
        if (nativeExitMenu.prevVisibleDiv)
            prevTdIndex = nativeExitMenu.prevVisibleDiv.parentNode.cellIndex;

        //Show target submenu div
        targetDiv.style.display = 'block';
        nativeExitMenu.currVisibleDiv = targetDiv;

        var exitMenuTable = document.getElementById("exitMenuTable");
        var targetDivSansTitle = getChildWithMatchingId(targetDiv, 'sansTitle');
        if (targetDivSansTitle) {
            targetDivSansTitle.children[0].style.top = '0px';
        }
        adjustHeightsToFit(targetDiv);
        var tdWidth = exitMenuTable.children[0].children[0].children[0].offsetWidth + 1; //Don't know a better way
        if (document.documentElement.dir == "rtl") {
            var currLeftVal = tdWidth * prevTdIndex;
            var targetLeftVal = tdWidth * targetTdIndex;
        }
        else {
            var currLeftVal = (-1) * tdWidth * prevTdIndex;
            var targetLeftVal = (-1) * tdWidth * targetTdIndex;
        }

        if ((exitMenuWasOpen) &&  // slide into place if previously open, and the current and previous are part of same table, otherwise just display
             (nativeExitMenu.prevVisibleDiv.id.indexOf("Row_Exit") == nativeExitMenu.currVisibleDiv.id.indexOf("Row_Exit")))
            ANIM.animate(exitMenuTable, 'sinSlideX', currLeftVal, targetLeftVal, 500);
        else
            exitMenuTable.style.left = '0px';

        if (nativeExitMenu.prevVisibleDiv)
            nativeExitMenu.prevVisibleDiv.style.display = 'none';
    }
}

/*
* This function fits the current visible div in according available height resizes the div containing exitMenuTable.
* It also considers adding scroll buttons if the div height exceeds available height.
*/
function adjustHeightsToFit(targetDiv) {
    if (targetDiv) {
        var exitMenuTable = document.getElementById("exitMenuTable");
        var exitMenuTableDiv = exitMenuTable.parentNode;
        var e1formDivTable = document.getElementById("e1formDivTable");
        var targetDivSansTitle = getChildWithMatchingId(targetDiv, 'sansTitle');
        var maxHeight = nativeExitMenu.maxHeight;

        // when inside of CafeOne, the height of fly in menu shall be less than the height of E1FormDiv
        var e1FormDiv = document.getElementById(RIUTIL.ID_E1_FORM_DIV);
        if (e1FormDiv) {
            maxHeight = Math.min(maxHeight, e1FormDiv.clientHeight);
        }

        maxHeight += getModelessTabPixelAdjustment();
        exitMenuTableDiv.style.height = maxHeight + "px";
        targetDivSansTitle.style.height = maxHeight + nativeExitMenu.maxHeightSansTitleDivAdjustment + "px";
    }
}


function getModelessTabPixelAdjustment() {
    if (document.getElementById("modelessTabDiv")) {
        return -25;
    }

    return 0;
}


function allowFavoriteAction() {
    nativeExitMenu.preventFavAct = false;
}

/*
* The event listener keeps the scroll button in sync with current state of the div by enabling/disabling them as required.
*/
function addScrollEventListener(targetElement) {
    targetElement.addEventListener("scroll", function () {
        /*This timer is a solution for stopping the favorites message from popping up if user continues to scroll for long time pressed on the same menu item*/
        nativeExitMenu.preventFavAct = true;

        exitMenuItemReleased();

        if (nativeExitMenu.resetPreventionTimer)
            clearTimeout(nativeExitMenu.resetPreventionTimer);
        nativeExitMenu.resetPreventionTimer = setTimeout(allowFavoriteAction, 500);
    });
}

/*
* This function inserts a Back navigation button in targetDiv as its first child(absolutely positioned).
* It also assigns onclick action to transition to prevDiv.
*/
function insertBackButton(targetDivId, prevDivId) {
    var targetDiv = null;
    if (targetDivId) {
        targetDiv = document.getElementById(targetDivId);
    }
    if (prevDivId == null) {
        return;
    }
    if (targetDiv && targetDiv.children[0].getAttribute('id') != 'goBack') {
        var goBackDiv = document.createElement('div');
        goBackDiv.setAttribute('id', 'goBack');

        //Temoporarily set display:block for target Div
        targetDiv.style.display = 'block';
        var maxDivHeight = nativeExitMenu.maxHeight;
        goBackDivTopPos = 0;
        targetDiv.style.display = 'none'; //Reset display
        goBackDiv.setAttribute('style', 'top:' + goBackDivTopPos);
        if (document.documentElement.dir == "rtl") {
            goBackDiv.setAttribute('class', 'exitMenuNavigationItem exitMenuNavigationBackButton_rtl RIClickable');
        }
        else {
            goBackDiv.setAttribute('class', 'exitMenuNavigationItem exitMenuNavigationBackButton RIClickable');
        }

        goBackDiv.setAttribute('onclick', "goTo('" + prevDivId + "',this.className);");
        goBackDiv.setAttribute('type', 'button');
        var backButtonIcon = document.createElement('img');
        if (document.documentElement.dir == "rtl") {
            backButtonIcon.setAttribute('src', window["E1RES_share_images_backbutton_rtl_png"]);
        }
        else {
            backButtonIcon.setAttribute('src', window["E1RES_share_images_backbutton_png"]);
        }
        goBackDiv.appendChild(backButtonIcon);
        targetDiv.insertBefore(goBackDiv, targetDiv.children[1]);
    }
}

/*
* This function is a utility function.
* It returns the first child of inputDiv which has partId as a substring of its id attribute value.
*/
function getChildWithMatchingId(inputDiv, partId) {
    var childWithPartId = null;
    if (inputDiv && partId) {
        for (i = 0; i < inputDiv.children.length; i++) {
            var nChild = inputDiv.children[i];
            var childId = nChild.getAttribute('id');
            if (childId && childId.indexOf(partId) != -1) {
                childWithPartId = nChild;
                break;
            }
        }
    }
    return childWithPartId;
}

/*
* This function is reusing releaseElement() function's code of dragDrop oject
* This is a stripped down version of that function.
* For complete implementation refer to original function.
* Hierarchy of DOM elements: baseRowExitDiv(withTitleDiv) --> sansTitleDiv --> selectedObject
*/
function updateFavoritesSection() {
    var selectedObject = nativeExitMenu.selectedElement;
    var selectedObjectTitle = selectedObject.children[0].children[0].children[1].children[0].innerHTML;
    nativeExitMenu.selectedElement = null;
    var selectedObjMenuID;
    var isRTL = document.documentElement.dir == "rtl";
    var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);

    //FF Check
    if (!document.getElementsByClassName) // firefox version < 3
    {
        selectedObjMenuID = selectedObject.parentNode.parentNode.id.split("-")[0].slice(nativeExitMenu.ITEM_ID_PREFIX.length);
    }
    else {
        if (selectedObjMenuID == null || selectedObjMenuID.indexOf("Form_Exit") == -1 || selectedObjMenuID.indexOf("Row_Exit") == -1) {
            selectedObjMenuID = selectedObject.parentNode.parentNode.parentNode.parentNode.parentNode.id.split("-")[0].slice(nativeExitMenu.ITEM_ID_PREFIX.length); /*Splitting "exitMenu_ACTUAL_ID-sansTitle" and stripping 'exitMenu_' part*/
        }
    }


    var menuType = getMenuType(selectedObjMenuID);

    if (menuType == "FORM_EXIT_TYPE") {
        var selectedObjectClone = selectedObject.cloneNode(true);
        // Favorites menuItem ID starts with outerFEFA
        if (selectedObjectClone.id.indexOf("exitMenuOuterFEFA") == 0) {
            removeFormExitFavMenuStack(selectedObjectClone.id.substring(5));
            getDtaInstance().doAsynPostById("formExitFavRemove", null, selectedObjectClone.id.substring(selectedObjectClone.id.lastIndexOf("_") + 1));

            // remove dragObjectClone
            selectedObjectClone = null;
        }
        else {
            var origDragCloneID = selectedObjectClone.id;

            // ID comes with prefix outerHE- Replace the HE with FA[Favorites]
            // to maintain unique ID's for the FormExit/RowExit Fav MenuItems
            if (window.namespace)
                selectedObjectClone.setAttribute('id', selectedObjectClone.id.replace("HE" + namespace, "FEFA" + namespace));
            else
                selectedObjectClone.setAttribute('id', selectedObjectClone.id.replace("HE", "FEFA"));

            //if it's a New Formexit favorite
            //exit menu items have 'exitMenu' additionally to differentiate them from toolbar items, we will need to strip it off to work with same functions as toolbar's; e.g. ID=exitMenuOuterFEFA-12
            if (isNewFormExitFavorite(selectedObjectClone.id.substring(8))) {
                addFormExitFavMenuStack(selectedObjectClone.id.substring(5));
                getDtaInstance().doAsynPostById("formExitFavAdd", null, selectedObjectClone.id.substring(selectedObjectClone.id.lastIndexOf("_") + 1));
            }
            else {
                alert("\"" + selectedObjectTitle + "\"" + alreadyInFavoritesMessage);
                selectedObjectClone = null;
            }
        }
    }
    else if (menuType == "ROW_EXIT_TYPE") {
        var selectedObjectClone = selectedObject.cloneNode(true);

        //Favorite Row Exit menuitem ID comes with the prefix -outerREFA
        if (selectedObjectClone.id.indexOf("exitMenuOuterREFA") == 0) {
            //remove the Fav menuItem from the rowexitfavmenu Array
            removeRowExitFavMenuStack(selectedObjectClone.id.substring(5));
            getDtaInstance().doAsynPostById("rowExitFavRemove", null, selectedObjectClone.id.substring(selectedObjectClone.id.lastIndexOf("_") + 1));
            selectedObjectClone = null;
        }
        else {
            var origDragCloneID = selectedObjectClone.id;
            if (window.namespace)
                selectedObjectClone.setAttribute('id', selectedObjectClone.id.replace("HE" + namespace, "REFA" + namespace));
            else
                selectedObjectClone.setAttribute('id', selectedObjectClone.id.replace("HE", "REFA"));

            //check if it's a New Rowexit favorite
            //exit menu items have "exitMenu" additionally at the beginnning to differentiate them from toolbar items, we will need to strip it off to work with same functions as toolbar's; e.g. ID=exitMenuOuterFEFA-12 will become OuterFEFA-12
            if (isNewRowExitFavorite(selectedObjectClone.id.substring(8))) {
                addRowExitFavMenuStack(selectedObjectClone.id.substring(5));
                getDtaInstance().doAsynPostById("rowExitFavAdd", null, selectedObjectClone.id.substring(selectedObjectClone.id.lastIndexOf("_") + 1));
            }
            else {
                alert("\"" + selectedObjectTitle + "\"" + alreadyInFavoritesMessage);
                // remove dragObjectClone
                selectedObjectClone = null;
            }
        }
    }
}


/**
* This is the customized E1 tooltip component.<br>
* User can set the follwing parameters: background color,
* show delay time, hide delay time, show delay, hide delay,
* vertical offset, horizontal offset.<br>
*/
function Tooltip(id) {
    this.id = id;
    window[id] = this;

    this.showDelayTime = 800;
    this.hideDelayTime = 0;

    this.xOffset = 9;
    this.yOffset = 12;

    this.showDelay = null;
    this.hideDelay = null;

    this.tooltipElem = document.getElementById(this.id);
    if (this.tooltipElem == null) {
        this.tooltipElem = this._createTooltip(this.id);
    }
}

Tooltip.prototype.NEW_LINE = "<br>";

Tooltip.prototype._createTooltip = function (id) {
    var elem = document.createElement("div");
    elem.id = id;
    elem.className = 'JSGridTooltip';

    document.body.appendChild(elem);
    return elem;
}

Tooltip.prototype.setTooltip = function (tooltipContent, obj, e) {
    if (e.stopPropagation) {
        e.stopPropagation();
    }
    else if (window.event) {
        event.cancelBubble = true;
    }

    this.tooltipElem.innerHTML = tooltipContent;
    this.tooltipElem.x = getAbsoluteLeftPos(obj, true);
    this.tooltipElem.y = getAbsoluteTopPos(obj, true);
    this.tooltipElem.style.left =
    this.tooltipElem.x + this.xOffset + "px";
    this.tooltipElem.style.top =
    this.tooltipElem.y + obj.offsetHeight + this.yOffset + "px";
}

Tooltip.prototype.showTooltip = function () {
    if (this.tooltipElem != null) {
        this.tooltipElem.style.visibility = "visible";
    }
}

Tooltip.prototype.hideTooltip = function () {
    if (this.tooltipElem != null) {
        this.tooltipElem.style.visibility = "hidden";
    }
}

Tooltip.prototype.clearHideTooltipDelay = function () {
    if (this.hideDelay != null) {
        clearTimeout(this.hideDelay);
    }
}

Tooltip.prototype.clearShowTooltipDelay = function () {
    if (this.showDelay != null) {
        clearTimeout(this.showDelay);
    }
}

Tooltip.prototype.setShowTooltipDelay = function (showDelay) {
    this.showDelay = showDelay;
}

Tooltip.prototype.setHideTooltipDelay = function (hideDelay) {
    this.hideDelay = hideDelay;
}

Tooltip.prototype.setShowDelayTime = function (showDelayTime) {
    this.showDelayTime = showDelayTime;
}

Tooltip.prototype.setHideDelayTime = function (hideDelayTime) {
    this.hideDelayTime = hideDelayTime;
}

Tooltip.prototype.getShowDelayTime = function () {
    return this.showDelayTime;
}

Tooltip.prototype.getHideDelayTime = function () {
    return this.hideDelayTime;
}

Tooltip.prototype.setXOffset = function (xOffset) {
    this.xOffset = xOffset;
}

Tooltip.prototype.setYOffset = function (yOffset) {
    this.yOffset = yOffset;
}

Tooltip.prototype.setBGColor = function (bgColor) {
    if (this.tooltipElem != null) {
        this.tooltipElem.style.backgroundColor = bgColor;
    }
}

function getCursorPosition(evt) {
    if (!evt) {
        evt = window.event;
    }
    if (evt) {
        //Take into account the scroll positions on the 'e1formDiv' also (after HFE - lock toolbar project)
        var formDiv = document.getElementById('e1formDiv');
        var formDivScrollTop = 0;
        var formDivScrollLeft = 0;
        if (formDiv) {
            formDivScrollTop = formDiv.scrollTop;
            formDivScrollLeft = formDiv.scrollLeft;
        }

        if (evt.pageX === undefined) {
            // IE8
            var dE = document.documentElement || {};
            var dB = document.body || {};
            if ((evt.clientX || evt.clientX == 0) && ((dB.scrollLeft || dB.scrollLeft == 0) || (dE.clientLeft || dE.clientLeft == 0))) {
                return [evt.clientX + (dE.scrollLeft || dB.scrollLeft || 0) + formDivScrollLeft - (dE.clientLeft || 0), evt.clientY + (dE.scrollTop || dB.scrollTop || 0) + formDivScrollTop - (dE.clientTop || 0)];
            }

            return null;
        }

        return [evt.pageX + formDivScrollLeft, evt.pageY + formDivScrollTop];
    }
    return null;
}

function framebusting(option) {
    switch (option) {
        case 'always': // take over top window unconditionally
            if (parent != self) {
                parent.location = self.location;
            }
            break;

        case 'never': // do nothing about it
            break;

        case 'differentDomain':
        default:
            try {
                var differentDomain = (parent != self && parent.document.domain != self.document.domain);
                if (differentDomain)
                    parent.location = self.location;
            }
            catch (e) {
                parent.location = self.location;
            }
    }
}

function getNativeContainerParent() {
    /*
    * Traverse up the window hierarchy searching for the NativeContainer object/library.
    */
    var parentWindow = null;

    if (window) {
        parentWindow = window.parent;
    }

    while (parentWindow && !parentWindow.NativeContainer) {
        if (parentWindow != parentWindow.parent) {
            parentWindow = parentWindow.parent;
        }
        else {
            // No NativeContainer found.
            parentWindow = null;
            break;
        }
    }

    return parentWindow;
}

if (!window.ORIENTATION) {
    ORIENTATION = new Object();
    ORIENTATION.LANDSCAPE = 0;
    ORIENTATION.PORTRAIT = 1;
}
/* 
* This function returns the device orientation for iOS. A Util function but for lack of better place adding it here. 
* 0: Portrait; 180: Inverted Portrait; -90: Landscape(Clockwise) 90:  Landscape(Counterclockwise)
*/
ORIENTATION.getDeviceOrientation = function () {
    var oriAbsVal = Math.abs(window.top.orientation);
    switch (oriAbsVal) {
        case 0:
            return ORIENTATION.PORTRAIT;
        case 90:
            return ORIENTATION.LANDSCAPE;
        case 180:
            return ORIENTATION.PORTRAIT;
    }
}

function setTransform(elem, transform) {
    elem.style.WebkitTransform = elem.style.MozTransform = elem.style.OTransform = elem.style.MsTransform = elem.style.transform = transform;
}

// Public utility method to recursively delete child nodes from parent
function removeAllChildren(parentElem) {
    if (parentElem && parentElem.childNodes) {
        while (parentElem.childNodes.length > 0) {
            var child = parentElem.firstChild;
            parentElem.removeChild(child);
        }
    }
}

// Accessibility functions to be initialized only when acccesibility flag is ON.
var setAccAttr = function (elem, AttrName, AttrValue) {
    if (userAccessibility) {
        setAccAttr = function (elem, AttrName, AttrValue) { elem.setAttribute(AttrName, AttrValue); };
    }
    else {
        setAccAttr = function (elem, AttrName, AttrValue) { };
    }

    return setAccAttr(elem, AttrName, AttrValue);
}

var setAccHide = function (elem) {
    if (userAccessibility) {
        setAccHide = function (elem) { this.setAccAttr(elem, "role", "presentation"); };
    }
    else {
        setAccHide = function (elem) { };
    }

    return setAccHide(elem);
}

// create a mask frame on demond for elem and attach the reference as maskFrame inside of the elem. The method will try to reuse the inside reference if avaiable instead of creat a new one
function createMaskFrameForElement(elem) {
    if (isIE && isActiveXAvailable())  // for IE activeX enabled only
    {
        // reuse the one already referenced by the owner
        if (elem.maskFrame) {
            elem.maskFrame.showMask(elem);
        }
        else // create a new mask frame and keep the reference inside of the owner
        {
            var itemMask = new MaskFrame(elem.id + '-maskframe');
            itemMask.attachMask(elem);
            elem.maskFrame = itemMask;
        }
    }
}

// remove the internal mask frame for clean up
function removeMaskForElement(elem) {
    if (isIE && isActiveXAvailable())  // for IE activeX enabled only
    {
        if (elem && elem.maskFrame) {
            elem.maskFrame.detachMask();
            elem.maskFrame = null;
        }
    }
}

// this removes html markup from a string
// primarily used for menu items with <u> and </u> tags
function decodeEntities(str) {
    // this prevents any overhead from creating the object each time
    var element = document.createElement('div');
    if (str && typeof str === 'string') {
        // strip script/html tags
        str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
        str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
        element.innerHTML = str;
        str = element.textContent;
        element.textContent = '';
    }

    return str;
}

function ariaLog(str) {
    var ariaLog = top.document.getElementById('ariaLog');  // this should work in most cases
    if (!ariaLog) {
        // for portal, top.document won't work, so we check document, then parent.document
        ariaLog = document.getElementById('ariaLog');
        if (!ariaLog) {
            ariaLog = parent.document.getElementById('ariaLog');
        }
    }
    if (ariaLog) {
        ariaLog.innerHTML = str;
    }
}

function ariaAlert(str) {
    var ariaAlert = top.document.getElementById('ariaAlert');  // this should work in most cases
    if (!ariaAlert) {
        // for portal, top.document won't work, so we check document, then parent.document
        ariaAlert = document.getElementById('ariaAlert');
        if (!ariaAlert) {
            ariaAlert = parent.document.getElementById('ariaAlert');
        }
    }
    if (ariaAlert) {
        ariaAlert.innerHTML = str;
    }
}

function ariaAlertForIYFE(BGColor) {
    switch (BGColor) {
        case "#ffff00":
        case "#ffffdd":
            ariaAlert(jdeWebGUIStaticStrings.ACCESS_WARNING_ON_PAGE + " " + jdeWebGUIStaticStrings.ACCESS_ERROR_INSTRUCTION);
            break;
        case "#ff0000":
        case "#ff7c7c":
            ariaAlert(jdeWebGUIStaticStrings.ACCESS_ERROR_ON_PAGE + " " + jdeWebGUIStaticStrings.ACCESS_ERROR_INSTRUCTION);
            break;
    }
}


//code for autoscroll begins 
function favDivScroll(scrollDivObj, iScrollFactor) {
    var divObj = document.getElementById(scrollDivObj);
    divObj.scrollTop += iScrollFactor;
};

function clearIntervalScroll() {
    clearInterval(favIntervalScroll);
}

function cleanUpInlineHeightWidthForIFrame() {
    var e1frame = top.document.getElementById('e1menuAppIframe');
    if (e1frame) {
        e1frame.style.removeProperty('width');
        e1frame.style.removeProperty('height');
    }

    var extframe = top.document.getElementById('e1ExternalAppIframe');
    if (extframe) {
        extframe.style.removeProperty('width');
        extframe.style.removeProperty('height');
    }

    var sbFrame = top.document.getElementById('sbFrame'); //if sbFrame doesn't exist, this function is not called, so no need to check 
    if (sbFrame == null) {
        sbFrame = document.getElementById('sbFrame');
    }
    sbFrame.parentNode.setAttribute("sbState", "collapse");
    sbFrame.parentNode.removeChild(sbFrame);
    var sbToggle = top.document.getElementById('sbToggle');
    if (sbToggle == null) {
        sbToggle = document.getElementById('sbToggle');
    }
    sbToggle.style.display = 'none';
}


//code for autoscroll ends