if (!CARO) {
    var CARO = new Object();
}

if (!ANIM) {
    var ANIM = new Object();
    ANIM.Animations = new Object();
    ANIM.TimingFunctions = new Object();
    ANIM.isAnimating = false;
}

var ICONSERVICE;
if (!ICONSERVICE) {
    ICONSERVICE = new Object();
    ICONSERVICE.iconVersion = 9.2;
}

var LOGGER;
if (!LOGGER) {
    LOGGER = new Object();
}

CARO.USE_LARGE_ICONS = 0;

CARO.NORTH = 0;
CARO.EAST = 1;
CARO.SOUTH = 2;
CARO.WEST = 3;

CARO.CONSTANTS = new Object();
CARO.CONSTANTS.EXPAND_COLLAPSE_CAROUSEL_DURATION = 400;
// CARO.CONSTANTS.EXPAND_COLLAPSE_CAROUSEL_DURATION = 4000;
CARO.CONSTANTS.EXPAND_COLLAPSE_CAROUSEL_DURATION_LIST = 400;
CARO.CONSTANTS.TILE_FADE_DURATION = 500;
CARO.CONSTANTS.TAB_FADE_DURATION = 500;
CARO.CONSTANTS.SWAP_CARO_FADE_DURATION = 500;
CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TILES = 500;
CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_SECTION = CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TILES;
CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST = 350;
CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_SPEEDY = 150;
CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TABS = 500;
CARO.CONSTANTS.GLOW_LIST_INTIME = 200;
CARO.CONSTANTS.GLOW_LIST_OUTTIME = 500;
CARO.CONSTANTS.TILE_SIZE = (CARO.USE_LARGE_ICONS) ? 80 : 40;
CARO.CONSTANTS.TILE_SPACING = (CARO.USE_LARGE_ICONS) ? 4 : 24;
CARO.CONSTANTS.CAPTION_SPACE_MAX_PX = (CARO.USE_LARGE_ICONS) ? 78 : 58;
CARO.CONSTANTS.TILE_IMAGE_SIZE = (CARO.USE_LARGE_ICONS) ? 68 : 38;
CARO.CONSTANTS.OFFSET_PER_TILE = CARO.CONSTANTS.TILE_SIZE + CARO.CONSTANTS.TILE_SPACING;
CARO.CONSTANTS.EXTRA_WIDTH_FOR_SHADOW = 4;
CARO.CONSTANTS.MENU_TOTAL_HEIGHT = 65;
CARO.CONSTANTS.LIST_HEIGHT_LOST_TO_MARGIN = 6;
CARO.CONSTANTS.LIST_HEADER_HEIGHT = 25;
CARO.CONSTANTS.LIST_ITEM_HEIGHT = 32;
CARO.CONSTANTS.TIME_INC_FOR_MOUSE_VELOCITY = 64;
CARO.CONSTANTS.TAB_INITIAL_INDENT = 1;
CARO.CONSTANTS.TAB_SPACING = 0;
CARO.CONSTANTS.BACK_CTRL_WIDTH = 20;
CARO.CONSTANTS.TAB_LEFT_WIDTH = 12;
CARO.CONSTANTS.TAB_RIGHT_WIDTH = 12;
CARO.CONSTANTS.TAB_X_WIDTH = 12;
CARO.CONSTANTS.TAB_FOLDER_OVERHEAD = CARO.CONSTANTS.TAB_LEFT_WIDTH + CARO.CONSTANTS.TAB_RIGHT_WIDTH + CARO.CONSTANTS.TAB_X_WIDTH + CARO.CONSTANTS.TAB_SPACING;
CARO.CONSTANTS.TAB_PADDING_SUFFIX_STRING = '';
CARO.CONSTANTS.LIST_SECTION_TEXT_MAX_LENGTH = 138;
CARO.CONSTANTS.MIN_TAB_WIDTH = CARO.CONSTANTS.TAB_FOLDER_OVERHEAD + 17;
CARO.CONSTANTS.MAX_TAB_WIDTH = 215;
CARO.CONSTANTS.MIN_TIME_TO_MOVE_FOR_FULL_CARO_SLIDE = 575;
CARO.CONSTANTS.THRESHHOLD_PX_SIGNIFYING_INTENDED_MOTION = 3;
CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION = 500;
CARO.CONSTANTS.TAB_HIDER_ALLOWANCE = 34;
CARO.CONSTANTS.TAB_EXPAND_COLLAPSE_DURATION = 425;
CARO.CONSTANTS.DETAIL_PANE_DIMMS = { SMALL: { W: 200, H: 102 },
    LARGE: { W: 230, H: 130}
};
CARO.CONSTANTS.DETAIL_PANE_VERT_MARGIN = 6;
CARO.CONSTANTS.DETAIL_PANE_VERT_BORDER = 1;
CARO.CONSTANTS.DETAIL_PANE_VERT_EXTRA = 2 * (CARO.CONSTANTS.DETAIL_PANE_VERT_MARGIN + CARO.CONSTANTS.DETAIL_PANE_VERT_BORDER);
CARO.CONSTANTS.DETAIL_PANE_MAX_OPACITY = 0.93;
CARO.CONSTANTS.RCUX_BOTTOM = 5;
CARO.CONSTANTS.CARO_HEIGHT = (CARO.USE_LARGE_ICONS) ? 81 : 56;
CARO.CONSTANTS.CARO_WIDTH = 200;
CARO.CONSTANTS.HOVER_DELAY_TO_SHOW_DETAILS = 1000;
CARO.CONSTANTS.DETAIL_FADE_IN_OUT_DURATION = 250;
CARO.CONSTANTS.DETAIL_FADE_OUT_DELAY = 400;
CARO.CONSTANTS.DETAIL_ENCROACHMENT = 6;
CARO.CONSTANTS.UISTATE_CAROUSEL = "carousel";
CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT = (CARO.USE_LARGE_ICONS) ? 14 : 12;
CARO.CONSTANTS.ACCIDENTAL_DOUBLE_CLICK_THRESHHOLD = 400;

CARO.CONSTANTS.STATE_VARS = new Object();
CARO.CONSTANTS.STATE_VARS.SIDE = "side";
CARO.CONSTANTS.STATE_VARS.EXPANDED = "expanded";
CARO.CONSTANTS.STATE_VARS.ACTIVE_TAB = "activeTab";
CARO.CONSTANTS.STATE_VARS.TABS_SHOWN = "tabsShown";
CARO.CONSTANTS.STATE_VARS.LIST_EXPANDED_OCL = "listExpandedOCL";
CARO.CONSTANTS.STATE_VARS.LIST_EXPANDED_RR = "listExpandedRR";
CARO.CONSTANTS.STATE_VARS.LIST_EXPANDED_FAV = "listExpandedFAV";
CARO.CONSTANTS.STATE_VARS.WHICH_CAPTION = "whichCaption";
CARO.CONSTANTS.STATE_VARS.EXPANDED_USER_FOLDERS = "expandedUserFolders";
CARO.CONSTANTS.STATE_VARS.OPEN_USER_FOLDERS = "openUserFolders";
// Delimited list of favorited task folder to be reopened on startup.
CARO.CONSTANTS.STATE_VARS.OPEN_FAV_FOLDER_IDS = "openFavFolderIds";
// Ordered, delimeted list of the carousel sections (both tab and list).
CARO.CONSTANTS.STATE_VARS.SECTION_ORDER = "caroSectionOrder";
// JSON object arrays containing the path of task IDs from each open favorite folder
// opened on startup back to the user's favorite folder.
CARO.CONSTANTS.STATE_VARS.OPEN_SECTIONS_BACK_PATHS = "openSectionsBackPaths";
// Setting for the default tabs that will be displayed before any user or task folders
CARO.CONSTANTS.STATE_VARS.DEFAULT_TABS = ['OCL', 'RecRpts', 'Fav'];
// Setting for the hidden default tabs
CARO.CONSTANTS.STATE_VARS.HIDDEN_DEFAULT_SECTIONS = "hiddenDefaultSections";

CARO.CONSTANTS.UNKNOWN_VERSION_STRING = "default";

CARO.TABS = new Object();
CARO.TABS.SHOWN = new Array();
CARO.TABS.HIDDEN = new Array();

CARO.TILE_TYPES = new Object();
CARO.TILE_TYPES.OCL = 0;
CARO.TILE_TYPES.FAV = 1;
CARO.TILE_TYPES.RRPT = 2;

CARO.TILE_TYPES.TYPE_TO_ID_PREFIX = ['caroOCL_', 'caroFav_', 'caroRRpt_'];
CARO.TILE_TYPES.TYPE_TO_ID_LPREFIX = ['listOCL_', 'listFav_', 'listRRpt_'];
CARO.TILE_TYPES.TYPE_TO_SUFFIX = ['ocl_', 'fav_', 'rrpt_'];

CARO.SIDE_TO_CLASSNAME = ['caroTop', 'caroRight', 'caroBottom', 'caroLeft'];
CARO.SIDE_TO_SETTING_VALUE = ['north', 'east', 'south', 'west'];

CARO.CONSTANTS.COLLAPSED_POS_BY_SIDE = (CARO.USE_LARGE_ICONS) ? [-84, -188, -84, -188] : [-56, -188, -56, -188];
CARO.CONSTANTS.EXPANDED_POS_BY_SIDE = [0, 7, 0, 5];
CARO.CONSTANTS.CARO_COLLAPSED_CONTAINER_BOTTOM = 11;

//Constants for organize favorites in carousel
CARO.CONSTANTS.TIME_BEFORE_TAB_ACTIVATION = 750;

var isNativeContainer = (navigator.userAgent.toUpperCase().indexOf("JDECONTAINER") > -1);

var JDE_ICON_PREFIX; // Defined in dropdownViewmax.jsp
var JDE_ICON_SUFFIX = { SMALL: '_small.', MEDIUM: '_medium.', LARGE: '_large.' };

var CARO_STRING_CONSTS; // from CarouselTag.jsp
if (!CARO_STRING_CONSTS) {
    CARO_STRING_CONSTS = new Object();
}

CARO.suspendListResizing = true;

CARO.debug = function (str) {
    if (CARO.caroDebugPane) {
        CARO.caroDebugPane.innerHTML += '<tt>' + str + '</tt><br/>';
        CARO.caroDebugPane.scrollTop = CARO.caroDebugPane.scrollHeight;
    }
}

CARO.log = function (message) {
    if (LOGGER && LOGGER.log) {
        LOGGER.log('CARO: ' + message);
    }
}

CARO.caroInit = function () {
    if (CARO.alreadyInit) {
        return;
    }
    CARO.alreadyInit = true;

    if (!JDE_ICON_PREFIX) {
        /* The constant should hold the WSRP-safe path to icon files, but if it is not 
        supplied, fallback to the default. */
        JDE_ICON_PREFIX = CARO.createUrlString('/share/images/jdeicons/');
    }

    reSpaceTopBar();
    var doc = document;
    CARO.carousel = doc.getElementById('carousel');

    if (!window.favoritesSecurity) // avoid undefined error for jde/GraphPrototype.maf
        return;

    if ((favoritesSecurity == 'readOnly') && (CARO.countNumberOfRenderedFavorites() == 0)) {
        // Favorites are read only and no rendered favorites exist, so we will not render
        // Favorites at all.  

        favoritesSecurity = 'hide';

        var objectsToDelete = ['caroTabFav', 'caroContentFav', 'listFav'];
        for (var i = 0; i < objectsToDelete.length; i++) {
            CARO.deleteFromDom(doc.getElementById(objectsToDelete[i]));
        }
    }

    if ((favoritesSecurity == 'hide') && (caroInitState.caroActiveTab == 'FAV')) {
        //Favs will not be rendered so we will use the first tab on the tabBar
        var bar = doc.getElementById('caroTabBar');
        if (bar) {
            caroInitState.caroActiveTab = bar.children[0].whichcaro;
        }
    }


    if (largeIconsInCarousel == "true") {
        CARO.USE_LARGE_ICONS = 1;
    }
    CARO.redefineTileSizeDependantConstants();

    if (doc.getElementById('caroHolder')) {
        CARO.CONSTANTS.MENU_TOTAL_HEIGHT = doc.getElementById('caroHolder').offsetTop;
    }

    CARO.caroDebugPane = doc.getElementById('caroDebug');

    CARO.carousel.side = CARO.WEST;
    CARO.carousel.sideStr = 'caroLeft';

    switch (caroInitState.caroInitSide) {
        case "north":
            CARO.carousel.side = CARO.NORTH;
            CARO.carousel.sideStr = 'caroTop';
            break;

        case "east":
            CARO.carousel.side = CARO.EAST;
            CARO.carousel.sideStr = 'caroRight';
            break;

        case "south":
            CARO.carousel.side = CARO.SOUTH;
            CARO.carousel.sideStr = 'caroBottom';
            break;

        case "west":
            CARO.carousel.side = CARO.WEST;
            CARO.carousel.sideStr = 'caroLeft';
            break;
    }

    CARO.carousel.expanded = (caroInitState.caroExpanded == "true") ? 1 : 0;
    CARO.carousel.expandedStr = (caroInitState.caroExpanded == "true") ? 'caroExpanded' : 'caroCollapsed';
    doc.getElementById('caroBar').title = (CARO.carousel.expanded ? CARO_STRING_CONSTS.hide : CARO_STRING_CONSTS.show) + ' ' + CARO_STRING_CONSTS.carousel;

    CARO.carousel.activeCaroStr = 'OCL';
    switch (caroInitState.caroActiveTab) {
        case 'OCL':
            CARO.carousel.activeCaroStr = 'OCL';
            break;

        case 'RR':
            CARO.carousel.activeCaroStr = 'RecRpts';
            break;

        case 'FAV':
            CARO.carousel.activeCaroStr = 'Fav';
            break;
    }

    if (!CARO.carousel.getAttribute('whichCaption')) {
        CARO.carousel.setAttribute('whichCaption', 'label_two_rows');
    }
    doc.getElementById('e1AppFrameContainer').setAttribute('whichCaption', CARO.carousel.getAttribute('whichCaption'))
    CARO.numExtraLinesText = (CARO.carousel.getAttribute('whichCaption') == 'label_two_rows') ? 1 : 0;

    CARO.carousel.unselectable = "on";
    CARO.carousel.onselectstart = function () { return false };
    CARO.carousel.style.userSelect = CARO.carousel.style.MozUserSelect = CARO.carousel.style.webkitUserSelect = "none";

    CARO.e1AppFrameContainer = doc.getElementById('e1AppFrameContainer');
    CARO.e1MenuAppIframe = doc.getElementById('e1menuAppIframe');

    CARO.caroMouseState = new Object();

    CARO.caroMouseState.histX = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    CARO.caroMouseState.histY = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    CARO.caroMouseState.histT = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    CARO.caroMouseState.grabStartTime = 0;

    var userAgent = navigator.userAgent.toUpperCase();
    CARO.isWebKit = userAgent.indexOf("WEBKIT") > -1;
    CARO.isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
    CARO.isTouchEnabled = (CARO.isIOS == true) || (userAgent.indexOf("ANDROID") > -1);
    if (CARO.isTouchEnabled)
        CARO.CONSTANTS.THRESHHOLD_PX_SIGNIFYING_INTENDED_MOTION = 5;

    if (!CARO.isTouchEnabled) {
        doc.onmousemove = CARO.caroGlobalOnMouseMove;
        doc.onmouseup = CARO.caroGlobalOnMouseUp;
    }
    else {
        doc.ontouchmove = CARO.caroGlobalOnMouseMove;
        doc.ontouchend = CARO.caroGlobalOnMouseUp;
    }
    doc.body.onresize = CARO.onResize;

    CARO.nbrFingers = 0;
    CARO.doubleTapTimer = null;
    CARO.doubleTapDeltaTime = 700;

    var contentHolder = doc.getElementById('caroContentHolder');
    for (var i = 0; i < contentHolder.children.length; i++) {
        if (contentHolder.children[i].isAScroller) {
            // Not a real content
            continue;
        }
        contentHolder.children[i].getWidth = CARO.getContentWidth;
        contentHolder.children[i].CMID = 0; CMID = 0;
        contentHolder.children[i].computeMinMaxX = CARO.contentComputeMinMaxX;
    }

    CARO.oldActiveOCLTile = -1;
    CARO.oldPageWidth = UTIL.pageWidth();

    doc.getElementById('listOCL').expanded = (caroInitState.caroListExpandedOCL == "true") ? 1 : 0;
    doc.getElementById('listRecRpts').expanded = (caroInitState.caroListExpandedRR == "true") ? 1 : 0;
    if (doc.getElementById('listFav')) {
        // Unlike OCL and RR, Favs may not always exist (i.e., Favs security may be set to hide)
        doc.getElementById('listFav').expanded = (caroInitState.caroListExpandedFAV == "true") ? 1 : 0;
    }

    // document.getElementById('listOCLInner').listOrderBackwards = 1;

    CARO.debug('<div style="position:absolute;top:4px;right:4px;border: 1px solid #000000;width:16px;height:16px;background-color:#ffffff;text-align:center;cursor:pointer" onclick="javascript:this.parentNode.parentNode.style.display=\'none\'">X</div>');

    var homePage = CARO.e1MenuAppIframe.getAttribute("home");
    if (homePage) {
        CARO.e1MenuAppIframe.location = homePage;
    }

    CARO.addHorizontalScrollers();

    CARO.setupTabBar();
    CARO.setupLists();
    CARO.setupHoverPane();

    //Set the arrays based on what the hidden setting is from the server
    //If there is no setting then the logic will set all default tabs to be shown.
    CARO.setHideShowSections();

    CARO.carouselTouch = false;

    var homeTile = doc.getElementById('caroOCL_0');
    if (homeTile) {
        // Make sure caption renders in Chrome
        UTIL.forceRedraw(homeTile.children[1]);
    }

    // self.updateOCL();
    syncRecentReports();
    if (favoritesSecurity != 'hide') {
        CARO.reRenderFavorites();
    }
    else {
        // We don't need favs, but we should mark them as initialized so we don't wait for them
        CARO.initializedFAV = true;
    }

    CARO.expandedUserFoldersArr = caroInitState.caroExpandedUserFolders.split('|');
    // Load the retrieved folder paths to the carousel.
    CARO.openSectionsBackPaths = caroInitState.openSectionsBackPaths;

    CARO.openUserFoldersArr = caroInitState.caroOpenUserFolders.split('|');
    // Load the information for each task folder to restore on login to an 
    // array so that the item can be removed from the setting if the folder
    // is closed.
    CARO.openFavFolderIds = caroInitState.openFavFolderIds.split('|');
    // Load the JSON representation of the open favorite task folders content
    // to the carousel object.
    CARO.openSectionsFavObject = caroInitState.openSectionsFavObject;
    // Create a collection of favorite folders that need to be reopened on 
    // startup.  We need to do these async, so we build up an array of items
    // that need to be processed, then launch them one at a time.
    CARO.openFavTabTiles = [];
    // Create an array to hold the IDs of invalid folders that may be in the user state.
    var invalidIds = [];
    for (var i = 0; i < CARO.openUserFoldersArr.length; i++) {
        // If the 'empty' XML specifier exists in the array, ignore it.
        if (CARO.openUserFoldersArr[i] == "nil" || CARO.openUserFoldersArr[i] == "") {
            continue;
        }
        var tileForFolder = doc.getElementById('caroFav_' + CARO.openUserFoldersArr[i]);
        if (tileForFolder) {
            // We found the tile to open because it's a direct child of Favorites.  
            // Add it to the collection of folders to open once we've determined
            // all the folders that need to be opened.
            CARO.openFavTabTiles.push(tileForFolder);
        }
        else {
            // We didn't find the tile meaning we're dealing with +1 deep task folder.

            // Create a temporary tile object that will be used solely to reopen the tab.
            var tempTile = CARO.createTile(CARO.openUserFoldersArr[i], CARO.TILE_TYPES.FAV);

            // Get the contents of the folder from the pre-loaded JSON favorites object.
            // Each favorites task folder to open should have a property in the openSectionsFavObject object.
            if ((CARO.openSectionsFavObject) && (CARO.openSectionsFavObject['fav' + CARO.openUserFoldersArr[i]])) {
                // We found the content data, so add the temp tile to the async open collection.
                try {
                    tempTile.favInfo = CARO.openSectionsFavObject['fav' + CARO.openUserFoldersArr[i]];
                    if ((tempTile.favInfo) && (tempTile.id) && (tempTile.favInfo.uniqueId == tempTile.id.substring(CARO.TILE_TYPES.TYPE_TO_ID_PREFIX[CARO.TILE_TYPES.FAV].length))) {
                        CARO.openFavTabTiles.push(tempTile);
                    }
                    else {
                        // The folder is invalid for some reason, pull it out of the user state.
                        invalidIds.push(CARO.openUserFoldersArr[i]);
                    }
                }
                catch (error) {
                    CARO.log("Failed to build temp folder for load on startup.  " + error);
                }
            }
            else {
                // The folder is invalid for some reason, pull it out of the user state.
                invalidIds.push(CARO.openUserFoldersArr[i]);
            }
        }
    }

    try {
        // Clean out invalid user state data.
        for (var j = 0; j < invalidIds.length; j++) {
            // Old, invalid data is present in the user state (the key structure likely has changed
            // via a code update), remove the old folder from the user state.
            CARO.singletonRemoveFromArray(CARO.openUserFoldersArr, invalidIds[j]);
            CARO.singletonRemoveFromArray(CARO.expandedUserFoldersArr, invalidIds[j]);

            for (var k = 0; k < CARO.openFavFolderIds.length; k++) {
                if ((CARO.openFavFolderIds[k]) && (CARO.openFavFolderIds[k].substring(0, invalidIds[j].length) == invalidIds[j])) {
                    CARO.singletonRemoveFromArray(CARO.openFavFolderIds, CARO.openFavFolderIds[k]);
                    break;
                }
            }
            var backPathId = (CARO.openSectionsFavObject['fav' + invalidIds[j]]) ? CARO.openSectionsFavObject['fav' + invalidIds[j]].trueTaskId : "";
            if (backPathId.length > 0) {
                CARO.removeBackPathFromSetting(backPathId);
            }
        }
    }
    catch (error) {
        CARO.log("Could not clean invalid data from user state on init. Error: " + error);
    }

    // We've finished as much of the initialization as we can without opening the
    // folders one at a time.  Start opening the folders.
    CARO.openAsyncFavFolders();

    // accessibility - hide the content if CARO is collapsed
    if (!CARO.carousel.expanded) {
        contentHolder.style.display = 'none';
    }

    if (window.WLIST && window.WLIST.initialize) {
        window.WLIST.initialize();
    }
    if (isNativeContainer) {
        CARO.forceHideCarousel();
        if (NativeContainer) {
            NativeContainer.updateData(NativeContainer.PARAM_UPDATE_DATA_WATCHLISTS);
            setTimeout(NativeContainer.notifyApplicationFullyLoaded(), 500);
        }
    }
}

//Helper function called in the caroInit to evaluate
//the state setting from the server and setup the 
//Hidden and shown tab/section information and objects.
CARO.setHideShowSections = function () {
    var listIdBase = "list";
    var tabIdBase = "caroTab";
    var noneHidden = false;
    var doc = document;
    //Check to remove the Fav tab/section from the DEFAULT array if 
    //Favorite security is in place
    if (favoritesSecurity == 'hide' && CARO.CONSTANTS.STATE_VARS.DEFAULT_TABS.indexOf('Fav') != -1) {
        CARO.singletonRemoveFromArray(CARO.CONSTANTS.STATE_VARS.DEFAULT_TABS, 'Fav');
    }
    //Start with the SHOWN matching the list of defaults
    CARO.TABS.SHOWN = CARO.CONSTANTS.STATE_VARS.DEFAULT_TABS;

    //Load the order of the default sections hidden on last saved to server 
    if (caroInitState.caroHiddenDefaultSections && caroInitState.caroHiddenDefaultSections != 'null') {
        CARO.caroHiddenDefaultSections = caroInitState.caroHiddenDefaultSections.split('|');
        //      Add logic to go through the hidden tab(s)/section(s) to move them to the caroHideShowHolder element
        //      Additionally, we may want to make sure there is no overlap with hidden and shown tab/section info.
        //      Which one wins?  I think the section order array should win at this time.
        if (CARO.caroHiddenDefaultSections && CARO.caroHiddenDefaultSections.length > 0 && CARO.caroHiddenDefaultSections[0] != 'nil')//Use the value set from the server state information
        {
            for (var k = 0; k < CARO.caroHiddenDefaultSections.length; k++) {
                var tabName = CARO.caroHiddenDefaultSections[k].split('_')[0];
                var slot = CARO.caroHiddenDefaultSections[k].split('_')[1];
                //Set the slot value for the tab/section,
                // move the element and add the tab/section
                // to the hidden array
                var tab = doc.getElementById(tabIdBase + tabName);
                var list = doc.getElementById(listIdBase + tabName);
                if (tab && list && tabName != null) {
                    if (CARO.TABS.SHOWN.length > 1) {
                        CARO.singletonRemoveFromArray(CARO.TABS.SHOWN, tabName);
                        CARO.singletonAddToArray(CARO.TABS.HIDDEN, tabName);
                        CARO.moveToHideShowHolderSilent(tabName);
                        tab.slot = list.slot = slot;
                    }
                }
            }
        }
        else {
            noneHidden = true;
        }
    }
    else {
        noneHidden = true;
    }

    if (noneHidden) {
        CARO.caroHiddenDefaultSections = ['nil'];
    }
}

CARO.disableTouchEvents = function () {
    document.ontouchmove = document.ontouchend = null;
}

CARO.enableTouchEvents = function () {
    document.ontouchmove = CARO.caroGlobalOnMouseMove;
    document.ontouchend = CARO.caroGlobalOnMouseUp;
}

CARO.enableListResizingWhenAllCarosInitialized = function () {
    if (CARO.initializedOCL && CARO.initializedRR && CARO.initializedFAV) {
        CARO.okToHaveListArrows = true;
        CARO.enableListResizing(true);
    }
    else {
        setTimeout(CARO.enableListResizingWhenAllCarosInitialized, 100);
    }
}

CARO.enableListResizing = function (doTwice) {
    CARO.suspendListResizing = false;
    CARO.computeHeightsForListSections();
    if ((CARO.carousel.side == CARO.WEST) || (CARO.carousel.side == CARO.EAST)) {
        CARO.actGraduallyOnHeightsForListSections(0, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST, 0);
        setTimeout(CARO.decideOnPanArrowsForAllLists, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST + 50);
    }
    else {
        CARO.actImmediatelyOnHeightsForListSections();
    }

    // As a backup.
    if (doTwice) {
        setTimeout('CARO.enableListResizing(false)', 1000);
    }
}

CARO.caroToggleBar = function () {
    if (CARO.lastCaptionHitTime && (((new Date()).getTime() - CARO.lastCaptionHitTime) < CARO.CONSTANTS.ACCIDENTAL_DOUBLE_CLICK_THRESHHOLD)) {
        // This was not a real toggle: it's an accidental double-click on a caption
        return;
    }

    var horizontal = CARO.isHorizontal();
    var collapsed_pos = CARO.CONSTANTS.COLLAPSED_POS_BY_SIDE[CARO.carousel.side];
    var expanded_pos = CARO.CONSTANTS.EXPANDED_POS_BY_SIDE[CARO.carousel.side];
    var duration = horizontal
            ? CARO.CONSTANTS.EXPAND_COLLAPSE_CAROUSEL_DURATION
            : CARO.CONSTANTS.EXPAND_COLLAPSE_CAROUSEL_DURATION_LIST;
    var doc = document;

    if ((CARO.numExtraLinesText) && horizontal) {
        collapsed_pos -= CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT
    }

    if (CARO.carousel.expanded) {
        CARO.hideTabs();
        CARO.showHide_carousel(duration, expanded_pos, collapsed_pos);
    }
    else {
        // accessibility - re-show content holders to allow tabbing to items,
        // and allow screen reader te see the links.
        doc.getElementById('caroContentHolder').style.display = '';
        doc.getElementById('listContentHolder').style.display = '';

        CARO.showTabs();
        CARO.showHide_carousel(duration, collapsed_pos, expanded_pos);
    }

    CARO.carousel.expanded = 1 - CARO.carousel.expanded;
    CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.EXPANDED, (CARO.carousel.expanded) ? "true" : "false");
    setTimeout("CARO.afterResize()", duration);
}

// trigger springboard frame resize only after animiation to avoid too much UI resizse override inside of iframe
CARO.afterResize = function () {
    // triger resize of springboard
    var sbFrame = document.getElementById('sbFrame');
    if (sbFrame != null) {
        sbFrame.contentWindow.SB.onResize();
    }
}

CARO.showHide_carousel = function (tTotal, vFirst, vLast) {
    // fade out the caroBar if CARO.SOUTH; we'll fade it back in after expand/collapse
    if (CARO.carousel.side == CARO.SOUTH) {
        var caroBar = document.getElementById('caroBar');
        ANIM.fadeObj(caroBar, 1, 0, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION / 2);
    }
    CARO.tempDisableResize();
    var now = (new Date()).getTime();
    // springboard may change the width and height of E1MenuAppIframe from the default 100% to a certain px value during drill in app process and we need to reset it back.
    CARO.e1MenuAppIframe.style.width = "100%";
    CARO.e1MenuAppIframe.style.height = "100%";
    CARO.showHide_carousel_inner(now, tTotal, vFirst, vLast, now + 16);
}

// disable resize logic temporally to avod too much overhead for CafeOne to reconstruct workspace during the animation
CARO.tempDisableResize = function () {
    if (CARO.e1MenuAppIframe && CARO.e1MenuAppIframe.contentWindow.JSCompMgr) {
        CARO.e1MenuAppIframe.contentWindow.JSCompMgr.enableResize = false;
        setTimeout("CARO.enableAndTriggerResize()", 1000)
    }
}

// enable resize logic again and trigger the logic to catchup the change after anmiation
CARO.enableAndTriggerResize = function () {
    CARO.e1MenuAppIframe.contentWindow.JSCompMgr.enableResize = true;
    CARO.e1MenuAppIframe.contentWindow.JSCompMgr.onResize("");
}

CARO.showHide_carousel_inner = function (tStart, tTotal, vFirst, vLast, tMin) {
    var now = (new Date()).getTime();
    var t = Math.min(1, Math.max(0, (now - tStart) / tTotal));
    var doc = document;

    if (now >= tMin || t >= 1) {
        var offset =
            ((CARO.carousel.side == CARO.EAST) || (CARO.carousel.side == CARO.WEST)) ?
            195 : (CARO.CONSTANTS.CARO_HEIGHT + ((CARO.numExtraLinesText) ? CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT : 0));

        var caroValue;
        var contValue;

        caroValue = (vFirst + ANIM.TimingFunctions.sinMot(t) * (vLast - vFirst));
        contValue = (offset + vFirst + ANIM.TimingFunctions.sinMot(t) * (vLast - vFirst));

        switch (CARO.carousel.side) {
            case CARO.NORTH:
                CARO.carousel.style.top = caroValue + 'px';
                CARO.e1AppFrameContainer.style.top = contValue + 'px';
                break;
            case CARO.SOUTH:
                CARO.carousel.style.bottom = caroValue + 'px';
                if (contValue > CARO.CONSTANTS.CARO_COLLAPSED_CONTAINER_BOTTOM)  // don't go below the collapsed bar height
                    CARO.e1AppFrameContainer.style.bottom = contValue + 'px';
                else
                    CARO.e1AppFrameContainer.style.bottom = CARO.CONSTANTS.CARO_COLLAPSED_CONTAINER_BOTTOM + 'px';
                break;
            case CARO.EAST:
                CARO.carousel.style.right = caroValue + 'px';
                CARO.e1AppFrameContainer.style.right = contValue + 'px';
                CARO.updateFormDivWidth();
                break;
            case CARO.WEST:
                CARO.carousel.style.left = caroValue + 'px';
                CARO.e1AppFrameContainer.style.left = contValue + 'px';
                CARO.updateFormDivWidth();
                break;
        }

        tMin = now + 16;
    }
    if (t < 1) {
        var expr = 'CARO.showHide_carousel_inner(' + tStart + ', ' + tTotal + ', ' + vFirst + ', ' + vLast + ', ' + tMin + ')';
        setTimeout(expr, tMin - now);
    }
    else {

        CARO.carousel.expandedStr = (CARO.carousel.expanded ? 'caroExpanded' : 'caroCollapsed');
        CARO.carousel.className = CARO.carousel.sideStr + ' ' + CARO.carousel.expandedStr;
        CARO.e1AppFrameContainer.className = CARO.carousel.sideStr + ' ' + CARO.carousel.expandedStr;

        CARO.carousel.style.top =
        CARO.carousel.style.bottom =
        CARO.carousel.style.left =
        CARO.carousel.style.right =

        CARO.e1AppFrameContainer.style.top =
        CARO.e1AppFrameContainer.style.bottom =
        CARO.e1AppFrameContainer.style.left =
        CARO.e1AppFrameContainer.style.right =

        CARO.e1AppFrameContainer.style.height =
        CARO.e1AppFrameContainer.style.width = '';

        // fade the caroBar back in if CARO.SOUTH
        if (CARO.carousel.side == CARO.SOUTH) {
            var caroBar = doc.getElementById('caroBar');
            ANIM.fadeObj(caroBar, 0, 1, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION);
        }
        else if (CARO.carousel.side == CARO.EAST || CARO.carousel.side == CARO.WEST) {
            CARO.shiftTabsIfNecessary();
        }

        doc.getElementById('caroBar').title = (CARO.carousel.expanded ? CARO_STRING_CONSTS.hide : CARO_STRING_CONSTS.show) + ' ' + CARO_STRING_CONSTS.carousel;
        ariaLog(CARO_STRING_CONSTS.carousel + ' ' + CARO_STRING_CONSTS.updated);
        if (!CARO.carousel.expanded) {
            // accessibility - hide content holders to prevent tabbing to items not shown,
            // and prevent screen reader from seeing the links.            
            doc.getElementById('caroContentHolder').style.display = 'none';
            doc.getElementById('listContentHolder').style.display = 'none';
        }
    }
}

CARO.shiftTabsIfNecessary = function () {
    //Check if the active tab needs to shift for visibility
    var fName = "e1menuAppIframe";
    var appFrame = document.getElementById(fName);
    if (!appFrame) {
        appFrame = window.frames[fName];
    }
    if ((appFrame) && (appFrame.contentWindow) && (appFrame.contentWindow.CHP)) {
        appFrame.contentWindow.CHP.shiftIfNecessary(appFrame.contentWindow.CHP.curActiveTab);
    }
}

CARO.updateFormDivWidth = function () {
    if (CARO.isIOS) {
        var formDivElem = CARO.e1MenuAppIframe.contentWindow.document.getElementById('e1formDiv');
        var SPContentDiv = CARO.e1MenuAppIframe.contentWindow.document.getElementById('SPContentDiv');
        var menuBar = CARO.e1MenuAppIframe.contentWindow.document.getElementById('WebMenuBarFrame');
        var doc = document;
        if (SPContentDiv) {
            if (formDivElem)
                formDivElem.style.width = (doc.getElementById('innerRCUX').offsetWidth - SPContentDiv.offsetWidth) + "px";

            if (menuBar)
                menuBar.style.width = (doc.getElementById('innerRCUX').offsetWidth - SPContentDiv.offsetWidth) + "px";
        }
        else {

            if (formDivElem)
                formDivElem.style.width = doc.getElementById('innerRCUX').offsetWidth + "px";
            if (menuBar)
                menuBar.style.width = doc.getElementById('innerRCUX').offsetWidth + "px";
        }
    }
    if (window.CHP_SP) {
        if (CHP_SP.onResize) {
            CHP_SP.onResize();
        }
    }
}

ANIM.TimingFunctions.dropBounce = function (t) {
    var DECAY = 0.25;

    t *= 11;
    if (t < 4) {
        return t * t / 16;
    }
    if (t < 8) {
        return ((t - 6) * (t - 6) / 4) * DECAY + (1 - DECAY);
    }
    if (t < 10) {
        var D_SQUARED = DECAY * DECAY;
        return (t - 9) * (t - 9) * D_SQUARED + (1 - D_SQUARED);
    }
    var D_CUBED = DECAY * DECAY * DECAY;
    return ((t - 10.5) * (t - 10.5) * 4) * D_CUBED + (1 - D_CUBED);
}

ANIM.TimingFunctions.dropNoBounce = function (t) {
    t *= 11;
    if (t < 4) {
        return t * t / 16;
    }
    return 1;
}

CARO.caroShowEater = function () {
    document.getElementById('caroGlobalEventEater').className = 'on';
}

CARO.caroHideEater = function () {
    document.getElementById('caroGlobalEventEater').className = 'off';
}

CARO.caroMouseDown = function (event) {
    if (event.preventDefault) {
        event.preventDefault();
    }
    else {
        event.returnValue = false;
    }

    if (CARO.carousel.setCapture && !CARO.carousel.capturingObject) {
        // Needed in IE to accomodate dragging outside window.
        CARO.carousel.setCapture();
        CARO.carousel.capturingObject = CARO.carousel;
    }

    CARO.caroShowEater();
    CARO.caroMouseState.mouseIsDown = true;
}

CARO.caroTouchStart = function (event) {
    if (event.preventDefault) {
        event.preventDefault();
    }
    else {
        event.returnValue = false;
    }
    CARO.caroShowEater();
    CARO.caroMouseState.mouseIsDown = true;
}

CARO.caroContentTouchStart = function (event) {
    var tFingers = event.touches.length;
    if (tFingers == 1) {
        event.preventDefault();
    }
    CARO.nbrFingers = tFingers;
}

CARO.caroContentTouchCancel = function (e) {
    CARO.clearTouch();
}

CARO.caroContentTouchMove = function (event) {
    // 1 finger touch events are clicks only. If a move is detected then throw away the event and let
    // the device handle the event natively
    if (CARO.nbrFingers == 1) {
        CARO.nbrFingers = 0;
    }
}

CARO.caroContentTouchEnd = function (event) {
    if (CARO.nbrFingers == 1) {
        event.preventDefault();
        CARO.tap(event);
    }
    CARO.clearTouch();
}

CARO.clearTouch = function (clear) {
    CARO.nbrFingers = 0;
    if (clear) {
        CARO.doubleTapTimer = null;
    }
}

CARO.singleTap = function (event) {
    CARO.clearTouch(true);
}

CARO.doubleTap = function (event) {
    CARO.clearTouch(true);
    CARO.caroToggleBar();
}

CARO.tap = function (event) {
    var cTimer = CARO.doubleTapTimer;
    if (cTimer == null) {
        // First tap, we wait X ms to the second tap
        CARO.doubleTapTimer = setTimeout(function () { CARO.singleTap(event) }, CARO.doubleTapDeltaTime);
    } else {
        // Second tap
        clearTimeout(cTimer);
        CARO.doubleTap(event);
    }
    CARO.clearTouch();
}

CARO.performGenericGesture = function (e, gesture) {
    switch (gesture) {
        case TouchLib.LEFT:
        case TouchLib.RIGHT:
            e.preventDefault();
            var tile = CARO.getNextTileToRun(gesture);
            if (tile) {
                if (tile.tileForm.compid == 0) {
                    goHome();
                }
                else {
                    RunOldApp(tile.tileForm.compid);
                }
            }
            break;
    }
}

CARO.caroGlobalOnMouseMove = function (event) {
    event = event || window.event;

    // Track mouse velocity
    var x = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var y = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);
    var t = (new Date()).getTime();

    CARO.caroMouseState.histX.unshift(x);
    CARO.caroMouseState.histY.unshift(y);
    CARO.caroMouseState.histT.unshift(t);

    CARO.caroMouseState.histX.pop();
    CARO.caroMouseState.histY.pop();
    CARO.caroMouseState.histT.pop();

    var gt = CARO.caroMouseState.grabbedTile;
    if (gt) {
        // If we are further than threshold from original mouse 
        // coordinates, decide we're not activating a tile.  
        if ((Math.abs(x - gt.grabX) >= CARO.CONSTANTS.THRESHHOLD_PX_SIGNIFYING_INTENDED_MOTION) ||
            (Math.abs(y - gt.grabY) >= CARO.CONSTANTS.THRESHHOLD_PX_SIGNIFYING_INTENDED_MOTION)) {
            gt.maybeActivateTile = false;
            if (gt.maybeSlideCaro) {
                // If we're before the time threshhold for moving a single tile
                // vs sliding entire carousel, decide on this
                gt.maybeMoveTile = false;
            }
        }
    }

    if (CARO.caroMouseState.mouseIsDown) {
        CARO.caroEvaluateWhetherToMoveCarousel(event);
        if (gt) {
            if (gt.maybeSlideCaro) {
                CARO.updatePosForSlide(event, gt);
            }
            else if (gt.maybeMoveTile) {
                if (!gt.tile.tileForm.doNotMove) {
                    CARO.updatePosForTileMove(event, gt);
                }
            }
            //Adding a check to see if the mouse is over a droppable tab when we have a tile grabbed
            if (CARO.isHorizontal())//Horizontal handling
            {
                var tabBar = document.getElementById('caroTabBar');
                if (tabBar.state == 'show' && CARO.isMouseOverTabBar(event)) {
                    if ((gt.tile.favInfo && CARO.isUserFolder(gt.tile.favInfo.appType)) || gt.tile.tileForm.parentNode.isTFContent) {
                        gt.tile.style.cursor = 'not-allowed';
                    }
                    else {
                        gt.tile.style.cursor = ''; //Reset this in case it is not-allowed, it should always be the pointer here
                        for (var i = 0; i < tabBar.children.length - 1; i++) {
                            var tab = tabBar.children[i];
                            if (tabBar.activeTab != tab) {
                                if (CARO.isMouseOverTab(event, tab)) {
                                    tab.dropPending = true;
                                    CARO.tabMarkedForDrop = tab;
                                    gt.tile.currTab = tabBar.activeTab.id;
                                    setTimeout(function () { CARO.dropActivation(x) }, CARO.CONSTANTS.TIME_BEFORE_TAB_ACTIVATION);
                                }
                            }
                        }
                    }
                }
                else {
                    if (CARO.tabMarkedForDrop != null) {
                        CARO.tabMarkedForDrop.dropPending = false;
                        CARO.tabMarkedForDrop = null;
                    }
                }
            }
        }
    }

    // Handle mouse movement when a carousel section is
    // grabbed.
    var gs = CARO.caroMouseState.grabbedSection;
    if (gs) {
        // If we are further than threshold from original mouse 
        // coordinates, decide we're not activating a section.  

        var motionThreshold = CARO.CONSTANTS.THRESHHOLD_PX_SIGNIFYING_INTENDED_MOTION;
        // Use the proper dimension for the section represenation.
        if (gs.section && gs.section.isTab) {
            motionThreshold = Math.abs(gs.grabX - x);
        }
        else if (gs.section && gs.section.isList) {
            motionThreshold = Math.abs(gs.grabY - y);
        }
        // Put a time threshold in to verify if the user isn't insignificantly dragging the section.
        if (gs.markedToMove && (CARO.caroMouseState.grabStartTime != 0 && (((new Date()).getTime() - CARO.caroMouseState.grabStartTime) > CARO.CONSTANTS.MIN_TIME_TO_MOVE_FOR_FULL_CARO_SLIDE))) {
            gs.moveCommitted = 1;
            gs.maybeSlideCaro = 0;
        }
        // Put a distance threshold in to user isn't insignificantly dragging the section.
        if (CARO.caroMouseState.mouseIsDown && motionThreshold > CARO.CONSTANTS.THRESHHOLD_PX_SIGNIFYING_INTENDED_MOTION && gs.moveCommitted && gs.markedToMove) {
            // The section is being intentionally moved, so update the UI to reflect the drag.
            CARO.updatePosForSectionMove(event, gs);
        }
        else if (gs.maybeSlideCaro) {
            CARO.updatePosForSlide(event, gs);
        }
    }
}


//Helper function to determine if the carousel is horizontal mode
CARO.isHorizontal = function () {
    return (CARO.carousel && (CARO.carousel.side == CARO.SOUTH || CARO.carousel.side == CARO.NORTH));
}

CARO.isVertical = function () {
    return (CARO.carousel && (CARO.carousel.side == CARO.EAST || CARO.carousel.side == CARO.WEST));
}

CARO.dropActivation = function (mouseX) {
    var doc = document;
    var tabBar = doc.getElementById('caroTabBar');
    if (tabBar) {
        if (CARO.tabMarkedForDrop && CARO.tabMarkedForDrop != null) {
            var tab = CARO.tabMarkedForDrop;
            if (tab && tab.dropPending) {
                //No more work to be done if this is already the active tab
                if (tab && tabBar.activeTab && tab == tabBar.activeTab) {
                    return;
                }
                tab.dropPending = false;
                CARO.tabMarkedForDrop = null;
                var gt = CARO.caroMouseState.grabbedTile;
                if (gt)//This should always be the case, but do a check
                {
                    //Check to see if we are over the Open Applications or the Recent Reports tab
                    //if so, we can change the cursor and return
                    if (tab.id == 'caroTabOCL' || tab.id == 'caroTabRecRpts') {
                        gt.tile.style.cursor = 'not-allowed';
                        return;
                    }
                    var toContent = null;
                    if (tab.id == 'caroTabFav') {
                        toContent = doc.getElementById("caroContentFav");
                    }
                    else {
                        var strId = tab.id.substring(16);
                        //Now append the caroContentFavFolder string to the above and get the element
                        toContent = doc.getElementById("caroContentFavFolder" + strId);
                    }

                    if (toContent != null && toContent != undefined) {
                        //Check to see that the toContent is a Task folder type
                        if (toContent.isTFContent == true) {
                            gt.tile.style.cursor = 'not-allowed';
                            return;
                        }
                        //Check to see if the favorite already exists
                        if (CARO.isDuplicateFavorite(toContent, gt.tile)) {
                            gt.tile.style.cursor = 'not-allowed';
                        }
                        else {
                            //Set the previous parentTaskId value
                            if (gt.tile.tileForm.favInfo && gt.tile.tileForm.favInfo.parentTaskId != "null") {
                                gt.tile.prevParentTaskId = gt.tile.tileForm.favInfo.parentTaskId;
                            }
                            else {
                                gt.tile.prevParentTaskId = gt.tile.tileForm.parentNode.folderId;
                            }
                            var fromContentX = gt.tile.tileForm.parentNode.actualX;
                            var toContentX = toContent.actualX;
                            var fromContent = gt.tile.tileForm.parentNode;
                            var shiftValue = CARO.computeShiftValue(fromContentX, toContentX);
                            CARO.moveTileToNewContent(toContent, gt.tile); //This will move it to the destination container.
                            //If the content location is the same as when the tile was grabbed
                            //this is not actually a folder move.
                            if (gt.contentWhenGrabbed == gt.tile.tileForm.parentNode.id) {
                                gt.tile.folderMove = false;
                            }
                            else {
                                gt.tile.folderMove = true;
                            }
                            //Click the tab where the tile should be moved to
                            tab.onclick(null);
                            var leftNow = gt.tile.tileForm.actualX;
                            gt.offsetX += shiftValue;
                            //Reset the offset from the containers location
                            gt.tile.tileForm.style.left = (gt.tile.tileForm.actualX = leftNow + shiftValue) + 'px';
                            //Try to reset the content of the from locations content to remove any spaces
                            CARO.setDesiredPositions(fromContent);
                            CARO.slideToDesiredPositions(fromContent, 50);
                        }
                    }
                }
            }
        }
    }
}


CARO.computeShiftValue = function (from, to) {
    return (from - to);
}

CARO.isDuplicateFavorite = function (content, tile) {
    var found = false;
    for (var i = 0; i < content.children.length; i++) {
        if (content.childNodes[i].favInfo && !CARO.isUserFolder(content.childNodes[i].favInfo.appType)) {
            found = (content.childNodes[i].favInfo.trueTaskId == tile.favInfo.trueTaskId);
        }
        if (found) {
            break;
        }
    }
    return found;
}

//Helper function to check for duplicate tile within a tile that is a user folder
//The assumption is that the folderTile parm is known to be a user folder type appType='21'
//Account for inadvertent drops on the manageFavs tile.
CARO.isDuplicateFavByTile = function (moveTile, folderTile) {
    var found = false;
    if (folderTile.special != 'manageFavs') {
        var childrenFavs = folderTile.favInfo.childFavs;
        if (childrenFavs) {
            for (var i = 0; i < childrenFavs.length; i++) {
                found = (moveTile.favInfo.trueTaskId == childrenFavs[i].trueTaskId);
                if (found) {
                    break;
                }
            }
        }
    }
    return found;
}

CARO.activateTile = function (tile) {
    tile = tile.tileForm;

    var now = (new Date()).getTime();
    if (tile.lastActivated && ((now - tile.lastActivated) < CARO.CONSTANTS.ACCIDENTAL_DOUBLE_CLICK_THRESHHOLD)) {
        // User didn't really intend to activate tile twice.
        return;
    }
    tile.lastActivated = now;

    switch (tile.type) {
        case CARO.TILE_TYPES.OCL:
            if (tile.compid == 0) {
                goHome();
            }
            else {
                RunOldApp(tile.compid);
            }
            break;
        case CARO.TILE_TYPES.FAV:
            if (tile.special == 'manageFavs') {
                if (manageFavorites) {
                    manageFavorites();
                }
            }
            else if (CARO.isFolder(tile.favInfo.appType)) {
                CARO.activateFavoriteFolder(tile);
            }
            else if (CARO.isURL(tile.favInfo.appType)) {
                CARO.launchFavoriteUrlFromFavInfo(tile.favInfo);
            }
            else if (CARO.isOVR(tile.favInfo.appType) || CARO.isExternalApp(tile.favInfo.appType)) {
                CARO.launchFavoriteTaskFromFavInfo(tile.favInfo);
            }
            else {
                CARO.launchFavoriteFromFavInfo(tile.favInfo);
            }
            break;
        case CARO.TILE_TYPES.RRPT:
            if (tile.special == 'WSJ') {
                runE1App('P986110B', 'W986110BA', WSJNewVersion, '', '', '|7|', '|' + tile.dataSource + '|');
            }
            else {
                /* DS for WSJ is:
                *  4 - EXEHOST (Execution Host Name) 
                *  5 - CHFG (Change Flag)
                *  7 - DATP (Data Source)
                *  8 - EV01 (J.D. Edwards EnterpriseOne Event Point One)
                *  9 - FNDFUF2 (Foundation - Future Use 2)
                * 10 - JOBNBR (Server Job Number)
                * 11 - PRTQ (Print Queue)
                * 12 - ??
                * 13 - (AutoLaunch Selected)
                */

                var prefix = tile.rptInfo.reportNameDesc;
                runE1App('P986110B', 'W986110BA', WSJNewVersion, '', prefix, '|4|7|10|11|13|', ['', tile.rptInfo.server, tile.dataSource, tile.rptInfo.jobId, tile.rptInfo.port, '1', ''].join('|'));
            }
            break;

    }
}

CARO.launchFavoriteFromFavInfo = function (details) {
    RunNewApp(details.launchAction,
              details.promptPO,
              details.appID,
              details.appType,
              details.formID,
              details.version,
              details.mode,
              details.menuSysCode,
              details.taskName,
              details.formDSTmpl,
              details.formDSData,
              details.launchInNewWindow,
              details.accessibilityMode,
              details.nodeID,
              details.taskName,
              details.RID,
              details.menuNodeId);
}

CARO.launchFavoriteUrlFromFavInfo = function (details) {
    //The favorite URL value (appID) should be an encoded URL.  We need 
    //to decode the URL before calling RunUrl.    
    RunUrl(decodeURIComponent(details.appID));
}

CARO.launchFavoriteTaskFromFavInfo = function (details) {
    doFastPath(details.taskNameLabel);
}

// This handles keydown for various carousel objects
// It allows SPACE and ENTER to activate the click event
CARO.keyDown = function (event, obj) {
    if (!obj) obj = this;
    // treat space or enter the same as a click
    if (event.keyCode == 32 || event.keyCode == 13) {
        obj.click();
    }
}

CARO.caroGlobalOnMouseUp = function (event) {
    event = event || window.event;

    CARO.caroMouseState.mouseIsDown = false;
    CARO.caroMouseState.grabStartTime = 0;
    CARO.caroHideEater();

    var gt = CARO.caroMouseState.grabbedTile;
    CARO.caroMouseState.grabbedTile = null;
    if (gt) {
        if (gt.maybeActivateTile) {
            if (event && event.button == 2)//Added this so a right mouse click does not act like a right mouse click.
            {
                //CARO.log('Right mouse clicked, ignoring.');
            }
            else {
                CARO.activateTile(gt.tile);
            }
        }
        else if (!gt.maybeSlideCaro) {
            CARO.slideTileToDesiredPos(gt.tile, 0, (gt.tile.isListElem) ? CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_SPEEDY : 0);
            gt.tile.style.cursor = '';

            var possiblyReorder = false; //Use this to send a reorder request back to the server when needed.

            if (gt.tile.tileForm.type == CARO.TILE_TYPES.FAV && gt.tile.tileForm.favInfo) {
                var parentTaskId = gt.tile.tileForm.favInfo.parentTaskId;
                if (!parentTaskId || parentTaskId == "null") {
                    parentTaskId = gt.tile.tileForm.parentNode.folderId;
                }

                //Check if an Organize was done to set the possible reorder flag
                if (!gt.tile.folderMove && CARO.possibleDropTarget == null && !gt.listFavFloating) {
                    possiblyReorder = true;
                }

                //No need to check if it moved actual folders because the call to submit the move will not do anything if it does not need to.
                if (gt.tile.folderMove && gt.tile.prevParentTaskId) {
                    if (gt.tile.tileForm.parentNode.id != gt.contentWhenGrabbed)//This is an additional check to make sure the submit should happen.
                    {
                        parentTaskId = gt.tile.prevParentTaskId;
                        if (gt.tile.tileForm.parentNode.id == 'caroContentFav') {
                            CARO.submitMoveToManageFavorites(gt.tile.tileForm.favInfo.trueTaskId, parentTaskId, '98', gt.tile.tileForm.favInfo.seqNo, gt.tile.tileForm.favInfo.taskName);
                        }
                        else {
                            CARO.submitMoveToUserFolder(gt.tile.tileForm.favInfo.trueTaskId, parentTaskId, '98', gt.tile.tileForm.favInfo.seqNo, gt.tile.tileForm.favInfo.taskName, gt.tile.tileForm.parentNode.folderId);
                        }
                    }
                    //Reset organize values
                    gt.tile.folderMove = false;
                    gt.tile.prevParentTaskId = null;
                }
                //Handle drop on tile for Organize Favorites possible tracking objects and drop tile into folder
                if (CARO.possibleDropTarget != null) {
                    //First check for active drop and over tile still
                    if (CARO.isDropActive(CARO.possibleDropTarget.tile.tileForm) && CARO.isTileOverFolderTile(gt.tile.tileForm, CARO.possibleDropTarget.tile.tileForm, gt.tile.isTile)) {
                        //Get the parent task id of the tile being dropped into/onto
                        var pTaskId = CARO.possibleDropTarget.tile.tileForm.favInfo.parentTaskId;
                        if (!pTaskId || pTaskId == null) {
                            //If this doesn't get set, then try to get it from the parentNode
                            pTaskId = CARO.possibleDropTarget.tile.parentNode.favsParentTaskId;
                        }
                        //Submit the move to the server
                        CARO.submitMoveToUserFolder(gt.tile.tileForm.favInfo.trueTaskId, pTaskId, '98', gt.tile.tileForm.favInfo.seqNo, gt.tile.tileForm.favInfo.taskName, CARO.possibleDropTarget.tile.tileForm.favInfo.trueTaskId);
                        //Remove it  from DOM
                        CARO.removeElem(gt.tile.listForm);
                        CARO.removeElem(gt.tile.tileForm);
                    }
                    var caroContent = CARO.possibleDropTarget.tile.tileForm.parentNode;
                    var listContent = CARO.possibleDropTarget.tile.listForm.parentNode;
                    CARO.setDesiredPositions(caroContent);
                    CARO.setDesiredPositionsList(listContent);
                    CARO.deactivateDropTarget(CARO.possibleDropTarget.tile); //This call resets the CARO.possibleDropTarget also
                }
                //Handling drop of listItem onto a section for Organize Favorites
                if (gt.listFavFloating) {
                    //Set the original section the item came from
                    var origContent = document.getElementById(gt.listFavGrabbedParentId);
                    //If there is a section set - drop it 
                    if (gt.sectionForDrop) {
                        var taskId = gt.tile.tileForm.favInfo.trueTaskId;
                        var pTaskId = gt.tile.tileForm.favInfo.parentTaskId;
                        if (pTaskId == null || pTaskId == 'null') {
                            var tileParent = (gt.tile.tileForm.parentElement === undefined) ? gt.tile.tileForm.parentNode : gt.tile.tileForm.parentElement;
                            pTaskId = tileParent.folderId;
                        }
                        var seqNum = gt.tile.tileForm.favInfo.seqNo;
                        var tName = gt.tile.tileForm.favInfo.taskName;

                        if (gt.sectionForDrop.id == 'listFav')//Favorites section
                        {
                            //Remove list item and the item will be added back when the form returns.
                            CARO.removeElem(gt.tile.listForm);
                            CARO.removeElem(gt.tile.tileForm);
                            //Submit the move to the server -moveToFavorites 
                            var expr = 'CARO.submitMoveToManageFavorites("' + taskId + '","' + pTaskId + '","' + '98' + '","' + seqNum + '","' + tName + '");';
                            setTimeout(expr, 50);
                        }
                        else//This means the section is anything but "Favorites"
                        {
                            var caroFolderObj = document.getElementById('caroContentFavFolder_' + gt.sectionForDrop.userFolderId); //This is used to get to the folderId when submitting to server
                            if (caroFolderObj) {
                                //Remove list item and the item will be added back when the form returns.
                                CARO.removeElem(gt.tile.listForm);
                                CARO.removeElem(gt.tile.tileForm);
                                //Submit the move to the server - moveToUserFolder
                                var expr = 'CARO.submitMoveToUserFolder("' + taskId + '","' + pTaskId + '","' + '98' + '","' + seqNum + '","' + tName + '","' + caroFolderObj.folderId + '");';
                                setTimeout(expr, 50);
                            }
                            else//This means something went wrong, so put it back where it came from
                            {
                                //To original section location
                                CARO.moveTileToNewContent(origContent, gt.tile.listForm);
                                possiblyReorder = true;
                            }
                        }
                        CARO.deactivateSectionForDrop(gt.sectionForDrop);
                        CARO.setDesiredPositionsList(origContent);
                    }
                    else//This means there is no active section to drop it on, so we should put it back in its original location
                    {
                        //To original section location
                        CARO.moveTileToNewContent(origContent, gt.tile.listForm);
                        possiblyReorder = true;
                    }
                    //Reset tracking values for listItem moves
                    if (gt.sectionForDrop) {
                        CARO.deactivateSectionForDrop(gt.sectionForDrop);
                        CARO.setDesiredPositionsList(gt.tile.listForm.parentNode);
                        CARO.setDesiredPositions(gt.tile.tileForm.parentNode);
                    }
                    gt.listFavFloating = null;
                    gt.sectionForDrop = null;
                    gt.listFavGrabbedParentId = null;
                    //Check to see if the organize was executed
                }

                //Check to see if a reorder is necessary - A organize may have executed, but still need a reorder
                if (gt.lastSwapTarget && possiblyReorder) {
                    var oldIndex = gt.indexWhenGrabbed;
                    var newIndex = gt.tile.tileForm.slot;

                    if (oldIndex != newIndex) {
                        var fromSeqNo = gt.tile.tileForm.favInfo.seqNo;
                        var toSeqNo = 0;
                        if (newIndex > oldIndex && oldIndex != 0) {
                            // Want seqNo of the one right before where it ended up.
                            toSeqNo = gt.tile.tileForm.parentNode.children[newIndex - 1].favInfo.seqNo;
                        }
                        else {
                            if (gt.tile.tileForm.parentNode.children.length - 1 != newIndex) {
                                // Want seqNo of the one right after where it ended up.
                                toSeqNo = gt.tile.tileForm.parentNode.children[newIndex + 1].favInfo.seqNo;
                            }
                        }
                        //Send the reorder form back to the server
                        reOrderFavoritesFrom(fromSeqNo, parentTaskId);
                        reOrderFavoritesTo(toSeqNo);
                    }
                }

            }

        }

        /* We always want to kick off caro motion, in case we are partailly offscreen.  
        If user is sliding carousel, we will use the mouse velocity.  If user is not 
        sliding carousel (activate tile or reorder tile), then use initial velocity 0. */

        var velocity = 0;
        if (gt.maybeSlideCaro) {
            velocity = (gt.tile.isTile) ? CARO.getMouseVelocity().x : CARO.getMouseVelocity().y;
        }

        if (gt.tile.parentNode) {
            if (gt.tile.isTile) {
                CARO.caroMotion(gt.tile.parentNode.id, gt.tile.parentNode.actualX, velocity);
            }
            else {
                CARO.caroMotion(gt.tile.parentNode.id, gt.tile.parentNode.actualY, velocity);
            }
        }
        CARO.markTileNormal(gt.tile);
        CARO.deactivateDropTarget(gt.tile.listForm);

    }

    if (CARO.carousel.capturingObject && CARO.carousel.capturingObject.releaseCapture) {
        CARO.carousel.capturingObject.releaseCapture();
        CARO.carousel.capturingObject = null;
    }

    // Handle the drop action for dragged carousel sections.
    CARO.checkForSectionDrop();
}

CARO.submitMoveToUserFolder = function (taskId, parentTaskId, taskView, seqNumber, taskName, folderTaskId) {
    var doc = document;
    doc.getElementById("E1MFavoritesTaskId").value = taskId;
    doc.getElementById("E1MFavoritesParentTaskId").value = parentTaskId;
    doc.getElementById("E1MFavoritesTaskView").value = taskView;
    doc.getElementById("E1MFavoritesSeqNum").value = seqNumber;
    doc.getElementById("E1MFavoritesAction").value = "moveToUserFolder";
    var unescapeName = CARO.decodeText(taskName);
    doc.getElementById("E1MFavoritesNewName").value = unescapeName;
    doc.getElementById("E1MFavoritesUserDefinedFolderTaskId").value = folderTaskId;
    doc.getElementById("E1FavoritesForm").submit();
}

//Helper function to submit the favorites form to move a tile from a folder to 
//favorites menu location.
CARO.submitMoveToManageFavorites = function (taskId, parentTaskId, taskView, seqNum, taskName) {
    var doc = document;
    doc.getElementById("E1MFavoritesTaskId").value = taskId;
    doc.getElementById("E1MFavoritesParentTaskId").value = parentTaskId;
    doc.getElementById("E1MFavoritesTaskView").value = taskView;
    doc.getElementById("E1MFavoritesSeqNum").value = seqNum;
    doc.getElementById("E1MFavoritesAction").value = 'fm';
    var unescapeName = CARO.decodeText(taskName);
    doc.getElementById("E1MFavoritesNewName").value = unescapeName;
    doc.getElementById("E1FavoritesForm").submit();
}

// Will decode any HTML character entities in the provided string and return
// the fully decoded/human-readable string.
CARO.decodeText = function (text) {
    if (!CARO.textDecoder) {
        var doc = document;
        CARO.textDecoder = doc.createElement('div');
        var textSpan = doc.createElement('span');
        CARO.textDecoder.appendChild(textSpan);
        // The decoder should not be visible.
        CARO.textDecoder.className = 'stealthed';
        doc.getElementById('carousel').appendChild(CARO.textDecoder);
    }
    CARO.textDecoder.style.display = 'block';
    CARO.textDecoder.firstChild.innerHTML = text;
    var returnValue = (CARO.textDecoder.textContent === undefined) ? CARO.textDecoder.innerText : CARO.textDecoder.textContent;
    CARO.textDecoder.style.display = 'none';
    return returnValue;
}

CARO.isDropActive = function (tile) {
    var dropActiveTile = tile.tileForm.getAttribute('activatefordrop');
    var dropActiveList = tile.listForm.getAttribute('activatefordrop');
    if ((dropActiveTile && dropActiveTile == 'true') || (dropActiveList && dropActiveList == 'true')) {
        return true;
    }
    return false;
}

CARO.decideOnFullSlide = function () {
    var gt = CARO.caroMouseState.grabbedTile;
    if (gt && gt.maybeMoveTile) {
        // maybeMoveTile has not been eliminated, meaning we've had no significant 
        // motion.  Conclude we are not sliding the entire carousel.
        gt.maybeSlideCaro = false;

        // We also want to set the border to indicate the tile is moveable.
        if (!gt.tile.tileForm.doNotMove) {
            // Normal tile
            CARO.markTileMoveable(gt.tile);
        }
        else {
            // Immoveable tile
            CARO.markTileImmoveable(gt.tile);
        }
    }
    // If a section is grabbed, check if it is 
    // draggable and set appropriate UI and state
    // indicators.
    var gs = CARO.caroMouseState.grabbedSection;
    if (gs) {
        gs.maybeSlideCaro = false;

        if (!gs.section.doNotMove) {
            // Normal section
            CARO.markSectionMoveable(gs.section); // UI styling
            gs.markedToMove = 1; // flag it in the drag state.
        }
        else {
            // Immoveable section
            CARO.markSectionImmoveable(gs.section); // UI styling
            gs.markedToMove = 0; // flag it in the normal (i.e. non-drag) state.
        }
    }
}

CARO.updatePosForSlide = function (event, gt) {
    if (gt.tile) {
        if (gt.tile.isTile) {
            // This is a tile (horizontal mode)
            gt.tile.parentNode.style.left = (gt.tile.parentNode.actualX = ((CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX) + gt.wholeCaroOffsetX)) + 'px';
            CARO.decideWhetherToHaveArrowsCaro();
        }
        else {
            // This is a list (vertical mode)
            gt.tile.parentNode.style.top = (gt.tile.parentNode.actualY = (event.clientY + gt.wholeCaroOffsetY)) + 'px';
            CARO.decideWhetherToHaveArrowsList(gt.tile.parentNode);
        }
    }
    else if (gt.section) {
        if (gt.section.isTab) {
            if (gt.wholeCaroOffsetX) {
                // This is a tab (horizontal mode)
                gt.section.parentNode.style.left = (gt.section.parentNode.actualX = ((CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX) + gt.wholeCaroOffsetX)) + 'px';
            }
        }
        else {
            if (gt.wholeCaroOffsetY) {
                // This is a list (vertical mode)
                gt.section.parentNode.style.top = (gt.section.parentNode.actualY = (event.clientY + gt.wholeCaroOffsetY)) + 'px';
            }
        }
    }
}

CARO.updatePosForTileMove = function (event, gt) {
    var isTile = gt.tile.isTile;
    var isList = gt.tile.isListElem;

    var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);
    if (isTile) {
        gt.tile.style.left = (gt.tile.actualX = eClientX + gt.offsetX) + 'px';
        gt.tile.style.top = (gt.tile.actualY = eClientY + gt.offsetY) + 'px';
    }
    else {
        var minY = 0;
        var maxY = gt.tile.parentNode.clientHeight - gt.tile.clientHeight;
        if (CARO.canOrganizeListItem(gt.tile)) {
            var listContentHolder = document.getElementById('listContentHolder');
            if (((eClientY + gt.offsetY) <= -3 || eClientY + gt.offsetY > maxY + 5) || gt.listFavFloating) {
                if (listContentHolder && !gt.listFavFloating) {
                    gt.listFavGrabbedParentId = gt.tile.listForm.parentNode.id;
                    listContentHolder.appendChild(gt.tile.listForm);
                    gt.listFavFloating = true;
                }
                maxY = listContentHolder.draggableHeight;
                gt.tile.style.top = (gt.tile.actualY = UTIL.clamp(eClientY - CARO.CONSTANTS.MENU_TOTAL_HEIGHT, minY, maxY)) + 'px';
            }
            else {
                gt.tile.style.top = (gt.tile.actualY = UTIL.clamp(eClientY + gt.offsetY, minY, maxY)) + 'px';
            }

        }
        else {
            gt.tile.style.top = (gt.tile.actualY = UTIL.clamp(eClientY + gt.offsetY, minY, maxY)) + 'px';
        }
    }


    var content = gt.tile.parentNode;


    gt.tile.style.cursor = '';
    if ((!gt.listFavFloating && isList) || isTile)//Floating means a list item is being moved from one section to another
    {
        for (var i = gt.tile.tileForm.slot - 1; i >= 0; i--) {
            var otherTile = content.childNodes[i];
            //Added for Organize Favorites
            var overFolderTile = CARO.isTileOverFolderTile(gt.tile.tileForm, otherTile.tileForm, isTile); //Droppable means "Can drop onto/into" .dropCapable

            if (((isTile) &&
                 (gt.tile.desiredX > otherTile.desiredX) &&
                 (gt.tile.actualX < otherTile.desiredX + 0.4 * CARO.CONSTANTS.OFFSET_PER_TILE))
                ||
            ((isList) && (gt.tile.backwardsOrder) &&
                 (gt.tile.desiredY < otherTile.desiredY) &&
                 (gt.tile.actualY > otherTile.desiredY - 0.4 * gt.tile.clientHeight))
                ||
            ((isList) && (!gt.tile.backwardsOrder) &&
                 (gt.tile.desiredY > otherTile.desiredY) &&
                 (gt.tile.actualY < otherTile.desiredY + 0.4 * CARO.CONSTANTS.LIST_ITEM_HEIGHT))) {
                CARO.decideOnSwap(gt, otherTile, content, overFolderTile, i);
            }
            else {
                break;
            }
        }
        for (var i = gt.tile.tileForm.slot + 1; i < content.childNodes.length; i++) {
            var otherTile = content.childNodes[i];
            //Added for Organize Favorites
            var overFolderTile = CARO.isTileOverFolderTile(gt.tile.tileForm, otherTile.tileForm, isTile); //Droppable means "Can drop onto/into"

            if (otherTile.tileForm.doNotMove) {
                // Can never move anything past an immoveable tile
                break;
            }

            if (((isTile) &&
                 (gt.tile.desiredX < otherTile.desiredX) &&
                 (gt.tile.actualX > otherTile.desiredX - 0.4 * CARO.CONSTANTS.OFFSET_PER_TILE))
                ||
                ((isList) && (gt.tile.backwardsOrder) &&
                 (gt.tile.desiredY > otherTile.desiredY) &&
                 (gt.tile.actualY < gt.tile.desiredY - 0.4 * gt.tile.clientHeight))
                ||
                ((isList) && (!gt.tile.backwardsOrder) &&
                 (gt.tile.desiredY < otherTile.desiredY) &&
                 (gt.tile.actualY > otherTile.desiredY - 0.4 * CARO.CONSTANTS.LIST_ITEM_HEIGHT))) {
                CARO.decideOnSwap(gt, otherTile, content, overFolderTile, i);
            }
            else {
                break;
            }
        }
    }
    //Organize Favorites - Added logic for moving list tiles from one favorite section to another
    else {
        //Where is the item hovering over - Highlight the section being hovered over
        //And attach that information to gt object
        CARO.locateAndMarkSection(gt);
    }
}

CARO.decideOnSwap = function (gt, otherTile, content, overFolderTile, newSlot) {
    if (!otherTile.tileForm.doNotMove && !overFolderTile) {
        if (CARO.possibleDropTarget != null)//We're not over a droppable tile so reset the possible drop
        {
            CARO.deactivateDropTarget(CARO.possibleDropTarget.tile.tileForm);
        }
        gt.lastSwapTarget = otherTile;
        CARO.swapTiles(content, gt.tile.tileForm.slot, newSlot);
        CARO.slideTileToDesiredPos(otherTile, 0);
        CARO.slideTileToDesiredPos(gt.tile.otherForm, 0);
        CARO.slideTileToDesiredPos(otherTile.otherForm, 0);

        // The swap causes the capturing to be broken in IE, so we must reset capture
        if (gt.tile.setCapture) {
            gt.tile.setCapture();
            CARO.carousel.capturingObject = gt.tile;
        }
    }
    else {
        if (overFolderTile && (gt.tile.tileForm.favInfo && !CARO.isUserFolder(gt.tile.tileForm.favInfo.appType))) {
            if (CARO.possibleDropTarget == null) {
                //Time to set to possible drop - Organize Favorites
                CARO.possibleDropTarget = new Object();
                CARO.possibleDropTarget.tile = otherTile;
                CARO.possibleDropTarget.startTime = (new Date()).getTime();
                setTimeout("CARO.checkForDropActivation()", CARO.CONSTANTS.TIME_BEFORE_TAB_ACTIVATION);
            }
        }
    }
}

//Helper to determine if the dragged list item can be organized
//on the glass
CARO.canOrganizeListItem = function (listItem) {
    return (listItem.listForm.id.match('listFav') != null && !CARO.isUserFolder(listItem.tileForm.favInfo.appType) && !listItem.tileForm.parentNode.isTFContent);
}

//Checking to see where a list item is in the list container
//Then marking and tracking a section for possible drop
//Used in list mode when a tile is being dragged outside its
//original content holder
CARO.locateAndMarkSection = function (gt) {
    var doc = document;
    var listContentHolder = doc.getElementById('listContentHolder');
    var gtList = gt.tile.listForm;
    if (listContentHolder) {
        for (var i = 0; i < listContentHolder.children.length; i++) {
            //List item top and bottom
            var section = listContentHolder.children[i];
            var topEdge_a = gtList.actualY - 2;
            var btmEdge_a = topEdge_a + (CARO.CONSTANTS.LIST_ITEM_HEIGHT - 2);
            //Folder tile edges list form
            var topEdge_b = section.logicalY - 2;
            var btmEdge_b = topEdge_b + section.offsetHeight;
            var onSection = ((topEdge_a < btmEdge_b)
                     && (btmEdge_a > topEdge_b));
            if (onSection) {
                //Check for duplicate favorite in section
                var caroFolder = null;
                if (section.id == 'listFav') {
                    caroFolder = doc.getElementById('caroContentFav');
                }
                else {
                    caroFolder = doc.getElementById('caroContentFavFolder_' + section.userFolderId);
                }

                var isDupFav = false;
                if (caroFolder && CARO.sectionIsDroppable(section)) {
                    isDupFav = CARO.isDuplicateFavorite(caroFolder, gt.tile.tileForm);
                }
                //Check to see if the section is the same one as before
                var lastSection = null;
                if (gt.sectionForDrop) {
                    lastSection = gt.sectionForDrop;
                }

                if (lastSection && lastSection != section) {
                    CARO.deactivateSectionForDrop(lastSection);
                    gt.sectionForDrop = null;
                }
                //Activate it if its not already active, is a droppable section and does not have a duplicate favorite in it.
                if (CARO.sectionIsDroppable(section) && !CARO.isSectionActivated(section) && !isDupFav) {
                    CARO.activateSectionForDrop(section);
                    gt.sectionForDrop = section;
                    gt.tile.listForm.style.cursor = 'move';
                }
                else {
                    var sectionInner = section.id + 'Inner';
                    if (gt.listFavGrabbedParentId != sectionInner) {
                        gt.tile.listForm.style.cursor = 'not-allowed';
                    }
                    else {
                        gt.tile.listForm.style.cursor = 'move';
                    }
                }
                break;
            }
        }
    }
}

//Returns if the passed in section is a droppable section
CARO.sectionIsDroppable = function (section) {
    var isDroppable = false;
    if (section.id == 'listFav') {
        return true;
    }
    var folderType = document.getElementById('caroContentFavFolder_' + section.userFolderId); //Use the carousel version - Horizontal
    if (folderType) {
        isDroppable = folderType.isUFContent;
    }
    return isDroppable;
}

CARO.isSectionActivated = function (section) {
    var isActive = false;
    var doCheck = section.getAttribute('activatefordrop') ? true : false;
    if (doCheck) {
        isActive = doCheck == 'true' ? true : false;
    }
    return isActive;
}

//Controlling background using css for activation
CARO.activateSectionForDrop = function (section) {
    section.setAttribute('activatefordrop', 'true');
    UTIL.forceRedraw(section);
}

CARO.deactivateSectionForDrop = function (section) {
    section.setAttribute('activatefordrop', 'false');
    UTIL.forceRedraw(section);
}

//Check to see if we activate the tile for dropping into/onto
CARO.checkForDropActivation = function () {
    var gt = CARO.caroMouseState.grabbedTile;
    if (gt && CARO.possibleDropTarget != null) {
        var dt = CARO.possibleDropTarget.tile;
        var dup = CARO.isDuplicateFavByTile(gt.tile.tileForm, dt.tileForm);
        //Check if the tile is still hovering over the droppable target
        //Also if the target tile/folder already contains the grabbed tile (favorite)
        if (CARO.isTileOverFolderTile(gt.tile.tileForm, dt.tileForm, gt.tile.isTile) && !dup) {
            if (dt.tileForm.favInfo.appType == '07') {
                gt.tile.style.cursor = 'not-allowed';
            }
            else {
                CARO.activateDropTarget(CARO.possibleDropTarget.tile.tileForm);
            }
        }
        else {
            CARO.deactivateDropTarget(CARO.possibleDropTarget.tile.tileForm);
            if (dup) {
                gt.tile.style.cursor = 'not-allowed';
            }
        }
    }
}

//Handles styling changes to the tile 
//when it is read for dropping another tile
//into it
CARO.activateDropTarget = function (tile) {
    if (tile) {
        tile.setAttribute('activatefordrop', 'true');
        tile.listForm.setAttribute('activatefordrop', 'true');
        UTIL.forceRedraw(tile.tileForm);
        UTIL.forceRedraw(tile.listForm);
    }

}

//Handles styling changes to the tile 
//when it is read for dropping another tile
//into it
CARO.deactivateDropTarget = function (tile) {
    if (tile) {
        tile.tileForm.setAttribute('activatefordrop', 'false');
        tile.listForm.setAttribute('activatefordrop', 'false');
        CARO.markTileNormal(tile);
        CARO.possibleDropTarget = null;
        UTIL.forceRedraw(tile.tileForm);
        UTIL.forceRedraw(tile.listForm);
    }
}

// Handles updating the UI position of a carousel section during
// dragging and orchestration of the transitions of related 
// carousel sections influenced by the dragging section.
CARO.updatePosForSectionMove = function (event, gs) {
    // Check the UI representation of the section.
    var isTab = gs.section.isTab;
    var isList = gs.section.isList;

    // If we're on a touch-enabled OS, use the current touch coordinates rather than the 
    // mouse coordinates.
    var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);

    // Update the position of the dragging section.
    if (isTab) {
        var minX = CARO.CONSTANTS.TAB_INITIAL_INDENT; // The left-most tab section position.
        var maxX = /*gt.tab.parentNode.actualX + */gs.section.parentNode.offsetWidth - gs.section.offsetWidth; // The right-most tab section position.
        // Set the current position of the dragging element and limit it to the left and right boundaries.
        if (!isRTL) {
            gs.section.style.left = (gs.section.actualX = UTIL.clamp(eClientX + gs.offsetX, minX, maxX)) + 'px';
        }
        else {
            gs.section.style.right = (gs.section.actualX = (UTIL.clamp(maxX - (eClientX + gs.offsetX), minX, maxX))) + 'px';
        }
    }
    else if (isList) {
        var minY = 0; // The top-most list section position.
        var maxY = gs.section.parentNode.draggableHeight; // The bottom-most list section position.
        // Set the current position of the dragging element and limit it to the top and bottom boundaries.
        gs.section.style.top = (gs.section.actualY = UTIL.clamp(eClientY + gs.offsetY, minY, maxY)) + 'px';
    }

    var content = gs.section.parentNode;

    // Check if there are peer sections to either side of the dragging section: before (i.e. left/above) or after (i.e. right/below).

    // Assume there are sections to either side.
    var sectionsBefore = 1;
    var sectionsAfter = 1;
    // Verify if sections exist to either side.
    if (gs.section.slot == 0) {
        sectionsBefore = 0;
    }
    else {
        // We use the count of list elements because there are no "fake" items in that 
        // collection.  All children are proper sections.
        var listContainer = document.getElementById('listContentHolder');
        if (gs.section.slot == listContainer.children.length - 1) {
            sectionsAfter = 0;
        }
    }

    // Adjust the position of the peer sections relative to the dragging section.
    var otherSection;
    var dragOffsetPos = 0;
    var dragOffsetDim = 0;
    var otherOffsetDim = 0;
    var otherLogicalPos = 0;
    if (sectionsBefore) {
        // Get the preceding peer section.
        otherSection = content.children[gs.section.slot - 1];
        if (!otherSection || otherSection.doNotMove) {
            // Nothing to do because the other section is non-movable (immutable) and we 
            // can't cross non-movable sections.
        }
        else {
            // Collect the appropriate dimmensions and positions for the UI representation of the section.
            if (isTab) {
                dragOffsetPos = gs.section.offsetLeft;
                dragOffsetDim = gs.section.offsetWidth;
                otherLogicalPos = otherSection.logicalX;
                otherOffsetDim = otherSection.offsetWidth;
                if (isRTL) {
                    dragOffsetPos = gs.section.parentNode.offsetWidth - (gs.section.offsetLeft + gs.section.offsetWidth);
                }
            }
            else if (isList) {
                dragOffsetPos = gs.section.offsetTop;
                dragOffsetDim = gs.section.offsetHeight;
                otherLogicalPos = otherSection.logicalY;
                otherOffsetDim = otherSection.offsetHeight;
            }
            // Check if the dragging section is overtaking the peer section.
            if (gs.section.slot > otherSection.slot && dragOffsetPos < (otherLogicalPos) + (0.4 * otherOffsetDim)) {
                // If so, move swap the two tab positions.

                // Swap the DOM elements.
                CARO.swapSections(content, gs.section.slot, gs.section.slot - 1);
                // Change the peer section's UI position through animation.
                otherLogicalPos = otherLogicalPos + (dragOffsetDim);
                if (isTab) {
                    otherLogicalPos = otherLogicalPos + CARO.CONSTANTS.TAB_SPACING;
                    otherSection.logicalX = otherLogicalPos;
                }
                else if (isList) {
                    otherSection.logicalY = otherLogicalPos;
                }
                CARO.slideSectionToDesiredPos(otherSection, otherLogicalPos, 0);
                if (isList) {
                    CARO.setListItemStylingAfterMove(gs.section); // Have to account for the rounded corner for the top list item.
                }
                // The swap causes the capturing to be broken in IE, so we must reset capture
                if (gs && gs.section && gs.section.setCapture) {
                    gs.section.setCapture();
                    CARO.carousel.capturingObject = gs.section;
                }
            }
        }
    }
    if (sectionsAfter) {
        // Get the following peer section.
        otherSection = content.children[gs.section.slot + 1];
        if (!otherSection || otherSection.doNotMove) {
            // Nothing to do because the other section is non-movable (immutable) and we 
            // can't cross non-movable sections.
        }
        else {
            // Collect the appropriate dimmensions and positions for the UI representation of the section.
            if (isTab) {
                dragOffsetPos = gs.section.offsetLeft + gs.section.offsetWidth;
                dragOffsetDim = gs.section.offsetWidth;
                otherLogicalPos = otherSection.logicalX;
                otherOffsetDim = otherSection.offsetWidth;
                if (isRTL) {
                    dragOffsetPos = gs.section.parentNode.offsetWidth - gs.section.offsetLeft;
                }
            }
            else if (isList) {
                dragOffsetPos = gs.section.offsetTop + gs.section.offsetHeight;
                dragOffsetDim = gs.section.offsetHeight;
                otherLogicalPos = otherSection.logicalY;
                otherOffsetDim = otherSection.offsetHeight;
            }
            // Check if the dragging section is overtaking the peer section.
            if (gs.section.slot < otherSection.slot && dragOffsetPos > (otherLogicalPos) + (0.6 * otherOffsetDim)) {
                // If so, move swap the two tab positions.

                // Swap the DOM elements.
                CARO.swapSections(content, gs.section.slot, gs.section.slot + 1);
                // Change the peer section's UI position through animation.
                otherLogicalPos = otherLogicalPos - (dragOffsetDim);
                if (isTab) {
                    otherLogicalPos = otherLogicalPos - CARO.CONSTANTS.TAB_SPACING;
                    otherSection.logicalX = otherLogicalPos;
                }
                else if (isList) {
                    otherSection.logicalY = otherLogicalPos;
                }
                CARO.slideSectionToDesiredPos(otherSection, otherLogicalPos, 0);
                if (isList) {
                    CARO.setListItemStylingAfterMove(gs.section); // Have to account for the rounded corner for the top list item.
                }
                // The swap causes the capturing to be broken in IE, so we must reset capture
                if (gs && gs.section && gs.section.setCapture) {
                    gs.section.setCapture();
                    CARO.carousel.capturingObject = gs.section;
                }
            }
        }
    }
    if (isTab) {
        CARO.computeTabPositions(content);
    }
}

// Applies the appropriate visual styling to carousel list sections 
// relative to the section's position in the list.
CARO.setListItemStylingAfterMove = function (item) {
    //All styling applied via CSS states.

    // The first item applies a rounded corner.
    if (item.slot == 0) {
        item.children[0].setAttribute('toplistitem', 'true');
    }
    else {
        // The remaining items don't.
        item.children[0].setAttribute('toplistitem', 'false');
    }
}

// Handles the drop action for dragging carousel sections.
CARO.checkForSectionDrop = function () {
    // Clear the dragging section from the mouse action state.
    var gs = CARO.caroMouseState.grabbedSection;
    CARO.caroMouseState.grabbedSection = null;

    if (gs) {
        // Reset the flag that indicates to the click 
        // event handlers whether the click indicates 
        // a valid section drop occurred.
        //
        // The click handlers take different objects for the click event
        // per UI representation of the section.
        if (gs.section.isList) {
            gs.section.sectionDropOccurred = false;
        }
        else {
            gs.section.parentNode.sectionDropOccurred = false;
        }

        // If the list is in a re-order state, we always
        // set the drop indicator to prevent the section 
        // from collapsing after a non-committed move.
        if (CARO.isListRepositioning()) {
            gs.section.sectionDropOccurred = true;
            //gs.moveCommitted = 1;
        }
        // Clear the UI and state indicators that the section
        // is in a drag state.
        CARO.markSectionNormal(gs.section);
        // If the user actually moved the section, perform
        // any final UI positioning and state management.
        if (gs.moveCommitted && !(gs.maybeSlideCaro)) {
            // Clear the committed move indicator.
            gs.moveCommitted = 0;

            if (gs.section.isList) {
                // List sections only require the drop indicator be 
                // set because the list container reverts back to a 
                // static layout after the drop.
                gs.section.sectionDropOccurred = true;
            }
            else {
                //Slide to final position     
                //based on final slot location
                if (gs.section.slot == 0) {
                    gs.section.logicalX = CARO.CONSTANTS.TAB_INITIAL_INDENT;
                    CARO.slideSectionToDesiredPos(gs.section, gs.section.logicalX, 0);
                }
                else {
                    var sectionToLeft = gs.section.parentNode.children[gs.section.slot - 1];
                    gs.section.logicalX = sectionToLeft.logicalX + (sectionToLeft.offsetWidth + CARO.CONSTANTS.TAB_SPACING);
                    CARO.slideSectionToDesiredPos(gs.section, gs.section.logicalX, 0);
                }

                gs.section.parentNode.sectionDropOccurred = true;

                UTIL.forceRedraw(gs.section); // Because IE does not always immediately apply new style definitions.
            }

            // Since the sections have been re-ordered and are now in final positions, 
            // save the order of the sections back to per-user settings.
            CARO.saveSectionOrder();
        }
        // Ensure list section absolute position values are properly set after the container
        // reverts back to the static layout.
        if (gs.section.isList) {
            CARO.actImmediatelyOnHeightsForListSections();
        }
    }
}

// Saves the order of carousel sections back to the user settings on the 
// server.
CARO.saveSectionOrder = function () {
    // Just check the list view regardless of what's being viewed since
    // the two sections should always be in sync.
    var listContainer = document.getElementById('listContentHolder');
    if (listContainer) {
        CARO.sectionOrderArray = new Array();
        // Get the base ID (the unique portion of the ID minus the leading 
        // representation indicator (i.e. 'list or 'caroTab') for each
        // section in the order they exist in the UI.
        for (var i = 0; i < listContainer.children.length; i++) {
            CARO.sectionOrderArray[i] = listContainer.children[i].id.split('list')[1];
        }
        // Save the setting back to the server.
        CARO.saveTaskFolderSettings();
    }
}

CARO.caroEvaluateWhetherToMoveCarousel = function (event) {
    if (CARO.isMouseOverCarousel(event) ||
        UTIL.isMouseOutsideWindow(event) ||
        (CARO.caroMouseState.grabbedTile && !CARO.caroMouseState.grabbedTile.maybeSlideCaro) ||
        (CARO.caroMouseState.grabbedSection && !CARO.caroMouseState.grabbedSection.maybeSlideCaro)) {
        return;
    }


    var newSide = CARO.caroFindQuadrent(event);

    if (newSide != CARO.carousel.side) {
        CARO.carousel.side = newSide;
        CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.SIDE, CARO.SIDE_TO_SETTING_VALUE[CARO.carousel.side]);
        CARO.carousel.sideStr = CARO.SIDE_TO_CLASSNAME[CARO.carousel.side];
        CARO.carousel.className = CARO.carousel.sideStr + ' ' + CARO.carousel.expandedStr;
        CARO.e1AppFrameContainer.className = CARO.carousel.sideStr + ' ' + CARO.carousel.expandedStr;

        if ((newSide == CARO.EAST) || (newSide == CARO.WEST)) {
            CARO.computeHeightsForListSections();
            CARO.actImmediatelyOnHeightsForListSections();
        }
        else {
            CARO.fixTabAlignmentNow();
        }
        CARO.updateFormDivWidth();
    }
}

CARO.isMouseOverCarousel = function (event) {
    var carousel = CARO.carousel;

    return (CARO.isMouseOverObject(event, carousel) || CARO.isMouseOverTabBar(event));
}

CARO.isMouseOverObject = function (event, obj) {
    /* Only works on top-level objects */
    var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);

    return ((eClientX >= obj.offsetLeft) &&
            (eClientX <= obj.offsetLeft + obj.offsetWidth) &&
            (eClientY >= obj.offsetTop + CARO.CONSTANTS.MENU_TOTAL_HEIGHT) &&
            (eClientY <= obj.offsetTop + CARO.CONSTANTS.MENU_TOTAL_HEIGHT + obj.offsetHeight));
}

//This will check if the tile is over the 
//folder tile based on the tile size and position.
CARO.isTileOverFolderTile = function (gtTile, folderTile, isTile) {
    // If there is no favInfo, folder logic should not apply.
    if (!folderTile.tileForm.favInfo) {
        return false;
    }

    if ((folderTile.tileForm.special == 'manageFavs') ||
        !CARO.isFolder(folderTile.tileForm.favInfo.appType) ||
        CARO.isUserFolder(gtTile.tileForm.favInfo.appType)) {
        return false;
    }

    if (isTile) {
        //Tile moving edges tile form
        var lftEdge_a = gtTile.actualX;
        var rgtEdge_a = lftEdge_a + CARO.CONSTANTS.OFFSET_PER_TILE;
        var btmEdge_a = gtTile.actualY;
        var topEdge_a = btmEdge_a + CARO.CONSTANTS.CARO_HEIGHT;
        //Folder tile edges tile form
        var lftEdge_b = folderTile.actualX;
        var rgtEdge_b = lftEdge_b + CARO.CONSTANTS.OFFSET_PER_TILE;
        var btmEdge_b = folderTile.actualY;
        var topEdge_b = btmEdge_b + CARO.CONSTANTS.CARO_HEIGHT;
    }
    else {
        //Tile moving edges list form
        var gtList = gtTile.listForm;
        var folderList = folderTile.listForm;
        var topEdge_c = gtList.actualY - 2;
        var btmEdge_c = topEdge_c + (CARO.CONSTANTS.LIST_ITEM_HEIGHT - 2);
        //Folder tile edges list form
        var topEdge_d = folderList.actualY - 2;
        var btmEdge_d = topEdge_d + (CARO.CONSTANTS.LIST_ITEM_HEIGHT - 2);
    }
    var onTile = false; //set false as default value 

    // This logic considers the moving tile over a folder if it is mostly
    // over the center area. This allow swapping of tiles to work without having
    // to slide too far past the folder.
    if (isTile) {
        onTile = ((lftEdge_a < (rgtEdge_b - (0.55 * CARO.CONSTANTS.OFFSET_PER_TILE)))
              && (rgtEdge_a > (lftEdge_b + (0.55 * CARO.CONSTANTS.OFFSET_PER_TILE)))
              && (topEdge_a > btmEdge_b)
              && (btmEdge_a < topEdge_b));
    }
    else {
        onTile = ((topEdge_c < (btmEdge_d - (0.55 * CARO.CONSTANTS.LIST_ITEM_HEIGHT)))
              && (btmEdge_c > (topEdge_d + (0.55 * CARO.CONSTANTS.LIST_ITEM_HEIGHT))));
    }


    return onTile;
}

CARO.isMouseOverTab = function (event, obj) {
    if (!event) {
        event = window.event; // Accomodate IE
    }
    //First see if we're over the tabBar
    if (CARO.isMouseOverTabBar(event) && obj) {
        var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);

        if (document.documentElement.dir == "rtl") {
            var rightStyle = obj.style.right;
            var right = rightStyle.substring(0, rightStyle.length - 2);
            return (((document.body.clientWidth - eClientX) >= parseFloat(right)) && ((document.body.clientWidth - eClientX) <= parseFloat(right) + obj.offsetWidth));
        }
        else {
            return ((eClientX >= obj.offsetLeft) && (eClientX <= obj.offsetLeft + obj.offsetWidth));
        }
    }
    return false;
}

CARO.isMouseOverTabBar = function (event) {
    var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);

    var carousel = CARO.carousel;
    if ((carousel.side == CARO.EAST) || (carousel.side == CARO.WEST)) {
        return false;
    }

    var tabBar = document.getElementById('caroTabBar');

    var horizCorrect = ((eClientX >= tabBar.offsetLeft) &&
                        (eClientX <= tabBar.offsetLeft + tabBar.offsetWidth));

    var vertCorrect = (carousel.side == CARO.SOUTH)
    ? ((eClientY >= carousel.offsetTop + CARO.CONSTANTS.MENU_TOTAL_HEIGHT - tabBar.offsetHeight) &&
       (eClientY <= carousel.offsetTop + CARO.CONSTANTS.MENU_TOTAL_HEIGHT))
    : ((eClientY >= CARO.CONSTANTS.MENU_TOTAL_HEIGHT + carousel.offsetHeight) &&
       (eClientY <= CARO.CONSTANTS.MENU_TOTAL_HEIGHT + carousel.offsetHeight + tabBar.offsetHeight));

    return (horizCorrect && vertCorrect);
}

CARO.caroFindQuadrent = function (event) {
    var holder = document.getElementById('caroHolder');
    var hy1 = holder.offsetTop;
    var hy2 = holder.offsetTop + holder.offsetHeight;
    var hx1 = holder.offsetLeft;
    var hx2 = holder.offsetLeft + holder.offsetWidth;

    var x = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var y = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);

    var above_LL_to_UR_diagonal = (y < (((hy1 - hy2) / (hx2 - hx1)) * (x - hx1) + hy2));
    var above_UL_to_LR_diagonal = (y < (((hy2 - hy1) / (hx2 - hx1)) * (x - hx1) + hy1));

    if (above_LL_to_UR_diagonal) {
        if (above_UL_to_LR_diagonal) {
            return CARO.NORTH;
        }
        else {
            return CARO.WEST;
        }
    }
    else {
        if (above_UL_to_LR_diagonal) {
            return CARO.EAST;
        }
        else {
            return CARO.SOUTH;
        }
    }
}

CARO.updateOCL = function (oclData) {
    if (isNativeContainer) {
        if (NativeContainer) {
            NativeContainer.updateData(NativeContainer.PARAM_UPDATE_DATA_OPENAPPS);
        }
    }

    if (!CARO.caroInitFinished) {
        /* There is a race condition because this code is called by a script in 
        another iframe.  Depending on the browser and other variables, it may
        run before, during, or after CARO.caroInit runs.  Therefore, if caroInit 
        is not completely finished, we will perform the essential prerequisites 
        here.  */

        CARO.USE_LARGE_ICONS = (window.largeIconsInCarousel == "true") ? 1 : 0;
        CARO.redefineTileSizeDependantConstants();
    }

    var needDel = false;
    var needAdd = false;
    var doc = document;

    if (oclData) {
        var caroOCL = doc.getElementById('caroContentOCL');
        var listOCL = doc.getElementById('listOCLInner');

        if (!caroOCL) {
            // Safety net - shouldn't happen.
            return;
        }

        var OCLStartsEmpty = (caroOCL.childNodes.length == 0);

        for (var i = 0; i < caroOCL.childNodes.length; i++) {
            caroOCL.childNodes[i].keepMe = false;
        }

        for (var i = oclData.childNodes.length - 1; i >= 0; i--) {
            var node = oclData.childNodes[i];
            var id = node.getAttribute('oclid');
            var label = node.getAttribute('label');
            var appID = node.getAttribute('appID');
            var versID = node.getAttribute('versID');
            var sysCode = node.getAttribute('sysCode');
            if (versID == 'null') {
                versID = '';
            }
            if (!appID || appID == 'null') {
                appID = label;
            }
            var formID = node.getAttribute('formID');
            if (!formID || formID == 'null') {
                formID = label;
            }
            var iconFile = node.getAttribute('iconFile');
            var iconExt = node.getAttribute('iconExtension');

            // Test for this entry already to exist.
            var tile = doc.getElementById('caroOCL_' + id);
            var listElem = doc.getElementById('listOCL_' + id);
            if (tile) {
                tile.keepMe = true;
                // Above line could be moved after the if/else, but included
                // in both clauses for improved readability.
            }
            else {
                tile = CARO.createTile(id, CARO.TILE_TYPES.OCL);
                tile.compid = id;
                tile.label = label;
                tile.appID = appID;
                tile.formID = formID;
                tile.versID = versID;
                tile.sysCode = sysCode
                //Set droppable - Organize favorites (This means they cannot have tiles dropped into them)
                tile.dropCapable = false;

                CARO.silentAddTile(caroOCL, tile);

                tile.isNew = true;
                tile.keepMe = true;

                listElem = tile.listForm;

                CARO.silentAddTile(listOCL, tile.listForm);
                // listOCL.appendChild(tile.listForm);

                /* If this new tile corresponds to a newly launched app or is otherwise 
                * the active tile, mark it so. */
                if (jdeMenuParent && (jdeMenuParent.currCompId == id)) {
                    CARO.setActiveAppTile(id);
                }

                needAdd = true;
            }

            tile.setAttribute('specialIcon', iconFile);
            var imageUrlLarge = JDE_ICON_PREFIX + iconFile + JDE_ICON_SUFFIX.LARGE + iconExt;
            var imageUrlMedium = JDE_ICON_PREFIX + iconFile + JDE_ICON_SUFFIX.MEDIUM + iconExt;
            var imageUrlSmall = JDE_ICON_PREFIX + iconFile + JDE_ICON_SUFFIX.SMALL + iconExt;

            tile.imgUrlLarge = imageUrlLarge;
            tile.imgUrlMed = imageUrlMedium;

            CARO.setTileImage(tile, (CARO.USE_LARGE_ICONS) ? imageUrlLarge : imageUrlMedium);

            var icon = listElem.icon;
            icon.src = imageUrlSmall;
            UTIL.forceRedraw(icon);
            UTIL.forceRedraw(tile.children[0]);

            /* If there is only one tile, it must be the home tile, and by virtue of 
            being the only tile, it must be the active tile. 
            
            Also, check for CHP to be active in iFrame
            */
            if ((oclData.childNodes.length == 1) || (jdeMenuParent && (jdeMenuParent.currCompId == 0))) {
                CARO.setActiveAppTile(0);
            }

            // Make sure the title is set and set correctly.  Unlike compid, the label can change 
            // over time.  For instance, "Opening Application..." becomes "Work With Servers" 
            // becomes "Work With Servers 1" becomes "Submitted Job Search" etc.
            tile.label = label;
            tile.formID = formID;
            tile.appID = appID;
            tile.versID = versID;
            tile.sysCode = sysCode;

            var active = (listElem.tileForm.getAttribute('isActiveTile') == 'true');

            // There has been a change in text - need to update caption:
            CARO.labelTileAs(tile, label, appID, formID, active);
            UTIL.forceRedraw(tile.caption);
        }

        for (var i = caroOCL.childNodes.length - 1; i >= 0; i--) {
            if (!caroOCL.childNodes[i].keepMe) {
                needDel = true;
                caroOCL.childNodes[i].ignoreForSpacing = true;
                CARO.fadeAndDelTileByIndex(caroOCL, i);
                CARO.fadeAndDelTileByIndex(listOCL, i);
            }
        }

        CARO.setDesiredPositions(caroOCL);
        CARO.setDesiredPositionsList(listOCL);

        var delayToRearrange = (needDel) ? (4 / 5 * CARO.CONSTANTS.TILE_FADE_DURATION) : 0;
        var delayToFadeInNew = delayToRearrange;
        var delayToFadeInNewList = delayToRearrange;

        for (var i = 0; i < caroOCL.childNodes.length; i++) {
            if (caroOCL.childNodes[i].isNew) {
                // Initialize actual pos to desired pos for the new nodes
                caroOCL.childNodes[i].style.left = (caroOCL.childNodes[i].actualX = caroOCL.childNodes[i].desiredX) + 'px';
                listOCL.childNodes[i].style.top = (listOCL.childNodes[i].actualY = listOCL.childNodes[i].desiredY) + 'px';
            }
        }

        // If carousel OCL was empty to start, snap it to desired position
        if (OCLStartsEmpty) {
            caroOCL.style.left = (caroOCL.actualX = caroOCL.desiredX) + 'px';
        }

        if (CARO.desiredPositionsDifferFromActual(caroOCL)) {
            delayToFadeInNew += CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TILES;
            CARO.slideToDesiredPositions(caroOCL, delayToRearrange);
        }

        if (CARO.desiredPositionsDifferFromActualList(listOCL)) {
            delayToFadeInNewList += CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST;
            CARO.slideToDesiredPositionsList(listOCL, delayToRearrange);
        }

        if (needDel) {
            setTimeout('CARO.slideIfNecessary(document.getElementById(\'' + listOCL.id + '\'))', delayToFadeInNewList);
        }

        if (needAdd) {
            for (var i = 0; i < caroOCL.childNodes.length; i++) {
                if (caroOCL.childNodes[i].isNew) {
                    ANIM.fadeObj(caroOCL.childNodes[i], 0, 1, CARO.CONSTANTS.TILE_FADE_DURATION, delayToFadeInNew, true);
                    ANIM.fadeObj(caroOCL.childNodes[i].listForm, 0, 1, CARO.CONSTANTS.TILE_FADE_DURATION, delayToFadeInNewList, true);
                    caroOCL.childNodes[i].isNew = false;
                }
            }
        }
    }

    CARO.initializedOCL = true;

    if (CARO.activeCaro == caroOCL) {
        CARO.decideWhetherToHaveArrowsCaro();
    }
    CARO.decideOnPanArrowsForAllLists();
}

/* The following line provides a failsafe against the case in 
which the top level object "myFravoriteObject" does not exist. */
var myFavoriteObject;
if (!myFavoriteObject) {
    myFavoriteObject = new Object();
    myFavoriteObject.favorites = [];
}

CARO.createUrlString = function (imgPath) {
    if ((parent) && (parent._e1URLFactory)) {
        var e1UrlCreator = parent._e1URLFactory.createInstance(parent._e1URLFactory.URL_TYPE_RES);
        e1UrlCreator.setURI(imgPath);
        return e1UrlCreator.toString();
    }
    else {
        // If we cannot find the url creator, we will have to make our best guess:
        return '/jde' + ((imgPath.substring(0, 1) == '/') ? '' : '/') + imgPath;
    }
}

CARO.setDesiredPositions = function (content) {
    if (!content) {
        return; //If invalid content just return
    }

    var numIgnored = 0;

    CARO.computeContentDesiredPos(content);

    for (var i = 0; i < content.childNodes.length; i++) {
        var tile = content.childNodes[i];
        if (tile.ignoreForSpacing) { numIgnored++; }
        tile.slot = i - numIgnored;
        if (tile.orderOverride) {
            tile.desiredX = CARO.CONSTANTS.OFFSET_PER_TILE * tile.orderOverride;
        }
        else {
            tile.desiredX = CARO.CONSTANTS.OFFSET_PER_TILE * (i - numIgnored);
        }
    }
}

CARO.setDesiredPositionsList = function (content) {

    if (!content) {
        return;
    }

    var offsetSoFar = 0;

    /* TODO: can refactor the two for loops */

    if (content.listOrderBackwards) {
        // Loop backwards because we want newest OCL items at the top.
        for (var i = content.childNodes.length - 1; i >= 0; i--) {
            var listElem = content.childNodes[i];
            if (!listElem.tileForm.ignoreForSpacing) {
                listElem.desiredY = offsetSoFar;

                offsetSoFar += CARO.CONSTANTS.LIST_ITEM_HEIGHT;
            }
        }
    }
    else {
        for (var i = 0; i < content.childNodes.length; i++) {

            var listElem = content.childNodes[i];
            if (!listElem.tileForm.ignoreForSpacing) {
                if (listElem.tileForm.orderOverride) {
                    listElem.desiredY = listElem.tileForm.orderOverride * CARO.CONSTANTS.LIST_ITEM_HEIGHT
                }
                else {
                    listElem.desiredY = offsetSoFar;
                }
                offsetSoFar += CARO.CONSTANTS.LIST_ITEM_HEIGHT;
            }
        }
    }

    // offsetSoFar is now the total height.

    content.overrideHeight = offsetSoFar;
    content.desiredHeight = offsetSoFar;

    if (CARO.isHorizontal() || CARO.suspendListResizing) {
        // Snap to correct size.
        content.style.height = offsetSoFar + 'px';
    }
}

CARO.computeContentDesiredPos = function (content) {
    if (!content) {
        return; //If content comes in invalid just return
    }

    if (!content.getWidth) {
        if (!CARO.alreadyInit) {
            CARO.caroInit();
        }
        else {
            return;
        }
    }

    var wPage = UTIL.pageWidth();
    var wCaro = content.getWidth();

    var oldWidth = content.lastWidth;
    content.lastWidth = wCaro;

    if ((!oldWidth) || (oldWidth < CARO.CONSTANTS.TILE_SIZE)) {
        /* carousel was previously empty.  
        *  - if there's room for everything, center it
        *  - if there's not room, prioritize left edge.
        */
        if (wPage > wCaro) {
            content.desiredX = (wPage - wCaro) / 2;
        }
        else {
            content.desiredX = 0;
        }
    }
    else {
        if (wCaro < wPage) {
            if (content.actualX + oldWidth >= wPage - 1) {
                // Against right edge
                content.desiredX = wPage - wCaro
            }
            else if (content.actualX < 1) {
                // Against left edge
                content.desiredX = content.actualX
            }
            else {
                // Somewhere in the middle -- try to preserve same center
                content.desiredX = content.actualX - (wCaro - oldWidth) / 2
            }
        }
        else {
            // Caro is too big - try to preserve same center.
            content.desiredX = content.actualX - (wCaro - oldWidth) / 2
        }
    }

    content.computeMinMaxX();

    // Make sure we're only sticking things off the edge if we can't avoid it 
    // and, in that case, that we are using the full space for something.

    content.desiredX = UTIL.clamp(content.desiredX, content.minX, content.maxX);

    return;
}

CARO.desiredPositionsDifferFromActual = function (content) {
    if (content.desiredX != content.actualX) {
        return true;
    }

    for (var i = 0; i < content.childNodes.length; i++) {
        if (content.childNodes[i].desiredX != content.childNodes[i].actualX) {
            return true;
        }
    }

    return false; // No discrepencies found.
}

CARO.desiredPositionsDifferFromActualList = function (content) {
    // Check height.
    if (content.parentNode.offsetHeight != content.desiredHeight) {
        return true;
    }

    // TODO: May need to be able to scroll list to show new entries.  Possibly employ iphone logic.

    for (var i = 0; i < content.childNodes.length; i++) {
        if (content.childNodes[i].desiredY != content.childNodes[i].actualY) {
            return true;
        }
    }

    return false; // No discrepencies found.
}

CARO.slideToDesiredPositions = function (content, delay) {
    if (!content.actualX) {
        content.style.left = (content.actualX = content.desiredX) + 'px';
    }
    if (content.actualX != content.desiredX) {
        CARO.slideTileToDesiredPos(content, delay)
    }

    for (var i = 0; i < content.childNodes.length; i++) {
        if ((!(content.childNodes[i].ignoreForSpacing)) && (content.childNodes[i].desiredX != content.childNodes[i].actualX)) {
            CARO.slideTileToDesiredPos(content.childNodes[i], delay);
        }
    }
}

CARO.snapToDesiredPositions = function (content) {
    content.style.left = (content.actualX = content.desiredX) + 'px';

    for (var i = 0; i < content.childNodes.length; i++) {
        content.childNodes[i].style.left = (content.childNodes[i].actualX = content.childNodes[i].desiredX) + 'px';
    }
}

CARO.slideToDesiredPositionsList = function (content, delay) {
    if (!content) {
        return;
    }

    if (content.parentNode.parentNode.expanded) {
        CARO.computeHeightsForListSections();
        CARO.actGraduallyOnHeightsForListSections(0, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST, delay);
    }
    else {
        // if collapsed, snap to desired size
        if (content.desiredHeight) {
            content.style.height = (content.actualY = content.desiredHeight) + 'px';
        }
    }

    if (content.desiredX) {
        if (!content.actualY) {
            content.style.top = (content.actualY = content.desiredY) + 'px';
        }
        if (content.actualY != content.desiredY) {
            content.isListElem = true;
            CARO.slideTileToDesiredPos(content, delay)
        }
    }


    for (var i = 0; i < content.childNodes.length; i++) {
        if ((!(content.childNodes[i].ignoreForSpacing)) && (content.childNodes[i].desiredY != content.childNodes[i].actualY)) {
            CARO.slideTileToDesiredPos(content.childNodes[i], delay);
        }
    }
}

CARO.slideTileToDesiredPos = function (tile, delay, durationOverride) {
    if (!delay) delay = 0;

    var duration = durationOverride || ((tile.isTile) ? CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TILES : CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST);

    if (tile.isListElem) {
        // Just need to worry about Y dimmension
        ANIM.animate(tile, "sinSlideY", tile.actualY, tile.desiredY, duration, delay)

        return;
    }

    if (!tile.actualY) {
        // If the tile is at 0 vertically, we can just worry about the X dimmension
        ANIM.animate(tile, "sinSlideX", tile.actualX, tile.desiredX, duration, delay);
    }
    else {
        // We have to handle the y-dimension as well
        ANIM.animate(tile, "sinSlideXY", tile.actualX, tile.desiredX, duration, delay);
    }
}

// Moves the location of a carousel section to the specified
// position via a slide animation.
CARO.slideSectionToDesiredPos = function (section, toPos, delay) {
    if (!delay) {
        delay = 0;
    }

    var duration = CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_SECTION;

    if (section.isTab) {
        if (!isRTL) {
            ANIM.animate(section, "sinSlideX", section.offsetLeft, toPos, duration, delay);
        }
        else {
            ANIM.animate(section, "sinSlideXright", section.parentNode.offsetWidth - (section.offsetLeft + section.offsetWidth), toPos, duration, delay);
        }
    }
    else if (section.isList) {
        ANIM.animate(section, "sinSlideY", section.offsetTop, toPos, duration, delay);
        CARO.setListItemStylingAfterMove(section);
    }
}

CARO.silentAddTile = function (content, tile, position) {
    tile.style.opacity = 0;
    tile.style.filter = 'alpha(opacity=0)';

    content.appendChild(tile);
}

CARO.moveTileToNewContent = function (toContent, tile) {
    var otherFormContent = null;
    var doc = document;
    if (tile.isTile) {
        if (toContent.id == 'caroContentFav') {
            otherFormContent = doc.getElementById('listFavInner');
        }
        else {
            otherFormContent = doc.getElementById('listFavFolder_' + toContent.id.substring(21) + 'Inner');
        }
    }
    else {
        if (toContent.id == 'listFavInner') {
            otherFormContent = doc.getElementById('caroContentFav');
        }
        else {
            otherFormContent = doc.getElementById('caroContentFavFolder_' + toContent.id.substring(21));
        }
    }
    //Move the tile 
    toContent.appendChild(tile);
    UTIL.forceRedraw(toContent);

    if (otherFormContent) {
        //Move the otherForm
        otherFormContent.appendChild(tile.otherForm);
        if (tile.isTile) {
            CARO.setDesiredPositionsList(otherFormContent);
            CARO.setDesiredPositions(toContent);
        }
        else {
            CARO.setDesiredPositions(otherFormContent);
            CARO.setDesiredPositionsList(toContent);
        }
        UTIL.forceRedraw(otherFormContent);
    }
}

CARO.fadeAndDelTileByIndex = function (content, index) {
    var callback = "CARO.delTileByIndex(document.getElementById('" + content.getAttribute('id') + "'), " + index + ")";
    ANIM.animate(content.childNodes[index], 'fade', 1, 0, CARO.CONSTANTS.TILE_FADE_DURATION, 0, callback);
}

CARO.delTileByIndex = function (content, index) {
    content.removeChild(content.childNodes[index]);
}

CARO.mouseDownOnTile = function (event) {
    CARO.endDetailHoverMode();
    if (CARO.caroMouseState.mouseIsDown) {
        // We think the mouse is already down, which means somehow the mouseup got lost.  
        // Ignore the second mousedown.
        return;
    }

    CARO.caroMouseState.mouseIsDown = true;
    this.parentNode.CMID++; // Stops any current motion.
    if (!event) event = window.event; // Accomodate IE

    if (this.setCapture && !CARO.carousel.capturingObject) {
        // Needed in IE to accomodate dragging outside window.
        this.setCapture();
        CARO.carousel.capturingObject = this;
    }

    if (!this.clickIsOnCaption) {
        CARO.markTileActive(this);
    }

    if (event.preventDefault) {
        // Prevents trying to select when dragging in firefox and chrome
        event.preventDefault();
    }
    else {
        // Should prevent selection in IE but does not seem to work reliably
        event.returnValue = false;
    }

    var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
    var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);

    var gt = (CARO.caroMouseState.grabbedTile = new Object());
    gt.tile = this;
    gt.indexWhenGrabbed = this.tileForm.slot;
    gt.grabX = eClientX;
    gt.grabY = eClientY;

    gt.contentWhenGrabbed = gt.tile.parentNode.id; //Organize favorite tracking of original content location

    gt.offsetX = this.actualX - eClientX;
    gt.offsetY = this.actualY - eClientY;

    if (!this.parentNode.actualX) { this.parentNode.actualX = 0; }
    if (!this.parentNode.actualY) { this.parentNode.actualY = 0; }
    gt.wholeCaroOffsetX = this.parentNode.actualX - eClientX;
    gt.wholeCaroOffsetY = this.parentNode.actualY - eClientY;

    gt.maybeMoveTile = 1;
    gt.maybeSlideCaro = 1;
    gt.maybeActivateTile = 1;

    if (this.clickIsOnCaption) {
        gt.maybeMoveTile = 0;
        gt.maybeSlideCaro = 0;
        gt.maybeActivateTile = 0;
    }

    this.clickIsOnCaption = 0;

    // If we don't have (non-trivial) motion within the following threshold, than we aren't moving 
    // the whole carousel -- we're moving a single tile.  
    setTimeout("CARO.decideOnFullSlide()", CARO.CONSTANTS.MIN_TIME_TO_MOVE_FOR_FULL_CARO_SLIDE);
}

// Handles the mouse down event on a carousel section.
CARO.mouseDownOnSection = function (event) {
    var obj = this;

    CARO.endDetailHoverMode();
    if (CARO.caroMouseState.mouseIsDown) {
        // We think the mouse is already down, which means somehow the mouseup got lost.  
        // Ignore the second mousedown.
        return;
    }

    CARO.caroMouseState.mouseIsDown = true;
    if (!event) {
        event = window.event; // Accomodate IE
    }

    if (this.setCapture && !CARO.carousel.capturingObject) {
        // Needed in IE to accomodate dragging outside window.
        this.setCapture();
        CARO.carousel.capturingObject = this;
    }

    if (event.preventDefault) {
        // Prevents trying to select when dragging in firefox and chrome
        event.preventDefault();
    }
    else {
        // Should prevent selection in IE but does not seem to work reliably
        event.returnValue = false;
    }

    if (obj.doNotMove) {
        // The section cannot be moved or modified so don't do anything further.
    }
    else {
        // We have a draggable section.

        this.parentNode.CMID++; // Stops any current motion.
        // Store the dragging state information in the mouse state.
        var gs = (CARO.caroMouseState.grabbedSection = new Object());
        var t = (new Date()).getTime();
        if (CARO.caroMouseState.grabStartTime == 0) {
            CARO.caroMouseState.grabStartTime = t;
        }

        // If on a touch-enabled OS, use touch coordinates rather than mouse coordinates.
        var eClientX = (CARO.isTouchEnabled ? event.touches[0].clientX : event.clientX);
        var eClientY = (CARO.isTouchEnabled ? event.touches[0].clientY : event.clientY);

        // Store the appropriate DOM element representing the section in the 
        // drag state for the section's UI representation.
        if (obj.isTab) {
            gs.section = obj;
        }
        else {
            gs.section = obj.parentNode;
        }
        // Capture the rest of the drag state information.
        gs.indexWhenGrabbed = gs.section.slot;
        gs.grabX = eClientX;
        gs.grabY = eClientY;
        gs.offsetX = gs.section.offsetLeft - eClientX;
        gs.offsetY = gs.section.offsetTop - eClientY;
        gs.markedToMove = 0;
        gs.moveCommitted = 0;
        gs.maybeSlideCaro = 1;

        // Resume mouse handling on a different thread.
        setTimeout("CARO.decideOnFullSlide()", CARO.CONSTANTS.MIN_TIME_TO_MOVE_FOR_FULL_CARO_SLIDE);
    }
}

CARO.getContentWidth = function (content) {
    if (!content) content = this;

    var width = CARO.CONSTANTS.EXTRA_WIDTH_FOR_SHADOW;
    for (var i = 0; i < content.childNodes.length; i++) {
        if (!content.childNodes[i].ignoreForSpacing) {
            width += (CARO.CONSTANTS.OFFSET_PER_TILE);
        }
    }
    return width;
}

CARO.getMouseVelocity = function () {
    var curT = (new Date()).getTime();
    var curX = CARO.caroMouseState.histX[0];
    var curY = CARO.caroMouseState.histY[0];

    var index;
    for (index = 0; index < CARO.caroMouseState.histT.length - 1; index++) {
        if ((curT - CARO.caroMouseState.histT[index]) >= CARO.CONSTANTS.TIME_INC_FOR_MOUSE_VELOCITY) {
            break;
        }
    }

    var oldT = CARO.caroMouseState.histT[index];
    var oldX = CARO.caroMouseState.histX[index];
    var oldY = CARO.caroMouseState.histY[index];

    var dt = curT - oldT;

    var velocity = new Object();
    velocity.x = 1000 * (curX - oldX) / dt;
    velocity.y = 1000 * (curY - oldY) / dt;

    return velocity;
}

// v = velocity, a = acceleration (0.5 < a < 2.0)
CARO.caroMotion = function (id, x, v, a) {
    if (!id) {
        // Safety
        return;
    }

    if (v == 0) {
        // This avoids numerous singularities and special cases.
        v = 0.01;
    }
    var content = document.getElementById(id);
    if (!content.horizontal) {
        if (content.computeMinMaxYList) {
            content.computeMinMaxYList();
        }
    }
    if (!content.CMID) { content.CMID = 0; /* Failsafe */ }
    if (a && a >= 0.5 && a <= 2.0) {
        v = v * a;
    }
    ANIM.isAnimating = true;
    CARO.caroMotionInner(id, x, v, (new Date()).getTime(), 0, ++content.CMID, null);
}

CARO.caroMotionInner = function (id, x0, v0, t0, tMin, cmid, tTotal) {
    var caroContent = document.getElementById(id);

    if ((!caroContent) || (caroContent.CMID != cmid)) {
        return;
    }

    var now = (new Date()).getTime();
    var t = (now - t0) / 1000;

    var done = 0;

    if (now >= tMin) {
        K_SLIDING = 80 * UTIL.sign(v0);
        K_FLUID = 1.5;

        var k = K_FLUID;
        var c = K_SLIDING / K_FLUID;

        tMin = now + 16;
        if (!tTotal) {
            // Log isn't free
            tTotal = 1 / k * Math.log((v0 + c) / c);
        }
        done = (v0 == 0) || (t > tTotal);

        if (done) {
            t = tTotal;
        }

        // If v0 was exactly 0, then sign(v0) = 0, so c = 0, meaning tTotal = NaN, and done = true, so t = NaN.  In this case, set t to 0 instead so x is not NaN
        if (!t) t = 0;

        var x = x0 + (-1 / k) * (v0 + c) * (Math.exp(-k * t) - 1) - c * t;

        if (caroContent.vertical) {
            // The x is really a y in this case
            caroContent.style.top = (caroContent.actualY = x) + 'px';
            CARO.decideWhetherToHaveArrowsList(caroContent);
        }
        else {
            caroContent.style.left = (caroContent.actualX = x) + 'px';
            CARO.decideWhetherToHaveArrowsCaro();
        }

        if (caroContent.id == "tabBar") {
            CHP.panArrowStatus();
        }

        var v;
        if (x < caroContent.minX) {
            done = 1;
            v = (v0 + c) * Math.exp(-k * t) - c;
            CARO.caroSpringMotion(id, x, caroContent.minX, v, (new Date()).getTime(), 0, cmid);
        }

        if (x > caroContent.maxX) {
            done = 1;
            v = (v0 + c) * Math.exp(-k * t) - c;
            CARO.caroSpringMotion(id, x, caroContent.maxX, v, (new Date()).getTime(), 0, cmid);
        }
    }

    if (!done) {
        var expr = 'CARO.caroMotionInner(' + ["'" + id + "'", x0, v0, t0, tMin, cmid, tTotal].join(', ') + ')';
        now = (new Date()).getTime();
        setTimeout(expr, tMin - now);
    }
    else {
        ANIM.isAnimating = false;
    }
}

CARO.caroSpringMotion = function (id, x0, xF, v0, t0, tMin, cmid) {
    var caroContent = document.getElementById(id);

    if ((!caroContent) || (cmid != caroContent.CMID)) {
        return;
    }

    var now = (new Date()).getTime();

    var done = 0;

    if (now > tMin) {
        tMin = now + 16;

        var t = (now - t0) / 1000;

        /*
        * x(t) = e^(-kt) * (c1 + c2 * t)
        *
        *   c1 = x0 - xF
        *   c2 = c1 + v0
        */

        var k = 6;
        c1 = x0 - xF;
        c2 = c1 + v0;

        var x = xF + Math.exp(-1 * k * t) * ((c1) + (c2) * t);

        if (Math.abs(xF - x) < 0.5) {
            if (Math.abs(Math.exp(-1 * k * t) * (-k * c1 + (1 - k * t) * c2)) < 2) {
                x = xF;
                if (caroContent.vertical) {
                    CARO.decideWhetherToHaveArrowsList(caroContent);
                }
                else {
                    CARO.decideWhetherToHaveArrowsCaro();
                }
                done = 1;
            }
        }

        if (caroContent.vertical) {
            // The x is really a y in this case
            caroContent.style.top = (caroContent.actualY = x) + 'px';
        }
        else {
            caroContent.style.left = (caroContent.actualX = x) + 'px';
        }

        if (((x > xF) && (xF > x0)) ||
            ((x < xF) && (xF < x0))) {
            // We passed the end -- return to regular motion
            var v = Math.exp(-1 * k * t) * (-k * c1 + (1 - k * t) * c2);
            CARO.caroMotion(id, x, v);
            done = 1;
        }
    }

    if (!done) {
        var expr = 'CARO.caroSpringMotion(' + ["'" + id + "'", x0, xF, v0, t0, tMin, cmid].join(', ') + ')';
        now = (new Date()).getTime();
        setTimeout(expr, tMin - now);
    }
}

CARO.onResize = function () {
    reSpaceTopBar();

    var oldWidth = CARO.oldPageWidth;
    var newWidth = CARO.oldPageWidth = UTIL.pageWidth();

    var caroContentHolder = document.getElementById('caroContentHolder');
    for (var i = 0; i < caroContentHolder.children.length; i++) {
        var content = caroContentHolder.children[i];

        if (content.isAScroller) {
            // List scroller, not a real content.
            continue;
        }

        if (content.id) // Text Nodes (whitespace in the code) will have no id
        {
            // Don't bother with positioning unless width is at least one tile
            if (content.getWidth() >= CARO.CONSTANTS.TILE_SIZE) {
                if (Math.abs(content.actualX) <= 1) {
                    // We're left-aligned.
                    content.desiredX = content.actualX;
                }
                else if (Math.abs(content.actualX - (oldWidth - content.getWidth())) <= 1) {
                    // We're right-aligned.
                    content.desiredX = content.actualX + (newWidth - oldWidth);
                }
                else {
                    // We're somewhere in the middle.  Find the point of the carousel 
                    // that's in the center of the space, and make that fixed as the
                    // center point.  

                    var caroCenterPoint = oldWidth / 2 - content.actualX;
                    content.desiredX = newWidth / 2 - caroCenterPoint;
                }

                content.computeMinMaxX();
                content.desiredX = UTIL.clamp(content.desiredX, content.minX, content.maxX);
                if (isNaN(content.desiredX)) { content.desiredX = 0; }
                content.style.left = (content.actualX = content.desiredX) + 'px';
            }
        }
    }

    CARO.computeHeightsForListSections();
    CARO.actImmediatelyOnHeightsForListSections();
    CARO.decideWhetherToHaveArrowsCaro();

    CARO.fixTabAlignmentNow();

    try {
        if (window.WLIST) {
            if (WLIST.menuContent) {
                if (WLIST.getMenuContent().scrollHeight > 0) {
                    WLIST.getMenuContent().customScrollLogic();
                }
            }
        }
        if (window.CHP_SP) {
            if (CHP_SP.onResize) {
                CHP_SP.onResize();
            }
        }
    }
    catch (excep) {
        // Do nothing
    }
}

CARO.contentComputeMinMaxX = function () {
    var wPage = UTIL.pageWidth();
    var wCaro = this.getWidth();

    var lAligned = 0;
    var rAligned = wPage - wCaro;

    this.minX = Math.min(lAligned, rAligned);
    this.maxX = Math.max(rAligned, lAligned);

    this.horizontal = true;
}

CARO.contentComputeMinMaxYList = function () {
    var hAvailable = this.desiredHeight || this.clientHeight;
    var hSection = this.parentNode.clientHeight;

    var tAligned = 0;
    var bAligned = hSection - hAvailable;

    this.minX = Math.min(tAligned, bAligned);
    this.maxX = Math.max(tAligned, bAligned);

    /* Really, these attributes are y-values, no x-values, but we're keeping the
    x name because there are already some external clients to the caroMotion
    API, so we don't want to break the interface. */

    this.vertical = true;
}

CARO.markTileMoveable = function (obj) {
    obj.setAttribute('moveable', 'true');
    obj.style.filter = "alpha(opacity=50)"; // Can't control via class in IE
    UTIL.forceRedraw(obj);
}
CARO.markTileImmoveable = function (obj) {
    obj.setAttribute('immoveable', 'true');
    UTIL.forceRedraw(obj);
}
CARO.markTileNormal = function (obj) {
    obj.setAttribute('active', 'false');
    obj.setAttribute('moveable', 'false');
    obj.setAttribute('immoveable', 'false');
    obj.style.filter = "alpha(opacity=100)"; // Can't control via class in IE
    UTIL.forceRedraw(obj);
}
CARO.markTileActive = function (obj) {
    obj.setAttribute('active', 'true');
    UTIL.forceRedraw(obj);
}

// Applies apprioriate styling and state 
// to the provided section to indicate 
// the section is in a drag state.
CARO.markSectionMoveable = function (obj) {
    obj.setAttribute('moveable', 'true');
    obj.style.filter = 'alpha(opacity=50)'; // Can't control via class in IE
    var listContainer = document.getElementById('listContentHolder');
    // This will place the list container in an absolute layout state.
    listContainer.setAttribute('positioning', 'true');
    UTIL.forceRedraw(listContainer); //  // Because IE does not always immediately apply new style definitions.
    UTIL.forceRedraw(obj);
}

// Applies apprioriate styling and state 
// to the provided section to indicate 
// the section is not draggable.
CARO.markSectionImmoveable = function (obj) {
    obj.setAttribute('immoveable', 'true');
    UTIL.forceRedraw(obj);
}

// Applies apprioriate styling and state 
// to the provided section to indicate 
// the section is in a neutral (default) state.
CARO.markSectionNormal = function (obj) {
    obj.setAttribute('moveable', 'false');
    obj.setAttribute('immoveable', 'false');
    if (obj.isList) {
        obj.style.filter = 'alpha(opacity=100)'; // Can't control via class in IE
    }
    else {
        obj.style.filter = '-';
    }
    var listContainer = document.getElementById('listContentHolder');
    // This will place the list container in an static layout state (default).
    // This makes the expanding/collapsing of sections significantly easier to 
    // manage.
    listContainer.setAttribute('positioning', 'false');
    UTIL.forceRedraw(listContainer);  // Because IE does not always immediately apply new style definitions.
    UTIL.forceRedraw(obj);
}

// Returns true if the carousel list container is 
// in the middle of a section drag-and-drop operation.
CARO.isListRepositioning = function () {
    var listContainer = document.getElementById('listContentHolder');
    if (listContainer.getAttribute('positioning') == 'true') {
        return true;
    }
    else {
        return false;
    }
}

CARO.setActiveAppTile = function (compId) {
    var doc = document;
    if (true) {
        /* Bulletproofing: if we couldn't find a specific last active tile, 
        go through every tile and mark it inactive. */

        var OCL = doc.getElementById('caroContentOCL');
        var oldTile;

        if (OCL != null && OCL.children.length > 1) {
            for (var i = 0; i < OCL.children.length; i++) {
                if (OCL.children[i].compid != compId) {
                    oldTile = OCL.children[i];
                    if (oldTile && oldTile.getAttribute('isActiveTile') == 'true') {
                        oldTile.setAttribute("isActiveTile", "");
                        oldTile.listForm.setAttribute("isActiveTile", "");
                        CARO.reComputeListText(oldTile, false);
                        UTIL.forceRedraw(oldTile);
                        UTIL.forceRedraw(oldTile.listForm);
                    }
                }
            }
        }
    }

    var newTile = doc.getElementById('caroOCL_' + compId);
    if (newTile) {
        newTile.setAttribute("isActiveTile", "true");
        newTile.listForm.setAttribute("isActiveTile", "true");
        CARO.reComputeListText(newTile, true);
        UTIL.forceRedraw(newTile);
        UTIL.forceRedraw(newTile.listForm);
    }

    CARO.oldActiveOCLTile = compId;
}


CARO.swapTiles = function (content, i, j, skipPropegateToOtherForm) {
    if (Math.abs(i - j) != 1) {
        CARO.debug('CARO.swapTiles:  tiles must be consecutive.');
        return;
    }

    var tile1 = content.childNodes[i];
    var tile2 = content.childNodes[j];

    if (!skipPropegateToOtherForm) {
        var otherContent = tile1.otherForm.parentNode;
        CARO.swapTiles(otherContent, i, j, true);
    }

    var temp = tile1.desiredX;
    tile1.desiredX = tile2.desiredX
    tile2.desiredX = temp;

    temp = tile1.desiredY;
    tile1.desiredY = tile2.desiredY
    tile2.desiredY = temp;

    temp = tile1.slot;
    tile1.slot = tile2.slot;
    tile2.slot = temp;

    var first = content.childNodes[Math.min(i, j)];
    var second = content.childNodes[Math.max(i, j)];

    // Note: we are assuming i and j are consecutive.  
    content.replaceChild(first, second);
    content.insertBefore(second, first);
}

// Swaps the position of two carousel sections within 
// the DOM.
CARO.swapSections = function (content, i, j, isJoinedSwap) {
    // Verify the sections are immediate peers.
    if (Math.abs(i - j) != 1) {
        CARO.debug('CARO.swapSections:  sections must be consecutive.');
        return;
    }

    // content is the section container, so get the two 
    // specified sections.
    var section1 = content.children[i];
    var section2 = content.children[j];

    // Explicitly swap the slot values for the two sections.
    // This is the one attribute that changes on swap.  The
    // other attributes go with the DOM element.
    var temp = section1.slot;
    section1.slot = section2.slot;
    section2.slot = temp;

    // Get two sections in their current top/bottom | before/after order.
    var first = content.children[Math.min(i, j)]; // current preceding section
    var second = content.children[Math.max(i, j)]; // current trailing section

    // Swap the two DOM elements.
    //Note: we are assuming i and j are consecutive.  
    content.replaceChild(first, second);
    content.insertBefore(second, first);

    // Also swap the other UI representation of the section unless
    // explicitly suppressed.
    if (!isJoinedSwap) {
        // Build the appriopriate ID for the non-active UI representation
        // of the sections.
        var currentIdBase = "";
        var joinedIdBase = "";

        if (section1.isList) {
            currentIdBase = "list"
            joinedIdBase = "caroTab"
        }
        else {
            currentIdBase = "caroTab"
            joinedIdBase = "list"
        }
        var joinedTabId = joinedIdBase + section1.id.split(currentIdBase)[1];

        // Get the non-active UI representation of the section and swap
        // the sections in the non-active section container.
        var joinedTabElement = document.getElementById(joinedTabId);
        if (joinedTabElement) {
            CARO.swapSections(joinedTabElement.parentNode, i, j, true); // Suppress the join swap since that's what we're doing.
        }
    }
}

// Reorders the carousel sections based on user settings on startup.
CARO.orderSectionsOnInit = function () {
    var listIdBase = "list";
    var tabIdBase = "caroTab";
    var doc = document;
    // Place the sections for both UI representations into ordered collections.
    var listElements = new Array();
    var tabElements = new Array();
    for (var i = 0; i < CARO.sectionOrderArray.length; i++) {
        // If we can't find the element, we'll end up ignoring that section when we
        // re-order the section container.
        listElements[i] = doc.getElementById(listIdBase + CARO.sectionOrderArray[i]);
        if (listElements[i]) {
            // If this item is not a 'caroList', then we have an invalid state.
            // We make a best-effort attempt to correct it here.
            if (listElements[i].className != 'caroList') {
                listElements.splice(i, 1);
                CARO.sectionOrderArray.splice(i, 1);  // remove this element from the sectionOrderArray
                continue;
            }
            listElements[i].slot = i;
        }
        tabElements[i] = doc.getElementById(tabIdBase + CARO.sectionOrderArray[i]);
        if (tabElements[i]) {
            tabElements[i].slot = i;
        }
    }

    // Re-order the sections within both UI representation containers.
    var listContainer = doc.getElementById('listContentHolder');
    var tabBar = doc.getElementById('caroTabBar');
    // Add the sections back in reverse order.
    for (i = listElements.length - 1; i >= 0; i--) {
        // If we couldn't find the element specified in the user settings, we just 
        // ignore it.  This should only ever happen if a user folder is deleted
        // on the server and not through the UI.
        if (listElements[i]) {
            // Any sections not in the user settings order list, will simply appear
            // at the end of the list.
            listContainer.insertBefore(listElements[i], listContainer.children[0]);
        }
        // If we couldn't find the element specified in the user settings, we just 
        // ignore it.  This should only ever happen if a user folder is deleted
        // on the server and not through the UI.
        if (tabElements[i]) {
            // Any sections not in the user settings order list, will simply appear
            // at the end of the list.
            tabBar.insertBefore(tabElements[i], tabBar.children[0]);
        }

        // If currently don't explicitly handle a count mismatch in the 
        // two states.  It should never happen.  If we do end up there, 
        // the ordering logic should be able to handle it.  At worst, a
        // rogue tab will always position at the end of the list on init.
    }

    // Make sure the list representation's position state and styling 
    // reflect the new order.
    CARO.setListItemStylingAfterMove(listContainer.children[0]);
    CARO.actImmediatelyOnHeightsForListSections();
}

CARO.linkTileAndListElem = function (tile, listElem) {
    tile.isTile = true;
    listElem.isListElem = true;
    tile.tileForm = listElem.otherForm = listElem.tileForm = tile;
    listElem.listForm = tile.otherForm = tile.listForm = listElem;

    /** Following post-conditions are now true:
    *   - isTile is true for the Tile only
    *   - isListElem is true for the ListElem only
    *   - tileForm points to the tile for either incarnation
    *   - listForm points to the list element for either incarnation
    *   - otherForm point to the other version for either incarnation 
    * 
    * Ex 1. If you want to do something to the tile form but don't know
    *       which one you're working with, write:
    *
    *              doSomething(obj.tileForm);
    *
    * Ex 2. If you want to do something to both forms and don't know 
    *       which one you're working with, write:
    *
    *              doSomething(obj);
    *              doSomething(obj.otherForm);
    */
}

CARO.computeHeightsForListSections = function () {
    /* Method to assign a height to each list of the carousel.  The total height is divided 
    evenly among all the lists.  If any list doesn't need its full quota of space, then 
    the excess is returned to the pool to be divided evenly among any lists that don't fit.
    */

    if ((CARO.carousel.side != CARO.EAST) && (CARO.carousel.side != CARO.WEST)) {
        // Carousel isn't side-docked, so don't waste time.
        return;
    }

    var listContentHolder = document.getElementById('listContentHolder');
    var lists = listContentHolder.children;

    var availableHeight = UTIL.pageHeight() - CARO.CONSTANTS.MENU_TOTAL_HEIGHT - CARO.CONSTANTS.LIST_HEIGHT_LOST_TO_MARGIN;

    for (var i = 0; i < lists.length; i++) {
        if (lists[i].desiredHeight) {
            lists[i].optimalHeight = lists[i].desiredHeight;
        }
        else {
            lists[i].optimalHeight = CARO.computeListOptimalHeight(lists[i]);
        }
        lists[i].heightDecided = 0;
    }

    var numUndecided = lists.length;
    var fairHeight;

    if ((availableHeight <= 0) || (numUndecided <= 0)) {
        return;
    }

    var changeMade = true;
    while (changeMade && (numUndecided > 0)) {
        changeMade = false;
        fairHeight = availableHeight / numUndecided;

        for (var i = 0; i < lists.length; i++) {
            if ((!lists[i].heightDecided) && (lists[i].optimalHeight < fairHeight)) {
                // This list doesn't need its full quota.
                availableHeight -= (lists[i].heightDecided = lists[i].optimalHeight);
                numUndecided--;

                changeMade = true;
            }
        }
    }

    /* Every list that can fit entirely in its quota has been assigned it's optimal size.
    For any list that remains with no assigned height, it must make do with fairHeight
    */
    if (numUndecided > 0) {
        for (var i = 0; i < lists.length; i++) {
            if (!lists[i].heightDecided) {
                lists[i].heightDecided = fairHeight;
            }
        }
    }
}

CARO.actImmediatelyOnHeightsForListSections = function () {
    if (CARO.suspendListResizing) {
        return;
    }

    if ((CARO.carousel.side != CARO.EAST) && (CARO.carousel.side != CARO.WEST)) {
        // Carousel isn't side-docked, so don't waste time.
        return;
    }

    var listContentHolder = document.getElementById('listContentHolder');
    var lists = listContentHolder.children;
    // Since the code was changed for the accessibility project to hide the listContentHolder when the carousel is collapsed,
    // we need to temporarily 'show' it so list heights can be properly determined.
    var reHideContent = false;
    if (listContentHolder.style.display == 'none') {
        listContentHolder.style.display = '';
        reHideContent = true;
    }

    var yAccum = 0;
    for (var i = 0; i < lists.length; i++) {
        if (lists[i].heightDecided) {
            lists[i].listHeightTarget = lists[i].heightDecided;
            lists[i].style.height = lists[i].heightDecided + 'px';
            var visibleContentHeight = lists[i].heightDecided - CARO.CONSTANTS.LIST_HEADER_HEIGHT
            lists[i].children[1].style.height = visibleContentHeight + 'px';

            // May need to initiate a slide on the content
            // CARO.slideIfNecessary(lists[i].children[1].children[0]);

            var content = lists[i].children[1].children[0];
            var totalContentHeight = content.clientHeight;

            var minimumLegalValueForContentPosition = (visibleContentHeight - totalContentHeight);

            if (content.actualY < minimumLegalValueForContentPosition) {
                content.style.top = (content.actualY = minimumLegalValueForContentPosition) + 'px';
            }
            CARO.decideWhetherToHaveArrowsList(content);
            // Set the absolute positioning state of the list sections here.
            // It is the earliest reliable place to track that information 
            // while the list is in a static layout.
            lists[i].style.top = yAccum + 'px';
            lists[i].logicalY = yAccum;
            yAccum += lists[i].offsetHeight;
            lists[i].parentNode.draggableHeight = yAccum;
        }
    }
    CARO.setListItemStylingAfterMove(lists[0]);
    if (reHideContent) {
        listContentHolder.style.display = 'none';
    }
}

CARO.actGraduallyOnHeightsForListSections = function (mode, duration, delay) {
    if (CARO.suspendListResizing) {
        return;
    }

    var MODE_SIN = 0;
    var MODE_SPRING = 1;

    mode = mode || MODE_SIN;

    if ((CARO.carousel.side != CARO.EAST) && (CARO.carousel.side != CARO.WEST)) {
        // Carousel isn't side-docked, so don't waste time.
        return;
    }

    var listContentHolder = document.getElementById('listContentHolder');
    var lists = listContentHolder.children;

    var yAccum = 0;
    for (var i = 0; i < lists.length; i++) {
        if (lists[i].heightDecided) {
            var list = lists[i];
            var contentWindow = list.children[1];
            var content = contentWindow.children[0];

            if ((lists[i].heightDecided != lists[i].listHeightTarget) || (content.clientHeight != content.desiredHeight));
            {
                lists[i].listHeightTarget = lists[i].heightDecided;

                // Container height updates gradually
                var height0 = lists[i].clientHeight;
                var heightF = lists[i].heightDecided;

                switch (mode) {
                    case MODE_SIN:
                        ANIM.animate(lists[i], 'changeHeight', height0, heightF, duration, delay);
                        ANIM.animate(contentWindow, 'changeHeight', height0 - CARO.CONSTANTS.LIST_HEADER_HEIGHT, heightF - CARO.CONSTANTS.LIST_HEADER_HEIGHT, duration, delay);

                        if (!content.desiredHeight) {
                            content.desiredHeight = 0;
                        }
                        if (content.clientHeight != content.desiredHeight) {
                            ANIM.animate(content, 'changeHeight', content.clientHeight, content.desiredHeight, duration, delay);
                        }
                        break;
                    case MODE_SPRING:
                        // Not implemented
                }

                var timeUntilDone = duration + delay;
                setTimeout('CARO.setListResizeInProgressToFalseById("' + list.id + '");', timeUntilDone);
                setTimeout(CARO.decideOnPanArrowsForAllLists, timeUntilDone + 30); // Failsafe
                setTimeout(CARO.slideListsWhereNecessary, timeUntilDone + 30);
            }
        }
        // Set the absolute positioning state of the list sections here.
        // It is the earliest reliable place to track that information 
        // while the list is in a static layout.
        lists[i].style.top = yAccum + 'px';
        lists[i].logicalY = yAccum;
        yAccum += lists[i].offsetHeight;
        lists[i].parentNode.draggableHeight = yAccum;
    }
    CARO.setListItemStylingAfterMove(lists[0]);
}

CARO.setListResizeInProgressToFalseById = function (listId) {
    var listObj = document.getElementById(listId);
    if (listObj) {
        listObj.listResizeInProgress = false;
    }
}

CARO.computeListOptimalHeight = function (list) {
    var bodyHeight = 0;

    if (list.expanded) {
        if (list.children[1].children[0].overrideHeight != undefined) {
            bodyHeight = list.children[1].children[0].overrideHeight
        }
        else {
            bodyHeight = list.children[1].children[0].clientHeight;
        }
    }

    return CARO.CONSTANTS.LIST_HEADER_HEIGHT + bodyHeight;
}


CARO.setupTabBar = function () {
    /* Need to make tabs displayed but invisible while we find the positioning; 
    then we will set display:none. This is handled by having class 'stealthed'
    initially, which we will clear when done.  */
    var doc = document;
    var tabBar = doc.getElementById('caroTabBar');

    tabBar.style.width = '1000px';

    // Add refresh control to recent reports
    var rrTab = doc.getElementById('caroTabRecRpts');
    var refreshControl = doc.createElement('a');
    var wrapper = doc.createElement('div');
    wrapper.className = 'caroTabR';
    refreshControl.setAttribute('tabindex', '0');
    refreshControl.setAttribute('role', 'button');
    refreshControl.setAttribute('aria-labelledby', 'caroTabRecRpts');
    refreshControl.className = "recentReportsRefreshControl";
    refreshControl.onkeydown = CARO.keyDown;
    refreshControl.onclick = function () { syncRecentReports(true); };
    refreshControl.ontouchend = function () { syncRecentReports(true); };
    refreshControl.title = CARO_STRING_CONSTS.refresh;

    rrTab.children[1].innerHTML += CARO.CONSTANTS.TAB_PADDING_SUFFIX_STRING;
    rrTab.trueText = rrTab.children[1].innerHTML;

    wrapper.appendChild(refreshControl);
    rrTab.insertBefore(wrapper, rrTab.children[2]);

    CARO.computeTabPositions(tabBar);

    for (var i = 0; i < tabBar.children.length; i++) {
        var tab = tabBar.children[i];
        if (!isRTL) {
            tab.style.left = tab.newL + 'px';
        }
        else {
            tab.style.right = tab.newL + 'px';
        }
        tab.onclick = CARO.tabClicked;
        tab.onmousedown = CARO.mouseDownOnSection;
        //Added for organize favorites - Mike B
        //The Favorite tab is the only tab droppable at this time.
        tab.dropCapable = false;
        if (tab.id == 'caroTabFav') {
            tab.dropCapable = true;
            tab.dropPending = false;
        }
        tab.slot = i;
        tab.isTab = true;
        tab.actualX = 0;
        if (CARO.isTouchEnabled) {
            tab.ontouchend = CARO.tabClicked;
            tab.ontouchstart = CARO.mouseDownOnSection;
        }
    }

    var showHideCaroTabs = tabBar.children[tabBar.children.length - 1];
    tabBar.showHideControl = showHideCaroTabs;
    showHideCaroTabs.expanded = (caroInitState.caroTabsShown == "true") ? 1 : 0;

    showHideCaroTabs.onclick = CARO.slideInOutTabs;
    showHideCaroTabs.ontouchend = CARO.slideInOutTabs;
    showHideCaroTabs.onkeydown = CARO.keyDown;
    showHideCaroTabs.setAttribute('tabindex', '0'); // allow keyboard access
    showHideCaroTabs.setAttribute('role', 'button'); // screen reader accessibility
    var title = ((showHideCaroTabs.expanded == 1) ? CARO_STRING_CONSTS.hide : CARO_STRING_CONSTS.show) + ' ' + CARO_STRING_CONSTS.carousel + ' ' + CARO_STRING_CONSTS.tabs;
    showHideCaroTabs.title = title;
    showHideCaroTabs.doNotMove = true;

    if (!showHideCaroTabs.expanded) {
        if (!isRTL) {
            tabBar.style.left = tabBar.newCollapsedL + 'px';
        }
        else {
            tabBar.style.right = tabBar.newCollapsedL + 'px';
        }
    }

    // We reorder the tabs based on user preference AFTER the initial tab 
    // setup.  This code still works as long as the tab reordering based
    // on user preference occurs after this code runs.  If reordering
    // happens prior to this code, bad things will happen.
    var indexOfActiveTab = 0;
    switch (caroInitState.caroActiveTab) {
        case 'OCL':
            indexOfActiveTab = 0;
            break;
        case 'RR':
            indexOfActiveTab = 1;
            break;
        case 'FAV':
            indexOfActiveTab = 2;
            break;
    }

    tabBar.children[indexOfActiveTab].setAttribute('active', "true");
    tabBar.activeTab = tabBar.children[indexOfActiveTab];

    tabBar.style.display = 'none';
    tabBar.state = 'hide';

    if (CARO.carousel.expanded) {
        CARO.showTabs();
    }
}

CARO.tabClicked = function (event) {
    event = event || window.event;

    // Test for middle click if not a fixed tab

    if (event && !this.fixed) {
        var middleClick = false;
        if (navigator && navigator.appName == "Microsoft Internet Explorer") {
            if (event.button && (event.button == 4)) {
                // This doesn't seem to work in practice.  IE only populates button correctly
                // with mouseup / mousedown events, not onclick.  
                middleClick = true;
            }
        }
        else {
            if (event.button && (event.button == 1)) {
                middleClick = true;
            }
        }

        if (middleClick) {
            // Close the tab.
            this.children[2].children[0].onclick(event);
            return;
        }
    }

    var tabBar = this.parentNode;
    // If we are recycling the tab (reloadingContent), we need to ensure the 
    // activation logic runs even if the tab is active.
    if (((this == tabBar.activeTab) || (tabBar.sectionDropOccurred)) && (!this.reloadingContent)) {
        // No-op: tab is already active;
        // or this immediately followed a section drop.
        tabBar.sectionDropOccurred = false;
        return;
    }
    else {
        // If we're just recycling the tab, we don't need to perform these
        // specific initializations.
        if (!this.reloadingContent) {
            tabBar.activeTab.setAttribute('active', "false");
            UTIL.forceRedraw(tabBar.activeTab);
            tabBar.activeTab = this;
            this.setAttribute('active', "true");
            UTIL.forceRedraw(this);
        }
        this.reloadingContent = false;

        CARO.loadContent(this.getAttribute('whichCaro'));

        if (CARO.caroInitFinished == true) {
            var strToSaveAsActiveTab = "";
            switch (this.getAttribute('whichCaro')) {
                case 'OCL':
                    strToSaveAsActiveTab = 'OCL';
                    break;
                case 'RecRpts':
                    strToSaveAsActiveTab = 'RR';
                    break;
                case 'Fav':
                    strToSaveAsActiveTab = 'FAV';
                    break;
                default:
                    strToSaveAsActiveTab = this.getAttribute('whichCaro');
            }
            CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.ACTIVE_TAB, strToSaveAsActiveTab);
            CARO.saveHiddenSectionsSetting();
        }
    }
}

CARO.loadContent = function (caroStr) {
    var prefix = 'caroContent';
    var doc = document;
    var oldActiveCaro = doc.getElementById(prefix + CARO.carousel.activeCaroStr);
    var newActiveCaro = doc.getElementById(prefix + caroStr);

    if (newActiveCaro != oldActiveCaro) {
        if (newActiveCaro) {
            if (oldActiveCaro) {
                CARO.hideCaro(oldActiveCaro);
            }

            CARO.carousel.activeCaroStr = caroStr;

            if (CARO.caroInitFinished == true) {
                CARO.showCaro(newActiveCaro);
            }
        }
    }
}

CARO.hideCaro = function (content) {
    if (navigator && navigator.appName == "Microsoft Internet Explorer") {
        // Because IE incorrectly handles inherited opacity
        for (var i = 0; i < content.children.length; i++) {
            content.children[i].filter = 'alpha(opacity=0)';
            ANIM.fadeObjIE(content.children[i], 1, 0, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION);
        }
    }
    else {
        /* custom zIndex also breaks opacity in IE.  In non ie browsers, we can force the 
        entering browser to be on top of the exiting one.  */
        content.style.zIndex = '1';
    }
    ANIM.fadeObj(content, 1, 0, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION);
    setTimeout(function () { content.style.display = 'none'; content.style.zIndex = ''; }, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION);
}

CARO.showCaro = function (content) {
    CARO.activeCaro = content;

    content.style.opacity = '0';
    content.style.filter = 'alpha(opacity=0)';
    content.style.display = '';
    if (navigator && navigator.appName == "Microsoft Internet Explorer") {
        // Because IE incorrectly handles inherited opacity
        for (var i = 0; i < content.children.length; i++) {
            content.children[i].filter = 'alpha(opacity=0)';
            if (content.children[i].ignoreForSpacing) {
                delete content.children[i].ignoreForSpacing;
            }
            ANIM.fadeObjIE(content.children[i], 0, 1, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION);
        }
    }
    else {
        for (var j = 0; j < content.children.length; j++) {
            if (content.children[j].ignoreForSpacing) {
                delete content.children[j].ignoreForSpacing;
            }
        }
        /* custom zIndex also breaks opacity in IE.  In non ie browsers, we can force the 
        entering browser to be on top of the exiting one.  */
        content.style.zIndex = '2';
    }
    ANIM.fadeObj(content, 0, 1, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION);

    setTimeout(function () { content.style.display = ''; content.style.zIndex = ''; }, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION);

    CARO.decideWhetherToHaveArrowsCaro();
}

CARO.showTabs = function () {
    var tabBar = document.getElementById('caroTabBar');

    if (tabBar.state != 'hide') {
        return;
    }
    tabBar.state = 'showing';

    tabBar.style.opacity = 0;
    tabBar.style.filter = '-';
    tabBar.style.display = '';
    ANIM.fadeObj(tabBar, 0, 1, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION);
    for (var i = 0; i < tabBar.children.length; i++) {
        ANIM.fadeObjIE(tabBar.children[i], 0, 1, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION);
    }
    setTimeout(function () { tabBar.state = 'show'; }, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION);
}

CARO.hideTabs = function () {
    var tabBar = document.getElementById('caroTabBar');

    if ((tabBar.state == 'hide') || (tabBar.state == 'hiding')) {
        return;
    }

    var delay = (tabBar.state == 'show') ? 0 : CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION;

    tabBar.state = 'hiding';

    ANIM.fadeObj(tabBar, 1, 0, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION, delay);
    for (var i = 0; i < tabBar.children.length; i++) {
        ANIM.fadeObjIE(tabBar.children[i], 1, 0, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION);
    }
    setTimeout(function () { tabBar.state = 'hide'; tabBar.style.display = 'none'; }, CARO.CONSTANTS.TABS_FADE_IN_OUT_DURATION + delay);
}

CARO.generateTileCaption = function (str, numLines) {
    if (!numLines) {
        numLines = 1;
    }

    if (!CARO.captionSpacerDiv) {
        CARO.captionSpacerDiv = document.createElement('div');
        CARO.captionSpacerDiv.textSpan = document.createElement('span');

        CARO.captionSpacerDiv.className = 'stealthed captionSpacer';
        document.getElementById('carousel').appendChild(CARO.captionSpacerDiv);
        CARO.captionSpacerDiv.appendChild(CARO.captionSpacerDiv.textSpan)
    }

    var ts = CARO.captionSpacerDiv.textSpan;
    CARO.captionSpacerDiv.style.width = CARO.CONSTANTS.CAPTION_SPACE_MAX_PX + 'px';

    CARO.captionSpacerDiv.style.display = 'inline';

    var MAX_H = (numLines + 0.5) * CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT;

    ts.innerHTML = str;
    if (ts.offsetHeight <= MAX_H) {
        CARO.captionSpacerDiv.style.display = 'none';
        return str; // Whole string fits
    }

    var lowerLimit = 0;
    var upperLimit = str.length - 1;


    /* Binary search on string length */
    while (lowerLimit < upperLimit) {
        var tryLen = Math.ceil((upperLimit + lowerLimit) / 2);
        ts.innerHTML = str.substr(0, tryLen) + '...';

        if (ts.offsetHeight <= MAX_H) {
            lowerLimit = tryLen;
        }
        else {
            upperLimit = tryLen - 1;
        }
    }

    CARO.captionSpacerDiv.style.display = 'none';
    return (str.substr(0, lowerLimit) + '...');
}

/* TODO: merge in with generateTileCaption */
CARO.generateListText = function (str, active) {
    if (!CARO.listTextSpacerDiv) {
        CARO.listTextSpacerDiv = document.createElement('div');
        CARO.listTextSpacerDiv.textSpan = document.createElement('span');

        CARO.listTextSpacerDiv.className = 'stealthed listTextSpacer';
        document.getElementById('carousel').appendChild(CARO.listTextSpacerDiv);
        CARO.listTextSpacerDiv.appendChild(CARO.listTextSpacerDiv.textSpan)
    }

    var ts = CARO.listTextSpacerDiv.textSpan;
    ts.setAttribute('active', ((active) ? 'true' : ''));
    ts.style.fontWeight = (active) ? 'bold' : '';

    var MAX_PX = CARO.CONSTANTS.LIST_ITEM_HEIGHT;

    CARO.listTextSpacerDiv.style.display = 'inline';

    ts.innerHTML = str;
    if (ts.offsetHeight <= MAX_PX) {
        CARO.listTextSpacerDiv.style.display = 'none';
        return str; // Whole string fits
    }

    var lowerLimit = 0;
    var upperLimit = str.length - 1;


    /* Binary search on string length */
    while (lowerLimit < upperLimit) {
        var tryLen = Math.ceil((upperLimit + lowerLimit) / 2);
        ts.innerHTML = str.substr(0, tryLen) + '...';

        if (ts.offsetHeight <= MAX_PX) {
            lowerLimit = tryLen;
        }
        else {
            upperLimit = tryLen - 1;
        }
    }

    CARO.listTextSpacerDiv.style.display = 'none';
    return (str.substr(0, lowerLimit) + '...');
}

CARO.rotateCaption = function () {
    // Used so that double-click on caption doesn't cause minimize
    CARO.lastCaptionHitTime = (new Date()).getTime();

    var curValue = CARO.carousel.getAttribute('whichCaption');
    var newValue = 'label_two_rows';

    var numRowsChanged = false;

    switch (curValue) {
        case 'label_two_rows':
            newValue = 'label';
            numRowsChanged = true;
            break;
        case 'label':
            newValue = 'appID';
            break;
        case 'appID':
            newValue = 'formID';
            break;
        case 'formID':
        default:
            numRowsChanged = true;
            newValue = 'label_two_rows';
            break;
    }
    var doc = document;
    CARO.carousel.setAttribute('whichCaption', newValue);
    doc.getElementById('e1AppFrameContainer').setAttribute('whichCaption', newValue);
    CARO.numExtraLinesText = (CARO.carousel.getAttribute('whichCaption') == 'label_two_rows') ? 1 : 0;
    if (numRowsChanged && (navigator && navigator.appName == "Microsoft Internet Explorer")) {
        // The size of the carousel has changed, but IE does not realize on its own.  
        UTIL.forceRedraw(doc.getElementById('caroHolder'));
    }
    else {
        // In most cases, just need to rerender the captions themselves
        UTIL.forceRedraw(document.getElementById('caroContentHolder'));
    }
    CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.WHICH_CAPTION, newValue);
}

CARO.mouseDownOnCaption = function () {
    this.parentNode.clickIsOnCaption = 1;
}

CARO.showHideTabBar = function (showHide) {
    var doc = document;
    var caroTabBar = doc.getElementById('caroTabBar');
    var showHideCaroTabs = doc.getElementById('showHideCaroTabs');
    caroTabBar.setAttribute('expanded', showHide);
    // we loop through the children to hide or show to prevent or allow tabindex
    for (i = 0; i < caroTabBar.children.length; i++) {
        var child = caroTabBar.children[i];
        if (child.nodeType != 1) continue; // check for IE8 or lower
        if (child == showHideCaroTabs) break; // don't do anything with the show/hide control itself
        if (showHideCaroTabs.expanded == 1) {
            child.style.display = '';
        }
        else {
            child.style.display = 'none';
        }
    }

    var title = ((showHideCaroTabs.expanded == 1) ? CARO_STRING_CONSTS.hide : CARO_STRING_CONSTS.show) + ' ' + CARO_STRING_CONSTS.carousel + ' ' + CARO_STRING_CONSTS.tabs;
    showHideCaroTabs.title = title;
    UTIL.forceRedraw(showHideCaroTabs);
}

CARO.slideInOutTabs = function () {
    var expandCollapseControl = this;
    var tabBar = this.parentNode;

    if (expandCollapseControl.expanded) {
        // It's expanded, so we need to collapse.
        if (!isRTL) {
            ANIM.animate(tabBar, 'sinSlideX', 0, CARO.CONSTANTS.TAB_HIDER_ALLOWANCE - tabBar.offsetWidth, CARO.CONSTANTS.TAB_EXPAND_COLLAPSE_DURATION, 0, "CARO.showHideTabBar('false');");
        }
        else {
            ANIM.animate(tabBar, 'sinSlideXright', 0, CARO.CONSTANTS.TAB_HIDER_ALLOWANCE - tabBar.offsetWidth, CARO.CONSTANTS.TAB_EXPAND_COLLAPSE_DURATION, 0, "CARO.showHideTabBar('false');");
        }
    }
    else {
        if (!isRTL) {
            ANIM.animate(tabBar, 'sinSlideX', CARO.CONSTANTS.TAB_HIDER_ALLOWANCE - tabBar.offsetWidth, 0, CARO.CONSTANTS.TAB_EXPAND_COLLAPSE_DURATION, 0, "CARO.showHideTabBar('true');");
        }
        else {
            ANIM.animate(tabBar, 'sinSlideXright', CARO.CONSTANTS.TAB_HIDER_ALLOWANCE - tabBar.offsetWidth, 0, CARO.CONSTANTS.TAB_EXPAND_COLLAPSE_DURATION, 0, "CARO.showHideTabBar('true');");
        }
    }

    expandCollapseControl.expanded = (1 - expandCollapseControl.expanded);
    CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.TABS_SHOWN, (expandCollapseControl.expanded) ? 'true' : 'false');
}

// Try to avoid covering the form title when carousel is on top
CARO.adjustFormTitleIfTop = function () {
    var formTitle = CARO.e1MenuAppIframe.contentWindow.document.getElementById('jdeFormTitle0'); // only care about the first form title
    if (!formTitle) return;
    if (CARO.carousel.side != CARO.NORTH) return;
    var tabBar = document.getElementById('caroTabBar');

    formTitle.style.position = 'relative';
    var targetOffset = tabBar.offsetWidth == 0 ? 0 : tabBar.offsetWidth + tabBar.offsetLeft;

    // Would really like to animate this, but it's a pain for various reasons,
    // and it's only the top carousel anyway, which is - most likely - rarely used.
    if (!isRTL) {
        formTitle.style.left = targetOffset + 'px';
    }
    else {
        formTitle.style.right = targetOffset + 'px';
    }
}

CARO.retFalse = function () {
    return false;
}

CARO.createTile = function (num, type) {
    // num does not necessarily need to be a number - just a unique ID for this tile among others of the same type
    var sfx = CARO.TILE_TYPES.TYPE_TO_SUFFIX[type];
    var appCaptionId = 'appCaption_' + sfx + num;
    var doc = document;

    var tile = doc.createElement('div');
    tile.type = type;
    tile.className = 'caroTile';
    tile.actualX = tile.actualY = 0;
    tile.setAttribute('id', (tile.id = (CARO.TILE_TYPES.TYPE_TO_ID_PREFIX[type] + num)));
    //if accessibility
    tile.setAttribute('aria-labelledby', appCaptionId);
    tile.setAttribute('role', 'link');
    tile.setAttribute('tabindex', '0');

    tile.unselectable = "on";

    tile.onselectstart = CARO.retFalse;
    tile.style.userSelect = tile.style.MozUserSelect = tile.style.webkitUserSelect = "none";
    tile.onmousedown = CARO.mouseDownOnTile;
    tile.onmouseover = CARO.startHoverTimer;
    tile.onmouseout = CARO.delayedlyEndDetailHoverMode;
    tile.onkeydown = CARO.onkeydownTile;
    if (CARO.isTouchEnabled) {
        tile.ontouchstart = CARO.mouseDownOnTile;
    }
    tile.onfocus = CARO.determineIfCaroScrollIsNeeded;

    tile.isNew = true;
    tile.keepMe = true;

    var mainGraphic = doc.createElement('div');
    mainGraphic.className = 'caroTileMainGraphic';
    mainGraphic.id = 'mainGraphic_' + sfx + num;
    mainGraphic.setAttribute('id', mainGraphic.id);

    tile.appendChild(mainGraphic);

    var tileImage = doc.createElement('img');
    tileImage.setAttribute('role', 'presentation');
    tileImage.alt = "";
    tileImage.className = 'filter tileImg';
    tileImage.id = 'filter_tileImg_' + sfx + num;
    tileImage.setAttribute('id', tileImage.id);
    tile.tileImg = tileImage;
    mainGraphic.appendChild(tileImage);


    var caption = doc.createElement('div');
    caption.className = 'caroTileCaption';
    tile.appendChild(caption);
    tile.caption = caption;

    caption.onclick = CARO.rotateCaption;
    caption.onmousedown = CARO.mouseDownOnCaption;
    caption.ontouchstart = CARO.mouseDownOnCaption;
    caption.ontouchend = CARO.rotateCaption;

    caption.appendChild(caption.label = doc.createElement('div'));
    caption.appendChild(caption.appID = doc.createElement('div'));
    caption.appendChild(caption.formID = doc.createElement('div'));
    caption.appendChild(caption.labelTwoRows = doc.createElement('div'));
    caption.appendChild(caption.fullDesc = doc.createElement('div'));

    caption.label.className = 'labelCaptionDiv';
    caption.labelTwoRows.className = 'labelTwoRowsCaptionDiv';
    caption.appID.className = 'appIDCaptionDiv';
    caption.formID.className = 'formIDCaptionDiv';
    //if accessibility
    caption.fullDesc.id = appCaptionId;
    caption.fullDesc.className = 'accessibility';

    // Tile form now complete; create list form

    listElem = document.createElement('div');
    listElem.className = 'listItem';
    listElem.setAttribute('id', CARO.TILE_TYPES.TYPE_TO_ID_LPREFIX[type] + num);
    //if accessibility
    listElem.setAttribute('aria-labelledby', appCaptionId);
    listElem.setAttribute('role', 'link');
    listElem.setAttribute('tabindex', '0');
    listElem.onkeydown = CARO.onkeydownTile;

    CARO.linkTileAndListElem(tile, listElem);

    listElem.unselectable = "on";
    listElem.onselectstart = function () { return false };
    listElem.style.userSelect = tile.style.MozUserSelect = listElem.style.webkitUserSelect = "none";
    listElem.onmousedown = CARO.mouseDownOnTile;
    if (CARO.isTouchEnabled) {
        listElem.ontouchstart = CARO.mouseDownOnTile;
    }
    listElem.onmouseover = CARO.startHoverTimer;
    listElem.onmouseout = CARO.delayedlyEndDetailHoverMode;
    listElem.onfocus = CARO.determineIfListScrollIsNeeded;

    var table = doc.createElement('table');
    var tbody = doc.createElement('tbody');
    var row = doc.createElement('tr');
    var iconCell = doc.createElement('td');
    var textCell = doc.createElement('td');
    var icon = doc.createElement('img');

    table.className = 'listItem';
    iconCell.className = 'listIcon';
    textCell.className = 'listText';
    icon.alt = '';

    listElem.appendChild(table);
    table.appendChild(tbody);
    tbody.appendChild(row);
    row.appendChild(iconCell);
    iconCell.appendChild(icon);
    row.appendChild(textCell);

    listElem.textCell = textCell;
    listElem.icon = icon;

    return tile;
}

// trigger click action of tile for keyboard user
CARO.onkeydownTile = function (event) {
    var elem = RIUTIL.getEventSource(event);
    if (event.keyCode == 32 || event.keyCode == 13) {
        CARO.activateTile(elem);
        event.stopPropagation();
        event.preventDefault();
    }
}

ANIM.Animations.expandCollapseList = new Object();
ANIM.Animations.expandCollapseList.doFrame = function (obj, v0, vF, t) {
    var A = v0 - vF;
    var k = 0.016;
    var B = k * A;

    var innerPos = vF + (A + B * t) * Math.exp(-1 * k * t);
    var outerHeight = CARO.CONSTANTS.LIST_HEADER_HEIGHT + Math.max(innerPos + obj.innerHeight, 0);

    obj.listOuter.style.height = outerHeight + 'px';
    obj.listInner.style.top = (obj.listInner.actualY = innerPos) + 'px';
    obj.innerPos = innerPos;

    obj.desiredHeight = outerHeight;

    CARO.computeHeightsForListSections();
    CARO.actImmediatelyOnHeightsForListSections();

    var lowStop = Math.min(v0, vF);
    var highStop = Math.max(v0, vF);
    var rotation = (innerPos - lowStop) / (highStop - lowStop);

    if (Math.abs(innerPos - vF) < 0.25) {
        obj.animationDoneFlag = true;
        rotation = Math.round(rotation);
    }

    if (isRTL) {
        rotation = 2 - rotation;
    }

    CARO.applyRotation(obj.arrow, rotation * 90);
}
ANIM.Animations.expandCollapseList.doEnd = function (obj, vF) {
    obj.expanded = (1 - obj.expanded);
    obj.desiredHeight = undefined;

    switch (obj.id) {
        case "listOCL":
            CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.LIST_EXPANDED_OCL, (obj.expanded) ? 'true' : 'false');
            break;
        case "listRecRpts":
            CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.LIST_EXPANDED_RR, (obj.expanded) ? 'true' : 'false');
            break;
        case "listFav":
            CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.LIST_EXPANDED_FAV, (obj.expanded) ? 'true' : 'false');
            break;
    }

    CARO.decideWhetherToHaveArrowsList(obj.listInner);
}

CARO.expandCollapseListCallback = function (listParent) {
    var list = document.getElementById(listParent);
    if (list.expanded) {
        list.listInner.style.display = '';
    }
    else {
        list.listInner.style.display = 'none'; // prevents tabbing to listItems that are not visible
        list.listOuter.style.height = '0px';
    }
}

CARO.expandCollapseList = function () {
    var listNode = this.parentNode.id;
    CARO.expandCollapseListNode(listNode);
}

CARO.expandCollapseListNode = function (listParent) {
    var list = document.getElementById(listParent);
    var doc = document;
    if (!list.animationDoneFlag || (list.sectionDropOccurred)) {
        // Don't expand and collapse at the same time
        // or collapse/expand after a section drop.
        list.sectionDropOccurred = false;
        return;
    }

    list.listOuter = doc.getElementById(list.id + 'Outer');
    list.listInner = doc.getElementById(list.id + 'Inner');
    list.arrow = doc.getElementById(list.id + 'Arrow');
    // Change the title right away - this helps the screen reader read the correct information on the button
    list.arrow.setAttribute('title', list.expanded ? CARO_STRING_CONSTS.show : CARO_STRING_CONSTS.hide);

    list.innerHeight = list.listInner.children.length * CARO.CONSTANTS.LIST_ITEM_HEIGHT;
    list.innerPos = list.listInner.offsetTop;

    list.animationDoneFlag = false;

    // If not recycling the section container, toggle the list section open or closed.
    if (!list.reloadingContent) {
        if (list.expanded) {
            // Change bgImage immediately.
            list.children[0].setAttribute('useExpandedBackground', 'false');
            UTIL.forceRedraw(list.children[0]);

            ANIM.animate(list, "expandCollapseList", (list.listInner.actualY) ? list.listInner.actualY : 0, -1 * list.innerHeight, -1, 0, "CARO.expandCollapseListCallback('" + listParent + "')");
            list.listInner.actualY = 0;

            if (list.isUserFolder) {
                CARO.removeSectionFromExpandedSectionsSetting(list.userFolderId);
            }
        }
        else {
            // Change bgImage immediately.
            list.children[0].setAttribute('useExpandedBackground', 'true');
            list.listInner.style.display = '';
            UTIL.forceRedraw(list.children[0]);
            ANIM.animate(list, "expandCollapseList", -1 * list.innerHeight, 0, -1, 0, "CARO.expandCollapseListCallback('" + listParent + "')");

            if (list.isUserFolder) {
                CARO.singletonAddToArray(CARO.expandedUserFoldersArr, list.userFolderId);
            }
        }
    }
    else {
        // If recycling the section container, reset the contents of the folder and reset the recycling indicators.
        list.reloadingContent = false;
        list.animationDoneFlag = true;
        UTIL.forceRedraw(list.children[0]);

        // If the list section is expanded, then the previous section will be listed in the expanded folders user setting.
        // Replace it with the new folder.
        if (list.expanded) {
            if (list.isUserFolder) {
                CARO.singletonAddToArray(CARO.expandedUserFoldersArr, list.userFolderId);
            }
        }
    }
    CARO.saveTaskFolderSettings();
}

CARO.setupLists = function () {
    var doc = document;
    var listContentHolder = doc.getElementById('listContentHolder');
    var lists = listContentHolder.children;

    // Add refresh control to recent reports
    var rrListHeaderPosHelper = doc.getElementById('listRecRpts').children[0].children[1];
    var refreshControl = doc.createElement('a');
    refreshControl.title = CARO_STRING_CONSTS.refresh;
    refreshControl.className = "recentReportsRefreshControl";
    refreshControl.onclick = CARO.syncRecentReportsControlClicked;
    refreshControl.ontouchend = CARO.syncRecentReportsControlClicked;
    refreshControl.onkeydown = CARO.keyDown;
    refreshControl.setAttribute('tabindex', '0');
    refreshControl.setAttribute('role', 'button');
    refreshControl.setAttribute('aria-labelledby', 'listRecRptsPositionHelper');
    rrListHeaderPosHelper.appendChild(refreshControl);

    for (var i = 0; i < lists.length; i++) {
        CARO.doPerListSetup(lists[i]);
        lists[i].slot = i;
        // The top list item has a rounded corner, this attribute
        // governs that via CSS.
        CARO.setListItemStylingAfterMove(lists[i]);
    }

    CARO.computeHeightsForListSections();
    CARO.actImmediatelyOnHeightsForListSections();

    // accessibility - hide the content if CARO is collapsed
    if (!CARO.carousel.expanded) {
        listContentHolder.style.display = 'none';
    }
}

CARO.doPerListSetup = function (list) {
    // First child of each list is its header
    list.children[0].onclick = CARO.expandCollapseList;
    list.children[0].onmousedown = CARO.mouseDownOnSection;
    list.isList = true;
    var doc = document;

    // Set up the list arrow for keyboard accessibility
    var listArrow = doc.getElementById(list.id + 'Arrow');
    listArrow.onkeydown = CARO.keyDown;
    listArrow.setAttribute('tabindex', '0');
    listArrow.setAttribute('role', 'button');
    listArrow.setAttribute('title', list.expanded ? CARO_STRING_CONSTS.hide : CARO_STRING_CONSTS.show);
    listArrow.setAttribute('aria-labelledby', list.id + 'HeaderText' + ' ' + list.id + 'Arrow');

    if (CARO.isTouchEnabled) {
        list.children[0].ontouchend = CARO.expandCollapseList;
        list.children[0].ontouchstart = CARO.mouseDownOnSection;
    }
    list.animationDoneFlag = true;

    if (!list.expanded) {
        list.expanded = 0;
        list.desiredHeight = CARO.CONSTANTS.LIST_HEADER_HEIGHT;
        var listInner = doc.getElementById(list.id + 'Inner');
        listInner.style.display = 'none'; // prevents tabbing to listItems that are not visible
    }
    else {
        list.expanded = 1;
    }

    list.children[1].children[0].computeMinMaxYList = CARO.contentComputeMinMaxYList;
    list.children[1].children[0].CMID = 0;
    list.children[1].children[0].actualY = 0;
    list.children[1].children[0].vertical = 1;
    list.actualY = 0;

    CARO.addListScrollers(list);
}


CARO.addListScrollers = function (list) {
    var outer = list.children[1];
    list.panUpControl = document.createElement('a');
    list.panUpControl.className = 'panControl up';
    list.panUpControl.onclick = CARO.panListUp;
    list.panUpControl.ontouchend = CARO.panListUp;
    list.panDownControl = document.createElement('a');
    list.panDownControl.className = 'panControl down';
    list.panDownControl.onclick = CARO.panListDown;
    list.panDownControl.ontouchend = CARO.panListDown;
    list.onwheel = CARO.mouseWheel;
    outer.appendChild(list.panUpControl);
    outer.appendChild(list.panDownControl);
}

CARO.addHorizontalScrollers = function () {
    var contentHolder = document.getElementById('caroContentHolder');

    contentHolder.panLeftControl = document.createElement('a');
    contentHolder.panLeftControl.className = 'panControl left';
    contentHolder.panLeftControl.onclick = CARO.panCaroLeft;
    contentHolder.panLeftControl.ontouchend = CARO.panCaroLeft;
    contentHolder.panLeftControl.isAScroller = true;

    contentHolder.panRightControl = document.createElement('a');
    contentHolder.panRightControl.className = 'panControl right';
    contentHolder.panRightControl.onclick = CARO.panCaroRight;
    contentHolder.panRightControl.ontouchend = CARO.panCaroRight;
    contentHolder.panRightControl.isAScroller = true;

    contentHolder.appendChild(contentHolder.panLeftControl);
    contentHolder.appendChild(contentHolder.panRightControl);

    contentHolder.panLeftControl.setAttribute('necessary', 'true');
    contentHolder.panRightControl.setAttribute('necessary', 'true');
}

CARO.panListUp = function () {
    CARO.panCaro(this.parentNode.children[0], 1);
}

CARO.panListDown = function () {
    CARO.panCaro(this.parentNode.children[0], -1);
}

CARO.panCaroLeft = function () {
    CARO.panCaro(CARO.activeCaro, 1);
}

CARO.panCaroRight = function () {
    CARO.panCaro(CARO.activeCaro, -1);
}

CARO.mouseWheel = function (event) {
    var listOuter = event.currentTarget.children[1];
    var accel = 1.0;
    // If we are already animating, we're adding an acceleration factor.
    // This allows the scroll to speed up upon multiple consecutive mouse wheel events.
    // The isAnimating flag could be set for some other reason, but we aren't taking
    // that edge case into account here.
    if (ANIM.isAnimating) {
        accel = 1.5;
    }
    if (listOuter.className == "listContentOuter") {
        event.deltaY > 0 ? CARO.panCaro(listOuter.children[0], -1, accel) : CARO.panCaro(listOuter.children[0], 1, accel);
    }
}

CARO.determineIfCaroScrollIsNeeded = function () {
    var caroContent = this.parentNode;
    var caroContentHolder = caroContent.parentNode;
    var tileLeftEdge = this.offsetLeft + caroContent.offsetLeft;
    var tileRightEdge = tileLeftEdge + this.offsetWidth - 2;

    if (tileLeftEdge + (0.85 * caroContentHolder.offsetWidth) < 0) {
        // if the tileLeftEdge will still be negative (hidden) after a nudge,
        // we'll just set caroContent left to be the tile.offsetLeft
        caroContent.actualX = this.offsetLeft;
        CARO.slideIfNecessary(caroContent);
    }
    else if (tileRightEdge - (0.85 * caroContentHolder.offsetWidth) > caroContentHolder.offsetWidth) {
        // if the tileRightEdge will still be beyond content boundary after a nudge,
        // we'll reset caroContent left and slide into place
        caroContent.actualX = (0.85 * caroContentHolder.offsetWidth) - caroContent.lastWidth;
        CARO.slideIfNecessary(caroContent);
    }
    else if (tileLeftEdge < 0) {
        // tileLeftEdge is left of displayed content
        CARO.panCaroLeft();
    }
    else if (tileRightEdge > caroContentHolder.offsetWidth) {
        // tileRightEdge is beyond the right of listContentHolder boundary
        // we adjust the actualX by the amount we are off the screen to prevent
        // the motion routines from showing unnecessary scrolling
        caroContent.actualX = caroContent.actualX - (tileRightEdge - caroContentHolder.offsetWidth);
        CARO.panCaroRight();
    }
}

CARO.determineIfListScrollIsNeeded = function () {
    var listInner = this.parentNode;
    var listOuter = listInner.parentNode;
    var itemTop = this.offsetTop + listInner.offsetTop;
    var itemBottom = itemTop + this.offsetHeight - 2;

    if (itemTop + (0.85 * listOuter.offsetHeight) < 0) {
        // if the itemTop will still be negative (hidden) after a nudge,
        // we'll just set listInner top to be the listItem.offsetTop
        listInner.actualY = this.offsetTop;
        CARO.slideIfNecessary(listInner);
    }
    else if (itemBottom - (0.85 * listOuter.offsetHeight) > listOuter.offsetHeight) {
        // if the itemBottom will still be beyond listOuter height after a nudge,
        // we'll reset listInner top and slide into place
        listInner.actualY = (0.85 * listOuter.offsetHeight) - listInner.offsetHeight;
        CARO.slideIfNecessary(listInner);
    }
    else if (itemTop < 0) {
        // itemTop is above listOuter top edge
        CARO.panCaro(listInner, 1);
    }
    else if (itemBottom > listOuter.offsetHeight) {
        // itemBottom is below the bottom of listOuter boundary
        CARO.panCaro(listInner, -1);
    }
    // the focus event can change the scrollTop, but we are controlling this ourselves
    setTimeout(function () { listOuter.scrollTop = 0; }, 0);
}

CARO.applyRotation = function (obj, degrees) {
    obj.style.webkitTransform =
    obj.style.MozTransform =
    obj.style.transform = 'rotate(' + degrees + 'deg)';

    if (navigator && navigator.appName == "Microsoft Internet Explorer") {
        var theta = degrees * (Math.PI / 180);

        var ct = Math.cos(theta);
        var st = Math.sin(theta);

        var w = (obj.offsetWidth || obj.savedKnownWidth) / 2;
        var h = (obj.offsetHeight || obj.savedKnownHeight) / 2;

        /* Note: This function did not work properly when the object was hidden 
        (display:none) at call-time because the width and height cannot be 
        determined.  Therefore, check for a savedKnownWidth/Height, which must
        be set by the client if this function is to be used on a hidden object.*/

        /* Note: Since this function will be repeatedly called during an animation,
        a possible performance improvement would be to cache w and h since they
        presumably will not change during the animation.  
           
        In practice, there does not seem to be any performance problem, and the
        animation is quite smooth.  IE does seem to cache the obj.offsetWidth 
        and obj.offsetHeight as opposed to recomputing them each time, so saving
        them as constants would really only save the division by 2 and is 
        probably not worth the sacrifice in code cleanliness at this time.  */

        obj.style.filter = 'progid:DXImageTransform.Microsoft.Matrix(M11='
            + ct
            + ', M12='
            + (-1 * st)
            + ', Dx='
            + (-1 * w * ct + h * st + w)
            + ', M21='
            + st
            + ', M22='
            + ct
            + ', Dy='
            + (-1 * w * st + -1 * h * ct + h)
            + ')'
    }
}

CARO.reComputeListText = function (tile, active) {
    tile.listForm.textCell.innerHTML = CARO.generateListText(tile.tileForm.label, active);
}

/* If the carousel is wasting any space, it will slide gracefully to the nearest, non-wasteful
position.  */
CARO.slideIfNecessary = function (content) {
    CARO.caroNudge(content, 0);
}

CARO.caroNudge = function (content, velocity, accel) {
    var curPos = 0;
    if (content.vertical) {
        curPos = content.actualY;
        if (curPos == NaN || curPos == undefined) {
            curPos = content.offsetTop;
        }
    }
    else {
        curPos = content.actualX;
        if (curPos == NaN || curPos == undefined) {
            curPos = content.offsetLeft;
        }
    }

    // Slide with an initial velocity of zero
    CARO.caroMotion(content.id, curPos, velocity, accel);
}

CARO.decideWhetherToHaveArrowsList = function (inner) {
    var outer = inner.parentNode;
    var list = outer.parentNode;

    if ((inner.clientHeight <= outer.clientHeight + 1) || (!inner.parentNode.parentNode.animationDoneFlag) || (!CARO.okToHaveListArrows)) {
        list.panUpControl.setAttribute('necessary', 'false');
        list.panDownControl.setAttribute('necessary', 'false');
    }
    else {
        list.panUpControl.setAttribute('necessary', (inner.actualY < -0.5) ? 'true' : 'false');
        list.panDownControl.setAttribute('necessary', ((inner.actualY + inner.clientHeight) > (outer.clientHeight + 0.5)) ? 'true' : 'false');
    }

    UTIL.forceRedraw(list.panUpControl);
    UTIL.forceRedraw(list.panDownControl);
}

CARO.decideWhetherToHaveArrowsCaro = function () {
    if (!CARO.activeCaro) {
        return;
    }

    var content = CARO.activeCaro;

    var lArrow = content.parentNode.panLeftControl;
    var rArrow = content.parentNode.panRightControl;

    var wPage = UTIL.pageWidth();
    var wCaro = content.getWidth();

    if (wCaro < wPage) {
        lArrow.setAttribute('necessary', 'false');
        rArrow.setAttribute('necessary', 'false');
    }
    else {
        var lAligned = 0;
        var rAligned = wPage - wCaro;
        var curX = content.actualX;

        lArrow.setAttribute('necessary', ((curX < lAligned) ? 'true' : 'false'));
        rArrow.setAttribute('necessary', ((curX > rAligned) ? 'true' : 'false'));
    }

    CARO.forceRedrawIfNecessaryChanged(lArrow);
    CARO.forceRedrawIfNecessaryChanged(rArrow);
}

CARO.forceRedrawIfNecessaryChanged = function (obj) {
    var curValue = obj.getAttribute('necessary');
    // Force redraw is an expensive operation, so only redraw if there's been a change

    if ((!obj.savedNecessaryFlag) || (obj.savedNecessaryFlag != curValue)) {
        obj.savedNecessaryFlag = curValue;
        UTIL.forceRedraw(obj);
    }
}

CARO.computeSpeedToNudgeNPixels = function (pixels) {
    return (pixels + 40) / 0.641486;
    // return (pixels + 70.382489)/0.641486;
}

CARO.panCaro = function (content, dir, a) {
    var dist = 0;
    if (content.vertical) {
        dist = content.parentNode.clientHeight;
    }
    else {
        dist = UTIL.pageWidth();
    }

    var v = dir * CARO.computeSpeedToNudgeNPixels(0.85 * dist);

    CARO.caroNudge(content, v, a);
}

CARO.updateRecentReports = function (rrData, dataSource) {
    if (isNativeContainer) {
        if (NativeContainer) {
            NativeContainer.updateData(NativeContainer.PARAM_UPDATE_DATA_RECENTREPORTS);
        }
    }
    var doc = document;
    var caroRR = doc.getElementById('caroContentRecRpts');
    var listRR = doc.getElementById('listRecRptsInner');

    if (!caroRR) {
        // Carousel doesn't exist.  We are probably in a separate window.
        return;
    }

    var needAdd = false;

    // Make WSJ tile for first time
    if (caroRR.children.length == 0) {
        var tile = CARO.createTile('WSJ', CARO.TILE_TYPES.RRPT);
        tile.special = 'WSJ';
        tile.dataSource = dataSource;
        tile.isNew = true;
        tile.doNotMove = true;
        tile.dropCapable = false;

        needAdd = true;

        CARO.labelTileAs(tile, CARO_STRING_CONSTS.viewJobStatus, 'P986110B', 'W986110BA');

        CARO.silentAddTile(caroRR, tile);
        CARO.silentAddTile(listRR, tile.listForm);

        var imageUrlLarge = JDE_ICON_PREFIX + CARO_SPECIAL_ICONS.WSJ.BASENAME + JDE_ICON_SUFFIX.LARGE + CARO_SPECIAL_ICONS.MANAGE_FAVS.EXT;
        var imageUrlMedium = JDE_ICON_PREFIX + CARO_SPECIAL_ICONS.WSJ.BASENAME + JDE_ICON_SUFFIX.MEDIUM + CARO_SPECIAL_ICONS.MANAGE_FAVS.EXT;
        var imageUrlSmall = JDE_ICON_PREFIX + CARO_SPECIAL_ICONS.WSJ.BASENAME + JDE_ICON_SUFFIX.SMALL + CARO_SPECIAL_ICONS.MANAGE_FAVS.EXT;

        tile.imgUrlLarge = imageUrlLarge;
        tile.imgUrlMed = imageUrlLarge;

        CARO.setTileImage(tile, (CARO.USE_LARGE_ICONS) ? imageUrlLarge : imageUrlMedium);

        tile.listForm.icon.src = imageUrlSmall;
    }

    // Mark everything except WSJ Tile for deletion
    for (var i = 0; i < caroRR.children.length; i++) {
        caroRR.children[i].keepMe = false;
    }
    caroRR.children[0].keepMe = true;

    for (var i = 0; i < rrData.length; i++) {
        var rpt = rrData[i];

        var rpt_suffix = (rpt.server + '_' + rpt.port + '_' + rpt.jobId);
        var tileId = CARO.TILE_TYPES.TYPE_TO_ID_PREFIX[CARO.TILE_TYPES.RRPT] + rpt_suffix;

        var tile = document.getElementById(tileId);

        if (!tile) {
            // Doesn't exist yet - we have to create it.
            tile = CARO.createTile(rpt_suffix, CARO.TILE_TYPES.RRPT);
            tile.doNotMove = true;
            tile.dropCapable = false;
            CARO.labelTileAs(tile, rpt.reportNameDesc + ' (' + rpt.timeSubmitted + ')', rpt.reportName);

            CARO.silentAddTile(caroRR, tile);
            CARO.silentAddTile(listRR, tile.listForm);

            tile.isNew = true;
            needAdd = true;
        }

        tile.orderOverride = i + 1;

        tile.keepMe = true;
        tile.rptInfo = rpt;
        tile.dataSource = dataSource;

        var image_name;
        var med_image_name;
        var small_image_name;

        switch (tile.rptInfo.statusCode) {
            case 'D':
            case 'E':
            case 'P':
            case 'W':
            case 'H':
            case 'S':
                image_name = JDE_ICON_PREFIX + tile.rptInfo.iconBasename + '_' + tile.rptInfo.statusCode + JDE_ICON_SUFFIX.LARGE + tile.rptInfo.iconExtension;
                med_image_name = JDE_ICON_PREFIX + tile.rptInfo.iconBasename + '_' + tile.rptInfo.statusCode + JDE_ICON_SUFFIX.MEDIUM + tile.rptInfo.iconExtension;
                small_image_name = JDE_ICON_PREFIX + tile.rptInfo.iconBasename + '_' + tile.rptInfo.statusCode + JDE_ICON_SUFFIX.SMALL + tile.rptInfo.iconExtension;
                break;

            default:
                /* Should never happen */
                image_name = JDE_ICON_PREFIX + tile.rptInfo.iconBasename + JDE_ICON_SUFFIX.LARGE + tile.rptInfo.iconExtension;
                med_image_name = JDE_ICON_PREFIX + tile.rptInfo.iconBasename + JDE_ICON_SUFFIX.LARGE + tile.rptInfo.iconExtension;
                small_image_name = JDE_ICON_PREFIX + tile.rptInfo.iconBasename + JDE_ICON_SUFFIX.LARGE + tile.rptInfo.iconExtension;
        }

        tile.imgUrlLarge = image_name;
        tile.imgUrlMed = med_image_name;

        CARO.setTileImage(tile, (CARO.USE_LARGE_ICONS) ? image_name : med_image_name);
        tile.listForm.icon.src = small_image_name;

        UTIL.forceRedraw(tile.children[0].children[0]);
        UTIL.forceRedraw(tile.listForm.icon);
    }

    // Go through and delete any tiles no longer needed
    var needDel = false;
    for (var i = caroRR.children.length - 1; i >= 0; i--) {
        var child = caroRR.children[i];
        if (!child.keepMe) {
            needDel = true;
            child.ignoreForSpacing = true;
            CARO.fadeAndDelTileByIndex(caroRR, i);
            CARO.fadeAndDelTileByIndex(listRR, i);
        }
    }

    CARO.setDesiredPositions(caroRR);
    CARO.setDesiredPositionsList(listRR);

    var delayToRearrange = (needDel) ? (4 / 5 * CARO.CONSTANTS.TILE_FADE_DURATION) : 0;
    var delayToFadeInNew = delayToRearrange;
    var delayToFadeInNewList = delayToRearrange;

    // Snap any new nodes to their final position
    for (var i = 0; i < caroRR.children.length; i++) {
        var tile = caroRR.children[i];
        if (tile.isNew) {
            var lItm = tile.listForm;

            tile.style.left = (tile.actualX = tile.desiredX) + 'px';
            lItm.style.top = (lItm.actualY = lItm.desiredY) + 'px';
        }
    }

    // TODO: desired pos for contents.

    if (CARO.desiredPositionsDifferFromActual(caroRR)) {
        delayToFadeInNew += CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TILES;
        CARO.slideToDesiredPositions(caroRR, delayToRearrange);
    }

    if (CARO.desiredPositionsDifferFromActualList(listRR)) {
        delayToFadeInNewList += CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST;
        CARO.slideToDesiredPositionsList(listRR, delayToRearrange);
    }

    if (needDel) {
        setTimeout('CARO.slideIfNecessary(document.getElementById(\'' + listRR.id + '\'))', delayToFadeInNewList);
    }

    if (needAdd) {
        for (var i = 0; i < caroRR.children.length; i++) {
            if (caroRR.children[i].isNew) {
                ANIM.fadeObj(caroRR.children[i], 0, 1, CARO.CONSTANTS.TILE_FADE_DURATION, delayToFadeInNew, true);
                ANIM.fadeObj(caroRR.children[i].listForm, 0, 1, CARO.CONSTANTS.TILE_FADE_DURATION, delayToFadeInNewList, true);
                caroRR.children[i].isNew = false;
            }
        }
    }

    CARO.initializedRR = true;

    if (CARO.activeCaro == caroRR) {
        CARO.decideWhetherToHaveArrowsCaro();
    }
    CARO.decideOnPanArrowsForAllLists();
}

CARO.labelTileAs = function (tile, textual, appId, formId, active) {
    tile.trueCaptionState = { textual: textual, appId: appId, formId: formId, active: active };

    tile = tile.tileForm;
    var listItem = tile.listForm;

    // Edge cases for external/ADF app tasks: 1) Invalid setup (no proxy app) and 2) never-present form/formID
    // (the tasks lack the concept of a form/formID).  To mirror OCL's treatment of blank appIds and formIds (and
    // prevent any silliness with null labels with the string "null"), adjust input values while simultaneously
    // limiting changes and impact to existing, standard E1 logic).
    if (tile.favInfo && CARO.isExternalApp(tile.favInfo.appType)) {
        if (appId == "") {
            appId = null;
        }

        if (formId == "null") {
            formId = null;
        }
    }

    if (!appId) {
        appId = textual;
    }
    if (!formId) {
        formId = appId;
    }

    tile.caption.label.innerHTML =
        CARO.generateTileCaption(textual);
    listItem.textCell.innerHTML = CARO.generateListText(textual, active);

    tile.caption.appID.innerHTML =
        CARO.generateTileCaption(appId);

    tile.caption.formID.innerHTML =
        CARO.generateTileCaption(formId);

    tile.caption.labelTwoRows.innerHTML =
        CARO.generateTileCaption(textual, 2);

    //if accessibility
    if (tile.favInfo && CARO.isFolder(tile.favInfo.appType)) {
        textual += CARO_STRING_CONSTS.folder;
    }
    tile.caption.fullDesc.innerHTML = textual;

    if (tile.caption.label.innerHTML == tile.caption.labelTwoRows.innerHTML) {
        /* If the 1-line and 2-line text are identical, then only one line
        was needed.  We should vertically center the two-line text */
        tile.caption.labelTwoRows.style.lineHeight = (2 * CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT) + 'px';
    }
    else {
        tile.caption.labelTwoRows.style.lineHeight = '';
    }
}

CARO.syncRecentReportsControlClicked = function (event) {
    CARO.preventBubbling(event || window.event);
    syncRecentReports(true);
}

CARO.getDetailPane = function () {
    if (!CARO.detailPane) {
        var detailPane = CARO.detailPane = document.createElement('div');
        detailPane.className = 'caroDetailPane';
        detailPane.id = 'caroDetailPane';
        detailPane.setAttribute('id', detailPane.id);

        var textHolder = detailPane.textDiv = document.createElement('div');
        textHolder.className = 'detailText';
        detailPane.appendChild(textHolder);

        // var glareFilter = document.createElement('div');
        // glareFilter.className = 'glareCover';
        // detailPane.appendChild(glareFilter);

        CARO.carousel.appendChild(CARO.detailPane);
    }

    return CARO.detailPane;
}

CARO.showTileDetailPane = function (tile, wasLastHidden) {
    var detailPane = CARO.getDetailPane();
    detailPane.idOfObj = tile.id;
    detailPane.lastInteraction = (new Date()).getTime();

    CARO.setDetailText(detailPane, tile);

    CARO.positionDetailPaneForTile(detailPane, tile);

    if (wasLastHidden) {
        detailPane.style.display = 'block';
        ANIM.fadeObj(detailPane, 0, CARO.CONSTANTS.DETAIL_PANE_MAX_OPACITY, CARO.CONSTANTS.DETAIL_FADE_IN_OUT_DURATION, 0);
    }
}

CARO.positionDetailPaneForTile = function (detailPane, tile) {
    if (tile.isTile) {
        var x_pos = tile.parentNode.offsetLeft + tile.offsetLeft + 0.5 * (tile.offsetWidth - detailPane.dimmensions.W);
        x_pos = UTIL.clamp(x_pos, 0, UTIL.pageWidth() - detailPane.dimmensions.W);
        detailPane.style.left = x_pos + 'px';
        detailPane.style.right = '';

        if (CARO.carousel.side == CARO.NORTH) {
            detailPane.style.top = CARO.CONSTANTS.CARO_HEIGHT + CARO.numExtraLinesText * CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT - CARO.CONSTANTS.DETAIL_ENCROACHMENT + 'px';
            detailPane.style.bottom = '';
        }
        else {
            detailPane.style.top = '';
            detailPane.style.bottom = CARO.CONSTANTS.CARO_HEIGHT + CARO.numExtraLinesText * CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT - CARO.CONSTANTS.DETAIL_ENCROACHMENT + 'px';
        }
    }
    else {
        var y_pos = tile.offsetTop + tile.parentNode.offsetTop + tile.parentNode.parentNode.offsetTop + 0.5 * (tile.offsetHeight - detailPane.dimmensions.H);
        y_pos = UTIL.clamp(y_pos, 0, UTIL.pageHeight() - CARO.CONSTANTS.MENU_TOTAL_HEIGHT - detailPane.dimmensions.H - CARO.CONSTANTS.RCUX_BOTTOM);
        detailPane.style.top = y_pos + 'px';
        detailPane.style.bottom = '';

        if (CARO.carousel.side == CARO.WEST) {
            detailPane.style.left = CARO.CONSTANTS.CARO_WIDTH - CARO.CONSTANTS.DETAIL_ENCROACHMENT + 'px';
            detailPane.style.right = '';
        }
        else {
            detailPane.style.left = '';
            detailPane.style.right = CARO.CONSTANTS.CARO_WIDTH - CARO.CONSTANTS.DETAIL_ENCROACHMENT + 'px';
        }
    }
}

CARO.setupHoverPane = function () {
    var detailPane = CARO.getDetailPane();
    detailPane.lastHoverId = 0;
    detailPane.hoveredObjectId = '';
}

CARO.startHoverTimer = function () {
    if (CARO.caroMouseState.grabbedTile) {
        // No hover details while dragging.
        return;
    }
    var detailPane = CARO.getDetailPane();
    if (detailPane.isInHoverMode) {
        CARO.showTileDetailPane(this, false);
    }
    else {
        detailPane.hoveredObjectId = this.id;
        var expr = 'CARO.checkHoverTimer(' + (++detailPane.lastHoverId) + ')';
        setTimeout(expr, CARO.CONSTANTS.HOVER_DELAY_TO_SHOW_DETAILS);
    }
}

CARO.checkHoverTimer = function (hoverId) {
    var detailPane = CARO.getDetailPane();
    if (hoverId == detailPane.lastHoverId) {
        // Mouse has not left the object during the timer.  
        if (detailPane.hoveredObjectId) {
            var obj = document.getElementById(detailPane.hoveredObjectId);
            if (obj) {
                detailPane.isInHoverMode = true;
                CARO.showTileDetailPane(obj, true);
            }
        }
    }
}

CARO.endDetailHoverMode = function () {
    var detailPane = CARO.getDetailPane();
    if (detailPane.isInHoverMode) {
        // ANIM.fadeObj(detailPane, 1, 0, CARO.CONSTANTS.DETAIL_FADE_IN_OUT_DURATION, 0);
        ANIM.animate(detailPane, 'fade', CARO.CONSTANTS.DETAIL_PANE_MAX_OPACITY, 0, CARO.CONSTANTS.DETAIL_FADE_IN_OUT_DURATION, 0, 'CARO.getDetailPane().style.display = \'none\'');
    }
    detailPane.isInHoverMode = false;
    detailPane.lastHoverId++;
}

CARO.preventBubbling = function (event) {
    event = event || window.event;
    if (event.stopPropagation) {
        event.stopPropagation();
    }
    else {
        event.cancelBubble = true;
    }
}

CARO.delayedlyEndDetailHoverMode = function () {
    var detailPane = CARO.getDetailPane();
    var expr = 'CARO.endDetailHoverIfNotOverTile(' + (++detailPane.lastHoverId) + ', "' + this.id + '")';
    setTimeout(expr, CARO.CONSTANTS.DETAIL_FADE_OUT_DELAY);
}

CARO.endDetailHoverIfNotOverTile = function (hId, objId) {
    var detailPane = CARO.getDetailPane();

    if ((detailPane.lastHoverId == hId) && (detailPane.idOfObj == objId) && (detailPane.lastInteraction <= (new Date()).getTime() - (1.01 * CARO.CONSTANTS.DETAIL_FADE_OUT_DELAY))) {
        CARO.endDetailHoverMode();
    }
}

CARO.setDetailText = function (detailPane, tile) {
    tile = tile.tileForm;
    detailPane.style.height = ''; // default to CSS control

    switch (tile.type) {
        case CARO.TILE_TYPES.OCL:
            detailPane.setAttribute('sizing', 'small');
            detailPane.dimmensions = CARO.CONSTANTS.DETAIL_PANE_DIMMS.SMALL;
            if (tile.compid == 0) {
                // Home Tile
                detailPane.textDiv.innerHTML =
                    '<p class="tileTitle">' + CARO_STRING_CONSTS.home + '</p>' +
                    '<p>' + CARO_STRING_CONSTS.homeDesc + '</p>';
            }
            else {
                detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.label + '</p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.application + '</label> ' + tile.appID + '</p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ' + ((tile.versID) ? tile.versID : CARO.CONSTANTS.UNKNOWN_VERSION_STRING) + '</p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.form + '</label> ' + tile.formID + '</p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.productCode + '</label> ' + ((tile.sysCode && tile.sysCode != 'null') ? tile.sysCode : '') + '</p>';
            }
            break;

        case CARO.TILE_TYPES.FAV:
            detailPane.setAttribute('sizing', 'small');
            detailPane.dimmensions = CARO.CONSTANTS.DETAIL_PANE_DIMMS.SMALL;
            if (tile.special) {
                detailPane.textDiv.innerHTML =
                    '<p class="tileTitle">' + CARO_STRING_CONSTS.manageFavo + '</p>' +
                    '<p>' + CARO_STRING_CONSTS.manageFavoritesDesc + '</p>';
            }
            else {
                if (tile.favInfo.appType == 'APP' || tile.favInfo.appType == '11') {
                    detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.label + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.application + '</label> ' + tile.favInfo.appID + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ' + ((tile.favInfo.version) ? tile.favInfo.version : CARO.CONSTANTS.UNKNOWN_VERSION_STRING) + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.form + '</label> ' + tile.favInfo.formID + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.productCode + '</label> ' + tile.favInfo.sysCode + '</p>';
                }
                else if (tile.favInfo.appType == 'UBE') {
                    detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.label + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.report + '</label> ' + tile.favInfo.appID + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ' + ((tile.favInfo.version) ? tile.favInfo.version : CARO.CONSTANTS.UNKNOWN_VERSION_STRING) + '</p>';
                }
                if (CARO.isOVR(tile.favInfo.appType)) {
                    detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.label + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.report + '</label> ' + tile.favInfo.ovrName + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.application + '</label> ' + tile.favInfo.appID + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ' + ((tile.favInfo.version) ? tile.favInfo.version : CARO.CONSTANTS.UNKNOWN_VERSION_STRING) + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.form + '</label> ' + tile.favInfo.formID + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.productCode + '</label> ' + tile.favInfo.sysCode + '</p>';
                }
                else if (CARO.isFolder(tile.favInfo.appType)) {
                    detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.label + '</p>';
                    if (tile.favInfo.childFavs && tile.favInfo.childFavs.length > 0) {
                        var contentsList = '<ul>';
                        for (var i = 0; i < Math.min(tile.favInfo.childFavs.length, 50); i++) {
                            contentsList += '<li>' + tile.favInfo.childFavs[i].taskName + '</li>';
                        }
                        contentsList += '</ul>';
                        detailPane.textDiv.innerHTML += contentsList;
                    }
                    else {
                        detailPane.textDiv.innerHTML += '<p class="detailItem">[' + CARO_STRING_CONSTS.empty + ']</p>';
                    }
                }
                // External apps lack a form; but, for consistency with OCL, we use the task label for any missing app and the never-present form value.
                else if (CARO.isExternalApp(tile.favInfo.appType)) {
                    var appIdForDisplay = tile.favInfo.appID;
                    if (appIdForDisplay == null || appIdForDisplay == "" || appIdForDisplay == "null") {
                        appIdForDisplay = tile.label;
                    }
                    detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.label + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.application + '</label> ' + appIdForDisplay + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ' + ((tile.favInfo.version) ? tile.favInfo.version : CARO.CONSTANTS.UNKNOWN_VERSION_STRING) + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.form + '</label> ' + tile.label + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.productCode + '</label> ' + tile.favInfo.sysCode + '</p>';
                }
                // else { alert(tile.favInfo.appType); }
            }

            break;

        case CARO.TILE_TYPES.RRPT:
            if (!tile.special) {
                detailPane.setAttribute('sizing', 'large');
                detailPane.dimmensions = CARO.CONSTANTS.DETAIL_PANE_DIMMS.LARGE;
                detailPane.textDiv.innerHTML =
                '<p class="tileTitle">' + tile.rptInfo.reportNameDesc + '</p>' +
                '<p class="detailItem"><label>' + CARO_STRING_CONSTS.report + '</label> ' + tile.rptInfo.reportNameDesc + ' (' + tile.rptInfo.reportName + ')</p>' +
                '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ' + tile.rptInfo.reportVersionDesc + ' (' + tile.rptInfo.reportVersion + ')</p>' +
                '<p class="detailItem"><label>' + CARO_STRING_CONSTS.status + ':</label> ' + tile.rptInfo.statusText + ' (' + tile.rptInfo.statusCode + ')</p>' +
                '<p class="detailItem"><label>' + CARO_STRING_CONSTS.dateSubmitted + '</label> ' + tile.rptInfo.timeSubmitted + '</p>' +
                '<p class="detailItem"><label>' + CARO_STRING_CONSTS.lastModified + ':</label> ' + tile.rptInfo.timeLastActivity + '</p>';
            }
            else {
                detailPane.setAttribute('sizing', 'small');
                detailPane.dimmensions = CARO.CONSTANTS.DETAIL_PANE_DIMMS.SMALL;
                if (WSJNewVersion == "")
                    detailPane.textDiv.innerHTML = '<p class="tileTitle"> ' + CARO_STRING_CONSTS.viewJobStatus + '</p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.application + '</label> P986110B</p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> </p>' +
                    '<p class="detailItem"><label>' + CARO_STRING_CONSTS.form + '</label> W986110BA</p>';
                else
                    detailPane.textDiv.innerHTML = '<p class="tileTitle"> ' + CARO_STRING_CONSTS.viewJobStatus + '</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.application + '</label> P986110B</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.version + '</label> ZJDE0001</p>' +
                        '<p class="detailItem"><label>' + CARO_STRING_CONSTS.form + '</label> W986110BA</p>';
            }
            break;

        default:
            detailPane.setAttribute('sizing', 'small');
            detailPane.dimmensions = CARO.CONSTANTS.DETAIL_PANE_DIMMS.SMALL;
            detailPane.textDiv.innerHTML = '<p class="tileTitle">' + tile.id + '</p>';
            break;
    }

    // Handle case where default (min) size is not tall enough.  
    var savedClassName = detailPane.className || '';
    var savedDisplay = detailPane.style.display || '';
    detailPane.className += ' stealthed';
    detailPane.style.display = 'block';

    var necessaryHeight = detailPane.textDiv.offsetHeight + 2 * CARO.CONSTANTS.DETAIL_PANE_VERT_MARGIN;
    if (necessaryHeight > detailPane.dimmensions.H) {
        detailPane.dimmensions = { W: detailPane.dimmensions.W,
            H: necessaryHeight + CARO.CONSTANTS.DETAIL_PANE_VERT_EXTRA
        };
        detailPane.style.height = necessaryHeight + 'px';
    }

    detailPane.style.display = savedDisplay;
    detailPane.className = savedClassName;
}

CARO.reRenderFavorites = function (passed_in_myFavObj) {
    var doc = document;
    var caroFavs = doc.getElementById('caroContentFav');
    var listFavs = doc.getElementById('listFavInner');

    var favs = null;
    if (passed_in_myFavObj) {
        favs = passed_in_myFavObj.favorites;
    }
    else {
        if (myFavoriteObject && myFavoriteObject.favorites) {
            favs = myFavoriteObject.favorites;
        }
    }


    if (!caroFavs) {
        /* Two possibilities:
        1. Initialization is not yet complete - this will be called again when it is.
        2. Favorites are locked by security settings.  We never need to call this again.
        */
        return;
    }

    CARO.reRenderFavoritesSpecificFolder(favs, caroFavs, listFavs, '');

    CARO.initializedFAV = true;
}

/**
* Determines if the provided task type is a folder type.
* 
* type - The task type
*/
CARO.isFolder = function (type) {
    // Check the type against known folder task types.
    if ((type == '07') /* Task Folder */ || (type == '21') /* User Defined Folder */) {
        return true;
    }
    return false;
}

/**
* Determines if the provided task type is a User folder type.
* type - The task type
*/
CARO.isUserFolder = function (type) {
    // Check the type against known folder task types.
    if (type == '21' /* User Defined Folder */) {
        return true;
    }
    return false;
}

/**
* Determines if the provided task type is a URL.
* type - The task type
*/
CARO.isURL = function (type) {
    // Check the type against known folder task types.
    if ((type == '08') /* Task url */) {
        return true;
    }
    return false;
}

/**
* Determines if the provided task type is a One View Report.
* type - The task type
*/
CARO.isOVR = function (type) {
    // Check the type against known folder task types.
    if ((type == '23') /* Task url */) {
        return true;
    }
    return false;
}

/**
* Determines if the provided task type is a External App.
* type - The task type
*/
CARO.isExternalApp = function (type) {
    // Check the type against external app task type.
    if ((type == '30' || type == '31') /* External app task */) {
        return true;
    }
    return false;
}

CARO.debugIntoFavs = function () {
    var caroFavs = document.getElementById('caroContentFav');
    CARO.debug(caroFavs.children);
}

CARO.setActiveAppTile = function (compId) {
    var doc = document;
    if (true) {
        /* Bulletproofing: if we couldn't find a specific last active tile, 
        go through every tile and mark it inactive. */

        var OCL = doc.getElementById('caroContentOCL');
        var oldTile;

        if (OCL != null && OCL.children.length > 1) {
            for (var i = 0; i < OCL.children.length; i++) {
                if (OCL.children[i].compid != compId) {
                    oldTile = OCL.children[i];
                    if (oldTile && oldTile.getAttribute('isActiveTile') == 'true') {
                        oldTile.setAttribute("isActiveTile", "");
                        oldTile.listForm.setAttribute("isActiveTile", "");
                        CARO.reComputeListText(oldTile, false);
                        UTIL.forceRedraw(oldTile);
                        UTIL.forceRedraw(oldTile.listForm);
                    }
                }
            }
        }
    }

    var newTile = doc.getElementById('caroOCL_' + compId);
    if (newTile) {
        newTile.setAttribute("isActiveTile", "true");
        newTile.listForm.setAttribute("isActiveTile", "true");
        CARO.reComputeListText(newTile, true);
        UTIL.forceRedraw(newTile);
        UTIL.forceRedraw(newTile.listForm);
    }

    CARO.oldActiveOCLTile = compId;
}

// Do the state-saving in a separate thread soas not to delay animations in a high-latency setting.
CARO.saveSetting = function (setting, value) {
    setTimeout(function () { CARO.saveSettingImpl(setting, value); }, 0);
}

CARO.saveSettingImpl = function (setting, value) {
    if (STATE) {
        if (!CARO.stateURL) {
            CARO.stateURL = STATE.createStateUrl()
        }

        STATE.sendStateUpdate(STATE.setXMLDocument(CARO.CONSTANTS.UISTATE_CAROUSEL, setting, value), CARO.stateURL);
    }
}

CARO.saveHiddenSectionsSetting = function () {
    //First we need to build this array from the existing data in the DOM
    CARO.caroHiddenDefaultSections = new Array();
    //Use the CARO.TABS.HIDDEN to get the element and then set the slot value after getting the element
    for (var i = 0; i < CARO.TABS.HIDDEN.length; i++) {
        //Walk through this array and add the slot value to the end of the name of the tab/section '_' delimeter
        var tabName = CARO.TABS.HIDDEN[i];
        var tab = document.getElementById('caroTab' + tabName);
        if (tab) {
            var tabData = tabName + '_' + tab.slot;
            CARO.singletonAddToArray(CARO.caroHiddenDefaultSections, tabData);
        }
    }
    var hiddenSections = CARO.join(CARO.caroHiddenDefaultSections, '|');
    // If there are no hidden default sections, set the setting to XML-safe 'nothing'.
    if (!hiddenSections) {
        hiddenSections = 'nil';
    }
    CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.HIDDEN_DEFAULT_SECTIONS, hiddenSections);
}

// Provides a batch save for UI state settings related to task folders.
CARO.saveTaskFolderSettings = function () {
    if (!CARO.caroInitFinished) {
        return;
    }

    if (CARO.taskFolderSettingsSaveScheduled != true) {
        CARO.taskFolderSettingsSaveScheduled = true;
        setTimeout(function () { CARO.saveTaskFolderSettingsImpl(); }, 1000);
    }
}

CARO.saveTaskFolderSettingsImpl = function () {
    var properties = [];

    // Expanded User Folders
    var expandedFolders = CARO.join(CARO.expandedUserFoldersArr, '|');
    if (!expandedFolders) {
        expandedFolders = 'nil';
    }
    properties.push(new STATEPROPERTY(CARO.CONSTANTS.STATE_VARS.EXPANDED_USER_FOLDERS, expandedFolders));

    // Open User Folders
    var openFolders = CARO.join(CARO.openUserFoldersArr, '|');
    if (!openFolders) {
        openFolders = "nil";
    }
    properties.push(new STATEPROPERTY(CARO.CONSTANTS.STATE_VARS.OPEN_USER_FOLDERS, openFolders));

    // Open Fav Folder Restore State IDs
    var openFavFolderIds = CARO.join(CARO.openFavFolderIds, '|');
    if (!openFavFolderIds) {
        openFavFolderIds = "nil";
    }
    properties.push(new STATEPROPERTY(CARO.CONSTANTS.STATE_VARS.OPEN_FAV_FOLDER_IDS, openFavFolderIds));

    // Section Order
    var sectionOrder = CARO.join(CARO.sectionOrderArray, '|');
    if (!sectionOrder) {
        // Since the setting is stored as as XML, mark 'no value' appropriately.
        sectionOrder = 'nil';
    }
    properties.push(new STATEPROPERTY(CARO.CONSTANTS.STATE_VARS.SECTION_ORDER, sectionOrder));

    // Open Sections Back Paths
    var backStackSetting = '';
    for (var pathId in CARO.openSectionsBackPaths) {
        if (backStackSetting.length > 0) {
            backStackSetting += '|';
        }
        backStackSetting += (CARO.join(CARO.openSectionsBackPaths[pathId], '~') + '~' + pathId.substr(9));
    }
    // If there are no paths in the setting, set the setting to XML-safe 'nothing'.
    if ((!backStackSetting) || (backStackSetting.length == 0)) {
        backStackSetting = "nil";
    }
    properties.push(new STATEPROPERTY(CARO.CONSTANTS.STATE_VARS.OPEN_SECTIONS_BACK_PATHS, backStackSetting));

    CARO.taskFolderSettingsSaveScheduled = false;

    CARO.saveSettingsInBatchImpl(properties);
}

CARO.saveSettingsInBatch = function (properties) {
    setTimeout(function () { CARO.saveSettingsInBatchImpl(properties); }, 0);
}

CARO.saveSettingsInBatchImpl = function (properties) {
    if (STATE) {
        if (!CARO.stateURL) {
            CARO.stateURL = STATE.createStateUrl()
        }

        STATE.sendStateUpdate(STATE.setXMLDocumentForBatch(CARO.CONSTANTS.UISTATE_CAROUSEL, properties), CARO.stateURL);
    }
}

CARO.redefineTileSizeDependantConstants = function () {
    CARO.CONSTANTS.TILE_SIZE = (CARO.USE_LARGE_ICONS) ? 80 : 40;
    CARO.CONSTANTS.TILE_SPACING = (CARO.USE_LARGE_ICONS) ? 4 : 24;
    CARO.CONSTANTS.CAPTION_SPACE_MAX_PX = (CARO.USE_LARGE_ICONS) ? 78 : 58;
    CARO.CONSTANTS.OFFSET_PER_TILE = CARO.CONSTANTS.TILE_SIZE + CARO.CONSTANTS.TILE_SPACING;
    CARO.CONSTANTS.CARO_HEIGHT = (CARO.USE_LARGE_ICONS) ? 81 : 56;
    CARO.CONSTANTS.COLLAPSED_POS_BY_SIDE = (CARO.USE_LARGE_ICONS) ? [-84, -188, -84, -188] : [-56, -188, -56, -188];
    CARO.CONSTANTS.TILE_IMAGE_SIZE = (CARO.USE_LARGE_ICONS) ? 68 : 38;
    CARO.CONSTANTS.TILE_CAPTION_LINE_HEIGHT = (CARO.USE_LARGE_ICONS) ? 14 : 12;
}

CARO.setTileImage = function (tile, imageUrlLarge) {
    tile.tileImg.setAttribute('src', imageUrlLarge);
    tile.tileImg.src = imageUrlLarge;
}

CARO.setTileSize = function (large) {
    var sizeStr = (large) ? "large" : "small";
    var newUseLargeIcons = (large) ? 1 : 0;

    if (CARO.USE_LARGE_ICONS != newUseLargeIcons) {
        var doc = document;
        CARO.USE_LARGE_ICONS = newUseLargeIcons;
        CARO.redefineTileSizeDependantConstants();

        CARO.carousel.setAttribute('tilesize', sizeStr);
        doc.getElementById('e1AppFrameContainer').setAttribute('tilesize', sizeStr);

        var contentHolder = doc.getElementById('caroContentHolder');
        for (var i = 0; i < contentHolder.children.length; i++) {
            var content = contentHolder.children[i];
            if (!content.isAScroller) {
                CARO.setDesiredPositions(content);
                CARO.snapToDesiredPositions(content);
            }
        }

        CARO.decideWhetherToHaveArrowsCaro();

        CARO.useSizeAppropriateIcons();
        CARO.redoAllTileCaptions();
    }
}

CARO.useSizeAppropriateIcons = function () {
    var caroContentHolder = document.getElementById('caroContentHolder');
    for (var i = 0; i < caroContentHolder.children.length; i++) {
        var content = caroContentHolder.children[i];
        for (var j = 0; j < content.children.length; j++) {
            var tile = content.children[j];

            var urlToUse = (CARO.USE_LARGE_ICONS) ? tile.imgUrlLarge : tile.imgUrlMed;
            if (urlToUse) {
                CARO.setTileImage(tile, urlToUse);
            }
        }
    }
}

CARO.decideOnPanArrowsForAllLists = function () {
    var lists = document.getElementById('listContentHolder');

    for (var i = 0; i < lists.children.length; i++) {
        var listContent = lists.children[i].children[1].children[0];
        CARO.decideWhetherToHaveArrowsList(listContent);
    }
}

CARO.deleteFromDom = function (obj) {
    if (obj && obj.parentNode) {
        obj.parentNode.removeChild(obj);
    }
}

CARO.countNumberOfRenderedFavorites = function () {
    if ((!myFavoriteObject) ||
         (!myFavoriteObject.favorites) ||
         (myFavoriteObject.favorites.length == 0)) {
        return 0;
    }

    /* The favorites array has some entry/entries, but it/they may not all 
    be of a type that gets rendered in carousel (e.g., there may be 
    some folders). */

    var favsArray = myFavoriteObject.favorites;
    var count = 0;

    for (var i = 0; i < favsArray.length; i++) {
        var type = favsArray[i].appType;

        /* Expected types:
        *   'APP' - Application
        *   'UBE' - Batch Job
        *   '11'  - UDC
        *   '07'  - Menu Folder (TODO)
        *   '21'  - User Folder 
        */

        if ((type == 'APP') || (type == 'UBE') || (CARO.isFolder(type)) || (type == '11')) {
            count++;
        }
    }

    return count;
}

CARO.activateFavoriteFolder = function (tile, silent, stillActivateTab) {
    // Check if the tile has all of it's child data loaded (secondLevelChildrenLoaded).
    // If not, go to the server and get that information.
    if ((tile) && (tile.favInfo) && (!tile.favInfo.secondLevelChildrenLoaded)) {
        // This server call will re-call activateFavoriteFolder once the server 
        // comes back with the child favorite data.
        loadChildFavoritesForTile(tile, silent);
        return;
    }

    if (tile.isEmpty) {
        return;
    }

    var whichCaro = 'FavFolder_' + tile.favInfo.uniqueId;
    var tabId = 'caroTab' + whichCaro;
    var listId = 'list' + whichCaro;
    var doc = document;

    // Start the safety valve here.  Everything above this 
    // should be extra-safe to run.
    try {
        var horizontal = (CARO.isHorizontal());
        var tab = document.getElementById(tabId);

        if ((!silent) && (!tab)) {
            // We need to add this folder to the list of open folders.

            CARO.singletonAddToArray(CARO.openUserFoldersArr, tile.favInfo.uniqueId);
        }

        // Ingore the section activation if the section is being recycled.
        if (tab) // Folder is open 
        {
            // The tab already exists; hence, the folder is already open
            if (horizontal) {
                if (tab.onclick && !silent) {
                    // Simulate click on the tab
                    tab.onclick(null);
                }
            }
            else // Vertical case
            {
                var list = doc.getElementById('list' + whichCaro);
                if (!list.expanded && !silent) {
                    // Expand the list if it is contracted
                    list.children[0].onclick(null);
                }

                CARO.glowList(list);
            }
        }
        else {
            // We have to create the tab/section

            // Step 0: Change the icon
            CARO.changeFolderIcon(tile, true);

            // Step 1: Create the actual horizontal content
            var hContent = UTIL.createElement('div', null, 'caroContent' + whichCaro);
            hContent.getWidth = CARO.getContentWidth;
            hContent.CMID = 0;
            hContent.computeMinMaxX = CARO.contentComputeMinMaxX;
            hContent.style.display = 'none';
            hContent.folderId = tile.favInfo.trueTaskId;
            if (CARO.isUserFolder(tile.favInfo.appType)) {
                hContent.isUFContent = true; //Is user folder content type
            }
            else {
                hContent.isTFContent = true; //Is task folder content type                
            }
            var hContHolder = doc.getElementById('caroContentHolder');
            if (hContHolder) {
                hContHolder.appendChild(hContent);
            }
            else {
                // We have serious problems
                return;
            }

            // Step 2: Create the tab handle
            var tabBar = doc.getElementById('caroTabBar');
            var theTabBackControl = null;
            // The backPathId is used to identify the section's call stack in the 
            // user settings.  It will always be set to the task ID of the folder
            // currently open in the section (i.e. top of the call stack).
            var backPathId = tile.favInfo.trueTaskId;

            var tileParent = (tile.parentElement === undefined) ? tile.parentNode : tile.parentElement;


            // Check to see if we are recycling an existing section.
            var launchNewSection = (((tileParent == null) /* task folder being re-opened on startup */
                              || (tileParent.id == 'caroContentFav') /* the tile is in the Favorites section */)
                              && (!tile.backButtonPressed) /* the user pressed the back button to navigate back up on level */);
            if (launchNewSection) {
                // We need to create a new tab. from scratch.

                tab = UTIL.createElement('div', 'caroTab', tabId);
                tab.setAttribute('whichCaro', whichCaro);
                if (CARO.isFolder(tile.tileForm.favInfo.appType)) {
                    if (CARO.isUserFolder(tile.tileForm.favInfo.appType)) {
                        tab.isUserFolderTab = true;
                        tab.dropCapable = true; //Adding for organize favorites to allow for favorites to be dropped into the folder           
                        tab.dropPending = false;
                    }
                }

                tab.appendChild(UTIL.createElement('div', 'caroTabL', null));
                tab.appendChild(tab.middle = UTIL.createElement('div', 'caroTabM', null));
                // Add an 'up one level' control to the tab.
                var backControl = UTIL.createElement('a', 'backFolderControl', 'back' + tabId);
                backControl.backStack = []; // The call stack of tiles to retrace when using the back button.
                backControl.whichCaro = whichCaro;
                if (tileParent == null) {
                    // We're restoring a folder on startup.

                    // Rebuild the path stack to enable the back button to walk all the way back up to the child
                    // folder saved in the user's favorites folder.
                    try {
                        // Get the current section's path from the user setting retrieved from the server.
                        // backPathId is used to manage the back path setting in the user setting.
                        var pathArray = CARO.openSectionsBackPaths['pathArray' + backPathId.replace(/[^\w]/gi, '_')];
                        // Cycle through the path and load the path data to the call stack used by the back control.
                        for (var index = 1; index < pathArray.length; index++) {
                            // Create a parent tile for each level of the path.
                            var tempTile = CARO.createTile(pathArray[index], CARO.TILE_TYPES.FAV);
                            // Add the minimum path information to the parent tile's fav information. 
                            // This is just enough information to retrieve all of the favorite information
                            // from the server if needed.

                            var pathArrayComponents = pathArray[index].split('^');
                            var parentPathArrayComponents = pathArray[index - 1].split('^');
                            tempTile.favInfo = new Object();
                            tempTile.favInfo.taskView = (pathArrayComponents[1]) ? pathArrayComponents[1] : '';
                            tempTile.favInfo.trueTaskId = pathArrayComponents[0];
                            tempTile.favInfo.parentTaskId = parentPathArrayComponents[0];
                            // Add the level to the call stack.
                            backControl.backStack.push(tempTile);
                        }
                    }
                    catch (error) {
                        // If any problem is encountered while rebuilding the back control path stack, clear it.
                        // This should never happen with good data, but will disable the back control, but otherwise
                        // make the sections fully functional.
                        backControl.backStack = [];
                    }
                    // Based on the contents of the stack, enable or disable the back control.
                    if (backControl.backStack.length > 0) {
                        backControl.setAttribute('ctlDisabled', 'false');
                    }
                    else {
                        backControl.setAttribute('ctlDisabled', 'true');
                    }
                }
                else {
                    backControl.setAttribute('ctlDisabled', 'true'); // Since we're opening a new tab normally, there are no more levels back to go to, so mark it as disabled.
                }
                backControl.onclick =
                backControl.ontouchend = CARO.backSectionControlClicked; // The button handler.
                backControl.onkeydown = CARO.keyDown;
                backControl.setAttribute('tabindex', '0'); // allow keyboard access
                backControl.setAttribute('role', 'button'); // screen reader accessibility
                backControl.setAttribute('title', CARO_STRING_CONSTS.back); // screen reader accessibility
                backControl.setAttribute('aria-labelledby', listId + 'HeaderText'); // screen reader accessibility
                backControl.currentTile = tile; // This information will be used to recycle the tab navigating both back and forward.
                backControl.currentFolderId = whichCaro.substr(10); // This information will be used to recycle the tab navigating both back and forward.
                // These values need to be populated on creation to support the case where sections are restored on startup.
                backControl.backTabId = tabId; // This value is used to identify the tab to replace when the back button is pressed.
                backControl.backListId = listId; // This value is used to identify the tab to replace when the back button is pressed.
                // This information will be used to reopen the tab on startup. This needs to be set for new tabs as well to make sure the item is removed 
                // from the open fav task folders collection.
                backControl.currentFavFolderTaskId = backControl.currentFolderId + '~' + backControl.currentTile.favInfo.trueTaskId + '~' + backControl.currentTile.favInfo.parentTaskId + '~' + backControl.currentTile.favInfo.taskView;
                var backWrapper = UTIL.createElement('div', 'caroTabR', null);
                // The back control is used to help build the list section.
                theTabBackControl = backControl;
                backControl.title = CARO_STRING_CONSTS.back;
                backWrapper.appendChild(backControl);
                if (backControl.getAttribute('ctlDisabled') == 'true') {
                    backWrapper.style.display = 'none';
                }
                tab.appendChild(backWrapper);
                var spacer = UTIL.createElement('div', 'caroTabR', null);  // adding some space between the backFolderControl and closeFolderControl
                spacer.style.width = (CARO.CONSTANTS.TAB_RIGHT_WIDTH / 2) + 'px';
                tab.appendChild(spacer);
                var closeControl = UTIL.createElement('a', 'closeFolderControl', 'close' + tabId);
                closeControl.whichCaro = whichCaro;
                closeControl.parentTile = tile;
                closeControl.backPathId = backPathId;
                closeControl.currentFolderId = whichCaro.substr(10); // This information will be used to recycle the tab navigating both back and forward.
                closeControl.currentFavFolderTaskId = backControl.currentFavFolderTaskId;
                closeControl.onclick =
                closeControl.ontouchend = CARO.closeSectionControlClicked;
                closeControl.onkeydown = CARO.keyDown;
                closeControl.setAttribute('tabindex', '0'); // allow keyboard access
                closeControl.setAttribute('role', 'button'); // screen reader accessibility
                closeControl.setAttribute('title', CARO_STRING_CONSTS.close + ' ' + CARO_STRING_CONSTS.folder); // screen reader accessibility
                closeControl.setAttribute('aria-labelledby', listId + 'HeaderText'); // screen reader accessibility
                var closeWrapper = UTIL.createElement('div', 'caroTabR', null);
                closeControl.title = CARO_STRING_CONSTS.close + ' ' + CARO_STRING_CONSTS.fav + ' ' + CARO_STRING_CONSTS.folder;
                closeWrapper.appendChild(closeControl);
                tab.appendChild(closeWrapper);
                tab.appendChild(UTIL.createElement('div', 'caroTabR', null));

                tab.fullText = tile.favInfo.taskName;
                tab.middle.innerHTML = tile.favInfo.taskName + CARO.CONSTANTS.TAB_PADDING_SUFFIX_STRING;
                // if accessibility
                tab.middle.setAttribute('tabindex', '0');
                tab.middle.setAttribute('role', 'link');
                tab.middle.setAttribute('aria-label', tile.favInfo.taskName + ' ' + CARO_STRING_CONSTS.tab);
                tab.middle.onkeydown = CARO.keyDown;

                // Start out invisible
                tab.style.opacity = 0;
                tab.style.filter = 'alpha(opacity=0)';

                // Hook up tab
                tab.onclick = CARO.tabClicked;
                tab.onmousedown = CARO.mouseDownOnSection;
                if (CARO.isTouchEnabled) {
                    tab.ontouchend = CARO.tabClicked;
                    tab.ontouchstart = CARO.mouseDownOnSection;
                }

                // We have built the tab, now insert it right before the last tab (the show/hide)
                tab.slot = tabBar.children.length - 1; // The full length will reflect the slot position because we haven't inserted yet.
                tab.isTab = true;
                tab.actualX = 0;
                var showHideTabs = tabBar.children[tabBar.children.length - 1];
                tabBar.insertBefore(tab, showHideTabs);
            }
            else {
                // We need to recycle the existing tab.
                var oldTabId = '';
                // If we navigating backward, we need to get that information from the tile. 
                if (tile.backButtonPressed) {
                    oldTabId = tile.backTabId;
                }
                else {
                    // Get the ID of the current tab we need to replace.
                    oldTabId = 'caroTab' + tileParent.id.substring('caroContent'.length);
                }
                // Get the object to replace.
                tab = document.getElementById(oldTabId);
                // Reset the ID.
                tab.id = tabId;
                tab.setAttribute('whichCaro', whichCaro);

                // Get 'up one level' control.
                var backCtrl = document.getElementById('back' + oldTabId);
                // The back control is used to help build the list section.
                theTabBackControl = backCtrl;

                // Remove the tab we are replacing from the list of open folders.
                // We'll only need to do this if we're recycling a folder that was
                // launched brand new, but we always do it to err on the side of caution.
                CARO.singletonRemoveFromArray(CARO.openUserFoldersArr, backCtrl.currentFolderId);
                // Remove the tab we are replacing from the list of open favorite task folders.
                CARO.singletonRemoveFromArray(CARO.openFavFolderIds, 'nil');
                CARO.singletonRemoveFromArray(CARO.openFavFolderIds, backCtrl.currentFavFolderTaskId);

                // Reset the current tab's parent icon to closed.
                CARO.changeFolderIcon(backCtrl.currentTile, false);

                // If we're moving forward, add the tile we are replacing to the call stack.
                if (!tile.backButtonPressed) {
                    backCtrl.backStack.push(backCtrl.currentTile);
                    CARO.saveBackPathSetting(backCtrl.backStack, backPathId);
                    // Activate the back button now that we have somewhere to go back up to.
                    backCtrl.setAttribute('ctlDisabled', 'false');
                    backCtrl.parentNode.style.display = 'inline-block';
                }

                // Reset the back control's state information to reflect the new tab content.
                backCtrl.id = 'back' + tabId;
                backCtrl.whichCaro = whichCaro;
                backCtrl.backTabId = tabId; // This value is used to identify the tab to replace when the back button is pressed.
                backCtrl.backListId = listId; // This value is used to identify the tab to replace when the back button is pressed.
                backCtrl.currentTile = tile;
                backCtrl.currentFolderId = whichCaro.substr(10);
                backCtrl.currentFavFolderTaskId = backCtrl.currentFolderId + '~' + backCtrl.currentTile.favInfo.trueTaskId + '~' + backCtrl.currentTile.favInfo.parentTaskId + '~' + backCtrl.currentTile.favInfo.taskView;

                // Add the new tab to the list of open favorite task folders.
                CARO.singletonAddToArray(CARO.openFavFolderIds, backCtrl.currentFavFolderTaskId);

                // Reset the close button control to reflect the new tab content.
                var closeCtrl = document.getElementById('close' + oldTabId);
                // Remove the previous call stack from the user settings.
                // We replaced it above with a new one keyed off the of the 
                // new folder's Id.
                CARO.removeBackPathFromSetting(closeCtrl.backPathId);

                closeCtrl.id = 'close' + tabId;
                closeCtrl.whichCaro = whichCaro;
                closeCtrl.currentFolderId = whichCaro.substr(10);
                closeCtrl.currentFavFolderTaskId = backCtrl.currentFavFolderTaskId;
                closeCtrl.parentTile = tile;
                closeCtrl.backPathId = backPathId;

                // If the stack is now empty (path levels are popped off the stack in the control handler),
                // disable the control.
                if ((backCtrl.backStack) && (backCtrl.backStack.length == 0)) {
                    backCtrl.setAttribute('ctlDisabled', 'true');
                    backCtrl.parentNode.style.display = 'none';
                    // Since the stack is exhausted, remove the path from the user setting.
                    CARO.removeBackPathFromSetting(closeCtrl.backPathId);
                    CARO.saveBackPathSetting([]); // Triggers a rebuild of the back path setting for all sections.
                }

                // Reset the tab label.
                tab.fullText = tile.favInfo.taskName;
                tab.middle.innerHTML = tile.favInfo.taskName + CARO.CONSTANTS.TAB_PADDING_SUFFIX_STRING;
                // if accessibility
                tab.middle.setAttribute('tabindex', '0');
                tab.middle.setAttribute('role', 'link');
                tab.middle.setAttribute('aria-label', tile.favInfo.taskName + ' ' + CARO_STRING_CONSTS.tab);

                // Mark that tab as being recycled.
                tab.reloadingContent = true;
            }

            // Now that we have everything in place, force a redraw to make sure I.E. updates
            // positions correctly.
            UTIL.forceRedraw(tabBar);

            // Compute new tab positions.
            CARO.computeTabPositions(tabBar);

            // Slide tabs to their positions.  
            CARO.slideTabsToPositions(tabBar);

            // Only fade in the tab if we opened it new.
            if (launchNewSection) {
                // Fade in new tab
                ANIM.fadeObj(tab, 0, 1, CARO.CONSTANTS.TAB_FADE_DURATION, 0.8 * CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TABS, true);
            }

            // Click the new tab if we are in horizontal mode and not silent, or if we want it active
            if (((horizontal) && (!silent)) || (stillActivateTab)) {
                tab.onclick(null);
            }

            // CARO.markTileNormal(tile);
            // CARO.markTileNormal(tile.listForm);

            // Step 3: Create the list section

            if (launchNewSection) {
                var listSection = UTIL.createElement('div', 'caroList', 'list' + whichCaro);
                var listHeader = UTIL.createElement('div', 'listHeader collapsed', 'list' + whichCaro + 'Header');
                var listArrowBox = UTIL.createElement('div', 'listArrowBox', null);
                var listArrow = UTIL.createElement('div', 'listArrow', 'list' + whichCaro + 'Arrow');
                var textHelper = UTIL.createElement('div', 'listHeaderTextPositionHelper', null);
                var headerText = UTIL.createElement('span', 'listHeaderText', 'list' + whichCaro + 'HeaderText');
                var listCloseCtrl = UTIL.createElement('a', 'closeFolderControl', 'list' + whichCaro + 'CloseCtrl');
                var listOuter = UTIL.createElement('div', 'listContentOuter', 'list' + whichCaro + 'Outer');
                var listInner = UTIL.createElement('div', 'listContentInner', 'list' + whichCaro + 'Inner');

                var listBackControl = UTIL.createElement('a', 'backFolderControl', 'listBacklist' + whichCaro);
                listBackControl.backStack = theTabBackControl.backStack; // The call stack of tiles to retrace when using the back button.  Should mirror the tab back stack.
                listBackControl.whichCaro = whichCaro;
                if (tileParent == null) {
                    // We're restoring a folder on startup.

                    // Based on the contents of the stack, enable or disable the back control.
                    if ((listBackControl.backStack) && (listBackControl.backStack.length > 0)) {
                        listBackControl.setAttribute('ctlDisabled', 'false');
                    }
                    else {
                        listBackControl.setAttribute('ctlDisabled', 'true');
                    }
                }
                else {
                    listBackControl.setAttribute('ctlDisabled', 'true'); // Since we're opening a new list section normally, there is no more levels back to go to, so mark it as disabled.
                }
                listBackControl.isList = true;
                listBackControl.onclick =
                listBackControl.ontouchend = CARO.backSectionControlClicked; // The button handler.
                listBackControl.onkeydown = CARO.keyDown;
                listBackControl.setAttribute('tabindex', '0'); // allow keyboard access
                listBackControl.setAttribute('role', 'button'); // screen reader accessibility
                listBackControl.setAttribute('title', CARO_STRING_CONSTS.back); // screen reader accessibility
                listBackControl.setAttribute('aria-labelledby', listId + 'HeaderText'); // screen reader accessibility
                listBackControl.currentTile = tile; // This information will be used to recycle the tab navigating both back and forward.
                listBackControl.currentFolderId = whichCaro.substr(10); // This information will be used to recycle the tab navigating both back and forward.
                listBackControl.backTabId = tabId; // This value is used to identify the tab to replace when the back button is pressed.
                listBackControl.backListId = listId; // This value is used to identify the tab to replace when the back button is pressed.
                // This information will be used to reopen the tab on startup. This needs to be set for new tabs as well to make sure the item is removed 
                // from the open fav task folders collection.
                listBackControl.currentFavFolderTaskId = listBackControl.currentFolderId + '~' + listBackControl.currentTile.favInfo.trueTaskId + '~' + listBackControl.currentTile.favInfo.parentTaskId + '~' + listBackControl.currentTile.favInfo.taskView;
                listBackControl.title = CARO_STRING_CONSTS.back;

                listSection.isUserFolder = true;
                listSection.userFolderId = tile.favInfo.uniqueId;

                listCloseCtrl.title = CARO_STRING_CONSTS.close;
                listCloseCtrl.whichCaro = whichCaro;
                listCloseCtrl.parentTile = tile;
                // We need to keep the list back control in sync with the tab.
                listCloseCtrl.backPathId = backPathId;
                listCloseCtrl.onclick =
                listCloseCtrl.ontouchend = CARO.closeSectionControlClicked;
                listCloseCtrl.onkeydown = CARO.keyDown;
                listCloseCtrl.setAttribute('tabindex', '0'); // allow keyboard access
                listCloseCtrl.setAttribute('role', 'button'); // screen reader accessibility
                listCloseCtrl.setAttribute('title', CARO_STRING_CONSTS.close + ' ' + CARO_STRING_CONSTS.folder); // screen reader accessibility
                listCloseCtrl.setAttribute('aria-labelledby', listId + 'HeaderText'); // screen reader accessibility
                listCloseCtrl.currentFolderId = whichCaro.substr(10);
                listCloseCtrl.currentFavFolderTaskId = listBackControl.currentFavFolderTaskId;

                listArrow.savedKnownWidth = 13;
                listArrow.savedKnownHeight = 13;

                listSection.appendChild(listHeader);
                listHeader.appendChild(listArrowBox);
                listArrowBox.appendChild(listArrow);
                textHelper.id = listId + 'PositionHelper';
                listHeader.appendChild(textHelper);
                textHelper.appendChild(listBackControl);
                textHelper.appendChild(headerText);
                textHelper.appendChild(listCloseCtrl);
                listSection.appendChild(listOuter);
                listOuter.appendChild(listInner);

                var listContentHolder = doc.getElementById('listContentHolder');
                if (listContentHolder) {
                    listContentHolder.appendChild(listSection);

                    // This needs to be done after the list section is added to the DOM, otherwise the control 
                    // dimensions are not correct.
                    CARO.assignNameToListHeader(headerText, tile.favInfo.taskName, listBackControl);

                    CARO.doPerListSetup(listSection);
                    listSection.slot = listContentHolder.children.length - 1;
                }
                else {
                    // We have a big problem.
                    return;
                }
            }
            else {
                // We need to recycle the existing list section.

                var oldListId = '';

                // If we navigating backward, we need to get that information from the tile. 
                if (tile.backButtonPressed) {
                    oldListId = tile.backListId;
                }
                else {
                    // Get the ID of the current tab we need to replace.
                    oldListId = 'list' + tileParent.id.substring('caroContent'.length);
                }

                var listSec = doc.getElementById(oldListId);
                var listHdr = doc.getElementById(oldListId + 'Header');
                var listArw = doc.getElementById(oldListId + 'Arrow');
                var headerTxt = doc.getElementById(oldListId + 'HeaderText');
                var listCloseControl = doc.getElementById(oldListId + 'CloseCtrl');
                var listOtr = doc.getElementById(oldListId + 'Outer');
                var listInr = doc.getElementById(oldListId + 'Inner');
                var listBackCntrl = doc.getElementById('listBack' + oldListId);

                if ((listBackCntrl.backStack) && (listBackCntrl.backStack.length == 0)) {
                    listBackCntrl.setAttribute('ctlDisabled', 'true');
                }

                // Since we're recycling the section, replace the section in the list of expanded folders user setting.
                CARO.removeSectionFromExpandedSectionsSetting(listBackCntrl.currentFolderId);

                // Reset the current tab's parent icon to closed.
                CARO.changeFolderIcon(listBackCntrl.currentTile, false);

                // If we're moving forward, add the tile we are replacing to the call stack.
                if (!tile.backButtonPressed) {
                    // Map the call stack of the tab control to reflect the newly added level.
                    listBackCntrl.backStack = theTabBackControl.backStack;
                    // Activate the back button now that we have somewhere to go back up to.
                    listBackCntrl.setAttribute('ctlDisabled', 'false');
                }

                listBackCntrl.whichCaro = whichCaro;
                listBackCntrl.backTabId = tabId; // This value is used to identify the tab to replace when the back button is pressed.
                listBackCntrl.backListId = listId; // This value is used to identify the tab to replace when the back button is pressed.
                listBackCntrl.onclick =
                listBackCntrl.ontouchend = CARO.backSectionControlClicked; // The button handler.
                listBackCntrl.currentTile = tile; // This information will be used to recycle the tab navigating both back and forward.
                listBackCntrl.currentFolderId = whichCaro.substr(10); // This information will be used to recycle the tab navigating both back and forward.
                // This information will be used to reopen the tab on startup. This needs to be set for new tabs as well to make sure the item is removed 
                // from the open fav task folders collection.
                listBackCntrl.currentFavFolderTaskId = listBackCntrl.currentFolderId + '~' + listBackCntrl.currentTile.favInfo.trueTaskId + '~' + listBackCntrl.currentTile.favInfo.parentTaskId + '~' + listBackCntrl.currentTile.favInfo.taskView;

                listSec.id = listId;
                listSec.userFolderId = tile.favInfo.uniqueId;
                listSec.reloadingContent = true;
                listSec.isUserFolder = true;
                listInr.reloadingContent = true;

                listHdr.id = listId + 'Header';
                listArw.id = listId + 'Arrow';
                headerTxt.id = listId + 'HeaderText';
                listOtr.id = listId + 'Outer';
                listInr.id = listId + 'Inner';
                listBackCntrl.id = 'listBack' + listId;

                listCloseControl.id = listId + 'CloseCtrl';
                listCloseControl.title = CARO_STRING_CONSTS.close + ' ' + CARO_STRING_CONSTS.fav + ' ' + CARO_STRING_CONSTS.folder;
                listCloseControl.whichCaro = whichCaro;
                listCloseControl.parentTile = tile;
                listCloseControl.backPathId = backPathId;
                listCloseControl.currentFolderId = whichCaro.substr(10);
                listCloseControl.currentFavFolderTaskId = listBackCntrl.currentFavFolderTaskId;

                CARO.assignNameToListHeader(headerTxt, tile.favInfo.taskName, listBackCntrl);
            }

            CARO.saveTaskFolderSettings();

            // Clear out back button state.
            tile.backButtonPressed = null;
            tile.backTabId = null;
            tile.backListId = null;

            // STEP 4: Content
            // Now that we've built content holders, we need to actually populate these containers
            CARO.reRenderFavoritesSpecificFolder(tile.favInfo.childFavs, hContent, (listInner) ? listInner : listInr, tile.favInfo.uniqueId + '_');

            // We wait to expand the list until we've had a chance to populate it with content.  
            // setTimeout("document.getElementById('"+listHeader.id+"').onclick(null)", CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST+1000);
            if (!silent) {
                if (listHeader) {
                    listHeader.onclick(null);
                }
                else {
                    listHdr.onclick(null);
                }
            }
        }
    }
    catch (error) {
        // The carousel can be crippled if this method tosses an exception.
        // Rendering improperly is a better condition that a loss of service
        // condition.
        CARO.log("Failed to activate favorite folder. Error: " + error);
        // Try to clean things up.
        CARO.obliterateSection(whichCaro);
        document.getElementById('caroTabFav').onclick(null);
    }

    // Activations happen serially after they've been pulled from the server asynchronously.  Always call the 
    // async processor once an activation is complete to see if another tab is waiting in the queue.
    CARO.openAsyncFavFolders();
}

// Removes the specified section from the list of expanded list sections in the user settings.
CARO.removeSectionFromExpandedSectionsSetting = function (sectionId) {
    CARO.singletonRemoveFromArray(CARO.expandedUserFoldersArr, sectionId);
    CARO.saveTaskFolderSettings();
}

// Calculates the necessary offset to account for the back button.  If the back
// button is visible, it will take up room in the text area.
CARO.calculateListHeaderBackControlTextAdjustment = function (backControl) {
    var txtAdjustment = 0;
    if (backControl) {
        if (backControl.getAttribute('ctlDisabled') == 'false') {
            txtAdjustment = CARO.CONSTANTS.BACK_CTRL_WIDTH;
        }
    }

    return txtAdjustment;
}

// Opens all asynchronously-loaded tabs in serial.  If there is a pending open operation, it will 
// activate that tab.  If all pending tabs are open, it will either finish initalizing the carousel
// or finalize the new tab state.
CARO.openAsyncFavFolders = function () {
    // Check if there are pending tabs to open.  If so, pop the next tile off of the stack and 
    // launch it.
    if (CARO.openFavTabTiles.length > 0) {
        var tempTile = CARO.openFavTabTiles.pop();
        CARO.activateFavoriteFolder(tempTile, true);
    }
    else {
        // If all tabs have been opened.

        // If the carousel has not finished intializing, complete that process.
        if (!CARO.caroInitFinished) {
            for (var i = 0; i < CARO.expandedUserFoldersArr.length; i++) {
                var listHeaderId = 'listFavFolder_' + CARO.expandedUserFoldersArr[i] + 'Header';
                var listHeader = document.getElementById(listHeaderId);
                if (listHeader) {
                    listHeader.onclick(null);
                }
            }

            // Load the order of carousel setions retrieved from the user
            // settings and load it to the carousel state.
            CARO.sectionOrderArray = caroInitState.caroSectionOrder.split('|');
            // Apply the retrieved order to the carousel sections.
            CARO.orderSectionsOnInit();

            // Safety
            setTimeout(CARO.fixCaroTabPositioning, 500);

            if ((caroInitState.caroActiveTab != 'OCL') &&
                (caroInitState.caroActiveTab != 'RR') &&
                (caroInitState.caroActiveTab != 'FAV')) {
                var tab = document.getElementById('caroTab' + caroInitState.caroActiveTab);
                if (tab) {
                    tab.onclick(null);
                }
            }

            //Added for Organize Favorites added functionality
            CARO.tabMarkedForDrop = null; //Initialize to null for droppability purposes
            CARO.possibleDropTarget = null; //Initialize for folder tile as drop target

            CARO.enableListResizingWhenAllCarosInitialized();

            // Now that all the tabs are in the proper order, make the bar visible.
            CARO.showCaro(document.getElementById('caroContent' + CARO.carousel.activeCaroStr));
            // Setting of tabBar class (tabBar.className = 'caroTabBar') is now in fixCaroTabPositioning;

            CARO.caroInitFinished = true;
            CARO.saveTaskFolderSettings();
        }
        // If the carousel has been fully initialized, then all that should be required
        // is to save the carousel tab state.
        else {
            // Save the tab order back to the server because we've added a
            // new tab.
            CARO.saveSectionOrder();
        }
    }
}

// Save the full path of open folders user setting back to the server. 
CARO.saveBackPathSetting = function (backStackArray, baseFolderTaskId) {
    // Transform the provided stack (this is the one path in the list of paths that has changed)
    // to an array of task IDs.  This is the info saved back to the server.
    var newStackArray = [];
    for (var i = 0; i < backStackArray.length; i++) {
        newStackArray.push((backStackArray[i].favInfo.trueTaskId) + "^" + (backStackArray[i].favInfo.taskView));
    }
    // Transform the modified path from a JS array to a delimited string and add it back 
    // to the full list of all paths.
    if (newStackArray.length > 0) {
        var pathArrayId = ((baseFolderTaskId.split('^'))[0]).replace(/[^\w]/gi, '_');
        // Replace the modified path in the JS object representation of the paths.
        CARO.openSectionsBackPaths['pathArray' + pathArrayId] = newStackArray;
    }
    CARO.saveTaskFolderSettings();
}

// Removes the specified path for a given section from the user setting saved on the server.
// This method does not save the user setting back to the server.
CARO.removeBackPathFromSetting = function (backStackFolderId) {
    delete CARO.openSectionsBackPaths['pathArray' + backStackFolderId.replace(/[^\w]/gi, '_')];
}

CARO.assignNameToListHeader = function (targetDomElement, fullText, backControl) {
    var textAdjustment = (backControl) ? CARO.calculateListHeaderBackControlTextAdjustment(backControl) : 0;
    var textThatFits = CARO.computeListHeaderTextToFit(fullText, textAdjustment);
    targetDomElement.innerHTML = textThatFits;

    if (textThatFits != fullText) {
        // If not all of the text fit, add a tool-tip with full text.
        targetDomElement.title = CARO.decodeText(fullText);
    }
    else {
        // Since this can be used to rename, we may need to remove an existing tooltip
        if (targetDomElement.title) {
            targetDomElement.title = '';
        }
    }
}

CARO.adjustTabWidths = function (tabBar) {
    var availableSpace = UTIL.pageWidth();
    var fixedWidthSpace = CARO.CONSTANTS.TAB_INITIAL_INDENT;
    var doc = document;

    var fixedTabIds = ['caroTabOCL', 'caroTabRecRpts', 'caroTabFav', 'showHideCaroTabs']
    for (var i = 0; i < fixedTabIds.length; i++) {
        var ftab = doc.getElementById(fixedTabIds[i]);
        if (ftab) {
            fixedWidthSpace += ftab.offsetWidth + CARO.CONSTANTS.TAB_SPACING;
            ftab.fixed = true;
        }
    }

    fixedWidthSpace -= CARO.CONSTANTS.TAB_SPACING;

    var flexSpaceAvail = availableSpace - fixedWidthSpace;

    flexSpaceAvail = Math.max(0, flexSpaceAvail);

    var desiredFlexSpace = 0;
    var minFlex = 0;

    var flexTabs = [];
    for (var i = 0; i < tabBar.children.length; i++) {
        var tab = tabBar.children[i];
        if (!tab.fixed) {
            minFlex += CARO.CONSTANTS.MIN_TAB_WIDTH;
            flexTabs.unshift(tab);
            tab.widthDecided = 0;
            //if (tab.desiredWidth)
            //{
            //    desiredFlexSpace += Math.min(tab.desiredWidth, CARO.CONSTANTS.MAX_TAB_WIDTH);
            //}
            //else
            //{
            // Because the back button is variably visible, recalc the desiredWidth on 
            // each pass.
            CARO.setDesiredWidthBasedOnFullText(tab);
            desiredFlexSpace += tab.desiredWidth;
            //}
        }
    }

    var numUndecided = flexTabs.length;
    var changeMade = true;
    var fairWidth;

    while (changeMade && (numUndecided > 0)) {
        changeMade = false;

        fairWidth = Math.min(Math.floor(Math.max(flexSpaceAvail / numUndecided, CARO.CONSTANTS.MIN_TAB_WIDTH)), CARO.CONSTANTS.MAX_TAB_WIDTH);

        for (var i = 0; i < flexTabs.length; i++) {
            var tab = flexTabs[i];
            if (!tab.widthDecided && (tab.desiredWidth <= fairWidth)) {
                flexSpaceAvail -= (tab.widthDecided = tab.desiredWidth);
                numUndecided--;
                changeMade = true;
            }
        }
    }

    // At this point, anything undecided will have to make do with quota.  

    for (var i = 0; i < flexTabs.length; i++) {
        var tab = flexTabs[i];
        if (!tab.widthDecided) {
            tab.widthDecided = Math.min(fairWidth, CARO.CONSTANTS.MAX_TAB_WIDTH);
        }

        tab.middle.style.width = '';
        if (tab.desiredWidth <= tab.widthDecided) {
            tab.middle.innerHTML = tab.fullText + CARO.CONSTANTS.TAB_PADDING_SUFFIX_STRING;
            tab.title = ''; // No tool-tip if full text fits
        }
        else {
            CARO.makeTabHaveWidth(tab, tab.widthDecided);
        }

        tab.style.width = Math.ceil(tab.fullWidthDecided = CARO.getRealTabWidth(tab)) + 'px';
    }
}

CARO.getRealTabWidth = function (tab) {
    // We can't just access tab.offsetWidth, nor can we take the cumulative sum of all
    // child widths because we are in the process of making updates to these quantities.  

    var lastChild = tab.children[tab.children.length - 1];
    if (!isRTL) {
        // Non RTL mode: find the right edge of the entire tab by finding right edge of right-most piece
        return lastChild.offsetLeft + lastChild.offsetWidth;
    }
    else {
        // RTL Mode: last child is the left-most piece.  Find the right edge of the right-most piece 
        // (firstChild), and subtract away the left edge of the left-most element (lastChild) to get tab width.

        var firstChild = tab.children[0];
        return (firstChild.offsetLeft + firstChild.offsetWidth) - lastChild.offsetLeft;
    }
}

CARO.makeTabHaveWidth = function (tab, width) {
    width = width - CARO.CONSTANTS.TAB_FOLDER_OVERHEAD;
    var sizerTab = CARO.getSizerTab();

    var str = tab.fullText;
    var backCtrl = document.getElementById('back' + tab.id);
    var variableControlAdjustment = 0;
    if (backCtrl) {
        variableControlAdjustment += backCtrl.offsetWidth;
    }
    width -= variableControlAdjustment;

    // See if just ditching the two nbsp's is enough.
    sizerTab.style.display = '';
    sizerTab.innerHTML = str;
    if (sizerTab.offsetWidth <= width) {
        // That was enough.
        tab.middle.innerHTML = str;
        tab.title = ''; // No tooltip if full text fits
        sizerTab.style.display = 'none';
        return;
    }

    // Will need a tool-tip with full text
    tab.title = CARO.decodeText(tab.fullText);

    var lowerLimit = 1;
    var upperLimit = str.length - 1;

    while (lowerLimit < upperLimit) {
        var tryLen = Math.ceil((upperLimit + lowerLimit) / 2);
        sizerTab.innerHTML = str.substr(0, tryLen) + '...';

        if (sizerTab.offsetWidth <= width) {
            lowerLimit = tryLen;
        }
        else {
            upperLimit = tryLen - 1;
        }
    }
    sizerTab.style.display = 'none';

    tab.middle.innerHTML = str.substr(0, lowerLimit) + '...';
    tab.middle.style.width = Math.min(width, tab.middle.offsetWidth) + 'px';
    if (tab.widthDecided <= CARO.CONSTANTS.MIN_TAB_WIDTH) {
        tab.middle.innerHTML = '';
    }
}


CARO.computeTabTextWidth = function (str) {
    var sizerTab = CARO.getSizerTab();

    sizerTab.style.display = 'inline';
    sizerTab.innerHTML = str;
    var width = sizerTab.offsetWidth;
    sizerTab.style.display = 'none';

    return width;
}

CARO.setDesiredWidthBasedOnFullText = function (tab) {
    return (tab.desiredWidth = CARO.computeTabTextWidth(tab.fullText + CARO.CONSTANTS.TAB_PADDING_SUFFIX_STRING) + CARO.CONSTANTS.TAB_FOLDER_OVERHEAD);
}

CARO.getSizerTab = function () {
    var sizerTab = CARO.tempTabMiddleSizer;
    if (!sizerTab) {
        sizerTab = UTIL.createElement('div', 'stealthed caroTab caroTabM');
        sizerTab.style.display = 'none';
        sizerTab.style.whiteSpace = 'nowrap';
        CARO.carousel.appendChild(sizerTab);

        CARO.tempTabMiddleSizer = sizerTab;
    }

    return sizerTab;
}

CARO.computeTabPositions = function (tabBar) {
    var vertical = ((CARO.carousel.side == CARO.WEST) || (CARO.carousel.side == CARO.EAST));
    var savedClassname = tabBar.className;
    var savedDisplay = tabBar.style.display;
    var temporarilyOverrideDisplay = false;
    if (vertical || (!CARO.carousel.expanded)) {
        temporarilyOverrideDisplay = true;
        tabBar.className += " stealthed";
        tabBar.style.display = 'block';
    }

    CARO.adjustTabWidths(tabBar);

    var offsetSoFar = CARO.CONSTANTS.TAB_INITIAL_INDENT;

    for (var i = 0; i < tabBar.children.length; i++) {
        if (vertical) {
            tabBar.style.display = 'block';
        }
        var tab = tabBar.children[i];
        tab.oldL = (tab.newL ? tab.newL : offsetSoFar);
        tab.newL = offsetSoFar;
        tab.logicalX = offsetSoFar;
        if (!tab.fullWidthDecided) {
            tab.fullWidthDecided = tab.offsetWidth;
        }
        if (tab.id == 'showHideCaroTabs') {
            tab.style.width = tab.offsetWidth + 'px';
        }
        offsetSoFar += tab.fullWidthDecided + CARO.CONSTANTS.TAB_SPACING;
        if (!tab.paddingOneTimeSet) {
            tab.style.paddingLeft = tab.style.paddingRight = '0px';
            tab.paddingOneTimeSet = true;
        }
        tab.style.display = ''; // return to css-control.
    }

    tabBar.oldCollapsedL = (tabBar.newCollapsedL) ? tabBar.newCollapsedL : 0;
    tabBar.newCollapsedL = (CARO.CONSTANTS.TAB_HIDER_ALLOWANCE - offsetSoFar);

    tabBar.style.width = offsetSoFar + 'px';
    if (temporarilyOverrideDisplay) {
        tabBar.className = savedClassname;
        if (savedDisplay) {
            tabBar.style.display = savedDisplay;
        }
        else {
            tabBar.style.display = '';
        }
    }
}

CARO.slideTabsToPositions = function (tabBar) {
    if (tabBar.showHideControl.expanded) {
        for (var i = 0; i < tabBar.children.length; i++) {
            var tab = tabBar.children[i];
            if (!isRTL) {
                ANIM.animate(tab, 'sinSlideX', tab.oldL, tab.newL, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TABS);
            }
            else {
                ANIM.animate(tab, 'sinSlideXright', tab.oldL, tab.newL, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TABS);
            }
        }
    }
    else {
        // If tabs are hidden, just snap to final locations.  
        CARO.snapTabsToPositions(tabBar);
        if (!isRTL) {
            tabBar.style.left = tabBar.newCollapsedL + 'px';
        }
        else {
            tabBar.style.right = tabBar.newCollapsedL + 'px';
        }
    }
}

CARO.snapTabsToPositions = function (tabBar) {
    for (var i = 0; i < tabBar.children.length; i++) {
        var tab = tabBar.children[i];
        if (!isRTL) {
            tab.style.left = tab.newL + 'px';
        }
        else {
            tab.style.right = tab.newL + 'px';
        }
    }
}

/* TODO: long-term, it would be good to merge the following function in with 
* CARO.reRenderFavorites.  For now, because there is limited time for testing,
* it seemed best not to make substantial changes to a tested, working API. */
CARO.didAddParentTask = false;

CARO.reRenderFavoritesSpecificFolder = function (favs, caroFavs, listFavs, prefix) {
    if (isNativeContainer) {
        if (NativeContainer) {
            NativeContainer.updateData(NativeContainer.PARAM_UPDATE_DATA_FAVORITES);
        }
    }

    if ((!(caroFavs && listFavs)) || (favoritesSecurity == 'hide')) {
        // A necessary component does not exist.
        return;
    }

    var isRoot = (caroFavs.id == 'caroContentFav');
    var needAdd = false;

    /* If this is the root level favorites, then on the first time through (detected because 
    the favorites carousel is empty), we need to create manageFavorites tile.  The exception
    to this is when favorites are readOnly.  Note that, when favorites are secured, this code
    never runs, so we don't need to chack for that case. */

    if (isRoot && (caroFavs.children.length == 0) && (favoritesSecurity != 'readOnly')) {
        var manFavsTile = CARO.createTile('manageFavs', CARO.TILE_TYPES.FAV);
        manFavsTile.special = 'manageFavs';
        manFavsTile.doNotMove = true;
        manFavsTile.dropCapable = false;
        manFavsTile.isNew = true;

        needAdd = true;

        CARO.labelTileAs(manFavsTile, CARO_STRING_CONSTS.manageFavo);
        manFavsTile.label = CARO_STRING_CONSTS.manageFavo;

        var imageUrlLarge = JDE_ICON_PREFIX + CARO_SPECIAL_ICONS.MANAGE_FAVS.BASENAME + JDE_ICON_SUFFIX.LARGE + CARO_SPECIAL_ICONS.MANAGE_FAVS.EXT;
        var imageUrlMedium = JDE_ICON_PREFIX + CARO_SPECIAL_ICONS.MANAGE_FAVS.BASENAME + JDE_ICON_SUFFIX.MEDIUM + CARO_SPECIAL_ICONS.MANAGE_FAVS.EXT;
        var imageUrlSmall = JDE_ICON_PREFIX + CARO_SPECIAL_ICONS.MANAGE_FAVS.BASENAME + JDE_ICON_SUFFIX.SMALL + CARO_SPECIAL_ICONS.MANAGE_FAVS.EXT;

        manFavsTile.imgUrlLarge = imageUrlLarge;
        manFavsTile.imgUrlMed = imageUrlMedium;

        manFavsTile.listForm.icon.src = imageUrlSmall;
        manFavsTile.listForm.textCell.innerHTML = CARO.generateListText(CARO_STRING_CONSTS.manageFavo);

        CARO.setTileImage(manFavsTile, (CARO.USE_LARGE_ICONS) ? imageUrlLarge : imageUrlMedium);

        CARO.silentAddTile(caroFavs, manFavsTile);
        CARO.silentAddTile(listFavs, manFavsTile.listForm);
    }

    // Mark everything for deletion.  Selectively un-mark what we find we'll keep.
    for (var i = 0; i < caroFavs.children.length; i++) {
        caroFavs.children[i].keepMe = false;
    }

    // Delete anything no longer needed.
    var needDel = false;

    // If we're recycling the list section, remove all of the existing items.
    if (listFavs.reloadingContent) {
        for (var j = listFavs.children.length - 1; j >= 0; j--) {
            var child = listFavs.children[j];
            needDel = true;
            child.keepMe = false;
            child.tileForm.ignoreForSpacing = true;
            CARO.delTileByIndex(listFavs, j);
        }
        listFavs.reloadingContent = false;
    }

    // If we find a manage favorites tile, keep it.
    if ((caroFavs.children.length >= 1) &&
        (caroFavs.children[0].special) &&
        (caroFavs.children[0].special == 'manageFavs')) {
        caroFavs.children[0].keepMe = true;
    }

    if (favs) {
        var numIgnored = 0;
        var oneOrMoreOpenFolderHasBeenRenamed = false;
        var doc = document;
        for (var i = 0; i < favs.length; i++) {
            if (CARO.isUserFolder(favs[i].appType)) {
                // This is a user-folder.  See if it is open, and update it if so.
                var horizFolderContent = doc.getElementById('caroContentFavFolder_' + favs[i].uniqueId);

                if (horizFolderContent) {
                    var vertFolderContent = doc.getElementById('listFavFolder_' + favs[i].uniqueId + 'Inner');
                    if (vertFolderContent) {
                        CARO.reRenderFavoritesSpecificFolder(favs[i].childFavs, horizFolderContent, vertFolderContent, favs[i].uniqueId + '_');
                    }
                }
            }
            else {
                if (!CARO.didAddParentTask) {
                    var caroFavElement = document.getElementById('caroContentFav');
                    if (caroFavElement && favs[i].parentTaskId) {
                        caroFavElement.favsParentTaskId = favs[i].parentTaskId;
                        CARO.didAddParentTask = true;
                    }
                }
            }

            var favSuffix = prefix + favs[i].uniqueId;
            var tileId = CARO.TILE_TYPES.TYPE_TO_ID_PREFIX[CARO.TILE_TYPES.FAV] + favSuffix;
            var tile = document.getElementById(tileId);
            if (tile && tile.parentNode != caroFavs) {
                // This isn't the tile we're looking for.
                tile = null;
            }
            if (!tile) {
                // Does not yet exist - we must create it
                var tile = CARO.createTile(favSuffix, CARO.TILE_TYPES.FAV);

                if (favoritesSecurity == 'readOnly') {
                    tile.doNotMove = true;
                }

                CARO.silentAddTile(caroFavs, tile);
                CARO.silentAddTile(listFavs, tile.listForm);

                tile.isNew = true;
                needAdd = true;
            }

            tile.keepMe = true;

            tile.favInfo = favs[i];
            tile.favIndex = i;
            tile.label = tile.favInfo.taskName;
            CARO.labelTileAs(tile, tile.favInfo.taskName, tile.favInfo.appID, (tile.favInfo.formID || tile.favInfo.appID));

            var special_icon_suffix = '';

            if (CARO.isFolder(tile.favInfo.appType)) {
                //Set this as droppable - Organize Favorites
                tile.dropCapable = true;

                // Special icon if it's open.           
                var tab = document.getElementById('caroTabFavFolder_' + tile.favInfo.uniqueId);
                if (tab) {
                    //Open icon for folder
                    special_icon_suffix = '_open';
                    // Test for folder to have been renamed
                    if (tile.favInfo.taskName != tab.fullText) {
                        // The folder has been renamed.  

                        // First, update the horizontal form tab.
                        tab.fullText = tile.favInfo.taskName;
                        CARO.setDesiredWidthBasedOnFullText(tab);

                        /* The length of the tab has probably changed, but we will defer
                        * moving/resizing tabs until we have caught all renamed tabs. 
                        * For now, set a flag noting that we must do this later. */
                        oneOrMoreOpenFolderHasBeenRenamed = true;

                        /* Now update the listForm.  All of the following should exist, 
                        * since we know the folder is open, but just to be safe, use 
                        * a try-catch.  */
                        try {
                            var listHeader = document.getElementById('listFavFolder_' + tile.favInfo.uniqueId + 'Header');
                            var listHeaderTextObj = listHeader.children[1].children[0];
                            CARO.assignNameToListHeader(listHeaderTextObj, tile.favInfo.taskName);
                        }
                        catch (excep) {
                            CARO.log('Horizontal and vertical forms out of sync: cannot find list version of fav folder ' + tile.favInfo.uniqueId);
                        }
                    }
                }

                // Special icon if it's empty.
                var childFavsMissing = (!tile.favInfo.childFavs) ? true : (tile.favInfo.childFavs.length == 0);

                if (CARO.isFolder(tile.favInfo.appType) && (childFavsMissing == true)) {
                    special_icon_suffix = '_empty';
                    if (CARO.isUserFolder(tile.favInfo.appType)) {
                        CARO.attemptCloseFolderForFavTile(tile);
                    }
                    tile.isEmpty = true;
                }
                else {
                    tile.isEmpty = false;
                }
            }

            var imageUrlLarge = JDE_ICON_PREFIX + tile.favInfo.iconFile + special_icon_suffix + JDE_ICON_SUFFIX.LARGE + tile.favInfo.iconExt;
            var imageUrlMedium = JDE_ICON_PREFIX + tile.favInfo.iconFile + special_icon_suffix + JDE_ICON_SUFFIX.MEDIUM + tile.favInfo.iconExt;
            var imageUrlSmall = JDE_ICON_PREFIX + tile.favInfo.iconFile + special_icon_suffix + JDE_ICON_SUFFIX.SMALL + tile.favInfo.iconExt;

            tile.imgUrlLarge = imageUrlLarge;
            tile.imgUrlMed = imageUrlMedium;

            CARO.setTileImage(tile, (CARO.USE_LARGE_ICONS) ? imageUrlLarge : imageUrlMedium);
            tile.listForm.icon.src = imageUrlSmall;

            /* Force this to be at the right point in the array.  Specifically, we care that
            * any favorites that are not going to get deleted are in the correct relative 
            * order.  We don't care about the order of the ones that will get deleted.
            *
            * Therefore, it suffices to put each of these at the end of the list.
            */
            try {
                listFavs.removeChild(tile.listForm);
                listFavs.appendChild(tile.listForm);
            }
            catch (error) {
                CARO.log('Error removing a tile from the favorites listFavs.  Message: ' + error);
            }

            caroFavs.removeChild(tile);
            caroFavs.appendChild(tile);


            tile.slot = i + 1 - numIgnored;
        }

        if (oneOrMoreOpenFolderHasBeenRenamed) {
            var tabBar = document.getElementById('caroTabBar');

            // Compute new tab positions.
            CARO.computeTabPositions(tabBar);

            // Slide tabs to their positions.  
            CARO.slideTabsToPositions(tabBar);
        }
        for (var i = caroFavs.children.length - 1; i >= 0; i--) {
            var child = caroFavs.children[i];
            var listChild = listFavs.children[i];
            if ((!child.keepMe)) {
                needDel = true;
                // If it is a folder that is being deleted, we must make sure it is closed.
                CARO.attemptCloseFolderForFavTile(child); // It is ok to call this method on non-folders.                
                child.ignoreForSpacing = true;
                listChild.tileForm.ignoreForSpacing = true;
                listFavs.removeChild(listChild);
                caroFavs.removeChild(child);
            }
        }
    }

    CARO.setDesiredPositions(caroFavs);
    CARO.setDesiredPositionsList(listFavs);

    var delayToRearrange = (needDel) ? (4 / 5 * CARO.CONSTANTS.TILE_FADE_DURATION) : 0;
    var delayToFadeInNew = delayToRearrange;
    var delayToFadeInNewList = delayToRearrange;

    // Snap any new nodes to their final position
    for (var i = 0; i < caroFavs.children.length; i++) {
        var tile = caroFavs.children[i];
        if (tile.isNew) {
            var lItm = tile.listForm;

            tile.style.left = (tile.actualX = tile.desiredX) + 'px';
            lItm.style.top = (lItm.actualY = lItm.desiredY) + 'px';
        }
    }

    // TODO: desired pos for contents.

    if (!caroFavs.actualX) {
        caroFavs.style.left = (caroFavs.actualX = caroFavs.desiredX) + 'px';
    }

    if (CARO.desiredPositionsDifferFromActual(caroFavs)) {
        delayToFadeInNew += CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_TILES;
        CARO.slideToDesiredPositions(caroFavs, delayToRearrange);
    }

    if (CARO.desiredPositionsDifferFromActualList(listFavs)) {
        delayToFadeInNewList += CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST;
        CARO.slideToDesiredPositionsList(listFavs, delayToRearrange);
    }

    if (needDel) {
        setTimeout('CARO.slideIfNecessary(document.getElementById(\'' + listFavs.id + '\'))', delayToFadeInNewList);
    }

    if (needAdd) {
        for (var i = 0; i < caroFavs.children.length; i++) {
            if (caroFavs.children[i].isNew) {
                ANIM.fadeObj(caroFavs.children[i], 0, 1, CARO.CONSTANTS.TILE_FADE_DURATION, delayToFadeInNew, true);
                ANIM.fadeObj(caroFavs.children[i].listForm, 0, 1, CARO.CONSTANTS.TILE_FADE_DURATION, delayToFadeInNewList, true);
                caroFavs.children[i].isNew = false;
            }
        }
    }

    CARO.decideWhetherToHaveArrowsCaro();
    CARO.decideOnPanArrowsForAllLists();
}

/* Attempts to find and close an open carousel tab/list section 
associated with a favorite tile */
CARO.attemptCloseFolderForFavTile = function (tile) {
    try {

        if (CARO.isFolder(tile.tileForm.favInfo.appType) /* User Folder or Task Folder */) {
            // Find the tab for the favorite (if it exists)
            var doc = document;
            var tabCloseControl = doc.getElementById('closecaroTabFavFolder_' + tile.favInfo.uniqueId);
            if (tabCloseControl) {
                // Looks like it is open.  Simulate a click of the X
                tabCloseControl.onclick(new Object());
            }
            var listCloseControl = doc.getElementById('listFavFolder_' + tile.favInfo.uniqueId + 'CloseCtrl');
            if (listCloseControl) {
                // Looks like it is open.  Simulate a click of the X
                listCloseControl.onclick(new Object());
            }

        }
    }
    catch (excep) {
        CARO.log('Encountered an unexpected error in CARO.closeFolderForFavTile: ' + excep)
    }
}

CARO.closeSectionControlClicked = function (event) {
    CARO.preventBubbling(event);

    // Remove the back path from the user setting.
    CARO.removeBackPathFromSetting(this.backPathId);
    CARO.saveBackPathSetting([]); // Triggers a rebuild of the back path setting for all sections.

    // Persist the closure
    CARO.singletonRemoveFromArray(CARO.openUserFoldersArr, this.currentFolderId);
    // Remove the section from the list of open favorite task folders.
    CARO.singletonRemoveFromArray(CARO.openFavFolderIds, this.currentFavFolderTaskId);
    // Remove the section from the list of expanded list sections.
    CARO.removeSectionFromExpandedSectionsSetting(this.currentFolderId);

    CARO.saveTaskFolderSettings();

    // Change the icon back.
    CARO.changeFolderIcon(this.parentTile, false);

    // Fade out list section.  
    var listSection = document.getElementById('list' + this.whichCaro);
    ANIM.fadeObj(listSection, 1, 0, CARO.CONSTANTS.TAB_FADE_DURATION);

    // Fade out the tab itself.
    var tab = document.getElementById('caroTab' + this.whichCaro);
    ANIM.fadeObj(tab, 1, 0, CARO.CONSTANTS.TAB_FADE_DURATION);

    // If the active tab is the one being closed, need to set a different active tab.
    // This must be done even if we are currently vertical.
    if (CARO.carousel.activeCaroStr == this.whichCaro) {
        // Use Favorites tab as the one to which we go.  
        document.getElementById('caroTabFav').onclick(null);
    }

    // As of the coding of this line, the two constants were the same.
    var delay_to_delete = Math.max(CARO.CONSTANTS.TAB_FADE_DURATION, CARO.CONSTANTS.SWAP_CARO_FADE_DURATION) + 75;

    setTimeout("CARO.obliterateSection('" + this.whichCaro + "')", delay_to_delete);
}

// Handle the pressing of the 'up one level' button.
CARO.backSectionControlClicked = function (event) {
    CARO.preventBubbling(event);

    // The user clicked the back button on a tab section.
    if (!this.isList) {
        // If the tab isn't the active tab, ignore the back control 
        // click and instead activate the tab.
        var doc = document;
        var tab = doc.getElementById('caroTab' + this.whichCaro);
        var tabBar = doc.getElementById('caroTabBar');
        if (tab != tabBar.activeTab) {
            tab.onclick(null);
            return;
        }
    }
    else {
        // The user clicked the back button on a list section.
        // No special handling required.
    }

    // If there are levels in the call stack, activate the next level in the stack.
    if (this.backStack) {
        var nextTab = null;
        try {
            // Just being careful.
            if (this.backStack.length > 0) {
                // Check if the next tab in the stack is already open.  If so, we're just going to activate it,
                // rather than recycling the current tab.  That affects how we query from the back stack.
                nextTab = document.getElementById('caroTabFavFolder_' + this.backStack[this.backStack.length - 1].favInfo.uniqueId);
            }
        }
        catch (error) {
            CARO.log(error);
        }

        // Get the previous tile from the call stack.
        if (nextTab) {
            // If the next tile is already open, don't remove the next tile from the stack.
            this.parentTile = this.backStack[this.backStack.length - 1];
        }
        else {
            this.parentTile = this.backStack.pop();

            // If a previous section was found, store state that indicates and enables the 
            // backward navigation.
            if (this.parentTile) {
                this.parentTile.backButtonPressed = true;
                this.parentTile.backTabId = this.backTabId;
                this.parentTile.backListId = this.backListId;
            }
        }
        // Activate the previous section from the call stack.
        if (this.parentTile) {
            CARO.activateFavoriteFolder(this.parentTile);
        }
    }
}

CARO.obliterateSection = function (section) {
    // First, delete the offending entities.
    var doc = document;
    var caroContent = doc.getElementById('caroContent' + section);
    var tab = doc.getElementById('caroTab' + section);
    var list = doc.getElementById('list' + section);

    CARO.removeElem(caroContent);
    CARO.removeElem(tab);
    CARO.removeElem(list);

    // Second, redo spacing.
    var tabBar = doc.getElementById('caroTabBar');
    CARO.computeTabPositions(tabBar);
    CARO.slideTabsToPositions(tabBar);

    CARO.computeHeightsForListSections();
    if (CARO.carousel.side == CARO.EAST || CARO.carousel.side == CARO.WEST) {
        CARO.actGraduallyOnHeightsForListSections(0, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST, 0);
    }

    // After a section has been removed, we need to make sure the slot values 
    // are up to date.  We do this on a different thread to not impact
    // animations.
    setTimeout("CARO.recalculateSlotValues()", 100);
}

// Cycles through the contents of all UI representations of the 
// carousel and resets the slot values (used for section positioning) for
// each section to reflect the section's current position in the 
// carousel.
CARO.recalculateSlotValues = function () {
    var doc = document;
    var listContainer = doc.getElementById('listContentHolder');
    var tabBar = doc.getElementById('caroTabBar');
    for (var i = 0; i < listContainer.children.length; i++) {
        listContainer.children[i].slot = i;
        tabBar.children[i].slot = i;
    }
    // Assume the method is being called because 
    // tabs were added or removed and save the 
    // tab order back to the server.
    CARO.saveSectionOrder();
}

CARO.removeElem = function (elem) {
    if (elem && elem.parentNode) {
        elem.parentNode.removeChild(elem);
    }
}

CARO.slideListsWhereNecessary = function () {
    var listHolder = document.getElementById('listContentHolder');
    if (listHolder) {
        for (var i = 0; i < listHolder.children.length; i++) {
            CARO.slideIfNecessary(listHolder.children[i].children[1].children[0]);
        }
    }
}

CARO.changeFolderIcon = function (tile, isOpen) {
    var special_icon_suffix = isOpen ? '_open' : '';

    var imageUrlLarge = JDE_ICON_PREFIX + tile.favInfo.iconFile + special_icon_suffix + JDE_ICON_SUFFIX.LARGE + tile.favInfo.iconExt;
    var imageUrlMedium = JDE_ICON_PREFIX + tile.favInfo.iconFile + special_icon_suffix + JDE_ICON_SUFFIX.MEDIUM + tile.favInfo.iconExt;
    var imageUrlSmall = JDE_ICON_PREFIX + tile.favInfo.iconFile + special_icon_suffix + JDE_ICON_SUFFIX.SMALL + tile.favInfo.iconExt;

    tile.imgUrlLarge = imageUrlLarge;
    tile.imgUrlMed = imageUrlMedium;

    CARO.setTileImage(tile, (CARO.USE_LARGE_ICONS) ? imageUrlLarge : imageUrlMedium);
    tile.listForm.icon.src = imageUrlSmall;
}

CARO.glowList = function (list) {
    // TODO: eventually should specify these values via CSS
    list.children[1].style.backgroundColor = '#EBF1F9';
    list.style.backgroundColor = '#50a8ff';

    for (var i = 0; i < list.children.length; i++) {
        ANIM.fadeObjSin(list.children[i], 1, 0.7, CARO.CONSTANTS.GLOW_LIST_INTIME);
        ANIM.fadeObjSin(list.children[i], 0.7, 1, CARO.CONSTANTS.GLOW_LIST_OUTTIME, CARO.CONSTANTS.GLOW_LIST_INTIME);
    }

    setTimeout(function () { list.style.backgroundColor = ''; list.children[1].style.backgroundColor = '' }, CARO.CONSTANTS.GLOW_LIST_INTIME + CARO.CONSTANTS.GLOW_LIST_OUTTIME);
}

CARO.computeListHeaderTextToFit = function (str, adjustmentWidth) {
    var width = CARO.CONSTANTS.LIST_SECTION_TEXT_MAX_LENGTH;
    // If the caller specifies an adjustment value, apply
    // it to the width before calculating the text.
    if (adjustmentWidth) {
        width -= adjustmentWidth;
    }
    var sizer = CARO.getListSectionHeadingSizer();
    sizer.style.display = '';

    sizer.innerHTML = str;
    if (sizer.offsetWidth <= width) {
        // Full text fits.
        sizer.style.display = 'none';
        return str;
    }

    // Need to do a binary search and find how much fits.
    var lowerLimit = 0;
    var upperLimit = str.length - 1;

    while (lowerLimit < upperLimit) {
        var tryLen = Math.ceil((upperLimit + lowerLimit) / 2);
        sizer.innerHTML = str.substr(0, tryLen) + '...';

        if (sizer.offsetWidth <= width) {
            lowerLimit = tryLen;
        }
        else {
            upperLimit = tryLen - 1;
        }
    }
    sizer.style.display = 'none';

    return str.substr(0, lowerLimit) + '...';
}

CARO.getListSectionHeadingSizer = function () {
    var sizer = CARO.listSectionHeaderTextSizer;
    if (!sizer) {
        CARO.listSectionHeaderTextSizer = sizer = UTIL.createElement('span', 'stealthed listHeaderTextSizer');
        sizer.style.display = 'none';
        CARO.carousel.appendChild(sizer);
    }

    return sizer;
}

CARO.singletonAddToArray = function (arr, item) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == item) {
            // It's already there: abort;
            return;
        }
    }

    // If we made it here, the item is not yet in the array.
    arr.push(item);
}

CARO.singletonRemoveFromArray = function (arr, item) {
    // Assume the item is in the list at most once.  
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == item) {
            // Found it.  Kill it.
            arr.splice(i, 1);
        }
    }
}

/* TODO: calls to this should be changed to use javascript built-in
*    join function.  That is, replace calls with arr.join(delim); */
CARO.join = function (arr, delim) {
    var stringBuffer = '';
    for (var i = 0; i < arr.length; i++) {
        if (i != 0) {
            stringBuffer += delim;
        }
        stringBuffer += arr[i];
    }

    return stringBuffer;
}

CARO.fixCaroTabPositioning = function () {
    var tabBar = document.getElementById('caroTabBar');
    CARO.computeTabPositions(tabBar);
    CARO.snapTabsToPositions(tabBar);
    tabBar.className = 'caroTabBar';
}

CARO.redoAllTileCaptions = function () {
    var contentHolder = document.getElementById('caroContentHolder');
    if (!contentHolder) {
        return;
    }

    for (var i = 0; i < contentHolder.children.length; i++) {
        var content = contentHolder.children[i];
        if (!content.isAScroller) {
            // This is an actual content
            for (var j = 0; j < content.children.length; j++) {
                CARO.redoTileCaption(content.children[j]);
            }
        }
    }
}

CARO.redoTileCaption = function (tile) {
    if (!tile) {
        return;
    }

    var tcs = tile.trueCaptionState;
    if (tcs) {
        CARO.labelTileAs(tile, tcs.textual, tcs.appId, tcs.formId, tcs.active);
    }
}

CARO.fixTabAlignmentNow = function () {
    var tabBar = document.getElementById('caroTabBar');
    CARO.computeTabPositions(tabBar);
    CARO.snapTabsToPositions(tabBar);
}

//Shows a tab/section that was hidden previously
//Either in this user session or another.
CARO.showDefaultTab = function (tabName) {
    //Gotta show tab and section both
    //Moving hidden tab name to the hidden tabs variable
    CARO.singletonAddToArray(CARO.TABS.SHOWN, tabName);
    CARO.singletonRemoveFromArray(CARO.TABS.HIDDEN, tabName);

    //Move back to the right elements
    CARO.moveFromHideShowHolder(tabName)

    //Save hidden tab/section state to server
    CARO.saveHiddenSectionsSetting();
}

//Hides a tab/section that is being shown at this moment in time.
CARO.hideDefaultTab = function (tabName) {
    //Gotta hide tab and section both

    //Moving hidden tab name to the hidden tabs variable
    CARO.singletonAddToArray(CARO.TABS.HIDDEN, tabName);
    CARO.singletonRemoveFromArray(CARO.TABS.SHOWN, tabName);

    //Move the tab/section
    CARO.moveToHideShowHolder(tabName);

    //Save hidden tab/section state to server
    CARO.saveHiddenSectionsSetting();
}

CARO.moveToHideShowHolderSilent = function (section) {
    var doc = document;
    var hideShowHolder = doc.getElementById('caroHideShowHolder');
    if (hideShowHolder) {
        var tabBar = doc.getElementById('caroTabBar');
        var tab = doc.getElementById('caroTab' + section);
        var list = doc.getElementById('list' + section);
        var caroContent = doc.getElementById('caroContent' + section);

        hideShowHolder.appendChild(tab);
        hideShowHolder.appendChild(list);

        // After a section has been removed, we need to make sure the slot values 
        // are up to date.  We do this on a different thread to not impact
        // animations.
        setTimeout("CARO.recalculateSlotValues()", 0);
    }
}


//Moves the tab/section (both) to an element of the DOM
//that holds hidden tab/section(s).
CARO.moveToHideShowHolder = function (section) {

    //This will handle collapsing a list item that is not collapsed 
    //first then call this method again to move everything after
    //the collapse is complete.
    var doc = document;
    var list = doc.getElementById('list' + section);
    if (list && list.expanded) {
        CARO.expandCollapseListNode(list.id);
        setTimeout('CARO.moveToHideShowHolder("' + section + '")', CARO.CONSTANTS.GLOW_LIST_OUTTIME); //pause long enough for the list to collapse.
        return;
    }

    var hideShowHolder = doc.getElementById('caroHideShowHolder');
    if (hideShowHolder) {

        var tabBar = doc.getElementById('caroTabBar');
        var tab = doc.getElementById('caroTab' + section);
        var caroContent = doc.getElementById('caroContent' + section);

        hideShowHolder.appendChild(tab);
        hideShowHolder.appendChild(list);
        CARO.hideCaro(caroContent);

        //Check to see if the hiding tab/section is the active tab
        if (tab == tabBar.activeTab) {
            tab.setAttribute('active', "false");
            tabBar.activeTab = tabBar.children[0];
            if (tabBar.activeTab) {
                tabBar.activeTab.setAttribute('active', "true");
                tabBar.activeTab.onclick(null);
                var whichCaro = tabBar.activeTab.getAttribute('whichcaro');
                CARO.loadContent(whichCaro);
                CARO.saveSetting(CARO.CONSTANTS.STATE_VARS.ACTIVE_TAB, whichCaro);
            }
        }
        // After a section has been removed, we need to make sure the slot values 
        // are up to date.  We do this on a different thread to not impact
        // animations.
        setTimeout("CARO.recalculateSlotValues()", 0);

        //Redo spacing.
        CARO.computeTabPositions(tabBar);
        CARO.slideTabsToPositions(tabBar);

        CARO.computeHeightsForListSections();
        if (CARO.carousel.side == CARO.EAST || CARO.carousel.side == CARO.WEST) {
            CARO.actGraduallyOnHeightsForListSections(0, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST, 0);
        }
    }
}

//Moves the tab/section both from an element of the DOM
//that holds hidden tab/section(s).
CARO.moveFromHideShowHolder = function (section) {
    var doc = document;
    var tab = doc.getElementById('caroTab' + section);
    var list = doc.getElementById('list' + section);

    //if the tab and list exist within the hide show holder element then we can move it back
    if (tab && list && tab.parentNode.id == 'caroHideShowHolder') {
        var tabBar = doc.getElementById('caroTabBar');
        var listHolder = doc.getElementById('listContentHolder');
        if (tab.slot >= listHolder.children.length) {
            tabBar.insertBefore(tab, tabBar.children[tabBar.children.length - 1]);
            listHolder.appendChild(list);
        }
        else {
            tabBar.insertBefore(tab, tabBar.children[tab.slot]);
            listHolder.insertBefore(list, listHolder.children[tab.slot]);
        }
    }
    //Reset the slot values after adding it to the tabBar
    CARO.recalculateSlotValues()

    CARO.computeTabPositions(tabBar);
    CARO.slideTabsToPositions(tabBar);

    CARO.computeHeightsForListSections();
    if (CARO.carousel.side == CARO.EAST || CARO.carousel.side == CARO.WEST) {
        CARO.actGraduallyOnHeightsForListSections(0, CARO.CONSTANTS.SLIDE_TO_DESIRED_DURATION_LIST, 0);
        //Attempt to expand the list here.
        if (list && !list.expanded) {
            var expr = 'CARO.expandCollapseListNode("' + list.id + '")';
            setTimeout(expr, CARO.CONSTANTS.EXPAND_COLLAPSE_CAROUSEL_DURATION_LIST);
        }
    }
}

// Added building the context menuing using the webgui.js functionality.
// Private: render the drop down hide/show tab/section Context Menu
CARO.updateDropdownMenuView = function (elem, event) {
    var parentMenuId = 'caroTabContextMenu';
    var dropdownMenu = document.getElementById(parentMenuId);
    if (!dropdownMenu)
        return;

    // clean up all the menu item in One View and all sub menus

    CARO.removeChildren(dropdownMenu);

    jdeWebGUIstartMenu(parentMenuId);

    //Create the hide show sections with the actual hide/show tab/sections within it.
    CARO.addHideShowSections(parentMenuId, true);


    // get html for all menu
    var html = jdeWebGUIwriteMenu(true);
    // move all new item into One View
    var newDiv = document.createElement("div");
    newDiv.innerHTML = html;
    var innerDiv = newDiv.firstChild;
    while (innerDiv.firstChild) {
        dropdownMenu.appendChild(innerDiv.firstChild);
    }
    moveItemShadow(dropdownMenu);
    //Time to show the menu
    jdeWebGUIdoMenu(elem, parentMenuId, event);
    jdeWebGUImenuStack[0] = parentMenuId;
    //stop event bubbling
    if (event.preventDefault) { //meaning is Firefox
        event.stopPropagation();
        event.preventDefault();
        if (event.cancelBubble) {
            event.cancelBubble = true;
        }
    }
    else {
        event.cancelBubble = true;
        event.returnValue = false;
    }
    return false;
}

CARO.removeChildren = function (parentNode) {
    while (parentNode.childNodes.length > 0) {
        var child = parentNode.firstChild;
        parentNode.removeChild(child);
    }
}

// Render Hide and Show sections
// return false if there is not Hide Show sections at all
// false should never be returned.
CARO.addHideShowSections = function (parentMenuId) {
    var doc = document;
    // Hide section
    if (CARO.TABS.SHOWN.length > 1)//This means there are tab/sections to hide (SHOULD ALWAYS BE TRUE)
    {
        CARO.createMenuSection(parentMenuId, 'hide');
        //Add each hide tab/section item here
        //Shown tabs (If there is only one tab/section left it will not be capable of being hidden)
        for (var i = 0; i < CARO.TABS.SHOWN.length; i++) {
            var tab = doc.getElementById('caroTab' + CARO.TABS.SHOWN[i]);
            if (CARO.TABS.SHOWN.length > 1) {
                jdeWebGUIaddMenuItem(parentMenuId, tab.children[1].innerHTML, "javascript:CARO.hideDefaultTab('" + CARO.TABS.SHOWN[i] + "')", "MENU", "caroShown_" + CARO.TABS.SHOWN[i] + "_" + i, "Menu Item", "", false, "", "", "false", "");
            }
            else {
                jdeWebGUIaddMenuItem(parentMenuId, tab.children[1].innerHTML, "", "DISABLED_MENU", "caroShown_" + CARO.TABS.SHOWN[i] + "_" + i, "Menu Item", "", false, "", "", "false", "");
            }
        }
    }


    // seperator
    if (CARO.TABS.SHOWN.length > 1 && CARO.TABS.HIDDEN.length > 0) {
        CARO.addMenuSeperator(parentMenuId);
    }

    // Show section
    if (CARO.TABS.HIDDEN.length > 0) {
        CARO.createMenuSection(parentMenuId, 'show');

        //Add each show tab/section item here
        for (var i = 0; i < CARO.TABS.HIDDEN.length; i++) {
            var tab = doc.getElementById('caroTab' + CARO.TABS.HIDDEN[i]);
            if (tab) {
                jdeWebGUIaddMenuItem(parentMenuId, tab.children[1].innerHTML, "javascript:CARO.showDefaultTab('" + CARO.TABS.HIDDEN[i] + "')", "MENU", "caroHidden_" + CARO.TABS.HIDDEN[i] + "_" + i, "Menu Item", "", false, "", "", "false", "");
            }
        }
    }

}

// Add a menu seperator into the current menu
CARO.addMenuSeperator = function (parentMenuId) {
    jdeWebGUIaddMenuItem(parentMenuId, "-", "", "SEPARATOR", "CARO_MENU_SEP", "Menu Item", "", false, "", "", "false", "");
}

// Create a section (Hide or Show) in the current menu
CARO.createMenuSection = function (parentMenuId, action) {
    // add a label for hide or show
    switch (action) {
        case 'hide':
            jdeWebGUIaddMenuItem(parentMenuId, CARO_STRING_CONSTS.hide, "", "MENU", "FormRowLabels", "Menu Item", "", false, "", "", "false", "");
            break;
        case 'show':
            jdeWebGUIaddMenuItem(parentMenuId, CARO_STRING_CONSTS.show, "", "MENU", "FormRowLabels", "Menu Item", "", false, "", "", "false", "");
            break;
        default:
            return;
    }
}

/* This function is an attempt at solving an issue which causes desktop UI being displayed on iPad in native container.
* Conjecture: NativeContainer.css which is used to hide carousel on iPad is not delivered sometimes causing above issue. Dropping carousel component altogether on iPad was not an option as it is closely tied with carousel/dog-ear in container.
*/
CARO.forceHideCarousel = function () {
    var carousel, e1AppFrameContainer;
    var doc = document;
    carousel = doc.getElementById('carousel');
    carousel.style.display = "none";
    e1AppFrameContainer = doc.getElementById('e1AppFrameContainer');
    e1AppFrameContainer.style.left = "0px";
    e1AppFrameContainer.style.right = "0px";
    e1AppFrameContainer.style.top = "0px";
    e1AppFrameContainer.style.bottom = "0px";
}

// Prior to 9.2, the 'extraInfArr' did not exist.
// Adding to the end of the parameter list accomplishes both of the following:
//  1) allows the new version of the E1 Pages generator (jdeflow.js) to call the old ICONSERVICE (new parameter will be ignored)
//  2) allows the old version of the E1 Pages generator to work with the new ICONSERVICE (new parameter won't be sent)
ICONSERVICE.getIcons = function (appIdArr, formIdArr, pCodeArr, callback, extraInfArr) {
    if ((appIdArr.length + formIdArr.length + pCodeArr.length) == 0) {
        // Don't bother with XML if the list is empty.
        setTimeout(function () { callback([]) }, 0);
        return;
    }

    var xmlReq = ICONSERVICE.getXMLRequest();
    var url = ICONSERVICE.getURL(appIdArr.join('|'), formIdArr.join('|'), pCodeArr.join('|'), extraInfArr ? extraInfArr.join('|') : '');

    xmlReq.onreadystatechange = ICONSERVICE.getReadyStateHandler(xmlReq, callback);

    xmlReq.open('GET', url, false);
    xmlReq.send('');
}

// request handler for ajax (similar to JDEDTA and RIAF)
ICONSERVICE.getReadyStateHandler = function (req, responseHandler) {
    // Return an anonymous function that listens to the 
    // XMLHttpRequest instance
    return function () {
        // If the request's status is "complete"
        if (req.readyState == 4 && req.status == 200) {
            var response = req.responseText;
            // Example: response = default_app|financial_mgmt:png|png

            if (!response) {
                return;
            }

            var iconArrays = response.split(':');
            if (iconArrays.length < 2) {
                return;
            }

            var basenames = (iconArrays[0]).split('|');
            var extensions = (iconArrays[1]).split('|');
            var extraInf = iconArrays[2] ? (iconArrays[2]).split('|') : '';

            var iconObjects = [];
            for (var i = 0; i < basenames.length; i++) {
                iconObjects.push({ basename: basenames[i], ext: extensions[i], extra: extraInf[i] });
            }

            responseHandler(iconObjects);
        }
    }
}

ICONSERVICE.createURLFactory = function () {
    var e1UrlCreator = _e1URLFactory.createInstance(parent._e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI('IconService');
    return e1UrlCreator;
}

ICONSERVICE.getURL = function (appIds, formIds, pCodes, extra) {
    // Need to get a new urlFactory each time so that parameters correctly
    // take on replacement values
    var urlFactory = ICONSERVICE.createURLFactory();

    urlFactory.setParameter('appIds', appIds)
    urlFactory.setParameter('formIds', formIds)
    urlFactory.setParameter('productCodes', pCodes);
    urlFactory.setParameter('extraInfo', extra);

    return urlFactory.toString();
}

ICONSERVICE.getXMLRequest = function () {
    if (STATE) {
        return STATE.getXMLHttpRequest();
    }
    return null;
}

/* The following functions have been moved to a utils object UTIL in webgui.js.
Because they were of a generic nature, some other pieces of code may have
relied on them, including in user-generated content such as e1pages, so we
create aliases to them here. 

- CARO.clamp
- CARO.createElement
- CARO.forceRedraw
- CARO.isMouseOutsideWindow
- CARO.pageHeight
- CARO.pageWidth
- CARO.sign
*/

if (window.UTIL) {
    for (var fnName in UTIL) {
        if (!CARO[fnName]) {
            CARO[fnName] = UTIL[fnName];
        }
    }
}
