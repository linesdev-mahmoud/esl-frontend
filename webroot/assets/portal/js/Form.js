﻿// JavaScripts for OneWorld Forms
var FormKeyboardHandler;
// we do not want undefined value for this global variable, 
// and it should be true if there is any FDA portlet on the page.
if (window.portletMode != true)
    window.portletMode = false;
var blockPane;
var CookieUtil;
var userAccessibility = false;
var userAgent = navigator.userAgent.toUpperCase();
var isNativeContainer = userAgent.indexOf("JDECONTAINER") > -1;
var isRTL = (document.documentElement.dir == "rtl");
var isFirefox = (userAgent.indexOf("GECKO/") > -1) || (userAgent.indexOf("FIREFOX") > -1);

if (localStorage.getItem("userAccessibilityMode"))
    userAccessibility = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;

if (CookieUtil == null) {
    CookieUtil = new function ()
    { }
}

CookieUtil.GetCookie = function (sName) {
    // cookies are separated by semicolons
    var aCookie = document.cookie.split("; ");
    for (var i = 0; i < aCookie.length; i++) {
        // a name/value pair (a crumb) is separated by an equal sign
        var aCrumb = aCookie[i].split("=");
        if (sName == aCrumb[0])
            return unescape(aCrumb[1]);
    }

    // a cookie with the requested name does not exist
    return null;
}

CookieUtil.SetCookie = function (sName, sValue) {
    var date = new Date();
    date.setMonth(date.getMonth() + 1);

    //set expiry to +1 month
    document.cookie = sName + "=" + escape(sValue) + "; expires=" + date.toGMTString();
}


if (FormKeyboardHandler == null) {
    FormKeyboardHandler = new function () {
        this.GJ_FORWARD = 1;
        this.GJ_BACKWARD = 2;
        this.safariComboBoxhasFocus = false;
        this.bDisableHotKey = "false";

        if (window.disableRightAltKey)
            this.bDisableHotKey = CookieUtil.GetCookie("DisableHotKey");
        else
            this.bDisableHotKey = "false";

        if (this.bDisableHotKey == null)
            this.bDisableHotKey = "false";

        this.SHIFT_TAB_KEY = 1;
        this.TAB_KEY = 2;

        this.isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
    }
}

FormKeyboardHandler.onKeyUp = function (evt) {
    this.tabOffControl = false; // reset this flag after we tab to another control
    return this.dispatchEventToParentFrame(evt, "onKeyUpHandler");
}

FormKeyboardHandler.onKeyDown = function (e) {
    var evt = e ? e : window.event;
    var activeElement = (evt.target === undefined) ? document.activeElement : evt.target;
    var namespace = JDEDTAFactory.getFirstNamespace();
    var stackId = JDEDTAFactory.getInstance(namespace).stackId;
    var today = new Date();
    var evtHandled = false;
    var rootContainer = null;
    //Evt 120 is Function Key F9
    //This key denotes the Toggle Mode to enable/disable the Hotkeys
    //This toggle mode is enabled only when disableRightAltKey is TRUE in jas.ini
    if (evt.keyCode == 120 && disableRightAltKey) {
        this.bDisableHotKey = CookieUtil.GetCookie("DisableHotKey");

        if (this.bDisableHotKey == "true") {
            CookieUtil.SetCookie("DisableHotKey", "false");
            this.bDisableHotKey = "false";
            var mesg = "The E1 Hotkeys have been reenabled.\n\nPress F9 key to disable the HotKeys"
            window.status = "HotKeys are Enabled";
            alert(mesg);
        }
        else {
            CookieUtil.SetCookie("DisableHotKey", "true");
            this.bDisableHotKey = "true";
            var mesg = "The E1 Hotkeys have been disabled.\n\nPress F9 key to reenable the HotKeys"
            window.status = "HotKeys are Disabled";
            alert(mesg);
        }
    }

    if (g_jdeWebGUIMenuShown) //If form menu is currently displayed, run menu handler first
    {
        evtHandled = jdeWebGUIOnMenuKeyDown(evt);
        if (evtHandled == true)
            return;
    }

    if (!document.all) {
        // We capture F1 explicitly for IE11 and other non-IE browsers.
        if (evt.keyCode == 112) {
            // evtHandled variable setting assists with stopPropagation() and preventDefault() calls downstream.
            evtHandled = true;

            // IE10 and lower lack support for a target on the event.
            if (evt.target.type == "radio") {
                //passing value instead of name as name attribute contains radio button group name
                //maintain consistency by adding prefix rbg
                eval(namespace + "hp('rbg'+evt.target.value)");
            }
            else {
                eval(namespace + "hp(evt.target.name)");
            }
        }
    }

    if (this.isCtrlDown(evt)) {
        if (evt.keyCode == 69 && !evt.shiftKey && !evt.altKey) {
            evtHandled = true;
            JSSidePanelRender.goToggleMode(JSSidePanelRender.MODE_QUERY);
        }
        if (evt.keyCode == 89 && !evt.shiftKey && !evt.altKey) { // Ctrl+Y: toggle watchlist creation panel 
            evtHandled = true;
            JSSidePanelRender.goToggleMode(JSSidePanelRender.MODE_WATCHLIST);
        }
        // Ctrl + Shift
        else if (evt.shiftKey) {
            if (evt.keyCode == 67 && document.all) { // Ctrl+Shift+C: Copy recovered form data 
                evtHandled = true;
                if (document.getElementById("underForm") == null)
                    toolsExitSelected('copyForm', namespace);
            }
            else if (evt.keyCode == 68) { // Ctrl+Shift+D: Advanced (batch versions)
            }
            else if (evt.keyCode == 74 && window.about != null) { //Ctrl+Shift+J: Launch About box
                evtHandled = true;
                about();
            }
            else if (evt.keyCode == 75)  // Ctrl+Shift+K:  Summary of all the hotkeys provided by the system 
            {
                this.cancelEventBubbling(evt);
                window[namespace + "LaunchHotKeysPage"]();
            }
            else if (evt.keyCode == 83 && document.all) { // Ctrl+Shift+S: Save the form data 
                evtHandled = true;
                if (document.getElementById("underForm") == null)
                    toolsExitSelected('saveForm', namespace);
            }
            else if (evt.keyCode == 85) { // Ctrl+Shift+U: Submit (batch version)
            }
            else if (evt.keyCode == 86 && document.all) { // Ctrl+Shift+V: Paste recovered form data 
                evtHandled = true;
                if (document.getElementById("underForm") == null)
                    toolsExitSelected('pasteForm', namespace);
            }
            else if (evt.keyCode == 192) //Ctrl+Shift ` to reset default layout in CafeOne 
            {
                if (RIUTIL)
                    RIUTIL.goHotkeySelectCustomizedLayout(0);
            }
            else if (evt.keyCode >= 49 && evt.keyCode <= 57) //Ctrl+Shift 1-9 to load customized layout in CafeOne 
            {
                var layoutIndex = evt.keyCode - 48;
                if (RIUTIL) {
                    RIUTIL.goHotkeySelectCustomizedLayout(layoutIndex);
                }
            }
            else if (portletMode == false) {
                var rootContainer = JSCompMgr.getRootContainer(namespace);
                evtHandled = rootContainer.processHotkey(namespace, activeElement, evt.keyCode, true, this.isAltDown(evt), true);
            }
        }
        else if (this.isAltDown(evt)) {

            if (evt.keyCode == 70) { // altLeft-F: Activate form exit
                if (document.getElementById("underForm") != null)
                    evtHandled = true;
                else {
                    var formExit = document.getElementsByName("FORM_EXIT_BUTTON")[0];
                    if (formExit != null) {
                        evtHandled = true;
                        jdeWebGUISaveFocusedElement(evt);
                        formExit.parentNode.focus();
                        var launchMenuID = formExit.getAttribute("launchMenuID");
                        jdeWebGUIdoToggleSubMenuOnKeyDown(formExit, launchMenuID, true, evt);
                    }
                }
            }
            else if (evt.keyCode == 82) { // altLeft-R: Activate row exit
                if (document.getElementById("underForm") != null)
                    evtHandled = true;
                else {
                    var rowExit = document.getElementsByName("ROW_EXIT_BUTTON")[0];
                    if (rowExit != null) {
                        evtHandled = true;
                        jdeWebGUISaveFocusedElement(evt);
                        rowExit.parentNode.focus();
                        var launchMenuID = rowExit.getAttribute("launchMenuID");
                        jdeWebGUIdoToggleSubMenuOnKeyDown(rowExit, launchMenuID, true, evt);
                    }
                }
            }
            else if (evt.keyCode == 80) { // altLeft-P: Activate report exit
                if (document.getElementById("underForm") != null)
                    evtHandled = true;
                else {
                    var reportExit = document.getElementsByName("REPORT_EXIT_BUTTON")[0];
                    if (reportExit != null) {
                        evtHandled = true;
                        jdeWebGUISaveFocusedElement(evt);
                        reportExit.parentNode.focus();
                        var launchMenuID = reportExit.getAttribute("launchMenuID");
                        jdeWebGUIdoToggleSubMenuOnKeyDown(reportExit, launchMenuID, true, evt);
                    }
                }
            }
            else if (evt.keyCode == 84) { // altLeft-T: Activate tools exit
                if (document.getElementById("underForm") != null)
                    evtHandled = true;
                else {
                    var toolsExit = document.getElementsByName("TOOLS_EXIT_BUTTON")[0];
                    if (toolsExit != null) {
                        evtHandled = true;
                        jdeWebGUISaveFocusedElement(evt);
                        toolsExit.parentNode.focus();
                        var launchMenuID = toolsExit.getAttribute("launchMenuID");
                        jdeWebGUIdoToggleSubMenuOnKeyDown(toolsExit, launchMenuID, true, evt);
                    }
                }
            }
            else if (evt.keyCode == 87) { // altLeft-W: Activate One View
                if (document.getElementById("underForm") != null)
                    evtHandled = true;
                else {
                    var reportExit = document.getElementsByName("BIP_EXIT_BUTTON")[0];
                    if (reportExit != null) {
                        evtHandled = true;
                        jdeWebGUISaveFocusedElement(evt);
                        reportExit.parentNode.focus();
                        var launchMenuID = reportExit.getAttribute("launchMenuID");
                        jdeWebGUIdoToggleSubMenuOnKeyDown(reportExit, launchMenuID, true, evt);
                    }
                }
            }
            else if (portletMode == false) {
                var rootContainer = JSCompMgr.getRootContainer(namespace);
                evtHandled = rootContainer.processHotkey(namespace, activeElement, evt.keyCode, true, true, evt.shiftKey);
            }
        } else if (window.JDEDTAFactory != undefined && (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM || JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_HIGH)) {
            if (evt.keyCode == 68) {
                evtHandled = true;
                JDEDTAFactory.getInstance(namespace).enableDebug();
            }
        }
    }
    else if (this.isAltDown(evt)) {
        // TLK - SAR 8514508
        // Alt
        if (evt.keyCode == 74) { // Alt + J: Grid-to-Grid Jump
            evtHandled = true;
            if (document.getElementById("underForm") == null) {
                // Blur the active element if present to allow for blur event to fire
                if (activeElement && activeElement.blur)
                    activeElement.blur();
                // Need to delay this to allow the blur to happen - IE6.0 bug
                setTimeout("JDEDTAFactory.getInstance('" + namespace + "').cycleGridFocus(" + !evt.shiftKey + ")", 10);
            }
        }
        else if (evt.keyCode == 191) { // Alt + /: Cycle QBE Focus
            evtHandled = true;
            if (document.getElementById("underForm") == null) {
                // Blur the active element if present to allow for blur event to fire
                if (activeElement && activeElement.blur)
                    activeElement.blur();
                // Need to delay this to allow the blur to happen - IE6.0 bug
                setTimeout("JDEDTAFactory.getInstance('" + namespace + "').cycleGridQBEFocus(" + !evt.shiftKey + ")", 10);
            }
        }
    }
    else if (evt.keyCode == 123)//F12
    {
        if (document.getElementById("underForm") == null) {
            if (!evt.shiftKey) {
                this.doControlGroupJump(evt, null, this.GJ_FORWARD);
            }
            else {
                this.doControlGroupJump(evt, null, this.GJ_BACKWARD);
            }
        }
        evtHandled = true;
    }
    else if (evt.keyCode == 119)//F8
    {
        // save the current focused element for OCL cycling
        if (self.parent && self.parent.MENUUTIL) {
            self.parent.MENUUTIL.processOpenApplicationList(1, true);
            evtHandled = true;
        }
    }

    // tabOffControl is used only for keeping track of tab into a grid action and need to be checked and reset by the grid

    if (!JDEDTAFactory.getInstance(namespace).gridInFocus && evt.keyCode != 16) // shift key has no effect on the state of tabOffControl
    {
        this.tabOffControl = (evt.keyCode == 9) ? (evt.shiftKey ? this.SHIFT_TAB_KEY : this.TAB_KEY) : false;
    }
    if (this.isAltDown(evt) && evt.keyCode == 81) { //Alt+Q
        var inyfeObj = window["inyfeHandler" + namespace];
        if (inyfeObj != undefined) {
            if (inyfeObj.isErrorPopupEnabled()) {
                inyfeObj.cycleError(evt);
            }
            else {
                inyfeObj.cycleErrorWithoutPopup(evt);
            }
        }
    }
    if (evt.keyCode == 13 && (activeElement.tagName == "INPUT") && activeElement.id != "" && activeElement.id.indexOf("C") == 0)//only cancel bubbling for form header fields
    {
        evtHandled = true;
    }
    if (!evtHandled) {
        evtHandled = (!this.dispatchEventToParentFrame(evt, "onKeyDownHandler"));
    }
    if (evtHandled == true) {
        this.cancelEventBubbling(evt);
    }
}

// search up the event handler up to top window (E1Menu is parent to E1 appplication but it is grandparent of the embedded e1 application in e1 pages, 
// and it will be more layer in between after we support composite page), and execute the event handler pass back the return.
// return true when event it is handled or throw exception
// return true if we can not find handler
FormKeyboardHandler.dispatchEventToParentFrame = function (evt, functionName) {
    var returnValue = false;
    var evtHandlerExisted = false;
    try {
        if (self.PSFT_TE_NAMESPACE)
            functionName = self.PSFT_TE_NAMESPACE + functionName;

        var currentParentWin = parent;

        while (true) {
            var parentEventHandler = currentParentWin[functionName];
            if (parentEventHandler) {
                returnValue = parentEventHandler(evt);
                evtHandlerExisted = true;
                break;
            }

            if (currentParentWin == top) // could not find event handler in any layer to the top, reutrn false
                break;

            currentParentWin = currentParentWin.parent;
        }

        if (!evtHandlerExisted) {
            // parent event may not be present
            returnValue = true;
        }

    }
    catch (problem) {
        // parent event may not be present
        returnValue = true;
    }

    return returnValue;
}

FormKeyboardHandler.cancelEventBubbling = function (evt) {
    if (evt.preventDefault) {
        evt.stopPropagation();
        evt.preventDefault();
        if (evt.cancelBubble) {
            evt.cancelBubble = true;
        }
    }
    else {
        evt.cancelBubble = true;
        evt.returnValue = false;
    }
}

FormKeyboardHandler.isCtrlDown = function (keyEvent) {
    if (this.bDisableHotKey == "true" || this.isIOS) {
        window.status = "Hot Keys are Disabled";
        return false;
    }
    var evt = keyEvent ? keyEvent : window.event;

    return evt.ctrlKey;
}

FormKeyboardHandler.isAltDown = function (keyEvent) {
    if (this.bDisableHotKey == "true") {
        window.status = "Hot Keys are Disabled";
        return false;
    }
    var evt = keyEvent ? keyEvent : window.event;
    if (document.all) {
        return (evt.altLeft || (evt.altKey && !this.isRightAltKeyDisabled(evt)))
    }
    else {
        return evt.altKey || evt.metaKey;
    }
}

FormKeyboardHandler.isShiftDown = function (keyEvent) {
    var evt = keyEvent ? keyEvent : window.event;
    return evt.shiftKey;
}

/*
This routine is to check whether right mouse click enabled on Form 
*/
FormKeyboardHandler.isRightClickPrefCheckEnabled = function () {
    var rightClickPrefCheck = true;
    try {
        if (window.modalSSIndex != undefined && modalSSIndex != 0) {
            rightClickPrefCheck = parent.parent.isPreferenceChecked("rowExitPreference");
        }
        // please don't reorder this condition as used in case of WSRP
        else if (prefObject.isWSRP) {
            rightClickPrefCheck = prefObject.rowExitpref;
        }
        else if (parent.prefObj) {
            rightClickPrefCheck = parent.isPreferenceChecked("rowExitPreference");
        }
        //app opened in New window     
        else if (self.parent.opener != null && self.parent.opener != undefined) {
            if (!self.parent.opener.closed)
                rightClickPrefCheck = self.parent.opener.isPreferenceChecked("rowExitPreference");
        }
    }
    catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
    }

    return rightClickPrefCheck;
}

FormKeyboardHandler.formExitContextMenu = function (keyEvent) {
    if (FormKeyboardHandler.isRightClickPrefCheckEnabled() == false) {
        return;
    }

    var evt = keyEvent ? keyEvent : window.event;
    var activeElement = (evt.target === undefined) ? document.activeElement : evt.target;
    if (activeElement.className != "MenuDropdownBack" && activeElement.className != "gridheader" && null != activeElement.className
    && activeElement.id != "" && activeElement.id != "FormAboveGrid") {
        /* Note:IE returns useful values in event.button only on mousedown and mouseup events.
        It is not possible to identify the mouse button for a click, dblclick or contextmenu event because event
        button will be zero regardless of which button was pressed.But Firefox returns the Correct value on mouse rightClick
        */
        if (document.all || (!document.all && evt.button == 2)) {
            var formExit = document.getElementById('FORM_EXIT_BUTTON');
            if (formExit != null) {
                formExit.onclick(evt);
                return false;
            }
        }
    }
}

FormKeyboardHandler.onContainerKeyDown = function (keyEvent, id) {
    var evt = keyEvent ? keyEvent : window.event;
    var activeElement = (evt.target === undefined) ? document.activeElement : evt.target;
    var shiftDown = this.isShiftDown(evt);
    var altDown = this.isAltDown(evt);
    if (this.isCtrlDown(evt)) {
        if (altDown || shiftDown) {
            var nameSpace = JSCompMgr.findNameSpace(id);
            var JSContainer = JSCompMgr.getNodeByID(id);
            if (nameSpace != null && JSContainer != null) {
                if (JSContainer.processHotkey(nameSpace, activeElement, evt.keyCode, true, altDown, shiftDown))
                    this.cancelEventBubbling(evt);
            }
        }
    }
    else if (this.isAltDown(evt)) {
    }
    else {
        if (evt.keyCode == 123)//F12
        {
            if (!evt.shiftKey) {
                this.doControlGroupJump(evt, id, this.GJ_FORWARD);
            }
            else {
                this.doControlGroupJump(evt, id, this.GJ_BACKWARD);
            }
        }
    }
}

FormKeyboardHandler.doControlGroupJump = function (evt, containerID, direction) {
    var nextFocusJSControl = null;
    var nextFocusElement = null

    //pass in null at document level.	
    if (containerID == null) {
        var rootContainer = JSCompMgr.getRootContainer(JDEDTAFactory.getFirstNamespace());
        if (rootContainer != null) {
            if (direction == this.GJ_FORWARD)
                nextFocusJSControl = rootContainer.getFirstFocusableLeaf();
            else
                nextFocusJSControl = rootContainer.getLastFocusableLeaf();
        }
    }
    else {
        var curContainer = JSCompMgr.findByID(containerID);
        if (curContainer != null) {
            if (direction == this.GJ_FORWARD) {
                nextFocusJSControl = JSCompMgr.getNextFocusableControl(curContainer);
            }
            else {
                nextFocusJSControl = JSCompMgr.getPrevFocusableControl(curContainer);
            }
        }
    }
    if (nextFocusJSControl != null) {
        nextFocusElement = nextFocusJSControl.getFocusableElement();
        if (nextFocusElement) {
            nextFocusElement.focus();
            nextFocusJSControl.setGroupJumpIndicator(true);
        }
        this.cancelEventBubbling(evt);
    }
}

FormKeyboardHandler.isRightAltKeyDisabled = function (evt) {
    var disableRightAltKeyElem = window.disableRightAltKey;

    if (disableRightAltKeyElem != null) {
        if (document.all) {
            // Right alt key is press and it is disabled.
            return disableRightAltKeyElem;
        } else {
            return false;
        }
    }
    else {
        return false;
    }
}

function TabKeyboardHandler(id, pages, activePage, enabledPages, pageIndexes) {
    this.id = id;
    this.tabPages = new Array();
    this.subPages = null;
    this.currentPage = activePage;

    for (var i = 0; i < pages.length; i++) {
        this.tabPages[i] = pages[i];
    }

    this.switchTab = function (tab, namespace, modelIndex) {
        if (tab < 0 || tab >= this.tabPages.length || tab == this.currentPage || enabledPages[tab] == false)
            return false;

        // For In Your Face Errors Only, this only happens when errors on inactive tab page
        var inyfeObj = window["inyfeHandler" + namespace];
        if (modelIndex != null && inyfeObj != null && inyfeObj.shouldFocusOnErrorAfterTabSwitch()) {
            var elem = document.getElementById("outerJDETabBody" + id);
            if (elem != null) {
                var pageElem = document.getElementById(JSConstant.pfTabCT + id + "." + modelIndex);
            }
            if (elem == null || pageElem == null) {
                return false;
            }
        }

        FCHandler.onTabChange(id, "tab" + id + "." + this.tabPages[tab], namespace);
        return true;
    }

    this.switchSubTab = function (tab, namespace) {
        if (tab < 0 || tab >= this.subPages || this.subPages[tab] == null)
            return;
        JDEDTAFactory.getInstance(namespace).post(this.subPages[tab]);
    }

    this.getViewIndex = function (modelIndex) {
        var viewIndex = -1;
        for (var i = 0; i < pageIndexes.length; i++) {
            if (modelIndex == pageIndexes[i]) {
                viewIndex = i;
                break;
            }
        }
        return viewIndex;
    }
}

FormKeyboardHandler.RegisterTabKBHandler = function (id, pages, activePage, enabledPages, pageIndexes) {
    if (!this.tabKBHandlers) {
        this.tabKBHandlers = new Array();
    }

    var tbkbHandler = null;
    for (var i = 0; i < this.tabKBHandlers.length; i++) {
        if (id == this.tabKBHandlers[i].id) {
            tbkbHandler = this.tabKBHandlers[i];
            tbkbHandler.currentPage = activePage;
            break;
        }
    }
    if (null == tbkbHandler) {
        this.tabKBHandlers[this.tabKBHandlers.length] = new TabKeyboardHandler(id, pages, activePage, enabledPages, pageIndexes);
    }
}

FormKeyboardHandler.registerSubTabsForTab = function (tabId, subPages) {
    var tbkbHandler = null;
    if (!this.tabKBHandlers) {
        this.tabKBHandlers = new Array();
    }

    for (var i = 0; i < this.tabKBHandlers.length; i++) {
        if (tabId == this.tabKBHandlers[i].id) {
            tbkbHandler = this.tabKBHandlers[i];
            break;
        }
    }
    if (null != tbkbHandler) {
        tbkbHandler.subPages = subPages;
    }
}

FormKeyboardHandler.OnKeyDownInTab = function (e, id, namespace) {
    var evt = e ? e : window.event;
    var evtHandled = false;
    if (this.isAltDown(evt)) {
        var tabpage = -1;
        //Digit keys on top of letter keys
        if ((evt.keyCode >= 49) && (evt.keyCode <= 57)) {
            tabpage = evt.keyCode - 49;
        }
        //Digit keys by NumLock
        else if ((evt.keyCode >= 97) && (evt.keyCode <= 105)) {
            tabpage = evt.keyCode - 97;
        }
        if (tabpage >= 0) {
            var tbkbHandler = null;
            for (var i = 0; i < this.tabKBHandlers.length; i++) {
                if (id == this.tabKBHandlers[i].id) {
                    tbkbHandler = this.tabKBHandlers[i];
                    break;
                }
            }
            if (null != tbkbHandler) {
                if (this.isCtrlDown(evt))
                    tbkbHandler.switchSubTab(tabpage, namespace);
                else
                    tbkbHandler.switchTab(tabpage, namespace);

                if (document.all) {
                    evt.cancelBubble = true;
                    evt.returnValue = false;
                }
                else {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
                evtHandled = true;
            }
        }
    }
    if (evtHandled == false)
        this.onContainerKeyDown(e, id);
}

FormKeyboardHandler.OnKeyDownInSubForm = function (namespace, id, e) {

    var evt = e ? e : window.event;
    var evtHandled = false;
    if (this.isAltDown(evt)) {
        if (evt.keyCode == 75) { // altLeft-K:	Expand/Collapse subform or container
            evt.cancelBubble = true;
            var bmp = document.getElementById("SFHdrBmp" + id);
            if (bmp != null) {
                // Set the ToolTip on Expand/Collapse			     
                setCollapsibleSubformTooltip(bmp);
                if (bmp.alt.length > 0) {
                    // Set the Alt on Expand/Collapse			     
                    setCollapsibleSubformAlt(bmp);
                }
            }
            FCHandler.ExpandCollapseSF(namespace, id);
            evtHandled = true;
            if (document.all) {
                evt.cancelBubble = true;
            }
            else {
                evt.stopPropagation();
                evt.preventDefault();
            }
        }
    }
    if (evtHandled == false)
        this.onContainerKeyDown(e, id);
}

var writeString;

var g_isCE = false;

// asynchronous row processing
var rowCriticalSize = 2;
var rowQueue = new Array(), colQueue = new Array();
var previousRow = -1;
var procQueue, procImages;

function BackButtonProtect() {
    if (document.prot.visit.value == "Visited") {
        document.prot.submit();
    }
    else {
        document.prot.visit.value = "Visited";
    }
}

var bShowRowHdr = 0;

function del(namespace, cmd) {
    // if we need to warn about hidden but selected items, let's do so:
    if (isNonVisibleRowsSelected == true) {
        if (confirm(szCfmDelete + "\n\n" + szCfmDeleteHidden))
            JDEDTAFactory.getInstance(namespace).post(cmd);
    }
    else {
        if (confirm(szCfmDelete))
            JDEDTAFactory.getInstance(namespace).post(cmd);
    }
}

function confirmPost(namespace, cmd, message) {
    if (confirm(message))
        JDEDTAFactory.getInstance(namespace).post(cmd);
}

function ocCalc(namespace, cmd) {
    var jdeCalc = new JDECalc(document.images['va' + cmd], document.getElementsByName(cmd)[0],
                        decsep, calcOrCalnStrings);
    jdeCalc.namespace = namespace;
    FCHandler.calcObj = jdeCalc;
    FCHandler.calcObj.oldCalcOK = jdeCalc.onOK;
    FCHandler.calcObj.oldCalcCancel = jdeCalc.onCancel;
    jdeCalc.onCancel = FCHandler.onCalcCancel;
    jdeCalc.onOK = FCHandler.onCalcOK;
    jdeCalc.show();
}

function ocCaln(namespace, cmd, isUTime, TimeZone, isDateOnly) {
    var jdeCaln = new JDECaln(document.images['va' + cmd], document.getElementsByName(cmd)[0],
                        dateseparator, dateformat, calcOrCalnStrings, isUTime, true, TimeZone, isDateOnly);
    jdeCaln.namespace = namespace;
    FCHandler.calnObj = jdeCaln;
    FCHandler.calnObj.oldCalnOK = jdeCaln.onOK;
    FCHandler.calnObj.oldCalnCancel = jdeCaln.onCancel;
    jdeCaln.onCancel = FCHandler.onCalnCancel;
    jdeCaln.onOK = FCHandler.onCalnOK;
    jdeCaln.show();
}

////////////////////////////////////////////////////////////////
// Singleton Object that is used to provide event handling
// for textfields, checkbox, combobox, radiobutton, grid etc.
//////////////////////////////////////////////////////////////
var FCHandler;

if (FCHandler == null) {
    FCHandler = new function () {
        this.eventNames = new Array(); // This array will contain events for Grid as well.
        this.prevValue = null;
        this.formLoaded = false;
        var userAgent = navigator.userAgent.toUpperCase();
        this.isSafari = false;
        this.isWebkit = (userAgent.indexOf("WEBKIT") > -1);
        this.isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
        this.isTouchEnabled = (this.isIOS == true) || (userAgent.indexOf("ANDROID") > -1);
        this.controlListBelowTB = new Array();
    }
}

FCHandler.onTextLimit = function (field, maxlimit) {

    if (field.value.length > maxlimit) // if too long...trim it!
    {
        field.value = field.value.substring(0, maxlimit);
    }

}

// Public method called by VTTextBlock Render to register the text block during rendering.
FCHandler.registerTextBlock = function (ID) {
    this.controlListBelowTB.push(ID);
}

// Public method called only by JSComponent onload method after the enlongation is done for initial render of form.
// Since elongation changes the width of text block, and text in the text block will be wrapped to certain height,
// It is more accurate to do this process after elongation is finished, so that we do not push the effect controls 
// down unnecessaryly.
FCHandler.addjustEffectControlBelowTextBlock = function () {
    for (var i = 0; i < this.controlListBelowTB.length; i++) {
        this.adjustTBEffectedControls(this.controlListBelowTB[i]);
    }
}

FCHandler.formatNumericValue = function (ctrl) {
    var dataArray = ctrl.value.split(',');
    var formattedValue = '';
    for (var i = 0; i < dataArray.length; i++) {
        formattedValue = formattedValue + dataArray[i];
    }
    ctrl.value = formattedValue;
}

FCHandler.onTouchCell = function (ctrl, isNumberInput) {
    if (isNumberInput) {
        FCHandler.formatNumericValue(ctrl);
        if (window.showNumericKeyboard) {
            ctrl.type = "number";
        }
    }
    if (window.event) {
        window.event.stopPropagation();
    }
}

FCHandler.onTouchSpanCell = function (ctrl, isNumberInput) {
    if (ctrl && !(ctrl.childNodes.length >= 2 && ctrl.childNodes[1].childNodes.length >= 1)) {
        return;
    }

    var inputTag = ctrl.childNodes[1].childNodes[0];
    if (inputTag) {
        FCHandler.formatNumericValue(inputTag);
        if (window.showNumericKeyboard) {
            inputTag.type = "number";
        }
    }
}

FCHandler.onFocusVA = function (ctrl, namespace, isNumberInput) {
    if (ctrl.getAttribute("isDropdown") != 1) {
        if (document.documentElement.dir == "rtl")
            document.images['va' + ctrl.name].src = window["E1RES_img_va1_rtl_gif"];
        else
            document.images['va' + ctrl.name].src = window["E1RES_img_va1_gif"];

        if (ctrl.getAttribute("autoSuggestEnabled") == 1 && FCHandler.isAdvancedQueryMode() == false) {
            FCHandler.showAutoSuggestIndicator(ctrl, null, ctrl.name, ctrl.getAttribute("minCharNumber"), ctrl.getAttribute("hotKey") == 1, namespace, "ctrlAutoSuggest");
        }
    }
    this.onFocus(ctrl, namespace, isNumberInput);
}

FCHandler.onFocusVACalc = function (ctrl, namespace, isNumberInput) {
    document.images['va' + ctrl.name].src = window["E1RES_img_vacalc_gif"];
    this.onFocus(ctrl, namespace, isNumberInput);
}

FCHandler.onFocusVACaln = function (ctrl, namespace) {
    document.images['va' + ctrl.name].src = window["E1RES_img_vacaln_gif"];
    this.onFocus(ctrl, namespace);
}

FCHandler.onFocus = function (ctrl, namespace, isNumberInput) {
    if (window.showNumericKeyboard && isNumberInput && !FORMTOUCH.callBackFocus) {
        FCHandler.formatNumericValue(ctrl);
        ctrl.type = "number";
    }
    if (this.isTouchEnabled) {
        if (self && self.parent && !FORMTOUCH.callBackFocus) {
            if (self.parent.CARO)
                self.parent.CARO.disableTouchEvents();
            var e1PaneForm = document.getElementById("E1PaneForm");
            if (e1PaneForm && FORMTOUCH)
                FORMTOUCH.disableTouchEvents(e1PaneForm);
            var appFrame = self.parent.document.getElementById("e1AppFrameContainer");
            if (appFrame && self.parent.FRAMETOUCH)
                self.parent.FRAMETOUCH.disableTouchEvents(appFrame);
        }
        if (FORMTOUCH.restoreFormFocusOnRIAF)
            FORMTOUCH.restoreFormFocusOnRIAF = false;
    }
    if (ctrl.getAttribute("autoSuggestEnabled") == 1 && FCHandler.isAdvancedQueryMode() == false && ctrl.getAttribute("isDropdown") != 1) {
        FCHandler.showAutoSuggestIndicator(ctrl, null, ctrl.name, ctrl.getAttribute("minCharNumber"), ctrl.getAttribute("hotKey") == 1, namespace, "ctrlAutoSuggest");
    }

    if (this.isWebkit) {
        self.safariSelectField = ctrl;
        if (!this.isIOS) {
            setTimeout("if(self.safariSelectField && self.safariSelectField.select){self.safariSelectField.select();}self.safariSelectField=null;", 1);
        }
        else {
            setTimeout("if(self.safariSelectField && self.safariSelectField.type=='number' && window.showNumericKeyboard){self.safariSelectField.type='text';}if(self.safariSelectField && self.safariSelectField.select){self.safariSelectField.select();}if(self.safariSelectField && self.safariSelectField.value==' '){self.safariSelectField.value='';self.safariSelectField.value.RTrim().LTrim();}self.safariSelectField=null;", 1);
        }
    }
    else {
        ctrl.select();
    }

    this.prevValue = ctrl.value;
    this.onFocusCtrl(ctrl, namespace);

    var inyfeObj = window["inyfeHandler" + namespace];
    if (inyfeObj != undefined && this.formLoaded == true) {
        inyfeObj.checkAndDisplayErrorWindowOnFocus(ctrl, ctrl.name);
    }
}

FCHandler.removeGroupJumpIndicatorFromControl = function (ctrl) {
    var id = ctrl.getAttribute('id');
    if (id != null) {
        id = id.substring(1); //strip off 'C'
        this.removeGroupJumpIndicatorById(id);
    }
}

FCHandler.onExitVA = function (ctrl, htmlPostFlag, namespace, isNumberInput) {
    if (typeof (isDesignMode) != 'undefined' && isDesignMode) {
        return;
    }
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_HIGH && ctrl.getAttribute("autoSuggestEnabled") == 1 && typeAheadData.mouseInFlag == 1) {
        // Do nothing
    }
    else {
        if (ctrl.getAttribute("isDropdown") != 1) {
            document.images['va' + ctrl.name].src = window["E1RES_img_va0_gif"];


            // Adjust VA icon position when Auto Suggest icon is hidden.  If not adjusting, the active click position still shift 11px to the left 
            if (ctrl.getAttribute("autoSuggestEnabled") == 1 && FCHandler.isAdvancedQueryMode() == false) {
                var vaIcon = document.images['va' + ctrl.name];

                if (vaIcon) {
                    vaIcon.style.left = "0px";
                }
            }
        }

        this.onExit(ctrl, htmlPostFlag, namespace, isNumberInput);
    }
}

FCHandler.onExit = function (ctrl, htmlPostFlag, namespace, isNumberInput) {
    if (window.showNumericKeyboard && isNumberInput) {
        ctrl.type = "text";
    }
    if (this.isTouchEnabled) {
        if (self && self.parent && !FORMTOUCH.callBackFocus) {
            if (self.parent.CARO)
                self.parent.CARO.enableTouchEvents();
            var e1PaneForm = document.getElementById("E1PaneForm");
            if (e1PaneForm && FORMTOUCH)
                FORMTOUCH.enableTouchEvents(e1PaneForm);
            var appFrame = self.parent.document.getElementById("e1AppFrameContainer");
            if (appFrame && self.parent.FRAMETOUCH)
                self.parent.FRAMETOUCH.enableTouchEvents(appFrame);
        }
        if (FORMTOUCH.callBackFocus && !FORMTOUCH.restoreFormFocusOnRIAF)
            FORMTOUCH.callBackFocus = false;
    }
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_HIGH && ctrl.getAttribute("autoSuggestEnabled") == 1 && typeAheadData.mouseInFlag == 1) {
        // Do nothing
    }
    else {
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW) {
            this.markExited(ctrl);

            if (this.prevValue != ctrl.value && htmlPostFlag) {
                JDEDTAFactory.getInstance(namespace).postLater("");
                if (ChangeConf.isOkConf || ChangeConf.isCancelConf) {
                    if (this.prevValue.toLowerCase() != ctrl.value.toLowerCase()) {
                        ChangeConf.isDirtyForm = true;       // dirtyForm is set to true since the value of the control is changed
                        ChangeConf.unmaskOkHyperControl();
                    }
                }
            }
        }
        else {
            if (undefined != window.JSCompMgr) {
                this.removeGroupJumpIndicatorFromControl(ctrl);
            }

            FCHandler.hideAutoSuggestIndicator(ctrl.name);
            if (typeAheadData.isTypeAheadWindowVisible())
                typeAheadData.resetTypeAhead();
            //var ctrlVal = ctrl.value.replace(/"/g,'&quot;');

            var ctrlVal = ctrl.value;
            //ctrlTitle is used by RUEI
            var ctrlTitle = ctrl.title;
            if (window.RIUTIL != undefined && window.RIUTIL.isEmbedded && this.prevValue != ctrl.value) {
                window.RIUTIL.dirtyRIFormBegTimeStmp = new Date().getTime();
            }
            if (this.prevValue == ctrl.value && (ctrl.getAttribute("autoSuggestEnabled") != 1 || (ctrl.getAttribute("autoSuggestEnabled") == 1 && !typeAheadData.dataSelected)))// control is exited, if a value is picked from the AS dropdown, it's considered changed
            {
                JDEDTAFactory.getInstance(namespace).addToAsynQueue("ctrlExit", ctrl.name, null, null, ctrlTitle);
            }
            else {
                if (ChangeConf.isOkConf || ChangeConf.isCancelConf) {
                    if (this.prevValue.toLowerCase() != ctrl.value.toLowerCase()) {
                        ChangeConf.isDirtyForm = true;       // dirtyForm is set to true since the value of the control is changed                        
                        ChangeConf.unmaskOkHyperControl();
                    }
                }
                if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM && !htmlPostFlag)
                    JDEDTAFactory.getInstance(namespace).addToAsynQueue("ctrlExit", ctrl.name, ctrlVal);
                else {
                    if (ctrl.getAttribute("autoSuggestEnabled") == 1 && typeAheadData.dataSelected) {
                        typeAheadData.clearDataSelected();
                    }
                    //ctrlTitle is used by RUEI
                    JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("ctrlExit", ctrl.name, ctrlVal, ctrlTitle);
                    if (window.RIUTIL) {
                        RIUTIL.onFormHeaderChange(ctrl.name);
                    }
                }
            }
        }

        var inyfeObj = window["inyfeHandler" + namespace];
        if (inyfeObj != null && inyfeObj.currentErrorObj != null) {
            inyfeObj.closeWindowTimeout = window.setTimeout("inyfeHandler" + namespace + ".closeErrorWindowTimeout()", 100);
        }
    }
}

FCHandler.onMouseIsHovered = function (ctrl, namespace, evt) {
    var ctrlVal = ctrl.value;
    var ctrlName = ctrl.name;
    // special case for radio button.  NAME attribute of radio button stores "rbg" + DBRef,
    // VALUE attribute stores idObject.  Need to post idObject consistent with other controls.
    if (ctrlName.indexOf("rbg") == 0)
        ctrlName = ctrl.value;
    var dta = JDEDTAFactory.getInstance(namespace);
    clearTimeout(dta.displayPopup);
    //if (this.prevValue == ctrl.value) // only control is exited
    if (ctrlName) {
        var ctrlId = "popupWin" + ctrl.name;
        if (window.openedPopup && window.openedPopup.type != SHOW_HOVER) {
            if (window.openedPopup.id == ctrlId) {
                return;
            }
            else {
                clearInterval(window.intervalIdForOpenedPopup);
                window.openedPopup.hide();
                window.openedPopup = null;
                window.intervalIdForOpenedPopup = null;
            }
        }

        var inyfeObj = window["inyfeHandler" + namespace];
        var isError = false;
        var controlId = null;
        for (var i = 0; i < inyfeObj.errorList.length; i++) {
            controlId = inyfeObj.errorList[i].id.substring(0, inyfeObj.errorList[i].id.lastIndexOf("_"));
            if (controlId == ctrl.name) {
                isError = true;
                break;
            }
        }
        if (ctrl.value && ctrl.value != " " && ctrl.value != "*" && !isError) {

            if ((this.prevValue == ctrl.value) && window[ctrlId]) {
                mymousestop = function () {
                    window[ctrlId].show(ctrl);
                };
                dta.displayPopup = setTimeout(mymousestop, 1000);

            }
            else {
                mymousestop = function () {
                    JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("mouseHover", ctrlName, ctrlVal);
                };
                dta.displayPopup = setTimeout(mymousestop, 1);
                dta.popupPos[0] = getAbsoluteLeftPos(ctrl) + ctrl.offsetWidth;
                if ((ctrl.type == "checkbox") || (ctrl.type == "radio")) {
                    dta.popupPos[1] = getAbsoluteTopPos(ctrl) + ctrl.offsetHeight;
                }
                else {
                    dta.popupPos[1] = getAbsoluteTopPos(ctrl);
                }
            }
        }
    }
    return this.cancelEventBubbling(evt);
}

FCHandler.onMouseIsHoveredOnLabel = function (ctrlId, namespace, evt) {

    if (ctrlId) {
        var dta = JDEDTAFactory.getInstance(namespace);
        var ctrlWinId = "popupWinpopup" + ctrlId;

        clearTimeout(dta.displayPopup);
        mymousestop = function () {
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("mouseHover", ctrlId);
        };
        dta.displayPopup = setTimeout(mymousestop, 1000);
        ctrlElem = document.getElementById("popup" + ctrlId)
        if (ctrlElem) {
            dta.popupPos[0] = getAbsoluteLeftPos(ctrlElem) + ctrlElem.offsetWidth;
            dta.popupPos[1] = getAbsoluteTopPos(ctrlElem);
        }
    }
    return this.cancelEventBubbling(evt);


}

FCHandler.onMouseOutControl = function (ctrl, namespace, e) {
    document.getElementById("popupImg" + ctrl.name).firstChild.src = window["E1RES_img_popup_indicator_png"];
    var dta = JDEDTAFactory.getInstance(namespace);
    clearTimeout(dta.displayPopup);
    if (ctrl) {
        var ctrlId = "popupWin" + ctrl.id;
        var evt = e ? e : window.event;
        var targObjName = evt.relatedTarget || evt.toElement;

        var parentName;
        var closePopup = true;
        while (targObjName != null && targObjName.tagName != 'BODY') {
            if ((ctrl === targObjName) || (targObjName.tagName == 'DIV' && (targObjName.id == ctrlId || (targObjName.id.indexOf(ctrlId + "-shadow") === 0)))) {
                closePopup = false;
            }
            targObjName = targObjName.parentNode;
        }
        if (closePopup && window[ctrlId]) {
            window[ctrlId].hide();
        }
    }
    dta.displayPopup = 0;
    return this.cancelEventBubbling(evt);
}


FCHandler.onMouseOutLabel = function (ctrlId, namespace, e) {
    document.getElementById("popupImg" + ctrlId).firstChild.src = window["E1RES_img_popup_indicator_png"];
    var dta = JDEDTAFactory.getInstance(namespace);
    clearTimeout(dta.displayPopup);
    if (ctrlId) {
        var popupId = "popupWinpopup" + ctrlId;
        var evt = e ? e : window.event;
        var targObjName = evt.relatedTarget || evt.toElement;

        var parentName;
        var closePopup = true;
        while (targObjName != null && targObjName.tagName != 'BODY') {
            if (targObjName.tagName == 'DIV' && (targObjName.id == popupId || (targObjName.id.indexOf(popupId + "-shadow") === 0))) {
                closePopup = false;
            }
            //targObjName = targObjName.parentElement; 
            targObjName = targObjName.parentNode;
        }
        if (closePopup && window[popupId]) {
            window[popupId].hide();
        }
    }
    dta.displayPopup = 0;
    return this.cancelEventBubbling(evt);
}



FCHandler.cancelEventBubbling = function (evt) {
    if (document.all) {
        evt.cancelBubble = true;
        evt.returnValue = false;
    }
    else {
        evt.stopPropagation();
        evt.preventDefault();
    }
}

FCHandler.onKeyDownNumberTextField = function (ctrl, isNumberInput) {
    if (window.showNumericKeyboard) {
        if (isNumberInput) {
            ctrl.type = "text";
        }
    }
}

//SAR 8621821 - TLK
// Add three arguments to method:  isUTime, TimeZone, isDateOnly
FCHandler.onKeyDownTextField = function (ctrl, namespace, vaType, e, isUTime, TimeZone, isDateOnly) {
    if (window.showNumericKeyboard) {
        if (ctrl.type == "number") {
            ctrl.type = "text";
        }
    }
    var evt = e ? e : window.event;

    if (evt.keyCode == 113) { //F2 Visual assist
        if (vaType == "3") {
            var ctrlVal = ctrl.value;
            if (this.prevValue == ctrl.value) // only control is exited
            {
                JDEDTAFactory.getInstance(namespace).addToAsynQueue("ctrlExit", ctrl.name, null);
                oc(namespace, ctrl.name);
            }
            else {
                JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("ctrlExit", ctrl.name, ctrlVal);
                window.setTimeout("JDEDTAFactory.getInstance('" + namespace + "').post('va" + ctrl.name + "')", 100);
            }
        }
        else if (vaType == "1") {
            ocCalc(namespace, ctrl.name);
            this.calcObj.clickWillHide = true;
        }
        else if (vaType == "2" || vaType == "4") {
            ocCaln(namespace, ctrl.name, isUTime, TimeZone, isDateOnly);
            this.calnObj.clickWillHide = true;
        }
        // Confirm stopPropagation support as it may be lacking for some IE releases and this particular object.
        if (evt && evt.stopPropagation) {
            evt.stopPropagation();
            evt.preventDefault();
        }
        else {
            // Using window.event here for some IE releases and in the unlikely scenario evt is not window.event.
            window.event.cancelBubble = true;
            window.event.returnValue = false;
        }
    }
    else if (FormKeyboardHandler.isCtrlDown(evt) && FormKeyboardHandler.isAltDown(evt) && evt.keyCode == 72) {
        // Auto Suggest Hot Key Ctrl+Alt+h hotkey
        if (ctrl.getAttribute("hotKey") == 1) {
            FCHandler.onKeyUpTextFieldTypeAhead(ctrl, namespace, true);

            // Confirm stopPropagation support as it may be lacking for some IE releases and this particular object.
            if (evt && evt.stopPropagation) {

                evt.stopPropagation();
                evt.preventDefault();
            }
            else {
                // Using window.event here for some IE releases and in the unlikely scenario evt is not window.event.
                window.event.cancelBubble = true;
                window.event.returnValue = false;
            }
        }
    }
    else if (evt.keyCode == 9 && !FormKeyboardHandler.isCtrlDown(evt) && !FormKeyboardHandler.isAltDown(evt)) // TAB or SHIFT TAB key
    {
        if (ctrl.getAttribute("autoSuggestEnabled") == 1 && FCHandler.isAdvancedQueryMode() == false) {
            // Clear 
            typeAheadData.resetTypeAheadInternal();
        }
    }
}

FCHandler.onKeyUpTextField = function (ctrl, namespace, e) {

    var evt = e ? e : window.event;
    // When user starts typing close the popup window, if opened
    if ((ctrl.getAttribute("isPopupAssociated") == 1) && (window.openedPopup) && window.openedPopup.type != SHOW_HOVER) {
        switch (evt.keyCode) {
            case 9: // TAB key
            case 16:  // SHIFT-TAB key
            case 17:  // CTRL key
            case 18:  // ALT key
            case 20:  // CAPS Lock key
            case 37:  // LEFT key
            case 38:  // UP key
            case 39:  // RIGHT key
            case 40:  // DOWN key
                // Ignore these keys, should not start Auto Suggest search
                break;
            default:
                var ctrlId = "popupWin" + ctrl.id;
                if (window.openedPopup.id == ctrlId) {
                    clearInterval(window.intervalIdForOpenedPopup);
                    window.openedPopup.hide();
                    window.openedPopup = null;
                    window.intervalIdForOpenedPopup = null;
                }
        }
    }
    if (ctrl.getAttribute("autoSuggestEnabled") == 1 && FCHandler.isAdvancedQueryMode() == false) {

        if (evt.keyCode == 9 || evt.keyCode == 16 || evt.keyCode == 37 || evt.keyCode == 39 || evt.keyCode == 35 || evt.keyCode == 36) {


            // 9 -TAB key
            // 16 - SHIFT-TAB key
            // 37 - LEFT key
            // 39 - RIGHT key
            // 35 - END key
            // 36 - HOME key
            // Ignore these keys, should not start Auto Suggest search
            //window.status = window.status + "tab;";

        }
        else if (evt.keyCode == 17 || evt.keyCode == 18)  // CTRL key and ALT key  
        {
            // Ignore these key up since they are used in hot key search
        }
        else if (evt.keyCode == 27) {  // Escape key
            if (typeAheadData.isTypeAheadWindowVisible()) {
                typeAheadData.typeAheadWindow.hide();
            }
            FCHandler.updateAutoSuggestIndicator(ctrl, ctrl.name, ctrl.getAttribute("minCharNumber"), ctrl.getAttribute("hotKey") == 1);
        }
        else if (evt.keyCode == 33) { // PAGE UP key
            if (typeAheadData.isTypeAheadWindowVisible()) {
                typeAheadData.pressPageUpKey(ctrl);
            }
        }
        else if (evt.keyCode == 34) { // PAGE DOWN key
            if (typeAheadData.isTypeAheadWindowVisible()) {
                typeAheadData.pressPageDownKey(ctrl);
            }
        }
        else if (evt.keyCode == 38) { // UP key
            if (typeAheadData.isTypeAheadWindowVisible()) {
                typeAheadData.pressUpKey(ctrl);
            }
        }
        else if (evt.keyCode == 40) { // DOWN key
            if (typeAheadData.isTypeAheadWindowVisible()) {
                typeAheadData.pressDownKey(ctrl);
            }
            else if (ctrl.getAttribute("isDropdown") == 1 && (ctrl.value.Trim().length == 0 || ctrl.value == '*'))//for dropdown, send a wild card search
            {
                typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrl.name, namespace, ctrl.value, "ctrlAutoSuggest", 2) }, autoSuggestDelay);
            }
        }
        else if (evt.keyCode == 13 || evt.keyCode == 3) { // Enter key  Safari Enter key is 3
            if (typeAheadData.isTypeAheadWindowVisible()) {
                ctrl.value = typeAheadData.getSelectedValue();
                typeAheadData.typeAheadWindow.hide();
                typeAheadData.setDataSelected();
            }
        }
        else {
            //typeAheadData.currentValue = ctrl.value;            
            FCHandler.onKeyUpTextFieldTypeAhead(ctrl, namespace, false);

        }
    }
}

FCHandler.onFocusCombo = function (ctrl, namespace, updatefocus) {
    if (this.isSafari && this.safariComboBoxhasFocus == true)
        return;
    this.safariComboBoxhasFocus = true;

    if (ctrl.selectedIndex != -1 && ctrl.options != null && ctrl.options.length > ctrl.selectedIndex)
        this.prevValue = ctrl.options[ctrl.selectedIndex].value;
    else
        this.prevValue = null;
    // SAR 6998393
    if (updatefocus == true) {
        this.onFocusCtrl(ctrl, namespace);
    }

    var inyfeObj = window["inyfeHandler" + namespace];
    if (inyfeObj != undefined && this.formLoaded == true) {
        inyfeObj.checkAndDisplayErrorWindowOnFocus(ctrl, ctrl.name);
    }
}

FCHandler.removeGroupJumpIndicatorById = function (id) {
    JSCompMgr.removeGroupJumpIndicator(id);
}

FCHandler.onExitButton = function (ctrl) {
    this.removeGroupJumpIndicatorFromControl(ctrl);
}

FCHandler.updateControlClassInfo = function (element, updateClass, addClass) {
    var className = element.className;
    if (addClass) {
        element.className = className + updateClass;
    }
    else {
        element.className = className.replace(updateClass, '');
    }
}

FCHandler.onMouseOverButton = function (ctrl) {
    FCHandler.updateControlClassInfo(ctrl, ' RCUXbuttonMouseOver', true);
}

FCHandler.onMouseOutButton = function (ctrl) {
    FCHandler.updateControlClassInfo(ctrl, ' RCUXbuttonMouseOver', false);
    FCHandler.updateControlClassInfo(ctrl, ' RCUXbuttonMouseDown', false);
}

FCHandler.onMouseDownButton = function (ctrl) {
    FCHandler.updateControlClassInfo(ctrl, ' RCUXbuttonMouseOver', false);
    FCHandler.updateControlClassInfo(ctrl, ' RCUXbuttonMouseDown', true);
}

FCHandler.onMouseUpButton = function (ctrl) {
    FCHandler.updateControlClassInfo(ctrl, ' RCUXbuttonMouseDown', false);
}

FCHandler.onExitImg = function (ctrlID) {
    this.removeGroupJumpIndicatorById(ctrlID);
}

FCHandler.onExitLabel = function (ctrlID) {
    this.removeGroupJumpIndicatorById(ctrlID);
}

FCHandler.onExitMO = function (ctrlID) {
    this.removeGroupJumpIndicatorById(ctrlID);
}

FCHandler.onExitSavedQueryCombo = function (ctrl, htmlPostFlag, namespace, accessibilityMode) {

    // Fire the Selection Changed event if the Accessibility Mode is set and the combo box selection has changed
    if (accessibilityMode == true && this.prevValue != ctrl.options[ctrl.selectedIndex].value) {
        FCHandler.onSelectionChangedCombo(ctrl, htmlPostFlag, namespace);
    }
    this.removeGroupJumpIndicatorFromControl(ctrl);

}

FCHandler.onExitCombo = function (ctrl, htmlPostFlag, namespace, accessibilityMode) {
    this.safariComboBoxhasFocus = false;
    this.removeGroupJumpIndicatorFromControl(ctrl);

    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW) {
        // Fire the Selection Changed event if the Accessibility Mode is set and the combo box selection has changed
        if (accessibilityMode == true && this.prevValue != ctrl.options[ctrl.selectedIndex].value) {
            FCHandler.onSelectionChangedCombo(ctrl, htmlPostFlag, namespace);
        }
        this.markExited(ctrl);

        if (this.prevValue != ctrl.options[ctrl.selectedIndex].value && htmlPostFlag)
            JDEDTAFactory.getInstance(namespace).postLater("");
    } else {
        var ctrlVal = ctrl.options[ctrl.selectedIndex].value;
        if (this.prevValue == ctrlVal || (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM && !htmlPostFlag))
            JDEDTAFactory.getInstance(namespace).addToAsynQueue("ctrlExit", ctrl.name, ctrlVal, "", ctrl.title);
        else
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("ctrlExit", ctrl.name, ctrlVal, "", ctrl.title);
    }

    var inyfeObj = window["inyfeHandler" + namespace];
    if (inyfeObj != null && inyfeObj.currentErrorObj != null) {
        inyfeObj.closeWindowTimeout = window.setTimeout("inyfeHandler" + namespace + ".closeErrorWindowTimeout()", 100);
    }
}

FCHandler.onFocusCheckboxRadio = function (ctrl, namespace) {
    var inyfeObj = window["inyfeHandler" + namespace];
    if (inyfeObj != undefined && this.formLoaded == true) {
        inyfeObj.checkAndDisplayErrorWindowOnFocus(ctrl, ctrl.name);
    }
    var ctrlId = ctrl.getAttribute('id');
    if (ctrlId != null) {
        ctrlId = ctrlId.substring(1); //strip off 'C'
    }
    this.onFocusByCtrlId(ctrlId, namespace);
    if (isFirefox)
        ctrl.parentNode.className = "focusedHelpDiv";
}

FCHandler.onExitCheckboxRadio = function (ctrl, namespace) {

    this.removeGroupJumpIndicatorFromControl(ctrl);

    var inyfeObj = window["inyfeHandler" + namespace];
    if (inyfeObj != null && inyfeObj.currentErrorObj != null) {
        inyfeObj.closeWindowTimeout = window.setTimeout("inyfeHandler" + namespace + ".closeErrorWindowTimeout()", 100);
    }
    if (isFirefox)
        ctrl.parentNode.className = "unfocusedHelpDiv";
}
//To use this method, ctrl.name has to be the ctrl's FullyQualifiedId
FCHandler.onFocusCtrl = function (ctrl, namespace) {
    if (window.JDEDTAFactory != undefined && this.formLoaded == true) {
        JDEDTAFactory.getInstance(namespace).controlGotFocus(ctrl);
    }
}
//ctrlId is the ctrl's FullyQualifiedId
FCHandler.onFocusByCtrlId = function (ctrlId, namespace) {
    if (window.JDEDTAFactory != undefined && this.formLoaded == true) {
        JDEDTAFactory.getInstance(namespace).controlGotFocusById(ctrlId);
    }
}

FCHandler.makeEventSeqStr = function (namespace) {
    var strVal = "";
    for (var c = 0; c < this.eventNames.length; c++) {
        if (c > 0)
            strVal += "|";
        strVal += this.eventNames[c];
    }
    document.forms[JDEDTAFactory.getInstance(namespace).formName].evtSeq.value = strVal;
}

FCHandler.markExited = function (ctrl) {
    var ctrlName = ctrl.name;

    // special case for radio button.  NAME attribute of radio button stores "rbg" + DBRef,
    // VALUE attribute stores idObject.  Need to post idObject consistent with other controls.
    if (ctrlName.indexOf("rbg") == 0)
        ctrlName = ctrl.value;

    FCHandler.markExitedByName(ctrlName);
}

FCHandler.markExitedByName = function (ctrlName, alwaysRecord) {
    if (alwaysRecord) {
        this.eventNames[this.eventNames.length] = ctrlName;
    }
    else {
        // check if the field is already there
        for (var c = 0; c < this.eventNames.length; c++) {
            if (ctrlName == this.eventNames[c])
                return;
        }
        if (c == this.eventNames.length) //new one
            this.eventNames[c] = ctrlName;
    }
}

// For radio button, checkboxes and comboboxes
FCHandler.onClickCtrl = function (ctrl, htmlPostFlag, namespace) {
    if (JDEDTAFactory.captureStatus == true)
        return;
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW) {
        this.markExited(ctrl);
        if (htmlPostFlag) {
            JDEDTAFactory.getInstance(namespace).post('');
        }
    } else {
        var ctrlID, ctrlVal, ctrlTitle = null;
        //ctrlTitle is used by RUEI
        var ctrlTitle = ctrl.title;

        if (ChangeConf.isOkConf || ChangeConf.isCancelConf) {
            ChangeConf.isDirtyForm = true;       // dirtyForm is set to true since the radio or checkbox crtl is clicked
            ChangeConf.unmaskOkHyperControl();
        }
        if (ctrl.type == "radio")
            ctrlID = ctrl.value;
        else
            ctrlID = ctrl.name;

        if (ctrl.type == "checkbox" && !ctrl.checked)
            ctrlVal = null;
        else
            ctrlVal = ctrl.value;

        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM && !htmlPostFlag)
            JDEDTAFactory.getInstance(namespace).addToAsynQueue("ctrlExit", ctrlID, ctrlVal);
        else
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("ctrlExit", ctrlID, ctrlVal, ctrlTitle);
    }
}

FCHandler.onTabChange = function (id, cmd, namespace, isLcmPreview) {
    if (document.all && document.activeElement) {
        document.activeElement.blur();
    }
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM || JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_HIGH) {
        var elem = document.getElementById("outerJDETabBody" + id);
        saveBICacheUnderElement(elem.parentNode.parentNode);
        if (!isLcmPreview && elem != null)
            elem.style.display = "none";
    }
    window.setTimeout("JDEDTAFactory.getInstance('" + namespace + "').post('" + cmd + "')", 100);
}

FCHandler.onMOSelChange = function (elem, namespace) {
    if (JDEDTAFactory.getInstance(namespace).iLevel != ILEVEL_LOW) {
        JDEDTAFactory.getInstance(namespace).doAsynPostById(elem.name, null, elem.value);
    } else
        JDEDTAFactory.getInstance(namespace).post(elem.name);
}

FCHandler.insertErrorBtn = function (nErrCount, nWarnCount, namespace) {
    var accessibility = (document.all && window.JDEDTAFactory != undefined && JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW);
    var errorString = "&nbsp;";
    var defaultImgAlignment = "align=absmiddle";
    if (isNativeContainer) {
        // With consolidated help and a new close button to align with, bypass the existing image alignment.
        // No alignment adjustment is necessary.
        defaultImgAlignment = "style='vertical-align:-1px;'";
    }
    if (accessibility) {// typeahead get messed up if a link is clicked. accessibility needs this link.
        errorString += "<a href=javascript:onClick=" + namespace + "errors() TABINDEX=-1 onMouseOver=updateHelpCursor('" + namespace + "',this)";
        errorString += " NAME='ERRORS' ID=E" + nErrCount + "_" + nWarnCount + ">";
    }
    errorString += "<img border=0 " + defaultImgAlignment;
    if (0 == nErrCount) // then there are only warnings
        errorString += " src=\"" + window["E1RES_img_jdewarning_gif"] + "\"";
    else // there is at least 1 error
        errorString += " src=\"" + window["E1RES_img_jdeerror_gif"] + "\"";
    if (FCHandler.errorText != null)
        errorString += " alt=\"" + FCHandler.errorText + "\"";
    if (!accessibility) {
        errorString += " NAME='ERRORS' ID=E" + nErrCount + "_" + nWarnCount;
        errorString += " onclick=\"" + namespace + "errors()\" onMouseOver=\"updateHelpCursor('" + namespace + "',this)\">";
    }
    else
        errorString += "></a>";
    return errorString;

}

FCHandler.onCalcOK = function () {
    if (FCHandler.calcObj != null) {
        FCHandler.calcObj.oldCalcOK();
        FCHandler.onVAChange(FCHandler.calcObj.inputField, FCHandler.calcObj.namespace);
        FCHandler.calcObj = null;
    }
}

FCHandler.onCalcCancel = function () {
    if (FCHandler.calcObj != null) {
        FCHandler.calcObj.oldCalcCancel();
        FCHandler.calcObj = null;
    }
}

FCHandler.onCalnOK = function () {
    if (FCHandler.calnObj != null) {
        FCHandler.calnObj.oldCalnOK();
        FCHandler.onVAChange(FCHandler.calnObj.inputField, FCHandler.calnObj.namespace);
        FCHandler.calnObj = null;
    }
}

FCHandler.onCalnCancel = function () {
    if (FCHandler.calnObj != null) {
        FCHandler.calnObj.oldCalnCancel();
        FCHandler.calnObj = null;
    }
}

FCHandler.onVAChange = function (ctrl, namespace) {
    this.prevValue = ctrl.value;
    if (window.JDEDTAFactory == undefined || JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW) {
        this.markExited(ctrl);

        if (ctrl.htmlPostFlag)
            JDEDTAFactory.getInstance(namespace).postLater("");
    } else {
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM && !ctrl.htmlPostFlag)
            JDEDTAFactory.getInstance(namespace).addToAsynQueue("ctrlExit", ctrl.name, ctrl.value);
        else
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("ctrlExit", ctrl.name, ctrl.value, ctrl.title);
    }
}

FCHandler.getEndFontTag = function (fontTag) {
    var endTag = "";

    if (fontTag.indexOf("<u>") >= 0)
        endTag += "</u>";
    if (fontTag.indexOf("<s>") >= 0)
        endTag += "</s>";
    if (fontTag.indexOf("<b>") >= 0)
        endTag += "</b>";
    if (fontTag.indexOf("<i>") >= 0)
        endTag += "</i>";
    if (fontTag.indexOf("<FONT") >= 0)
        endTag += "</FONT>";
    return endTag;
}

// This func. primarily used by ECB's for IE focus issues when this control is in the grid (e.g. SAR-7178733)
FCHandler.setFocusOnElementLater = function (id, delayTime) {
    window.setTimeout("FCHandler.setFocusOnElement('" + id + "')", delayTime);
}

// This func. primarily used by ECB's for IE focus issues when this control is in the grid (e.g. SAR-7178733)
FCHandler.setFocusOnElement = function (id) {
    var elemToFocusOn = document.getElementById(id);
    if (elemToFocusOn != null && elemToFocusOn.focus != null) {
        elemToFocusOn.focus();
    }
}

// look for the focusable element (clickable hyperlink or expend/collpase icon given the id of the prefered node) if does not row has no clickable element, search downward for the next candidate
FCHandler.searchFocusOnElement = function (id) {
    if (id.indexOf("treers") == 0 && FCHandler.setFocusOnElementWithkeydown(id)) // node selector
        return;

    var targetId = TCMH.getIdOfTargetElement(id, 0);
    if (targetId && FCHandler.setFocusOnElementWithkeydown(targetId))
        return;

    var targetId = TCMH.getIdOfTargetElement(id, 1);
    if (targetId && FCHandler.setFocusOnElementWithkeydown(targetId))
        return;
}

// only set focuse on the element support keydown event (same as to support click event), return false if this element is not qualified.
FCHandler.setFocusOnElementWithkeydown = function (id) {
    var elemToFocusOn = document.getElementById(id);
    if (elemToFocusOn != null && elemToFocusOn.focus != null && elemToFocusOn.getAttribute('onkeydown')) {
        elemToFocusOn.focus();
        return true;
    }
    return false;
}

FCHandler.registerOnLoadMethod = function (namespace, methodName) {
    var onLoadVar = eval("this.onLoadMethods" + namespace);
    if (undefined == onLoadVar) {
        eval("this.onLoadMethods" + namespace + "= new Array()");
        onLoadVar = eval("this.onLoadMethods" + namespace);
    }
    onLoadVar[onLoadVar.length] = methodName;
}

FCHandler.callOnLoadMethods = function (namespace) {
    var onLoadVar = eval("this.onLoadMethods" + namespace);
    if (undefined != onLoadVar) {
        for (var i = 0; i < onLoadVar.length; i++) {
            eval(onLoadVar[i]);
        }
        onLoadVar.length = 0;
    }
    this.formLoaded = true;
}

FCHandler.addToModalStack = function (namespace, methodName) {
    var modalStackVar = eval("this.modalStack" + namespace);
    if (undefined == modalStackVar) {
        eval("this.modalStack" + namespace + "= new Array()");
        modalStackVar = eval("this.modalStack" + namespace);
    }
    modalStackVar[modalStackVar.length] = methodName;
}

FCHandler.handleModalStack = function (namespace) {

    //trigger the launching of the next nested modal form
    var modalStackVar = eval("this.modalStack" + namespace);
    if (undefined != modalStackVar && modalStackVar.length != 0) {
        //2 elements are inserted into the array, the 1st is the iframeSrc declaration, the 2nd is the creation of the popup window
        var newElem = modalStackVar.shift();
        eval(newElem)//execute AND remove the first element
        newElem = modalStackVar.shift();
        eval(newElem);
    }
    else {
        try {
            if (window.parent.FCHandler != null) {
                var modalStackVar = eval("window.parent.FCHandler.modalStack" + namespace);
                if (undefined != modalStackVar && modalStackVar.length != 0) {
                    var newElem = modalStackVar.shift();
                    window.parent.eval(newElem)//execute AND remove the first element from the parent form(non-modal form)
                    newElem = modalStackVar.shift();
                    window.parent.eval(newElem);
                }
            }
        } catch (err) {
            //This try/catch was added to handle forms embedded in iFrames 
            //that don�t have access to the parent or top document.  
        }
    }
}

FCHandler.isModalStackEmpty = function (namespace) {

    var modalStackVar = eval("this.modalStack" + namespace);
    if (undefined != modalStackVar && modalStackVar.length != 0)
        return false;
    else {
        try {
            if (window.parent.FCHandler != null) {
                var modalStackVar = eval("window.parent.FCHandler.modalStack" + namespace);
                if (undefined != modalStackVar && modalStackVar.length != 0)
                    return false;
            }
        } catch (err) {
            //This try/catch was added to handle forms embedded in iFrames 
            //that don�t have access to the parent or top document.  
        }
    }
    return true;
}

FCHandler.isFormLoaded = function () {
    return (true == this.formLoaded);
}

FCHandler.addElemIntoArray = function (arrayObj, index, elem) {
    if (arrayObj.length < index)
        arrayObj[index] = null; // grow the array incase it is short
    var tempArray = arrayObj.splice(index, arrayObj.length - index, elem);
    arrayObj = arrayObj.concat(tempArray);

    return arrayObj;
}

FCHandler.removeElemFromArray = function (arrayObj, index) {
    if (index < arrayObj.length) {
        var tempArray = arrayObj.splice(index + 1, arrayObj.length - (index + 1));
        arrayObj.pop(); // remove the deleted row
        arrayObj = arrayObj.concat(tempArray);
    }
    return arrayObj;
}

FCHandler.ExpandCollapseSF = function (namespace, id, suppressNotify) {
    var container = document.getElementById("SFContainer" + id);
    var bmp = document.getElementById("SFHdrBmp" + id);
    var sfTitle = document.getElementById("Tbl" + id);
    if (container != null) {
        if (container.getAttribute("state") == "collapsed") {
            if (bmp != null) {
                if (document.documentElement.dir == "rtl") {
                    bmp.src = window["E1RES_img_discloseexpanded_rtl_ena_png"];
                    bmp.name = "/img/discloseexpanded_rtl_ena.png";
                }
                else {
                    bmp.src = window["E1RES_img_discloseexpanded_ena_png"];
                    bmp.name = "/img/discloseexpanded_ena.png";
                }
            }
            container.setAttribute("state", "expanded");
            container.style.visibility = "visible";
            container.style.display = "";
            if (JDEDTAFactory.getInstance(namespace).jdeGrids != null)
                for (var i = 0; i < JDEDTAFactory.getInstance(namespace).jdeGrids.length; i++) {
                    var gridID = JDEDTAFactory.getInstance(namespace).jdeGrids[i].gridId;
                    if (gridID.indexOf(id + "_") == 0)
                        GCMH.setGridScrollbars(gridID);
                }
            this.adjustSFEffectedControls(id, (sfTitle != null));

            if (null != container.calendarControlId) {
                var calendarContainer = eval("calendarContainer" + container.calendarControlId);
                if (!calendarContainer.alreadyDrawn)
                    calendarContainer.drawCalendar();
            }

            // Resize on expansion; Doing so is necessary for elongation calculations/recalculations.
            if (window.gContainerId == E1URLFactory.prototype.CON_ID_WSRP) // we need to give portal more time to catch up otherwise, the space key will cause page scrolling down issue on portal
                setTimeout(function () { JSCompMgr.onResize(namespace) }, 1);
            else
                JSCompMgr.onResize(namespace)
        }
        else {
            if (bmp != null) {
                if (document.documentElement.dir == "rtl") {
                    bmp.src = window["E1RES_img_disclosecollapsed_rtl_ena_png"];
                    bmp.name = "/img/disclosecollapsed_rtl_ena.png";
                }
                else {
                    bmp.src = window["E1RES_img_disclosecollapsed_ena_png"];
                    bmp.name = "/img/disclosecollapsed_ena.png";
                }
            }
            container.setAttribute("state", "collapsed");
            container.style.visibility = "hidden";
            this.adjustSFEffectedControls(id, (sfTitle != null));
            var focusElement = document.getElementById("SFHdrHref" + id);

            if (focusElement)
                focusElement.focus();
        }
    }
    if (null == suppressNotify) {
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW)
            FCHandler.markExitedByName("toggleSubform" + id, true);
        else
            JDEDTAFactory.getInstance(namespace).post("toggleSubform" + id);
    }
}

FCHandler.setControlsBelow = function (id, height, controlsBelow) {
    var ctrl = document.getElementById("outer" + id);
    if (ctrl != null) {
        ctrl.height = height;
        ctrl.controlsBelow = controlsBelow;
    }
    /* removed since SAR 7364421 breaks reclaim whitespace in P55KGRID
    for(var i=0; i<controlsBelow.length; i++){
    var curControl = document.getElementById("outer"+controlsBelow[i]);		
    if(curControl != null){
    curControl.initialTopPosition = parseInt(curControl.style.top);
    }
    }*/
}

FCHandler.adjustEffectedControls = function (ctrl, effectedControls, adjustment) {

    if (null == effectedControls)
        return;
    for (var i = 0; i < effectedControls.length; i++) {
        if (effectedControls[i].indexOf("outer") === 0)
            var curControl = document.getElementById(effectedControls[i]);
        else
            var curControl = document.getElementById("outer" + effectedControls[i]);
        if (curControl != null) {
            // added since SAR 7364421 breaks reclaim whitespace in P55KGRID
            curControl.style.top = parseInt(curControl.style.top) + adjustment + "px";
            /* removed since SAR 7364421 breaks reclaim whitespace in P55KGRID
            curControl.style.top = curControl.initialTopPosition + adjustment + "px";
            */
        }
    }
}

// This function adjusts the Text block controls in the form to prevent
// overlap of text.

FCHandler.adjustTBEffectedControls = function (id) {

    var ctrl = document.getElementById("outer" + id);
    if (ctrl != null && ctrl.offsetHeight > ctrl.height) {
        var adjustment = ctrl.offsetHeight - ctrl.height;
        var effectedControls = ctrl.controlsBelow;
        var prevControl = ctrl;
        var curControl = document.getElementById("outer" + effectedControls[0]);
        if (curControl != null) {
            if (curControl.prevCtrl != null) {
                var prevY = parseInt(prevControl.offsetTop), curY = parseInt(curControl.offsetTop);
                // check for vertical overlap of sphere of influence. This is for Textblocks which are side by side.
                if ((curY < (prevY + prevControl.offsetHeight))) {
                    // overlap case
                    //In the overlap scenario, add the overlap height and an extra 2 px to all
                    // the below text block controls.
                    adjustment = ((prevY + prevControl.offsetHeight) - curY) + 2;
                    for (var i = 0; i < effectedControls.length; i++) {
                        curControl = document.getElementById("outer" + effectedControls[i]);
                        if (curControl != null) {
                            curControl.prevCtrl = prevControl;
                            curControl.prevMove = adjustment;
                            curControl.style.top = parseInt(curControl.style.top) + adjustment + "px";
                        }
                    }
                }
            } else {
                for (var i = 0; i < effectedControls.length; i++) {
                    curControl = document.getElementById("outer" + effectedControls[i]);
                    if (curControl != null) {
                        curControl.prevCtrl = prevControl;
                        curControl.prevMove = adjustment;
                        curControl.style.top = parseInt(curControl.style.top) + adjustment + "px";
                        prevControl = curControl;
                    }
                }
            }
        }
    }
}

FCHandler.adjustEffectedControlIdsReclaim = function (subFormIds) {
    for (var j = 0; j < subFormIds.length; j++) {
        var subFormId = subFormIds[j];
        var ctrl = document.getElementById("outer" + subFormId);
        var sfHeader = document.getElementById("SFContainer" + subFormId);
        if (ctrl && ctrl.getAttribute("reclaimspace") == "true" && ctrl.getAttribute("hasAdjacentControls") != "true")
            FCHandler.adjustEffectedControlsReclaim(subFormId);
        else {
            var sfTitle = document.getElementById("Tbl" + subFormId);
            FCHandler.adjustSFEffectedControls(subFormId, (sfTitle != null));
        }
    }
}

FCHandler.adjustSFEffectedControls = function (id, hasTitle) {

    var ctrl = document.getElementById("outer" + id);
    var sfHeader = document.getElementById("SFContainer" + id);
    var title = document.getElementById("Tbl" + id);
    if (ctrl != null) {
        if (sfHeader && sfHeader.getAttribute("state") == "collapsed")
            var adjustment = ctrl.offsetHeight - ctrl.height - sfHeader.clientHeight;
        //  var adjustment = (title?title.offsetHeight:0) - sfHeader.clientHeight;
        else
            var adjustment = ctrl.offsetHeight - ctrl.height;
        if (hasTitle)
            adjustment -= 20;
        if (null != ctrl.prevAdjustment) {
            // added since SAR 7364421 breaks reclaim whitespace in P55KGRID
            adjustment -= ctrl.prevAdjustment;
            ctrl.prevAdjustment += adjustment;
        }
        else
            ctrl.prevAdjustment = adjustment;
        if (!ctrl.controlsBelow) {
            var me = JSCompMgr.getNodeByID(id);
            ctrl.controlsBelow = ToggleGroupBox.getControlsBelow(me, id);
        }
        this.adjustEffectedControls(ctrl, ctrl.controlsBelow, adjustment);
    }
}

FCHandler.adjustEffectedControlsReclaim = function (id) {
    var ctrl = document.getElementById("outer" + id);
    var sfHeader = document.getElementById("SFContainer" + id);
    var hasTitle = document.getElementById("Tbl" + id);
    var hasAdjacentControls = ctrl.getAttribute("hasAdjacentControls");
    var spaceReclaimed = ctrl.style.visibility == "hidden" ? "true" : "false";
    if (!hasAdjacentControls) {
        // this should NEVER happen
        hasAdjacentControls = "false";
    }
    if (hasAdjacentControls == "false") {
        this.repositionControlsReclaim(id);
    }
}

FCHandler.repositionControlsReclaim = function (id) {
    var ctrl = document.getElementById("outer" + id);
    var isSubForm = document.getElementById("SFContainer" + id) ? true : false;
    if (isSubForm && ctrl.controlsBelow)
        var controlIds = ctrl.controlsBelow;
    else {
        var me = JSCompMgr.getNodeByID(id);
        var controlIds = ToggleGroupBox.getControlsBelow(me, id);
    }
    for (var j = 0; j < controlIds.length; j++) //All group boxes below the collapsed group boxes should be adjusted for the top coordinate
    {
        var controlId;
        var topValue;
        if (isSubForm) {
            if (controlIds[j].indexOf("outer") === 0)
                var controlId = controlIds[j];
            else
                var controlId = "outer" + controlIds[j];
            topValue = this.calculateSFTopValueReclaim(id, controlId);
        }
        else {
            controlId = controlIds[j];
            topValue = this.calculateGBTopValueReclaim(id, controlId);
        }
        document.getElementById(controlId).style.top = topValue + "px";
    }
}


FCHandler.calculateSFTopValueReclaim = function (id, controlId) {
    var ctrl = document.getElementById("outer" + id);
    var sfHeader = document.getElementById("SFContainer" + id);
    var ctrlVisible = ctrl.style.visibility == "hidden" ? false : true;
    var title = document.getElementById("Tbl" + id);
    var ctrlToAdjust = document.getElementById(controlId);
    var initialTop = ctrlToAdjust.style.top;
    var initialTopValue = initialTop.substring(0, initialTop.length - 2);
    var titleAdjust = title == null ? 0 : title.offsetHeight == 0 ? 17 : title.offsetHeight;
    if ((sfHeader && sfHeader.getAttribute("state") == "collapsed") || sfHeader.clientHeight == 0) {
        if (ctrl.prevAdjustment !== undefined)
            var heightValue = ctrl.height + titleAdjust + ctrl.prevAdjustment;
        else
            if (!ctrlVisible || ctrl.offsetHeight == 0)
                var heightValue = ctrl.height + titleAdjust;
            else {
                var heightValue = title.offsetHeight;
                ctrl.prevAdjustment = 0 - sfHeader.offsetHeight
            }
    }
    else {
        var heightValue = ctrl.offsetHeight;
    }
    if (ctrlVisible) {
        return parseInt(initialTopValue) + parseInt(heightValue);
    }
    else {
        return parseInt(initialTopValue) - parseInt(heightValue);
    }
}

FCHandler.calculateGBTopValueReclaim = function (id, controlId) {
    var groupBoxOuter = document.getElementById("outer" + id);
    var groupBoxTitle = document.getElementById("WGBTitle" + id);
    var groupBoxContainer = document.getElementById("WGBContainer" + id);
    var groupBoxDiv = document.getElementById("div" + id);
    var ctrlVisible = groupBoxOuter.style.visibility == "hidden" ? false : true;
    var ctrlToAdjust = document.getElementById(controlId);
    var initialTop = ctrlToAdjust.style.top;
    var initialTopValue = initialTop.substring(0, initialTop.length - 2);
    var titleAdjust = groupBoxTitle == null ? 0 : groupBoxTitle.offsetHeight;
    if (groupBoxDiv && groupBoxDiv.clientHeight == 0)
        if (groupBoxOuter.getAttribute("reclaimspace") == "true" && !ctrlVisible)
            var heightValue = groupBoxOuter.offsetHeight;
        else
            var heightValue = titleAdjust;
    else
        var heightValue = groupBoxOuter.offsetHeight;

    if (ctrlVisible) {
        return parseInt(initialTopValue) + parseInt(heightValue);
    }
    else {
        groupBoxContainer.style.visibility = "hidden";
        return parseInt(initialTopValue) - parseInt(heightValue);
    }
}

FCHandler.setFocusIfNotAlready_ComboBox = function (elem) {
    if (this.isSafari && !this.safariComboBoxhasFocus) {
        elem.focus();
    }
}

FCHandler.getWindowScrollPosition = function () {
    var offsetX = 0, offsetY = 0;
    var formDiv = document.getElementById("e1formDiv");
    if (formDiv != null) {
        offsetX = formDiv.scrollLeft;
        offsetY = formDiv.scrollTop;
    }
    else {
        if (window.pageYOffset !== undefined) {
            offsetX = window.pageXOffset;
            offsetY = window.pageYOffset;
        }
        // IE8 workarounds in "else ifs"
        else if (document.body &&
           (document.body.scrollLeft || document.body.scrollTop)) {
            offsetX = document.body.scrollLeft;
            offsetY = document.body.scrollTop;
        }
        else if (document.documentElement &&
                (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
            offsetX = document.documentElement.scrollLeft;
            offsetY = document.documentElement.scrollTop;
        }
    }
    return [offsetX, offsetY];
}
FCHandler.setWindowScrollPosition = function (offsetX, offsetY) {
    if (offsetX < 0)
        offsetX = 0;
    if (offsetY < 0)
        offsetY = 0;
    var formDiv = document.getElementById("e1formDiv");
    if (formDiv != null) {
        formDiv.scrollLeft = offsetX;
        formDiv.scrollTop = offsetY;
    }
    else {
        if (window.pageYOffset !== undefined) {
            offsetX = window.pageXOffset;
            offsetY = window.pageYOffset;
        }
        // IE8 workarounds in "else ifs"
        else if (document.body &&
                (document.body.scrollLeft || document.body.scrollTop)) {
            offsetX = document.body.scrollLeft;
            offsetY = document.body.scrollTop;
        }
        else if (document.documentElement &&
                (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
            offsetX = document.documentElement.scrollLeft;
            offsetY = document.documentElement.scrollTop;
        }
    }
    return [offsetX, offsetY];
}
FCHandler.setMMFFocus = function (focusElmName) {
    var focusElementMMF;
    if (focusElmName != null)
        focusElementMMF = document.getElementsByName(focusElmName);
    else if (minTabIdxElement != null)
        focusElementMMF = document.getElementsByName(minTabIdxElement);
    if (focusElementMMF != null && focusElementMMF[0] != null && focusElementMMF[0].focus != null && focusElementMMF[0].disabled != true)
        focusElementMMF[0].focus();
}

FCHandler.isAdvancedQueryMode = function () {
    var isAdvancedQueryMode = (window.JSSidePanelRender && JSSidePanelRender.getMode() == JSSidePanelRender.MODE_QUERY);
    return isAdvancedQueryMode;
}

FCHandler.showAutoSuggestIndicator = function (ctrl, grid, ctrlId, minCharNumber, hotKeyRequired, namespace, type) {

    // add AutoSuggest icon
    var iconId = "ASIcon_" + ctrlId;
    var icon = document.getElementById(iconId);
    if (icon) // reuse the one already created ealier
    {
        icon.style.display = "inline";
    }
    else // create a new icon and make offset left to the nature position, so that it can overlap with input cell
    {
        var isAutoSuggest = 1;
        if (hotKeyRequired)
            isAutoSuggest = 0;
        icon = document.createElement("img");
        icon.title = autoSuggestText;
        icon.onclick = function () { FCHandler.onClickTypeAheadIcon(ctrl, grid, ctrlId, minCharNumber, isAutoSuggest, namespace, type) };
        icon.onmousedown = function () { FCHandler.onMouseDownTypeAheadIcon() };

        icon.setAttribute("valign", "center");
        icon.id = iconId;
        icon.style.position = "relative";
        icon.style.top = "2px";

        if (ctrl.style.textAlign == "right") {
            // Field is RIGHT JUSTIFIED           
            var shift = 0 - ctrl.offsetWidth + 1;
            icon.style.left = shift + "px";
            icon.style.zIndex = ctrl.style.zIndex + 0;
        }
        else {
            if (isRTL) {
                // Field is RIGHT JUSTIFIED           
                icon.style.left = "13px";
                icon.style.zIndex = ctrl.style.zIndex + 1;
            }
            else {
                // Field is LEFT JUSTIFIED
                icon.style.left = "-13px";
                icon.style.zIndex = ctrl.style.zIndex + 1;
            }
        }

        icon.style.width = "11px";
        icon.style.height = "11px";

        FCHandler.insertAutoSuggestIcon(icon, ctrl);
    }

    FCHandler.updateAutoSuggestIndicatorInternal(ctrl, ctrl.value, minCharNumber, hotKeyRequired, icon);

    // adjust the VA icon
    var imgTags = ctrl.parentNode.getElementsByTagName('IMG');

    // var vaIcon = imgTags[imgTags.length-1]; 
    var vaIcon = null;

    for (var i = 0; i < imgTags.length; i++) {
        if (imgTags[i].name.indexOf("va", 0) == 0 || imgTags[i].className == "JSVisualAssist") {
            vaIcon = imgTags[i];
            break;
        }
    }

    if (vaIcon) {
        if (isRTL) {
            vaIcon.style.position = "relative";
            vaIcon.style.left = "11px";
        }
        else {
            vaIcon.style.position = "relative";
            vaIcon.style.left = "-11px";
        }
    }
}

FCHandler.insertAutoSuggestIcon = function (icon, targetElement) {
    // target is the textfield, QBE cell or grid cell
    var parent = targetElement.parentNode; 	//if the parents lastchild is the targetElement...	
    if (parent.lastchild == targetElement) {
        //add the newElement after the target element.		
        parent.appendChild(icon);
    }
    else {
        // else the target has siblings, insert the new element between the target and it's next sibling.		
        parent.insertBefore(icon, targetElement.nextSibling);
    }
    targetElement.className += " autosuggesttextfield";
}


FCHandler.hideAutoSuggestIndicator = function (ctrlId) {
    var iconId = "ASIcon_" + ctrlId;

    var icon = document.getElementById(iconId);
    if (icon) // reuse the one already created ealier
    {
        icon.style.display = "none";
    }
}

FCHandler.updateAutoSuggestIndicatorInternal = function (ctrl, value, minCharNumber, hotKeyRequired, icon) {
    var trimmedValue = value.RTrim();

    if (trimmedValue.length < minCharNumber || trimmedValue == '*') {
        // Disabled arrow
        if (icon) {
            icon.src = window["E1RES_img_AutoSuggest_disabled_gif"];
        }
    }
    else {
        if (hotKeyRequired) {
            // Clickable arrow
            if (icon)
                icon.src = window["E1RES_img_AutoSuggest_clickable_gif"];
        }
        else {
            // Active arrow
            // AUTO SEARCH - B - Need to comment out this line
            // icon.src = window["E1RES_img_AutoSuggest_active_gif"];
            if (icon)
                icon.src = window["E1RES_img_AutoSuggest_disabled_gif"];
            // AUTO SEARCH - E
        }
    }
}


// AUTO SEARCH - B
FCHandler.setAutoSuggestAutoSearchIcon = function (icon) {
    if (icon)
        icon.src = window["E1RES_img_AutoSuggest_active_gif"];
}

FCHandler.resetAutoSuggestAutoSearchIcon = function (icon) {
    if (icon)
        icon.src = window["E1RES_img_AutoSuggest_disabled_gif"];
}
// AUTO SEARCH - E

FCHandler.updateAutoSuggestIndicator = function (ctrl, ctrlId, minCharNumber, hotKeyRequired) {
    var iconId = "ASIcon_" + ctrlId;
    var icon = document.getElementById(iconId);
    if (icon != null)
        FCHandler.updateAutoSuggestIndicatorInternal(ctrl, ctrl.value, minCharNumber, hotKeyRequired, icon);
}

FCHandler.hideFormAndRowExit = function () {
    if (window.hideFormAndRowExits) {
        var formExit = document.getElementById('FORM_EXIT_BUTTON');
        //hard-coded node traverse
        if (formExit != null) {
            formExit = formExit.parentNode.parentNode.parentNode.parentNode.parentNode;
            if (!this.isIOS) {
                formExit = formExit.parentNode;
            }
            formExit.parentNode.style.width = 0 + 'px';
            formExit.parentNode.style.minWidth = 0 + 'px';
            formExit.style.display = 'none';
            formExit.parentNode.nextSibling.style.display = 'none';
        }
        var rowExit = document.getElementById('ROW_EXIT_BUTTON');
        if (rowExit != null) {
            rowExit = rowExit.parentNode.parentNode.parentNode.parentNode.parentNode;
            if (!this.isIOS) {
                rowExit = rowExit.parentNode;
            }
            rowExit.parentNode.style.width = 0 + 'px';
            rowExit.parentNode.style.minWidth = 0 + 'px';
            rowExit.style.display = 'none';
            rowExit.parentNode.nextSibling.style.display = 'none';
        }
    }
}

FCHandler.notifyAllParallel = function (namespace) {
    var dta = JDEDTAFactory.getInstance(namespace);
    dta.parallelEventQueue.push(new JDEDTAEventObj("parallel", "ALL", "ALL"));
    dta.performAsynchParallelDataTransfer(true);
}

//////////////////////////////////////////////////
// In Your Face Message Handler
//////////////////////////////////////////////////
function INYFEHandler(namespace, nextErr, prevErr, summary, hiddenError) {
    this.namespace = namespace;
    this.errorList = [];
    this.oldErrorList = [];
    this.currentErrorObj = null;
    this.focusElement = null;
    this.errorWindowClicked = false;
    this.closeWindowTimeout = null;
    this.nextErrText = nextErr + " (Alt+Q)";
    this.prevErrText = prevErr;
    this.summaryText = summary;
    this.hiddenErrorText = hiddenError;
    this.focusErrorOnTabSwitch = false;
    this.errorObjToSetFocus = null;
    this.currentFullyQualifiedId = null;
    this.jumpToError = -1;
    this.eventListener = null;
    this.isSafari = false;
    this.prevHeight = 0;
    this.enableErrorPopup = true;
    this.goToErrorWindowClicked = false;
    this.resizing = false;
}

INYFEHandler.prototype.setIsResizing = function () {
    this.resizing = true;
}

INYFEHandler.prototype.resetIsResizing = function () {
    this.resizing = false;
}

INYFEHandler.prototype.isResizing = function () {
    return this.resizing;
}

INYFEHandler.prototype.isErrorPopupEnabled = function () {
    return this.enableErrorPopup;
}

INYFEHandler.prototype.setEnableErrorPopup = function (errorPopupOption) {
    this.enableErrorPopup = errorPopupOption;
}

INYFEHandler.prototype.shouldFocusOnErrorAfterTabSwitch = function () {
    return this.focusErrorOnTabSwitch;
}

INYFEHandler.prototype.setFocusOnErrorAfterTabSwitch = function () {
    this.locateAndSetFocus(this.errorObjToSetFocus);
    this.focusErrorOnTabSwitch = false;
    this.errorObjToSetFocus = null;
}

INYFEHandler.prototype.updateErrors = function () {
    this.showScrollBar();
    this.refreshWindowOnErrorObj();

    //adjust the window scrollbar position
    var currScrollPosn = JDEDTAFactory.getInstance(this.namespace).fcHandler.getWindowScrollPosition();
    var posX = currScrollPosn[0];
    var posY = currScrollPosn[1];
    var errSumWindow = document.getElementById("jdeINYFE" + this.namespace);
    var errSumWindowHChange = errSumWindow.offsetHeight - this.prevHeight;
    posY = posY + errSumWindowHChange;
    FCHandler.setWindowScrollPosition(posX, posY);
}

INYFEHandler.prototype.setPrevHeight = function (height) {
    this.prevHeight = height;
}
INYFEHandler.prototype.refreshWindowOnErrorObj = function () {
    // SAR 7512181 INYFE_Post validation position
    if (this.currentErrorObj != null) {
        var errorIndex = this.findItemByControlId(this.currentErrorObj.id);
        if (errorIndex == -1) {
            this.closeErrorWindow(false);
        }
    }

    if (this.currentErrorObj != null) {
        if (this.currentErrorObj.top == null && this.currentErrorObj.left == null &&
            this.currentErrorObj.width == null && this.currentErrorObj.height == null) {
            if (this.windowElem != null && this.focusElement != null) {
                var left = this.getErrorWindowLeft(this.focusElement, this.windowElem.getWidth());
                var top = this.getErrorWindowTop(this.focusElement, this.windowElem.getHeight());
                var errSumWindow = document.getElementById("jdeINYFE" + this.namespace);
                var errSumWindowHChange = errSumWindow.offsetHeight - this.prevHeight;
                top = top - errSumWindowHChange;
                this.windowElem.moveTo(left, top);
            }
        }
        this.refreshPreviousNextButton(this.currentErrorObj);
    }
}

INYFEHandler.prototype.showScrollBar = function () {
    var inyfeContent = document.getElementById("INYFEContent" + this.namespace);
    if (inyfeContent != null) {
        if (this.isScrollBarNeeded(inyfeContent.offsetHeight)) {
            inyfeContent.className = "RaisedBorders";
            inyfeContent.style.padding = "3px";
            inyfeContent.style.margin = "3px";
            inyfeContent.style.height = "125px";
            inyfeContent.style.overflow = "auto";
        }
        var firstGrid = JDEDTAFactory.getInstance(this.namespace).getFirstGrid();
        if (firstGrid != null)
            firstGrid.adjustFormBelowGrid();
    }
}

INYFEHandler.prototype.isScrollBarNeeded = function (divHeight) {
    if (divHeight > 125) {
        return true;
    }
    return false;
}

INYFEHandler.prototype.ErrorObj = function (id, isWarning, controlType, title, parentTabId, tabPageNumber, tabIndex, context) {
    this.id = id;
    this.isWarning = isWarning;
    this.controlType = controlType;
    this.title = title;
    this.parentTabId = parentTabId;
    this.tabPageNumber = tabPageNumber;
    this.tabIndex = tabIndex;
    this.top = this.left = this.width = this.height = null;
    this.context = context;
}

INYFEHandler.prototype.addError = function (id, isWarning, controlType, title, parentTabId, tabPageNumber, tabIndex, context) {
    var errorObj = new INYFEHandler.prototype.ErrorObj(id, isWarning, controlType, title, parentTabId, tabPageNumber, tabIndex, context);
    if (this.oldErrorList.length > 0) {
        var errorIndex = this.findItemByContextAndControlId(errorObj, this.oldErrorList);
        if (errorIndex != -1) {
            errorObj.top = this.oldErrorList[errorIndex].top;
            errorObj.left = this.oldErrorList[errorIndex].left;
            errorObj.width = this.oldErrorList[errorIndex].width;
            errorObj.height = this.oldErrorList[errorIndex].height;
        }
    }
    this.errorList[this.errorList.length] = errorObj;
}

INYFEHandler.prototype.resetErrors = function () {
    this.oldErrorList = this.errorList;
    this.errorList = new Array();
}

INYFEHandler.prototype.removeItem = function (errorObj) {
    var errorIndex = -1;
    for (var i = 0; i < this.errorList.length; i++) {
        if (this.errorList[i].id == errorObj.id) {
            errorIndex = i;
            break;
        }
    }

    if (errorIndex >= 0 && errorIndex < this.errorList.length) {
        for (var i = errorIndex; i < this.errorList.length - 1; i++) {
            this.errorList[i] = this.errorList[i + 1];
        }
        this.errorList.length = this.errorList.length - 1;
    }
}

INYFEHandler.prototype.findItem = function (id) {
    var errorIndex = -1;
    for (var i = 0; i < this.errorList.length; i++) {
        if (this.errorList[i].id == id) {
            errorIndex = i;
            break;
        }
    }
    return errorIndex;
}

INYFEHandler.prototype.findItemByControlId = function (id) {
    var errorIndex = -1;
    var controlId = null;
    errorIndex = this.findItem(id);
    if (errorIndex == -1) {
        for (var i = 0; i < this.errorList.length; i++) {
            controlId = this.errorList[i].id.substring(0, this.errorList[i].id.lastIndexOf("_"));
            if (controlId == id.substring(0, id.lastIndexOf("_"))) {
                errorIndex = i;
                break;
            }
        }
    }
    return errorIndex;
}

INYFEHandler.prototype.findItemByContextAndControlId = function (errorObj, errorList) {
    var errorIndex = -1;
    var controlId = null;
    for (var i = 0; i < errorList.length; i++) {
        controlId = errorList[i].id.substring(0, errorList[i].id.lastIndexOf("_"));
        if (controlId == errorObj.id.substring(0, errorObj.id.lastIndexOf("_")) &&
            errorList[i].context == errorObj.context &&
            errorList[i].title == errorObj.title) {
            errorIndex = i;
            break;
        }
    }
    return errorIndex;
}

INYFEHandler.prototype.hidePreviousNextButton = function (errorId) {
    if (this.errorList.length == 1) {
        this.hidePreButton();
        this.hideNextButton();
    }
    else {
        if (this.findItem(errorId) == 0) {
            this.hidePreButton();
        }
        if (this.findItem(errorId) == this.errorList.length - 1) {
            this.hideNextButton();
        }
    }
}

INYFEHandler.prototype.refreshPreviousNextButton = function (errorObj) {
    if (this.errorList.length == 1) {
        this.hidePreButton();
        this.hideNextButton();
    }
    else {
        var errorIndex = this.findItemByContextAndControlId(errorObj, this.errorList);
        if (errorIndex != -1) {
            if (errorIndex == 0) {
                this.hidePreButton();
            }
            else if (errorIndex > 0) {
                this.enablePreButton();
            }
            if (errorIndex == this.errorList.length - 1) {
                this.hideNextButton();
            }
            else if (errorIndex < this.errorList.length - 1) {
                this.enableNextButton();
            }
        }
    }
}

INYFEHandler.prototype.isErrorOnControl = function (errorId, controlId) {
    if (errorId.indexOf(controlId) >= 0 &&
        errorId.charAt(errorId.indexOf(controlId) + controlId.length) == "_")
        return true;
    else
        return false;
}

INYFEHandler.prototype.checkAndDisplayErrorWindowOnFocus = function (ctrl, fullyQualifiedId) {
    var errorId = null;

    if (this.currentErrorObj != null) {
        this.closeErrorWindow(false);
    }
    // Make sure the currentFullyQualifiedId and fullyQualifiedId 
    // refer to the same control
    if (this.currentFullyQualifiedId != null &&
        this.isErrorOnControl(this.currentFullyQualifiedId, fullyQualifiedId)) {
        errorId = this.currentFullyQualifiedId;
    }
    if (errorId == null) {
        for (var i = 0; i < this.errorList.length; i++) {
            if (this.isErrorOnControl(this.errorList[i].id, fullyQualifiedId)) {
                errorId = this.errorList[i].id;
                this.currentFullyQualifiedId = errorId;
                break;
            }
        }
    }
    if (errorId == null) {
        this.currentFullyQualifiedId = null;
    }

    if (errorId != null && this.isErrorPopupEnabled()) {
        if (this.closeWindowTimeout != null) {
            window.clearTimeout(this.closeWindowTimeout);
            this.closeWindowTimeout = null;
        }
        this.showErrorWindow(ctrl, errorId);
        this.hidePreviousNextButton(errorId);
    }
    return errorId;
}

INYFEHandler.prototype.checkAndreturnErrorString = function (ctrl, gridCellId) {
    var errorString = "";
    var errorId = null;
    var errorDesc = null;
    for (var i = 0; i < this.errorList.length; i++) {
        if (this.isErrorOnControl(this.errorList[i].id, gridCellId)) {
            errorId = this.errorList[i].id;
            var errorObj = this.errorList[this.findItemByControlId(errorId)];
            //Error/Warning message
            errorString += errorObj.title;
            //Cause & Resolution
            errorDesc = document.getElementById("DESCID_" + errorId);
            if (errorDesc != null && errorDesc != undefined && errorDesc.outerText) {
                errorString += ". " + errorDesc.outerText;
            }
        }
    }
    return errorString;
}

INYFEHandler.prototype.locateAndSetFocus = function (errorObj) {
    var focusId = null, focusElem = null;
    if (errorObj.controlType != "VTGrid" && errorObj.controlType != "VTPCGrid") {
        focusId = this.getFocusId(errorObj);
        focusElem = document.getElementById(focusId);

        //if error is set on a collapsible group box, expand it first if needed
        var parentElem = focusElem.parentNode;
        var groupBoxes = new Array();
        while (parentElem && parentElem.id != 'e1formDiv') {
            if (parentElem.className == "CollapsableContainer") {
                groupBoxes.push(parentElem.parentNode.id.substring(5));
            }
            parentElem = parentElem.parentNode;
        }
        for (var i = groupBoxes.length - 1; i >= 0; i--) {
            //the function should return if the groupbox is already expanded
            ToggleGroupBox.toggle(groupBoxes[i], "expand");
        }

        if (focusElem && focusElem.focus != null) {
            try {
                focusElem.focus();
            }
            catch (problem) {
                alert(this.hiddenErrorText);
            }
            this.jumpToError = -1;
        }
        else if (errorObj.parentTabId != null && errorObj.tabPageNumber != -1) {
            this.switchTabToError(errorObj);
        }
        else {
            this.jumpToValidError(errorObj);
        }
    }
    else {
        var controlId = errorObj.id.substring(0, errorObj.id.indexOf("."));
        focusElem = JDEDTAFactory.getInstance(this.namespace).resolveGrid(controlId);

        if (focusElem) {
            var row = errorObj.id.substring(errorObj.id.indexOf(".") + 1, errorObj.id.lastIndexOf("."));
            var col = errorObj.id.substring(errorObj.id.lastIndexOf(".") + 1, errorObj.id.lastIndexOf("_"));
            var rowIndex = parseInt(row);
            var colIndex = parseInt(col);

            //Check if the gridObject exists in the DOM or not, if it does not exist, it means the grid is on an inactive tab, should switch tab.                        
            if (document.getElementById('jdeGridContainer' + controlId) != null && focusElem.setFocusOnCellInSection(rowIndex, colIndex)) {
                this.jumpToError = -1;
            }
            else if (errorObj.parentTabId != null && errorObj.tabPageNumber != -1) {
                this.switchTabToError(errorObj);
            }
            else {
                this.jumpToValidError(errorObj);
            }
        }
        else {
            //if focusElem is null, it means that the grid hasn't been registered in JDEDTA yet
            if (errorObj.parentTabId != null && errorObj.tabPageNumber != -1) {
                this.switchTabToError(errorObj);
            }
            else {
                this.jumpToValidError(errorObj);
            }
        }
    }
    return focusElem;
}

INYFEHandler.prototype.jumpToValidError = function (errorObj) {
    this.removeItem(errorObj);
    if (this.currentErrorObj != null)
        this.hidePreviousNextButton(this.currentErrorObj.id);
    switch (this.jumpToError) {
        case 0:
        case 1:
            if (this.errorList.length > 1)
                this.nextError();
            break;

        case 2:
            if (this.errorList.length > 1)
                this.previousError();
            break;

        default:
            alert(this.hiddenErrorText);
            break;
    }
}

INYFEHandler.prototype.switchTabToError = function (errorObj) {
    var tbkbHandler = null;
    for (var i = 0; i < FormKeyboardHandler.tabKBHandlers.length; i++) {
        if (errorObj.parentTabId == FormKeyboardHandler.tabKBHandlers[i].id) {
            tbkbHandler = FormKeyboardHandler.tabKBHandlers[i];
            break;
        }
    }
    if (null != tbkbHandler && !this.focusErrorOnTabSwitch) {
        this.focusErrorOnTabSwitch = true;
        var modelIndex = errorObj.tabPageNumber;
        var viewIndex = tbkbHandler.getViewIndex(modelIndex);
        if (tbkbHandler.switchTab(viewIndex, this.namespace, modelIndex)) {
            this.errorObjToSetFocus = errorObj;
        }
        else {
            //Error on hidden tab page, jump to valid error
            this.focusErrorOnTabSwitch = false;
            this.jumpToValidError(errorObj);
        }
    }
}

INYFEHandler.prototype.getFocusId = function (errorObj) {
    var focusId = null;
    var controlId = errorObj.id.substring(0, errorObj.id.lastIndexOf("_"));

    switch (errorObj.controlType) {
        case JSConstant.eTextField:
            focusId = JSConstant.pfControl + controlId;
            break;

        case JSConstant.eCheckBox:
            focusId = JSConstant.pfControl + controlId;
            break;

        case JSConstant.eRadioButton:
            focusId = JSConstant.pfControl + controlId;
            break;

        case JSConstant.eLabel:
            focusId = JSConstant.pfLabelDiv + controlId;
            break;

        case JSConstant.eButton:
            focusId = JSConstant.pfControl + controlId;
        case JSConstant.eBitmap:
        case JSConstant.eTextBlock:
            // NOT Allowed 
            break;

        case JSConstant.eMediaObject:
            // Try only media object text. Rest not allowed
            focusId = JSConstant.pfMediaObjectText + controlId;
            break;

        case JSConstant.eComboBox:
        case JSConstant.eSaveQueryComboBox:
            focusId = JSConstant.pfControl + controlId;
            break;

        default:
            // Unknown type or items that application developers should not be setting focus on
            break;
    }
    return focusId;
}

INYFEHandler.prototype.goToError = function (id) {
    var errorIndex = this.findItem(id);
    this.jumpToError = -1;
    this.currentFullyQualifiedId = id;
    this.goToErrorWindowClicked = true;
    if (errorIndex != -1) {
        var focusElem = this.locateAndSetFocus(this.errorList[errorIndex]);
    }
    else {
        alert(this.hiddenErrorText);
    }
}

INYFEHandler.prototype.goToWarning = function (id) {
    var errorIndex = this.findItem(id);
    this.jumpToError = -1;
    this.currentFullyQualifiedId = id;
    this.goToErrorWindowClicked = true;
    if (errorIndex != -1) {
        var focusElem = this.locateAndSetFocus(this.errorList[errorIndex]);
    }
    else {
        alert(this.hiddenErrorText);
    }
}

INYFEHandler.prototype.toggleDescription = function (widgetID, descriptionID, isError) {
    var toggleWidget = document.getElementById(widgetID);
    var theDescription = document.getElementById(descriptionID);
    if (theDescription.style.display == "none") {
        theDescription.style.display = "block";
        if (isError)
            toggleWidget.src = window["E1RES_img_err_d_gif"];
        else
            toggleWidget.src = window["E1RES_img_warn_d_gif"];
    }
    else {
        theDescription.style.display = "none";
        if (isError) {
            if (document.documentElement.dir == "rtl")
                toggleWidget.src = window["E1RES_img_err_r_rtl_gif"];
            else
                toggleWidget.src = window["E1RES_img_err_r_gif"];
        }
        else {
            if (document.documentElement.dir == "rtl")
                toggleWidget.src = window["E1RES_img_warn_r_rtl_gif"];
            else
                toggleWidget.src = window["E1RES_img_warn_r_gif"];
        }
    }
    this.showScrollBar();
}

INYFEHandler.prototype.toggleSummary = function (namespace, titleAppend, collapseAlt, expandAlt) {
    var toggleWidget = document.getElementById("collapsed_expanded_icon" + namespace);
    var content = document.getElementById("summary_content" + namespace);
    var title = document.getElementById("title_append" + namespace);
    if (content.style.display == "none") {
        this.expandSummary(title, content, toggleWidget, collapseAlt);
        if (!document.all) {
            // firefox need explicit div width
            var contentDiv = document.getElementById("INYFEContent" + namespace);
            if (this.isScrollBarNeeded(contentDiv.offsetHeight)) {
                // Scroll bar has extra width 15 
                contentDiv.style.width = content.offsetWidth - 27 + "px";
            }
            else {
                contentDiv.style.width = content.offsetWidth - 12 + "px";
            }
        }
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW)
            FCHandler.markExitedByName("toggleErrorSummary" + "." + "true");
        else
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithProcIndicator("toggleErrorSummary", null, true, this.NO_PROC_INDICATOR);
        this.showScrollBar();
    }
    else {
        this.collapseSummary(title, content, toggleWidget, titleAppend, expandAlt);
        var firstGrid = JDEDTAFactory.getInstance(namespace).getFirstGrid();
        if (firstGrid != null)
            firstGrid.adjustFormBelowGrid();
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW)
            FCHandler.markExitedByName("toggleErrorSummary" + "." + "false");
        else
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithProcIndicator("toggleErrorSummary", null, false, this.NO_PROC_INDICATOR);
    }
}

INYFEHandler.prototype.resetErrorSummary = function (namespace, titleAppend, expandAlt, errorSummaryExpanded) {
    var toggleWidget = document.getElementById("collapsed_expanded_icon" + namespace);
    var content = document.getElementById("summary_content" + namespace);
    var title = document.getElementById("title_append" + namespace);
    if (errorSummaryExpanded == false) {
        this.collapseSummary(title, content, toggleWidget, titleAppend, expandAlt);
    }
}

INYFEHandler.prototype.collapseSummary = function (title, content, toggleWidget, titleAppend, expandAlt) {
    content.style.display = "none";
    title.innerHTML = titleAppend;
    if (document.documentElement.dir == "rtl") {
        toggleWidget.src = window["E1RES_img_icon_collapsed_rtl_gif"];
    }
    else {
        toggleWidget.src = window["E1RES_img_icon_collapsed_gif"]
    }
    toggleWidget.alt = expandAlt;
    toggleWidget.setAttribute("title", expandAlt);
}

INYFEHandler.prototype.expandSummary = function (title, content, toggleWidget, collapseAlt) {
    content.style.display = "block";
    title.innerHTML = "";
    if (document.documentElement.dir == "rtl") {
        toggleWidget.src = window["E1RES_img_icon_expanded_rtl_gif"];
    }
    else {
        toggleWidget.src = window["E1RES_img_icon_expanded_gif"];
    }
    toggleWidget.alt = collapseAlt;
    toggleWidget.setAttribute("title", collapseAlt);
}


INYFEHandler.prototype.toggleDetail = function (detailID, detailIconID) {
    var toggleDetail = document.getElementById(detailID);
    var toggleDetailIcon = document.getElementById(detailIconID);

    if (toggleDetail.style.display == "none") {
        toggleDetail.style.display = "block";
        toggleDetailIcon.src = window["E1RES_img_warn_d_gif"];
    }
    else {
        toggleDetail.style.display = "none";
        if (document.documentElement.dir == "rtl")
            toggleDetailIcon.src = window["E1RES_img_warn_r_rtl_gif"];
        else
            toggleDetailIcon.src = window["E1RES_img_warn_r_gif"];
    }

    this.showScrollBar();
}

INYFEHandler.prototype.toggleErrorPopup = function (namespace) {
    var errorCheckbox = document.getElementById("errorPopup" + namespace);
    this.enableErrorPopup = errorCheckbox.checked;
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW)
        FCHandler.markExitedByName(errorCheckbox.name + "." + errorCheckbox.checked);
    else
        JDEDTAFactory.getInstance(namespace).doAsynPostById("toggleErrorPopup", null, this.enableErrorPopup);
}

INYFEHandler.prototype.hidePreButton = function () {
    var textContainer = document.getElementById("preButton" + this.namespace);
    textContainer.style.display = "none";
    var tdTag = document.getElementById("tdPreButton" + this.namespace);
    tdTag.onclick = null;
}

//Need to redirect otherwise "this" is not correct.
INYFEHandler.prototype.redirectPreviousError = function () {
    var namespace = JDEDTAFactory.getFirstNamespace();
    eval("inyfeHandler" + namespace + ".previousError()");
}

INYFEHandler.prototype.enablePreButton = function () {
    var sb = new PSStringBuffer();

    sb.append("<img src='").append(isRTL ? window["E1RES_img_prevmon_rtl_gif"] : window["E1RES_img_prevmon_gif"]).append('\' alt=\"').append(this.prevErrText).append("\"");
    sb.append(" onMouseOver=\"this.src='").append(isRTL ? window["E1RES_img_prevmon_mo_rtl_gif"] : window["E1RES_img_prevmon_mo_gif"]).append("'\"");
    sb.append(" onMouseOut=\"this.src='").append(isRTL ? window["E1RES_img_prevmon_rtl_gif"] : window["E1RES_img_prevmon_gif"]).append("'\" border=0 width=20 height=20>");

    var textContainer = document.getElementById("preButton" + this.namespace);
    textContainer.innerHTML = sb.toString();

    var tdTag = document.getElementById("tdPreButton" + this.namespace);
    tdTag.onclick = INYFEHandler.prototype.redirectPreviousError;
}

INYFEHandler.prototype.hideNextButton = function () {
    var textContainer = document.getElementById("nextButton" + this.namespace);
    textContainer.style.display = "none";
    var tdTag = document.getElementById("tdNextButton" + this.namespace);
    tdTag.onclick = null;
}

//Need to redirect otherwise "this" is not correct.
INYFEHandler.prototype.redirectNextError = function () {
    var namespace = JDEDTAFactory.getFirstNamespace();
    eval("inyfeHandler" + namespace + ".nextError()");
}

INYFEHandler.prototype.enableNextButton = function () {
    var sb = new PSStringBuffer();

    sb.append("<img src='").append(isRTL ? window["E1RES_img_nextmon_rtl_gif"] : window["E1RES_img_nextmon_gif"]).append('\' alt=\"').append(this.nextErrText).append("\"");
    sb.append(" onMouseOver=\"this.src='").append(isRTL ? window["E1RES_img_nextmon_mo_rtl_gif"] : window["E1RES_img_nextmon_mo_gif"]).append("'\"");
    sb.append(" onMouseOut=\"this.src='").append(isRTL ? window["E1RES_img_nextmon_rtl_gif"] : window["E1RES_img_nextmon_gif"]).append("'\" border=0 width=20 height=20>");

    var textContainer = document.getElementById("nextButton" + this.namespace);
    textContainer.innerHTML = sb.toString();

    var tdTag = document.getElementById("tdNextButton" + this.namespace);
    tdTag.onclick = INYFEHandler.prototype.redirectNextError;
}

INYFEHandler.prototype.showErrorWindow = function (errorElem, errorId) {
    var itemWidth = 250, itemHeight = 120;
    var errorDescription, descriptionText = null;
    var errorObj = this.errorList[this.findItemByControlId(errorId)];
    if (errorObj != null) {
        errorDescription = document.getElementById("DESCID_" + errorId);
        if (errorDescription != null) {
            descriptionText = errorDescription.innerHTML;
            // This is an attempt to get a better idea of how tall the popup needs to be,
            // and will allow better positioning (top calculation) of the error window.
            var estLines = parseInt(descriptionText.length / 40);
            if (estLines > 3) {
                itemHeight += (estLines - 3) * 10;  // add 10px for every 'estimated' line over 3
            }
        }

        if (errorObj.width && errorObj.height) {
            itemWidth = errorObj.width;
            itemHeight = errorObj.height;
        }
        var itemTop = 0;
        var itemLeft = 0;
        if (errorObj.top && errorObj.left) {
            itemTop = errorObj.top;
            itemLeft = errorObj.left;
        }
        else {
            itemTop = this.getErrorWindowTop(errorElem, itemHeight);
            itemLeft = this.getErrorWindowLeft(errorElem, itemWidth);
        }

        this.currentErrorObj = errorObj;
        this.focusElement = errorElem;
        this.errorWindowClicked = false;

        this.createErrorWindow("INYFEWindow" + this.namespace, itemLeft, itemTop, itemWidth,
                    itemHeight, errorObj.title, descriptionText, errorObj.isWarning);

        var errorWindow = document.getElementById("INYFEWindow" + this.namespace);
        var windowHeight = errorWindow.offsetHeight;

        itemTop = this.getErrorWindowTop(errorElem, windowHeight);

        errorWindow.style.top = itemTop + "px";
    }
}

INYFEHandler.prototype.getErrorWindowTop = function (errorElem, winHeight) {
    var winTop = getAbsoluteTopPos(errorElem, true);
    winTop = (winTop - winHeight - 10 > 0) ? (winTop - winHeight - 10) : (winTop + 25);
    return winTop;
}

INYFEHandler.prototype.getErrorWindowLeft = function (errorElem, winWidth) {
    var winLeft = getAbsoluteLeftPos(errorElem, true);
    if (document.documentElement.dir == "rtl") {
        winLeft -= (winWidth - errorElem.offsetWidth);
        if (winLeft < 0)
            winLeft = 0;
    } else {
        if (winLeft + winWidth > document.body.offsetWidth)
            winLeft += document.body.offsetWidth - (winLeft + winWidth);
        //Fix for Mozilla
        if (!document.all && winLeft < 0)
            winLeft = 0;

    }
    return winLeft;
}

INYFEHandler.prototype.getErrorWindowContent = function (title, descriptionText, isWarning) {
    var sb = new PSStringBuffer();

    sb.append("<table width=100% cellpadding=0 cellspacing=0>");
    sb.append("<tr><td>&nbsp;</td><td valign=top><div style=\"height:105;width:245;overflow: auto;font-size:11px;\"");
    sb.append(" id=inyfeDescContainer").append(this.namespace).append("><table cellpadding=0 cellspacing=0><tr></tr><tr><td>&nbsp;<br></td></tr><tr><td align=left id=inyfeDescText").append(this.namespace).append(">").append(descriptionText).append("</td></tr></table></div></td></tr>");
    sb.append("<tr><td colspan=2 bgcolor='#929292' height=1></td></tr>");
    sb.append("<tr valign=middle><td colspan=2 height=22><table border=0 cellpadding=0 cellspacing=0 height=100% width=100%");
    sb.append(" background='").append(window["E1RES_img_visualassistbackground_gif"]).append("'><tr>");

    sb.append("<td width=5>&nbsp;</td>");

    sb.append("<td><table border=0 cellpadding=0 cellspacing=0><tr>")
    sb.append("<td>");
    sb.append("<img border=0 src='").append(window["E1RES_img_up_arrow_top_gif"]).append("' alt=\"").append(this.summaryText).append("\"");
    sb.append(" onMouseOver=\"this.src='").append(window["E1RES_img_up_arrow_top_mo_gif"]).append("'\"");
    sb.append(" onMouseOut=\"this.src='").append(window["E1RES_img_up_arrow_top_gif"]).append("'\"");
    sb.append(" style=\"cursor:pointer\" onclick=\"javascript:inyfeHandler").append(this.namespace).append(".showSummary()\">");
    sb.append("<A href=\"javascript:onclick=inyfeHandler").append(this.namespace).append(".showSummary()\">");
    sb.append(this.summaryText).append("</A>");
    sb.append("</td>");
    sb.append("<td width=5>&nbsp;</td>");
    sb.append("</tr></table></td>")

    sb.append("<td align=").append(isRTL ? "left" : "right").append(">&nbsp;</td>");

    sb.append("<td align=").append(isRTL ? "left" : "right").append("><table border=0 cellpadding=0 cellspacing=0><tr>")
    sb.append("<td id=tdPreButton").append(this.namespace);
    sb.append(" style=\"cursor:pointer\" onclick=\"javascript:inyfeHandler").append(this.namespace).append(".previousError()\"");
    sb.append(" title=\"").append(this.prevErrText).append("\">");
    sb.append("<span id=preButton").append(this.namespace).append(">");
    sb.append("<img src='").append(isRTL ? window["E1RES_img_prevmon_rtl_gif"] : window["E1RES_img_prevmon_gif"]).append('\' alt=\"').append(this.prevErrText).append("\"");
    sb.append(" onMouseOver=\"this.src='").append(isRTL ? window["E1RES_img_prevmon_mo_rtl_gif"] : window["E1RES_img_prevmon_mo_gif"]).append("'\"");
    sb.append(" onMouseOut=\"this.src='").append(isRTL ? window["E1RES_img_prevmon_rtl_gif"] : window["E1RES_img_prevmon_gif"]).append("'\" border=0 width=20 height=20>");
    sb.append("</span>");
    sb.append("</td>");
    sb.append("<td>&nbsp;</td>");
    sb.append("<td id=tdNextButton").append(this.namespace);
    sb.append(" style=\"cursor:pointer\" onclick=\"javascript:inyfeHandler").append(this.namespace).append(".nextError()\"");
    sb.append(" title=\"").append(this.nextErrText).append("\">");
    sb.append("<span id=nextButton").append(this.namespace).append(">");
    sb.append("<img src='").append(isRTL ? window["E1RES_img_nextmon_rtl_gif"] : window["E1RES_img_nextmon_gif"]).append('\' alt=\"').append(this.nextErrText).append("\"");
    sb.append(" onMouseOver=\"this.src='").append(isRTL ? window["E1RES_img_nextmon_mo_rtl_gif"] : window["E1RES_img_nextmon_mo_gif"]).append("'\"");
    sb.append(" onMouseOut=\"this.src='").append(isRTL ? window["E1RES_img_nextmon_rtl_gif"] : window["E1RES_img_nextmon_gif"]).append("'\" border=0 width=20 height=20>");
    sb.append("</span>");
    sb.append("</td>");
    sb.append("<td width=5>&nbsp;</td>");
    sb.append("</tr></table></td>")
    sb.append("</tr></table></td>");
    sb.append("</tr>");
    sb.append("</table>");
    return sb.toString();
}

INYFEHandler.prototype.createErrorWindow = function (id, left, top, width, height, title, descriptionText, isWarning) {
    this.eventListener = new INYFEHandler.prototype.INYFEErrorEventListener(this.namespace);
    this.windowElem = new PopupWindow(id, true,
        { backgroundImage: isWarning ? window["E1RES_img_warn_header_bkgd_gif"] : window["E1RES_img_error_header_bkgd_gif"],
            titleIcon: "",
            titleText: title,
            titleTextColor: isWarning ? "#414141" : "#ffffff",
            hasClose: true,
            hasAbout: false
        }, // Title Info object
        this.eventListener);
    this.windowElem.setContent(this.getErrorWindowContent(title, descriptionText, isWarning));
    this.windowElem.display(left, top, width, height);
    //The window can be resized already, need to resized the content.
    if (this.currentErrorObj != null &&
        this.currentErrorObj.width != null &&
        this.currentErrorObj.height != null)
        this.eventListener.resizedTo(width, height);
}

INYFEHandler.prototype.INYFEErrorEventListener = function (namespace) {
    this.namespace = namespace;
    this.mouseDown = function () {
        var inyfeObj = window["inyfeHandler" + this.namespace];
        if (inyfeObj != undefined && inyfeObj.currentErrorObj != null) {
            inyfeObj.errorWindowClicked = true;
        }
    }

    this.onClose = function () {
        var inyfeObj = window["inyfeHandler" + this.namespace];
        if (inyfeObj != undefined)
            inyfeObj.closeErrorWindow(true);
    }

    this.movedTo = function (left, top) {
        var inyfeObj = window["inyfeHandler" + this.namespace];
        if (inyfeObj != undefined && inyfeObj.currentErrorObj != null) {
            var errorIndex = inyfeObj.findItem(inyfeObj.currentErrorObj.id);
            if (errorIndex != -1) {
                inyfeObj.errorList[errorIndex].top = parseInt(top);
                inyfeObj.errorList[errorIndex].left = parseInt(left);
            }
        }
    }
    this.resizedTo = function (width, height) {
        var descDiv = document.getElementById("inyfeDescContainer" + this.namespace);
        if (descDiv != null && height != null) {
            descDiv.style.height = parseInt(height) - 45 + "px";
            if (width != null)
                descDiv.style.width = parseInt(width) - 5 + "px";
        }
        var inyfeObj = window["inyfeHandler" + this.namespace];
        if (inyfeObj != undefined && inyfeObj.currentErrorObj != null) {
            var errorIndex = inyfeObj.findItem(inyfeObj.currentErrorObj.id);
            if (errorIndex != -1) {
                inyfeObj.errorList[errorIndex].width = parseInt(width);
                inyfeObj.errorList[errorIndex].height = parseInt(height);
            }
        }
    }
}

INYFEHandler.prototype.closeErrorWindowTimeout = function () {
    if (this.isErrorPopupEnabled()) {
        if (this.errorWindowClicked == false) {
            this.closeErrorWindow(false);
        }
        this.closeWindowTimeout = null;
    }
}

INYFEHandler.prototype.closeErrorWindow = function (userClosed) {
    if (userClosed && this.currentErrorObj != null) {
        var errorIndex = this.findItem(this.currentErrorObj.id);
        if (errorIndex != -1) {
            this.errorList[errorIndex].top = null;
            this.errorList[errorIndex].left = null;
            this.errorList[errorIndex].width = null;
            this.errorList[errorIndex].height = null;
        }
    }
    if (this.windowElem != null) {
        this.windowElem.hide();
        delete this.windowElem;
        this.windowElem = null;
    }
    this.currentErrorObj = null;
    this.focusElement = null;
    this.errorWindowClicked = false;
}

INYFEHandler.prototype.showSummary = function () {
    var summaryTop = document.getElementById("summaryTop" + this.namespace);
    this.closeErrorWindow(false);
    summaryTop.focus();
}

INYFEHandler.prototype.findPrevError = function (errorObj) {
    var index = this.findNextError(errorObj);
    var errorIndex = this.findItemByContextAndControlId(errorObj, this.errorList);

    if (errorIndex < 0)
        return index - 1;
    else
        return index - 2;
}

INYFEHandler.prototype.compareFCId = function (id1, id2) {
    // Input ID "fullyqualifiedcontrolid_seq"
    // This comparation assumes these two have the same fullyqualifiedcontrolid,
    // because these two have the same tab index
    var seq1 = id1.substring(id1.lastIndexOf("_") + 1, id1.length);
    var seq2 = id2.substring(id2.lastIndexOf("_") + 1, id2.length);

    if (parseInt(seq1) > parseInt(seq2))
        return 1;
    else if (parseInt(seq1) < parseInt(seq2))
        return -1;
    else
        return 0;
}

INYFEHandler.prototype.compareGCId = function (id1, id2) {
    // Input ID can be "fullyqualifiedgridid.row.col_seq" or "gridid.row.col"
    // This comparation assumes these two have the same fullyqualifiedgridid,
    // because these two have the same tab index
    var row1 = id1.substring(id1.indexOf(".") + 1, id1.lastIndexOf("."));
    var col1 = id1.substring(id1.lastIndexOf(".") + 1, id1.length);
    var row2 = id2.substring(id2.indexOf(".") + 1, id2.lastIndexOf("."));
    var col2 = id2.substring(id2.lastIndexOf(".") + 1, id2.length);
    var seq1 = "0";
    var seq2 = "0";

    if (col1.indexOf("_") > 0) {
        seq1 = col1.substring(col1.indexOf("_") + 1, col1.length);
        col1 = col1.substring(0, col1.indexOf("_"));
    }
    if (col2.indexOf("_") > 0) {
        seq2 = col2.substring(col2.indexOf("_") + 1, col2.length);
        col2 = col2.substring(0, col2.indexOf("_"));
    }
    if (parseInt(row1) > parseInt(row2)) {
        return 1;
    }
    else if (parseInt(row1) < parseInt(row2)) {
        return -1;
    }
    else {
        if (parseInt(col1) > parseInt(col2))
            return 1;
        else if (parseInt(col1) < parseInt(col2))
            return -1;
        else {
            if (parseInt(seq1) > parseInt(seq2))
                return 1;
            else if (parseInt(seq1) < parseInt(seq2))
                return -1;
            else
                return 0;
        }
    }
}

INYFEHandler.prototype.findNextError = function (errorObj) {
    // Have to go through the whole list to find the next error.
    // Because when clear/add an error for a control, other errors 
    // may be added or cleared on other control(s)
    var errorIndex = this.findItemByContextAndControlId(errorObj, this.errorList);
    // Find the real error first, in case the error id is changed by update.
    if (errorIndex != -1) {
        errorObj = this.errorList[errorIndex];
    }

    for (var i = 0; i < this.errorList.length; i++) {
        // Find next error that has the larger tabIndex
        if (this.errorList[i].tabIndex > errorObj.tabIndex) {
            return i;
        }
        else if (errorObj.tabIndex == this.errorList[i].tabIndex) {
            // For FC, multiple errors on same control
            if (errorObj.controlType != "VTGrid" && errorObj.controlType != "VTPCGrid") {
                // Find next error that has the larger fully qualified id
                for (var j = i; j < this.errorList.length && errorObj.tabIndex == this.errorList[j].tabIndex; j++) {
                    if (this.compareFCId(this.errorList[j].id, errorObj.id) > 0) {
                        break;
                    }
                }
                // Break, tab index different(find next tab index), or end of the list, return
                return j;
            }
            // For GC
            else {
                var currentId = errorObj.id.substring(0, errorObj.id.lastIndexOf("_"));
                var errorId = null;
                for (var j = i; j < this.errorList.length && errorObj.tabIndex == this.errorList[j].tabIndex; j++) {
                    errorId = this.errorList[j].id.substring(0, this.errorList[j].id.lastIndexOf("_"));

                    // Find next error that has the larger (gridid.row.col)                    
                    if (this.compareGCId(errorId, currentId) > 0) {
                        return j;
                    }
                    // For GC, multiple errors on same control        
                    else if (this.compareGCId(errorId, currentId) == 0) {
                        // Find next error that has the larger fully qualified id,
                        // if the error id (gridid.row.col) are the same
                        for (var k = j; k < this.errorList.length; k++) {
                            errorId = this.errorList[k].id.substring(0, this.errorList[k].id.lastIndexOf("_"));
                            if (errorId != currentId || this.compareGCId(this.errorList[k].id, errorObj.id) > 0) {
                                break;
                            }
                        }
                        return k;
                    }
                }
                return j;
            }
        }
    }
    return i;
}

INYFEHandler.prototype.refreshFocus = function (currentErrorObj, errorIndex) {
    // For multiple errors on the same control
    if (currentErrorObj.tabIndex == this.errorList[errorIndex].tabIndex) {
        if (document.all) {
            document.body.focus();
        }
        else {
            var activeElement = evt.target;
            if (activeElement && activeElement.blur) {
                activeElement.blur();
            }
        }
    }
}

INYFEHandler.prototype.cycleError = function (evt) {
    var errorIndex = -1;
    this.jumpToError = 0;
    if (this.errorList.length > 0) {
        if (this.currentErrorObj == null) {
            errorIndex = 0;
        }
        else {
            errorIndex = this.findNextError(this.currentErrorObj);
            if (errorIndex >= this.errorList.length) {
                errorIndex = 0;
            }
            this.refreshFocus(this.currentErrorObj, errorIndex);
        }
        this.currentFullyQualifiedId = this.errorList[errorIndex].id;
        this.locateAndSetFocus(this.errorList[errorIndex]);
    }
}

INYFEHandler.prototype.cycleErrorWithoutPopup = function (evt) {
    var errorIndex = -1;
    this.jumpToError = 0;
    if (this.errorList.length > 0) {
        // no error is on focus
        if (this.currentFullyQualifiedId == null) {
            errorIndex = 0;
        }
        else {
            errorIndex = this.findItemByControlId(this.currentFullyQualifiedId);
            if (errorIndex == -1 || errorIndex >= this.errorList.length) {
                //if focused error is corrected, the index may be -1, set to 0
                errorIndex = 0;
            }
            else {
                var currentErrorObj = this.errorList[errorIndex];
                errorIndex = this.findNextError(currentErrorObj);
                if (errorIndex == -1 || errorIndex >= this.errorList.length) {
                    //if can't find next error set the index to 0
                    errorIndex = 0;
                }
                this.refreshFocus(currentErrorObj, errorIndex);
            }
        }
        this.currentFullyQualifiedId = this.errorList[errorIndex].id;
        this.locateAndSetFocus(this.errorList[errorIndex]);
    }
}

INYFEHandler.prototype.nextError = function () {
    var errorIndex = -1;
    this.jumpToError = 1;
    if (this.currentErrorObj != null) {
        errorIndex = this.findNextError(this.currentErrorObj);
        if (errorIndex < this.errorList.length) {
            this.currentFullyQualifiedId = this.errorList[errorIndex].id;
            var focusElem = this.locateAndSetFocus(this.errorList[errorIndex]);
        }
        else {
            this.hideNextButton();
        }
    }
}

INYFEHandler.prototype.previousError = function () {
    var errorIndex = -1;
    this.jumpToError = 2;
    if (this.currentErrorObj != null) {
        errorIndex = this.findPrevError(this.currentErrorObj);
        if (errorIndex >= 0) {
            this.currentFullyQualifiedId = this.errorList[errorIndex].id;
            var focusElem = this.locateAndSetFocus(this.errorList[errorIndex]);
        }
        else {
            this.hidePreButton();
        }
    }
}

FCHandler.onChartClick = function (row, column, namespace, ctrlId) {
    JDEDTAFactory.getInstance(namespace).doAsynPostById("cht" + ctrlId + "." + row + "." + column, ctrlId, null);
}

FCHandler.onSelectionChangedCombo = function (ctrl, htmlPostFlag, namespace) {
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW) {
        this.markExited(ctrl);
        // don't need to check value change because this is fired onChange instead of onBlur
        if (htmlPostFlag)
            JDEDTAFactory.getInstance(namespace).postLater("");
    }
    else {
        var ctrlVal = ctrl.options[ctrl.selectedIndex].value;
        if (ChangeConf.isOkConf || ChangeConf.isCancelConf) {
            ChangeConf.isDirtyForm = true;       // dirtyForm is set to true since the combobox selection is changed
            ChangeConf.unmaskOkHyperControl();
        }
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM && !htmlPostFlag)
            JDEDTAFactory.getInstance(namespace).addToAsynQueue("comboChange", ctrl.name, ctrlVal, ctrl.title);
        else {
            FCHandler.onSectionChange(ctrl.id);
            JDEDTAFactory.getInstance(namespace).doAsynPostByIdWithDelayProcIndicator("comboChange", ctrl.name, ctrlVal, ctrl.title);
        }
    }
}

// if we are change section for a list view grid we need to start in list view grid in rendering indicator
FCHandler.onSectionChange = function (ctrlId) {
    if (!ctrlId)
        return;

    var pos = ctrlId.indexOf("sectionDropDown");
    if (pos != 0)
        return;

    var gridId = ctrlId.substring("sectionDropDown".length);
    var grid = GCMH.findGrid(gridId);
    if (grid.listViewMetadata)
        grid.vm.onStartRowAgregate();

}


FCHandler.onExitRTSelect = function (ctrl, namespace) {
    if (ctrl.selectedIndex == -1 || ctrl.options == null || ctrl.options.length <= ctrl.selectedIndex) {
        return;
    }
    var ctrlVal = ctrl.options[ctrl.selectedIndex].value;
    if (ctrlVal != this.prevValue) {
        if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW) {
            this.markExited(ctrl);
        } else {
            JDEDTAFactory.getInstance(namespace).addToAsynQueue("rtFilterChange", ctrl.name, ctrlVal);
        }
    }
}
// Display help through servlet in a new window
function showHelp(contextID, preContextID, wideContextID) {
    e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI("Help");
    e1UrlCreator.setParameter("ContextID", (contextID == null ? "" : contextID));
    e1UrlCreator.setParameter("PreContextID", (contextID == null ? "" : preContextID));
    e1UrlCreator.setParameter("WideContextID", (contextID == null ? "" : wideContextID));
    var stamp = new Date().getTime();
    var helpName = "OneWorldHelp" + stamp;
    e1UrlCreator.setParameter("id", stamp);
    window.open(e1UrlCreator.toString(), helpName, "toolbar=no,width=740,height=540,resizable,scrollbars=yes");
}

// -----------------------------------------------------------------------------
// Based on the import order, Only the folloing classes are supposed
// to use this verion of insertGenericHeader
//                      OutputRender
//                      VTFetchWarningModal
//                      VTCustomizedGrid
//                      VTExportGrid
//                      VTImportGrid
//                      VTMedioObjectRender
//                      UTBMAFComponentInstance
//                      VTPortletPreferencesRenderAdaptee
// -----------------------------------------------------------------------------
// 1. formText      | title
// 2. newTaskExpl   | 0/1
// 3. helpURL       | override context help
// 4. launcherDesc  |
// 5. launcherUrl   |
// 6. addlFormHeader... | Additional HTML added to end of header.
// -----------------------------------------------------------------------------
function insertGenericHeader(formText, newTaskExplorer, helpURL, launcherDesc, launcherUrl, helpTooltip, embeddedInCafeOneOrCompositePage, addlFormHeaderButton) {
    var helpText = (helpTooltip != null) ? helpTooltip : "Help";
    if (newTaskExplorer == 1) {
        insertGenericHeaderForNewTE(formText, helpURL, launcherDesc, launcherUrl,
            helpText, addlFormHeaderButton);
        return;
    }
    var dataStr = new Array(20), iStr = 0;
    if (!embeddedInCafeOneOrCompositePage) {
        dataStr[iStr++] = insertLogoTopLeft(launcherDesc, launcherUrl);

        var newHelpURL = helpURL;

        var jdehelpImgName = window["E1RES_img_jdehelp_ena_png"];
        var jdehelpmoImgName = window["E1RES_img_jdehelp_hov_png"];
        if (document.documentElement.dir == "rtl") {
            jdehelpImgName = window["E1RES_img_jdehelp_ena_rtl_png"];
            jdehelpmoImgName = window["E1RES_img_jdehelp_hov_rtl_png"];
        }

        if (helpURL != null && helpURL != "") {
            dataStr[iStr++] = "<a TABINDEX=-1 href=\"";
            dataStr[iStr++] = newHelpURL;
            dataStr[iStr++] = "\" ";
            dataStr[iStr++] = " ID=GENERICHEADER";
            dataStr[iStr++] = "><img border=0 align=absmiddle src=\"" + jdehelpImgName + "\" onMouseOver=\"this.src='" + jdehelpmoImgName + "';\" onMouseOut=\"this.src='" + jdehelpImgName + "';\" title=\"" + helpText + "\" alt=\"" + helpText + "\"></a>&nbsp;";
        }
        dataStr[iStr++] = "</tr></table></td>";
        dataStr[iStr++] = "</tr></table>";

        dataStr[iStr++] = "<table><tr id=formTitle><td width=10>&nbsp;</td><td class=MainHeading role=heading height=23px>";
        dataStr[iStr++] = "<nobr><span id=jdeFormTitle title=\"" + formText + "\" style=\"overflow-x:hidden;text-overflow:ellipsis;\">" + formText + "&nbsp;</span></nobr>";
    }
    dataStr[iStr++] = "</td></tr></Table>";

    document.write(dataStr.join(""));

    if (document.body != null && document.body.scroll != null)
        document.body.scroll = "yes";
}

function insertGenericHeaderForNewTE(formText, helpURL, launcherDesc, launcherUrl, helpText,
    addlFormHeaderButton) {
    var dataStr = new Array(20), iStr = 0;

    dataStr[iStr++] = "<TABLE cellspacing=0 width=100%>";
    dataStr[iStr++] = "<TR id=formTitle>";
    dataStr[iStr++] = "<TD class=MainHeading role=heading width=100% height=23px>";
    dataStr[iStr++] = "<nobr><span id=jdeFormTitle title=\"" + formText + "\" style=\"overflow-x:hidden;text-overflow:ellipsis;\">" + formText + "&nbsp;</span></nobr>";
    dataStr[iStr++] = "</TD>";

    dataStr[iStr++] = "<TD class=toptext nowrap>";
    dataStr[iStr++] = insertLauncherUrl(launcherDesc, launcherUrl);
    dataStr[iStr++] = "</TD>";

    dataStr[iStr++] = "<TD class=buttons nowrap>";
    var newHelpURL = helpURL;

    var jdehelpImgName = window["E1RES_img_jdehelp_ena_png"];
    var jdehelpmoImgName = window["E1RES_img_jdehelp_hov_png"];
    if (document.documentElement.dir == "rtl") {
        jdehelpImgName = window["E1RES_img_jdehelp_ena_rtl_png"];
        jdehelpmoImgName = window["E1RES_img_jdehelp_hov_rtl_png"];
    }
    if (helpURL != null && helpURL != "") {
        dataStr[iStr++] = "<a TABINDEX=-1 href=\"";
        dataStr[iStr++] = newHelpURL;
        dataStr[iStr++] = "\" ";
        dataStr[iStr++] = " ID=GENERICHEADER";
        dataStr[iStr++] = "><img border=0 align=absmiddle src=\"" + jdehelpImgName + "\" onMouseOver=\"this.src='" + jdehelpmoImgName + "';\" onMouseOut=\"this.src='" + jdehelpImgName + "';\" title=\"" + helpText + "\" alt=\"" + helpText + "\"></a>&nbsp;";
    }
    if (addlFormHeaderButton && addlFormHeaderButton != null && addlFormHeaderButton != "") {
        dataStr[iStr++] = addlFormHeaderButton;
    }

    if (addlFormHeaderButton) {
        // The extra spaces are unncessary in and problematic for the Native Container.
        dataStr[iStr++] = "</TD></TR></TABLE>";
    }
    else {
        dataStr[iStr++] = "&nbsp;&nbsp;</TD></TR></TABLE>";
    }

    document.write(dataStr.join(""));
    if (document.body != null && document.body.scroll != null)
        document.body.scroll = "yes";
}

// -----------------------------------------------------------------------------
// Only the following classes are supposed to use this version of insertFormHeader
//                      | VTFormRenderAdaptee
// -----------------------------------------------------------------
// 1.  formFullId       | full qualified form id include namespace
// 2.  contextID        | <Form ID>
// 3.  preContextID     | <The parent Form ID if the current Form started by FI>
// 4.  wideContextID    | <Application ID>
// 5.  formText         | title
// 6.  errorText        |
// 7.  bErrorExist      |
// 8.  helpText         | helptip for context help button = "help"
// 9.  aboutText        | helptip for about help button ="about"
// 10. tooltip          | helptip for item help button ="item help"
// 11. nErrCount        |
// 12. nWarnCount       |
// 13. idAutoPilot      |
// 14. namespace        |
// 15. editText         | helptip for portlet edit button = "edit"
// 16. aqEnabledText    | advanced query enabled
// 17. formClearText    | clear form hover text
// 18. bWLEnabledForm   | Watchlist Enabled Form
// 19 .embeddedInCafeOneOrCompositePage
// 20. portletEdit      | Control ID for portlet edit button
// 21. NewTaskExp       | 0/1
// 22. launcherDesc     |
// 23. launcherUrl      |
// 24. addlFormHeader...| WebGraphicButton as an additional add-on to form header
// 25. aqEnabledEditor  | advanced query enabled Editor  0/1
// 26. CAFE1EnabledEditor  | CafeOne enabled Editor  0/1
// 27. objE1UDO         |  Object for UDO variables  
// -----------------------------------------------------------------------------
function insertFormHeader(formFullId, contextID, preContextID, wideContextID, formText,
                          errorText, bErrorExist, helpText, aboutText, tooltip,
                          nErrCount, nWarnCount, idAutoPilot, namespace,
                          editText, aqEnabledText, formClearText, bWLEnabledForm, embeddedInCafeOneOrCompositePage, portletEdit, newTaskExplorer, launcherDesc, launcherUrl, aqEnabledEditor, bCafeOneEnabled, addlFormHeaderButton, objE1UDO) {
    if (newTaskExplorer == 1) {
        insertFormHeaderForNewTE(formFullId, contextID, preContextID, wideContextID, formText,
                errorText, bErrorExist, helpText, aboutText, tooltip, nErrCount, nWarnCount,
                idAutoPilot, launcherDesc, launcherUrl, namespace, editText, aqEnabledText, formClearText,
            bWLEnabledForm, portletEdit, aqEnabledEditor, bCafeOneEnabled, addlFormHeaderButton, objE1UDO);
        return;
    }
    var dataStr = new Array(20), iStr = 0;

    if (!embeddedInCafeOneOrCompositePage) {
        dataStr[iStr++] = insertLogoTopLeft(launcherDesc, launcherUrl);

        if (addlFormHeaderButton) {
            // The extra spaces are unncessary in and problematic for the Native Container.
            dataStr[iStr++] = "</td></tr></table>";
        }
        else {
            dataStr[iStr++] = "&nbsp;&nbsp;</td></tr></table>";
        }
        dataStr[iStr++] = "<td></tr></table>";

        dataStr[iStr++] = "<table><tr id=formTitle" + formFullId + "><td width=10>&nbsp;</td><td id=formHeading role=heading class=MainHeading>";
        dataStr[iStr++] = "<nobr><span id=jdeFormTitle" + formFullId + " title=\"" + formText + "\" style=\"overflow-x:hidden;text-overflow:ellipsis\">" + formText + "&nbsp;</span></nobr>";

        dataStr[iStr++] = "</TD>";
        dataStr[iStr++] = "<TD class='buttons ";
        dataStr[iStr++] = "e1HelpLayoutCell";
        dataStr[iStr++] = "' nowrap>";
        dataStr[iStr++] = insertFormHeaderButtons(contextID, preContextID, wideContextID,
                                              errorText, bErrorExist, helpText,
                                              aboutText, tooltip, nErrCount, nWarnCount,
                                              idAutoPilot, namespace, editText, aqEnabledText, formClearText, bWLEnabledForm, portletEdit,
                                              aqEnabledEditor, bCafeOneEnabled, addlFormHeaderButton, objE1UDO);
    }

    dataStr[iStr++] = "</td></tr></Table>";

    document.write(dataStr.join(""));

    if (document.body != null && document.body.scroll != null)
        document.body.scroll = "yes";
}

function insertFormHeaderForNewTE(formFullId, contextID, preContextID, wideContextID, formText,
                  errorText, bErrorExist, helpText, aboutText, tooltip, nErrCount,
                  nWarnCount, idAutoPilot, launcherDesc, launcherUrl, namespace,
                  editText, aqEnabledText, formClearText, bWLEnabledForm, portletEdit, aqEnabledEditor, bCafeOneEnabled, addlFormHeaderButton, objE1UDO) {
    var dataStr = new Array(20), iStr = 0;
    dataStr[iStr++] = "<TABLE id=formTitleTable cellspacing=0 width=100%>";
    dataStr[iStr++] = "<TR id=formTitle" + formFullId + ">";
    dataStr[iStr++] = "<TD class=MainHeading  id=formHeading role=heading>";
    dataStr[iStr++] = "<span id=jdeFormTitle" + formFullId + " title=\"" + formText + "\" style=\"overflow-x:hidden;text-overflow:ellipsis;\">&nbsp;" + formText + "</span>";
    dataStr[iStr++] = "</TD>";

    dataStr[iStr++] = "<td nowrap class=toptext>";
    dataStr[iStr++] = insertLauncherUrl(launcherDesc, launcherUrl);
    dataStr[iStr++] = "</TD>";
    dataStr[iStr++] = "<TD class='buttons ";
    dataStr[iStr++] = "e1HelpLayoutCell";
    dataStr[iStr++] = "' nowrap>";
    dataStr[iStr++] = insertFormHeaderButtons(contextID, preContextID, wideContextID,
            errorText, bErrorExist, helpText, aboutText, tooltip, nErrCount, nWarnCount,
        idAutoPilot, namespace, editText, aqEnabledText, formClearText, bWLEnabledForm, portletEdit, aqEnabledEditor, bCafeOneEnabled, addlFormHeaderButton, objE1UDO);

    // The extra spaces are unncessary in and problematic for the Native Container.
    dataStr[iStr++] = "</TD></TR></TABLE>";

    document.write(dataStr.join(""));

    if (document.body != null && document.body.scroll != null)
        document.body.scroll = "yes";
}

function insertLauncherUrl(launcherDesc, launcherUrl) {
    var dataStr = "";
    if (launcherDesc != null && launcherDesc != "") {
        dataStr = "&nbsp;<a href=\"\" onClick=\"" + launcherUrl + "\">&nbsp;" + launcherDesc + "</a>&nbsp;&nbsp;|&nbsp;";
    }
    return dataStr;
}

function insertFormHeaderButtons(contextID, preContextID, wideContextID, errorText, bErrorExist, helpText,
    aboutText, tooltip, nErrCount, nWarnCount, idAutoPilot, namespace, editText, aqEnabledText, formClearText, bWLEnabledForm, portletEdit,
    aqEnabledEditor, bCafeOneEnabled, addlFormHeaderButton, objE1UDO) {
    var dataStr = new Array(50), iStr = 0;  /* "48" is the current max array size if all code paths are hit. */
    var defaultImgAlignment = "align=absmiddle";
    if (addlFormHeaderButton) {
        // With consolidated help and a new close button to align with, bypass the existing image alignment.
        // Alignment is association with text requires a slight drop.
        defaultImgAlignment = "style='vertical-align:-4px;'";
    }

    /*
    * The native container will present the help and about buttons as options in a dropdown/popup.
    * That presentation will be contained within a new table with all spans added to their own column.
    * Doing so prevents wrapping of content.
    */
    if (addlFormHeaderButton) {
        // Start embedding the "buttons" content within a table.
        // Column 1 is CafeOne.
        dataStr[iStr++] = "<table ";
        if (!isNativeContainer)
            dataStr[iStr++] = "class=e1HelpLayoutTable"
        dataStr[iStr++] = "><tr><td class='helpOptionsSiblingColumn' nowrap>";
    }

    //RTL ,iOS,Andriod Design Mode is not supported 
    var renderMode = $('#E1PaneForm').children('input[name=jdemafjasLauncher]').val();
    var restrictedModes = ["E1PAGES_PORTLETS", "EMBEDDED_E1_LAUNCHER", "EMBEDDED_E1_PAGE_LAUNCHER", "AHQ_LAUNCHER"];
    var tablets = ["IPAD", "LINUX ARM"]; // To check whether 'Request Desktop Site' is enabled.
    var renderedPlatform = navigator.platform.toUpperCase();
    var isTabletMode = false;
    for (var i = 0; i < tablets.length; i++) {
        if (renderedPlatform.indexOf(tablets[i]) > -1) {
            isTabletMode = true;
        }
    }

    var bFeatureSecurityPF = objE1UDO.E1UDO.PF.bFeatureSecurityPF;
    if (bFeatureSecurityPF && !isPowerForm) {
        if (!((renderMode.toUpperCase() === "AHQ_LAUNCHER") || (renderMode.toUpperCase() === "EMBEDDED_E1_LAUNCHER"))) {
            dataStr[iStr++] = "<span><span class='selectLabel'";
            // The title bar is not wide enough to support all possible content in iOS.  A temporary solution is hiding dropdown labels.
            if (this.isIOS) {
                dataStr[iStr++] = " name='compressedSelectLabel'";
            }
            dataStr[iStr++] = ">Personal Form: </span><select title='Select a Personal Form' TABINDEX=0 id='PersonalizeViewList' class='RCUXComboBox'><option value='none'>(No Personalization)</option></select></span>";
        }
    }
    if (bCafeOneEnabled) {
        // CafeOne Layout Manager
        dataStr[iStr++] = "<span id='";
        dataStr[iStr++] = RIUTIL.ID_LAYOUT_SELECTION_SPAN;
        dataStr[iStr++] = "' style='display:none;padding-right:10px;'><span class='selectLabel'";
        // The title bar is not wide enough to support all possible content in iOS.  A temporary solution is hiding dropdown labels.
        if (this.isIOS) {
            dataStr[iStr++] = " name='compressedSelectLabel'";
        }
        dataStr[iStr++] = ">Layout: </span><select id='";
        dataStr[iStr++] = RIUTIL.ID_LAYOUT_SELECTION_FORM;
        dataStr[iStr++] = "' class='RCUXComboBox' TABINDEX=0 ></select></span>";
    }

    if (addlFormHeaderButton) {
        // End the Cafe One column.
        dataStr[iStr++] = "</td>";
    }

    if (aqEnabledText) // advanced query is enabled 
    {
        if (addlFormHeaderButton) {
            // Start the AQ column.
            dataStr[iStr++] = "<td class='helpOptionsSiblingColumn";
            if (FCHandler.isTouchEnabled) { dataStr[iStr++] = " touchEnabled"; }
            dataStr[iStr++] = "' nowrap>";
        }

        dataStr[iStr++] = "<span><span";
        dataStr[iStr++] = " class='selectLabel'";
        // The title bar is not wide enough to support all possible content in iOS.  A temporary solution is hiding dropdown labels.
        if (this.isIOS) {
            dataStr[iStr++] = " name='compressedSelectLabel'";
        }
        dataStr[iStr++] = ">Query: </span><select title='Select a Query' TABINDEX=0 id='AQFormQueryList' class='RCUXComboBox'><option>All Records</option></select>";
        if (aqEnabledEditor) {
            dataStr[iStr++] = "&nbsp;<a role='button' tabindex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.click()};' aria-labelledby='AQDesignIcon' onClick='AQ.goToggleAdvancedQuery()'><img id='AQDesignIcon' border=0 " + defaultImgAlignment + " src='" + window["E1RES_img_jdequery_ena_png"] + "' alt='"
            dataStr[iStr++] = aqEnabledText;
            dataStr[iStr++] = "' title='";
            dataStr[iStr++] = aqEnabledText;
            dataStr[iStr++] = "'></a>";
        }
        else {
            dataStr[iStr++] = "</a>";
        }

        // form clear Icon

        dataStr[iStr++] = "<a role='button' tabindex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.click()};' onClick='AQ.goClearForm()'><img id='AQClearIcon' border=0 " + defaultImgAlignment + " src='" + window["E1RES_img_jdeclear_ena_png"] + "' alt='"
        dataStr[iStr++] = formClearText;
        dataStr[iStr++] = "' title='";
        dataStr[iStr++] = formClearText;
        dataStr[iStr++] = "'></a>";

        if (bWLEnabledForm) {

            dataStr[iStr++] = "<a tabindex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.firstChild.click()};'";
            dataStr[iStr++] = " role='button' aria-labelledby='WLDesignIcon' onClick='WL.goToggleWatchlist()'><img id='WLDesignIcon' border=0 " + defaultImgAlignment + " src='" + window["E1RES_img_jdewatchlist_ena_png"] + "' alt='"
            dataStr[iStr++] = "Watchlist Design"; //translation handled in JSWL.js
            dataStr[iStr++] = "' title='";
            dataStr[iStr++] = "Watchlist Design"; //translation handled in JSWL.js
            dataStr[iStr++] = "' ></a>";

            // The spacer is unnecessary for the Native Containers consolidated help options.
            if (!addlFormHeaderButton) {
                dataStr[iStr++] = "<img border=0 " + defaultImgAlignment + " src='" + window["E1RES_img_divider_png"] + "'>";
            }

            dataStr[iStr++] = "</span>";
        }

        if (addlFormHeaderButton) {
            // End the AQ column.
            dataStr[iStr++] = "</td>";
        }
    }

    var bCanCreatePF = objE1UDO.E1UDO.PF.bCanCreatePF;
    if (bCanCreatePF && !isPowerForm) {
        if (!(this.isRTL || this.isIOS || this.isAndroid || $.inArray(renderMode.toUpperCase(), restrictedModes) > -1 || this.isWSRPContainer() || isTabletMode)) {
            dataStr[iStr++] = "<td class='helpOptionsSiblingColumn'>";
            dataStr[iStr++] = "&nbsp;<a id='PersonalizeDesignBtn' href='#openPersonalizeFormDesign' role='button' style='vertical-align:4px;' tabindex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.firstChild.click()};' ><img id='PersonalizeForm' border=0 " + defaultImgAlignment + " src='" + this.window.E1RES_img_PF_personalize_df_png + "' alt='"
            dataStr[iStr++] = "Personal Form";
            dataStr[iStr++] = "' onMouseOver=\"this.src='" + this.window.E1RES_img_PF_personalize_mo_png + "' \"";
            dataStr[iStr++] = " onMouseOut=\"this.src='" + this.window.E1RES_img_PF_personalize_df_png + "' \"";
            dataStr[iStr++] = " title='";
            dataStr[iStr++] = "Personal Form";
            dataStr[iStr++] = "'></a>";
            dataStr[iStr++] = "</td>";
        }
    }

    if (addlFormHeaderButton) {
        // Start the column for jdeDTAStatusBtn, errors, and warnings.
        dataStr[iStr++] = "<td class='helpOptionsSiblingColumn' nowrap>";
    }

    dataStr[iStr++] = "<span id=jdeDTAStatusBtn" + namespace + "><!--4JTidy--></span>";

    // begin errors and warnings
    dataStr[iStr++] = "<span class=jdeErrorBtn id=jdeErrorBtn" + namespace + ">";
    FCHandler.errorText = errorText;
    if (bErrorExist && nErrCount != null && nWarnCount != null) {
        dataStr[iStr++] = FCHandler.insertErrorBtn(nErrCount, nWarnCount, namespace);
    }

    dataStr[iStr++] = "</span>";

    var jdeeditImgName = window["E1RES_img_jdeedit_ena_png"];
    var jdeeditmoImgName = window["E1RES_img_jdeedit_hov_png"];
    if (document.documentElement.dir == "rtl") {  /* change these if we create different images for RTL */
        jdeeditImgName = window["E1RES_img_jdeedit_ena_png"];
        jdeeditmoImgName = window["E1RES_img_jdeedit_hov_png"];
    }

    if (portletEdit != "")
        dataStr[iStr++] = "&nbsp;<a href=javascript:oc(\"" + namespace + "\",\"" + namespace + portletEdit + "\") TABINDEX=-1><img border=0 " + defaultImgAlignment + " src=\"" + jdeeditImgName + "\" onMouseOver=\"this.src='" + jdeeditmoImgName + "';\" onMouseOut=\"this.src='" + jdeeditImgName + "';\" alt=\"" + editText + "\" ; title = \"" + editText + "\"></a>";


    /*
    * End the column for jdeDTAStatusBtn, errors, and warnings. Begin
    * the consolidated help options column.
    */
    dataStr[iStr++] = "</td><td ";
    dataStr[iStr++] = "class=e1HelpLayoutCell";
    dataStr[iStr++] = " nowrap>";
    if (userAccessibility) // render help icons in old fashion for accessbility user since they do not care how the options are displayed but what option are avaliable
    {
        dataStr[iStr++] = "&nbsp;<a TABINDEX=0 role=button onkeydown='if(event.keyCode==32){this.click()};' href=javascript:" + namespace + "about(\"FormInfo\")><img id='MainPanelAbout' border=0 align=absmiddle src=\"" + window["E1RES_img_jdeabout_ena_png"] + "\" onMouseOver=\"this.src='" + window["E1RES_img_jdeabout_hov_png"] + "';\" onMouseOut=\"this.src='" + window["E1RES_img_jdeabout_ena_png"] + "';\" alt=\"" + aboutText + "\" ; title = \"" + aboutText + "\"></a>";
        dataStr[iStr++] = "&nbsp;<a TABINDEX=0 role=button onkeydown='if(event.keyCode==32){this.click()};' href=javascript:showHelp('" + contextID + "','" + preContextID + "','" + wideContextID + "') ";
        if (idAutoPilot != null)
            dataStr[iStr++] = " ID=" + idAutoPilot;

        var jdehelpImgName = window["E1RES_img_jdehelp_ena_png"];
        var jdehelpmoImgName = window["E1RES_img_jdehelp_hov_png"];
        if (document.documentElement.dir == "rtl") {
            jdehelpImgName = window["E1RES_img_jdehelp_ena_rtl_png"];
            jdehelpmoImgName = window["E1RES_img_jdehelp_hov_rtl_png"];
        }

        dataStr[iStr++] = "><img border=0 align=absmiddle src=\"" + jdehelpImgName + "\" onMouseOver=\"this.src='" + jdehelpmoImgName + "';\" onMouseOut=\"this.src='" + jdehelpImgName + "';\" alt=\"" + helpText + "\" title=\"" + helpText + "\"></a>";

        var helptip4ImgName = window["E1RES_img_jdeitemhelp_ena_png"];
        var helptip0ImgName = window["E1RES_img_jdeitemhelp_hov_png"];
        if (document.documentElement.dir == "rtl") {
            helptip4ImgName = window["E1RES_img_jdeitemhelp_ena_rtl_png"];
            helptip0ImgName = window["E1RES_img_jdeitemhelp_hov_rtl_png"];
        }

        dataStr[iStr++] = "&nbsp;<img TABINDEX=0 role=button onkeydown='if(event.keyCode==32 || event.keyCode==13){this.click()};' src='" + helptip4ImgName + "' onMouseOver=\"this.src='" + helptip0ImgName + "';\" onMouseOut=\"this.src='" + helptip4ImgName + "';\" align=absmiddle onmouseover=updateHelpCursor('" + namespace + "', this) NAME='helpIcon' alt=\"" + tooltip;
        dataStr[iStr++] = "\" title=\"" + tooltip;
        dataStr[iStr++] = "\" border=0>";
    }
    else // for web client
    {
        dataStr[iStr++] = buildHelpOptionsTable(null, namespace, aboutText, contextID, preContextID, wideContextID, helpText, tooltip, addlFormHeaderButton);
    }
    // End the consolidated help options column.
    dataStr[iStr++] = "</td>";

    // Form header button
    if (addlFormHeaderButton && addlFormHeaderButton != null && addlFormHeaderButton != "") {
        dataStr[iStr++] = addlFormHeaderButton;
    }

    if (addlFormHeaderButton) {
        // Tie up the native containers consolidate help options presentation.
        dataStr[iStr++] = "</tr></table>";
    }

    return dataStr.join("");
}

function buildHelpOptionsTable(uniqueId, namespace, aboutText, contextID, preContextID, wideContextID, helpText, toolTip, addlFormHeaderButton) {
    //constant defines the layer(z-index) for the UIBlocking DIV
    var UIBLOCKING_LAYER_ZINDEX = 100000;
    /*
    * Redefined the preceding var as UIBlocking.js exists in the UI layer below NativeContainer.js.
    * The SSModal forms z-index is UIBLOCKING_LAYER_ZINDEX + the depth.  We'll add arbitrarily on top of that.
    */
    var clickCaptureDivZIndex = 999;
    var submenuZIndex = clickCaptureDivZIndex + 1;
    var dataStr = new Array(12), iStr = 0;
    var submenuClassName = "e1helpOptionsSubmenu";
    var bubbleDivClassName = "e1helpOptionsSubmenuBubbleDiv e1helpBubblePosition";
    var helpOptionsIcon = window["E1RES_img_jdehelp_ena_png"];
    var helpOptionsIconHover = window["E1RES_img_jdehelp_hov_png"];
    var rtlAddendum = "_rtl";
    var mouseoverColor = "#E5E5E5";
    var mouseoutColor = "transparent";

    if (document.documentElement.dir == "rtl") {
        helpOptionsIcon = window["E1RES_img_jdehelp_ena_rtl_png"];
        helpOptionsIconHover = window["E1RES_img_jdehelp_hov_rtl_png"];
    }

    // Consolidated help options will appear in a dropdown-like table with each option a button-like column there.
    if ((aboutText && aboutText != "") || // Need at least one button to throw into the table.
        (helpText && helpText != "") ||
        (toolTip && toolTip != "")) {
        if (!uniqueId || isNaN(uniqueId)) {
            uniqueId = "";
        }
        else {
            clickCaptureDivZIndex = clickCaptureDivZIndex + (UIBLOCKING_LAYER_ZINDEX * uniqueId);
            submenuZIndex = clickCaptureDivZIndex + 1;
        }

        /*In modal forms, the arrow is 4 px off horizontally. Comprensating for it here by overriding value in css.*/
        var bubbleStyleString = "", submenuStyleString = "";
        if (uniqueId > 0) {
            bubbleStyleString = "style=\"top:41px;";
            submenuStyleString = "top:41px;";
            if (document.documentElement.dir == "rtl") {
                bubbleStyleString += "left:24px;";
            }
            else {
                bubbleStyleString += "right:24px;";
            }
            bubbleStyleString += "\"";
        }
        var eventHandlerForToggleHelp = "onclick=toggleHelpOptionsSubmenu('" + uniqueId + "','" + namespace + "')";
        var toggleImageId = "helpOptionImage" + namespace + uniqueId;

        dataStr[iStr++] = "<table cellspacing='0' cellpadding='0' class=\"e1helpOptionsMenu e1HelpLayoutTable\"><tr><td class=\"e1helpOptionsGateway e1HelpLayoutCell\" " + eventHandlerForToggleHelp + " nowrap>";
        dataStr[iStr++] = "<div class='imageDiv16'>";
        dataStr[iStr++] = "<img tabindex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.click()};'  id='" + toggleImageId + "' class=\"e1helpOptionsImage\" src=\"" + helpOptionsIcon + "\" title=\"" + helpText + "\" alt=\"" + helpText + "\" onmouseover=\"this.src='" + helpOptionsIconHover + "'\" onmouseout=\"this.src='" + helpOptionsIcon + "'\">";
        dataStr[iStr++] = "</div>";
        dataStr[iStr++] = "</td></tr><tr><td>";
        dataStr[iStr++] = "<div style=\"z-index:" + clickCaptureDivZIndex + ";\" class=\"e1helpOptionsClickCaptureDiv\" id=\"helpOptionsClickCaptureDivId" + namespace + uniqueId + "\" " + eventHandlerForToggleHelp + ">";
        dataStr[iStr++] = "<div class=\"e1helpOptionsSubmenuDiv\">";
        if (!isIE8OrEarlier)
            dataStr[iStr++] = "<div id='helpBubbleTip" + namespace + uniqueId + "' " + bubbleStyleString + " class=\"" + bubbleDivClassName + "\"></div>";
        dataStr[iStr++] = "<table style=\"z-index:" + submenuZIndex + ";" + submenuStyleString + "\" class=\"" + submenuClassName + " AccessibilityContainer\" id='helpOptionsSubmenuId" + namespace + uniqueId + "' " + eventHandlerForToggleHelp + " cellspacing=\"10\">";

        if (aboutText && aboutText != "") {
            dataStr[iStr++] = "<tr class=\"e1helpOptionsRow\"><td class=\"e1helpOptionsItem\" onclick=" + namespace + "about(\"FormInfo\")><a tabindex=0 beginOfSubControl=true onkeydown='keydownHelpMenu(event, \"" + toggleImageId + "\")'>" + aboutText + "</a></td></tr>";
        }

        if (helpText && helpText != "") {
            dataStr[iStr++] = "<tr class=\"e1helpOptionsRow\"><td class=\"e1helpOptionsItem\" onclick=showHelp('" + contextID + "','" + preContextID + "','" + wideContextID + "');><a tabindex=0 onkeydown='keydownHelpMenu(event, \"" + toggleImageId + "\")'>" + helpText + "</a></td></tr>";
        }

        if (toolTip) // do not support keyboard for this link, because the user can not use mouse to select contorl later. 
        {
            // With the Item Help, we must trigger a toggle of the help options submenu to hide the transparent div.  Otherwise we're getting item help on it.
            dataStr[iStr++] = "<tr class=\"e1helpOptionsRow\"><td class=\"e1helpOptionsItem\" id=\"helpOptionsItemHelpId" + uniqueId + "\" " + eventHandlerForToggleHelp + "><a tabindex=0 endOfSubControl=true onkeydown='keydownHelpMenu(event, \"" + toggleImageId + "\")'>" + toolTip + "</a></td></tr>";
        }

        dataStr[iStr++] = "</table></div></div></td></tr></table>";
    }

    return dataStr.join("");
}

function keydownHelpMenu(event, toggleImageId) {
    var dismissPopup = false;
    var eventObject = document.activeElement;
    switch (event.keyCode) {
        case 9: // tab
            if (event.shiftKey) {
                if (eventObject.getAttribute("beginOfSubControl")) {
                    dismissPopup = true;
                }
            }
            else {
                if (eventObject.getAttribute("endOfSubControl")) {
                    dismissPopup = true;
                }
            }
            break;

        case 32: // Space
        case 13: // Enter
            eventObject.parentNode.click();
            dismissPopup = true;
            break;

        case 27: // ESC
            dismissPopup = true;
            break;
    }

    if (dismissPopup) {
        document.getElementById(toggleImageId).click();
    }
}

function insertLogoTopLeft(launcherDesc, launcherUrl) {
    var dataStr = new Array(50), iStr = 0;
    var oracleLogoImgName = window["E1RES_share_images_alta_oracle_logo_sm_png"];

    dataStr[iStr++] = "<table cellpadding=0 cellspacing=0 width=100% id=topimagecell class='AFBrandingBar'";
    dataStr[iStr++] = "><tr class='topnav'>";
    dataStr[iStr++] = "<td><img id='oracleImage' class='topLeftTable' src='" + oracleLogoImgName + "' ALT=ORACLE&reg border=0></td>";
    dataStr[iStr++] = "<td bottomborder width=100%>";
    dataStr[iStr++] = "<span id='appName' class='appname topLeftTable'>JD Edwards</span></td>";
    if (document.documentElement.dir == "rtl")
        dataStr[iStr++] = "<td align=left>";
    else
        dataStr[iStr++] = "<td align=right>";
    dataStr[iStr++] = "<table><tr><td nowrap class=toptext>";

    if (launcherDesc != null && launcherDesc != "") {
        dataStr[iStr++] = "&nbsp;&nbsp;<a href=\"\" onClick=\"" + launcherUrl + "\">&nbsp;" + launcherDesc + "</a>&nbsp;&nbsp;|&nbsp;";
    }
    return dataStr.join("");
}

/////////////////////////////////////////////////
// SCRIPTS FOR MENUBAR
/////////////////////////////////////////////////
function toolsExitSelected(name, namespace, refreshLayout) {
    try {
        if (refreshLayout && window.RIUTIL && RIUTIL.getRootRIUTIL() != null)
            RIUTIL.getRootRIUTIL().refreshLayout();
    } catch (err)
    { }

    if ("refresh" == name && (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_MEDIUM || JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_HIGH)) {
        var replaceUrl = window.location.href;
        if (replaceUrl.indexOf("&RID=") == -1) {
            replaceUrl = replaceUrl + "&RID=" + JDEDTAFactory.getInstance(namespace).RID;
        }
        window.location.replace(replaceUrl);
    }
    else
        JDEDTAFactory.getInstance(namespace).post("form:" + name);
}
///////////////////////////////////////////
// Scripts for ItemHelp
///////////////////////////////////////////

function insertSmallHeader(dataItem, szDict, closeText) {
    writeString = "<table width=100% cellspacing=0 cellpadding=0 border=0>";
    writeString += "<tr><td class=LogoBack colspan=2><img width=551 height=56 src=" + window["E1RES_img_cityscape_small_gif"] + " alt=\"\"></td></tr>";
    writeString += "<tr class=FormTitleBack><td height=25 ><span class=FormTitle>&nbsp;" + dataItem + "</span> <span class=FormTitleSmall>&nbsp;" + szDict + "</span></td>";
    if (document.documentElement.dir == "rtl")
        writeString += "<td align=left>";
    else
        writeString += "<td align=right>";
    writeString += "&nbsp;<a href=javascript:closeItemHelp()><span class=FormTitleExits>" + closeText + "</a>&nbsp;</td></tr>";
    writeString += "</table>";

    document.write(writeString);
}

function closeItemHelp() {
    window.close();
}

function mouseOverItemHelp() {
    top.status = "Use the F1 Key for field level helps";
}

function mouseOutItemHelp() {
    top.status = "";
}

///////////////////////////////////////////////////////////////////////
//scripts for Handle onClick event on document for update capture mode
//and showing Help on controls -SAR 3794993 NL6721482
///////////////////////////////////////////////////////////////////////

//update cursor style on document for event=onmouseover for all browser
function document_onmouseover(e) {
    if (window.JDEDTAFactory != undefined) {
        if (JDEDTAFactory.isBusy())
            document.body.style.cursor = 'wait';
        else if (window.keyPickerMode || window.parameterPickerMode) {
            document.body.style.cursor = 'pointer';
        }
        else if (JDEDTAFactory.captureStatus == false)
            document.body.style.cursor = 'auto';
        else
            document.body.style.cursor = 'help';
    }
    if (typeof GCMH != "undefined" && GCMH != null) {
        for (i = 0; i < GCMH.gridIds.length; i++) {
            var grid = GCMH.findGrid(GCMH.gridIds[i]);
            if (grid.rightInt)
                window.clearInterval(grid.rightInt);
            if (grid.bottomInt)
                window.clearInterval(grid.bottomInt);
            if (grid.leftInt)
                window.clearInterval(grid.leftInt);
            if (grid.topInt)
                window.clearInterval(grid.topInt);
        }
    }
}

//handle mouseup event for all browsers
function document_onmouseup(e) {
    //alert(e.target.id);
    //Iteration 2
    if (typeof GCMH != "undefined" && GCMH != null) {
        for (i = 0; i < GCMH.gridIds.length; i++) {
            var grid = GCMH.findGrid(GCMH.gridIds[i]);
            if (grid.copyMarked && e != null) //true for both IE and FF; with attachevent e != null means IE as well.
            {
                if (e.target && e.target == grid.gridBack) // true for FF only; true when click/drag scrollbars. Restore focus back to gridBack
                {
                    grid.deferGridOperation("agent.setFocus(GCMH.findGrid('" + grid.gridId + "').gridBack)", 150);
                }
                else {
                    grid.clearCopyMarking();
                }
            }
            if (grid.formatChanging == grid.FORMAT_TYPE_COLORDER) {
                grid.performPostFormattingCleanup();
            }
            else if (grid.formatChanging == grid.FORMAT_TYPE_COLSIZE) {
                grid.gridHeaderBack.onmouseup();
            }
        }
    }
}

// handle click event for all borwsers
// funtion also called for regular form and PO form
function document_onclick(e) {
    var evt = e ? e : window.event;
    var calendar = document.getElementById("calendar");
    var calculator = document.getElementById("calculator");
    if ((calendar != null) && calendar.owner.calnObj != null)
        calendar.owner.calnObj.hideCalendar();
    if ((calculator != null) && calculator.owner.calcObj != null)
        calculator.owner.calcObj.hideCalculator();
    updateCaptureMode(evt);
    //close the hover form if the uesr clicks outside the form
    if (window.openedPopup && window.openedPopup.type == SHOW_HOVER) {
        if (evt.target.id != 'popupWindowTitleHover')//hide the form if the user clicks anywhere outside the hover form
        {
            window.openedPopup.hide();
        }
    }

    // Now that fast path is inside E1Menu, this seems annoying (opening the menu whenever we go home on portal). Removing for now.
    //    var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);
    //    if(isWSRP){
    //    if(parent.document.getElementById ('TE_FAST_PATH_BOX') != null)
    //    parent.document.getElementById ('TE_FAST_PATH_BOX').click();
    //    }
    //    else
    //    {
    if (!FCHandler.isTouchEnabled && !FCHandler.isWebkit) {
        try {
            if (parent.document.getElementById('menutitle') != null) {
                parent.document.getElementById('menutitle').click();
            }
        } catch (err) {
            //This try/catch was added to handle forms embedded in iFrames 
            //that don�t have access to the parent or top document.  
        }
    }
    else {
        mouseClickWebkit();
    }
    //    }
}


function mouseClickWebkit() {
    try {
        if (parent.document.getElementById('menutitle_hidden') != null) {
            parent.document.getElementById('menutitle_hidden').click();
        }
    } catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.  
    }
}

// Capture key code for all browsers.
function document_onkeydown(e) {
    FormKeyboardHandler.onKeyDown(e);
}

function document_onkeyup(e) {
    FormKeyboardHandler.onKeyUp(e);
}

function document_onblur(e) {
}


// The reused code by both form and PO (CDA.js) (all browser)
function ShowCaptureOn(event, eventObject) {
    document.body.style.cursor = 'help';
    if (document.documentElement.dir == "rtl")
        eventObject.src = window["E1RES_img_jdeitemhelp_hov_rtl_png"];
    else
        eventObject.src = window["E1RES_img_jdeitemhelp_hov_png"];

    if (document.all) {
        document.body.setCapture();
        event.cancelBubble = true;
    }
    else {
        event.stopPropagation();
        event.preventDefault();
    }
    alert(helpWindowTip);
}

// The reused code by both form and PO (CDA.js) (all browser)
function ShowCaptureOff(event, eventObject) {
    document.body.style.cursor = 'auto';
    if (document.documentElement.dir == "rtl")
        eventObject.src = window["E1RES_img_jdeitemhelp_ena_rtl_png"];
    else
        eventObject.src = window["E1RES_img_jdeitemhelp_ena_png"];
    if (document.all) {
        document.body.releaseCapture();
        event.cancelBubble = true;
    }
    else {
        event.stopPropagation();
        event.preventDefault();
    }
}

// Only used by Form to update capture mode (all browsers)
//Function called by regular and PO forms
function updateCaptureMode(evt) {
    var eventObject;
    if (document.all)
        eventObject = evt.srcElement;
    else
        eventObject = evt.target;

    //when E1 Form is closed, reset the grid format 'allowToggle' flag
    if (eventObject.id && (eventObject.id == "hc_Close" || eventObject.id == "SidePanelClose" || eventObject.id == "jdeclose_ena")) {
        if (typeof (localStorage) != 'undefined')
            localStorage.setItem("allowToggle", true);
    }
    // do not do anything for new help option image
    if (eventObject.id && eventObject.id.indexOf("helpOptionImage") == 0)
        return;

    if (eventObject.nodeName == "A") {
        var evtParentElem = eventObject.parentElement;
        if (evtParentElem && evtParentElem.id.indexOf("helpOptionsItemHelpId") > -1)
            eventObject = evtParentElem;
    }
    //On PO screen JDEDTA is not loaded so this prevents a javascript exception
    if (typeof JDEDTAFactory === "undefined")
        return;

    if (JDEDTAFactory.captureStatus == false) {
        if ((eventObject.src != null && eventObject.src.indexOf("helptip") > 0) ||
            (eventObject.name != null && eventObject.name.indexOf("helpIcon") >= 0) ||
            (eventObject.id.indexOf("helpOptionsItemHelpId") > -1)) {
            ShowCaptureOn(evt, eventObject);
            JDEDTAFactory.captureStatus = true;
        }
    }
    else {
        // Confirm stopPropagation support as it may be lacking for some IE releases and this particular object.
        if (evt && evt.stopPropagation) {
            evt.stopPropagation();
            evt.preventDefault();
        }
        else {
            window.event.cancelBubble = true;
            window.event.returnValue = false;
        }

        if ((eventObject.src != null && eventObject.src.indexOf("helptip") > 0) ||
            (eventObject.name != null && eventObject.name.indexOf("helpIcon") >= 0) ||
            (eventObject.id.indexOf("helpOptionsItemHelpId") > -1)) {
            ShowCaptureOff(evt, eventObject);
            JDEDTAFactory.captureStatus = false;
        }
        else if (eventObject.type == "radio") {
            var namespace = JDEDTAFactory.getNamespace(eventObject.value);
            if (namespace != null)
                eval(namespace + "hp(\"rbg\"+eventObject.value)");
        }
        else if (GUTIL.popupHelpForGridCell(eventObject)) // all grid cell should be handled here
        {
            return;
        }
        // The rest are for form header only
        else if (eventObject.name != null && eventObject.name != "") {
            // TODO: ICON does not work in the old grid either.
            var namespace = JDEDTAFactory.getNamespace(eventObject.name);
            if (namespace != null) {
                if (eventObject.name.substring(0, 3) == "hc_" || eventObject.name == "ROW_EXIT_BUTTON" || eventObject.name == "FORM_EXIT_BUTTON" || eventObject.name == "REPORT_EXIT_BUTTON") {
                    if (eventObject.id != null && eventObject.id != "") {
                        var myObj = document.getElementById(eventObject.id);
                        eval(namespace + "hp(myObj.parentElement.id)");
                    }
                }
                else {
                    eval(namespace + "hp(eventObject.name)");
                }
            }
        }
        else if (eventObject.id != null && eventObject.id != "") {
            var namespace = JDEDTAFactory.getNamespace(eventObject.id);
            if (namespace != null)
                eval(namespace + "hp(eventObject.id)");
        }
    }
}

// Used only by form to update cursor style on controls for onmouseover event (all browsers)
function updateHelpCursor(namespace, eventObject) {
    if (eventObject && eventObject.style && JDEDTAFactory) {
        if (window.keyPickerMode || window.parameterPickerMode) {
            eventObject.style.cursor = 'pointer';
            return true;
        }
        else if (JDEDTAFactory.captureStatus == false) {
            eventObject.style.cursor = 'auto';
            return false;
        }
        else {
            eventObject.style.cursor = 'help';
            return true;
        }
    }
    return true;
}


/////////////////////////////////////////
// Scripts for Error View
/////////////////////////////////////////
// Returns the closest parent tag with tagName containing
// the src tag. If no such tag is found - null is returned.
function checkParent(src, tagName) {
    while (src != null) {
        if (src.tagName == tagName)
            return src;
        src = src.parentElement;
    }
    return null;
}


// Returns the first tag with tagName contained by
// the src tag. If no such tag is found - null is returned.
function checkContent(src, tagName) {
    if (src == null) {
        return null;
    }
    var children = src.children;
    for (var i = 0; i < children.length; i++) {
        if (children[i].tagName && children[i].tagName == tagName)
            return children[i];
    }
    return null;
}


// Handle onClick event in the outline box
function outlineAction() {
    var src = event.srcElement;
    var item = checkParent(src, "LI");

    if (parent != null) {
        var content = checkContent(item, "UL");

        if (content != null)
            if (content.style.display == "")
                content.style.display = "block";
            else
                content.style.display = "";
    }
    event.cancelBubble = true;
}

////////////////////////////////////////////////////////
// Tree Control
////////////////////////////////////////////////////////
var TREEROWPROPOFFSET = 0, TREEINDENTOFFSET = 1, TREEIMAGEOFFSET = 2, TREETEXTOFFSET = 3, TREENODEBITMAPSETOFFSET = 4;
var TN_INDENTHASSIBLING = 1; // properties for tree node
var TR_SELECTED = 1, TR_EXPANDED = 2, TR_COLLAPSED = 4, TR_LEAFNODE = 8, TR_HIDDEN = 16;
var TREE_NOTIFYSELCHANGE = 1, TREE_NOTIFYDBLCLICK = 2;
var ACCESS_TREE_ROW_SELECTOR_FOR = 0, ACCESSIBILITY_TREE_ROOT_NODE = 1, ACCESSIBILITY_EXPANDED = 2, ACCESSIBILITY_COLLAPSED = 3,
    ACCESSIBILITY_TREE_LEVEL = 4, ACCESSIBILITY_TREE_LEAF_NODE = 5, ACCESS_TREE_ROW_SELECTOR = 6,
    ACCESSIBILITY_TREE_CHILD_NODE = 7, ACCESS_TREE_INSTRUCTIONS = 8;

// Singleton Tree Control Message Handler
var TCMH;
if (TCMH == null) {
    TCMH = new function () { }
}

TCMH.onTreeNodeSelectorClick = function (row, elem, idObj, e) {
    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null)
        treeObj.onSelectorClick(row, elem);
    TCMH.cancelEventBubbling(e);
}

TCMH.cancelEventBubbling = function (evt) {
    if (document.all) {
        evt.cancelBubble = true;
    }
    else {
        evt.stopPropagation();
    }
}

TCMH.onTreeNodeDblClick = function (row, elem, idObj) {
    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null)
        treeObj.onDblClickNode(row, elem);
}

TCMH.onTreeNodeKeyDown = function (e, row, elem, idObj, action) {
    if (e.keyCode != 13 && e.keyCode != 37 && e.keyCode != 39)  // only process ENTER, or left/right arrows
        return;
    if (e.keyCode == 37 || e.keyCode == 39)  // only handle arrow keys if ctrl and shift are pressed
    {
        if (!(e.ctrlKey) || !(e.shiftKey))  // if ctrl key is not pressed or shift key is not pressed, we bail
            return;
    }

    var LEFTARROW = 37;
    var RIGHTARROW = 39;
    if (isRTL)  // switch left and right if we are in RTL mode
    {
        LEFTARROW = 39;
        RIGHTARROW = 37;
    }

    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null) {
        if (action == 'DBLCLICK' && (e.keyCode == 13)) {
            treeObj.onDblClickNode(row, elem);
            treeObj.elemIdToFocus = elem.id;
        }
        else if (action == 'EXPAND' && (e.keyCode == 13 || (e.ctrlKey && e.shiftKey && e.keyCode == RIGHTARROW))) {
            treeObj.onExpandNode(row, e);
            treeObj.elemIdToFocus = elem.id;
        }
        else if (action == 'COLLAPSE' && (e.keyCode == 13 || (e.ctrlKey && e.shiftKey && e.keyCode == LEFTARROW))) {
            treeObj.onCollapseNode(row, e);
            treeObj.elemIdToFocus = elem.id;
        }
    }
}

TCMH.onTreeNodeControlKeyDown = function (e, row, elem, idObj, action) {
    var LEFTARROW = 37;
    var RIGHTARROW = 39;
    var DOWNARROW = 40;
    var UPARROW = 38;
    var SPACE = 32;
    var ENTER = 13
    if (isRTL)  // switch left and right if we are in RTL mode
    {
        LEFTARROW = 39;
        RIGHTARROW = 37;
    }

    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null) {
        switch (e.keyCode) {
            case RIGHTARROW: // open sub tree
                if (action == 'EXPAND') {
                    treeObj.onExpandNode(row, e);
                    treeObj.elemIdToFocus = elem.id;
                }
                break;

            case LEFTARROW: // collapse sub tree
                if (action == 'COLLAPSE') {
                    treeObj.onCollapseNode(row, e);
                    treeObj.elemIdToFocus = elem.id;
                }
                break;

            case DOWNARROW: // go to the next node
                var nextNodeId = TCMH.getIdOfTargetElement(elem.id, 1);
                FCHandler.setFocusOnElement(nextNodeId);
                break;

            case UPARROW: // go to the previous node
                var previousNodeId = TCMH.getIdOfTargetElement(elem.id, -1);
                FCHandler.setFocusOnElement(previousNodeId);
                break;

            case ENTER:
            case SPACE:
                if (action == 'DBLCLICK') // trigger hyperlink action
                {
                    elem.click();
                    FCHandler.setFocusOnElement(elem.id);
                }
                else // trigger row selection
                {
                    elem.parentNode.click();
                    FCHandler.setFocusOnElement(elem.id);
                }
                break;
        }
    }
}

TCMH.getIdOfTargetElement = function (id, delta) {
    var pos = id.lastIndexOf("_");
    var rowIndex = parseInt(id.substring(pos + 1));
    var prefix = id.substring(0, pos + 1);
    var newRowIndex = rowIndex;
    while (true) {
        newRowIndex = newRowIndex + delta;
        var targetImageId = "EC" + prefix.substring(2) + newRowIndex;
        var targetLinkId = "LF" + prefix.substring(2) + newRowIndex;
        var targetRowId = "TR" + prefix.substring(2) + newRowIndex;

        var targetRow = document.getElementById(targetRowId);

        if (!targetRow) // the Tree Row does not exit and can not set focus on its leaf
            return null;

        var elem = document.getElementById(targetLinkId);
        if (elem && elem.focus && elem.getAttribute("onkeydown")) // if find link leaf node first, do not need to search for image 
            return targetLinkId;

        var elem = document.getElementById(targetImageId);
        if (elem && elem.focus && elem.getAttribute("onkeydown"))
            return targetImageId;

        if (delta == 0) // do not repeat try the same row
            return null;
        // Not findding target link or image means the tree row is hidden and we continue to search using the next index.
    }
}

TCMH.onTreeExpandNode = function (row, idObj, e) {
    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null)
        treeObj.onExpandNode(row, e);
}

TCMH.onTreeCollapseNode = function (row, idObj, e) {
    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null)
        treeObj.onCollapseNode(row, e);
}

TCMH.onTreeRowClick = function (e) {
    var rowElem, sep, row, idObj;
    if (document.all) {
        rowElem = event.srcElement;
        while (rowElem.tagName != "TR")
            rowElem = rowElem.parentNode;
    } else {
        rowElem = e.currentTarget;
    }
    sep = rowElem.id.lastIndexOf("_");
    idObj = rowElem.id.substring(2, sep);
    row = parseInt(rowElem.id.substring(sep + 1));
    var treeObj = eval("jdeTree" + idObj);
    if (treeObj != null)
        treeObj.onRowClick(row);
}

function JDETree(id, treeProps, treeBase, treeData, width, height, namespace,
                 expandedRootAlt, collapsedRootAlt, parentWithSiblingAlt,
                 parentWithNoSiblingAlt, expandedAlt, collapsedAlt, leafAlt,
                 isAccessibilityEnabled, treeAccessibilityText, treeTitle) {
    this.namespace = namespace;
    this.id = id;
    this.treeProps = treeProps;
    this.baseIndex = treeBase;
    this.treeData = treeData;
    this.width = width;
    this.height = height;
    this.rowAdjustIndex = Number.MAX_VALUE; // If nodes are added/deleted, the node from which tree is updated
    this.firstInsertedRow = Number.MAX_VALUE;
    this.expandedRootAlt = expandedRootAlt;
    this.collapsedRootAlt = collapsedRootAlt;
    this.parentWithSiblingAlt = parentWithSiblingAlt;
    this.parentWithNoSiblingAlt = parentWithNoSiblingAlt;
    this.collapsedAlt = collapsedAlt;
    this.expandedAlt = expandedAlt;
    this.leafAlt = leafAlt;
    this.isAccessibilityEnabled = isAccessibilityEnabled;
    this.treeAccessibilityText = treeAccessibilityText;
    this.treeTitle = treeTitle;


    this.render = function () {
        eval("var treeObj = window.jdeTree" + this.id);
        if (treeObj == undefined || treeObj != this) {
            eval("jdeTree" + this.id + "= this");
            if (treeObj != undefined && treeObj != null)
                delete treeObj;
        }
        var strData = new Array(10), iStr = 0;
        // adding header/instruction for tree control to aid screen reader users
        strData[iStr++] = "<TABLE width=";
        strData[iStr++] = this.width;
        strData[iStr++] = " id=jdeTree";
        strData[iStr++] = this.id;
        strData[iStr++] = " BORDER=0 cellpadding=0 cellspacing=0";
        if (this.isAccessibilityEnabled) {
            strData[iStr++] = " role='tree' aria-label='" + treeTitle + "'";
        }
        strData[iStr++] = ">";
        strData[iStr++] = this.buildTree();

        strData[iStr++] = "</TABLE>";
        document.getElementById("jdeTreeContainer" + this.id).innerHTML = strData.join("");
        this.treeElem = document.getElementById("jdeTree" + this.id);

        if (JDEDTAFactory.getInstance(this.namespace) != null) {
            JDEDTAFactory.getInstance(this.namespace)["jdeTree_" + this.id] = this;
        }
        this.highlightCurSelRows();
    }

    this.buildTree = function () {
        var treeStr = new Array(25 * this.treeData.length), iStr = 0; // Allocate data approximately
        var realIndex;


        for (var c = 0; c < this.treeData.length; c++) {
            realIndex = c + this.baseIndex;

            treeStr[iStr++] = "<TR onclick=TCMH.onTreeRowClick(event) ID=TR" + this.id + "_" + realIndex + " depth=";
            if (this.treeData[c][TREEINDENTOFFSET].length == 0)
                treeStr[iStr++] = this.treeData[c][TREEINDENTOFFSET].length;
            else
                treeStr[iStr++] = this.treeData[c][TREEINDENTOFFSET].length - 1;

            if ((this.treeData[c][TREEROWPROPOFFSET] & TR_HIDDEN) > 0) {
                treeStr[iStr++] = " style='display:none'></TR>";
                continue;
            }

            var gridCellClass = "GridCell";
            if (document.documentElement.dir == "rtl")
                gridCellClass += "_rtl";
            treeStr[iStr++] = "><TD class=";
            treeStr[iStr++] = gridCellClass;
            treeStr[iStr++] = " WIDTH=20 valign=bottom>";
            treeStr[iStr++] = this.insertRowSelectorTag(c);
            treeStr[iStr++] = "</TD>\n<TD class=";
            treeStr[iStr++] = gridCellClass;
            treeStr[iStr++] = " nowrap>";
            treeStr[iStr++] = this.insertTreeNodeTag(c);
            treeStr[iStr++] = "</TD>\n</TR>\n";
        }
        return treeStr.join("");
    }

    this.insertRowSelectorTag = function (rowIndex) {
        var realIndex = rowIndex + this.baseIndex;
        var rStr = new Array(10), iStr = 0;
        rStr[iStr++] = "<INPUT TYPE=radio NAME=treers" + this.id;
        rStr[iStr++] = " ID=treers" + this.id + "." + realIndex;
        if (realIndex == 0) {
            rStr[iStr++] = " onblur=\"FCHandler.removeGroupJumpIndicatorById('";
            rStr[iStr++] = this.id;
            rStr[iStr++] = "');\"";
            rStr[iStr++] = " onfocus=\"FCHandler.onFocusByCtrlId('" + this.id + "','" + this.namespace + "')\"";
        }
        rStr[iStr++] = " VALUE=" + realIndex;

        if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_SELECTED) > 0) // row is selected
            rStr[iStr++] = " checked";

        if ((this.treeProps & TREE_NOTIFYSELCHANGE) > 0 || JDEDTAFactory.getInstance(this.namespace).iLevel != ILEVEL_LOW)
            rStr[iStr++] = " onClick=TCMH.onTreeNodeSelectorClick(" + realIndex + ",this,'" + this.id + "',event)";

        if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_LEAFNODE) > 0) {	//leaf
            if ((this.treeProps & TREE_NOTIFYDBLCLICK) > 0)
                rStr[iStr++] = " onDblClick=TCMH.onTreeNodeDblClick(" + realIndex + ",this,'" + this.id + "') onkeydown=TCMH.onTreeNodeKeyDown(event," + realIndex + ",this,'" + this.id + "','DBLCLICK')";
        }
        else if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_EXPANDED) > 0)
            rStr[iStr++] = " onDblClick=TCMH.onTreeCollapseNode(" + realIndex + ",'" + this.id + "',event) onkeydown=TCMH.onTreeNodeKeyDown(event," + realIndex + ",this,'" + this.id + "','COLLAPSE')";
        else
            rStr[iStr++] = " onDblClick=TCMH.onTreeExpandNode(" + realIndex + ",'" + this.id + "',event) onkeydown=TCMH.onTreeNodeKeyDown(event," + realIndex + ",this,'" + this.id + "','EXPAND')";

        rStr[iStr++] = ">";
        if (this.isAccessibilityEnabled) {
            var isExpanded = ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_EXPANDED) > 0);
            rStr[iStr++] = "<LABEL FOR=\"treers" + this.id + "." + realIndex + "\"";
            rStr[iStr++] = " class=\"accessibility\">";
            rStr[iStr++] = this.treeAccessibilityText[ACCESS_TREE_ROW_SELECTOR_FOR];
            rStr[iStr++] = ": ";
            if (this.treeData[rowIndex][TREEINDENTOFFSET].length == 0) // root node
            {
                rStr[iStr++] = this.getAltText('ROOT', rowIndex, isExpanded);
            }
            else if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_LEAFNODE) > 0) {
                rStr[iStr++] = this.getAltText('LEAF', rowIndex, isExpanded);
            }
            else {
                rStr[iStr++] = this.getAltText('CHILD', rowIndex, isExpanded);
            }
        }

        rStr[iStr++] = "</LABEL>";
        return rStr.join("");
    }

    this.insertTreeNodeTag = function (rowIndex) {
        var realIndex = rowIndex + this.baseIndex;
        var tnStr = new Array(20), iStr = 0;
        if (this.treeData[rowIndex][TREEINDENTOFFSET].length == 0) { // root node
            tnStr[iStr++] = "<IMG ID=EC" + this.id + "_" + realIndex + " tabindex=0 border=0 role=treeitem align=absbottom width=16 height=23 SRC=";
            if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_EXPANDED) > 0) { //expanded
                var rootexpImage = window["E1RES_img_rootexp_gif"];
                if (document.documentElement.dir == "rtl")
                    rootexpImage = window["E1RES_img_rootexp_rtl_gif"];
                tnStr[iStr++] = rootexpImage;
                tnStr[iStr++] = " expanded=true alt='";
                tnStr[iStr++] = this.getAltText('ROOT', rowIndex, true);
                tnStr[iStr++] = this.treeAccessibilityText[ACCESS_TREE_INSTRUCTIONS];
                tnStr[iStr++] = "' onkeydown=TCMH.onTreeNodeControlKeyDown(event," + 0 + ",this,'" + this.id + "','COLLAPSE')";
                tnStr[iStr++] = " onClick=TCMH.onTreeCollapseNode(" + 0 + ",'" + this.id + "',event)>";
            }
            else {
                var rootcolImage = window["E1RES_img_rootcol_gif"];
                if (document.documentElement.dir == "rtl")
                    rootcolImage = window["E1RES_img_rootcol_rtl_gif"];
                tnStr[iStr++] = rootcolImage;
                tnStr[iStr++] = " expanded=false alt='";
                tnStr[iStr++] = this.getAltText('ROOT', rowIndex, false);
                tnStr[iStr++] = "' onkeydown=TCMH.onTreeNodeControlKeyDown(event," + 0 + ",this,'" + this.id + "','EXPAND')";
                tnStr[iStr++] = " onClick=TCMH.onTreeExpandNode(" + 0 + ",'" + this.id + "',event)>";
            }
        }
        else {
            for (var j = this.treeData[rowIndex][TREEINDENTOFFSET].length - 1; j >= 1; j--) {
                tnStr[iStr++] = "<IMG border=0 align=absbottom width=16 height=23 SRC=";
                if ((this.treeData[rowIndex][TREEINDENTOFFSET][j] & TN_INDENTHASSIBLING) > 0) { // parent at this location has siblings
                    if (document.documentElement.dir == "rtl")
                        tnStr[iStr++] = window["E1RES_img_parwithsib_rtl_gif"];
                    else
                        tnStr[iStr++] = window["E1RES_img_parwithsib_gif"];
                }
                else {
                    tnStr[iStr++] = window["E1RES_img_parnosib_gif"];
                }
                tnStr[iStr++] = " alt=''>";
            }
            var expImgName, colImgName, leafImgName;
            if ((this.treeData[rowIndex][TREEINDENTOFFSET][0] & TN_INDENTHASSIBLING) > 0) // it has siblings
            {
                if (document.documentElement.dir == "rtl") {
                    expImgName = window["E1RES_img_expwithsib_rtl_gif"];
                    colImgName = window["E1RES_img_colwithsib_rtl_gif"];
                    leafImgName = window["E1RES_img_leafwithsib_rtl_gif"];
                }
                else {
                    expImgName = window["E1RES_img_expwithsib_gif"];
                    colImgName = window["E1RES_img_colwithsib_gif"];
                    leafImgName = window["E1RES_img_leafwithsib_gif"];
                }
            }
            else // no siblings
            {
                if (document.documentElement.dir == "rtl") {
                    expImgName = window["E1RES_img_expnosib_rtl_gif"];
                    colImgName = window["E1RES_img_colnosib_rtl_gif"];
                    leafImgName = window["E1RES_img_leafnosib_rtl_gif"];
                }
                else {
                    expImgName = window["E1RES_img_expnosib_gif"];
                    colImgName = window["E1RES_img_colnosib_gif"];
                    leafImgName = window["E1RES_img_leafnosib_gif"];
                }
            }
            tnStr[iStr++] = "<IMG ID=EC" + this.id + "_" + realIndex + " border=0 align=absbottom width=16 height=23 role=treeitem SRC=";
            if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_EXPANDED) > 0) //expanded
            {
                tnStr[iStr++] = expImgName;
                tnStr[iStr++] = " tabindex='0' alt='";
                tnStr[iStr++] = this.getAltText('CHILD', rowIndex, true);
                tnStr[iStr++] = "' onkeydown=TCMH.onTreeNodeControlKeyDown(event," + realIndex + ",this,'" + this.id + "','COLLAPSE')";
                tnStr[iStr++] = " onClick=TCMH.onTreeCollapseNode(" + realIndex + ",'" + this.id + "',event) expanded=true>";
            }
            else if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_COLLAPSED) > 0) //collapsed
            {
                tnStr[iStr++] = colImgName;
                tnStr[iStr++] = " tabindex='0' alt='";
                tnStr[iStr++] = this.getAltText('CHILD', rowIndex, false);
                tnStr[iStr++] = "' onkeydown=TCMH.onTreeNodeControlKeyDown(event," + realIndex + ",this,'" + this.id + "','EXPAND')";
                tnStr[iStr++] = " onClick=TCMH.onTreeExpandNode(" + realIndex + ",'" + this.id + "',event) expanded=false>";
            }
            else {
                tnStr[iStr++] = leafImgName;
                tnStr[iStr++] = " alt='" + this.getAltText('LEAF', rowIndex, false) + "'>";
            }
        }

        // For accessibility provide an ALT description that is the file name without the path and extension.
        var fileIdx, pathIdx, altDesc;
        fileIdx = this.treeData[rowIndex][TREEIMAGEOFFSET].indexOf(".");
        pathIdx = this.treeData[rowIndex][TREEIMAGEOFFSET].lastIndexOf("/");
        if (fileIdx != -1 && pathIdx != -1) {
            altDesc = this.treeData[rowIndex][TREEIMAGEOFFSET].substring(pathIdx + 1, fileIdx);
        }

        tnStr[iStr++] = "<IMG border=0 align=texttop width=16 height=16 SRC=" + this.treeData[rowIndex][TREEIMAGEOFFSET];
        if (altDesc != null) {
            tnStr[iStr++] = " ALT='" + altDesc + "'";
        }
        tnStr[iStr++] = "><span ID=PCT" + this.id + "_" + realIndex + " class=TreeNodeText><nobr>&nbsp;";
        if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_LEAFNODE) > 0 &&
            (this.treeProps & TREE_NOTIFYDBLCLICK) > 0) {
            tnStr[iStr++] = "<A href=javascript:onclick=JDEDTAFactory.getInstance('" + this.namespace + "').post('treeDbC" + this.id + "." + realIndex + "') role=treeitem ID=LF" + this.id + "_" + realIndex;
            tnStr[iStr++] = " onkeydown=TCMH.onTreeNodeControlKeyDown(event," + realIndex + ",this,'" + this.id + "','DBLCLICK') aria-label='" + this.getAltText('LEAF', rowIndex, false) + "'>";
        }

        tnStr[iStr++] = this.treeData[rowIndex][TREETEXTOFFSET];
        if ((this.treeData[rowIndex][TREEROWPROPOFFSET] & TR_LEAFNODE) > 0 &&
            (this.treeProps & TREE_NOTIFYDBLCLICK) > 0) {
            tnStr[iStr++] = "</A>";
        }
        tnStr[iStr++] = "&nbsp;</nobr></span>";
        return tnStr.join("");
    }

    // generate text used for the row selector labels, and also for img ALT tags
    this.getAltText = function (nodeType, rowIndex, expanded) {
        var altStr = new Array(12), idx = 0;
        if (nodeType == 'ROOT') {
            if (this.isAccessibilityEnabled) {
                altStr[idx++] = this.treeData[rowIndex][TREETEXTOFFSET];
                altStr[idx++] = ". ";
                altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_TREE_ROOT_NODE];
                altStr[idx++] = ". ";
                if (expanded)
                    altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_EXPANDED];
                else
                    altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_COLLAPSED];
                altStr[idx++] = ". ";
            }
            else {
                if (expanded)
                    altStr[idx++] = this.expandedRootAlt;
                else
                    altStr[idx++] = this.collapsedRootAlt;
            }
        }
        else if (nodeType == 'CHILD') {
            if (this.isAccessibilityEnabled) {
                altStr[idx++] = this.treeData[rowIndex][TREETEXTOFFSET];
                altStr[idx++] = ". ";
                altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_TREE_CHILD_NODE];
                altStr[idx++] = " ";
                altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_TREE_LEVEL];
                altStr[idx++] = " ";
                altStr[idx++] = (this.treeData[rowIndex][TREEINDENTOFFSET].length - 2);
                altStr[idx++] = ". ";
                if (expanded)
                    altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_EXPANDED];
                else
                    altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_COLLAPSED];
                altStr[idx++] = ". ";
            }
            else {
                if (expanded)
                    altStr[idx++] = this.expandedAlt;
                else
                    altStr[idx++] = this.collapsedAlt;
                altStr[idx++] = (this.treeData[rowIndex][TREEINDENTOFFSET].length - 2);
            }
        }
        else if (nodeType == 'LEAF') {
            if (this.isAccessibilityEnabled) {
                altStr[idx++] = this.treeData[rowIndex][TREETEXTOFFSET];
                altStr[idx++] = ". ";
                altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_TREE_LEAF_NODE];
                altStr[idx++] = " ";
                altStr[idx++] = this.treeAccessibilityText[ACCESSIBILITY_TREE_LEVEL];
                altStr[idx++] = " ";
                altStr[idx++] = (this.treeData[rowIndex][TREEINDENTOFFSET].length - 2);
                altStr[idx++] = ". ";
            }
            else {
                altStr[idx++] = this.leafAlt;
                altStr[idx++] = (this.treeData[rowIndex][TREEINDENTOFFSET].length - 2);
            }
        }

        return altStr.join("");
    }

    // EVENT HANDLERS. The rowindex is already adjusted with the baseindex.
    this.onSelectorClick = function (rowIndex, elem) {
        if (!elem.checked) //already selected or doing a unselect
            return;

        // first unselect the previous row
        for (var r = 0; r < this.treeData.length; r++) {
            realIndex = r + this.baseIndex;
            if ((this.treeData[r][TREEROWPROPOFFSET] & TR_SELECTED) > 0) {
                this.treeData[r][TREEROWPROPOFFSET] &= ~TR_SELECTED;
                var selElem = document.getElementById("treers" + this.id + "." + realIndex);
                if (selElem != null)
                    this.setRowSel(selElem, false);
                break;
            }
        }
        this.treeData[rowIndex - this.baseIndex][TREEROWPROPOFFSET] |= TR_SELECTED;
        this.setRowSel(elem, true);
        JDEDTAFactory.getInstance(this.namespace).post("treeSel" + this.id + "." + rowIndex);
    }

    this.onDblClickNode = function (rowIndex, elem) {
        if (!elem.checked) //already selected or doing a unselect
            return;

        JDEDTAFactory.getInstance(this.namespace).post("treeDbC" + this.id + "." + rowIndex);
    }

    this.onExpandNode = function (rowIndex, e) {
        JDEDTAFactory.getInstance(this.namespace).post("tree_EC" + this.id + "." + rowIndex);
        if (document.all) {
            event.cancelBubble = true;
            event.returnValue = false;
        } else {
            e.stopPropagation();
            e.preventDefault();
        }
    }

    this.onCollapseNode = function (rowIndex, e) {
        JDEDTAFactory.getInstance(this.namespace).post("tree_EC" + this.id + "." + rowIndex);
        if (document.all) {
            event.cancelBubble = true;
            event.returnValue = false;
        } else {
            e.stopPropagation();
            e.preventDefault();
        }
    }

    this.onRowClick = function (rowIndex) {
        var selElem = document.getElementById("treers" + this.id + "." + rowIndex);
        if (selElem != null) {
            selElem.checked = true;
            this.onSelectorClick(rowIndex, selElem);
        }
    }

    this.setRowSel = function (elem, bSet) {
        var cellClass = "SelectedGridCell";

        if (bSet == 0) { // if we are removing it
            cellClass = "GridCell";
            if (document.documentElement.dir == "rtl")
                cellClass += "_rtl";
        }
        else
            elem.focus();

        while (elem.tagName != "TR") {
            elem = elem.parentNode;
        }
        if (elem != null) {
            var startIndex = 1, endIndex = elem.childNodes.length;
            for (var i = 0; i < elem.childNodes.length; i++) {
                if (elem.childNodes[i].tagName == "TD") {
                    elem.childNodes[i].className = cellClass;
                }
            }
        }
    }

    this.highlightCurSelRows = function () {
        for (var r = 0; r < this.treeData.length; r++) {
            var realIndex = r + this.baseIndex;
            if ((this.treeData[r][TREEROWPROPOFFSET] & TR_SELECTED) > 0) {
                var selElem = document.getElementById("treers" + this.id + "." + realIndex);
                if (selElem != null)
                    this.setRowSel(selElem, true);
                break;
            }
        }
    }

    // API's to dynamically update the UI
    this.selNode = function (row) {
        var selElem = document.getElementById("treers" + this.id + "." + row);
        if (selElem != null)
            selElem.checked = true;
    }

    this.insertDataForTreeRow = function (rowElem, row) {
        var cellElem = rowElem.insertCell(0);
        cellElem.width = 20;
        cellElem.vAlign = "bottom";
        if (document.documentElement.dir == "rtl")
            cellElem.className = "GridCell_rtl";
        else
            cellElem.className = "GridCell";
        cellElem.innerHTML = this.insertRowSelectorTag(row);
        cellElem = rowElem.insertCell(1);
        cellElem.noWrap = true;
        if (document.documentElement.dir == "rtl")
            cellElem.className = "GridCell_rtl";
        else
            cellElem.className = "GridCell";
        cellElem.innerHTML = this.insertTreeNodeTag(row);
        if ((this.treeData[row][TREEROWPROPOFFSET] & TR_SELECTED) > 0)
            this.setRowSel(rowElem, true);
    }

    this.insertNode = function (row, value) {
        var rowElem, cellElem, prevRowCount, tempRow = null;

        prevRowCount = this.treeData.length;
        eval("tempRow=new Array(" + value + ")");
        var tempArray = this.treeData.splice(row, this.treeData.length - row, tempRow);
        this.treeData = this.treeData.concat(tempArray);

        if (row < this.rowAdjustIndex)
            this.rowAdjustIndex = row + 1;
        var rowidx = row >= this.treeElem.rows.length ? -1 : row;
        rowElem = this.treeElem.insertRow(rowidx);
        rowElem.id = "TR" + this.id + "_" + row;
        if (this.treeData[row][TREEINDENTOFFSET].length == 0)
            rowElem.depth = this.treeData[row][TREEINDENTOFFSET].length;
        else
            rowElem.depth = this.treeData[row][TREEINDENTOFFSET].length - 1;

        if ((this.treeData[row][TREEROWPROPOFFSET] & TR_HIDDEN) > 0)
            rowElem.style.display = "none";
        else {
            if (rowElem.onclick == null) {
                if (!document.all)
                    rowElem.addEventListener("click", TCMH.onTreeRowClick, false);
                else
                    rowElem.onclick = TCMH.onTreeRowClick;
            }
            this.insertDataForTreeRow(rowElem, row);
            if (row < this.firstInsertedRow)
                this.firstInsertedRow = row;
        }
    }

    this.adjustRowsAfter = function (row) {
        var rowElem, cellElem;
        for (var i = row; i < this.treeData.length; i++) {
            rowElem = this.treeElem.rows[i];
            rowElem.id = "TR" + this.id + "_" + i; //adjust row id
            if ((this.treeData[i][TREEROWPROPOFFSET] & TR_HIDDEN) > 0)
                continue;

            cellElem = rowElem.cells[0];
            cellElem.innerHTML = this.insertRowSelectorTag(i);
            cellElem = rowElem.cells[1];
            cellElem.innerHTML = this.insertTreeNodeTag(i);
        }
    }

    this.startNodeAggregate = function () {
    }

    this.stopNodeAggregate = function (scrollOptionsValue) {
        document.getElementById("treeScrollOptions" + this.id).innerHTML = scrollOptionsValue;
        if (this.rowAdjustIndex < Number.MAX_VALUE) {
            this.adjustRowsAfter(this.rowAdjustIndex);
            this.rowAdjustIndex = Number.MAX_VALUE;
        }
        if (this.firstInsertedRow < Number.MAX_VALUE) {
            this.scrollNodeIntoView(this.firstInsertedRow);
            this.firstInsertedRow = Number.MAX_VALUE;
        }
    }

    this.deleteNode = function (row) {
        var rowElem, cellElem, prevRowCount;
        prevRowCount = this.treeData.length;
        this.treeElem.deleteRow(row);

        if (row < this.rowAdjustIndex)
            this.rowAdjustIndex = row;

        var tempArray = this.treeData.splice(row + 1, this.treeData.length - (row + 1));
        this.treeData.pop(); // remove the deleted row from data
        this.treeData = this.treeData.concat(tempArray);
    }

    this.showNode = function (row, value) {
        var rowElem = this.treeElem.rows[row];
        eval("this.treeData[" + row + "]=new Array(" + value + ")");
        var cellCount = rowElem.cells.length;
        for (var i = cellCount - 1; i >= 0; i--)
            rowElem.deleteCell(i)
        this.insertDataForTreeRow(rowElem, row);
        rowElem.style.display = "";
        if (this.elemIdToFocus) {
            FCHandler.searchFocusOnElement(this.elemIdToFocus); // set the focus back to the original location before user trigger expend, collpase event.
            this.elemIdToFocus = null;
        }
    }

    this.hideNode = function (row, value) {
        var rowElem = this.treeElem.rows[row];
        var cellCount = rowElem.cells.length;
        for (var i = cellCount - 1; i >= 0; i--)
            rowElem.deleteCell(i);
        rowElem.style.display = "none";
        this.treeData[row][TREEROWPROPOFFSET] |= TR_HIDDEN;
        if (this.elemIdToFocus) {
            FCHandler.searchFocusOnElement(this.elemIdToFocus);  // set the focus back to the original location before user trigger expend, collpase event.
            this.elemIdToFocus = null;
        }
    }

    this.delAllNodes = function () {
        this.treeData.length = 1;
        var rowCount = this.treeElem.rows.length;
        for (var i = rowCount - 1; i >= 1; i--)
            this.treeElem.deleteRow(i);
    }

    this.recCountChanged = function (scrollOptionsValue) {
        document.getElementById("treeScrollOptions" + this.id).innerHTML = scrollOptionsValue;
    }

    this.scrollNodeIntoView = function (row) {
        var elem = this.treeElem.rows[row];
        if (elem != null) {
            var treeBack = this.treeElem;
            while (treeBack != null && treeBack.tagName != "DIV")
                treeBack = treeBack.offsetParent;

            if (elem.offsetParent) {
                if (elem.offsetTop + elem.offsetParent.offsetTop > (treeBack.offsetHeight - 5)) {
                    treeBack.scrollTop = elem.offsetTop + elem.offsetParent.offsetTop + 5;
                }
            }
        }
    }
}

//////////////////////////////////////////////////
// Scripts for Media Object
/////////////////////////////////////////////////

var MOHandler;
if (MOHandler == null) {
    MOHandler = new function () {
        this.containerIDs = new Array();
        this.RTFIDs = new Array();
    }
}

MOHandler.addRTFEdit = function (id) {
    for (var index = 0; index < this.RTFIDs.length; ++index) {
        if (this.RTFIDs[index] == id) { // if the user is coming back to this tab
            return;
        }
    }
    this.RTFIDs[this.RTFIDs.length] = id;
}

MOHandler.addJDEContainer = function (id) {
    for (var index = 0; index < this.containerIDs.length; ++index) {
        if (this.containerIDs[index] == id) { // if the user is coming back to this tab
            return;
        }
    }
    this.containerIDs[this.containerIDs.length] = id;
}

MOHandler.doMediaObject = function (namespace) {
    var index;
    if (this.containerIDs.length > 0) {
        var dtaInstance = JDEDTAFactory.getInstance(namespace);
        var formObj = document.forms[dtaInstance.formName];
        if (formObj) {
            for (index = 0; index < this.containerIDs.length; ++index) {
                var jdeContainer = formObj("jdecontainer" + this.containerIDs[index]);
                if (jdeContainer != null) {
                    if (jdeContainer.ActiveSiteNumber >= 0) {
                        jdeContainer.Activate(jdeContainer.ActiveSiteNumber, false);
                        if (jdeContainer.IsModified()) {
                            var url = formObj.uploadURL.value;
                            if (url != null) {
                                jdeContainer.SetModified(false);
                                try {
                                    jdeContainer.UploadToWebServer(url);
                                    if (dtaInstance.iLevel == ILEVEL_LOW) {
                                        formObj["oleModified" + this.containerIDs[index]].value = 'TRUE';
                                        FCHandler.markExitedByName("oleModified" + this.containerIDs[index]);
                                    }
                                    else {
                                        dtaInstance.prependPostQueue("oleModified", this.containerIDs[index], null);
                                    }
                                }
                                catch (e)
                                { }
                            }
                        }
                    }
                }
            }
        }
    }

    if (this.RTFIDs.length > 0) {
        var dtaInstance = JDEDTAFactory.getInstance(namespace);
        var formObj = document.forms[dtaInstance.formName];
        if (formObj) {
            for (index = 0; index < this.RTFIDs.length; ++index) {

                if (document.all) {
                    var rtfEdit = document.getElementById("jdeRTF" + this.RTFIDs[index])
                }
                else {
                    var rtfEdit = formObj["jdeRTF" + this.RTFIDs[index]];
                }
                if (rtfEdit != null && rtfEdit.IsModified()) {
                    var text = null;
                    if (rtfEdit.ASCIIMode)
                        text = rtfEdit.GetTextData();
                    else
                        text = rtfEdit.GetData();

                    if (dtaInstance.iLevel == ILEVEL_LOW) {
                        FCHandler.markExitedByName("rtfModified" + this.RTFIDs[index]);
                        formObj["rtfModified" + this.RTFIDs[index]].value = 'TRUE';
                        formObj["motext" + this.RTFIDs[index]].value = text;
                    }
                    else {
                        dtaInstance.prependPostQueue("rtfModified", this.RTFIDs[index], text);
                        rtfEdit.SetModified(false);
                    }
                }
                else if ((formObj["ActiveXSupported"] != null) && (formObj["ActiveXSupported"].value == "false")) {
                    var ckeText = null;
                    var ckeDiv = document.getElementById("cke_" + this.RTFIDs[index]);
                    if (ckeDiv) {
                        var contentiFrames = ckeDiv.getElementsByTagName('iframe');
                        if (contentiFrames.length > 0) {
                            var iframe = contentiFrames[0];
                        }
                        if (iframe) {
                            if (iframe.contentDocument.body != null)
                                ckeText = iframe.contentDocument.body.innerHTML;
                        }
                    }
                    if (ckeText != null) {
                        ckeText = ckeText.replace(/\r/mg, "");
                        ckeText = ckeText.replace(/\n/mg, "");
                        ckeText = encodeHtml(ckeText);
                    }

                    if (dtaInstance.iLevel == ILEVEL_LOW) {
                        FCHandler.markExitedByName("rtfModified" + this.RTFIDs[index]);
                        formObj["rtfModified" + this.RTFIDs[index]].value = 'TRUE';
                        formObj["motext" + this.RTFIDs[index]].value = ckeText;
                    }
                    else {
                        dtaInstance.prependPostQueue("rtfModified", this.RTFIDs[index], ckeText);
                    }
                }
            }
        }
    }
}

function encodeHtml(htmlText) {
    htmlText = htmlText.replace(/</g, "&#0060;");
    htmlText = htmlText.replace(/>/g, "&#0062;");
    htmlText = htmlText.replace(/=/g, "&#061;");
    htmlText = htmlText.replace(/:/g, "&#058;");
    return htmlText;
}

//NL-XAPI functions
//This is called only for low interactivity. The "pollFrame" will not be rendered for high interactivity
var waitForPoll = false;
function poll(stackId, namespace) {
    if (document.all)
        var pollFrame = document.frames("pollFrame" + namespace); //frame for poll server
    else
        var pollFrame = window.frames["pollFrame" + namespace];

    if (pollFrame != null && waitForPoll == false && !JDEDTAFactory.getInstance(namespace).isBusy()) {
        var strWrite = "<html><body>";
        if (document.documentElement.dir == "rtl")
            strWrite = "<html dir=rtl><body dir=rtl>";

        strWrite += "<form name=pollForm action='" + JDEDTAFactory.getInstance(namespace).formAction + "' method=post>\n";
        strWrite += "<input type=hidden name=pollServerAct value='true'>\n";
        strWrite += "<input type=hidden name=stackId value='" + stackId + "'>\n";
        strWrite += "<input type=hidden name=RID value='" + JDEDTAFactory.getInstance(namespace).RID + "'>\n";
        strWrite += "<script>function postDataAct() {document.pollForm.submit();}</script>\n";
        strWrite += "</form></body></html>";

        var newDoc = pollFrame.document.open("text/html", "replace");
        newDoc.write(strWrite);
        newDoc.close();
        waitForPoll = true;
        if (document.all)
            document.frames("pollFrame" + namespace).postDataAct();
        else
            window.frames["pollFrame" + namespace].postDataAct();
    }
}

//Called only in low interactivity mode
function acknowledgePoll(namespace) {
    if (document.all)
        var pollFrame = document.frames("pollFrame" + namespace); //frame for poll server
    else
        var pollFrame = window.frames["pollFrame" + namespace];

    if (pollFrame != null) {
        var pEventType = pollFrame.document.pollForm["pEventType"].value;

        waitForPoll = false;

        if (pEventType != 0) {
            JDEDTAFactory.getInstance(namespace).post('');
        }
    }
}

function startPoll(interval, stackId, namespace) {
    if (JDEDTAFactory.getInstance(namespace).iLevel == ILEVEL_LOW)
        setInterval("poll('" + stackId + "','" + namespace + "')", interval);
    else
        setInterval("JDEDTAFactory.getInstance('" + namespace + "').post(\" \")", interval);
}

function isEmptyString(value) {
    if (value == null || value.length == 0)
        return true;
    else {
        var i;
        for (i = 0; i < value.length; i++) {
            if (value.charAt(i) != ' ') {
                return false;
            }
        }
        return true;
    }
}

// this function is used to check if an non-empty name has been entered in the saved query screen
function verifyAndPost(name, errorMessage, command, namespace) {
    var control = document.getElementById(name);
    if (control != null) {
        if (isEmptyString(control.value)) {
            alert(errorMessage);
        }
        else {
            JDEDTAFactory.getInstance(namespace).post(command);
        }
    }
}

// this function is used to check if an non-empty name has been entered in the saved query screen
function verifyAndPostLater(name, errorMessage, command, namespace) {
    var control = document.getElementById(name);
    if (control != null) {
        if (isEmptyString(control.value)) {
            alert(errorMessage);
        }
        else {
            JDEDTAFactory.getInstance(namespace).postLater(command);
        }
    }
}

// Dump Array to HTML for debug and monitor
if (window.JSMonitor && JSMonitor.enable) {
    Object.prototype.toHTML = function () {
        return this.toString();
    }
}

//////////////////////////////////////////////////
// String Trimming functions
//////////////////////////////////////////////////
String.prototype.LTrim = new Function("return this.replace(/^\\s+/,'')")
String.prototype.RTrim = new Function("return this.replace(/\\s+$/,'')")
String.prototype.Trim = new Function("return this.replace(/^\\s+|\\s+$/g,'')")


/*
Adds methods to core Array class
*/

/*
Adds single item to the end of this array
*/
Array.prototype.add = function (item) {
    this[this.length] = item;
}

/*
Adds a array of items to the end of this array
*/
Array.prototype.addAll = function (items) {
    for (var i = 0; i < items.length; i++) {
        this.add(items[i]);
    }
}

/*
Asks if this array contains an item
*/
Array.prototype.contains = function (item) {
    for (var i = 0; i < this.length; i++) {
        if (item == this[i]) {
            return true;
        }
    }

    return false;
}

/*
returns the index of this item
*/
Array.prototype.findIndex = function (item) {
    var fIndex = -1;
    for (var i = 0; i < this.length; i++) {
        if (item == this[i]) {
            fIndex = i;
            break;
        }
    }
    return fIndex;
}

/*
Performs a shallow clone of this array.
*/
Array.prototype.clone = function () {
    var clone = new Array();

    for (var i = 0; i < this.length; i++) {
        clone[i] = this[i];
    }

    return clone;
}

/*
Dump the array content into a table tag. It can be used for logging by JSMonitor.
*/
Array.prototype.toHTML = function () {
    var i;
    var str = '<table border="1" cellpadding="1" cellspacing="1" style="border-collapse: collapse" bordercolor="#111111"><tr>';
    for (i = 0; i < this.length; i++) {
        str += '<td NOWRAP>' + this[i].toHTML() + '</td>';
    }
    str += '</tr></table>';
    return str;
}

/*
Adds methods to core Date class
*/

Date.prototype.DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;

Date.prototype.WEEK_IN_MILLISECONDS = Date.prototype.DAY_IN_MILLISECONDS * 7;

Date.prototype.clone = function () {
    return new Date(this.getTime());
}

Date.prototype.compareTo = function (date) {
    return this.getTime() - date.getTime();
}

Date.prototype.isAfter = function (date) {
    return this.getTime() > date.getTime();
}

Date.prototype.isAfterOrEqual = function (date) {
    return this.getTime() >= date.getTime();
}

Date.prototype.isBefore = function (date) {
    return this.getTime() < date.getTime();
}

Date.prototype.isBeforeOrEqual = function (date) {
    return this.getTime() <= date.getTime();
}

Date.prototype.isEqual = function (date) {
    return this.getTime() == date.getTime();
}

/*
Adds a given number of days to this object
*/
Date.prototype.addDays = function (days) {
    var time = this.getTime();
    time += Date.prototype.DAY_IN_MILLISECONDS * days;

    this.setTime(time);
    return this;
}

/*
Adds a given number of weeks to this object
*/
Date.prototype.addWeeks = function (weeks) {
    return this.addDays(weeks * 7);
}

/*
Adds a given number of months to this object
*/
Date.prototype.addUTCMonths = function (months) {
    //TODO: May not work for days from 29 - 31
    var month = this.getUTCMonth();
    var year = this.getUTCFullYear();

    if (months > 0)
        year += Math.floor(months / 12);
    else
        year -= Math.ceil(months / 12);

    month += months % 12;

    if (month > 12) {
        years += 1;
        months -= 12;
    }
    else if (month < 0) {
        years -= 1;
        months += 12;
    }

    this.setUTCFullYear(year);
    this.setUTCMonth(month);

    return this;
}

/*
Gets the difference between this time and the targetDate in days.
*/
Date.prototype.getDayDifference = function (targetDate) {
    var sourceTime = this.getTime();
    var targetTime = targetDate.getTime();

    return Math.floor((targetTime - sourceTime) / Date.prototype.DAY_IN_MILLISECONDS);
}

Date.prototype.setUTCDateTime = function (year, month, date, hours, minutes, seconds, milliseconds) {
    this.setUTCFullYear(year);
    this.setUTCDate(date);
    this.setUTCMonth(month);
    this.setUTCHours(hours);
    this.setUTCMinutes(minutes);
    this.setUTCSeconds(seconds);
    this.setUTCMilliseconds(milliseconds);

    return this;
}

/*
A class that represents an interval of time.
*/
function TimeInterval(start, end) {
    assert("end time must be greater than start time", end.getTime() >= start.getTime());
    this.start = start;
    this.end = end;
}

/*
Gets this interval in milliseconds
*/
TimeInterval.prototype.timeSpan = function () {
    return this.end.getTime() - this.start.getTime();
}

/*
Asks if this interval overlaps with the given interval
*/
TimeInterval.prototype.isConcurrent = function (interval) {
    // check for overlap as well as one of the two being zero interval and then contained in other
    return ((this.end.isAfter(interval.start) && this.start.isBefore(interval.end)) ||
             (this.timeSpan() == 0 && interval.contains(this)) ||
             (interval.timeSpan() == 0 && this.contains(interval)));
}

/*
Asks if the given interval is entirely contained by this interval
*/
TimeInterval.prototype.contains = function (interval) {
    return this.start.isBeforeOrEqual(interval.start) && this.end.isAfterOrEqual(interval.end);
}

/*
Asks if the given date falls within this interval
*/
TimeInterval.prototype.containsDate = function (date) {
    return this.start.isBeforeOrEqual(date) && this.end.isAfterOrEqual(date);
}

// SAR # 7073561 - added assertion functions to be used by calendar

/*
Asserts arg is not null; else throws error with msg.
*/
function assertNotNull(msg, arg) {
    if (arg == null) {
        throw new Error(msg);
    }
}


/*
Asserts arg is true; else throws error with msg.
*/
function assert(msg, arg) {
    if (!arg) {
        throw new Error(msg);
    }
}

/*
Asserts args.length == numberOfArguments and that
each arg is not null; else throws error with functionName.
*/
function assertArgumentsNotNull(functionName, numberOfArguments, args) {
    if (numberOfArguments != args.length) {
        throw new Error(functionName + " expected " + numberOfArguments + " arguments but received " + args.length + " arguments");
    }

    for (var i = 0; i < args.length; i++) {
        assertNotNull(functionName + " received null argument at index " + i, args[i]);
    }
}

//block user access to the under form
function protectUnderform(depth, ownerDoc, namespace) {
    var ownerDoc = (ownerDoc == null) ? document : ownerDoc;
    var menuBar;
    if (depth != null && depth > 1) {
        var blockPaneDiv = ownerDoc.getElementById("UIBlockingDiv");
        if (blockPaneDiv != null)
            blockPaneDiv.style.zIndex = UIBLOCKING_LAYER_ZINDEX * depth;
        menuBar = window.parent.document.getElementById("modalIframe" + (depth - 1)).contentWindow.document.getElementById("WebMenuBar");
        if (menuBar)
            menuBar.style.zIndex = 0;
    }
    else {
        if (window.blockPane) {
            window.blockPane._showBlockingDiv(ownerDoc);
        }
        else {
            blockPane = new UIBlockingPane();
            blockPane.blockUI(ownerDoc);
        }

        if (depth == 1) {
            menuBar = document.getElementById("WebMenuBar");
            if (menuBar)
                menuBar.style.zIndex = 0;
        }
    }
}

function positionBlockPane() {
    if (blockPane != null)
        blockPane._positionBlockingDiv(document);
}

function removeEscape() {
    msgContent = window.unescape(msgContent);
}

function msgModalWindow(id, left, top, width, height, title, aboutText, namespace) {
    var titleInfo = {
        titleIcon: "",
        titleText: title,
        titleTextColor: "#000000",
        hasClose: false,
        hasAbout: true,
        aboutText: aboutText
    };

    var pw = new PopupWindow("modalForm" + id, false, titleInfo, null, null, null, MSG_MODAL);
    pw.setContent(msgContent);
    pw.setNameSpace(namespace);
    var e1formDiv = document.getElementById("e1formDiv");
    var formDivHeight = 0;
    var formDivWidth = 0;
    if (e1formDiv) {
        formDivHeight = e1formDiv.offsetHeight;
        formDivWidth = e1formDiv.offsetWidth;
    }
    //adjust the top if the modal form is out side of the window
    if ((top + height) > formDivHeight && formDivHeight != 0)
        top = formDivHeight - height - 60;

    if ((left + width) > formDivWidth)
        left = formDivWidth - width - 60;

    pw.display(left, top, width, height);
    //in IE, set tabindex=-1 will make tab non stop
    pw.windowElem.tabIndex = "-1";
    //trap the tab key to move within message form only
    if (document.all) //IE
    {
        pw.windowElem.onkeydown = mmfTabKeyPressHandler;
    }
    else //Mozilla/Firefox/Safari
    {
        pw.windowElem.onkeypress = mmfTabKeyPressHandler;
    }

    //eval the jscomponent creation script
    eval(jsComponentCreation);
    if (window.MsgFormAdjustTBScript != undefined) {
        eval(MsgFormAdjustTBScript);
        FCHandler.addjustEffectControlBelowTextBlock();
        pw.resize(width, height + heightInc);
    }
}

// Tab key trap. if popup is shown and key was [TAB], suppress it.
// @argument e - event - keyboard event that caused this function to be called.
function mmfTabKeyPressHandler(e) {
    var evt = e ? e : window.event;
    var activeElement = (evt.target === undefined) ? document.activeElement : evt.target;
    var stopPropagation = false;
    if (evt.keyCode == 9) //Tab
    {
        if (evt.shiftKey) //Shift+Tab
        {
            if (activeElement.tabIndex == minTabIdxOnMMF) {
                var focusElmOnMMF = document.getElementsByName(maxTabIdxElement);
                if (focusElmOnMMF != null && focusElmOnMMF[0] != null && focusElmOnMMF[0].focus != null && focusElmOnMMF[0].disabled != true)
                    focusElmOnMMF[0].focus();
                stopPropagation = true;
            }
        }
        else if (activeElement.tabIndex == maxTabIdxOnMMF) {
            var focusElmOnMMF = document.getElementsByName(minTabIdxElement);
            if (focusElmOnMMF != null && focusElmOnMMF[0] != null && focusElmOnMMF[0].focus != null && focusElmOnMMF[0].disabled != true)
                focusElmOnMMF[0].focus();
            stopPropagation = true;
        }
        //stop the event bubbling
        if (stopPropagation) {
            var agent = Agent.getAgent();
            agent.stopPropagation(evt);
            agent.preventDefault(evt);
            return false;
        }
    }
    return true;
}

function hideScroll(gridID) {
    var grid = eval("jdeGrid" + gridID);
    var gb = grid.gridBack;
    if (gb.style) {
        if (gb.style.overflow)
            gb.style.overflow = "hidden";
    }
}
function checkActiveXStatus(namespace, activeXCtrlName) {
    if (document.getElementById(activeXCtrlName) == null) {
        window.oc(namespace, "disable_ActiveX");
    }
}
///////////////////////////////////////
// Singleton Sync Toolbar with E1FormDiv Handler
///////////////////////////////////////
var FormDivScrollHandler;
if (FormDivScrollHandler == null) {
    FormDivScrollHandler = new function () {
        this.prevScrollLeft = 0;
        this.prevScrollTop = 0;
        this.isRTL = isRTL;
        this.SB_SIZE = 17;
        this.isIE = document.all;
        this.checkDropDownMenu = false;
    }
}

//set up a series of prototype functions -only set once, for efficiency
FormDivScrollHandler.initializeFunctions = function () {
    this.menuBarTable = document.getElementById("WebMenuBar");
    this.e1formDiv = document.getElementById("e1formDiv");
    this.formTable = document.getElementById("FormAboveGridTable");
    FormDivScrollHandler.getWindowHeight = FormDivScrollHandler.getWindowHeightFunction();
    FormDivScrollHandler.getWindowWidth = FormDivScrollHandler.getWindowWidthFunction();
    FormDivScrollHandler.handleHorizontalScroll = FormDivScrollHandler.handleHScrollFunc();
    //for RTL, init the prevScrollLeft to estimated scroll limit
    if (this.isRTL)
        this.prevScrollLeft = FormDivScrollHandler.getScrollLimit();
}

FormDivScrollHandler.getScrollLimit = function () {
    var formTableWidth = 0;
    if (this.formTable != null) {
        formTableWidth = this.formTable.offsetWidth;
    }
    var windowWidth = FormDivScrollHandler.getWindowWidth();
    var scrollLimit = 0;
    if (formTableWidth > (windowWidth + this.SB_SIZE + 5))
        scrollLimit = formTableWidth - windowWidth + this.SB_SIZE + 5;
    else
        scrollLimit = this.SB_SIZE;

    return scrollLimit;
}

//This function returns a function, so that a frequently calculated value 
//doesn't need to run through multiple IF tests every time it is needed
FormDivScrollHandler.getWindowWidthFunction = function () {
    if (window.innerWidth !== undefined) {
        return function () { return window.innerWidth; };
    }
    // IE8
    else {
        return function () { return document.body.offsetWidth; };
    }
}
//This function returns a function, so that a frequently calculated value 
//doesn't need to run through multiple IF tests every time it is needed
FormDivScrollHandler.getWindowHeightFunction = function () {
    if (window.innerHeight !== undefined) {
        return function () { return window.innerHeight; };
    }
    // IE8
    else {
        return function () { return document.body.offsetHeight; };
    }
}

FormDivScrollHandler.syncToolBar = function (checkDropDownMenu) {
    FormDivScrollHandler.rSyncToolBar(checkDropDownMenu);
}

//when scroll left/right or up/down of Form DIV, need sync the menu bar and dropdown menu if they are wider than window
FormDivScrollHandler.rSyncToolBar = function (checkDropDownMenu) {
    if (FormDivScrollHandler.getWindowHeight == null)
        FormDivScrollHandler.initializeFunctions();

    this.checkDropDownMenu = checkDropDownMenu;

    if (this.e1formDiv != null && this.menuBarTable != null) {
        //if scroll up down, need sync the submenu vertially if the submenu is longer than window
        if (this.prevScrollTop != this.e1formDiv.scrollTop && this.checkDropDownMenu) {
            FormDivScrollHandler.handleVerticalScroll();

        }
        if (this.prevScrollLeft != this.e1formDiv.scrollLeft) {
            FormDivScrollHandler.handleHorizontalScroll();
        }

        this.prevScrollLeft = this.e1formDiv.scrollLeft;
        this.prevScrollTop = this.e1formDiv.scrollTop;
    }
}

FormDivScrollHandler.handleHScrollFunc = function () {
    if (this.isRTL) {
        return function () { FormDivScrollHandler.rhandleHorizontalScroll_RTL(); };
    }
    else {
        return function () { FormDivScrollHandler.rhandleHorizontalScroll(); };
    }
}

//when scroll up/down of Form DIV, need sync the dropdown menu if they are longer than window
FormDivScrollHandler.handleVerticalScroll = function () {
    //dropdown sublevel menu from form exits, row exits
    if (jdeWebGUIcurrentSubMenu != null) {
        FormDivScrollHandler.syncDropDownMenuVert(jdeWebGUIcurrentSubMenu);
    }
    else if (jdeWebGUIcurrentMenu != null) //dropdown toplevel formexits/row exits menu
    {
        FormDivScrollHandler.syncDropDownMenuVert(jdeWebGUIcurrentMenu);
    }
}

FormDivScrollHandler.getDropDownMenuWidth = function (dropdownMenu) {
    var dropDownMenuWidth = 0;
    var dropdownLeftNum = 0;
    dropdownLeftNum = getAbsoluteLeftPos(dropdownMenu, false);
    dropDownMenuWidth = dropdownLeftNum + dropdownMenu.offsetWidth;
    return dropDownMenuWidth;
}

FormDivScrollHandler.getMenuWidth = function () {
    var menuWidth = this.menuBarTable.offsetWidth;
    var dropDownMenuAdj = 90;
    var dropDownMenuWidth = 0;

    if (this.checkDropDownMenu) {
        //dropdown sub menu - form exits, row exits
        if (jdeWebGUIcurrentSubMenu != null) {
            dropDownMenuWidth = FormDivScrollHandler.getDropDownMenuWidth(jdeWebGUIcurrentSubMenu);
        }
        //dropdown menu - eg. tools exit menu
        else if (jdeWebGUIcurrentMenu != null) {
            dropDownMenuWidth = FormDivScrollHandler.getDropDownMenuWidth(jdeWebGUIcurrentMenu);
        }
    }

    menuWidth = (menuWidth + dropDownMenuAdj);
    if (dropDownMenuWidth > menuWidth)
        menuWidth = dropDownMenuWidth;

    return menuWidth;
}

//when scroll left/right of Form DIV, need sync the menu bar and dropdown menu if they are wider than window
FormDivScrollHandler.rhandleHorizontalScroll = function () {
    var menuWidth = FormDivScrollHandler.getMenuWidth();
    var menuLeftNum = this.menuBarTable.offsetLeft;
    var windowWidth = FormDivScrollHandler.getWindowWidth();
    var scroll = menuWidth > (windowWidth - menuLeftNum);

    var formDivScrollLeft = this.e1formDiv.scrollLeft;
    var scrollDis = this.prevScrollLeft - formDivScrollLeft;
    var menuWinDif = windowWidth - menuWidth;
    if ((formDivScrollLeft == 0 && this.menuBarTable.offsetLeft < 0)//scrollbar back to the most left
        || (scrollDis > (-this.menuBarTable.offsetLeft))) {
        FormDivScrollHandler.resetMenuLeft();
    }
    else {
        var rightToMenuBarRightBound = (formDivScrollLeft + windowWidth - menuLeftNum) > menuWidth;
        //scrollbar is right to the right boundary of the menu bar and menu is wider than window        
        if (!scroll && (menuWinDif < 0) && rightToMenuBarRightBound) {
            if (scrollDis < 0 && this.menuBarTable.offsetLeft != menuWinDif) //scroll to right
                this.menuBarTable.style.left = menuWinDif + "px";
            else //scroll to left
            {
                this.menuBarTable.style.left = this.menuBarTable.offsetLeft + scrollDis + "px";
            }
        }
        else if (scroll || this.menuBarTable.offsetLeft < 0) {
            if (scrollDis < 0 && (scrollDis < menuWinDif)) //scroll to right
            {
                this.menuBarTable.style.left = menuWinDif + "px";
            }
            else {
                //scrollbar is between the right and left boundary of menubar
                this.menuBarTable.style.left = this.menuBarTable.offsetLeft + scrollDis + "px";
            }
        }

    }
}

FormDivScrollHandler.resetMenuLeft = function () {
    this.menuBarTable.style.left = 0 + "px";
}

FormDivScrollHandler.repositionMenuLeft = function () {
    if (FormDivScrollHandler.getWindowHeight == null)
        FormDivScrollHandler.initializeFunctions(false);
    //when load the form, do not need check the menu table
    if (this.menuBarTable != null) {
        var windowWidth = FormDivScrollHandler.getWindowWidth();
        if (windowWidth > this.menuBarTable.offsetWidth) {
            FormDivScrollHandler.resetMenuLeft();
        }
    }
}

//when scroll left/right of Form DIV, need sync the menu bar and dropdown menu if they are wider than window
FormDivScrollHandler.rhandleHorizontalScroll_RTL = function () {
    var menuWidth = FormDivScrollHandler.getMenuWidth();
    var menuLeftNum = this.menuBarTable.offsetLeft;

    var windowWidth = FormDivScrollHandler.getWindowWidth();
    var scroll = menuWidth > (windowWidth + menuLeftNum);
    var formDivScrollLeft = this.e1formDiv.scrollLeft;

    var menuWinDif = menuWidth - windowWidth;
    var scrollDis = this.prevScrollLeft - formDivScrollLeft;
    var leftToMenuBarLeftBound = (scrollDis + menuLeftNum) > menuWinDif;
    if (scroll && scrollDis > 0) //scrolling to left
    {
        if (formDivScrollLeft == 0 || leftToMenuBarLeftBound)
        //the scrollbar is at the max left position || left outside the left bound of menubar
            this.menuBarTable.style.left = menuWinDif + "px";
        else if (scrollDis > 0)
            this.menuBarTable.style.left = scrollDis + "px";
    }
    else if (scrollDis < 0 && menuWinDif > 0) //scrolling to right
    {
        this.menuBarTable.style.left = scrollDis + "px";
        if (this.menuBarTable.offsetLeft <= 1)
            FormDivScrollHandler.resetMenuLeft();
    }
}
//if scroll up/down, need sync the submenu vertially if the submenu is longer than window
FormDivScrollHandler.syncDropDownMenuVert = function (currentSubMenu) {
    var dropDownMenuHeight = currentSubMenu.offsetTop + currentSubMenu.offsetHeight;
    var scrollDis = this.e1formDiv.scrollTop - this.prevScrollTop;

    if (!currentSubMenu.dropDownMenuVertMoved)
        currentSubMenu.orgTop = currentSubMenu.offsetTop;

    //scrolling down and dropdown menu is longer than window    
    if ((dropDownMenuHeight > (FormDivScrollHandler.getWindowHeight() - 20)) && (scrollDis > 0)) {
        currentSubMenu.style.top = currentSubMenu.offsetTop - scrollDis + "px";
        moveItemShadow(currentSubMenu);
        currentSubMenu.dropDownMenuVertMoved = true;
    }
    //scrolling up, scroll the dropdown menu back
    else if (currentSubMenu.dropDownMenuVertMoved && (scrollDis < 0)) {
        if (currentSubMenu.offsetTop < currentSubMenu.orgTop)//dropdown menu moved up
        {
            if (scrollDis < (currentSubMenu.offsetTop - currentSubMenu.orgTop))
                currentSubMenu.style.top = currentSubMenu.orgTop + "px";
            else
                currentSubMenu.style.top = currentSubMenu.offsetTop - scrollDis + "px";
            moveItemShadow(currentSubMenu);
        }
    }

}

FormDivScrollHandler.resetDropDownMenuVert = function () {
    if (this.checkDropDownMenu) {
        if (jdeWebGUIcurrentSubMenu != null) {
            FormDivScrollHandler.resetDropDownMenuV(jdeWebGUIcurrentSubMenu);
        }
        else if (jdeWebGUIcurrentMenu != null) //dropdown toplevel formexits/row exits menu
        {
            FormDivScrollHandler.resetDropDownMenuV(jdeWebGUIcurrentMenu);
        }
    }
}
FormDivScrollHandler.resetDropDownMenuV = function (currentSubMenu) {
    if (currentSubMenu.dropDownMenuVertMoved && (currentSubMenu.offsetTop != currentSubMenu.orgTop)) {
        currentSubMenu.style.top = currentSubMenu.orgTop + "px";
        moveItemShadow(currentSubMenu);
    }

    var windowWidth = FormDivScrollHandler.getWindowWidth();
    var windowHeight = FormDivScrollHandler.getWindowHeight();
    if (((currentSubMenu.offsetTop + currentSubMenu.offsetHeight) > windowHeight)
       || ((currentSubMenu.offsetLeft + currentSubMenu.offsetWidth) > windowWidth))
        document.body.style.overflow = "auto";
}

function htmlEncode(s) {
    var encodeCharSet = Array("&", "<", ">", '"', "'", "%", "(", ")", "+", "-");
    var htmlCharSet = Array("&#38;", "&lt;", "&gt;", "&quot;", "&#39;", "&#37;", "&#40;", "&#41;", "&#43;", "&#45;");

    if (s != null) {
        for (var i = 0; i < encodeCharSet.length; i++) {
            var encodeChar = encodeCharSet[i];
            //escape the special punctuation char in Regular Expression
            if (encodeChar == "(" || encodeChar == ")" || encodeChar == "+") {
                encodeChar = "\\" + encodeChar;
            }

            var encodeCharExp = new RegExp("[" + encodeChar + "]", "g");
            var htmlChar = htmlCharSet[i];
            s = s.replace(encodeCharExp, htmlChar);
        }
    }
    return s;
}

function removeDuplicationFromAction(formName) {
    var action = document.forms[formName].action;
    //get the query string from form action.
    var query = action.substr(action.indexOf('?') + 1);
    var newquery = '';
    //Loop through the parameter list and build a newquery with only those parameters that are not available alerady in the form fields.
    while (query != '') {
        //get the name of each parameter in the query string
        var name = query.substring(0, query.indexOf('='));
        //If a field is not available in the form with same name, add it to the newquery
        if (!document.forms[formName].elements[name]) {
            //If we are past all the parameters simple add the rest of the querystring.
            if (query.indexOf('&') == -1) {
                newquery = newquery + query;
            }
            //Otherwise add the next parameter name=value pair to newquery
            else {
                newquery = newquery + query.substring(0, query.indexOf('&') + 1);
            }
        }
        //Remove the parsed parameter from the querystring
        if (query.indexOf('&') == -1) {
            query = '';
        }
        else {
            query = query.substring(query.indexOf('&') + 1);
        }
    }
    //Build the action url with the newquery
    action = action.substring(0, action.indexOf('?') + 1) + newquery;
    //If there is an '&' at the end of the new querystring remove it
    action = action.replace(/&$/, '');

    //Set the new action url to the form's action
    document.forms[formName].action = action;
}

function printPage() {
    document.body.style.overflow = 'auto'; //Browser scrollbars are shown when data goes beyond a page.
    if (document.getElementById('e1formDiv')) {
        document.getElementById('e1formDiv').style.overflow = 'visible';  //Overflow is now not clipped
    }
    window.print(); //Finally print
}

function printNewWindow(id, title) {
    var printData = document.getElementById(id);
    var div = document.createElement('div');
    div.appendChild(printData.cloneNode(true));
    var newWin = window.open("", "PrintPage", "menubar=1,resizable=1,status=1,scrollbars=1");
    newWin.document.write('<html><head><title>' + title + '</title>');
    newWin.document.write('<link type="text/css" rel="stylesheet" href="/jde/share/css/webclient.jsp">');
    newWin.document.write('<link type="text/css" rel="stylesheet" href="/jde/share/css/webclient.css">');
    newWin.document.write(div.innerHTML);
    newWin.document.write('</body></html>');
    newWin.document.close();
    newWin.print();
}

// define a type code for all 9.1 projects
var ADVANCED_QUERY = 1;
var SHOW_POPUP = 2;
var TYPE_AHEAD = 3;
var ADVANCED_VA = 4;
var FREEZE_COL = 5;
var SHOW_POPUP_FAV = 6;
var SHOW_UI_PREF = 7;
var UNHIDE_COL = 8;
var MSG_MODAL = 9;
var INYFE = 10;
var UDO = 11;
var SHOW_HOVER = 12;
var ENHANCED_ATTACHMENT = 14;

// The function is reused by web runtime 9.1 projects. 
// create a popup floatting resizable and movable div with customized content.
// you can use pre-defined method resize, setContent, display, hide, moveTo to manuplate the poup
// you can also define your own event listener for this control for your type of popup
// pass in false for hasClose if you do not need titlebar */
function createPopup(id, type, hasClose, autoHide, autoCenter, resizeable, titleText, titleIcon, aboutText, helpText, contextID, wideContextID, left, top, width, height, ctrl, htmlContent, eventListener, namespace, depth, serverCall, maximized, urlParams) {
    if (hasClose) // only when the hasClose is true, we need to show the title bar, otherwise, only show the custmoized content from the caller
    {
        var titleInfo = { titleIcon: titleIcon, // the icon show up on the left top conner of the window
            titleText: titleText, // text of title
            titleTextColor: "#003D5B", // text color of title
            hasClose: hasClose, // show the close icon on the right top conner of the window
            aboutText: aboutText, //the about text
            helpText: helpText, //help text
            contextID: contextID,
            wideContextID: wideContextID
        }
    }
    else {
        var titleInfo = null; // do not show title bar at all.
    }
    if (type == ADVANCED_VA && !document.all)
        resizeable = false;
    var popupWin = new PopupWindow(id, resizeable, titleInfo, eventListener, depth, serverCall, type);
    var popupWinId = (depth == null || type == SHOW_HOVER) ? id : id + depth;
    if (type == ADVANCED_VA) {
        popupWin.modalMaximized = maximized;
    }
    if (type == ADVANCED_VA || type == SHOW_HOVER || type == UDO) {
        popupWin.setMinSize(parseFloat(width), parseFloat(height));
    }
    popupWin.focusedControl = ctrl; // set the reference control int popup window, so it can be adjusted around this control later
    popupWin.type = type; // there is internal implementation difference when type is different
    popupWin.autoCenter = autoCenter; // allow the popup auto adjusted to the center based on the reference control
    popupWin.autoHide = autoHide // allow the popup hide automatically when the mouse move away from the popup
    popupWin.setContent(htmlContent); //setup inital dom struture
    popupWin.setNameSpace(namespace);
    popupWin.display(left, top, width, height); // visualize the popup window
    if (!serverCall && (type == ADVANCED_VA || type == SHOW_HOVER)) {
        //        urlParams : e.jdemafjasUID, e.jdemafjasComponent, e.jdemafjasLauncher, e.jdemafjascacheUID, e.RID, e.formonlyindex, e.dummyparam
        var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
        e1UrlCreator.setURI("E1Client");
        e1UrlCreator.setParameter("jdemafjasUID", urlParams[0]);
        e1UrlCreator.setParameter("jdemafjasComponent", urlParams[1]);
        e1UrlCreator.setParameter("jdemafjasLauncher", urlParams[2]);
        e1UrlCreator.setParameter("jdemafjascacheUID", urlParams[3]);
        e1UrlCreator.setParameter("RID", urlParams[4]);
        e1UrlCreator.setParameter("formonlyindex", urlParams[5]);
        e1UrlCreator.setParameter("dummyparam", urlParams[6]);
        //        alert(e1UrlCreator.toString());
        if (depth > 1) {
            if (type == SHOW_HOVER) {
                parent.document.getElementById("HoverFrame").setAttribute('src', e1UrlCreator.toString());
            }
            else {
                parent.document.getElementById("modalIframe" + depth).setAttribute('src', e1UrlCreator.toString());
            }
        }
        else {
            if (type == SHOW_HOVER) {
                document.getElementById("HoverFrame").setAttribute('src', e1UrlCreator.toString());
            }
            else {
                document.getElementById("modalIframe" + depth).setAttribute('src', e1UrlCreator.toString());
            }
        }
    }

    return popupWin; // the caller need to keep track the reference of the popwin object
}
//consolidate all the client functions for the server, this includes resolves the focused control, createpopup and disable MO
//signature is almost the same as createpop, the only difference is ctrlID is passed in and the DOM element is resolved based on the ctrlID and eventually call createpopup
function renderModalFormAppStack(id, type, hasClose, autoHide, autoCenter, resizeable, titleText, titleIcon, aboutText, helpText, contextID, wideContextID, left, top, width, height, ctrlID, htmlContent, eventListener, namespace, depth, serverCall, maximized) {
    var focusedCtrl = null;
    if (ctrlID != "null") {
        focusedCtrl = findFocusedControlForModalForm(ctrlID, depth, namespace, serverCall);
        if (type == SHOW_HOVER && ctrlID.charAt(0) !== 'C')//for hover form, need to get the parent element for grid cell, IE doesn't support startsWith
        {
            focusedCtrl = focusedCtrl.parentElement;
        }
    }
    createPopup(id, type, hasClose, autoHide, autoCenter, resizeable, HTMLDecoder(titleText), titleIcon, aboutText, helpText, contextID, wideContextID, left, top, width, height, focusedCtrl, htmlContent, eventListener, namespace, depth, serverCall, maximized);
    var ownerWindow = null;
    if (depth > 1) {
        ownerWindow = document.getElementById("modalIframe" + (depth - 1)).contentWindow;
    }
}
function updateModalCloseOnForm(ctrlID, depth, namespace, serverCall) {
    if (ctrlID.indexOf(".") != -1)//this is a grid
    {
        var index1 = ctrlID.indexOf(".");
        var index2 = ctrlID.lastIndexOf(".");
        var gridID = ctrlID.substring(0, index1);
        var row = parseInt(ctrlID.substring(index1 + 1, index2));
        var col = parseInt(ctrlID.substring(index2 + 1));
        var gridObj = null;
        if (depth == 1) {
            gridObj = window.JDEDTAFactory.getInstance(namespace).resolveGrid(gridID);
        }
        else {
            if (serverCall) {
                var ownerWindow = document.getElementById("modalIframe" + (depth - 1)).contentWindow;
            }
            else {
                var ownerWindow = window.parent.document.getElementById("modalIframe" + (depth - 1)).contentWindow;
            }
            gridObj = ownerWindow.JDEDTAFactory.getInstance(namespace).resolveGrid(gridID);
        }
        gridObj.focusManager.callBackFocus = true;
    }
    else//FC
    {
        FORMTOUCH.callBackFocus = true;
    }
}
function findFocusedControlForModalForm(ctrlID, depth, namespace, serverCall) {
    var focusedCtrl = null;
    if (ctrlID.indexOf(".") != -1)//this is a grid
    {
        var index1 = ctrlID.indexOf(".");
        var index2 = ctrlID.lastIndexOf(".");
        var gridID = ctrlID.substring(0, index1);
        var row = parseInt(ctrlID.substring(index1 + 1, index2));
        var col = parseInt(ctrlID.substring(index2 + 1));
        var gridObj = null;
        if (depth == 1) {
            gridObj = window.JDEDTAFactory.getInstance(namespace).resolveGrid(gridID);
        }
        else {
            if (serverCall) {
                var ownerWindow = document.getElementById("modalIframe" + (depth - 1)).contentWindow;
            }
            else {
                var ownerWindow = window.parent.document.getElementById("modalIframe" + (depth - 1)).contentWindow;
            }
            gridObj = ownerWindow.JDEDTAFactory.getInstance(namespace).resolveGrid(gridID);
        }
        if (row == -1)//it's a qbe
        {
            focusedCtrl = gridObj.getQBECellElem(col);
        }
        else {
            focusedCtrl = gridObj.getDataCellElem(row, col);
        }
    }
    else//FC
    {
        if (depth == 1) {
            focusedCtrl = document.getElementById(ctrlID);
        }
        else {
            if (serverCall) {
                focusedCtrl = document.getElementById("modalIframe" + (depth - 1)).contentWindow.document.getElementById(ctrlID);
            }
            else {
                focusedCtrl = window.parent.document.getElementById("modalIframe" + (depth - 1)).contentWindow.document.getElementById(ctrlID);
            }
        }
    }
    return focusedCtrl;
}
function findNativeContainerUtils() {
    /*
    * Traverse up the window hierarchy searching for the NativeContainer object/library.
    */
    var parentWindow = null;

    if (window) {
        parentWindow = window.parent;
    }

    while (parentWindow && !parentWindow.NativeContainer) {
        if (parentWindow != parentWindow.parent) {
            parentWindow = parentWindow.parent;
        }
        else {
            // No NativeContainer found.
            parentWindow = null;
            break;
        }
    }

    return parentWindow;
}
function toggleHelpOptionsSubmenu(uniqueId, namespace) {
    var submenuId = "helpOptionsSubmenuId";
    var clickCaptureDivId = "helpOptionsClickCaptureDivId";

    if (namespace) {
        submenuId = "helpOptionsSubmenuId" + namespace;
        clickCaptureDivId = "helpOptionsClickCaptureDivId" + namespace;
    }

    /*
    * The uniqueId accounts for multiple help options boxes appearing on a single application,
    * e.g., in a dialog popup box and on the main form.
    */
    if (uniqueId && !isNaN(uniqueId)) {
        submenuId = submenuId + uniqueId;
        clickCaptureDivId = clickCaptureDivId + uniqueId;
    }

    var submenu = document.getElementById(submenuId);
    var clickCaptureDiv = document.getElementById(clickCaptureDivId);
    toggleHelpOptionsElement(submenu, clickCaptureDiv, !namespace ? "" : namespace, uniqueId);
}

function toggleHelpOptionsElement(submenu, clickCaptureDiv, namespace, uniqueId) {
    if (submenu) {
        var currentState = submenu.getAttribute("currentState");

        if (currentState && currentState == "visible") {
            submenu.style["display"] = "none";
            submenu.setAttribute("currentState", "collapsed");
            clickCaptureDiv.style["display"] = "none";
            clickCaptureDiv.setAttribute("currentState", "collapsed");
        }
        else {
            submenu.style["display"] = "inline";
            submenu.setAttribute("currentState", "visible");
            clickCaptureDiv.style["display"] = "inline";
            clickCaptureDiv.setAttribute("currentState", "visible");

            var helpBubbleTip = document.getElementById("helpBubbleTip" + namespace + uniqueId);
            var helpOptionImage = document.getElementById("helpOptionImage" + namespace + uniqueId);

            var imageLeft = getAbsoluteLeftPos(helpOptionImage);
            var imageTop = getAbsoluteTopPos(helpOptionImage);
            var imageHeight = helpOptionImage.offsetHeight;
            var imageWidth = helpOptionImage.offsetWidth;
            var imageLeft = getAbsoluteLeftPos(helpOptionImage);
            var formWidth = document.body.offsetWidth;

            if (isRTL) {
                submenu.style.left = "1px";
                if (helpBubbleTip) {
                    var captureDivLeftOffset = getAbsoluteLeftPos(clickCaptureDiv);
                    helpBubbleTip.style.left = Math.max(helpBubbleTip.offsetWidth / 2, imageLeft + imageWidth / 2 - helpBubbleTip.offsetWidth / 2) - captureDivLeftOffset + "px";
                }
            }
            else {
                submenu.style.right = "1px";
                if (helpBubbleTip) {
                    var captureDivRightOffset = formWidth - getAbsoluteLeftPos(clickCaptureDiv) - clickCaptureDiv.offsetWidth; // for popup VA, the right edge of popup to the right edge of form need to be discount from caculation.
                    helpBubbleTip.style.right = Math.max(helpBubbleTip.offsetWidth / 2, formWidth - captureDivRightOffset - imageLeft - imageWidth / 2 - helpBubbleTip.offsetWidth / 2) + "px"; // we do not want extra scrollbar show up if there is no enough space in FDA poralet
                }
            }

            // for WPS FDA portalet, it is not rendered in a iframe, but part of main window, we have to deal with it specially do get it to the correct position for all t
            var isWPSNoFrameMode = window.JDEDTAFactory && JSCompMgr.checkForIBMPortal(JDEDTAFactory.getFirstNamespace()) && top == window;
            if (isWPSNoFrameMode) {
                if (helpBubbleTip)
                    helpBubbleTip.style.top = imageTop + imageHeight + 10 + "px";

                submenu.style.top = imageTop + imageHeight + 20 + "px";
                submenu.style.right = null;
                submenu.style.left = imageLeft - submenu.offsetWidth + 30 + "px";
                var helpOptionsClickCaptureDiv = document.getElementById("helpOptionsClickCaptureDivId" + namespace);
                helpOptionsClickCaptureDiv.style.top = "0px";
            }
        }
    }
}

FCHandler.onMouseDownTypeAheadIcon = function () {
    typeAheadData.setMouseInFlag();
}

FCHandler.onTypeAheadRowClick = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadRowClick(ctrl, namespace, e);
}

FCHandler.onTypeAheadPreviousPageClick = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadPreviousPageClick(ctrl, namespace, e);
}

FCHandler.onTypeAheadPreviousPageMouseOver = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadPreviousPageMouseOver(ctrl, namespace, e);
}

FCHandler.onTypeAheadPreviousPageMouseOut = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadPreviousPageMouseOver(ctrl, namespace, e);
}

FCHandler.onTypeAheadNextPageClick = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadNextPageClick(ctrl, namespace, e);
}

FCHandler.onTypeAheadNextPageMouseOver = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadNextPageMouseOver(ctrl, namespace, e);
}

FCHandler.onTypeAheadNextPageMouseOut = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadNextPageMouseOut(ctrl, namespace, e);
}

FCHandler.onTypeAheadVAClick = function (ctrl, namespace, e) {
    typeAheadData.clearMouseInFlag();
    typeAheadData.resetTypeAhead();
    JDEDTAFactory.getInstance(namespace).post(ctrl);
}

FCHandler.onTypeAheadVAMouseOver = function (ctrl, namespace, e) {
    typeAheadData.setMouseInFlag();
}

FCHandler.onTypeAheadVAMouseOut = function (ctrl, namespace, e) {
    typeAheadData.clearMouseInFlag();
}

FCHandler.onTypeAheadRowMouseOver = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadRowMouseOver(ctrl, namespace, e);
}

FCHandler.onTypeAheadRowMouseOut = function (ctrl, namespace, e) {
    typeAheadData.onTypeAheadRowMouseOut(ctrl, namespace, e);
}

FCHandler.onClickTypeAheadIcon = function (ctrl, grid, ctrlId, minCharNumber, hotKeyRequired, namespace, type) {
    typeAheadData.onClickTypeAheadIcon(ctrl, grid, ctrlId, minCharNumber, hotKeyRequired, namespace, type);
}

FCHandler.onKeyUpTextFieldTypeAhead = function (ctrl, namespace, hotKeyPressed) {

    if (typeAheadData.isTypeAheadWindowVisible()) {
        removeItemShadow(typeAheadData.typeAheadWindow.windowElem);
    }


    FCHandler.updateAutoSuggestIndicator(ctrl, ctrl.name, ctrl.getAttribute("minCharNumber"), ctrl.getAttribute("hotKey") == 1);
    clearTimeout(typeAheadData.timeoutIdForTypeAhead);

    if (ctrl.value.length >= ctrl.getAttribute("minCharNumber")) {

        if (ctrl.getAttribute("hotKey") == 1) {
            // hot key required
            if (hotKeyPressed) {
                // Manual search
                typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrl.name, namespace, ctrl.value, "ctrlAutoSuggest", 0) }, 1);
            }
        }
        else {
            var isDropDown = 1;
            if (ctrl.getAttribute("isDropdown") == 1)
                isDropDown = 2
            // Auto search
            typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrl.name, namespace, ctrl.value, "ctrlAutoSuggest", isDropDown) }, autoSuggestDelay);
        }
    }
    else if (ctrl.getAttribute("hotKey") == 0 || (ctrl.getAttribute("hotKey") == 1 && hotKeyPressed)) {
        if (typeAheadData.isTypeAheadWindowVisible()) {
            typeAheadData.resetTypeAheadInternal();
        }
    }
}


FCHandler.onKeyUpQBETypeAhead = function (ctrl, ctrlId, namespace, searchValue, hotKeyPressed, minCharNumber) {

    if (typeAheadData.isTypeAheadWindowVisible()) {
        removeItemShadow(typeAheadData.typeAheadWindow.windowElem);
    }

    if (searchValue.length >= minCharNumber) {
        clearTimeout(typeAheadData.timeoutIdForTypeAhead);

        if (hotKeyPressed == 0)
            typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrlId, namespace, searchValue, "qbeAutoSuggest", 0) }, 1);
        else
            typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrlId, namespace, searchValue, "qbeAutoSuggest", hotKeyPressed) }, autoSuggestDelay);
    }
    else if (typeAheadData.isTypeAheadWindowVisible()) {
        typeAheadData.resetTypeAheadInternal();
    }
}

FCHandler.onKeyUpCellTypeAhead = function (ctrl, ctrlId, namespace, searchValue, hotKeyPressed, minCharNumber) {

    if (typeAheadData.isTypeAheadWindowVisible()) {
        removeItemShadow(typeAheadData.typeAheadWindow.windowElem);
    }

    if (searchValue.length >= minCharNumber) {
        clearTimeout(typeAheadData.timeoutIdForTypeAhead);

        if (hotKeyPressed == 0)
            typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrlId, namespace, searchValue, "cellAutoSuggest", 0) }, 1);
        else
            typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrlId, namespace, searchValue, "cellAutoSuggest", hotKeyPressed) }, autoSuggestDelay);
    }
    else if (typeAheadData.isTypeAheadWindowVisible()) {
        typeAheadData.resetTypeAheadInternal();
    }
}


var typeAheadData = new TypeAheadData();

FCHandler.startTypeAhead = function (ctrl, ctrlId, namespace, searchValue, eventName, isAutoSearch, eventSource) {
    if (isAutoSearch == 2)
        typeAheadData.setIsDropDown();
    typeAheadData.setNamespace(namespace);
    typeAheadData.determineNewTypeAheadFetch(ctrl, ctrlId, namespace, searchValue, eventName, isAutoSearch, eventSource);
}

function TypeAheadData() {
    this.isRTL = isRTL;

    // The search text - is used to fetch rows from server
    this.searchedText = null;

    this.searchedRowIndex = -1;
    this.searchedColIndex = -1;

    // The text is searching on.  It is different than this.searchedText.  this.searchedText is set when the result is returned from server. 
    // this.searchingText is the text sending to server
    this.searchingText = null;
    // Next Page Id. Default is -1 (no next page)
    this.nextPageId = -1;
    // Current value in the text field or cell - this value is not always the same as this.searchedText
    this.currentValue = null;
    // The owning textfield in case of TextField, the input textfield for QBE or the Grid object for grid
    this.parentControl = null;
    // Ctrl ID
    this.ctrlId = null;
    // eventName:  "ctrlAutoSuggest"-Text field; "qbeAutoSuggest"-QBE Cell; "cellAutoSuggest"-Grid Cell 
    this.eventName = null;
    // Column Title
    this.columnTitlesArray = new Array();
    // Column Width
    this.columnWidthArray = new Array();
    // Fetched data
    this.modelDataRowsArray = new Array();
    // Contain the row index in modelDataRowsArray.  These rows will be displayed
    this.displayRowIndexArray = new Array();
    // Current row - the index of displayRowIndexArray
    this.currentRowIndex = 0;

    // Total page number
    this.totalPageNumber = 0;
    // Current page number, starting with 0
    this.currentPageIndex = -1;

    // The popup window 
    this.typeAheadWindow = null;
    // Interval handle
    this.timeoutIdForTypeAhead = null;

    // The search columns - true or false
    // this.searchColumnIndexs = new Array();
    this.searchColumnsArray = new Array();

    this.mouseInFlag = 0;

    //this is a dropdown
    this.isDropDown = false;

    //namespace
    this.namespace = null;

    //data selected from type ahead dropdown, this is used for grid cell change exclusively and can only be cleared from there
    this.dataSelected = false;
    var userAgent = navigator.userAgent.toUpperCase();
    this.isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
    // Not using "touchEnabled" as specific Android processing may be warranted in the dropdown.
    this.isAndroid = (userAgent.indexOf("ANDROID") > -1);
}

TypeAheadData.prototype.setMouseInFlag = function () {
    this.mouseInFlag = 1;
}

TypeAheadData.prototype.clearMouseInFlag = function () {
    this.mouseInFlag = 0;
}

TypeAheadData.prototype.setDataSelected = function () {
    this.dataSelected = true;
}

TypeAheadData.prototype.clearDataSelected = function () {
    this.dataSelected = false;
}

TypeAheadData.prototype.resetTypeAhead = function () {
    clearTimeout(typeAheadData.timeoutIdForTypeAhead);
    this.timeoutIdForTypeAhead = setTimeout(function () { TypeAheadData.prototype.resetTypeAheadInternal() }, 100);
    this.resetTypeAheadInternal();
}

// Need to be called when the control or QBE or grid cell is exited
TypeAheadData.prototype.resetTypeAheadInternal = function () {
    this.parentControl = null;
    this.ctrlId = null;
    this.eventName = null;
    this.searchingText = null;
    this.searchedText = null;
    this.searchedRowIndex = -1;
    this.searchedColIndex = -1;
    this.mouseInFlag = 0;

    this.displayRowIndexArray = new Array();
    this.totalPageNumber = 0;
    this.currentPageIndex = -1;
    this.currentRowIndex = 0;

    if (this.typeAheadWindow) {
        this.typeAheadWindow.hide();
    }

    this.isDropDown = false;
}

TypeAheadData.prototype.setIsDropDown = function () {
    this.isDropDown = true;
}

TypeAheadData.prototype.setNamespace = function (namespace) {
    this.namespace = namespace;
}

TypeAheadData.prototype.determineNewTypeAheadFetch = function (ctrl, ctrlId, namespace, enteredSearchText, eventName, isAutoSearch, eventSource) {
    this.currentValue = enteredSearchText;
    this.parentControl = ctrl;
    this.ctrlId = ctrlId;
    this.eventName = eventName;
    var trimString = enteredSearchText.Trim();
    //ctrlTitle is used by RUEI
    var ctrlTitle = ctrl.title;

    if (this.isDropDown && (trimString.length == 0 || trimString == "*")) {
        this.startNewTypeAheadFetch(trimString, ctrlId, eventName, namespace, isAutoSearch, eventSource, ctrlTitle);
    }
    else if (this.searchedText == null || this.searchedText.length == 0) {
        // This is the first time, do a fetch
        this.startNewTypeAheadFetch(enteredSearchText, ctrlId, eventName, namespace, isAutoSearch, eventSource, ctrlTitle);
    }
    else if (this.searchedText == enteredSearchText) {
        this.startNewTypeAheadFetch(enteredSearchText, ctrlId, eventName, namespace, isAutoSearch, eventSource, ctrlTitle);
    }
    else if (enteredSearchText.indexOf(this.searchedText) == 0) {
        if (this.needToFetchFromServer(enteredSearchText)) {
            this.startNewTypeAheadFetch(enteredSearchText, ctrlId, eventName, namespace, isAutoSearch, eventSource, ctrlTitle);
        }
        else {
            this.buildTypeAheadModelFromFetchData(enteredSearchText, ctrlId);
        }
    }
    else {
        // Do a new fetch because search text has been changed 
        this.startNewTypeAheadFetch(enteredSearchText, ctrlId, eventName, namespace, isAutoSearch, eventSource, ctrlTitle);
    }
}

TypeAheadData.prototype.getTypeAheadCurrentEditCell = function (grid, ctrlId) {
    // ctrlId is in the format of 'gce0_1.1.2'               
    var gridString = ctrlId.substring(3);
    var index1 = gridString.indexOf('.');
    var index2 = gridString.indexOf('.', index1 + 1);
    var rowIndex = parseInt(gridString.substring(index1 + 1, index2));
    var colIndex = parseInt(gridString.substring(index2 + 1));

    if (grid.focusManager.isCurrentlyFocusedCell(rowIndex, colIndex) || typeAheadData.isDropDown) {
        var cellElem = grid.getDataCellElem(rowIndex, colIndex);
        return cellElem;
    }

    return null;
}

TypeAheadData.prototype.needToFetchFromServer = function (enteredSearchText) {
    // Only all following conditions are met, then no need to fetch from server.  Need to rebuild the model from fetched data
    // 1. Search on single column
    // 2. The searchedText is the subset of current entered text
    // 3. current entered text is found in the fetched data
    // 4. the last record of the fetched data does not match the entered text or modelDataRowsArray is less than 300

    var searchedColumnIndex = -1;
    var searchColCount = 0;
    for (var i = 0; i < this.searchColumnsArray.length; i++) {
        if (this.searchColumnsArray[i] == 1) {
            searchColCount = searchColCount + 1;
            searchedColumnIndex = this.searchColumnsArray[i];
        }
    }

    if (searchColCount == 1) // Search on one column
    {
        if (enteredSearchText.indexOf(this.searchedText) == 0) {
            for (var i = 0; i < this.modelDataRowsArray.length; i++) {
                var value = this.modelDataRowsArray[i][searchedColumnIndex];
                if (value.indexOf(enteredSearchText) == 0) {
                    if (this.modelDataRowsArray.length < 300) {
                        return false;
                    }
                    else {
                        var lastValue = this.modelDataRowsArray[this.modelDataRowsArray.length - 1][searchedColumnIndex];
                        if (lastValue.indexOf(enteredSearchText) != 0) {
                            return false;
                        }
                    }
                }
            }
        }
    }

    return true;
}

TypeAheadData.prototype.buildTypeAheadModelFromFetchData = function (enteredSearchText, ctrlId) {
    var tempModelDataRowsArray = new Array();
    this.displayRowIndexArray = new Array();

    var searchedColumnIndex = -1;
    for (var i = 0; i < this.searchColumnsArray.length; i++) {
        if (this.searchColumnsArray[i] == 1) {
            searchedColumnIndex = this.searchColumnsArray[i];
        }
    }

    var j = 0;

    for (var i = 0; i < this.modelDataRowsArray.length; i++) {
        var value = this.modelDataRowsArray[i][searchedColumnIndex];
        if (value.indexOf(enteredSearchText) == 0) {
            tempModelDataRowsArray[j] = this.modelDataRowsArray[i];

            if (j < 10) {
                this.displayRowIndexArray[j] = j;
            }

            j++;

        }
    }

    this.modelDataRowsArray = tempModelDataRowsArray;

    this.searchedText = enteredSearchText;

    this.totalPageNumber = this.modelDataRowsArray.length / 10;
    this.currentPageIndex = 0;

    if (this.searchedRowIndex >= 0 && this.searchedColIndex >= 0) {
        // This is grid cell
        this.endNewTypeAheadFetch(this.parentControl.getDataCellElem(this.searchedRowIndex, this.searchedColIndex));
    }
    else if (this.searchedRowIndex == -1 && this.searchedColInde >= 0) {
        // This is QBE        
        this.endNewTypeAheadFetch(this.parentControl);
    }
    else {
        // This is a text field
        this.endNewTypeAheadFetch(this.parentControl);
    }
}
//ctrlTitle is used by RUEI
TypeAheadData.prototype.startNewTypeAheadFetch = function (searchText, ctrlId, eventName, mynamespace, isAutoSearch, eventSource, ctrlTitle) {
    if (typeAheadData.isTypeAheadWindowVisible()) {
        typeAheadData.typeAheadWindow.hide();
    }
    if (isAutoSearch == 1) {
        // Update the auto search animation icon
        var iconId = "ASIcon_" + ctrlId;
        var icon = document.getElementById(iconId);
        FCHandler.setAutoSuggestAutoSearchIcon(icon);
    }
    // Handle freeze column
    if (ctrlId.indexOf("fqbe") == 0) {
        ctrlId = ctrlId.substring(1);
    }
    // Call DTA to search
    if (eventSource && eventSource == "click") {
        ctrlId = ctrlId + ":ASImageClicked";
    }
    JDEDTAFactory.getInstance(mynamespace).addToAsynQueue(eventName, ctrlId, searchText, "", ctrlTitle);
    this.searchingText = searchText;

    JDEDTAFactory.getInstance(mynamespace).performAsynDataTransfer(this.NO_PROC_INDICATOR);
}

// This method should be called by DTA after 'Add Row Data'
TypeAheadData.prototype.endNewTypeAheadFetch = function (ctrl) {
    // Display the pop-up
    this.currentRowIndex = 0;
    this.displayTypeAhead(ctrl);
}

TypeAheadData.prototype.calculateTypeAheadWindowWidth = function () {
    var totalWidth = 0;
    for (var i = 0; i < this.columnWidthArray.length; i++) {
        totalWidth += this.columnWidthArray[i];
        totalWidth += 6;
    }
    return totalWidth;
}

TypeAheadData.prototype.calculateTypeAheadWindowHeight = function (isDropdown) {   /*This height is approximate as the rendered height of the table varies with browser. 
    The pop up window will be resized to excat size in PopupWindow's display function*/
    var height = 0;

    // Column Header
    if (!isDropdown) {
        //single row
        height = height + 23;
        //if the text is wrapped into two rows, need to resize
        for (var i = 0; i < this.columnTitlesArray.length; i++) {
            var title = this.columnTitlesArray[i];
            if (title.indexOf("<BR>") != -1) {
                height = height + 15;
                break;
            }
        }
    }

    // Data row
    height = height + this.displayRowIndexArray.length * 24;

    // next/previous page
    if (this.modelDataRowsArray.length > 10 || isDropdown) {
        height = height + 22;
    }

    return height;
}

TypeAheadData.prototype.onClickTypeAheadIcon = function (ctrl, grid, ctrlId, minCharNumber, hotKeyRequired, namespace, type) {
    this.clearMouseInFlag();

    if (hotKeyRequired != 2 || this.isIOS || this.isAndroid) {
        this.moveFocusBack(ctrl, null);
    }
    else {
        setTimeout(function () { TypeAheadData.prototype.moveFocusBack(ctrl, null); }, 10);
    }

    if (typeAheadData.isTypeAheadWindowVisible())
        typeAheadData.typeAheadWindow.hide();


    if (ctrl.value.length >= minCharNumber) {
        if (hotKeyRequired != 1) {
            clearTimeout(typeAheadData.timeoutIdForTypeAhead);
            if (type == "cellAutoSuggest") {
                typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(grid, ctrlId, namespace, ctrl.value, type, hotKeyRequired, "click") }, 1);
            }
            else {
                typeAheadData.timeoutIdForTypeAhead = setTimeout(function () { FCHandler.startTypeAhead(ctrl, ctrlId, namespace, ctrl.value, type, hotKeyRequired, "click") }, 1);
            }
        }
    }
}

TypeAheadData.prototype.pressPageUpKey = function (ctrl) {
    // Check whether there is more previous page
    if (this.hasPreviousPage())
        this.displayTypeAheadPreviousPage(ctrl);
}

TypeAheadData.prototype.pressPageDownKey = function (ctrl) {
    // Check whether there is more page
    if (this.hasNextPage())
        this.displayTypeAheadNextPage(ctrl);
}

TypeAheadData.prototype.pressUpKey = function (ctrl) {
    if (this.currentRowIndex == 0) {
        // Reach the first row of current page, check whether there is more previous page
        if (this.hasPreviousPage())
            this.displayTypeAheadPreviousPage(ctrl);
    }
    else {
        // Unselect the current seelcted row
        this.unselectCurrentRow();

        this.currentRowIndex--;

        // Select the new row
        this.selectCurrentRow();
    }
}

TypeAheadData.prototype.pressDownKey = function (ctrl) {
    if (this.currentRowIndex == this.displayRowIndexArray.length - 1) {
        // Reach the last row of current page, check whether there is more page
        if (this.hasNextPage())
            this.displayTypeAheadNextPage(ctrl);
    }
    else {
        // Unselect the current seelcted row
        this.unselectCurrentRow();

        this.currentRowIndex++;

        // Select the new row
        this.selectCurrentRow();
    }
}

TypeAheadData.prototype.onTypeAheadRowMouseOver = function (ctrl, namespace, e) {
    this.setMouseInFlag();
    // set current selected row
    if (this.currentRowIndex != ctrl.id.substring("typeAheadRow.".length)) {
        //window.status = window.status + "MouseOver;";    
        //alert(this.cuurentRowIndex + ", " + ctrl.id);
        this.unselectCurrentRow();
        this.currentRowIndex = ctrl.id.substring("typeAheadRow.".length);
        this.selectCurrentRow();
    }
}

TypeAheadData.prototype.onTypeAheadRowMouseOut = function (ctrl, namespace, e) {
    this.clearMouseInFlag();
}

TypeAheadData.prototype.moveFocusBack = function (ctrl, value) {
    var onFocusEventHandler = ctrl.onfocus;
    ctrl.onfocus = null;
    var onChangeEventHandler = ctrl.onchange;
    ctrl.onchange = null;

    ctrl.focus();
    if (value)
        ctrl.value = value;

    setTimeout(function () { ctrl.onfocus = onFocusEventHandler; }, 1000);
    setTimeout(function () { ctrl.onchange = onChangeEventHandler; }, 1000);
}

TypeAheadData.prototype.onTypeAheadRowClick = function (ctrl, namespace, e) {
    this.setDataSelected();
    this.clearMouseInFlag();
    if (this.eventName == "cellAutoSuggest") {
        var cellElem = this.getTypeAheadCurrentEditCell(this.parentControl, this.ctrlId);

        this.moveFocusBack(cellElem, typeAheadData.getSelectedValue());
    }
    else if (this.eventName == "openWatchlist") {
        var watchlistId = WL.getWatchlistIdFromRow(ctrl);

        WL.send("wlloadwatchlist." + watchlistId);
        return;
    }
    else {
        // Textfield or QBE
        this.moveFocusBack(this.parentControl, typeAheadData.getSelectedValue());
    }

    if (typeAheadData.isTypeAheadWindowVisible())
        typeAheadData.typeAheadWindow.hide();
}

TypeAheadData.prototype.onTypeAheadPreviousPageClick = function (ctrl, namespace, e) {
    this.clearMouseInFlag();
    if (this.eventName == "cellAutoSuggest") {
        var cellElem = this.getTypeAheadCurrentEditCell(this.parentControl, this.ctrlId);

        this.moveFocusBack(cellElem, null);
        if (this.hasPreviousPage())
            this.displayTypeAheadPreviousPage(cellElem);
    }
    else {
        // Textfield or QBE
        this.moveFocusBack(this.parentControl, null);
        if (this.hasPreviousPage())
            this.displayTypeAheadPreviousPage(this.parentControl);
    }
}

TypeAheadData.prototype.onTypeAheadPreviousPageMouseOver = function (ctrl, namespace, e) {

    this.setMouseInFlag();

    var moImageName = this.isRTL ? window["E1RES_img_AutoSuggest_previous_page_mo_rtl"] : window["E1RES_img_AutoSuggest_previous_page_mo"];
    ctrl.src = moImageName;
}

TypeAheadData.prototype.onTypeAheadPreviousPageMouseOut = function (ctrl, namespace, e) {

    this.clearMouseInFlag();

    var imageName = this.isRTL ? window["E1RES_img_AutoSuggest_previous_page_rtl"] : window["E1RES_img_AutoSuggest_previous_page"];
    ctrl.src = imageName;
}

TypeAheadData.prototype.onTypeAheadNextPageClick = function (ctrl, namespace, e) {
    this.clearMouseInFlag();
    if (this.eventName == "cellAutoSuggest") {
        var cellElem = this.getTypeAheadCurrentEditCell(this.parentControl, this.ctrlId);
        this.moveFocusBack(cellElem, null);
        if (this.hasNextPage())
            this.displayTypeAheadNextPage(cellElem);
    }
    else {
        // Textfield or QBE
        this.moveFocusBack(this.parentControl, null);
        if (this.hasNextPage())
            this.displayTypeAheadNextPage(this.parentControl);
    }
}

TypeAheadData.prototype.onTypeAheadNextPageMouseOver = function (ctrl, namespace, e) {

    this.setMouseInFlag();

    var moImageName = this.isRTL ? window["E1RES_img_AutoSuggest_next_page_mo_rtl"] : window["E1RES_img_AutoSuggest_next_page_mo"];
    ctrl.src = moImageName;
}

TypeAheadData.prototype.onTypeAheadNextPageMouseOut = function (ctrl, namespace, e) {

    this.clearMouseInFlag();

    var imageName = this.isRTL ? window["E1RES_img_AutoSuggest_next_page_rtl"] : window["E1RES_img_AutoSuggest_next_page"];
    ctrl.src = imageName;
}

TypeAheadData.prototype.displayTypeAheadPreviousPage = function (ctrl) {
    var firstRowOnCurrentPage = this.displayRowIndexArray[0];
    for (var i = 0; i < 10; i++) {
        this.displayRowIndexArray[i] = firstRowOnCurrentPage + i - 10;
    }

    this.currentPageIndex--;
    this.currentRowIndex = 9;
    if (ctrl.id == WL.ID_OPEN_ICON)
        WL.displayOpenDialog();
    else
        this.displayTypeAhead(ctrl);
}

TypeAheadData.prototype.displayTypeAheadNextPage = function (ctrl) {
    var lastRowOnCurrentPage = this.displayRowIndexArray[9];

    this.displayRowIndexArray = new Array();
    var j = 0;

    for (var i = lastRowOnCurrentPage + 1; i < this.modelDataRowsArray.length && j < 10; i++) {
        this.displayRowIndexArray[j] = i;
        j++;
    }

    this.currentPageIndex++;
    this.currentRowIndex = 0;
    if (ctrl.id == WL.ID_OPEN_ICON)
        WL.displayOpenDialog();
    else
        this.displayTypeAhead(ctrl);
}

TypeAheadData.prototype.unselectCurrentRow = function () {
    var currentRowElem = document.getElementById("typeAheadRow." + this.currentRowIndex);
    var currentCellElems = currentRowElem.childNodes;

    for (var i = 0; i < currentCellElems.length; i++) {
        var className = currentCellElems[i].className;
        currentCellElems[i].className = className.slice(0, currentCellElems[i].className.length - " selectedModifier".length);
    }
}

TypeAheadData.prototype.selectCurrentRow = function () {
    currentRowElem = document.getElementById("typeAheadRow." + this.currentRowIndex);
    currentCellElems = currentRowElem.childNodes;

    for (var i = 0; i < currentCellElems.length; i++) {
        currentCellElems[i].className = currentCellElems[i].className + " selectedModifier";
    }
}

TypeAheadData.prototype.getSelectedValue = function () {
    var currentModelRowIndex = this.displayRowIndexArray[this.currentRowIndex];
    var currentModelRow = this.modelDataRowsArray[currentModelRowIndex];
    return currentModelRow[0];
}

TypeAheadData.prototype.addTypeAheadColumnTitles = function (id, rowIndex, colIndex, columnTitles, rowCount, searchText, nextPageId, searchColumns, columnWidth) {
    // if the control has changed or the value in the control has changed, ignore it.
    if (this.checkTypeAheadControlMatch(id, rowIndex, colIndex, searchText) || typeAheadData.isDropDown) {
        if (rowCount > 0) {
            this.searchedText = searchText;
            this.columnTitlesArray = null;
            this.modelDataRowsArray = null;  // This is a array of array

            if (columnTitles != '' && columnTitles != null) {
                this.columnTitlesArray = columnTitles.split('\t');
            }

            if (searchColumns != '' && searchColumns != null) {
                this.searchColumnsArray = searchColumns.split('\t');
            }

            if (columnWidth != '' && columnWidth != null) {
                this.columnWidthArray = columnWidth.split('\t');
            }

            for (var i = 0; i < this.columnWidthArray.length; i++) {
                var ddSize = this.columnWidthArray[i];

                if (ddSize <= 6) {
                    this.columnWidthArray[i] = 60; // Min width
                }
                else if (ddSize <= 10) {
                    this.columnWidthArray[i] = this.columnWidthArray[i] * 9;
                }
                else if (ddSize <= 20) {
                    this.columnWidthArray[i] = this.columnWidthArray[i] * 8;
                }
                else if (ddSize <= 30) {
                    this.columnWidthArray[i] = 180;
                }
                else {
                    this.columnWidthArray[i] = 200;   // Max width
                }
            }
            this.modelDataRowsArray = new Array();

            this.nextPageId = nextPageId;
            this.resetDisplayRowIndexArray();
        }
        // AUTO SEARCH - B
        // Update Auto Search icon
        if (rowIndex >= 0 && colIndex >= 0) {
            // This is grid cell
            var grid = GCMH.findGrid(id);
            var colObj = grid.getColumnByColIndex(colIndex);
            if (colObj.isAutoSuggestEnabled == true && colObj.isAutoSuggestHotKeyRequired == false && colObj.isDropdownEnabled == false) {
                var iconId = "ASIcon_gce" + id + "." + rowIndex + "." + colIndex;
                var icon = document.getElementById(iconId);

                FCHandler.resetAutoSuggestAutoSearchIcon(icon);
            }
        }
        else if (rowIndex == -1 && colIndex >= 0) {
            // This is QBE           
            var grid = GCMH.findGrid(id);
            var colObj = grid.getColumnByColIndex(colIndex);
            if (colObj.isAutoSuggestEnabled == true && colObj.isAutoSuggestHotKeyRequired == false && colObj.isDropdownEnabled == false) {
                var iconId = "ASIcon_fqbe" + id + "." + colIndex;
                var icon = document.getElementById(iconId);
                if (!icon) {
                    iconId = "ASIcon_qbe" + id + "." + colIndex;
                    icon = document.getElementById(iconId);
                }
                FCHandler.resetAutoSuggestAutoSearchIcon(icon);
            }
        }
        else {
            // This is a text field
            var ctrl = document.getElementById("C" + id);
            if (ctrl.getAttribute("autoSuggestEnabled") == 1 && ctrl.getAttribute("hotKey") != 1 && ctrl.getAttribute("isDropdown") != 1) {
                var iconId = "ASIcon_" + id;
                var icon = document.getElementById(iconId);
                FCHandler.resetAutoSuggestAutoSearchIcon(icon);
            }
        }
    }
}

TypeAheadData.prototype.addTypeAheadModelDataRows = function (id, rowIndex, colIndex, dataRow, searchText) {
    // if the control has changed or the value in the control has changed, ignore it. 
    if (this.checkTypeAheadControlMatch(id, rowIndex, colIndex, searchText) || typeAheadData.isDropDown) {
        // ALl returned data is in one GUIChangeEvent in following format:  ROW1_COL1\tROW1_COL2\f\fROW2_COL11\tROW2_COL2\f\fROW3_COL1\tROW3_COL2 
        if (dataRow != '' && dataRow != null) {
            var tempArray = dataRow.split("\f\f");

            for (var i = 0; i < tempArray.length; i++) {
                var tempString = tempArray[i];

                this.modelDataRowsArray[i] = tempString.split('\t');

                // Add the first 10 rows to display
                if (i < 10) {
                    this.displayRowIndexArray[i] = i;
                }
            }

            this.totalPageNumber = this.modelDataRowsArray.length / 10;
            this.currentPageIndex = 0;
        }

        this.searchedRowIndex = rowIndex;
        this.searchedColIndex = colIndex;

        if (rowIndex >= 0 && colIndex >= 0) {
            // This is grid cell
            var grid = GCMH.findGrid(id);
            var cellElem = grid.getDataCellElem(rowIndex, colIndex);
            this.endNewTypeAheadFetch(cellElem);
        }
        else if (rowIndex == -1 && colIndex >= 0) {
            var grid = GCMH.findGrid(id);
            var isFrzQbeInEditGrid = true;
            var qbeId, cellElem;
            if (grid.isEditable && (grid.gridBack.scrollLeft <= 1 || grid.scrollLeft <= 1))
                isFrzQbeInEditGrid = false;
            // This is QBE
            if (isFrzQbeInEditGrid) {
                qbeId = "fqbe" + id + "." + colIndex;
                cellElem = document.getElementsByName(qbeId);
            }

            if (!cellElem || !cellElem[0]) {
                qbeId = "qbe" + id + "." + colIndex;
                cellElem = document.getElementsByName(qbeId);
            }
            this.endNewTypeAheadFetch(cellElem[0]);
        }
        else {
            // This is a text field
            var ctrl = document.getElementById("C" + id);
            this.endNewTypeAheadFetch(ctrl);
        }
    }
}

TypeAheadData.prototype.checkTypeAheadControlMatch = function (id, rowIndex, colIndex, searchText) {
    if (rowIndex >= 0 && colIndex >= 0) {
        // This is grid cell - then check whether the focused cell's row and col index matches
        var grid = GCMH.findGrid(id);

        // Check focused cell first
        if (this.parentControl == grid) {
            // Need to pass in visible(display) row and col index, NOT model row and col index
            // var visibleRowIndex = grid.getRowIndexForModelRow(rowIndex);
            // var visibleColIndex = grid.getRowIndexForModelRow = function(rowIndex);

            if (grid.focusManager.isCurrentlyFocusedCell(rowIndex, colIndex)) {
                // Then check the value
                var cellElem = grid.getDataCellElem(rowIndex, colIndex);
                if (cellElem.value == searchText)
                    return true;
            }
        }
    }
    else if (rowIndex == -1 && colIndex >= 0) {
        // This is a QBE        
        // This is grid cell       
        var searchedControl = document.getElementsByName("fqbe" + id + "." + colIndex);
        if (!searchedControl[0])
            searchedControl = document.getElementsByName("qbe" + id + "." + colIndex);

        if (this.parentControl == searchedControl[0] && this.parentControl.value == searchText) {
            return true;
        }
    }
    else {
        // This is text field
        var searchedControl = document.getElementById("C" + id);

        if (this.parentControl == searchedControl && this.parentControl.value == searchText) {
            return true;
        }
    }
    return false;
}

TypeAheadData.prototype.resetDisplayRowIndexArray = function () {

    this.displayRowIndexArray = new Array();

    for (var i = 0; i < this.displayRowIndexArray.length; i++) {
        this.displayRowIndexArray[i] = i;
    }
}

TypeAheadData.prototype.isTypeAheadWindowVisible = function () {
    if (typeAheadData.typeAheadWindow) {
        return (typeAheadData.typeAheadWindow.windowElem.style.display == "block");
    }

    return false;
}

TypeAheadData.prototype.hasPreviousPage = function () {
    if (this.currentPageIndex > 0)
        return true;
    else
        return false;
}

TypeAheadData.prototype.hasNextPage = function () {
    if ((this.currentPageIndex + 1) < this.totalPageNumber)
        return true;
    else
        return false;
}

// The 'ctrl' is a textfield, QBE and grid cell element
TypeAheadData.prototype.displayTypeAhead = function (ctrl) {
    var content = typeAheadData.buildTypeAheadHTML(ctrl);

    var winTop = getAbsoluteTopPos(ctrl, true);
    var winLeft = getAbsoluteLeftPos(ctrl, true);

    var winWidth = this.calculateTypeAheadWindowWidth();
    var winHeight = this.calculateTypeAheadWindowHeight(typeAheadData.isDropDown);

    if (!this.typeAheadWindow) {
        this.typeAheadWindow = createPopup("typeAheadWindow", TYPE_AHEAD, false, false, true, false, null, null, null, null, null, null, winLeft, winTop + ctrl.offsetHeight + 2, winWidth, winHeight, ctrl, content);
    }

    this.typeAheadWindow.hide();
    this.typeAheadWindow.setContent(content);
    this.typeAheadWindow.focusedControl = ctrl;

    this.typeAheadWindow.display(winLeft, winTop + ctrl.offsetHeight + 2, winWidth, winHeight);
}

TypeAheadData.prototype.buildTypeAheadHTML = function (ctrl) {
    var sb = new PSStringBuffer();
    var namespace = typeAheadData.namespace;
    var isDropDown = typeAheadData.isDropDown;
    var cmd = typeAheadData.ctrlId;
    if (cmd.indexOf("fqbe") != -1)
        cmd = cmd.substring(1);
    // Start here
    sb.append("<table id=\"typeAhead\" class=\"JSGridQTPClass e1container\" cellspacing=0 cellpadding=0>");
    sb.append("<tbody style=\"CURSOR: default;\">");
    // Start of Column Header
    sb.append("<tr>");
    sb.append("<td>");
    sb.append("<div style=\"WIDTH: ");

    var totalWidth = this.calculateTypeAheadWindowWidth();

    sb.append(totalWidth);
    sb.append("px");
    if (isDropDown)
        sb.append("; display:none");

    sb.append("\" class=\"JSGridHeaderBack JSGridRelative\">");

    sb.append("<table cellspacing=0 cellpadding=1>");

    sb.append("<tbody>");
    // Start of Column Header row
    sb.append("<tr id=\"typeAheadHeaderRow\">");

    // Render each Column Header cell
    for (var i = 0; i < this.columnTitlesArray.length; i++) {
        sb.append("<td style=\"POSITION: relative\" class=\"JSGridHeaderCell\" valign=\"bottom\" colindex=0>");
        sb.append("<nobr>");
        sb.append("<div style=\"WIDTH: ");
        sb.append(this.columnWidthArray[i]);
        sb.append("px; \">");
        sb.append("<table>");
        sb.append("<tbody>");
        sb.append("<tr>");
        sb.append("<td colindex=0><span>");
        sb.append(this.columnTitlesArray[i]);
        sb.append("</span></td>");
        sb.append("</tr>");
        sb.append("</tbody>");
        sb.append("</table>");
        sb.append("</div>");
        sb.append("</nobr>");
        //sb.append("<div style=\"FILTER: alpha(opacity=0); TOP: 0px; CURSOR: e-resize; RIGHT: 0px\" ></div>");
        //sb.append("<div style=\"FILTER: alpha(opacity=0); TOP: 0px; RIGHT: 0px; LEFT: 0px\" ></div>");
        sb.append("</td>");
    }

    sb.append("</tr>");
    // End of Column Header row
    sb.append("</tbody>");
    sb.append("</table>");
    sb.append("</div>");
    sb.append("</td>");
    sb.append("</tr>");
    // End of Column Header

    // Start of data
    sb.append("<tr>");
    sb.append("<td>");
    sb.append("<div style=\"WIDTH: ");
    sb.append(totalWidth);
    sb.append("px;\" id=\"typeAheadData\" class=\"JSGridBack JSGridRelative noHScrollModifier\">");
    sb.append("<div style=\"WIDTH: 822px; DISPLAY: none; HEIGHT: 0px\" id=\"jdeGridVirtualAbove0_1\" class=\"virtualVertical\"></div>");
    sb.append("<table cellspacing=0 cellpadding=0>");
    sb.append("<tbody>");
    sb.append("<tr>");
    sb.append("<td>");

    // Start of grid data section
    sb.append("<table id=\"typeAheadDataSection\" cellspacing=0 cellpadding=0>");
    sb.append("<tbody>");

    // Start of data row
    for (var i = 0; i < (this.displayRowIndexArray.length) && (this.displayRowIndexArray[i] != null); i++) {
        sb.append("<tr>");
        sb.append("<td>");

        sb.append("<table");
        sb.append(" border=0 cellspacing=0 cellpadding=1 ");
        if (i % 2 == 1) {
            sb.append("class='gridrowOdd'");
        }
        else {
            sb.append("class='gridrowEven'");
        }
        sb.append("><tbody>");

        // id of each row is 'typeAheadRow.0', 'typeAheadRow.1', etc.
        sb.append("<tr id=\"typeAheadRow.");
        sb.append(i);
        sb.append("\" onmouseover=\"FCHandler.onTypeAheadRowMouseOver(this, '', event);\"");
        sb.append(" onmouseout=\"FCHandler.onTypeAheadRowMouseOut(this, '', event);\"");
        sb.append(" onclick=\"FCHandler.onTypeAheadRowClick(this, '', event);\">");

        //strip off wildcard
        var currentValue = this.currentValue;
        var isBeginWith = true;
        if (currentValue.indexOf('*') != -1) {
            if (currentValue.indexOf('*') == 0)
                isBeginWith = false;
            currentValue = currentValue.replace(/\*/g, "");
        }

        // Go through each column to check whether there is a case-sensitive match
        var hasCaseSensitiveMatch = false;
        for (var k = 0; k < this.columnTitlesArray.length; k++) {
            var dataRowIndex = this.displayRowIndexArray[i];

            if (this.searchColumnsArray[k] == 1) {
                var value = this.modelDataRowsArray[dataRowIndex][k];
                if (isDropDown && k == 0)//for dropdown value column, data is always stored as upper case in DB
                {
                    if (value.toLowerCase().indexOf(currentValue.toLowerCase()) != -1) {
                        hasCaseSensitiveMatch = true;
                        break;
                    }
                }
                else {
                    if (value.indexOf(currentValue) != -1) {
                        hasCaseSensitiveMatch = true;
                        break;
                    }
                }
            }
        }

        for (var j = 0; j < this.columnTitlesArray.length; j++) {
            sb.append("<td class=\"JSGridCell textModifier");

            if (i == this.currentRowIndex) {
                sb.append(" selectedModifier");
            }
            sb.append("\" colindex=0>");
            sb.append("<div style=\"TEXT-ALIGN: left; WIDTH:");
            sb.append(this.columnWidthArray[j]);
            sb.append("px;\">");
            var dataRowIndex = this.displayRowIndexArray[i];

            // Bold and Underline the matched part in searched column
            if (this.searchColumnsArray[j] == 1) {
                var value = this.modelDataRowsArray[dataRowIndex][j];

                var matchPosition = -1;
                if (isDropDown && j == 0) {
                    matchPosition = value.toLowerCase().indexOf(currentValue.toLowerCase());
                }

                else {
                    if (hasCaseSensitiveMatch) {
                        matchPosition = value.indexOf(currentValue);
                    }
                    else {
                        matchPosition = value.toLowerCase().indexOf(currentValue.toLowerCase());
                    }
                }

                if ((matchPosition == 0 && isBeginWith) || (matchPosition != -1 && (!isBeginWith || (isDropDown && j == 0)))) {
                    var prePart = this.modelDataRowsArray[dataRowIndex][j].substring(0, matchPosition);
                    var matchedPart = this.modelDataRowsArray[dataRowIndex][j].substring(matchPosition, matchPosition + currentValue.length);
                    var remainPart = "";
                    if (this.modelDataRowsArray[dataRowIndex][j].length > matchPosition + currentValue.length) {
                        remainPart = this.modelDataRowsArray[dataRowIndex][j].substring(matchPosition + currentValue.length);
                    }
                    sb.append(prePart);
                    sb.append("<b><u>")
                    sb.append(matchedPart);
                    sb.append("</u></b>");
                    sb.append(remainPart);
                }
                else {
                    sb.append(this.modelDataRowsArray[dataRowIndex][j]);
                }
            }
            else {
                sb.append(this.modelDataRowsArray[dataRowIndex][j]);
            }
            sb.append("</div>");
            sb.append("</td>");
        }
        sb.append("</tr>");
        sb.append("</tbody>");
        sb.append("</table>");
        sb.append("</td>");
        sb.append("</tr>");
    }
    // End of data row

    // Previous/Next Page row
    if (this.modelDataRowsArray.length > 10 || isDropDown) {
        sb.append("<tr>");
        sb.append("<td class=\"JSGridHeaderCell\" cellPadding=0>");
        sb.append("<table style=\"width:100%\"><tbody><tr><td>");

        if (this.modelDataRowsArray.length > 10) {
            sb.append("<A style=\"TEXT-DECORATION: none;\" title=");
            sb.append(autoSuggestPreviousPage);
            sb.append(">");
            if (this.hasPreviousPage()) {
                if (this.isRTL) {
                    var moImageName = window["E1RES_img_AutoSuggest_previous_page_mo_rtl"];
                    var imageName = window["E1RES_img_AutoSuggest_previous_page_rtl"];
                }
                else {
                    var moImageName = window["E1RES_img_AutoSuggest_previous_page_mo"];
                    var imageName = window["E1RES_img_AutoSuggest_previous_page"];
                }

                sb.append("<IMG style=\"CURSOR: pointer\" id=\"typeAheadPreviousPage\"");
                sb.append(" onmouseover=\"FCHandler.onTypeAheadPreviousPageMouseOver(this, '', event);\"");
                sb.append(" onmouseout=\"FCHandler.onTypeAheadPreviousPageMouseOut(this, '', event);\"");
                sb.append(" onclick=\"FCHandler.onTypeAheadPreviousPageClick(this, '', event);\" border=0 alt=");
                sb.append(autoSuggestPreviousPage);
                //sb.append(" align=left src=");
                sb.append(" src=");
                sb.append(imageName);
                sb.append(">");
            }
            else {
                sb.append("<IMG style=\"CURSOR: pointer\" id=\"typeAheadPreviousPage\"");
                // sb.append(" onclick=\"FCHandler.onTypeAheadPreviousPageClick(this, '', event);\"");
                sb.append(" border=0 alt=");
                sb.append(autoSuggestPreviousPage);
                var imageName = this.isRTL ? window["E1RES_img_AutoSuggest_previous_page_dis_rtl"] : window["E1RES_img_AutoSuggest_previous_page_dis"];
                sb.append(" src=");
                //sb.append(" align=left src=");
                sb.append(imageName);
                sb.append(">");
            }
            sb.append("</A>");

            sb.append("<A style=\"TEXT-DECORATION: none;\" title=");
            sb.append(autoSuggestNextPage);
            sb.append(">");
            if (this.hasNextPage()) {
                if (this.isRTL) {
                    var moImageName = window["E1RES_img_AutoSuggest_next_page_mo_rtl"];
                    var imageName = window["E1RES_img_AutoSuggest_next_page_rtl"];
                }
                else {
                    var moImageName = window["E1RES_img_AutoSuggest_next_page_mo"];
                    var imageName = window["E1RES_img_AutoSuggest_next_page"];
                }

                sb.append("<IMG style=\"CURSOR: pointer\" id=\"typeAheadNextPage\"");
                sb.append(" onmouseover=\"FCHandler.onTypeAheadNextPageMouseOver(this, '', event);\"");
                sb.append(" onmouseout=\"FCHandler.onTypeAheadNextPageMouseOut(this, '', event);\"");
                sb.append(" onclick=\"FCHandler.onTypeAheadNextPageClick(this, '', event);\" border=0 alt=");
                sb.append(autoSuggestNextPage);
                sb.append(" src=");
                //sb.append(" align=left src=");
                sb.append(imageName);
                sb.append(">");
            }
            else {
                sb.append("<IMG style=\"CURSOR: pointer\" id=\"typeAheadNextPage\"");
                // sb.append(" onclick=\"FCHandler.onTypeAheadNextPageClick(this, '', event);\"");
                sb.append(" border=0 alt=");
                sb.append(autoSuggestNextPage);
                var imageName = this.isRTL ? window["E1RES_img_AutoSuggest_next_page_dis_rtl"] : window["E1RES_img_AutoSuggest_next_page_dis"];
                sb.append(" src=");
                //sb.append(" align=left src=");
                sb.append(imageName);
                sb.append(">");
            }

            sb.append("</A>");
        }
        sb.append("</td>");
        sb.append("<td style='width:50px' valign=middle><table><tbody><tr>");
        sb.append("<td>");
        if (isDropDown) {
            sb.append("<A class=clickablelink href=\"#\" ");
            sb.append(" onmouseover=\"FCHandler.onTypeAheadVAMouseOver(this, '', event);\"");
            sb.append(" onmouseout=\"FCHandler.onTypeAheadVAMouseOut(this, '', event);\"");
            sb.append(" onclick=\"FCHandler.onTypeAheadVAClick('");
            sb.append(cmd);
            sb.append("', '");
            sb.append(namespace);
            sb.append("', event);\"");
            sb.append(" align=left>");
            sb.append(autoSuggestSearch);
            sb.append("</A>");
        }
        sb.append("</td>");
        sb.append("</tr></tbody></table></td>");
        sb.append("</tr>");
        sb.append("</tbody>");
        sb.append("</table>");
        sb.append("</td>");
        sb.append("</tr>");
    }

    sb.append("</tbody>");
    sb.append("</table>");
    // End of grid data section
    sb.append("</td>");
    sb.append("</tr>");
    sb.append("</tbody>");
    sb.append("</table>");
    sb.append("</div>");
    sb.append("</td>");
    sb.append("</tr>");
    // End of data

    sb.append("</tbody>");
    sb.append("</table>");

    return sb.toString();
}

var isSubForm = false;
///////////////////////////////////////
// Singleton Touch Handler
///////////////////////////////////////
var FORMTOUCH;
if (FORMTOUCH == null) {
    FORMTOUCH = new function () {
        this.nbrFingers = 0;
        this.startTouchEvent = false;
        this.moveXArray = [];
        this.moveYArray = [];
        this.doubleTapTimer = null;
        this.doubleTapDeltaTime = 700;
    }
}

FORMTOUCH.restoreFormFocusOnRIAF = false;

// Process touch events from iPad and other touch devices
FORMTOUCH.touchFormStart = function (e) {
    var formTouch = FORMTOUCH;
    var tFingers = e.touches.length;
    mouseClickWebkit();
    // a single finger touch
    if (tFingers == 1) {
        var touch = e.touches[0];
        if (touch && touch.pageX > 60) {
            // only accept one finger touches in the first 60 pixels of the form
            formTouch.clearTouch();
            return;
        }
    }
    else if (tFingers == 2) {
        // fall though  
    }
    else if (tFingers == 3) {
        e.preventDefault();
    }
    else {
        formTouch.clearTouch();
        return;
    }
    formTouch.nbrFingers = tFingers;
    formTouch.startTouchEvent = true;
}

FORMTOUCH.touchFormCancel = function (e) {
    FORMTOUCH.clearTouch();
}

FORMTOUCH.touchFormMove = function (e) {
    var formTouch = FORMTOUCH;

    if (formTouch.startTouchEvent == false) {
        return;
    }

    if (formTouch.nbrFingers == 1) {
        e.preventDefault();
        formTouch.moveXArray.push(e.changedTouches[0].clientX);
        formTouch.moveYArray.push(e.changedTouches[0].clientY);
    }
    // 2 finger touch events are clicks only. If a move is detected then throw away the event and let
    // the device handle the event natively
    else if (formTouch.nbrFingers == 2) {
        formTouch.nbrFingers = 0;
    }
    else if (formTouch.nbrFingers == 3) {
        e.preventDefault();
        formTouch.moveXArray.push(e.touches[0].pageX);
        formTouch.moveYArray.push(e.touches[0].pageY);
    }
}

FORMTOUCH.touchFormEnd = function (e) {
    var formTouch = FORMTOUCH;
    // Only process 1,2 or 3 finger events
    if (formTouch.startTouchEvent == false || formTouch.nbrFingers < 1 || formTouch.nbrFingers > 3) {
        formTouch.clearTouch();
        return;
    }
    // 3 finger events need to move a certain distance before acceptance
    if (formTouch.nbrFingers == 3 && formTouch.moveXArray.length < 2) {
        formTouch.clearTouch();
        return;
    }
    if (formTouch.nbrFingers == 1) {
        if (formTouch.moveXArray.length < 1) {
            formTouch.clearTouch();
            return;
        }
        TouchLib.init(formTouch.moveXArray, formTouch.moveYArray);
        var gesture = TouchLib.determineGesture(TouchLib.DIRECTION_SENSITIVITY_HIGH);
        TouchLib.processGesture(gesture, window, document, e);
    }
    // 2 finger events are a single touch only (i.e. 2 finger 'click')
    // simple form exit display
    else if (formTouch.nbrFingers == 2) {
        e.preventDefault();
        formTouch.tap(formTouch, e);
    }
    else if (formTouch.nbrFingers == 3) {
        e.preventDefault();
        TouchLib.init(formTouch.moveXArray, formTouch.moveYArray);
        var gesture = TouchLib.determineGesture(TouchLib.DIRECTION_SENSITIVITY_MEDIUM);
        formTouch.clearTouch();
        TouchLib.performGenericGesture(e, gesture);
    }
    formTouch.clearTouch();
}


FORMTOUCH.clearTouch = function (clear) {
    TouchLib.clean();
    FORMTOUCH.nbrFingers = 0;
    FORMTOUCH.startTouchEvent = false;
    FORMTOUCH.moveXArray = [];
    FORMTOUCH.moveYArray = [];
    if (clear) {
        FORMTOUCH.doubleTapTimer = null;
    }
}

FORMTOUCH.singleTap = function (formTouch, event) {
    var formExit = getExitMenuLaunchElement(EXIT_TYPE.FORM_EXIT);
    if (formExit != null) {
        if (FormKeyboardHandler.isRightClickPrefCheckEnabled() || FCHandler.isTouchEnabled) {
            formExit.onclick(event);
        }
    }
    formTouch.clearTouch(true);
    return TouchLib.stopEventBubbling(event);
}

FORMTOUCH.doubleTap = function (formTouch, event) {
    formTouch.clearTouch(true);
    if (self.parent) {
        self.parent.goHome();
    }
}

FORMTOUCH.tap = function (formTouch, event) {
    var fTimer = formTouch.doubleTapTimer;
    if (fTimer == null) {
        // First tap, we wait X ms to the second tap
        formTouch.doubleTapTimer = setTimeout(function () { FORMTOUCH.singleTap(formTouch, event) }, formTouch.doubleTapDeltaTime);
    } else {
        // Second tap
        clearTimeout(fTimer);
        FORMTOUCH.doubleTap(formTouch, event);
    }
    formTouch.clearTouch();
}

FORMTOUCH.disableTouchEvents = function (elem) {
    if (elem)
        elem.ontouchstart = elem.ontouchmove = elem.ontouchend = elem.ontouchcancel = null;
}

FORMTOUCH.enableTouchEvents = function (elem) {
    if (elem) {
        elem.ontouchstart = FORMTOUCH.touchFormStart;
        elem.ontouchmove = FORMTOUCH.touchFormMove;
        elem.ontouchend = FORMTOUCH.touchFormEnd;
        elem.ontouchcancel = FORMTOUCH.touchFormCancel;
    }
}

/* 
This method will be called to update the list of DataBrowser currently open.
*/
function onCloseOfDataBrowserWindow() {
    if ((window.opener && window.opener.document.forms['ChooseTargetForm']) && (undefined != window.opener.document.forms['ChooseTargetForm'])) {
        var winOpener = window.opener;
        if (winOpener != null) {
            winOpener.setCloseMainWin(false);
        }
        window.opener.document.forms['ChooseTargetForm'].submit();
    }
}

/* 
JavaScript for RTF to HTML conversion
*/

function validateBatch() {
    var cmbBatch = document.getElementById("batchSize");
    var batchOption = document.getElementById('selectBatch');
    if (batchOption && batchOption.checked === true && cmbBatch) {
        var index = document.getElementById("batchSize").selectedIndex;
        var selValue = document.getElementById("batchSize").options[index].value
        if (selValue == "Completed") {
            alert(completedBatch);
            document.getElementById("batchSize").selectedIndex = 0;
            return false;
        }
    }
    return true;
}
FCHandler.showHideBatch = function (val) {
    if (val == 'on' || val === true) {
        document.getElementById('selectBatch').checked = true;
        document.getElementById('batchDropDown').style.display = 'block';
    }
    else if (val == 'off' || val === false) {
        document.getElementById('selectAll').checked = true;
        document.getElementById('batchDropDown').style.display = 'none';
    }
}

FCHandler.onFocusBatchSize = function (ctrl) {
    if (ctrl && ctrl.value >= 100) {
        this.prevValue = ctrl.value;
    }

}

FCHandler.onBlurBatchSize = function (ctrl, namespace) {
    var batchSize = ctrl.value;
    var numbers = /^[0-9]+$/;
    if ((batchSize.match(numbers))) {
        // The allowed batch size is between 100 and 1000000 for the RTF to HTML conversion.
        if (batchSize && (batchSize >= 100) && (batchSize <= 1000000)) {
            if (this.prevValue === batchSize) {
                return false;
            }
            JDEDTAFactory.getInstance(namespace).post('loadBatch')
            this.prevValue = ctrl.value;
        }
        else {
            alert(minMaxBatchSize);
            ctrl.focus();
        }
    }
    else {
        alert(numBatchSize);
        ctrl.focus();
    }
}

FCHandler.onRtfToHtmlButtonClick = function (btn, namespace) {
    if (btn.id == 'Ok') {
        if (document.getElementById("agreement").checked === false) {
            alert(agreementMsg);
            return;
        }
    }
    if (btn.id == 'Convert') {
        if (validateBatch()) {
            document.getElementById("Convert").setAttribute("disabled", "disabled")
            document.getElementById("Cancel").setAttribute("disabled", "disabled")
            if (document.getElementById('selectAll').checked === true) {
                document.getElementById("convertBatch").value = document.getElementById("convertAll").value
            }
            else {
                document.getElementById("convertBatch").value = document.getElementById("batchSize").value
            }
        }
        else {
            return;
        }
    }
    JDEDTAFactory.getInstance(namespace).post(btn.id);
}

showAddFormToFavorites = function (show) {
    try {
        var addToFavDiv = parent.document.getElementById('ADD_TO_FAVORITES');
    }
    catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
        var addToFavDiv = null;
    }
    if (addToFavDiv != null) {
        if (show) {
            addToFavDiv.style.display = 'table';
        }
        else {
            addToFavDiv.style.display = 'none';
        }
    }
    else//Only during Ctrl + F5, both the menu AND the E1 form refresh, we need to wait for the menu to finish.
    {
        setTimeout("showAddFormToFavorites(" + show + ");", 1000);
    }
}

var ToggleGroupBox;
if (!ToggleGroupBox) {
    ToggleGroupBox = new Object();

    ToggleGroupBox.CONSTANTS = new Object();
}

ToggleGroupBox.toggle = function (elementId, sysFnc, adjustControlsOnly, fromDTA) {
    var me = JSCompMgr.getNodeByID(elementId);
    if (me == null) {
        return;
    }
    if (!adjustControlsOnly) {
        adjustControlsOnly = false;
    }
    if (!fromDTA) {
        fromDTA = false;
    }

    var headerContainerId = 'outer' + elementId;
    var headerContainer = document.getElementById(headerContainerId);
    var groupBoxContainerId = "div" + elementId;
    var groupBoxContainer = document.getElementById(groupBoxContainerId);
    if (adjustControlsOnly == false) // if from post dialog init then trust the collapse request
    {
        if (!sysFnc) {
            sysFnc = groupBoxContainer.offsetHeight == 0 ? "expand" : "collapse";
        }
        else {
            // this means it is probably inside an already collasped groupbox or subform
            // so don't do anything with it
            if (groupBoxContainer.offsetHeight == 0 && headerContainer.offsetHeight == 0) {
                return;
            }
            var currentState = groupBoxContainer.offsetHeight == 0 ? "collapse" : "expand";
            if (currentState == sysFnc) {
                return;
            }
        }
    }
    var controlIds = this.getControlsBelow(me, elementId);
    var controlBelow = controlIds.length > 0 ? true : false;
    var hasAdjacentControls = headerContainer.getAttribute("hasAdjacentControls");
    if (!hasAdjacentControls) {
        // this should NEVER happen
        hasAdjacentControls = false;
    }

    var namespace = JSCompMgr.findNameSpace(elementId);
    if (adjustControlsOnly == true && hasAdjacentControls == "false") {
        this.repositionControls(me, controlIds, sysFnc);
    }
    else {
        var imageId = "img" + elementId;
        var image = document.getElementById(imageId);
        if (sysFnc == "expand") {
            // set the image to expanded state
            if (image != null) {
                image.src = (document.documentElement.dir == "rtl" ? window["E1RES_img_discloseexpanded_rtl_ena_png"] : window["E1RES_img_discloseexpanded_ena_png"]);
                setAccAttr(image, "aria-expanded", "true");
                image.focus();
            }

            groupBoxContainer.style.display = "block";
            groupBoxContainer.parentNode.parentNode.parentNode.parentNode.style.display = "block";
            groupBoxContainer.parentNode.parentNode.parentNode.parentNode.style.visibility = "visible";
        }
        else {
            // set the image to collapsed state
            if (image != null) {
                image.src = (document.documentElement.dir == "rtl" ? window["E1RES_img_disclosecollapsed_rtl_ena_png"] : window["E1RES_img_disclosecollapsed_ena_png"]);
                setAccAttr(image, "aria-expanded", "false");
                image.focus();
            }

            groupBoxContainer.style.display = "none";
            groupBoxContainer.parentNode.parentNode.parentNode.parentNode.style.visibility = "hidden";
        }
        if (fromDTA == false && !adjustControlsOnly) {
            JDEDTAFactory.getInstance(namespace).post("toggleGroupBox" + elementId);
        }
        // Ensure that there is some below the current groupbox before bothering doing too much else
        if (controlBelow == false) {
            return;
        }
        if (hasAdjacentControls == "false") {
            this.repositionControls(me, controlIds, sysFnc);
        }

    }

    //when expanding a groupbox, adjust the controls within the groupbox
    if (sysFnc == "expand" && headerContainer.getAttribute("beenAdjusted") == "false") {
        this.repositionChildren(me);
    }
    if (adjustControlsOnly == false) {
        headerContainer.setAttribute("beenAdjusted", "true");
    }

    var rootContainer = JSCompMgr.getRootContainer(namespace);
    JSCompMgr.doResize(rootContainer, namespace);
}

ToggleGroupBox.getControlsBelow = function (me, elementId) {
    var controlIds = new Array();
    var myTopValue = this.getElementTop(elementId);
    var myBottomValue = this.getElementBottom(elementId);
    var headerContainer = document.getElementById("outer" + elementId);
    var myRealParentId = headerContainer.parentNode.id;
    if (me) {
        for (var i = 0; i < me.parent.children.length; i++) {
            var currentControl = me.parent.children[i];
            if (currentControl.ID != me.ID) {
                var siblingOuter = document.getElementById("outer" + currentControl.ID);
                // do not take into account controls in the form below grid div
                if (siblingOuter && siblingOuter.parentNode && siblingOuter.parentNode.id != myRealParentId) {
                    continue;
                }
                var currTopValue = this.getElementTop(currentControl.ID);
                if (currTopValue > myTopValue) {
                    // only care about controls below me for adjustment
                    controlBelow = true;
                    controlIds.push("outer" + currentControl.ID);
                }
            }
        }
    }
    return controlIds;
}

ToggleGroupBox.getElementTop = function (id) {
    return document.getElementById("outer" + id).offsetTop;
}

ToggleGroupBox.getElementBottom = function (id) {
    var currTopValue = document.getElementById("outer" + id).offsetTop;
    var currHeightValue = document.getElementById("outer" + id).offsetHeight;
    return currTopValue + currHeightValue;
}

ToggleGroupBox.isParallelControl = function (myTopValue, myBottomValue, currTopValue, currBottomValue) {
    if ((currTopValue != currBottomValue) &&
       ((currTopValue >= myTopValue && currTopValue <= myBottomValue) ||
        (currBottomValue >= myTopValue && currBottomValue <= myBottomValue) ||
        (currTopValue <= myTopValue && currBottomValue >= myBottomValue))) {
        return true;
    }
    return false;
}

// Ths is called from Post Dialog Initialized collaspe requests
ToggleGroupBox.adjustEffectedControls = function (groupBoxIds, sysFnc) {
    for (var j = 0; j < groupBoxIds.length; j++) {
        var groupBoxId = groupBoxIds[j];
        ToggleGroupBox.toggle(groupBoxId, sysFnc, true);
    }
}

ToggleGroupBox.markForAdjacentControls = function (groupBoxIds) {
    for (var j = 0; j < groupBoxIds.length; j++) {
        var groupBoxId = groupBoxIds[j];
        var me = JSCompMgr.getNodeByID(groupBoxId);
        if (me == null) {
            return;
        }
        var headerContainerId = 'outer' + groupBoxId;
        var headerContainer = document.getElementById(headerContainerId);
        headerContainer.setAttribute("beenAdjusted", "false");
        var hasAdjacentControls = headerContainer.getAttribute("hasAdjacentControls");
        if (hasAdjacentControls) {
            // most probably already set by the elongation logic in JSComponent.js
            continue;
        }
        headerContainer.setAttribute("hasAdjacentControls", "false");
        var myTopValue = this.getElementTop(groupBoxId);
        var myBottomValue = this.getElementBottom(groupBoxId);
        for (var i = 0; i < me.parent.children.length; i++) {
            var currentControl = me.parent.children[i];
            if (currentControl.ID != me.ID) {
                var currTopValue = this.getElementTop(currentControl.ID);
                var currBottomValue = this.getElementBottom(currentControl.ID);
                if (this.isParallelControl(myTopValue, myBottomValue, currTopValue, currBottomValue) == true) {
                    // if there is any control parallel to me then I can't adjust controls, only expand / collapse
                    headerContainer.setAttribute("hasAdjacentControls", "true");
                    break;
                }
            }
        }
    }
}

ToggleGroupBox.repositionControls = function (me, controlIds, sysFnc) {
    for (var j = 0; j < controlIds.length; j++) //All group boxes below the collapsed group boxes should be adjusted for the top coordinate
    {
        var topValue = this.calculateTopValue(me, controlIds[j], sysFnc);
        document.getElementById(controlIds[j]).style.top = topValue + "px";
    }
}

ToggleGroupBox.calculateTopValue = function (me, controlId, sysFnc) {
    var currDivId = "div" + me.ID;
    var groupBoxTop = document.getElementById(controlId).style.top;
    var initialTopValue = groupBoxTop.substring(0, groupBoxTop.length - 2);
    var divHeight = document.getElementById(currDivId).style.height;
    var heightValue = divHeight.substring(0, divHeight.length - 2);
    if (sysFnc == "expand") {
        return parseInt(initialTopValue) + parseInt(heightValue);
    }
    else {
        return parseInt(initialTopValue) - parseInt(heightValue);
    }
}

ToggleGroupBox.repositionChildren = function (parentGroupBox) {
    //look for collapsed container 
    for (var i = 0; i < parentGroupBox.children.length; i++) {
        var controlIds = new Array();
        var collapsedControl = parentGroupBox.children[i];
        var collapsedTop = this.getElementTop(collapsedControl.ID);
        //if there is a collapsible groupbox
        if (collapsedControl.isCollapsableGroupBox() == true) {
            //groupbox is collapsed, adjust controls below this groupbox
            if (document.getElementById("div" + collapsedControl.ID).offsetHeight == 0) {
                if (document.getElementById("outer" + collapsedControl.ID).getAttribute("hasAdjacentControls") == "false") {
                    //find all the controls under the collapsed control
                    for (var j = 0; j < parentGroupBox.children.length; j++) {
                        var siblingControl = parentGroupBox.children[j];
                        var siblingTop = this.getElementTop(siblingControl.ID);
                        if (siblingTop > collapsedTop) {
                            controlIds.push("outer" + siblingControl.ID);
                        }
                    }
                }
            }
            else//for an expanded groupbox, need to adjust it's children
            {
                this.repositionChildren(collapsedControl);
            }
        }
        this.repositionControls(collapsedControl, controlIds, "collapse");
    }
}

var ChangeConf;

if (ChangeConf == null) {
    ChangeConf = new function () {
        this.isChangeConfEnabledForm = false;
        this.isOkConf = false;
        this.isCancelConf = false;
        this.isFindConf = false;
        this.isDirtyForm = false;
        this.isDirtyGrid = false;
        this.isConfProcessed = false;
    }
}

ChangeConf.initChangeConf = function () {
    this.isChangeConfEnabledForm = (changeConfEnabledForm == "true") ? true : false;
    this.isOkConf = (okConf == "true") ? true : false;
    this.isCancelConf = (cancelConf == "true") ? true : false;
    this.isFindConf = (findConf == "true") ? true : false;
    this.isDirtyForm = (dirtyForm == "true") ? true : false;
    this.isDirtyGrid = (dirtyGrid == "true") ? true : false;

    var okHyperElem = document.getElementById("hc_OK");
    // Gray out the OK HYPER BUTTON only when the form is Ok Confirmation enabled and it is NOT a Dirty Form (Unchanged by user or Import functionality)
    if (okHyperElem != null && this.isOkConf && !(this.isDirtyForm || this.isDirtyGrid)) {
        ChangeConf.maskOkHyperControl(okHyperElem);
    }
}

ChangeConf.maskOkHyperControl = function (okHyperElem) {
    okHyperElem.style.cursor = "default";
    okHyperElem.src = "/jde/img/hc_OK_grayedout.gif";
    okHyperElem.onmouseout = "";
    okHyperElem.onmouseover = "";
    okHyperElem.onclick = "";
}

ChangeConf.unmaskOkHyperControl = function () {
    var okHyperElem = document.getElementById("hc_OK");
    if (okHyperElem != null && (this.isDirtyForm || this.isDirtyGrid)) {
        okHyperElem.style.cursor = "pointer";
        okHyperElem.src = "/jde/img/hc_OK.gif";
        var okAnchorElemId = okHyperElem.parentNode.id;
        var postId = okAnchorElemId.substring(1);   // HyperExit anchor Ids will be like C0_xx but we post 0_xx only, hence we do substring to avoid "C"
        okHyperElem.onmouseout = function mouseOutOkImage() { okHyperElem.src = "/jde/img/hc_OK.gif"; }
        okHyperElem.onmouseover = function mouseOverOkImage() { okHyperElem.src = "/jde/img/hc_OKmo.gif"; }
        okHyperElem.onclick = function clickOkImage() { JDEDTAFactory.getInstance('').post(postId); }
    }
}

function allowSetFocusOnOpen() {
    var allowSetFocus = (!window.EUR || !EUR.isShowingReport()) && (!window.RIUTIL || !window.RIUTIL.isEmbedded) && !isTouchEnabled;
    return allowSetFocus;
}

function initEmbeddedE1FormPage(E1FormPreviewMode) {
    window.isRunningInE1FormPage = true;
    if (!E1FormPreviewMode) // for e1 page preview mode we need to clean up maflet using browser close control to prevent leakage of maflet and owvirtual
        window.controlledBrowserClose = true;
}

// API that uses the mailto URL scheme to create an editable email request.
// The email message shall be opened in the user's default mail client app.
function sendEmailRequest(emailJSON) {
    if (userAgent == null || userAgent == undefined) {
        var userAgent = navigator.userAgent.toUpperCase();
    }
    var isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
    var isWebkit = (userAgent.indexOf("WEBKIT") > -1);
    var isFirefox = (userAgent.indexOf("FIREFOX") > -1);
    var formTitle = HTMLDecoder(emailJSON.formTitle);
    // Enclose the Paramterized URL in Double quotes to ensure the URL does not break in Thunderbird
    // In Thunderbird the URL breaks at the Pipe (|) character. To prevent this we have to enclose it within double quotes
    var parameterizedURL = '"' + emailJSON.parameterizedURL + '"';
    var paramURLInHex = convertParamURLToHex(parameterizedURL);
    var encodedParamURL = encodeURIComponent(paramURLInHex);
    var emailReceipients = "";
    var mailtoLink = "";
    var showParameterizedURL = true;
    // for send email from Collaborate Tab of AN8 hover forms
    if (emailJSON.emailReceipients != null && emailJSON.emailReceipients != "undefined" && emailJSON.emailReceipients.length > 0) {
        emailReceipients = HTMLDecoder(emailJSON.emailReceipients);
        mailtoLink = "mailto:" + emailReceipients + "?subject=" + formTitle;
        // Show the paramterized URL in the email body only if the flag is true 
        if (emailJSON.showParameterizedURL) {
            mailtoLink += "&body=" + formTitle + ": " + encodedParamURL;
        }
        else {
            showParameterizedURL = false;
        }
    }
    // for send email from Tools - Collaborate menu 
    else {
        mailtoLink = "mailto:?subject=" + formTitle + "&body=" + formTitle + ": " + encodedParamURL;
    }

    // If user's default mail client is Gmail then in Firefox or Chrome 
    // the mailto has to be opened in a new tab/window to avoid xframe error thrown by Gmail
    if ((!isIOS) && (emailJSON.mailtoInNewWindow) && (isWebkit || isFirefox)) {
        var urlFactory = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
        urlFactory.setURI("owhtml/mailto.jsp");
        urlFactory.setParameter("subject", encodeURIComponent(formTitle));
        if (showParameterizedURL) {
            urlFactory.setParameter("body", encodeURIComponent(formTitle) + ": " + encodeURIComponent(encodedParamURL));
        }
        if (emailJSON.emailReceipients) {
            urlFactory.setParameter("emailReceipients", emailReceipients);
        }
        var mailtoLinkInNewWindow = urlFactory.toString();
        window.open(mailtoLinkInNewWindow, "SendEmail");
    }
    else {
        window.location.href = mailtoLink;
    }
}

// API that sends a request to the SendMeetingInvite Servlet to create an .iCalendar 
// file that will be used to create an editable meeting request .
// The editable meeting request will be opened in user's default calendar app.
function sendMeetingInviteRequest(meetingInviteJSON) {
    var parameterizedURL = meetingInviteJSON.parameterizedURL;
    var paramURLInHex = convertParamURLToHex(parameterizedURL);
    var encodedParamURL = encodeURIComponent(paramURLInHex);
    var formTitle = HTMLDecoder(meetingInviteJSON.formTitle);
    var meetingInviteReceipients = HTMLDecoder(meetingInviteJSON.meetingInviteReceipients);
    var urlFactory = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
    urlFactory.setURI("SendMeetingInvite");
    urlFactory.setParameter("parameterizedURL", encodedParamURL);
    urlFactory.setParameter("appName", formTitle);
    urlFactory.setParameter("useThunderbird", meetingInviteJSON.useThunderbird);
    if (meetingInviteReceipients != null && meetingInviteReceipients != "undefined" && meetingInviteReceipients.length > 0) {
        urlFactory.setParameter("showParameterizedURL", meetingInviteJSON.showParameterizedURL);
        urlFactory.setParameter("meetingInviteReceipients", meetingInviteReceipients);
    }
    var postURL = urlFactory.toString();
    if (isNativeContainer) {
        addEventToCalendarForNativeContainer(formTitle, encodedParamURL);
    }
    else {
        window.location.href = postURL;
    }
}

// API to convert spaces in the Paramterized URL to Hex code with % pre-appended to the Hex Code
// This is becuase if the mailto body has spaces in the paramterized URL then it does not appear as a Hyperlink in the mail/calendar client.
function convertParamURLToHex(parameterizedURL) {
    var hexURL = '';
    for (var i = 0; i < parameterizedURL.length; i++) {
        if (parameterizedURL.charAt(i) == ' ')
            hexURL += '%' + parameterizedURL.charCodeAt(i).toString(16);
        else
            hexURL += parameterizedURL.charAt(i);
    }
    return hexURL;
}

// API to decode HTML chars
function HTMLDecoder(encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    if (div.firstChild != null)
        return div.firstChild.nodeValue;
    else
        return "";
}

// Function to open calendar app and add event to calendar on Native Container
function addEventToCalendarForNativeContainer(appName, encodedParamURL) {
    try {
        var parameterizedURL = decodeURIComponent(encodedParamURL);
        var tokenizedParamURL = "";
        if (parameterizedURL.indexOf("&") != -1) {
            // first split the entire url on basis of &
            var params = parameterizedURL.split("&");
            // split the first param on basis of ? to seperate shortcutlauncher url and oid
            var urlParams = params[0].split("?");
            // shortcutlauncher url is first param
            var shortcutLauncherURL = urlParams[0];
            // put oid as first param in the list of params
            params[0] = urlParams[1];
            // tokenize the params further on basis of = to extract only values
            for (var i = 0; i < params.length; i++) {
                var keyValuePair = params[i].split("=");
                if (keyValuePair.length == 2) {
                    tokenizedParamURL += keyValuePair[1] + "#seperator#";
                }
            }
            var formLaunchParams = tokenizedParamURL.split("#seperator#");
            if (formLaunchParams) {
                var oid = formLaunchParams[0];
                var formDSTmpl = formLaunchParams[1];
                var formDSData = formLaunchParams[2];
                var formQueryId = "";
                if (formLaunchParams[3]) {
                    formQueryId = formLaunchParams[3]
                }
                var parentWindow = findNativeContainerUtils();
                if (parentWindow) {
                    parentWindow.NativeContainer.addEventToCalendar(appName, shortcutLauncherURL, oid, formDSTmpl, formDSData, formQueryId);
                }
            }
        }
    }
    catch (err) {
    }

}

function onRadioButtonFocus(e) {
    if (!isFirefox)
        return;

    var eventObject;
    if (e != undefined) {
        eventObject = e.target;
    }
    else // IE8 - IE11
    {
        e = event;
        eventObject = e.srcElement;
    }

    eventObject.parentNode.className = 'focusedHelpDiv';
}

function onRadioButtonBlur(e) {
    if (!isFirefox)
        return;

    var eventObject;
    if (e != undefined) {
        eventObject = e.target;
    }
    else // IE8 - IE11
    {
        e = event;
        eventObject = e.srcElement;
    }

    eventObject.parentNode.className = 'unfocusedHelpDiv';
}

function validateRefreshInCompositePage() {
    if (window.frameElement && window.frameElement.id == RIUTIL.ID_E1MENU_APP_FRAME) {
        top.location.replace(top.location.href);

        //refresh is interpreted as window close unless set controlled flat to true    
        window.controlledBrowserClose = true;
    }
}