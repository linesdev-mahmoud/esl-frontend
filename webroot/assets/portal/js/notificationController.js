if (!window.NotificationController) {
    NotificationController = new createNotificationController();
}

function createNotificationController() {
    this.DEBUG = false;

    /*Messages to notification worker*/
    this.WORKER_START = 0,
    this.WORKER_PAUSE = 1,
    this.WORKER_UNPAUSE = 2,
    this.WORKER_RESET = 3,

    // Displays the session timeout dialog
    // Returns true/false as an indication that a logout has (true)/hasn't (false) occurred.
    this.showTimeoutDialog = function (dialogText, logoutURL, keepAliveURL) {
        var newBtn = '<div class="imageDiv16"><img role="button" class="ui-dialog-titlebar-custom-close" tabindex="0" onclick="javascript: $(' + "'#timeoutDialog').dialog(" + "'close'" + ');" onkeydown="if(event.keyCode ==32 || event.keyCode == 13){this.click()};" src="/jde/img/jdeclose_ena.png" title="Close" alt="Close" name="img/jdeclose_ena.png" onmouseover="showMotion(this)" onmouseout="removeMotion(this)"></div>';

        $("#timeoutDialogText").text(dialogText);
        $("#timeoutDialog").dialog({
            dialogClass: "popupWindow",
            modal: false,
            height: 200,
            width: 300,
            resizable: false,
            buttons: [
                {
                    text: "Continue session", //NTF_STRING_CONSTS.continueSession,
                    id: "continueSessionBtn",
                    click: function () {
                        $(this).dialog("close");
                        if (NotificationController.DEBUG) {
                            console.log("Pressed 'Continue session'");
                            console.log(keepAliveURL);
                        }
                        E1AJAX.sendXMLReq("POST", "", keepAliveURL, NotificationController.loginVerificationHandler);
                        return false;
                    },
                    "class": "buttonstylenormal",
                    // Mouse events will behave like the Preferences popup.
                    mouseover: function (event) {
                        prefButtonHover(this.id, event);
                    },
                    mouseup: function (event) {
                        prefButtonHover(this.id, event);
                    },
                    mouseout: function (event) {
                        prefButtonHover(this.id, event);
                    },
                    mousedown: function (event) {
                        prefButtonHover(this.id, event);
                    }
                },
                {
                    text: "End session", //NTF_STRING_CONSTS.endSession,
                    id: "endSessionBtn",
                    click: function () {
                        $(this).dialog("close");
                        if (NotificationController.DEBUG) {
                            console.log("Pressed 'End session.' logoutURL is " + logoutURL + ".");
                        }
                        document.getElementById('LOGOUT_IFRAME').src = logoutURL;
                        return true;
                    },
                    "class": "buttonstylenormal",
                    // Mouse events will behave like the Preferences popup.
                    mouseover: function (event) {
                        prefButtonHover(this.id, event);
                    },
                    mouseup: function (event) {
                        prefButtonHover(this.id, event);
                    },
                    mouseout: function (event) {
                        prefButtonHover(this.id, event);
                    },
                    mousedown: function (event) {
                        prefButtonHover(this.id, event);
                    }
                }
            ],
            closeOnEscape: true,
            // Hide the default close button.
            open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
            create: function () { $(this).prev('.ui-dialog-titlebar').find('.ui-dialog-title').append(newBtn) }
        });
    }

    /* Inits pending notifications polling worker. Called from dropdown_viewmax.jsp */
    this.startNotificationWorker = function (workerURL, notificationPostURL, keepAliveURL, pingInterval) {
        NotificationController.notificationPostURL = notificationPostURL;
        if (typeof (Worker) !== "undefined") {
            if (typeof (w) == "undefined") {
                w = new Worker(workerURL);
                var startWorker = new Object();
                startWorker.notificationURL = notificationPostURL;
                startWorker.pingInterval = pingInterval;
                NotificationController.sendMessageToNotificationWorker(NotificationController.WORKER_START, startWorker);
                if (NotificationController.DEBUG) {
                    console.log(JSON.stringify(startWorker));
                }
            }

            // Event handler receiving messages from notfication worker
            w.addEventListener('message', function (event) {
                try {
                    var obj = JSON.parse(event.data),
                        expireWarning = obj.notificationReply.timeoutWarning,
                        timedout = obj.notificationReply.timedout,
                        refreshReports = obj.notificationReply.refreshReports,
                        onlineNotifications = obj.notificationReply.onlineNotifications,
                        dirtyWatchList = obj.notificationReply.dirtywatchlist,
                        notifications = obj.notificationReply.notifications,
                        messageToPost = new Object();

                    if (timedout == true) {
                        w.terminate();
                        w = undefined;
                        document.getElementById('LOGOUT_IFRAME').src = obj.notificationReply.logoutURL;
                        if (NotificationController.DEBUG) {
                            console.log("Timedout - click logout");
                        }
                        return;
                    }

                    //Once warning is received, stop polling for notifications.
                    if (expireWarning == true) {
                        if (NotificationController.DEBUG) {
                            console.log("Timeout Warning hit");
                        }

                        // Show the dialog.
                        var userEndedSession = NotificationController.showTimeoutDialog("Your session is about to expire.", //NTF_STRING_CONSTS.sessionExpiring,
                            obj.notificationReply.logoutURL, keepAliveURL);
                        if (userEndedSession) {
                            // Stop processing
                            return;
                        }
                    }

                    if (refreshReports == true) {
                        if (NotificationController.DEBUG) {
                            console.log("Refreshing Reports");
                        }
                        window.syncRecentReports(false);
                    }

                    if (dirtyWatchList != undefined) {
                        if (NotificationController.DEBUG) {
                            console.log("Dirty Watchlists found");
                        }
                        WLIST.evaluateByIdArray(dirtyWatchList);
                    }
                }
                catch (e) {
                    if (NotificationController.DEBUG) {
                        //console.log("**** invalid response ****");
                        console.error(e.message);
                    }
                }
            });
        }
        else {
            console.log("Workers not available in this browser.");
        }
    }

    this.loginVerificationHandler = function (containerId, resp) {
        if (NotificationController.DEBUG) {
            console.log(resp);
        }
        var obj = JSON.parse(resp);
        if (obj.loggedIn == false) {
            document.getElementById('LOGOUT_IFRAME').src = obj.notificationReply.logoutURL;
            return;
        }
    }

    this.sendMessageToNotificationWorker = function (type, msgObj) {
        var msgToPost = msgObj ? msgObj : {};
        switch (type) {
            case NotificationController.WORKER_START:
                //This call comes with its own msgObj.
                break;

            case NotificationController.WORKER_PAUSE:
                msgToPost.pause = true;
                break;

            case NotificationController.WORKER_UNPAUSE:
                msgToPost.pause = false;
                break;

            case NotificationController.WORKER_RESET:
                msgToPost.reset = true;
                break;

            default:
                break;
        }
        w.postMessage(JSON.stringify(msgToPost));
    }
}