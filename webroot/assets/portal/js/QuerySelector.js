var dbWindowHandle = null;
var winChildArray = null;
var isOKToClose = true;

function setDatabrowserWindowHandle(winhandle) {
    var mainWin = getE1MainWindow();
    if (mainWin != null) {
        mainWin.dbWindowHandle = winhandle;
    }
    else {
        dbWindowHandle = winhandle;
    }
}
function setChildDBWindowHandle(winhandle) {
    if (winChildArray == null) {
        winChildArray = new Array();
    }
    var i = winChildArray.length;
    winChildArray[i] = winhandle;
}

function setCloseMainWin(okToClose) {
    isOKToClose = okToClose;
}

function closeOrphanWindows() {
    var childArray = winChildArray;
    if (childArray == null || childArray.length < 1) {
        var parent = getE1MainWindow();
        if (parent != null) {
            childArray = parent.winChildArray;
            parent.winChildArray = null;
        }
    }

    if (childArray != null && childArray.length > 0) {
        var i = 0;
        for (i = 0; i < childArray.length; i++) {
            var childWin = childArray[i];
            if (childWin != null && !childWin.closed) {
                childWin.close();
            }
            childArray[i] = null;
        }
    }
    winChildArray = null;
}

function getE1MainWindow() {
    var winOpen = window.opener;
    var numTried = 0;
    var numMaxTry = 25;
    while (winOpen != null && numTried < numMaxTry) {
        var e1menuApp = winOpen.document.getElementById("e1menuAppIframe");
        if (e1menuApp != null) {
            return winOpen;
        }
        else {
            winOpen = winOpen.parent;
            numTried++;
        }
    }
    if (winOpen == null) {
        winOpen = window;
        var e1menuApp = winOpen.document.getElementById("e1menuAppIframe");
        if (e1menuApp != null) {
            return winOpen;
        }
    }
    return null;
}

function onCloseDatabrowserWindow() {
    if (isOKToClose == true) {
        var e1Main = getE1MainWindow();
        if (e1Main == null) {
            isOKToClose = true;
            closeOrphanWindows();
            return;
        }
        var winHandle = e1Main.dbWindowHandle;
        closeOrphanWindows();

        var e1DbURL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
        e1DbURL.setURI("DataBrowser");
        e1DbURL.setParameter("cmd", "mafCleanup");
        if (winHandle != null && !winHandle.closed) {
            var doc = winHandle.document;
            if (doc.P9860WS_W9860WSB != null && doc.P9860WS_W9860WSB != "undefined") {
                var elem = winHandle.document.getElementById("E1PaneForm");
                if (elem != null && elem != "undefined") {
                    var value = elem.jdemafjasUID.defaultValue;
                    if (value != null) {
                        e1DbURL.setParameter("uniqueId", value);
                    }
                }
            }
        }
        WebObjectUtil.sendXMLReq("POST", null, e1DbURL.toString(), null, false);
    }
    isOKToClose = true;
}

function processBrowserLogoff() {
    var e1Main = getE1MainWindow();
    if (e1Main == null) {
        isOKToClose = true;
        return;
    }

    var winHandle = e1Main.dbWindowHandle;
    if (winHandle != null && !winHandle.closed) {
        winHandle.close();
    }
    e1Main.dbWindowHandle = null;
}

function onSignOutClick() {
    document.ChooseTargetForm.cmd.value = "requestSignOut";
    document.ChooseTargetForm.submit();
}

function enablePersonalQueriesOption(b) {
    if (b) {
        document.ChooseTargetForm.personalQueryName.disabled = false;
    } else {
        document.ChooseTargetForm.personalQueryName.disabled = true;
    }
}

function enablePublicQueriesOption(b) {
    if (b) {
        document.ChooseTargetForm.publicQueryName.disabled = false;
    } else {
        document.ChooseTargetForm.publicQueryName.disabled = true;
    }
}

function enableTableOption(b) {
    if (b) {
        document.ChooseTargetForm.tableName.disabled = false;
        document.ChooseTargetForm.dataSource.disabled = false;
    } else {
        document.ChooseTargetForm.tableName.value = "";
        document.ChooseTargetForm.tableName.disabled = true;
        document.ChooseTargetForm.tableName.style.backgroundColor = "";

        var elm = document.getElementById("outertableNameDesc");
        if (elm != null) {
            elm.innerHTML = "";
        }

        document.ChooseTargetForm.dataSource.value = "";
        document.ChooseTargetForm.dataSource.disabled = true;
        document.ChooseTargetForm.dataSource.style.backgroundColor = "";
    }
}

function enableViewOption(b) {
    if (b) {
        document.ChooseTargetForm.viewName.disabled = false;
    } else {
        document.ChooseTargetForm.viewName.value = "";
        document.ChooseTargetForm.viewName.disabled = true;
        document.ChooseTargetForm.viewName.style.backgroundColor = "";

        var elm = document.getElementById("outerviewNameDesc");
        if (elm != null) {
            elm.innerHTML = "";
        }
    }
}

function showTableNameVA() {
    document.ChooseTargetForm.cmd.value = "vatableName";
    document.ChooseTargetForm.submit();
}

function onRadioPersonalClick(allowAccess) {
    enablePersonalQueriesOption(true);
    enablePublicQueriesOption(false);
    if (allowAccess) {
        enableTableOption(false);
        enableViewOption(false);
    }
}

function onRadioPublicClick(allowAccess) {
    enablePersonalQueriesOption(false);
    enablePublicQueriesOption(true);
    if (allowAccess) {
        enableTableOption(false);
        enableViewOption(false);
    }
}

function onRadioTableClick(allowAccess) {
    enablePersonalQueriesOption(false);
    enablePublicQueriesOption(false);
    if (allowAccess) {
        enableTableOption(true);
        enableViewOption(false);
    }
}

function onRadioViewClick(allowAccess) {
    enablePersonalQueriesOption(false);
    enablePublicQueriesOption(false);
    if (allowAccess) {
        enableTableOption(false);
    }
    enableViewOption(true);
}

function showTableNameVA() {
    isOKToClose = false;
    document.ChooseTargetForm.cmd.value = "vatableName";
    document.ChooseTargetForm.submit();
}

function showDataSourceVA() {
    isOKToClose = false;
    document.ChooseTargetForm.cmd.value = "vadataSource";
    document.ChooseTargetForm.submit();
}

function showViewNameVA() {
    isOKToClose = false;
    document.ChooseTargetForm.cmd.value = "vaviewName";
    document.ChooseTargetForm.submit();
}

function enableDatasource() {
    document.ChooseTargetForm.dataSource.disabled = false;
    document.ChooseTargetForm.vadataSource.style.display = "visible";
    var elm = document.getElementById("imgdataSource");
    if (elm != null) {
        elm.style.disabled = false;
        elm.style.display = "visible";
    }
}

function disableDatasource() {
    document.ChooseTargetForm.dataSource.disabled = true;
    document.ChooseTargetForm.vadataSource.style.display = "none";
    var elm = document.getElementById("imgdataSource");
    if (elm != null) {
        elm.style.disabled = true;
        elm.style.display = "none";
    }
}

function setTableDescErrorColor(setError) {
    var elm = document.getElementById("outertableNameDesc");
    if (elm != null) {
        elm.style.color = setError;
        setAccAttr(elm, "role", "heading");
    }
}

function setViewErrorDescription(setError) {
    var elm = document.getElementById("outerviewNameDesc");
    if (elm != null) {
        elm.style.color = setError;
        setAccAttr(elm, "role", "heading");
    }
}

function setDescriptionError(errDesc, isView) {
    var elm = null;
    if (isView) {
        elm = document.getElementById("outerviewNameDesc");
    }
    else {
        elm = document.getElementById("outertableNameDesc");
    }
    if (elm != null) {
        if (errDesc == null || errDesc.length < 1) {
            elm.style.color = "";
            setAccAttr(elm, "role", "");
        }
        else {
            elm.style.color = "#ff0000";
            setAccAttr(elm, "role", "heading");
        }
    }
}


function displayLoadMessage() {
    document.getElementById("processingMessage").style.display = "inline";
}

function onControlError(controlName, error) {
    var elm = document.getElementsByName(controlName);
    if (elm != null && elm[0] != null) {
        if (error == null || error.length < 1) {
            elm[0].style.backgroundColor = "";
        }
        else {
            elm[0].style.backgroundColor = "#ff0000";
        }
    }
}

