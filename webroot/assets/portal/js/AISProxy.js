var AISProxy = new function () {
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    //get context root
    var context = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));

    this.FORM_SERVICE = "/formservice.ais";
    this.DATA_SERVICE = "/dataservice.ais";
    this.BATCH_FORM_SERVICE = "/batchformservice.ais";
    this.APP_STACK_SERVICE = "/appstack.ais";
    this.PO_SERVICE = "/poservice.ais";
    this.LOG_SERVICE = "/log.ais";
    this.JARGON_SERVICE = "/jargon.ais";


    this.ORCHESTRATOR = ".orchestration";

    this.NODEJS = ".nodejs";

    this.callOrchestration = function (orchestration, input, callback) {
        //form url of an orchestration
        service = "/" + orchestration + AISProxy.ORCHESTRATOR;
        this.callService(input, service, callback);

    }

    this.callNodeJS = function (nodescript, input, callback) {

        //form url of an orchestration
        service = "/" + nodescript + AISProxy.NODEJS;
        this.callService(input, service, callback);
    }

    this.callService = function (input, service, callback) {

        if (typeof input === 'object') {
            //force version2 default
            if (input.outputType == null) {
                input.outputType = "VERSION2";
            }

            if (input.aliasNaming == null) {
                input.aliasNaming = true;
            }
            //convert the input object to a string
            requestDocString = JSON.stringify(input);
        }
        else {

            var jsonObject = JSON.parse(input);
            //force version2 default
            if (jsonObject.outputType == null) {
                jsonObject.outputType = "VERSION2";
            }
            if (jsonObject.aliasNaming == null) {
                jsonObject.aliasNaming = true;
            }

            //input is a string
            requestDocString = JSON.stringify(jsonObject);
        }

        //form url of form service
        url = window.location.origin + context + service;

        //post to the service with the input string as a parameter
        E1AJAX.sendXMLReq("POST", "E1Container", url,
            function (containerId, responseText) {
                if (typeof input === 'object') {
                    //return the response as an object
                    var arr_from_json = JSON.parse(responseText);
                    if (callback && typeof (callback) == "function") {
                        callback(arr_from_json);
                    }
                }
                else {
                    //return the response as a string
                    if (callback && typeof (callback) == "function") {
                        callback(responseText);
                    }
                }
            }, "json=" + requestDocString);
    }
}