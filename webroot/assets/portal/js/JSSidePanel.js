function JSSidePanelRender() {

}

JSSidePanelRender.userAgent = navigator.userAgent.toUpperCase();
JSSidePanelRender.isWebkit = (JSSidePanelRender.userAgent.indexOf("WEBKIT") > -1);
JSSidePanelRender.isIOS = (JSSidePanelRender.userAgent.indexOf("IPAD") > -1) || (JSSidePanelRender.userAgent.indexOf("IPOD") > -1) || (JSSidePanelRender.userAgent.indexOf("IPHONE") > -1);

JSSidePanelRender.isRTL = (document.documentElement.dir == "rtl");

JSSidePanelRender.MODE_NORMAL = 0;
JSSidePanelRender.MODE_REPORT_NEW = 1;
JSSidePanelRender.MODE_REPORT_EDIT = 2;
JSSidePanelRender.MODE_LAYOUT = 3;
JSSidePanelRender.MODE_QUERY = 4;
JSSidePanelRender.MODE_WATCHLIST = 5;
JSSidePanelRender.MODE_EDIT_END_USER_EVENT = 6;
JSSidePanelRender.MODE_VIEW_END_USER_EVENT = 7;
JSSidePanelRender.MODE_GRID_FORMAT = 8;
JSSidePanelRender.MODE_ENHANCED_ATTACHMENT = 9;
JSSidePanelRender.MODE_PERSONALIZE_FORM = 10;

JSSidePanelRender.PROMPT_WIDTH = 250;
JSSidePanelRender.PROMPT_INPUT_WIDTH = JSSidePanelRender.PROMPT_WIDTH - 60;
JSSidePanelRender.PROMPT_HEIGHT = (JSSidePanelRender.isIOS ? 70 : 50);
JSSidePanelRender.RESIZE_TD_WIDTH = 3;

JSSidePanelRender.ID_FRAME_TABLE = "SPFrameWorkTable";
JSSidePanelRender.ID_E1_CONTAINER_TD = "SPE1ContainerTD"
JSSidePanelRender.ID_SIDE_CONTAINER_TD = "SPSideContainerTD";
JSSidePanelRender.ID_SIDE_CONTENT_DIV = "SPContentDiv";
JSSidePanelRender.ID_SIDE_FORM_DIV = 'SPFormDiv';
JSSidePanelRender.ID_RESIZE_DIV = "SPResizeDiv";
JSSidePanelRender.ID_RESIZE_BLOCK = "SPResizeBlock";
JSSidePanelRender.ID_NAME_ENTERED = "SPNameEntered";

JSSidePanelRender.sidePanelWidth = 350; // initial side panel width
JSSidePanelRender.TAB_STUB_WIDTH = 135; // tab stub width in side panel. Increasing it from 120 to 135 to accomodate Watchlist Management Title.
JSSidePanelRender.mode = JSSidePanelRender.MODE_NORMAL; // initial mode
JSSidePanelRender.ACCESSIBILITY = false;
if (localStorage.getItem("userAccessibilityMode"))
    JSSidePanelRender.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;

// Public: method: gloabal singleton mode flags shared by all features, 
JSSidePanelRender.getMode = function (mode) {
    return this.mode;
}

// Public: method: global check to see if the mode is Query or anything else (less than the Query value
JSSidePanelRender.isQueryMode = function (mode) {
    var rtn;
    switch (mode) {
        case JSSidePanelRender.MODE_QUERY:
            {
                rtn = true;
                break;
            }
        case JSSidePanelRender.MODE_NORMAL:
        case JSSidePanelRender.MODE_REPORT_NEW:
        case JSSidePanelRender.MODE_REPORT_EDIT:
        case JSSidePanelRender.MODE_LAYOUT:
        default:
            {
                rtn = false;
            }
    }
    return rtn;
}

// Private method: only JSSidePanelRender can change this flag when it is acturally beging to update the UI
JSSidePanelRender.setMode = function (mode) {
    this.mode = mode;
}

// Private method: find the active feature model based on the state of the mode
JSSidePanelRender.getActiveFeatureModel = function () {
    return this.getFeatureModel(this.mode);
}

// Private method: find the feature model based on a given mode
JSSidePanelRender.getFeatureModel = function (mode) {
    if (mode == null || mode == this.MODE_NORMAL)
        return null;
    else if (mode == this.MODE_QUERY)
        return AQ;
    else if (mode == this.MODE_WATCHLIST)
        return WL;
    else if (mode == this.MODE_EDIT_END_USER_EVENT || mode == this.MODE_VIEW_END_USER_EVENT)
        return RIUTIL;
    else if (mode == this.MODE_GRID_FORMAT)
        return GF;
    else if (mode == this.MODE_ENHANCED_ATTACHMENT)
        return EATT;
    else if (mode == this.MODE_PERSONALIZE_FORM)
        return PF;
    else
        return EUR;
}

// Public event handler: open side panel given the target mode
JSSidePanelRender.reqOpen = function (mode) {
    var feature = this.getFeatureModel(mode);
    if (feature.onOpen)
        feature.onOpen(mode);
    else
        JSSidePanelRender.open(mode);
}

// Public event handler: close side panel to MODE_NORMAL
JSSidePanelRender.reqClose = function () {
    var feature = this.getActiveFeatureModel();
    if (!feature)
        return;

    if (feature.onClose)
        feature.onClose();
    else
        JSSidePanelRender.close();
}

// Public method call by the features to update mode and UI of Side Panel
//JSSidePanelRender.updateMode = function(mode) 
//{
//  if(mode == this.MODE_NORMAL)
//  {
//    this.close();
//  }
//  else
//  {
//    this.open(mode);
//  }
//}

// Private method: execute open action of the size panel
JSSidePanelRender.open = function (mode) {
    var fromFeature = this.getActiveFeatureModel();
    // switch mode between report and layout tab, we need to close the side panel to clean up dom
    if (fromFeature) // replace the content of side panel frame without close it
    {
        this.setMode(this.MODE_NORMAL);
        var sideTD = this.removeSidePanelContent();
        if (fromFeature.afterModeChange) {
            fromFeature.afterModeChange();
        }
        this.setMode(mode);
    }
    else // render out side panel frame first
    {
        this.setMode(mode);
        var sideTD = this.renderSidePanelFrame();
    }

    // add the side panel content into the frame
    this.renderSidePanelContent(sideTD);
    this.adjustSize();

    var toFeature = this.getActiveFeatureModel();
    if (toFeature.afterModeChange) {
        toFeature.afterModeChange();
    }
    this.repaintGridHeader();
}

// Private method: execute close action of the size panel
JSSidePanelRender.close = function () {
    var feature = this.getActiveFeatureModel();
    if (this.mode == JSSidePanelRender.MODE_QUERY) {
        AQ.send("aqloadobjectallowaction.0");
    }
    this.setMode(this.MODE_NORMAL);
    this.removeSidePanel();
    this.adjustSize();
    if (feature && feature.afterModeChange)
        feature.afterModeChange();
    this.repaintGridHeader();
}

// Private method: force the "repaint" of the grid header
JSSidePanelRender.repaintGridHeader = function () {
    if (document.all) {
        var gridBack = document.getElementById('jdeGridContainer0_1');
        if (gridBack != null) {
            GUTIL.updateClassInfo(gridBack, 'SPDummyClass', true);
            GUTIL.updateClassInfo(gridBack, 'SPDummyClass', false);
        }
    }
}

// Private method: build side panel frame from scratch.
// old dom structure
// e1PaneForm -- WebMenuBarFrame
//            -- efFormDiv
// new dom strcuture
// e1PaneForm - TABLE(SidePaneFrameWorkTable) - TR - TD(ID_E1_CONTAINER_TD) - DIV(WebMenuBarFrame)
//                                                                       - DIV(e1formDiv)
//                                                 - TD(SidePanelResizeTD)
//                                                 - TD(SidePanelContainerTD)    
JSSidePanelRender.renderSidePanelFrame = function () {
    var menuTable = document.getElementById("WebMenuBarFrame");
    var e1FormDiv = document.getElementById("e1formDiv");
    var e1PaneForm = e1FormDiv.parentNode;

    // new TABLE
    var layoutTB = document.createElement("TABLE");
    layoutTB.id = JSSidePanelRender.ID_FRAME_TABLE;
    layoutTB.className = "SPFrameLayoutTable";
    layoutTB.setAttribute('cellPadding', 0);
    layoutTB.setAttribute('cellSpacing', 0);
    e1PaneForm.appendChild(layoutTB);

    // new TR
    var tr = layoutTB.insertRow(-1);

    // new TD for e1FormTD
    var e1FormTD = tr.insertCell(-1);
    e1FormTD.className = "SPFrameLayoutCell";
    e1FormTD.vAlign = "top";
    e1FormTD.id = JSSidePanelRender.ID_E1_CONTAINER_TD;
    if (menuTable) // power form does not have menu bar
        e1FormTD.appendChild(menuTable);
    e1FormTD.appendChild(e1FormDiv);

    // new TD for sidePanel resize TD
    var sidePanelResizeTD = tr.insertCell(-1);
    sidePanelResizeTD.style.width = JSSidePanelRender.RESIZE_TD_WIDTH + 'px';
    sidePanelResizeTD.style.height = "100%";
    var sidePanelResizeDiv = document.createElement("DIV");
    sidePanelResizeTD.appendChild(sidePanelResizeDiv);
    sidePanelResizeDiv.className = "SPResizeDiv";
    sidePanelResizeDiv.style.width = JSSidePanelRender.RESIZE_TD_WIDTH + 'px';
    sidePanelResizeDiv.id = JSSidePanelRender.ID_RESIZE_DIV;
    sidePanelResizeDiv.style.height = sidePanelResizeTD.offsetHeight + "px";
    sidePanelResizeDiv.onmouseover = JSSidePanelRender.setResizingColor;
    sidePanelResizeDiv.onmouseout = JSSidePanelRender.resetResizingColor;
    sidePanelResizeDiv.onmousedown = JSSidePanelRender.startResizing;

    // new TD for side panel
    var e1SideTD = tr.insertCell(-1);
    e1SideTD.className = 'SPBody';
    e1SideTD.id = JSSidePanelRender.ID_SIDE_CONTAINER_TD;
    setTimeout(setExitMenuTopValue, 10);
    return e1SideTD;
}

// Private method: detach the size panel frame work for close action
JSSidePanelRender.removeSidePanel = function () {
    var layoutTB = document.getElementById(JSSidePanelRender.ID_FRAME_TABLE);
    if (layoutTB) {
        var menuTable = document.getElementById("WebMenuBarFrame");
        var e1FormDiv = document.getElementById("e1formDiv");
        var e1PaneForm = layoutTB.parentNode;
        if (menuTable)
            e1PaneForm.appendChild(menuTable);
        e1PaneForm.appendChild(e1FormDiv);
        e1PaneForm.removeChild(layoutTB);
        setTimeout(setExitMenuTopValue, 10);
    }
}

// Private method: only remove the content in side of the side panel frame work for replacing it later with different content
JSSidePanelRender.removeSidePanelContent = function () {
    var e1SideTD = document.getElementById(JSSidePanelRender.ID_SIDE_CONTAINER_TD);
    JSSidePanelRender.removeAllChildren(e1SideTD);
    return e1SideTD;
}

// Private method: adjust size of E1 and Side Panel based on the model and trigger full form elongation logic
JSSidePanelRender.adjustSize = function () {
    JSSidePanelRender.updateSidePanelWidth();
    JSSidePanelRender.updateE1Width();
    if (window.JSCompMgr)
        JSCompMgr.onLoad("", true);
}

// Private method: render the contontent of the side panel based on the mode
JSSidePanelRender.renderSidePanelContent = function (e1SideTD) {
    var sb = new PSStringBuffer();
    sb.append(
      "<div class='SPBody AccessibilityContainer' id='").append(JSSidePanelRender.ID_SIDE_CONTENT_DIV).append("'>\
          <table class='SPLayoutTable' cellSpacing=0 cellPadding=0>\
            <tr>\
              <td>"
            );

    this.renderSidePanelTabStub(sb);
    sb.append(
             "</td>\
            </tr>\
            <tr>\
              <td valign='top'>");

    this.renderSidePanelMenu(sb);
    sb.append("</td>\
            </tr>\
            <tr>\
              <td>");

    this.renderSidePanelForm(sb);

    sb.append(
             "</td>\
            </tr>\
          </table>\
        </div>"
            );
    e1SideTD.innerHTML = sb.toString();

}

// Private method: render the tab stub area in the side panel
JSSidePanelRender.renderSidePanelTabStub = function (sb) {
    sb.append(
    "<table id='SPTabStub' class='SPLayoutTable' cellSpacing=0 cellPadding=0>\
      <tbody>\
        <tr>"
            );

    var tabsArray = this.getActiveFeatureModel().getTabsArray();
    //var tabStubWidth = 90/tabsArray.length + "%"; // estimate the percentage width of a tab after discount some padding td

    for (var i = 0; i < tabsArray.length; i++) {
        var tab = tabsArray[i];
        if (tab.isActive) // active
        {
            var tabLeftImg = JSSidePanelRender.isRTL ? window['E1RES_share_images_tab_start_a_rtl_png'] : window['E1RES_share_images_tab_start_a_png'];
            var tabRightImg = JSSidePanelRender.isRTL ? window['E1RES_share_images_tab_end_a_rtl_png'] : window['E1RES_share_images_tab_end_a_png'];
        }
        else // inactive
        {
            var tabLeftImg = JSSidePanelRender.isRTL ? window['E1RES_share_images_tab_start_n_rtl_png'] : window['E1RES_share_images_tab_start_n_png'];
            var tabRightImg = JSSidePanelRender.isRTL ? window['E1RES_share_images_tab_end_n_rtl_png'] : window['E1RES_share_images_tab_end_n_png'];
        }

        sb.append("<td><table class='tabTable");
        if (JSSidePanelRender.isRTL)
            sb.append("_rtl");
        sb.append(" SPLayoutTable'");
        if (tab.isActive)
            sb.append("style='position:relative;z-index:100;'");
        else
            sb.append("style='position:relative;z-index:0;'");
        sb.append("><tbody><tr>");

        sb.append("<td class='SPLayoutCell'><table cellpadding='0' cellspacing='0' ");
        if (tab.isActive)
            sb.append("class='tabTableSelected'>");
        else
            sb.append("class='tabTable'>");
        sb.append("<tbody><tr><td width='100%' nowrap align='center'><div>");
        if (tab.isActive)
            sb.append("<span class='ActiveTabLink'>").append(tab.title).append("</span>");
        else {
            sb.append("<div class=RINoWrap title='").append(tab.title);
            sb.append("' valign='bottom'").append(tab.eventHandler).append(">");
            sb.append("<a tabindex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.click();event.stopPropagation();event.preventDefault();};' class='tablink'>").append(tab.title).append("</div>");
        }
        sb.append("</div></td></tr></tbody></table></td>");
        sb.append("</tr></tbody></table></td>");
    }
    sb.append("<td class='notabfiller' width=100%></td>");
    sb.append("</tr></tbody></table>");
}

// Private method: render the menu item of menu bar in the side panel
JSSidePanelRender.renderSidePanelMenuItem = function (sb, id, title, outImg, overImg, eventHandler, hideForAccessibility) {
    sb.append("<td class='SPMenuIconTD'>");
    JSSidePanelRender.renderMotionImg(sb, id, title, outImg, overImg, eventHandler, hideForAccessibility);
    sb.append("</td>");
}

// Private method: render the text menu item of menu bar in the side panel
JSSidePanelRender.renderSidePanelTextItem = function (sb, menu) {
    sb.append("<td class='SPMenuTextTD'>");
    sb.append("<input type='text' ");
    if (menu.id)
        sb.append("id='" + menu.id + "' ");
    if (menu.placeholder)
        sb.append("placeholder = '" + menu.placeholder + "' ");
    sb.append("></input>");

    sb.append("</td>");
}

// Private method: render the menu bar in the side panel
JSSidePanelRender.renderSidePanelMenu = function (sb) {
    var menusArray = this.getActiveFeatureModel().getMenusArray();
    var isNativeContainer = navigator.userAgent.toUpperCase().indexOf("JDECONTAINER") > -1;

    sb.append("<table id='SPMenuBarTable' class='SPMenuTable'><tr>");
    for (var i = 0; i < menusArray.length; i++) {
        var menu = menusArray[i];
        if (menu.type && menu.type == 'text') {
            this.renderSidePanelTextItem(sb, menu);
        }
        else {
            this.renderSidePanelMenuItem(sb, menu.id, menu.title, menu.outImg, menu.overImg, menu.onclick, menu.hideForAccessibility);
        }
    }

    sb.append("<td>&nbsp;</td>");
    modeNow = JSSidePanelRender.getMode();
    var aboutFuncAction = 'alert(\"No contextual information available at this time.\")'; //Default to the default about function call
    var isQueryMode = JSSidePanelRender.isQueryMode(modeNow);
    var hover = "No hover info";
    if (isQueryMode) // mode 4
    {
        aboutFuncAction = 'AQ.about()';
        hover = window['infoAqTip'];
    }
    else if (modeNow == JSSidePanelRender.MODE_GRID_FORMAT) // mode 8
    {
        aboutFuncAction = 'GF.about()';
        hover = window['infoGfTip'];
    }
    else if (modeNow == JSSidePanelRender.MODE_WATCHLIST) // mode 5
    {
        aboutFuncAction = 'WL.about()';
        hover = window['infoWlTip'];
    }
    else if (modeNow == JSSidePanelRender.MODE_REPORT_NEW || modeNow == JSSidePanelRender.MODE_REPORT_EDIT || modeNow == JSSidePanelRender.MODE_LAYOUT)//all other modes are for use with OVR (0-3)  TODO: Verify this.
    {
        aboutFuncAction = 'about(\"OVRInfo\")';
        hover = window['infoOvrTip'];
    } else if (modeNow === JSSidePanelRender.MODE_PERSONALIZE_FORM) {
        aboutFuncAction = 'PF.about()';
        hover = window['infoPfTip'];
    }
    else if (modeNow == JSSidePanelRender.MODE_ENHANCED_ATTACHMENT) {
        aboutFuncAction = 'about(\"EATTInfo\")';
        hover = window['infoEATTTip'];
    }

    //Set the about icon for the Advanced query or One View reporting
    sb.append("<td class='SPMenuInfoIconTD' align=right>");
    JSSidePanelRender.renderMotionImg(sb, 'SidePanelAbout', hover, window['E1RES_share_images_ulcm_info_ena_png'], window['E1RES_share_images_ulcm_info_hov_png'], aboutFuncAction);
    sb.append("</td>");

    var closeImage = window['E1RES_share_images_ulcm_close_ena_png'];
    var closeMOImage = window['E1RES_share_images_ulcm_close_hov_png'];

    sb.append("<td class='SPMenuCloseIconTD' align=right>");
    JSSidePanelRender.renderMotionImg(sb, 'SidePanelClose', 'Close Side Panel', closeImage, closeMOImage, 'JSSidePanelRender.reqClose(this)');
    sb.append("</td></tr></table>");
}

// Private method: render the form content of feature
JSSidePanelRender.renderSidePanelForm = function (sb) {
    var height = JSSidePanelRender.getSidePanelHeight();
    var width = JSSidePanelRender.getSidePanelWidth();
    sb.append("<div id='").append(JSSidePanelRender.ID_SIDE_FORM_DIV).append("' class='SPFormDiv' style='width:").append(width).append("px;height:").append(height).append("px;overflow:auto' >");
    this.getActiveFeatureModel().getHTMLForTabBody(sb);
    sb.append("</div>");
}

// Public method: render prompt popup window for user input a name given the id of the input field, the default value, displayed title
// ok and change handler of the input
JSSidePanelRender.renderPromptForName = function (doc, sourceElementId, popupType, labels, maxLength, okHandler, changeHandler, closeHandler) {
    var ctrl = doc.getElementById(sourceElementId);
    if (window.okWin) // reuse
    {
        window.okWin.show(ctrl);
    }
    else // create a new popup for the first time
    {

        this.okHandler = okHandler;

        var eventListener = {};
        if (closeHandler)
            eventListener.onClose = closeHandler;
        else
            eventListener.onClose = function (e) { JSSidePanelRender.hideUIBlock(); };

        var okScript = "javascript:JSSidePanelRender.okPromptForName()";
        var onChangeScript = changeHandler;
        var keyDownScript = "if (event.keyCode == 13) {return JSSidePanelRender.okPromptForName(event)}; if(event.keyCode==27){document.getElementById(\'Close_Prompt\').click()}; ";
        var winWidth = JSSidePanelRender.PROMPT_WIDTH;
        var winHeight = JSSidePanelRender.PROMPT_HEIGHT;
        if (localStorage.getItem("userAccessibilityMode"))
            JSSidePanelRender.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
        //if the popupType is 1, show the approver note popup
        if (popupType == 1) {
            var confirmScript = "javascript:JSSidePanelRender.handlerForRequestPublishNotes(this);";
            var ieHanlderScript = "javascript:JSSidePanelRender.ieHandlerforRequestPubilshTextArea(this);";
            //Confirmation
            var title = labels[0];
            //you are request to publish the [UDOName], Do you want to continue?
            var udoMessage = labels[1];
            // (Click the checkbox to include comments about your [UDONAME].)"
            var udoOption = labels[2];
            //in case of request publish of UDO, approver note should be shown in textarea
            winWidth = 280;
            winHeight = 240;
            var framework = JSSidePanelRender.renderNotesPromptHTML(udoMessage, udoOption, onChangeScript, keyDownScript, okScript, confirmScript, title);
        }
        else if (popupType == 2) {
            // new name
            var defaultValue = labels[0];
            //Enter New Name:
            var title = labels[1];
            if (JSSidePanelRender.ACCESSIBILITY) {
                var framework = '&nbsp;<LABEL class="accessibility" for="' + JSSidePanelRender.ID_NAME_ENTERED + '">' + title + '</LABEL>\
          <INPUT class=textfield style="width:' + JSSidePanelRender.PROMPT_INPUT_WIDTH + 'px" id="' + JSSidePanelRender.ID_NAME_ENTERED + '" MAXLENGTH=' + maxLength + ' onchange="' + onChangeScript + '" onkeydown ="' + keyDownScript + '" value="' + defaultValue + '"></input>&nbsp;\
          <LABEL class="accessibility" for="ok">OK</LABEL>\
          <input type="button" class=button id="ok" value="OK" onClick="' + okScript + '";></input>';
            }
            else {
                var framework = '&nbsp;<INPUT class=textfield style="width:' + JSSidePanelRender.PROMPT_INPUT_WIDTH + 'px" id="' + JSSidePanelRender.ID_NAME_ENTERED + '" MAXLENGTH=' + maxLength + ' onchange="' + onChangeScript + '" onkeydown ="' + keyDownScript + '" value="' + defaultValue + '"></input>&nbsp;<input type="button" class=button id="ok" value="OK" onClick="' + okScript + '";></input>';
            }
        }
        var winTop = ctrl ? getAbsoluteTopPos(ctrl, true) : document.body.clientHeight / 3;
        var winLeft = ctrl ? getAbsoluteLeftPos(ctrl, true) : (document.body.clientWidth / 2 - JSSidePanelRender.PROMPT_WIDTH / 2);

        this.showUIBlock();
        this.okWin = createPopup("savePrompt", ADVANCED_QUERY, true, false, true, false, title, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, ctrl, framework, eventListener);
        var enterNameBox = doc.getElementById(JSSidePanelRender.ID_NAME_ENTERED);
        if (enterNameBox) {
            enterNameBox.focus();
            if (popupType != 1)
                enterNameBox.select();
        }
    }
}

JSSidePanelRender.renderNotesPromptHTML = function (udoMessage, udoOption, onChangeScript, keyDownScript, okScript, confirmScript, title) {
    var table_Notes = document.createElement("table");
    table_Notes.className = "SPNotesTable";
    var tabsRow = table_Notes.insertRow(0);
    var col = tabsRow.insertCell(0);
    col.className = "SPNotesTableTD";
    var label = document.createElement("label");
    label.style.display = "inline-block";
    label.innerHTML = udoMessage;
    col.appendChild(label);

    tabsRow = table_Notes.insertRow(1);
    col = tabsRow.insertCell(0);
    col.className = "SPNotesTableTD";
    sb = new PSStringBuffer();
    sb.append("<input type='checkbox' align='center' id='notes' style='vertical-align: middle;' onClick=");
    sb.append("javascript:JSSidePanelRender.handlerForRequestPublishNotes(this);");
    sb.append("></input>");
    if (JSSidePanelRender.ACCESSIBILITY) {
        sb.append("<label class='accessibility' for='notes'").append("'>").append('Confirmation Popup').append("</label>");
    }
    sb.append("<span style='vertical-align: middle;'>").append(udoOption).append("</span>");
    col.innerHTML = sb.toString();

    tabsRow = table_Notes.insertRow(2);
    col = tabsRow.insertCell(0);
    col.className = "SPNotesTableTD";
    var sb = new PSStringBuffer();
    if (JSSidePanelRender.ACCESSIBILITY) {
        sb.append("<LABEL class='accessibility' for=");
        sb.append(JSSidePanelRender.ID_NAME_ENTERED)
        sb.append(">");
        sb.append(title);
        sb.append("</LABEL>");
    }
    sb.append("<textarea rows='4' cols='").append(JSSidePanelRender.isIOS ? 20 : 30).append("' style='display:none;vertical-align: middle;margin:auto;overflow=auto' id='");
    sb.append(JSSidePanelRender.ID_NAME_ENTERED).append("'  ");
    if (onChangeScript) {
        sb.append(" onchange=\"").append(onChangeScript).append("\" ");
    }
    if (keyDownScript) {
        sb.append(" onkeydown=\"").append(keyDownScript).append("\" ");
    }
    sb.append(">");
    sb.append("</textarea>");
    col.innerHTML = sb.toString();

    tabsRow = table_Notes.insertRow(3);
    col = tabsRow.insertCell(0);
    col.className = "SPNotesTableTD";
    col.style.textAlign = "center";
    sb = new PSStringBuffer();
    //render <label> for Accessibility
    if (JSSidePanelRender.ACCESSIBILITY) {
        sb.append("<LABEL class='accessibility' for='Ok'>OK</LABEL>");
    }
    sb.append("<input type='button' align='center' id='Ok' value='OK' class='SPNotesButton' onClick=");
    sb.append(okScript);
    sb.append("></input>");
    if (JSSidePanelRender.ACCESSIBILITY) {
        sb.append("<LABEL class='accessibility' for='Cancel'>Cancel</LABEL>");
    }
    sb.append("<input type='button' id='Cancel' value='Cancel' class='SPNotesButton' style='margin-left: 10px;' onClick=");
    sb.append(confirmScript);
    sb.append("></input>");
    col.innerHTML = sb.toString();

    return table_Notes.outerHTML;
}

// Private event handler for ok button and it trigger the feature ok handler
JSSidePanelRender.okPromptForName = function (e) {
    var newName = document.getElementById(JSSidePanelRender.ID_NAME_ENTERED).value;

    if (this.okHandler(newName)) {
        this.okWin.onClose();
    }
    return GCMH.stopEventBubbling(e);
}

// Private event handler for Request Publish Pop Up
JSSidePanelRender.handlerForRequestPublishNotes = function (obj) {

    if (obj.value == 'Cancel') {
        this.okWin.onClose();
    } else {
        if (obj.checked) {
            document.getElementById(JSSidePanelRender.ID_NAME_ENTERED).style.display = "block";
        } else {
            document.getElementById(JSSidePanelRender.ID_NAME_ENTERED).style.display = "none";
        }
    }
}
// Public reused utility method: remall all children nodes from the given parent node
JSSidePanelRender.removeAllChildren = function (parentNode) {
    if (parentNode && parentNode.childNodes) {
        while (parentNode.childNodes.length > 0) {
            var child = parentNode.firstChild;
            parentNode.removeChild(child);
        }
    }
}

// Private method to return desired height for side panel
JSSidePanelRender.getSidePanelHeight = function () {
    var e1formDiv = document.getElementById("e1formDiv");
    var menuTable = document.getElementById("WebMenuBarFrame");
    var sidePanelMenuBar = document.getElementById("SPMenuBar");
    var sidePanelTabStub = document.getElementById("SPTabStub");
    var height = e1formDiv.offsetHeight
    + (menuTable ? menuTable.offsetHeight : 0)
    - (sidePanelMenuBar ? sidePanelMenuBar.offsetHeight : 22)
    - (sidePanelTabStub ? sidePanelTabStub.offsetHeight : 22)
    - (document.all ? 2 : 0); //browser padding
    return height;
}

// Private method to return desired height for side panel
JSSidePanelRender.getSidePanelWidth = function () {
    if (JSSidePanelRender.mode == JSSidePanelRender.MODE_NORMAL)
        return 0;
    else {
        if (JSSidePanelRender.sidePanelWidth < 0)
            JSSidePanelRender.sidePanelWidth = 0;
        return JSSidePanelRender.sidePanelWidth;
    }
}

// Public Event handler for start drag and drop the edge of side panel
JSSidePanelRender.startResizing = function (e) {
    var resizeDiv = document.getElementById(JSSidePanelRender.ID_RESIZE_DIV);
    resizeDiv.style.backgroundColor = "#74C4FF";
    // create the div. If exists then something is wrong so get out
    var elem = document.getElementById(JSSidePanelRender.ID_RESIZE_BLOCK);
    if (!elem) {
        elem = document.createElement("div");
        var parentTable = document.getElementById(JSSidePanelRender.ID_FRAME_TABLE);
        var sepRangeTop = parentTable.offsetTop;
        var sepRangeLeft = parentTable.offsetTop;
        var sepWidth = parentTable.offsetWidth;
        var sepVRange = parentTable.offsetHeight;
        var containerPos = parentTable;
        while (containerPos.offsetParent) {
            containerPos = containerPos.offsetParent;
            sepRangeTop += containerPos.offsetTop;
            sepRangeLeft += containerPos.offsetLeft;
        }

        elem.style.position = "absolute";
        //elem.style.left = sepRangeLeft + "px";
        elem.style.left = 300 + "px";
        elem.style.top = sepRangeTop + "px";
        elem.style.height = sepVRange + "px";
        elem.style.width = sepWidth + "px";
        elem.style.width = sepWidth + "px";
        elem.style.zIndex = 2000000001;
        elem.style.cursor = "w-resize";
        elem.style.visibility = "visible";
        elem.id = JSSidePanelRender.ID_RESIZE_BLOCK;
        elem.style.overflow = "hidden";
        // attach events for resize to div
        elem.onmousemove = JSSidePanelRender.resizing;
        elem.onmouseup = JSSidePanelRender.stopResizing;
        elem.onmouseout = JSSidePanelRender.stopResizing;
        // eat out all selection event
        var table = document.createElement("table");
        var tbody = document.createElement("tbody");
        var td = document.createElement("td");
        var tr = document.createElement("tr");
        var div = document.createElement("div");
        div.style.height = sepVRange + "px";
        div.style.width = sepWidth + "px";
        td.appendChild(div);
        tr.appendChild(td);
        tbody.appendChild(tr);
        table.appendChild(tbody);
        elem.appendChild(table);
        document.body.appendChild(elem);
    }

    elem.originalX = JSSidePanelRender.getPageX(e);
    return RIUTIL.stopEventBubbling(e);
}

// Event handler: drag and drop resizing of side panel
JSSidePanelRender.resizing = function (e) {
    var evt = e ? e : window.event;

    var maxClientWidth;
    var CARO = window.parent.CARO;

    if (document.getElementById("WebMenuBar"))
        maxClientWidth = document.getElementById("WebMenuBar").clientWidth;
    else {
        maxClientWidth = 250;

        if (CARO && CARO.carousel.side == CARO.EAST) {
            if (CARO.carousel.expanded == 1) {
                maxClientWidth = maxClientWidth - CARO.carousel.offsetWidth - 20;
            }
            else {
                maxClientWidth = maxClientWidth - 12;
            }
        }
    }
    if (JSSidePanelRender.isRTL && (evt.clientX >= (document.body.clientWidth - (maxClientWidth + 5 + JSSidePanelRender.RESIZE_TD_WIDTH))))
        JSSidePanelRender.stopResizing(e);
    else if (!JSSidePanelRender.isRTL && (evt.clientX <= (maxClientWidth + 5 + JSSidePanelRender.RESIZE_TD_WIDTH)))
        JSSidePanelRender.stopResizing(e);
    else {
        var resizeDiv = document.getElementById(JSSidePanelRender.ID_RESIZE_DIV);
        resizeDiv.style.backgroundColor = "#74C4FF";
        var diffX = JSSidePanelRender.getDiffX(e);
        if (JSSidePanelRender.isRTL)
            diffX = -diffX;
        JSSidePanelRender.resizeByDiffX(diffX);

        return RIUTIL.stopEventBubbling(e);
    }
}

// Event handler: drag and drop resizing of side panel
JSSidePanelRender.stopResizing = function (e) {
    var resizeDiv = document.getElementById(JSSidePanelRender.ID_RESIZE_DIV);
    resizeDiv.style.backgroundColor = "transparent";
    var elem = document.getElementById(JSSidePanelRender.ID_RESIZE_BLOCK);
    elem.parentNode.removeChild(elem);
    //RIUTIL.notifyResizeE1Pane();
    return RIUTIL.stopEventBubbling(e);
}

JSSidePanelRender.setResizingColor = function (e) {
    var elem = document.getElementById(JSSidePanelRender.ID_RESIZE_DIV);
    elem.style.backgroundColor = "#74C4FF";
}

JSSidePanelRender.resetResizingColor = function (e) {
    var elem = document.getElementById(JSSidePanelRender.ID_RESIZE_DIV);
    elem.style.backgroundColor = "transparent";
}

// Private: get the related horizontal distance between previous mousedown position and current mouse position
JSSidePanelRender.getDiffX = function (e) {
    var elem = document.getElementById(JSSidePanelRender.ID_RESIZE_BLOCK);
    var pageX = this.getPageX(e);
    var diffX = pageX - elem.originalX;
    elem.originalX = pageX;
    return diffX;
}

// Private: resize horizontally by difference 
JSSidePanelRender.resizeByDiffX = function (diffX) {
    var originalSPWidth = JSSidePanelRender.getSidePanelWidth();
    var newSPWidth = originalSPWidth - diffX;
    JSSidePanelRender.setSidePanelWidth(newSPWidth);
    JSSidePanelRender.adjustSize();
}

// Private method to set side panel width to model
JSSidePanelRender.setSidePanelWidth = function (width) {
    JSSidePanelRender.sidePanelWidth = width;
}

// Public method to set E1 from width and all related elements
JSSidePanelRender.updateE1Width = function () {
    var sidePanelwidth = JSSidePanelRender.getSidePanelWidth();
    var e1PaneDIV = document.getElementById("E1PaneDIV");
    var totalWidth = e1PaneDIV ? e1PaneDIV.offsetWidth : (JSSidePanelRender.isIOS ? window.innerWidth : document.body.offsetWidth);

    var width = totalWidth - sidePanelwidth - JSSidePanelRender.RESIZE_TD_WIDTH;
    var e1ContainerDiv = document.getElementById(JSSidePanelRender.ID_E1_CONTAINER_TD);
    var e1FormDiv = document.getElementById("e1formDiv");
    var menuTable = document.getElementById("WebMenuBarFrame");

    if (e1ContainerDiv)
        e1ContainerDiv.style.width = width + "px";

    if (menuTable != null) {
        menuTable.style.width = width + "px";
        width = Math.max(menuTable.offsetWidth, width); // when menu bar reach the minimal width, the e1 form does not have to be narrower than it.
    }

    if (e1FormDiv) {
        e1FormDiv.style.width = width + "px";
        // this code is to force a repaint of the grid to work around an IE "feature" that shows up in RTL.
        if (JSSidePanelRender.isRTL)
            e1FormDiv.style.width = width - 1 + "px";
    }
}

// Public method to set E1 from width and all related elements
JSSidePanelRender.updateSidePanelHeight = function () {
    var height = JSSidePanelRender.getSidePanelHeight();
    var spFormDiv = document.getElementById(JSSidePanelRender.ID_SIDE_FORM_DIV);
    spFormDiv.style.height = height + "px";
    var feature = this.getActiveFeatureModel();
    if (feature.onUpdateSidePanelHeight)
        feature.onUpdateSidePanelHeight();

}

// Private method to update view related to size panel width
JSSidePanelRender.updateSidePanelWidth = function () {
    var width = JSSidePanelRender.getSidePanelWidth();
    var spContainerTD = document.getElementById(JSSidePanelRender.ID_SIDE_CONTAINER_TD);
    var spContentDiv = document.getElementById(JSSidePanelRender.ID_SIDE_CONTENT_DIV);
    var spFormDiv = document.getElementById(JSSidePanelRender.ID_SIDE_FORM_DIV);
    var spMenuBarTable = document.getElementById("SPMenuBarTable");

    if (spContainerTD)
        spContainerTD.style.width = width + "px";

    if (spContentDiv)
        spContentDiv.style.width = width + "px";

    if (spFormDiv)
        spFormDiv.style.width = width + "px";

    if (spMenuBarTable)
        spMenuBarTable.style.width = width + "px";
}


// Private: get page x crossing browser
JSSidePanelRender.getPageX = function (e) {
    if (!e || e.pageX === undefined) {
        // IE8
        return this.getPageXIE(e);
    }

    return e.pageX;
}

// Private: get page x for IE
JSSidePanelRender.getPageXIE = function (e) {
    e = window.event;
    var elm = e.srcElement
    var x = e.offsetX;
    while (elm.offsetParent != null) {
        x += elm.offsetLeft;
        elm = elm.offsetParent;
    }
    x += elm.offsetLeft;
    return x;
}

// Public: hotkey handler
JSSidePanelRender.goToggleMode = function (mode) {
    if (this.mode == mode)
        JSSidePanelRender.reqClose();
    else
        JSSidePanelRender.reqOpen(mode)
}

// Public: to display a UI Block under popup window
JSSidePanelRender.showUIBlock = function () {
    if (window.blockPane == null) // create new
    {
        window.blockPane = new UIBlockingPane();
        window.blockPane.blockUI(document);
    }
    else // reuse the existing one
    {
        window.blockPane._showBlockingDiv(document);
    }
}

// Public: to hide the UI Block under popup window, and can be reused in the future.
JSSidePanelRender.hideUIBlock = function () {
    if (window.blockPane != null)
        window.blockPane._hideBlockingDiv(document);
}

// Public method to setup motion and no motion event hander for a image, need to work in portal as well.
// if overImg is null, we need to clear up event handler for mouseover and mouseout (for disabled mode)
// if eventHandler is null, do not override the existing onclick event handler
JSSidePanelRender.setMotionImage = function (image, outImg, overImg, eventHandler) {
    image.src = outImg;
    image.onmouseover = overImg ? JSSidePanelRender.onMouseOverEvent : "";
    image.onmouseout = overImg ? JSSidePanelRender.onMouseOutEvent : "";

    image.setAttribute('outImg', outImg);
    if (overImg)
        image.setAttribute('overImg', overImg);
    image.className = "clickableImage";
    if (eventHandler && !JSSidePanelRender.ACCESSIBILITY)
        image.onclick = eventHandler;
}
//Public method to set the event hander for a <anchor> tag for the accessibility user
JSSidePanelRender.setAnchorEventHandler = function (image, eventHandler) {
    if (JSSidePanelRender.ACCESSIBILITY) {
        if (image.parentNode)
            var anchorElem = image.parentNode;
        anchorElem.onclick = eventHandler;
    }
}

// Event Handler for mouse over of motion image
JSSidePanelRender.onMouseOverEvent = function (e) {
    if (!e)
        element = this;
    else
        element = GUTIL.getEventSource(e);

    if (element != null && element.tagName == 'IMG') {
        var imgSrc = element.getAttribute('overImg');
        if (imgSrc != null && imgSrc != this.src) {
            element.src = imgSrc;

        }
    }
}

// Event Handler for mouse out of motion image
JSSidePanelRender.onMouseOutEvent = function (e) {
    if (!e)
        element = this;
    else
        element = GUTIL.getEventSource(e);

    if (element != null && element.tagName == 'IMG') {
        var imgSrc = element.getAttribute('outImg');
        if (imgSrc != null && imgSrc != this.src) {
            element.src = imgSrc;
        }
    }
}

// Public method: render a click image with motion.
JSSidePanelRender.renderMotionImg = function (sb, id, title, outImg, overImg, eventHandler, hideForAccessibility) {
    if (JSSidePanelRender.ACCESSIBILITY) {
        sb.append("<a role=presentation onclick='javascript:").append(eventHandler).append("' style='text-decoration:none;'>");
    }
    sb.append("<img tabIndex=0 onkeydown='if(event.keyCode==32 || event.keyCode==13){this.click()};' class='clickableImage' ");
    if (id)
        sb.append("id='").append(id).append("' ");
    sb.append("src='").append().append(outImg).append("' ")
    if (!JSSidePanelRender.ACCESSIBILITY || !hideForAccessibility)
        sb.append("role=button title='").append(title).append("' alt='").append(title).append("' ");
    else
        sb.append("role=presentation alt='' "); // hide this find button, we do not want JAWS user to be confused with two find buttons.

    sb.append("onmouseover='JSSidePanelRender.onMouseOverEvent(event)' onmouseout='JSSidePanelRender.onMouseOutEvent(event)'");
    if (!JSSidePanelRender.ACCESSIBILITY) {
        sb.append(" onclick='").append(eventHandler).append("' ");
    }
    sb.append("outImg='").append(outImg).append("' ");
    sb.append("overImg='").append(overImg).append("' ");
    sb.append(">");
    if (JSSidePanelRender.ACCESSIBILITY) {
        sb.append("</a>");
    }
}