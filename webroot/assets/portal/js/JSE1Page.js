var CHP_SP;
if (CHP_SP == null) {
    CHP_SP = new function () { }
    CHP_SP.activeE1page = {};
    CHP_SP.nameList = {};

    //security
    CHP_SP.userHasCreateAuthority = false;
    CHP_SP.userHasPublishAuthority = false;
    CHP_SP.userHasReserveAuthority = false;

    CHP_SP.isRTL = (document.documentElement.dir == "rtl");
    CHP_SP.BLANK_E1PAGE_ID = "NEW";
    CHP_SP.ICON_OFFSET_X = CHP_SP.isRTL ? 16 : -16;
    CHP_SP.ICON_OFFSET_Y = 5;
    CHP_SP.SEP_LINE = "\n";
    CHP_SP.SEP_CHAR = "|";
    CHP_SP.MANAGER_SERVICE = "E1PageManagerService";
    CHP_SP.FILEUPLOADER_SERVICE = "FileUpload";

    // all hard coded id name and attribute name
    CHP_SP.ATT_E1PINFOTOP = "E1PInfoTop";
    CHP_SP.ID_CONTENT_TABLE = "CHPContentTable";
    CHP_SP.ID_REQUEST_PUBLISH_ICON = "E1PRequestPublishIcon";
    CHP_SP.ID_RESERVE_ICON = "E1PReserveIcon";
    CHP_SP.ID_SAVE_ICON = "E1PSaveIcon";
    CHP_SP.ID_SAVE_AS_ICON = "E1PSaveAsIcon";
    CHP_SP.ID_DELETE_ICON = "E1PDeleteIcon";
    CHP_SP.ID_NOTES_ICON = "E1PNotesIcon";
    CHP_SP.ID_TRANSLATE_ICON = "E1PTranslateIcon";
    CHP_SP.ID_PREVIEW_BUTTON = "E1PPreviewButton";
    CHP_SP.ID_VALIDATE_BUTTON = "E1PValidateButton";

    CHP_SP.ID_E1PAGESLIST = "E1PList";
    CHP_SP.ID_MESSAGE_DIV = "E1PMessageDiv";
    CHP_SP.ID_OBJECT_NAME = "E1PObjectName";
    CHP_SP.ID_RESERVED_BY = "E1PReservedBy";
    CHP_SP.ID_DESCRIPTION = "E1PDescription";
    CHP_SP.ID_LANG_CODE = "E1PlangCode";
    CHP_SP.ID_LANG_CODE_CONTAINER = "E1PlangCodeContainer";
    CHP_SP.ID_LANG_LIST = "E1PLangList";
    CHP_SP.ID_LANG_LIST_CONTAINER = "E1PLangListContainer";
    CHP_SP.ID_PROD_CODE = "E1PprodCode";
    CHP_SP.ID_PAGE_TITLE_CONTAINER = "E1PPageTitleContainer";
    CHP_SP.ID_PAGE_TITLE = "E1PpageTitle";
    CHP_SP.NAME_PAGE_TYPE = "E1PpageType";
    CHP_SP.ID_PAGE_TYPE_FIELDSET = "E1PpageTypeFieldset";
    CHP_SP.ID_PAGE_TYPE_LEGEND = "E1PpageTypeLegend";
    CHP_SP.PAGE_TYPE_URL = 1;
    CHP_SP.PAGE_TYPE_HTMLCONTENT = 0;
    CHP_SP.ID_UPLOAD_CONTAINER = "E1PuploadContainer"
    CHP_SP.ID_FILEUPLOAD = "E1PfileUpload";
    CHP_SP.ID_PAGE_URL = "E1PURLField";
    CHP_SP.ID_UPLOAD_BUTTON = "E1PBtnLoad";
    CHP_SP.ID_DOWNLOAD_BUTTON = "E1PBtnDownload";
    CHP_SP.ID_LANG_CODE_DESC = "E1PlangCodeDesc";
    CHP_SP.ID_LANG_CODE_IMG = "imgE1PlangCode";
    CHP_SP.ID_LANG_CODE_VA = "vaE1PlangCode";
    CHP_SP.ID_PROD_CODE_DESC = "E1PprodCodeDesc";
    CHP_SP.ID_PROD_CODE_IMG = "imgE1PprodCode"
    CHP_SP.ID_PROD_CODE_VA = "vaE1PprodCode"
    CHP_SP.ID_LANG_LIST_DESC = "E1PlangListDesc";

    CHP_SP.ID_NAME_ENTERED = "SPNameEntered";
    CHP_SP.PROMPT_WIDTH = 257;
    CHP_SP.PROMPT_HEIGHT = (WebObjectUtil.isIOS ? 70 : 50) + 10;
    CHP_SP.PROMPT_INPUT_WIDTH = CHP_SP.PROMPT_WIDTH - 60;
    CHP_SP.TAB_STUB_WIDTH = 135;

    CHP_SP.CMD_INIT = "initContants";
    CHP_SP.CMD_SAVE = "save";
    CHP_SP.CMD_SAVEAS = "saveas";
    CHP_SP.CMD_GETLIST = "getList";
    CHP_SP.CMD_GETBYNAME = "loadE1PbyName";
    CHP_SP.CMD_GETBYID = "loadE1PbyId";
    CHP_SP.CMD_PUBLISH = "publish";
    CHP_SP.CMD_REPUBLISH = "republish";
    CHP_SP.CMD_RESERVE = "reserve";
    CHP_SP.CMD_UNRESERVE = "unreserve";
    CHP_SP.CMD_DELETE = "delete";
    CHP_SP.CMD_UPLOAD = "upload";
    CHP_SP.CMD_DOWNLOAD = "downloadContent";
    CHP_SP.CMD_PREVIEW = "viewContent";
    CHP_SP.CMD_TRANSLATE = "addLanguageRecord";
    CHP_SP.CMD_DEL_TRANSLATE = "deleteLanguageRecord";
    CHP_SP.CMD_VIEWINFO = "showInfo";
    CHP_SP.CMD_VISUALASSIST = "visualAssist";
    CHP_SP.CMD_VALIDATE_UDC = "validateUDC";
    CHP_SP.CMD_VACOMPLETED = "vaComplete";
    CHP_SP.CMD_BROWSERCLOSE = "browserClose";
    CHP_SP.CMD_GETLASTSAVEDIMAGE = "getLastNewImage";
    CHP_SP.CMD_GETIMAGEBYNAME = "getImageByName";

    CHP_SP.DIALOG_WIDTH = 190;
    CHP_SP.DIALOG_HEIGHT = 62;
    CHP_SP.BIG_DIALOG_HEIGHT = 90;
    CHP_SP.NAME_LENGTH = 30;
    CHP_SP.UDO_REQUESTOR_NOTE = "Requestor Note";
    CHP_SP.ACCESSIBILITY = false;
    if (localStorage.getItem("userAccessibilityMode"))
        CHP_SP.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;

    CHP_SP.ID_DESIGN_ACTIONS = "CHPDesignActions";
    CHP_SP.MANAGE_HOME_PAGES = "Manage Home Pages";
    CHP_SP.CLASS_ENABLED_DESIGN_ACTIONS = "CHPEnabledDesignActions";
    CHP_SP.CLASS_DISABLED_DESIGN_ACTIONS = "CHPDisabledDesignActions";
    CHP_SP.ID_ADD_ICON = "CHPAdd";
    CHP_SP.ID_LAYOUT_SELECTION_TOP = "CHPLayoutSelectionTop";
    CHP_SP.ID_LAYOUT_SELECTION_FORM = "CHPLayoutSelectionForm";
    CHP_SP.ID_LAYOUT_SELECTION_SPAN = "CHPLayoutSelectionSpan";
    CHP_SP.ID_SAVE_LAYOUT = "CHPSaveLayoutButton";
    CHP_SP.ID_SAVE_AS_LAYOUT = "CHPSaveAsLayoutButton";
    CHP_SP.ID_DESIGN_ACTIONS = "CHPDesignActions";
    CHP_SP.NAME_ACTION = "CHPAction";
    CHP_SP.ANIMATION_DURATION = 500;
    CHP_SP.MANAGEMENT_PANE_OPEN = false;
    CHP_SP.ISREADONLYMODE = false;
    CHP_SP.EDITMODE = 1;
    CHP_SP.PREVIEWMODE = 2;
    CHP_SP.MODE = 0;
    CHP_SP.PRODCODE_DEFAULT = "";
    CHP_SP.ID_RESIZE_DIV = "E1PResizeDiv";
    CHP_SP.ID_RESIZE_BLOCK = "E1PResizeBlock";
    CHP_SP.ID_FRAME_TABLE = "CHP_SPFrameworkTable";
    CHP_SP.ID_SIDE_CONTAINER_TD = "E1PSP_ContainerTD";
    CHP_SP.ID_E1_CONTAINER_TD = "E1P_ContainerTD";
    CHP_SP.RESIZE_TD_WIDTH = 3;
    CHP_SP.sidePanelWidth = 350;
    CHP_SP.originalSPWidth = null;
    CHP_SP.vaWindow = null;
    CHP_SP.prodError = null;
    CHP_SP.langError = null;
    CHP_SP.ISERROR = false;

    CHP_SP.isImage = false;
    CHP_SP.iconSecStartId = "";
    CHP_SP.iconSecEndId = "";
    CHP_SP.iconCurSecIndex = 0;
    CHP_SP.udoIconClicked = false;
}

CHP_SP.init = function () {
    CHP_SP.constants = new Object();

    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_INIT);
    if (CHP_SP.isImage) {
        e1URL.setParameter("isImage", CHP_SP.isImage);
    }
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.initConstants, false);
}

CHP_SP.viewInfo = function () {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_VIEWINFO);
    if (CHP_SP.isImage) {
        e1URL.setParameter("getImage", true);
        if (document.getElementById(CHP_SP.BLANK_E1PAGE_ID).getAttribute("omwObjectName") != null) {
            e1URL.setParameter("name", document.getElementById(CHP_SP.BLANK_E1PAGE_ID).getAttribute("omwObjectName"));
            e1URL.setParameter("user", document.getElementById(CHP_SP.BLANK_E1PAGE_ID).getAttribute("user"));
        }
        else {
            e1URL.setParameter("name", CHP_SP.activeE1page.omwObjectName);
            if (CHP_SP.activeE1page.user != "") {
                e1URL.setParameter("user", CHP_SP.activeE1page.user);
            }
        }
    }
    else {
        e1URL.setParameter("id", CHP_SP.activeE1page.id);
        e1URL.setParameter("langCode", CHP_SP.activeE1page.langCode);
    }
    window.open(e1URL.toString(), "", "width=400,height=435,scrollbars=1,resizable=yes,titlebar");
}

CHP_SP.onExitProductCode = function () {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_VALIDATE_UDC);
    e1URL.setParameter("id", CHP_SP.activeE1page.id);
    //    if(CHP_SP.isImage)
    //    {
    //        document.getElementById(CHP_SP.ID_PROD_CODE_IMG).style.display = 'none';
    //    }

    var udcValue = document.getElementById(CHP_SP.ID_PROD_CODE).value;
    if (CHP_SP.isImage || udcValue != CHP_SP.activeE1page.prodCode) {
        CHP_SP.activeE1page.prodCode = udcValue;
        e1URL.setParameter("UDC_Type", "SY");  // for prod code (DD Item) 
        e1URL.setParameter("UDC_ObjectType", "E1PAGE");
        e1URL.setParameter("UDC_SYValue", "98");
        e1URL.setParameter("UDC_RTValue", "SY");
        e1URL.setParameter("UDC_KYValue", udcValue);
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.postUDCValidate, true);
    }
}

CHP_SP.focusOnProductCode = function () {
    /*
    if(CHP_SP.isImage)
    {
    document.getElementById(CHP_SP.ID_PROD_CODE_IMG).style.display = 'inline';
    }*/
}

CHP_SP.onExitLangCode = function () {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_VALIDATE_UDC);
    e1URL.setParameter("id", CHP_SP.activeE1page.id);

    var langValue = document.getElementById(CHP_SP.ID_LANG_CODE).value;
    if (langValue != CHP_SP.activeE1page.langCode) {
        CHP_SP.activeE1page.langCode = langValue;
        e1URL.setParameter("UDC_Type", "LNGP");  // for prod code (DD Item) 
        e1URL.setParameter("UDC_ObjectType", "E1PAGE");
        e1URL.setParameter("UDC_SYValue", "01");
        e1URL.setParameter("UDC_RTValue", "LP");
        e1URL.setParameter("UDC_KYValue", langValue);
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.postUDCValidate, true);
    }
}

CHP_SP.postUDCValidate = function (container, retStr) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);

    var data = null;
    if (retStr != null) {
        data = eval(retStr);
    }

    var elemType = null;
    var elemDesc = null;
    var udcType = "";
    if (data != null) {
        udcType = data.UDC_Type;
        if (udcType == "SY") {
            elemType = document.getElementById(CHP_SP.ID_PROD_CODE);
            elemDesc = window.document.getElementById(CHP_SP.ID_PROD_CODE_DESC);
        }
        else {
            elemType = window.document.getElementById(CHP_SP.ID_LANG_CODE);
            elemDesc = window.document.getElementById(CHP_SP.ID_LANG_CODE_DESC);
        }
        var errorCode = data.UDC_ErrorCode;
        if (errorCode != null && errorCode != "null") {
            var errorDesc = data.UDC_ErrorDesc;
            elemDesc.innerHTML = " ";
            if (udcType == "SY") {
                CHP_SP.prodError = errorDesc;
            }
            else {
                CHP_SP.langError = errorDesc;
            }
        }
        else {
            var udcValue = data.UDC_KYValue;
            var udcDesc = data.UDC_DL01Value;
            elemType.value = udcValue;
            elemDesc.innerHTML = udcDesc;
            elemType.style.backgroundColor = "";
            if (udcType == "SY") {
                CHP_SP.activeE1page.prodCode = udcValue;
                CHP_SP.prodError = null;
            }
            else {
                CHP_SP.activeE1page.langCode = udcValue;
                CHP_SP.langError = null;
            }
        }
    }
    CHP_SP.udcCheckError(udcType);
}

CHP_SP.udcCheckError = function (udcType, errorIsSet) {
    if (CHP_SP.langError != null || CHP_SP.prodError != null) {
        CHP_SP.ISERROR = true;
        CHP_SP.updateControlEnablement();
        var elemType = null;
        if (udcType == "SY") {
            if (CHP_SP.prodError != null) {
                elemType = window.document.getElementById(CHP_SP.ID_PROD_CODE);
            }
            else {
                elemType = window.document.getElementById(CHP_SP.ID_LANG_CODE);
            }
        }
        else {
            if (CHP_SP.langError != null) {
                elemType = window.document.getElementById(CHP_SP.ID_LANG_CODE);
            }
            else {
                elemType = window.document.getElementById(CHP_SP.ID_PROD_CODE);
            }
        }
        elemType.style.backgroundColor = "#ff0000";
        CHP_SP.showErrorWindow(elemType, CHP_SP.prodError);
    }
    else {
        CHP_SP.ISERROR = false;
        if (!CHP_SP.isImage) {
            CHP_SP.updateControlEnablement();
        }
    }
}
CHP_SP.closeVisualAssistWindow = function () {
    if (CHP_SP.vaWindow != null && !CHP_SP.vaWindow.closed) {
        var win = CHP_SP.vaWindow;
        win.document.E1PUDOVisualAssist.submit();
        CHP_SP.vaWindow = null;
        //win.close();
    }
}

CHP_SP.browserCloseVAWindow = function () {
    if (CHP_SP.vaWindow != null && !CHP_SP.vaWindow.closed) {
        var win = CHP_SP.vaWindow;
        var elem = win.document.getElementById("E1PaneForm");
        var e1pURL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
        e1pURL.setURI(CHP_SP.MANAGER_SERVICE);
        e1pURL.setParameter("cmd", CHP_SP.CMD_BROWSERCLOSE);
        if (elem != null && elem != "undefined") {
            var value = elem.jdemafjasUID.defaultValue;
            if (value != null) {
                e1pURL.setParameter("UDC_MafUniqueID", value);
            }
        }
        WebObjectUtil.sendXMLReq("POST", null, e1pURL.toString(), null, false);
        win.close();
    }
    CHP_SP.vaWindow = null;
}

CHP_SP.vaProdCode = function () {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_VISUALASSIST);
    e1URL.setParameter("id", CHP_SP.activeE1page.id);
    e1URL.setParameter("ObjServiceName", CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("UDC_Type", "SY");  // for prod code (DD Item)   
    e1URL.setParameter("UDC_ObjectType", "E1PAGE");
    e1URL.setParameter("UDC_SYValue", "98");
    e1URL.setParameter("UDC_RTValue", "SY");
    var doc = document;
    var prodCode = doc.getElementById(CHP_SP.ID_PROD_CODE).value;
    var prodDesc = doc.getElementById(CHP_SP.ID_PROD_CODE_DESC).innerText;
    e1URL.setParameter("UDC_KYValue", prodCode);
    e1URL.setParameter("UDC_DL01Value", prodDesc);
    var openWindow = false;
    if (CHP_SP.vaWindow == null) {
        openWindow = true;
    }
    else if (CHP_SP.vaWindow.closed) {
        CHP_SP.vaWindow = null;
        openWindow = true;
    }
    else {
    }
    if (openWindow) {
        CHP_SP.vaWindow = window.open(e1URL.toString(), "E1PUDOVisualAssist", "width=700,height=650,scrollbars=1,resizable=yes,titlebar");
    }
}

CHP_SP.vaLangCode = function () {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_VISUALASSIST);
    e1URL.setParameter("id", CHP_SP.activeE1page.id);
    e1URL.setParameter("ObjServiceName", CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("UDC_SYValue", "01");
    e1URL.setParameter("UDC_RTValue", "LP");
    e1URL.setParameter("UDC_Type", "LNGP");  // for prod code (DD Item)   
    e1URL.setParameter("UDC_ObjectType", "E1PAGE");
    var doc = document;
    var langCode = doc.getElementById(CHP_SP.ID_LANG_CODE).value;
    var langDesc = doc.getElementById(CHP_SP.ID_LANG_CODE_DESC).innerText;
    e1URL.setParameter("UDC_KYValue", langCode);
    e1URL.setParameter("UDC_DL01Value", langDesc);
    var openWindow = false;
    if (CHP_SP.vaWindow == null) {
        openWindow = true;
    }
    else if (CHP_SP.vaWindow.closed) {
        CHP_SP.vaWindow = null;
        openWindow = true;
    }
    else {
    }
    if (openWindow) {
        CHP_SP.vaWindow = window.open(e1URL.toString(), "E1PUDOVisualAssist", "width=700,height=650,scrollbars=1,resizable=yes,titlebar");
    }
}

CHP_SP.goVaCompleteEnd = function (container, visualAssistString) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    CHP_SP.vaWindow = null;

    var data = null;
    if (container != null) {
        data = eval(container);
    }
    var udcType = "";
    if (data != null) {

        var udcValue = data.UDC_KYValue;
        var udcDesc = data.UDC_DL01Value;
        var elemType = null;
        var elemDesc = null;
        udcType = data.UDC_Type;

        if (udcType == "SY") {
            elemType = window.document.getElementById(CHP_SP.ID_PROD_CODE);
            elemDesc = window.document.getElementById(CHP_SP.ID_PROD_CODE_DESC);
            elemType.style.backgroundColor = "";
            CHP_SP.activeE1page.prodCode = udcValue;
            elemDesc.innerHTML = udcDesc;
            if (udcValue != elemType.value) {
                elemType.value = udcValue;
                CHP_SP.prodError = null;
            }
        }
        else {
            elemType = window.document.getElementById(CHP_SP.ID_LANG_CODE);
            elemDesc = window.document.getElementById(CHP_SP.ID_LANG_CODE_DESC);
            elemType.style.backgroundColor = "";
            CHP_SP.activeE1page.langCode = udcValue;
            elemDesc.innerHTML = udcDesc;
            if (udcValue != elemType.value) {
                elemType.value = udcValue;
                CHP_SP.langError = null;
            }
        }
    }
    CHP_SP.udcCheckError(udcType, false);

}

CHP_SP.initConstants = function (namespace, initConstantsResponseString) {
    var data = eval(initConstantsResponseString);
    CHP_SP.constants = data.constants;
    CHP_SP.objectType = data.objectType;
    CHP_SP.userHasCreateAuthority = data.createAuthority;
    CHP_SP.userHasPublishAuthority = data.publishAuthority;
    CHP_SP.userHasReserveAuthority = data.reserveAuthority;
    CHP_SP.canAddToProject = data.canAddToProject;
    CHP_SP.canAdd = data.canAdd;
    CHP_SP.resetActiveE1Page();
}

// Private: update preference to include or exclude "Manage pages" option.
CHP_SP.updatePersonalizationMenuForCHP = function () {
    var chpTable = document.getElementById('ManageCHPTable');
    if (chpTable && chpTable.style.display == '') {
        chpTable.style.display = 'none';
    }
    else if ((chpTable && chpTable.style.display == 'none') && CHP_SP.userHasCreateAuthority && !WebObjectUtil.isIOS) {
        chpTable.style.display = '';
    }
}

CHP_SP.goChangeLanguage = function (e) {
    e = e || window.event;
    var target = e.srcElement || e.target;
    var selectedLang = target.value;
    CHP_SP.activeE1page.langCode = selectedLang;
    CHP_SP.ISERROR = false;
    CHP_SP.langError = null;
    CHP_SP.prodError = null;
    if ("NEWLANG" == selectedLang) {
        CHP_SP.goAddTranslation();
        CHP_SP.togglePageTypeBoxDisablement(true);
    }
    else if ("" == selectedLang) {
        if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
            CHP_SP.activeE1page = CHP_SP.getActiveE1Page(CHP_SP.e1PageList);
        }
        CHP_SP.fetchE1PageById(CHP_SP.activeE1page.id);
    }
    else {
        CHP_SP.fetchE1PageByNameAndLang(CHP_SP.activeE1page.omwObjectName, selectedLang, CHP_SP.activeE1page.lineType);
    }
}

CHP_SP.populateAvailableLanguages = function (selectBox, languageListStr, defaultValue) {
    selectBox.onchange = CHP_SP.goChangeLanguage;
    for (var i = selectBox.options.length; i-- > 0; ) {
        selectBox.options[i] = null;
    }
    var indexOffset = 0;
    if (CHP_SP.activeE1page.isPrivate && !CHP_SP.activeE1page.isRequestPublished) {
        var option = document.createElement("option");
        option.value = "NEWLANG";
        option.id = "NEWLANG";
        option.name = "NEWLANG";
        var addTranslation = CHP_SP.constants.CHP_LABEL_ADD_TRANSLATION;
        option.appendChild(document.createTextNode(addTranslation));
        selectBox.appendChild(option);
        selectBox.options.selectedIndex = 1;
        indexOffset = 1;
    }
    else {
        selectBox.options.selectedIndex = 0;
    }

    if (!languageListStr)
        return;
    var languageArray = languageListStr.split(",");
    for (var i = 0; i < languageArray.length; i++) {
        var langCode = languageArray[i];
        var option = document.createElement("option");
        var langList = langCode.split("|");
        var item = langList[0];
        if (item == null || item.trim().length < 1) {
            item = "";
            option.value = "";
            option.id = "";
            option.name = "";
        }
        else {
            option.value = item;
            option.id = item;
            option.name = item;
        }

        var innertext = langList[1];
        option.appendChild(document.createTextNode(innertext));
        selectBox.appendChild(option);

        if (defaultValue == item)
            selectBox.options.selectedIndex = i + indexOffset;
    }
}

CHP_SP.resetActiveE1Page = function () {
    CHP_SP.activeE1page.id = CHP_SP.BLANK_E1PAGE_ID;
    CHP_SP.activeE1page.name = "";
    CHP_SP.activeE1page.tabName = "";
    CHP_SP.activeE1page.omwObjectName = "";
    CHP_SP.activeE1page.pageURL = "";
    CHP_SP.activeE1page.fileName = "";
    CHP_SP.activeE1page.prodCode = "";
    CHP_SP.activeE1page.langCode = "";
    CHP_SP.activeE1page.desc = "";
    CHP_SP.activeE1page.version = "";
    CHP_SP.activeE1page.lineType = "1";
    CHP_SP.activeE1page.isPrivate = true;
    CHP_SP.activeE1page.isCheckedOut = false;
    CHP_SP.activeE1page.isRework = false;
    CHP_SP.activeE1page.isRequestPublished = false;
    CHP_SP.activeE1page.reservedBy = "";
    CHP_SP.activeE1page.user = "";
    CHP_SP.activeE1page.notes = false;
    CHP_SP.activeE1page.hasToken = false;
    CHP_SP.activeE1page.tokenProjectName = "";
    CHP_SP.activeE1page.canAdd = CHP_SP.canAdd;
    CHP_SP.activeE1page.canAddToProject = CHP_SP.canAddToProject;
    CHP_SP.ISERROR = false;
    CHP_SP.langError = null;
    CHP_SP.prodError = null;
}

CHP_SP.goFetchE1pagesForPreview = function (user) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    if (CHP_SP.isImage) {
        e1URL.setParameter("getImage", true);
        e1URL.setParameter("cmd", CHP_SP.CMD_GETBYNAME);
        e1URL.setParameter("name", CHP_SP.objectForEditing);
        if (user != null) {
            e1URL.setParameter("user", user);
        }
    }
    else {
        e1URL.setParameter("cmd", CHP_SP.CMD_GETBYID);
        e1URL.setParameter("id", CHP_SP.objectForEditing);
    }
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    if (CHP_SP.isImage) {
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.updateActiveIcon, true);
    }
    else {
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.updatePanelForPreview, true);
    }
}

CHP_SP.updatePanelForPreview = function (container, retStr) {
    retStr = retStr.replace(/(\r\n|\n|\r)/gm, "");
    var fetchedE1Page = eval(retStr);
    if (fetchedE1Page) {
        CHP_SP.activeE1page = fetchedE1Page;
    }
    else {
        CHP_SP.activeE1page = null;
    }
    var items = [CHP_SP.activeE1page];
    if (!CHP_SP.isImage) {
        var e1pagesListBox = document.getElementById(CHP_SP.ID_E1PAGESLIST);
        removeChildren(e1pagesListBox);
        var privateOptionGroupTag = CHP_SP.createOptionGroupTag(e1pagesListBox, CHP_SP.constants.UDO_PERSONAL);
        CHP_SP.populateOptions(privateOptionGroupTag, items);

        CHP_SP.setActiveE1Page(CHP_SP.e1PageList, CHP_SP.objectForEditing);
        CHP_SP.setE1PageDropDownValueById();

        CHP_SP.updateActiveE1Page(container, retStr, true);
    }
}

CHP_SP.goFetchE1pagesList = function () {
    if (CHP_SP.isImage) {
        return;
    }
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_GETLIST);
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.updateE1pagesView, true);
}


CHP_SP.updateE1pagesView = function (containerId, e1PageModels) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    e1PageModels = e1PageModels.replace(/(\r\n|\n|\r)/gm, "");
    var data = eval(e1PageModels);
    CHP_SP.e1PageList = data[0].e1pages;
    CHP_SP.securityCacheDone = data[0].securityCacheDone;

    // empty the select tag first
    var e1pagesListBox = document.getElementById(CHP_SP.ID_E1PAGESLIST);
    removeChildren(e1pagesListBox);

    e1pagesListBox.onchange = CHP_SP.goChangeE1PageName;

    CHP_SP.separateE1pageslistNames(CHP_SP.e1PageList);
    CHP_SP.sortE1pagesListNames(CHP_SP.privateItems);
    CHP_SP.sortE1pagesListNames(CHP_SP.pendingApprovalItems);
    CHP_SP.sortE1pagesListNames(CHP_SP.reworkItems);
    CHP_SP.sortE1pagesListNames(CHP_SP.reservedItems);
    CHP_SP.sortE1pagesListNames(CHP_SP.publicItems);

    // populate private items
    // Always populate private options because we need to add "Create CHP_SP" in that section
    var privateOptionGroupTag = CHP_SP.createOptionGroupTag(e1pagesListBox, CHP_SP.constants.UDO_PERSONAL);
    CHP_SP.populateOptions(privateOptionGroupTag, CHP_SP.privateItems);

    // populate requestPublish (OR) pendingApproval Items 
    if (CHP_SP.pendingApprovalItems.length > 0) {
        var pendingApprovalOptionGroupTag = CHP_SP.createOptionGroupTag(e1pagesListBox, CHP_SP.constants.UDO_PENDING_APPROVAL);
        CHP_SP.populateOptions(pendingApprovalOptionGroupTag, CHP_SP.pendingApprovalItems);
    }
    //populate rework Items
    if (CHP_SP.reworkItems.length > 0) {
        var reworkOptionGroupTag = CHP_SP.createOptionGroupTag(e1pagesListBox, CHP_SP.constants.UDO_REWORK);
        CHP_SP.populateOptions(reworkOptionGroupTag, CHP_SP.reworkItems);
    }
    // populate reserved items
    if (CHP_SP.reservedItems.length > 0) {
        var reservedOptionGroupTag = CHP_SP.createOptionGroupTag(e1pagesListBox, CHP_SP.constants.UDO_RESERVED);
        CHP_SP.populateOptions(reservedOptionGroupTag, CHP_SP.reservedItems);
    }
    // populate public items
    if (CHP_SP.publicItems.length > 0) {
        var publicOptionGroupTag = CHP_SP.createOptionGroupTag(e1pagesListBox, CHP_SP.constants.UDO_SHARED);
        CHP_SP.populateOptions(publicOptionGroupTag, CHP_SP.publicItems);
    }

    CHP_SP.setE1PageDropDownValueById();

    if (CHP_SP.MODE > 0 && CHP_SP.objectForEditing != undefined && CHP_SP.objectForEditing.length > 0) {
        CHP_SP.setActiveE1Page(CHP_SP.e1PageList, CHP_SP.objectForEditing);
        CHP_SP.setE1PageDropDownValueById();
    }
    else {
        var currentSelection = CHP_SP.getCurrentlySelectedE1Page();
        if (currentSelection.id == CHP_SP.BLANK_E1PAGE_ID)
            CHP_SP.resetActiveE1Page();
        else
            CHP_SP.activeE1page = CHP_SP.getActiveE1Page(CHP_SP.e1PageList);
        CHP_SP.setDirty(false);
    }
    CHP_SP.goChangeE1PageName();
    CHP_SP.updateControlEnablement();
}

CHP_SP.getActiveE1Page = function (e1pagesList) {
    if (e1pagesList != null) {
        for (var i in e1pagesList) {
            if (CHP_SP.activeE1page.id == e1pagesList[i].id) {
                CHP_SP.activeE1page = e1pagesList[i];
            }
        }
    }
    return CHP_SP.activeE1page;
}

CHP_SP.setActiveE1Page = function (e1pagesList, oid) {
    if (e1pagesList != null) {
        for (var i in e1pagesList) {
            if (oid == e1pagesList[i].id) {
                CHP_SP.activeE1page = e1pagesList[i];
            }
        }
    }
}

CHP_SP.goChangeE1PageName = function (e) {
    CHP_SP.showMessageDiv(false);

    if (CHP_SP.proceedWithoutSave()) {
        //CHP_SP.validateInput();
        var currentE1page = CHP_SP.getCurrentlySelectedE1Page();
        if (currentE1page.id != CHP_SP.BLANK_E1PAGE_ID) {
            CHP_SP.fetchE1PageById(currentE1page.id);
        }
        else {
            var doc = document;
            var objectNameElem = doc.getElementById(CHP_SP.ID_OBJECT_NAME);
            var langCodeElem = doc.getElementById(CHP_SP.ID_LANG_CODE);
            var prodCodeElem = doc.getElementById(CHP_SP.ID_PROD_CODE);
            var prodCodeDesc = doc.getElementById(CHP_SP.ID_PROD_CODE_DESC);
            var langCodeDesc = doc.getElementById(CHP_SP.ID_LANG_CODE_DESC);
            var pageTitleContainer = doc.getElementById(CHP_SP.ID_PAGE_TITLE_CONTAINER);
            var pageTitleElem = doc.getElementById(CHP_SP.ID_PAGE_TITLE);
            var descElem = doc.getElementById(CHP_SP.ID_DESCRIPTION);
            var availableLangListContainer = doc.getElementById(CHP_SP.ID_LANG_LIST_CONTAINER);
            var availableLangListElem = doc.getElementById(CHP_SP.ID_LANG_LIST);

            objectNameElem.value = "";
            objectNameElem.disabled = false;
            langCodeElem.value = "";

            //Show existing language for E1Page                                    
            availableLangListContainer.style.display = "none";
            for (var i = availableLangListElem.options.length; i-- > 0; ) {
                availableLangListElem.options[i] = null;
            }

            var prodCodeImg = doc.getElementById(CHP_SP.ID_PROD_CODE_IMG);
            var langCodeContainer = doc.getElementById(CHP_SP.ID_LANG_CODE_CONTAINER);
            langCodeContainer.style.display = "none";

            prodCodeElem.disabled = false;
            prodCodeElem.value = "";
            prodCodeDesc.innerHTML = " ";
            prodCodeImg.style.display = "";
            langCodeElem.value = "";
            langCodeDesc.innerHTML = " ";
            pageTitleContainer.style.display = "none";
            pageTitleElem.value = "";
            descElem.value = "";
            descElem.disabled = false;

            CHP_SP.resetActiveE1Page();
            CHP_SP.setPageType(CHP_SP.PAGE_TYPE_URL);
            CHP_SP.goChangePageType(null, CHP_SP.PAGE_TYPE_URL, "");
            CHP_SP.updateControlEnablement();
            CHP_SP.showReservedBy();
        }
        CHP_SP.setDirty(false);
    }
    else {
        CHP_SP.setE1PageDropDownValueById();
        CHP_SP.updateControlEnablement();
    }

    if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
        CHP_SP.togglePageTypeBoxDisablement(false);
    }
    else {
        CHP_SP.togglePageTypeBoxDisablement(true);
    }


}

CHP_SP.fetchE1PageByNameAndLang = function (e1pageName, langCode, lineType) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_GETBYNAME);
    e1URL.setParameter("name", CHP_SP.HTMLDecoder(e1pageName));
    e1URL.setParameter("langCode", CHP_SP.HTMLDecoder(langCode));
    e1URL.setParameter("lineType", CHP_SP.HTMLDecoder(lineType));
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.updateActiveE1Page, true);
}

CHP_SP.fetchE1PageById = function (id) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_GETBYID);
    e1URL.setParameter("id", id);
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.updateActiveE1Page, true);
}

CHP_SP.updateActiveE1Page = function (container, retStr, isPreview) {
    if (isPreview) {
        CHP_SP.MODE = CHP_SP.PREVIEWMODE;
    }
    retStr = retStr.replace(/(\r\n|\n|\r)/gm, "");
    var data = eval(retStr);
    //var fetchedE1Page = data[0].e1pages[0];
    var fetchedE1Page = data;
    if (fetchedE1Page)
        CHP_SP.activeE1page = fetchedE1Page;
    else
        CHP_SP.activeE1page = null;

    var doc = document;
    var objectNameElem = doc.getElementById(CHP_SP.ID_OBJECT_NAME);
    var langCodeElem = doc.getElementById(CHP_SP.ID_LANG_CODE);
    var langDescElem = doc.getElementById(CHP_SP.ID_LANG_CODE_DESC);
    var langCodeContainer = doc.getElementById(CHP_SP.ID_LANG_CODE_CONTAINER);
    var availableLangListElem = doc.getElementById(CHP_SP.ID_LANG_LIST);

    var availableLangListContainer = doc.getElementById(CHP_SP.ID_LANG_LIST_CONTAINER);
    var prodCodeElem = doc.getElementById(CHP_SP.ID_PROD_CODE);
    var prodDescElem = doc.getElementById(CHP_SP.ID_PROD_CODE_DESC);
    var prodCodeImg = doc.getElementById(CHP_SP.ID_PROD_CODE_IMG);
    var pageTitleContainer = doc.getElementById(CHP_SP.ID_PAGE_TITLE_CONTAINER);
    var pageTitleElem = doc.getElementById(CHP_SP.ID_PAGE_TITLE);
    var descElem = doc.getElementById(CHP_SP.ID_DESCRIPTION);

    var prodCode_desc = doc.getElementById(CHP_SP.ID_PROD_CODE_DESC);
    objectNameElem.value = CHP_SP.HTMLDecoder(CHP_SP.activeE1page.name);
    objectNameElem.disabled = true;

    CHP_SP.activeE1page.prodCode = data.prodCode;
    prodCodeElem.value = data.prodCode;
    prodCodeElem.disabled = true;
    prodDescElem.innerHTML = data.prodCodeDesc;
    prodCodeElem.style.backgroundColor = "";
    prodCodeImg.style.display = "none";

    langCodeContainer.style.display = "none";
    langCodeElem.value = data.langCode;
    langCodeElem.style.backgroundColor = "";
    langDescElem.innerHTML = data.langCodeDesc;
    CHP_SP.activeE1page.langCode = data.langCode;


    CHP_SP.populateAvailableLanguages(availableLangListElem, CHP_SP.activeE1page.translation, CHP_SP.trimStr(CHP_SP.activeE1page.langCode));
    availableLangListContainer.style.display = "table-row";

    pageTitleElem.value = CHP_SP.HTMLDecoder(CHP_SP.activeE1page.tabName);
    pageTitleElem.disabled = false;

    if (CHP_SP.trimStr(CHP_SP.activeE1page.langCode).length > 0) {
        pageTitleContainer.style.display = "table-row";
    }
    else {
        pageTitleContainer.style.display = "none";
    }
    descElem.value = CHP_SP.HTMLDecoder(CHP_SP.activeE1page.desc);
    descElem.disabled = false;

    if (CHP_SP.activeE1page.pageType == CHP_SP.PAGE_TYPE_URL) {
        CHP_SP.setPageType(CHP_SP.PAGE_TYPE_URL);
        CHP_SP.goChangePageType(null, CHP_SP.PAGE_TYPE_URL, CHP_SP.activeE1page.pageURL);
    }
    else {
        CHP_SP.setPageType(CHP_SP.PAGE_TYPE_HTMLCONTENT);
        CHP_SP.goChangePageType(null, CHP_SP.PAGE_TYPE_HTMLCONTENT, CHP_SP.activeE1page.fileName);
    }
    //Preview should not allow an update in the editor, so disable controls      
    if (CHP_SP.MODE == CHP_SP.PREVIEWMODE || !CHP_SP.activeE1page.isPrivate || CHP_SP.activeE1page.isRework || CHP_SP.activeE1page.isRequestPublished) {
        prodCodeElem.disabled = true;
        prodCodeImg.style.display = "none";
        if (pageTitleElem)
            pageTitleElem.disabled = true;
        if (descElem)
            descElem.disabled = true;
        //disable Enter page URL input field
        var pageURLInput = doc.getElementById(CHP_SP.ID_PAGE_URL);
        if (pageURLInput)
            pageURLInput.disabled = true;

        var fileUpload = doc.getElementById(CHP_SP.ID_FILEUPLOAD);
        if (fileUpload) {
            fileUpload.disabled = true;
        }
        var uploadButton = doc.getElementById(CHP_SP.ID_UPLOAD_BUTTON);
        if (uploadButton) {
            uploadButton.className = "Prefbuttondisabled";
            uploadButton.disabled = true;
        }
    }

    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    CHP_SP.updateControlEnablement();
    if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
        CHP_SP.togglePageTypeBoxDisablement(false);
    }
    else {
        CHP_SP.togglePageTypeBoxDisablement(true);
    }
    CHP_SP.showReservedBy();
}

//false for enable//true for enable
CHP_SP.togglePageTypeBoxDisablement = function (state) {
    var options = document.getElementsByName(CHP_SP.NAME_PAGE_TYPE);
    if (options) {
        for (var i = 0, len = options.length; i < len; i++) {
            // disable page type radio buttons
            options[i].disabled = state;
        }
    }
}

CHP_SP.detectFrameBreaker = function (callbackfn) {
    CHP_SP.showMessageDiv(false);
    var doc = document;
    var targetUrl = doc.getElementById(CHP_SP.ID_PAGE_URL).value;
    if (!CHP_SP.isValidURL(targetUrl)) {
        CHP_SP.showErrorWindow(doc.getElementById(CHP_SP.ID_PAGE_URL), CHP_SP.constants.CHP_PAGEURL_INVALID);
        errorExists = true;
        return;
    }
    window.safeToEmbedded = false;
    window.currentCheckSafeToEmbed = 0;
    CHP_SP.isValidating = true;
    if (typeof callbackfn != 'undefined' && callbackfn != null) {
        setTimeout(function () {
            CHP_SP.checkSafeToEmbed(callbackfn);
        }, 1000);
    }
    else {
        setTimeout(CHP_SP.checkSafeToEmbed, 1000);
    }

    var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
    e1UrlCreator.setURI("owhtml/FBDection.jsp");

    // append preview mode flag to the embedded e1 form page launcher to prevent maflet from being reused.
    if (targetUrl.indexOf("/EmbeddedE1FormPageLaunch?") != -1)
        targetUrl += "&E1FormPreviewMode=true";

    e1UrlCreator.setParameter("url", targetUrl);
    var popupUrl = e1UrlCreator.toString();

    window.FBDetectionWin = window.open(e1UrlCreator.toString(), '_blank', 'location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=yes,dependent=no');
}

CHP_SP.MAX_CHECK_SAFE_TO_EMBED = 15;
CHP_SP.validURL = false;
CHP_SP.isValidating = false;
CHP_SP.checkSafeToEmbed = function (callbackfn) {
    if (window.safeToEmbedded) // popup onload event is triggered before timeout of detection process
    {
        CHP_SP.validURL = true;
        if (typeof callbackfn != 'undefined' && callbackfn != null)
            callbackfn();
        else
            alert(CHP_SP.constants.RI_CREATE_VALIDATE_OK);
    }
    // when user close the window by its own, we do not need to continue with the detection any more.
    else if (window.FBDetectionWin.closed) {
        return;
    }
    else if (CHP_SP.MAX_CHECK_SAFE_TO_EMBED <= window.currentCheckSafeToEmbed++) // the onload of popup never get chance to be invoke due to frame breaker
    {
        window.FBDetectionWin.close();
        CHP_SP.validURL = false;
        alert(CHP_SP.constants.RI_CREATE_VALIDATE_FAIL);
    }
    else {
        setTimeout(function () {
            CHP_SP.checkSafeToEmbed(callbackfn);
        }, 1000);
    }
}

CHP_SP.beginPreview = function () {
    var pageType = CHP_SP.getPageTypeForActiveE1page();
    if (CHP_SP.PAGE_TYPE_URL == pageType) {
        CHP_SP.detectFrameBreaker(CHP_SP.previewContent);
    }
    else {
        CHP_SP.previewContent();
    }
}

CHP_SP.previewContent = function () {
    CHP_SP.showMessageDiv(false);
    var pageType = CHP_SP.getPageTypeForActiveE1page();
    var doc = document;
    if (pageType == CHP_SP.PAGE_TYPE_HTMLCONTENT) {
        var fileName = doc.getElementById(CHP_SP.ID_FILEUPLOAD).value;
        fileName = fileName.substring(fileName.lastIndexOf("\\") + 1);
        fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
        if (!CHP_SP.isUploadFileValid(fileName) && CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
            CHP_SP.showErrorWindow(doc.getElementById(CHP_SP.ID_FILEUPLOAD), CHP_SP.constants.CHP_UPLOADCONTENT_INVALID);
            errorExists = true;
            return;
        }
        CHP_SP.activeE1page.fileName = fileName;
    }
    else //it is URL type
    {
        var pageURL = doc.getElementById(CHP_SP.ID_PAGE_URL).value;
        if (!CHP_SP.isValidURL(pageURL)) {
            CHP_SP.showErrorWindow(doc.getElementById(CHP_SP.ID_PAGE_URL), CHP_SP.constants.CHP_PAGEURL_INVALID);
            errorExists = true;
            return;
        }
        CHP_SP.activeE1page.pageURL = pageURL;

        // append preview mode flag to the embedded e1 form page launcher to prevent maflet from being reused.
        if (pageURL.indexOf("/EmbeddedE1FormPageLaunch?") != -1)
            CHP_SP.activeE1page.pageURL += "&E1FormPreviewMode=true";
    }

    CHP_SP.activeE1page.desc = CHP_SP.HTMLDecoder(doc.getElementById(CHP_SP.ID_DESCRIPTION).value);
    CHP_SP.activeE1page.tabName = CHP_SP.HTMLDecoder(doc.getElementById(CHP_SP.ID_PAGE_TITLE).value);
    var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.CMD_PREVIEW);
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.onAfterPreview, true);
}

CHP_SP.onAfterPreview = function (container, retStr) {
    CHP_SP.showMessageDiv(false);
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.activeE1page.id = CHP_SP.BLANK_E1PAGE_ID;
            CHP_SP.activeE1page.name = "";
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
            return;
        }
    }
    else {
        var obj = document.getElementById("e1menuAppIframe").contentWindow.CHP;
        CHP_SP.goHome();
    }
}

CHP_SP.goHome = function () {
    var fName = "e1menuAppIframe";
    var appFrame = document.getElementById(fName);

    if (appFrame) {
        appFrame.src = WEBGUIRES_https_dummy_html;
        CHP_SP.forceRedraw(appFrame);
        var initUrl = "/jde/Welcome.mafService?e1.namespace=&e1.service=Welcome&RENDER_MAFLET=E1Menu&e1.state=maximized&e1.mode=view";
        initUrl += "&random=" + Math.random();
        appFrame.src = initUrl;
    }
    else {
        appFrame = window.frames[fName];
        if (appFrame) {
            appFrame.location = WEBGUIRES_https_dummy_html;
            CHP_SP.forceRedraw(appFrame);
            var initUrl = "/jde/Welcome.mafService?e1.namespace=&e1.service=Welcome&RENDER_MAFLET=E1Menu&e1.state=maximized&e1.mode=view";
            initUrl += "&random=" + Math.random();
            appFrame.location = initUrl;
        }
    }

    if (CARO && CARO.setActiveAppTile) {
        CARO.setActiveAppTile(0);
    }
    hideAllDropdownMenus();
}

CHP_SP.forceRedraw = function (obj) {
    var savedVisibilityString = obj.style.visibility || '';
    obj.style.visibility = 'hidden';
    var hiddenHeight = obj.offsetHeight
    obj.style.visibility = savedVisibilityString;
    var finalHeight = obj.offsetHeight;
}

// Method to check if the current active e1page is already modified without save
CHP_SP.proceedWithoutSave = function () {
    if (CHP_SP.isDirty()) {
        var proceedWithoutSave = confirm(CHP_SP.constants.CHP_CONFIRM_SWITCH);
        return proceedWithoutSave; // true if user OKs, false if user cancels
    }
    return true;
}

CHP_SP.setE1PageDropDownValueById = function () {
    if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID)
        return CHP_SP.setE1PageObjectName();

    var e1pagesListBox = document.getElementById(CHP_SP.ID_E1PAGESLIST);
    for (i = 0; i < e1pagesListBox.options.length; i++) {
        if (e1pagesListBox.options[i].value == CHP_SP.activeE1page.id) {
            e1pagesListBox.options.selectedIndex = i;
            return i;
        }
    }
    e1pagesListBox.options.selectedIndex = 0;
    return 0;
}

// Private method to set the e1page object name select element based on the active e1page
CHP_SP.setE1PageObjectName = function () {
    var e1pagesListBox = document.getElementById(CHP_SP.ID_E1PAGESLIST);
    for (i = 0; i < e1pagesListBox.options.length; i++) {
        if ((CHP_SP.HTMLDecoder(e1pagesListBox.options[i].tabName) == CHP_SP.HTMLDecoder(CHP_SP.activeE1page.tabName)) && (e1pagesListBox.options[i].prodCode == CHP_SP.HTMLDecoder(CHP_SP.activeE1page.prodCode))) {
            e1pagesListBox.options.selectedIndex = i;
            return i;
        }
    }
    e1pagesListBox.options.selectedIndex = 0;
    return 0;
}

// Private method to retrieve user selected e1page
CHP_SP.getCurrentlySelectedE1Page = function () {
    var e1pagesListBox = document.getElementById(CHP_SP.ID_E1PAGESLIST);
    var e1PageObject = e1pagesListBox.options[e1pagesListBox.options.selectedIndex];
    return e1PageObject;
}

CHP_SP.createOptionGroupTag = function (selectTag, groupLabel) {
    var optionGroupTag = WebObjectUtil.getElementByAttribute("label", groupLabel, selectTag);
    if (optionGroupTag)
        return optionGroupTag;
    var optionGroupTag = document.createElement("optgroup");
    optionGroupTag.label = groupLabel;
    selectTag.appendChild(optionGroupTag);
    return optionGroupTag;
}

// Private method to populate the items into option group tag as children options.
CHP_SP.populateOptions = function (optionGroupTag, items) {
    if (optionGroupTag.label == CHP_SP.constants.UDO_PERSONAL) {
        var option = document.createElement("option");

        option.id = CHP_SP.BLANK_E1PAGE_ID;
        option.value = CHP_SP.BLANK_E1PAGE_ID;
        option.name = CHP_SP.BLANK_E1PAGE_ID;
        option.appendChild(document.createTextNode(CHP_SP.constants.UDO_CREATE));
        optionGroupTag.appendChild(option);
    }

    var doc = document;
    for (var i = 0, len = items.length; i < len; i++) {
        var item = items[i];
        var option = doc.createElement("option");

        option.value = item.id;
        option.id = item.id;
        option.name = item.name;
        option.tabName = item.tabName;
        option.langCode = item.langCode;
        option.prodCode = item.prodCode;
        var innertext = CHP_SP.HTMLDecoder(item.tabName.replace(/ /g, "\u00a0")) + "    (" + CHP_SP.trimStr(item.prodCode) + ")";
        option.appendChild(doc.createTextNode(innertext));
        optionGroupTag.appendChild(option);
    }
}

CHP_SP.trimStr = function (stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}

// Private method to distribute items array to publicItems and privateItems array, and we do not use items array any more after that
CHP_SP.separateE1pageslistNames = function (e1pagesModels) {
    //var items = e1pagesModels.items;
    var items = e1pagesModels;
    CHP_SP.privateItems = [];  // TODO: is this a leak? (creating a new array each time pane is opened)
    CHP_SP.pendingApprovalItems = [];
    CHP_SP.reservedItems = [];
    CHP_SP.publicItems = [];
    CHP_SP.reworkItems = [];

    for (var i = 0, len = items.length; i < len; i++) {
        var item = items[i];
        if (item.isPrivate) {
            if (item.isRequestPublished) {
                CHP_SP.pendingApprovalItems.push(item);
            }
            else if (item.isRework) {
                CHP_SP.reworkItems.push(item);
            }
            else if (item.isPersonal) {
                CHP_SP.privateItems.push(item);
            }
            else if (item.isCheckedOut && !(item.isRequestPublished) && !(item.isRework)) {
                CHP_SP.reservedItems.push(item);
            }
        }
        else {
            CHP_SP.publicItems.push(item);
        }
    }
}

// Private method to sort the list items by alphbetical order
CHP_SP.sortE1pagesListNames = function (items) {
    for (var endIndex = items.length - 1; endIndex >= 1; endIndex--) {
        for (var i = 0; i < endIndex; i++) {
            var name1 = items[i].tabName.toLowerCase();
            var name2 = items[i + 1].tabName.toLowerCase();
            if (name1 > name2) {
                var item = items[i];
                items[i] = items[i + 1];
                items[i + 1] = item;
            }
        }
    }
}

// Private method to update the enablement Control on the view
CHP_SP.updateControlEnablement = function () {
    var activeObject;
    activeObject = CHP_SP.activeE1page;
    if (CHP_SP.ISREADONLYMODE || CHP_SP.ISERROR) {
        CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
        CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
        CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], CHP_SP.constants.UDO_RESERVE);
        CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
        CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
        CHP_SP.disableIcon(CHP_SP.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], CHP_SP.constants.UDO_NOTES);
        return;
    }

    var isLanguageTranslation = false;
    if (!CHP_SP.isImage) {
        isLanguageTranslation = (activeObject.langCode.Trim().length > 0) ? true : false;
    }

    if (activeObject.isPrivate) {
        if (activeObject.id != CHP_SP.BLANK_E1PAGE_ID) {
            if (activeObject.notes && (CHP_SP.isImage || !isLanguageTranslation)) {
                CHP_SP.enableIcon(CHP_SP.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_ena_png"], window['E1RES_share_images_ulcm_notes_hov_png'], CHP_SP.goFetchNotes); //enable notes Icon
            }
            else {
                CHP_SP.disableIcon(CHP_SP.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], CHP_SP.constants.UDO_NOTES);
            }
            // When Pending Promotion section : enable SaveAs, reserve Icons
            if (activeObject.isRequestPublished || activeObject.isRework) {
                CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
                if (!CHP_SP.isImage) {
                    if (activeObject.canCopy && activeObject.canAddToProject && !isLanguageTranslation) {
                        CHP_SP.enableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], CHP_SP.onBeforeSaveAs); //enable saveAs Icon
                    }
                    else {
                        CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
                    }
                }
                if (activeObject.canCheckout && !isLanguageTranslation) {
                    CHP_SP.enableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_ena_png"], window['E1RES_share_images_ulcm_reserve_hov_png'], CHP_SP.goReserveBegin, CHP_SP.constants.UDO_RESERVE);
                }
                else {
                    CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], CHP_SP.constants.UDO_RESERVE);
                }
                CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
                CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
            }
            // personal E1Page section
            else if (activeObject.isPersonal) {
                CHP_SP.enableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], CHP_SP.onBeforeSave);
                if (!CHP_SP.isImage && activeObject.canCopy && activeObject.canAddToProject && !isLanguageTranslation) {
                    CHP_SP.enableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], CHP_SP.onBeforeSaveAs); //enable saveAs Icon
                }
                else {
                    CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
                }
                CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], CHP_SP.constants.UDO_RESERVE);
                if (!isLanguageTranslation) {
                    CHP_SP.enableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_ena_png"], window['E1RES_share_images_ulcm_requestpublish_hov_png'], CHP_SP.goRequestPublishBegin, CHP_SP.constants.UDO_REQUEST_PUBLISH);
                }
                else {
                    CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
                }
                // if NO allowed actions for the user to CanDelete or CanRemoveObjectFromProject disable delete icon
                if (activeObject.canDelete && activeObject.canRemoveFromProject) {
                    CHP_SP.enableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_ena_png"], window['E1RES_share_images_ulcm_delete_hov_png'], CHP_SP.goDeleteE1page);
                }
                else {
                    CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                }
            }
            // When in reserved section : enable save, unreserve, Request Publish Icons
            else if (activeObject.isCheckedOut && !(activeObject.isRequestPublished) && !(activeObject.isRework)) {
                CHP_SP.enableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], CHP_SP.onBeforeSave);
                if (!CHP_SP.isImage) {
                    if (activeObject.canCopy && activeObject.canAddToProject && !isLanguageTranslation) {
                        CHP_SP.enableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], CHP_SP.onBeforeSaveAs); //enable saveAs Icon
                    }
                    else {
                        CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
                    }
                }
                if (activeObject.canUnReserve && activeObject.canRemoveFromProject && activeObject.canDelete && !isLanguageTranslation) {
                    CHP_SP.enableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_unreserve_ena_png"], window['E1RES_share_images_ulcm_unreserve_hov_png'], CHP_SP.goReserveBegin, CHP_SP.constants.UDO_UNRESERVE);
                }
                else {
                    CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_unreserve_dis_png"], CHP_SP.constants.UDO_UNRESERVE);
                }
                if (!isLanguageTranslation) {
                    CHP_SP.enableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_ena_png"], window['E1RES_share_images_ulcm_requestpublish_hov_png'], CHP_SP.goRequestPublishBegin, CHP_SP.constants.UDO_REQUEST_PUBLISH);
                }
                else {
                    CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
                }
                CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
            }
        }
        else {
            // when create new E1page is selected
            if (activeObject.canAdd && activeObject.canAddToProject) {
                CHP_SP.enableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], CHP_SP.onBeforeSave);
            }
            else {
                CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
            }
            if (!CHP_SP.isImage) {
                CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
            }
            CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], CHP_SP.constants.UDO_RESERVE);
            CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
            CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
            CHP_SP.disableIcon(CHP_SP.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], CHP_SP.constants.UDO_NOTES);
        }
    }
    else //shared
    {
        // When in shared section
        CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);

        if (!CHP_SP.isImage) {
            if (activeObject.canCopy && activeObject.canAddToProject && !isLanguageTranslation) {
                CHP_SP.enableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], CHP_SP.onBeforeSaveAs); //enable saveAs Icon
            }
            else {
                CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
            }
        }
        if (!activeObject.isCheckedOut && activeObject.canCheckout && !isLanguageTranslation) {
            CHP_SP.enableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_ena_png"], window['E1RES_share_images_ulcm_reserve_hov_png'], CHP_SP.goReserveBegin, CHP_SP.constants.UDO_RESERVE);
        }
        else {
            CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], CHP_SP.constants.UDO_RESERVE);
        }
        CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
        CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
    }

    if (!CHP_SP.isImage) {
        var doc = document;
        if (activeObject.id == CHP_SP.BLANK_E1PAGE_ID) {
            GUTIL.setFocus(doc.getElementById(CHP_SP.ID_PROD_CODE), true);
        }
        else {
            GUTIL.setFocus(doc.getElementById(CHP_SP.ID_DESCRIPTION), true);
        }
    }
}

CHP_SP.disableIcon = function (id, outImg, title) {
    icon = top.document.getElementById(id);
    if (icon != null) {
        CHP_SP.setMotionImage(icon, outImg, null, null);
        icon.className = "";
        icon.onclick = null;
        icon.disabled = true;
        if (title) {
            icon.title = title;
            icon.alt = title;
            if (CHP_SP.ACCESSIBILITY && (title == CHP_SP.constants.UDO_RESERVE))
                icon.setAttribute("aria-label", title);
        }
        if (id == CHP_SP.ID_SAVE_ICON && CHP_SP.isImage) {
            document.getElementById(CHP_SP.ID_FILEUPLOAD).disabled = true;
        }
    }
}

CHP_SP.enableIcon = function (id, outImg, overImg, eventHandler, title) {
    var icon = top.document.getElementById(id);
    if (icon != null) {
        CHP_SP.setMotionImage(icon, outImg, overImg, null);
        icon.className = "clickableImage";
        icon.onclick = eventHandler;
        icon.disabled = false;
        if (title) {
            icon.title = title;
            icon.alt = title;
            if (CHP_SP.ACCESSIBILITY && (title == CHP_SP.constants.UDO_UNRESERVE))
                icon.setAttribute("aria-label", title);
        }
        if (id == CHP_SP.ID_SAVE_ICON && CHP_SP.isImage) {
            document.getElementById(CHP_SP.ID_FILEUPLOAD).disabled = false;
            document.getElementById(CHP_SP.ID_DESCRIPTION).readOnly = false;
        }
    }
}

CHP_SP.setMotionImage = function (image, outImg, overImg, eventHandler) {
    image.src = outImg;
    image.onmouseover = overImg ? CHP_SP.onMouseOverEvent : "";
    image.onmouseout = overImg ? CHP_SP.onMouseOutEvent : "";

    image.setAttribute('outImg', outImg);
    if (overImg)
        image.setAttribute('overImg', overImg);
    image.className = "clickableImage";
    if (eventHandler && !CHP_SP.ACCESSIBILITY)
        image.onclick = eventHandler;
}

CHP_SP.adjustResizeBar = function () {
    var sidePanelTd = document.getElementById(CHP_SP.ID_SIDE_CONTAINER_TD);
    var wDiff = sidePanelTd.offsetWidth - CHP_SP.E1PmanageSidepanel.offsetWidth;
    if (wDiff < 3 || wDiff > 5) {
        CHP_SP.adjustSize();
    }
}

CHP_SP.renderSidePanelFrame = function () {
    var doc = top.document;
    var e1AppFrameContainer = doc.getElementById("e1AppFrameContainer");

    //side panel for E1pages
    var control = doc.getElementById('E1PSidePanel');
    if (control) {
        //hack to compensage for the 2nd Preview Object system function call from P98220W
        control.parentNode.removeChild(control);
    }

    // new Table
    var layoutTable = document.createElement("TABLE");
    layoutTable.id = CHP_SP.ID_FRAME_TABLE;
    layoutTable.cellPadding = 0;
    layoutTable.cellSpacing = 0;
    layoutTable.className = "SPFrameLayoutTable";
    layoutTable.style.height = "100%";

    // new TR
    var tr = layoutTable.insertRow(-1);
    // new TD for e1FormTD
    var tdTag = tr.insertCell(-1);
    tdTag.id = CHP_SP.ID_E1_CONTAINER_TD;
    e1AppFrameContainer.appendChild(layoutTable);

    // new TD for side panel
    var e1SideTD = tr.insertCell(-1);
    e1SideTD.id = CHP_SP.ID_SIDE_CONTAINER_TD;
    e1SideTD.style.width = CHP_SP.sidePanelWidth + "px";
    e1SideTD.style.height = "100%";

    CHP_SP.E1PmanageSidepanel = doc.createElement("div");
    CHP_SP.E1PmanageSidepanel.id = 'E1PSidePanel';
    CHP_SP.E1PmanageSidepanel.style.width = CHP_SP.sidePanelWidth + "px";
    CHP_SP.E1PmanageSidepanel.style.height = "100%";
    CHP_SP.E1PmanageSidepanel.style.position = "relative";
    if (CHP_SP.isRTL) {
        CHP_SP.E1PmanageSidepanel.className = "SPBody E1PSidePanel_RTL";
    }
    else {
        CHP_SP.E1PmanageSidepanel.className = "SPBody";
    }
    e1SideTD.appendChild(CHP_SP.E1PmanageSidepanel);

    // new TD for sidePanel resize TD
    var sidePanelResizeTD = tr.insertCell(0);
    sidePanelResizeTD.className = "SPResizeDiv";
    sidePanelResizeTD.style.width = CHP_SP.RESIZE_TD_WIDTH + "px";
    var sidePanelResizeDiv = document.createElement("DIV");
    sidePanelResizeTD.appendChild(sidePanelResizeDiv);
    sidePanelResizeDiv.className = "SPResizeDiv";
    sidePanelResizeDiv.id = CHP_SP.ID_RESIZE_DIV;
    sidePanelResizeDiv.style.position = "relative";
    sidePanelResizeDiv.style.width = CHP_SP.RESIZE_TD_WIDTH + "px";
    sidePanelResizeDiv.onmouseover = CHP_SP.setResizingColor;
    sidePanelResizeDiv.onmouseout = CHP_SP.resetResizingColor;
    sidePanelResizeDiv.onmousedown = CHP_SP.startResizing;

    CHP_SP.updateE1ContainerWidth();

    return e1SideTD;
}

// Private method to set E1 form width and all related elements
CHP_SP.updateE1ContainerWidth = function () {
    var iframe = document.getElementById('e1menuAppIframe');
    if (iframe != null) {
        var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
        if (innerDoc != null && innerDoc != undefined) {
            var e1PaneDIV = innerDoc.getElementById("E1PaneDIV");
            var e1FormDiv = innerDoc.getElementById("e1formDiv");
            var menuTable = innerDoc.getElementById("WebMenuBarFrame");
        }
    }

    var Wavailable;
    var totalWidth = (JSSidePanelRender.isIOS ? window.innerWidth : document.body.offsetWidth);
    // carousel is aligned right/left side of the E1Form adjust the containers width
    if (CARO.isVertical()) {
        wAvailable = (CARO.carousel.expanded == 1) ? (totalWidth - CARO.carousel.offsetWidth - 12) : (totalWidth - 7);
    }
    else {
        wAvailable = totalWidth;
    }
    var width = wAvailable - CHP_SP.sidePanelWidth;
    var doc = top.document;  // TODO: this won't work for portal, but maybe we don't care
    var outerRCUX = doc.getElementById("outerRCUX");
    var resizeDiv = doc.getElementById("E1PResizeDiv");
    var sidePanel = doc.getElementById("E1PSidePanel");
    if (outerRCUX) {
        if (CHP_SP.isRTL) {
            outerRCUX.style.left = CHP_SP.sidePanelWidth + "px";
            outerRCUX.style.width = width + "px";
            CHP_SP.E1PmanageSidepanel.style.right = width + "px";
            resizeDiv.style.right = width + "px";
        }
        else {
            outerRCUX.style.right = CHP_SP.sidePanelWidth + "px";
            outerRCUX.style.width = width + "px";
            CHP_SP.E1PmanageSidepanel.style.left = width + "px";
            resizeDiv.style.left = width + "px";
        }
    }
}

// browser window resize - called from CARO.onResize(),
// or carousel expand/collapse - called from CARO.updateFormDivWidth()
CHP_SP.onResize = function () {
    if (CHP_SP.MANAGEMENT_PANE_OPEN)
        CHP_SP.updateE1ContainerWidth();
}

CHP_SP.resizeElem = function (Elem, width) {
    if (Elem)
        Elem.style.width = width + "px";
}

CHP_SP.showManageE1Pages = function (event, mode, oid_forEditing, isImage, user, namespace) {
    if (isImage) {
        CHP_SP.isImage = true;
        CHP_SP.namespace = namespace;
    }
    else {
        CHP_SP.isImage = false;
    }
    if (mode > 0) {
        CHP_SP.MODE = mode;
        if (mode == CHP_SP.PREVIEWMODE)
            CHP_SP.ISREADONLYMODE = true;
        CHP_SP.objectForEditing = oid_forEditing;
    }
    CHP_SP.renderSidePanelFrame();

    CHP_SP.MANAGEMENT_PANE_OPEN = true;

    CHP_SP.init();
    CHP_SP.renderSidePanel(CHP_SP.ISREADONLYMODE);
    CHP_SP.onOpen();
    if (CHP_SP.objectForEditing != null && (CHP_SP.ISREADONLYMODE || isImage)) {
        CHP_SP.goFetchE1pagesForPreview(user);
    }
    else if (!CHP_SP.isImage) {
        CHP_SP.goFetchE1pagesList();
    }
    else {
        CHP_SP.updateControlEnablement();
        //specifically enable the SAVE icon, the user's creator right is already checked on the server
        //CHP_SP.enableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], CHP_SP.onBeforeSave);        
    }
}

CHP_SP.renderSidePanelTabStub = function (sb) {
    sb.append(
    "<table id='SPTabStub' class='SPLayoutTable' cellSpacing=0 cellPadding=0>\
      <tbody>\
        <tr>"
            );

    var tabLeftImg = CHP_SP.isRTL ? window['E1RES_share_images_tab_start_a_rtl_png'] : window['E1RES_share_images_tab_start_a_png'];
    var tabRightImg = CHP_SP.isRTL ? window['E1RES_share_images_tab_end_a_rtl_png'] : window['E1RES_share_images_tab_end_a_png'];

    sb.append("<td><table class='tabTable");
    if (CHP_SP.isRTL)
        sb.append("_rtl");
    sb.append(" SPLayoutTable'");
    sb.append("style='position:relative;z-index:100;'");
    sb.append("><tbody><tr>");

    sb.append("<td class='SPLayoutCell'><table cellpadding='0' cellspacing='0' ");
    sb.append("class='tabTableSelected'>");
    sb.append("<tbody><tr><td width='100%' nowrap align='center'>");
    sb.append("<font class='ActiveTabLink'");
    if (CHP_SP.ACCESSIBILITY)
        sb.append(" role='presentation'");
    if (!CHP_SP.isImage) {
        sb.append(">").append(CHP_SP.constants.CHP_TITLE_TAB).append("</font>");
    }
    else {
        sb.append(">").append(CHP_SP.constants.UDO_ICON_MANAGER).append("</font>");
    }

    sb.append("</td></tr></tbody></table></td>");
    sb.append("</tr></tbody></table></td>");

    sb.append("<td class='notabfiller' width=100%></td>");
    sb.append("</tr></tbody></table>");
}

// Public Event handler for start drag and drop the edge of side panel
CHP_SP.startResizing = function (e) {
    var doc = document;
    var resizeDiv = doc.getElementById(CHP_SP.ID_RESIZE_DIV);
    var e1AppFrameContainer = doc.getElementById("e1AppFrameContainer");
    e1AppFrameContainer.style.overflow = "hidden";
    resizeDiv.style.backgroundColor = "#74C4FF";
    // create the div. If exists then something is wrong so get out
    var elem = doc.getElementById(CHP_SP.ID_RESIZE_BLOCK);
    if (!elem) {
        elem = doc.createElement("div");
        var parentTable = doc.getElementById(CHP_SP.ID_FRAME_TABLE);
        var sepRangeTop = parentTable.offsetTop;
        var sepRangeLeft = parentTable.offsetLeft;
        var sepWidth = parentTable.offsetWidth;
        var sepVRange = parentTable.offsetHeight;
        var containerPos = outerRCUX;
        sepRangeTop += containerPos.offsetTop;
        elem.style.position = "absolute";

        if (CARO.carousel.expanded == 1) {
            elem.style.left = 7 + "px";
            elem.style.right = 300 + "px"
        }
        else {
            elem.style.left = 200 + "px";
            elem.style.right = 300 + "px"
        }

        elem.style.top = sepRangeTop + "px";
        elem.style.height = sepVRange + "px";

        elem.style.zIndex = 2000000001;
        elem.style.cursor = "w-resize";
        elem.style.visibility = "visible";
        elem.id = CHP_SP.ID_RESIZE_BLOCK;
        elem.style.overflow = "hidden";
        // attach events for resize to div
        if (!elem.addEventListener) {
            elem.attachEvent("onmousemove", CHP_SP.resizing);
            elem.attachEvent("onmouseup", CHP_SP.stopResizing);
            elem.attachEvent("onmouseout", CHP_SP.stopResizing);
        }
        else {
            elem.addEventListener("mousemove", CHP_SP.resizing, false);
            elem.addEventListener("mouseup", CHP_SP.stopResizing, false);
            elem.addEventListener("mouseout", CHP_SP.stopResizing, false);
        }
        // eat out all selection event
        var table = doc.createElement("table");
        var tbody = doc.createElement("tbody");
        var td = doc.createElement("td");
        var tr = doc.createElement("tr");
        var div = doc.createElement("div");
        td.appendChild(div);
        tr.appendChild(td);
        tbody.appendChild(tr);
        table.appendChild(tbody);
        elem.appendChild(table);
        e1AppFrameContainer.appendChild(elem);
    }

    elem.originalX = JSSidePanelRender.getPageX(e);
    return RIUTIL.stopEventBubbling(e);
}

// Event handler: drag and drop resizing of side panel
CHP_SP.resizing = function (e) {
    var evt = e ? e : window.event;

    maxClientWidth = 250;

    if (CARO.carousel.side == CARO.EAST) {
        if (CARO.carousel.expanded == 1) {
            maxClientWidth = maxClientWidth - CARO.carousel.offsetWidth - 20;
        }
        else {
            maxClientWidth = maxClientWidth - 12;
        }
    }

    if (CHP_SP.isRTL && (evt.clientX >= (document.body.clientWidth - (maxClientWidth + 5 + CHP_SP.RESIZE_TD_WIDTH))))
        CHP_SP.stopResizing(e);
    else if (!CHP_SP.isRTL && (evt.clientX <= (maxClientWidth + 5 + CHP_SP.RESIZE_TD_WIDTH)))
        CHP_SP.stopResizing(e);
    else {
        var resizeDiv = document.getElementById(CHP_SP.ID_RESIZE_DIV);
        resizeDiv.style.backgroundColor = "#74C4FF";
        var diffX = CHP_SP.getDiffX(e);
        if (CHP_SP.isRTL)
            diffX = -diffX;
        CHP_SP.resizeByDiffX(diffX);

        return RIUTIL.stopEventBubbling(e);
    }
}

CHP_SP.setResizingColor = function (e) {
    var elem = document.getElementById(CHP_SP.ID_RESIZE_DIV);
    elem.style.backgroundColor = "#74C4FF";
}

CHP_SP.resetResizingColor = function (e) {
    var elem = document.getElementById(CHP_SP.ID_RESIZE_DIV);
    elem.style.backgroundColor = "transparent";
}

CHP_SP.getDiffX = function (e) {
    var elem = document.getElementById(CHP_SP.ID_RESIZE_BLOCK);
    var pageX = JSSidePanelRender.getPageX(e);
    var diffX = pageX - elem.originalX;
    elem.originalX = pageX;
    return diffX;
}


// Private: resize horizontally by difference 
CHP_SP.resizeByDiffX = function (diffX) {
    CHP_SP.originalSPWidth = CHP_SP.sidePanelWidth;
    var newSPWidth = CHP_SP.originalSPWidth - diffX;
    CHP_SP.sidePanelWidth = newSPWidth;
    CHP_SP.adjustSize();
}

CHP_SP.adjustSize = function () {
    CHP_SP.updateSidePanelWidth();
    CHP_SP.updateE1Width();
}

CHP_SP.updateSidePanelWidth = function () {
    var doc = document;
    var newSPWidth = CHP_SP.sidePanelWidth;
    var originalSPWidth = CHP_SP.originalSPWidth;
    var spContainerTD = doc.getElementById(CHP_SP.ID_SIDE_CONTAINER_TD);
    var spMenuBarTable = doc.getElementById("SPMenuBarTable");

    CHP_SP.E1PmanageSidepanel.style.width = (newSPWidth) + "px";

    var resizeBlock = doc.getElementById(CHP_SP.ID_RESIZE_BLOCK);
    var resizeBlockWidth = resizeBlock.offsetWidth;

    var sidePanel = document.getElementById('E1PSidePanel');

    var resizeLine = doc.getElementById(CHP_SP.ID_RESIZE_DIV);

    if (CHP_SP.isRTL) {
        var originalSPRight = sidePanel.style.right.replace('px', '');
        var newSPRightValue = originalSPRight - (newSPWidth - originalSPWidth);
        CHP_SP.E1PmanageSidepanel.style.right = newSPRightValue + "px";
        resizeLine.style.right = newSPRightValue + "px";
    }
    else {
        var originalSPLeft = sidePanel.style.left.replace('px', '');
        var newSPLeftValue = originalSPLeft - (newSPWidth - originalSPWidth);
        CHP_SP.E1PmanageSidepanel.style.left = newSPLeftValue + "px";
        resizeLine.style.left = newSPLeftValue + "px";
    }
}

CHP_SP.updateE1Width = function () {
    var doc = document;
    var sidePanelwidth = CHP_SP.sidePanelWidth;

    var e1AppFrameContainer = doc.getElementById("e1AppFrameContainer");
    var totalWidth = e1AppFrameContainer.offsetWidth;

    var width = totalWidth - sidePanelwidth;

    outerRCUX.style.width = width + "px";
}

// Event handler: drag and drop resizing of side panel
CHP_SP.stopResizing = function (e) {
    var doc = document;
    var resizeDiv = doc.getElementById(CHP_SP.ID_RESIZE_DIV);
    resizeDiv.style.backgroundColor = "transparent";
    var elem = doc.getElementById(CHP_SP.ID_RESIZE_BLOCK);
    elem.parentNode.removeChild(elem);
    var outerRCUX = doc.getElementById("outerRCUX");
    outerRCUX.style.overflow = "auto";
    return RIUTIL.stopEventBubbling(e);
}

CHP_SP.renderSidePanel = function (isReadOnlyMode) {
    var sidePanelLayout = document.createElement("table");
    sidePanelLayout.setAttribute("cellpadding", "0");
    sidePanelLayout.setAttribute("cellspacing", "0");
    sidePanelLayout.className = "SPLayoutTable";
    sidePanelLayout.id = "sidePanelLayout";

    var tabsRow = sidePanelLayout.insertRow(0);
    var col = tabsRow.insertCell(0);

    var tabStubsb = new PSStringBuffer();
    CHP_SP.renderSidePanelTabStub(tabStubsb);

    col.innerHTML = tabStubsb.toString();
    CHP_SP.E1PmanageSidepanel.appendChild(sidePanelLayout);

    var buttonBarRowContainer = sidePanelLayout.insertRow(1);
    var buttonBarContainer = buttonBarRowContainer.insertCell(0);
    CHP_SP.renderSidePanelMenu(buttonBarContainer);

    // HTML div placeholder for message/error area
    var errorAreaRow = sidePanelLayout.insertRow(2);
    var col = errorAreaRow.insertCell(0);
    col.setAttribute("colspan", "2");
    col.innerHTML = "<div id='" + CHP_SP.ID_MESSAGE_DIV + "' class='SPMessageDiv' style='display:none'></div>";

    // HTML div placeholder for reserved by area
    var messageAreaRow = sidePanelLayout.insertRow(3);
    var col = messageAreaRow.insertCell(0);
    col.setAttribute("colspan", "2");
    col.innerHTML = "<div id='" + CHP_SP.ID_RESERVED_BY + "' class='SPMessageDiv' style='display:none'></div>";

    var messageAreaRow = sidePanelLayout.insertRow(4);
    var col = messageAreaRow.insertCell(0);
    col.setAttribute("colspan", "2");


    var spContentRow = sidePanelLayout.insertRow(5);
    if (CHP_SP.isImage) {
        //radio button 
        var wrapperCol = spContentRow.insertCell(0);
        var wrapperTable = document.createElement('table');
        wrapperTable.style.borderSpacing = '2px';
        wrapperTable.style.border = '0px';

        var wrapperRow = wrapperTable.insertRow(0);
        var cell = wrapperRow.insertCell(0);
        var cellElem = document.createElement('input');
        cellElem.type = 'radio';
        cellElem.id = 'newImage';
        cellElem.checked = true;
        cellElem.addEventListener("click", CHP_SP.toggleUdoImage);
        cell.appendChild(cellElem);

        cellElem = document.createElement('span');
        cellElem.appendChild(document.createTextNode(CHP_SP.constants.UDO_ICON_NEW));
        cell.appendChild(cellElem);

        cell = wrapperRow.insertCell(1);
        cellElem = document.createElement('input');
        cellElem.type = 'radio';
        cellElem.id = 'existingImage';
        cellElem.addEventListener("click", CHP_SP.toggleUdoImage);
        cell.appendChild(cellElem);

        cellElem = document.createElement('span');
        cellElem.appendChild(document.createTextNode(CHP_SP.constants.UDO_ICON_EXISTING));
        cell.appendChild(cellElem);

        wrapperCol.appendChild(wrapperTable);
    }
    else {
        var col = spContentRow.insertCell(0);

        var sb = new PSStringBuffer();
        sb.append("<table border=0 style='border-spacing:2px 2px; width:100%'");
        if (CHP_SP.ACCESSIBILITY) {
            sb.append("role='presentation'");
        }
        sb.append("><tbody>");
        //row to contain select box
        sb.append("<tr><td style='width:30%;align:").append(CHP_SP.isRTL ? "center" : "left").append("'>&nbsp;").append(CHP_SP.constants.UDO_LABEL_NAME).append("</td><td align='").append(CHP_SP.isRTL ? "center" : "left").append("'><select style='max-width:93%' ");
        if (isReadOnlyMode)
            sb.append(" disabled ");
        sb.append(" id='").append(CHP_SP.ID_E1PAGESLIST).append("'");

        if (localStorage.getItem("userAccessibilityMode"))
            CHP_SP.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
        if (CHP_SP.ACCESSIBILITY) {
            sb.append(" aria-label='").append(CHP_SP.constants.UDO_LABEL_NAME).append("'");
        }
        sb.append("><select></td></tr>");
        //---------------------------
        //HTML for Available Language records
        sb.append("<tr></tr>");

        sb.append("<tr id='").append(CHP_SP.ID_LANG_LIST_CONTAINER).append("' style='display:none;' ><td style='width:32%;align:").append(CHP_SP.isRTL ? "center" : "left").append("'>&nbsp;").append(CHP_SP.constants.CHP_LABEL_AVAILABLE_LANG).append("</td><td align='").append(CHP_SP.isRTL ? "center" : "left").append("'><select style='max-width:85%' id='").append(CHP_SP.ID_LANG_LIST).append("'");
        if (CHP_SP.ACCESSIBILITY) {
            sb.append(" aria-label='").append(CHP_SP.constants.CHP_LABEL_AVAILABLE_LANG).append("'");
        }
        sb.append("><select></td></tr>");

        //---------------
        //HTML for Object Name
        sb.append("<tr></tr>");
        sb.append("<tr style='display:none'><td style='width:30%;align:").append(CHP_SP.isRTL ? "center" : "left").append("'>&nbsp;").append(CHP_SP.constants.CHP_LABEL_OBJECT_NAME).append("<span class='TextReqdInd'>&nbsp;*</span></td><td align='").append(CHP_SP.isRTL ? "center" : "left").append("'><input type='text' id='").append(CHP_SP.ID_OBJECT_NAME).append("'>");
        if (CHP_SP.ACCESSIBILITY) {
            sb.append("<label class='accessibility' for='").append(CHP_SP.ID_OBJECT_NAME).append("'>").append(CHP_SP.constants.CHP_LABEL_OBJECT_NAME).append("</label>");
        }
        sb.append("</td></tr>");

        //---------------
        //HTML for Product Code
        sb.append("<tr></tr>");
        sb.append("<tr><td style='width:30%;align:").append(CHP_SP.isRTL ? "center" : "left").append("'>&nbsp;").append(CHP_SP.constants.CHP_LABEL_PROD_CODE).append("<span class='TextReqdInd'>&nbsp;*</span></td><td align='").append(CHP_SP.isRTL + "?center:left'>")
        sb.append("<input type='text' id='" + CHP_SP.ID_PROD_CODE).append("' name='" + CHP_SP.ID_PROD_CODE);
        sb.append("' onfocus='FCHandler.onFocusVA(this)' ");
        sb.append("onblur='CHP_SP.onExitProductCode();' size='8'");

        if (CHP_SP.ACCESSIBILITY) {
            sb.append(" role='textbox' aria-label='").append(CHP_SP.constants.CHP_LABEL_PROD_CODE).append("'");
        }
        sb.append(">");
        sb.append("<img align=middle width=15 height=17 style='align:' id='" + CHP_SP.ID_PROD_CODE_IMG);
        sb.append("' NAME='" + CHP_SP.ID_PROD_CODE_VA + "' src='").append(window["E1RES_img_va0_gif"]).append("'");
        sb.append(" onMouseOver= this.src='").append(CHP_SP.isRTL ? window["E1RES_img_va1mo_rtl_gif"] : window["E1RES_img_va1mo_gif"]).append("'");
        sb.append(" onMouseOut= this.src='").append(CHP_SP.isRTL ? window["E1RES_img_va1_rtl_gif"] : window["E1RES_img_va1_gif"]).append("'");
        sb.append(" onclick='CHP_SP.vaProdCode()' alt='Visual Assist (F2)' />");
        sb.append("<label for=\"CHP_SP.ID_PROD_CODE\">\n");
        sb.append("<span id='").append(CHP_SP.ID_PROD_CODE_DESC).append("' value='Start Testing' style='font-weight:normal;FONT-SIZE: 11px;font-style:italic;color: #333333;'> </span>");
        sb.append("</label>");
        sb.append("</td></tr>");

        //---------------
        //HTML for Language Code
        sb.append("<tr></tr>");
        sb.append("<tr id='").append(CHP_SP.ID_LANG_CODE_CONTAINER).append("'><td style='width:30%;align:");
        sb.append(CHP_SP.isRTL ? "center" : "left").append("'>&nbsp;").append(CHP_SP.constants.CHP_LABEL_LANG_CODE);
        sb.append("</td><td align='").append(CHP_SP.isRTL ? "center" : "left").append("'>");
        sb.append("<input type='text' id='" + CHP_SP.ID_LANG_CODE).append("' name='" + CHP_SP.ID_LANG_CODE);
        sb.append("' onfocus='FCHandler.onFocusVA(this)' ");
        sb.append("onblur='CHP_SP.onExitLangCode();' size='8'");
        if (CHP_SP.ACCESSIBILITY) {
            sb.append(" aria-label='").append(CHP_SP.constants.CHP_LABEL_LANG_CODE).append("'");
        }
        sb.append(">");

        sb.append("<img align=middle width=15 height=17 style='align:' id='" + CHP_SP.ID_LANG_CODE_IMG);
        sb.append("' NAME=" + CHP_SP.ID_LANG_CODE_VA + "' src='").append(window["E1RES_img_va0_gif"]).append("'");
        sb.append(" onMouseOver= this.src='").append(CHP_SP.isRTL ? window["E1RES_img_va1mo_rtl_gif"] : window["E1RES_img_va1mo_gif"]).append("'");
        sb.append(" onMouseOut= this.src='").append(CHP_SP.isRTL ? window["E1RES_img_va1_rtl_gif"] : window["E1RES_img_va1_gif"]).append("'");
        sb.append(" onclick='CHP_SP.vaLangCode()' alt='Visual Assist (F2)' />");
        sb.append("<label for=\"CHP_SP.ID_LANG_CODE\">\n");
        sb.append("<span id='").append(CHP_SP.ID_LANG_CODE_DESC).append("' value='Start Testing' style='font-weight:normal;FONT-SIZE: 11px;font-style:italic;color: #333333;'> </span>");
        sb.append("</label>");
        sb.append("</td></tr>");

        //---------------

        //HTML for Page Title
        sb.append("<tr></tr>");
        sb.append("<tr id='").append(CHP_SP.ID_PAGE_TITLE_CONTAINER).append("'><td style='width:30%;align:").append(CHP_SP.isRTL ? "center" : "left").append("'>&nbsp;").append(CHP_SP.constants.UDO_LABEL_NAME).append("<span class='TextReqdInd'>&nbsp;*</span></td><td align='").append(CHP_SP.isRTL ? "center" : "left").append("'><input type='text' MAXLENGTH='30' style='width:180px' id='").append(CHP_SP.ID_PAGE_TITLE).append("'");
        if (CHP_SP.ACCESSIBILITY) {
            sb.append(" aria-label='").append("E1PAGE ").append(CHP_SP.constants.UDO_LABEL_NAME).append("'");
        }
        sb.append("></td></tr>");

        sb.append("<tr></tr>");
        // HTML for Description input
        sb.append("<tr><td style='width:30%;align:left'><div><nobr>&nbsp;").append(CHP_SP.constants.UDO_LABEL_DESCRIPTION).append("</nobr></div></td>");
        sb.append("<td align='").append(CHP_SP.isRTL ? "center" : "left").append("' style='width:80%'> <textarea style='width:85%' MAXLENGTH='200' rows='4' cols='24' id='").append(CHP_SP.ID_DESCRIPTION).append("'");
        if (CHP_SP.ACCESSIBILITY) {
            sb.append(" role='textbox' aria-label='").append(CHP_SP.constants.UDO_LABEL_DESCRIPTION).append("'");
        }
        sb.append("></textarea>");
        sb.append("</td></tr>");
        //----------------

        sb.append("<tr></tr>");
        sb.append("</tbody></table>");
        col.innerHTML = sb;

        //attach on input change event for description, page title, language, product code
        var doc = document;
        var langCodeElem = doc.getElementById(CHP_SP.ID_LANG_CODE);
        var langCodeImg = doc.getElementById(CHP_SP.ID_LANG_CODE_IMG);
        var prodCodeElem = doc.getElementById(CHP_SP.ID_PROD_CODE);
        var pageTitleElem = doc.getElementById(CHP_SP.ID_PAGE_TITLE);
        var descElem = doc.getElementById(CHP_SP.ID_DESCRIPTION);
        if (langCodeElem != null && !langCodeElem.addEventListener) {
            langCodeElem.attachEvent("onchange", CHP_SP.inputFieldChanged);
            prodCodeElem.attachEvent("onchange", CHP_SP.inputFieldChanged);
            pageTitleElem.attachEvent("onchange", CHP_SP.inputFieldChanged);
            descElem.attachEvent("onchange", CHP_SP.inputFieldChanged);
            descElem.attachEvent("onkeydown", function () {
                var maxlength = parseInt(descElem.getAttribute("maxlength"));
                if (descElem.value.length >= maxlength) {
                    descElem.value = descElem.value.substr(0, 199);
                }
            });
        }
        else {
            langCodeElem.addEventListener("change", CHP_SP.inputFieldChanged, false);
            prodCodeElem.addEventListener("change", CHP_SP.inputFieldChanged, false);
            pageTitleElem.addEventListener("change", CHP_SP.inputFieldChanged, false);
            descElem.addEventListener("change", CHP_SP.inputFieldChanged, false);
        }

    }
    //Row to contain Page Type GroupBox
    var spContentRow = sidePanelLayout.insertRow(6);
    if (CHP_SP.isImage) {
        //label and input field
        var wrapperCol = spContentRow.insertCell(0);
        var wrapperTable = document.createElement('table');
        wrapperTable.style.width = '100%';
        wrapperTable.style.borderSpacing = '2px';
        wrapperTable.style.border = '0px';

        //image name
        var wrapperRow = wrapperTable.insertRow(0);
        var cell = wrapperRow.insertCell(0);
        cell.style.width = '80px';
        var cellElem = document.createElement('div');
        cellElem.appendChild(document.createTextNode(CHP_SP.constants.UDO_LABEL_NAME));
        cell.appendChild(cellElem);

        cell = wrapperRow.insertCell(1);
        cell.style.width = '210px';
        cellElem = document.createElement('input');
        cellElem.id = CHP_SP.BLANK_E1PAGE_ID;
        cellElem.value = CHP_SP.constants.UDO_CREATE;
        cellElem.readOnly = true;
        cellElem.style.width = '210px';
        cellElem.onkeydown = function (event) {
            if (event.keyCode == 113)
                CHP_SP.renderIconViewer();
            else
                return true;
        }
        cell.appendChild(cellElem);

        cell = wrapperRow.insertCell(2);
        var imgElem = document.createElement('img');
        imgElem.title = CHP_SP.constants.IMG_VA_IMAGE;
        if (CHP_SP.isRTL) {
            imgElem.src = '/jde/img/va1_rtl.gif';
            imgElem.addEventListener("mouseover", function () { this.src = '/jde/img/va1mo_rtl.gif'; });
            imgElem.addEventListener("mouseout", function () { this.src = '/jde/img/va1_rtl.gif'; });
        }
        else {
            imgElem.src = '/jde/img/va1.gif';
            imgElem.addEventListener("mouseover", function () { this.src = '/jde/img/va1mo.gif'; });
            imgElem.addEventListener("mouseout", function () { this.src = '/jde/img/va1.gif'; });
        }
        imgElem.addEventListener("mousedown", CHP_SP.renderIconViewer);
        imgElem.inputField = cellElem;
        imgElem.id = 'udoIconVA';
        if (CHP_SP.MODE == 1) {
            imgElem.style.display = 'inline';
        }
        else {
            imgElem.style.display = 'none';
        }

        cell.appendChild(imgElem);

        wrapperRow = wrapperTable.insertRow(1);
        /*
        //product code
        cell = wrapperRow.insertCell(0);
        var labelElem = document.createElement('div');
        labelElem.appendChild(document.createTextNode(CHP_SP.constants.CHP_LABEL_PROD_CODE));
        cell.appendChild(labelElem);
        
        cell = wrapperRow.insertCell(1);
        labelElem = document.createElement('input');
        labelElem.id = CHP_SP.ID_PROD_CODE;
        labelElem.addEventListener("focus", CHP_SP.focusOnProductCode);
        labelElem.addEventListener("blur", CHP_SP.onExitProductCode);
        cell.appendChild(labelElem);
        
        cell = wrapperRow.insertCell(2);
        var imgElem = document.createElement('img');
        if(CHP_SP.isRTL)
        {
        imgElem.src = '/jde/img/va1_rtl.gif';
        imgElem.addEventListener("mouseover", function(){this.src = '/jde/img/va1mo_rtl.gif';});
        imgElem.addEventListener("mouseout", function(){this.src = '/jde/img/va1_rtl.gif';});
        }
        else
        {
        imgElem.src = '/jde/img/va1.gif';
        imgElem.addEventListener("mouseover", function(){this.src = '/jde/img/va1mo.gif';});
        imgElem.addEventListener("mouseout", function(){this.src = '/jde/img/va1.gif';});
        }
        imgElem.addEventListener("click", CHP_SP.vaProdCode);
        imgElem.id = CHP_SP.ID_PROD_CODE_IMG;
        imgElem.style.display = 'none';
        cell.appendChild(imgElem);
        
        cell = wrapperRow.insertCell(3);
        labelElem = document.createElement('div');
        labelElem.appendChild(document.createTextNode(""));//        
        labelElem.id = CHP_SP.ID_PROD_CODE_DESC;
        cell.appendChild(labelElem);
        
        //image description
        wrapperRow = wrapperTable.insertRow(2);
        */
        //image description
        cell = wrapperRow.insertCell(0);
        var cellElem = document.createElement('div');
        cellElem.appendChild(document.createTextNode(CHP_SP.constants.UDO_LABEL_DESCRIPTION));
        cell.appendChild(cellElem);

        cell = wrapperRow.insertCell(1);
        //        cell.style.width = '127px';
        var cellElem = document.createElement('textarea');
        cellElem.maxLength = "200";
        cellElem.rows = "4";
        cellElem.style.width = '208px';
        cellElem.id = CHP_SP.ID_DESCRIPTION;
        cellElem.onchange = CHP_SP.inputFieldChanged;
        cell.appendChild(cellElem);

        wrapperCol.appendChild(wrapperTable);
    }
    else {
        //Row to contain Page Type GroupBox
        var col = spContentRow.insertCell(0);
        CHP_SP.renderPageTypeGroupBox(col);
    }

    //Row to contain URL  / Upload groupbox
    var spContentRow = sidePanelLayout.insertRow(7);
    var col = spContentRow.insertCell(0);
    var uploadContainer = document.createElement("Div");
    uploadContainer.id = CHP_SP.ID_UPLOAD_CONTAINER;
    if (CHP_SP.isImage) {
        var uploadGroupBox = CHP_SP.createGroupBox(uploadContainer, CHP_SP.constants.IMG_LABEL_UPLOAD + ":");
        CHP_SP.renderUploadGroupBoxItems(CHP_SP.PAGE_TYPE_HTMLCONTENT, "", uploadGroupBox);
    }
    col.appendChild(uploadContainer);

    if (!CHP_SP.isImage) {
        var lang_Select = doc.getElementById(CHP_SP.ID_LANG_CODE);
        var lang_desc = doc.getElementById(CHP_SP.ID_LANG_CODE_DESC);
        var prodCode_Select = doc.getElementById(CHP_SP.ID_PROD_CODE);
        var prodCode_desc = doc.getElementById(CHP_SP.ID_PROD_CODE_DESC);
        var langCodeContainer = doc.getElementById(CHP_SP.ID_LANG_CODE_CONTAINER);
        langCodeContainer.style.display = "none";
        prodCode_Select.value = CHP_SP.PRODCODE_DEFAULT;
        prodCode_desc.innerHTML = " ";
        lang_Select.value = "";
        lang_desc.innerHTML = " ";
    }

    //show image inline
    if (CHP_SP.isImage) {
        var spContentRow = sidePanelLayout.insertRow(8);
        var col = spContentRow.insertCell(0);
        col.style.textAlign = 'center';
        var imgElem = document.createElement('img');
        imgElem.id = 'selectedUDOIcon';
        imgElem.style.maxWidth = 333 + 'px';
        imgElem.style.maxHeight = 333 + 'px';
        imgElem.style.borderStyle = 'dashed';
        imgElem.style.borderWidth = '1px';
        imgElem.style.borderColor = '#677174';
        imgElem.style.borderRadius = '8px';
        if (document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value == CHP_SP.constants.UDO_CREATE) {
            imgElem.style.display = 'none';
        }
        else {
            imgElem.style.display = 'inline';
        }
        col.appendChild(imgElem);
    }
}

CHP_SP.onUDCDataChange = function (srcElement, udcDesc, udc_Hash) {
    var selection = srcElement.value;
    var udcDescriptionText = udc_Hash[selection][1];
    udcDesc.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;" + udcDescriptionText;
}

CHP_SP.setValueForUDC = function (udc_selectBox, value) {
    for (i = 0; i < udc_selectBox.options.length; i++) {
        if (udc_selectBox.options[i].value == value) {
            udc_selectBox.options.selectedIndex = i;
            return i;
        }
    }
}

CHP_SP.inputFieldChanged = function () {
    CHP_SP.destroyErrorWindow();
    CHP_SP.setDirty(true);
}

CHP_SP.goChangePageType = function (e, pageType, fieldValue) {
    if (pageType == null || pageType == undefined) {
        selectedOption = CHP_SP.getPageTypeForActiveE1page();
    }
    else {
        selectedOption = pageType;
    }
    var legendText = "";
    if (selectedOption == CHP_SP.PAGE_TYPE_URL) {
        legendText = CHP_SP.constants.CHP_LABEL_PAGEURL;
        fieldValue = CHP_SP.activeE1page.pageURL;
    }
    else if (selectedOption == CHP_SP.PAGE_TYPE_HTMLCONTENT) {
        legendText = CHP_SP.constants.CHP_LABEL_HTMLUPLOAD + ":";
        fieldValue = "";
    }
    var uploadContainer = document.getElementById(CHP_SP.ID_UPLOAD_CONTAINER);
    for (var i = 0, len = uploadContainer.children.length; i < len; i++) {
        uploadContainer.removeChild(uploadContainer.children[i]);
    }
    var uploadGroupBox = CHP_SP.createGroupBox(uploadContainer, legendText);
    CHP_SP.renderUploadGroupBoxItems(selectedOption, fieldValue, uploadGroupBox);
}

CHP_SP.getPageTypeForActiveE1page = function () {
    var options = document.getElementsByName(CHP_SP.NAME_PAGE_TYPE);
    var selectedOption = 0;
    if (options) {
        for (i = 0; i < options.length; i++) {
            if (options[i].checked == true) {
                selectedOption = options[i].value;
                break;
            }
        }
    }
    return selectedOption;
}

CHP_SP.setPageType = function (pageType) {
    var options = document.getElementsByName(CHP_SP.NAME_PAGE_TYPE);
    if (options) {
        for (i = 0; i < options.length; i++) {
            if (pageType == options[i].value) {
                options[i].checked = true;
                break;
            }
        }
    }
}

CHP_SP.renderUploadGroupBoxItems = function (pageType, fieldValue, uploadGroupBox) {
    if (pageType == CHP_SP.PAGE_TYPE_URL) {
        var inputBox = document.createElement("input");
        inputBox.id = CHP_SP.ID_PAGE_URL;
        inputBox.setAttribute("type", "text");
        inputBox.style.width = "97%";
        inputBox.value = fieldValue;
        if (CHP_SP.ACCESSIBILITY) {
            inputBox.setAttribute("aria-label", CHP_SP.ID_PAGE_URL);
        }

        if (!inputBox.addEventListener) {
            inputBox.attachEvent("onchange", CHP_SP.inputFieldChanged);
        }
        else {
            inputBox.addEventListener("change", CHP_SP.inputFieldChanged, false);
        }

        uploadGroupBox.appendChild(inputBox);
        var buttonDiv = document.createElement("div");
        var buttonTable = document.createElement("table");
        buttonDiv.appendChild(buttonTable);
        var row = buttonTable.insertRow(0);
        var cell_1 = row.insertCell(0);
        var cell_2 = row.insertCell(1);
        var validateURLBtn = CHP_SP.createButton(cell_1, CHP_SP.ID_VALIDATE_BUTTON, CHP_SP.constants.CHP_VALIDATE_URL, "vaidateUrl", function () { CHP_SP.detectFrameBreaker(null); });
        var viewURLBtn = CHP_SP.createButton(cell_2, CHP_SP.ID_PREVIEW_BUTTON, CHP_SP.constants.CHP_VIEW_URL, "preview", CHP_SP.beginPreview);
        uploadGroupBox.appendChild(buttonDiv);
    }
    else if (pageType == CHP_SP.PAGE_TYPE_HTMLCONTENT) {
        var form = document.createElement("form");
        form.setAttribute("encType", "multipart/form-data");
        form.setAttribute("method", "POST");
        form.name = "FileUpload";
        form.id = "CHPUpload";
        uploadGroupBox.appendChild(form);

        //add hidden params
        if (!CHP_SP.isImage) {
            var inputhidden1 = document.createElement("input");
            inputhidden1.setAttribute("type", "hidden");
            inputhidden1.setAttribute("name", "stackId");
            inputhidden1.value = "1";
            form.appendChild(inputhidden1);

            var inputhidden2 = document.createElement("input");
            inputhidden2.setAttribute("type", "hidden");
            inputhidden2.setAttribute("name", "DATA_KEY");
            inputhidden2.value = "DATA_KEY_1374817590119";
            form.appendChild(inputhidden2);

            var inputhidden3 = document.createElement("input");
            inputhidden3.setAttribute("type", "hidden");
            inputhidden3.setAttribute("name", "DATA_TYPE");
            inputhidden3.value = "2";
            form.appendChild(inputhidden3);

            var inputhidden4 = document.createElement("input");
            inputhidden4.setAttribute("type", "hidden");
            inputhidden4.setAttribute("name", "RID");
            inputhidden4.value = "db421b6edeaab44";
            form.appendChild(inputhidden4);
        }
        else {
            var inputhidden3 = document.createElement("input");
            inputhidden3.setAttribute("type", "hidden");
            inputhidden3.setAttribute("name", "DATA_TYPE");
            inputhidden3.value = "1";
            form.appendChild(inputhidden3);
        }
        //------------------

        var inputFile = document.createElement("input");
        inputFile.setAttribute("type", "file");
        inputFile.id = CHP_SP.ID_FILEUPLOAD;
        inputFile.name = "FILE_KEY";
        inputFile.setAttribute("size", "40");
        inputFile.value = fieldValue;
        if (CHP_SP.isImage) {
            inputFile.addEventListener('click', CHP_SP.fileButtonClicked);
        }
        form.appendChild(inputFile);

        if (!CHP_SP.isImage) {
            if (CHP_SP.ACCESSIBILITY) {
                inputFile.setAttribute("aria-label", "E1PAGE FileUpload");
                inputFile.setAttribute("aria-describedby", "Browse Button to Upload HTML Content");
            }
            var buttonDiv = document.createElement("div");
            var buttonTable = document.createElement("table");
            buttonDiv.appendChild(buttonTable);
            var row = buttonTable.insertRow(0);
            var cell_1 = row.insertCell(0);
            var cell_2 = row.insertCell(1);
            var cell_3 = row.insertCell(2);
            var loadButton = CHP_SP.createButton(cell_1, CHP_SP.ID_UPLOAD_BUTTON, CHP_SP.constants.CHP_UPLOAD, "Load", CHP_SP.LoadFile);
            var viewContentBtn = CHP_SP.createButton(cell_2, CHP_SP.ID_PREVIEW_BUTTON, CHP_SP.constants.CHP_VIEW_CONTENT, "preview", CHP_SP.previewContent);
            var downloadBtn = CHP_SP.createButton(cell_3, CHP_SP.ID_DOWNLOAD_BUTTON, CHP_SP.constants.CHP_DOWNLOAD, "Download", CHP_SP.downloadContent);
            form.appendChild(buttonDiv);
        }
        else {
            inputFile.accept = "image/gif, image/jpeg, image/jpg, image/png";
        }
    }
}

CHP_SP.createButton = function (parentContainer, id, value, name, onclickHandler) {
    var btnControl = document.createElement("input");
    btnControl.setAttribute("type", "button");
    btnControl.id = id;
    btnControl.className = "button";
    btnControl.setAttribute("value", value);
    btnControl.name = name;
    //For Accesibility Users render Label for the input button tag
    if (CHP_SP.ACCESSIBILITY) {
        btnControl.setAttribute("role", "button");
        btnControl.setAttribute("aria-labelledby", value);
        btnControl.setAttribute("tabindex", "0");
    }
    btnControl.onclick = onclickHandler;
    btnControl.onmouseover = CHP_SP.onMouseOverButton;
    btnControl.onmouseout = CHP_SP.onMouseOutButton;
    btnControl.onmousedown = CHP_SP.onMouseDownButton;
    btnControl.onmouseup = CHP_SP.onMouseUpButton;
    btnControl.style.width = "92px";
    btnControl.style.height = "22px";
    btnControl.style.marginRight = "10px";
    parentContainer.appendChild(btnControl);
}

CHP_SP.LoadFile = function () {
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    var fileNameElem = document.getElementById(CHP_SP.ID_FILEUPLOAD);
    var fileName = fileNameElem.value;
    if (!CHP_SP.isUploadFileValid(fileName)) {
        CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
        CHP_SP.showErrorWindow(fileNameElem, CHP_SP.constants.CHP_UPLOADCONTENT_INVALID);
        return;
    }
    var formData = new FormData(top.document.getElementById("CHPUpload"));
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.FILEUPLOADER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_UPLOAD);
    e1URL.setParameter("src", "E1Page");
    WebObjectUtil.setProcInd(CHP_SP.namespace);

    WebObjectUtil.sendFile(formData, "POST", null, e1URL.toString(), CHP_SP.fileuploadDone, true);
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
}

CHP_SP.showUploadAlert = function (uploadStatus) {
    if (uploadStatus) {
        CHP_SP.isUploadSuccessful = true;
        CHP_SP.setDirty(true);
        CHP_SP.enableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], CHP_SP.onBeforeSave);
        alert(CHP_SP.constants.CHP_UPLOAD_SUCCESS);
    }
    else {
        CHP_SP.isUploadSuccessful = false;
        CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
        CHP_SP.showErrorWindow(document.getElementById(CHP_SP.ID_FILEUPLOAD), CHP_SP.constants.CHP_UPLOADCONTENT_INVALID);
        alert(CHP_SP.constants.CHP_UPLOAD_FAIL);
    }
}

CHP_SP.fileuploadDone = function (retStr, param2) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    (testdiv = document.createElement("div")).innerHTML = param2;
    var inputTags = testdiv.getElementsByTagName("input");
    var uploadSuccess = false;
    for (var i = 0; i < inputTags.length; i++) {
        if (inputTags[i].name == "UploadStatus" && inputTags[i].value == "TRUE") {
            uploadSuccess = true;
            break;
        }
    }
    CHP_SP.showUploadAlert(uploadSuccess);
}

CHP_SP.downloadContent = function () {
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    var pageType = CHP_SP.getPageTypeForActiveE1page();
    if (pageType == CHP_SP.PAGE_TYPE_HTMLCONTENT) {
        var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.CMD_DOWNLOAD);
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.onAfterDownload, true);
    }
}

CHP_SP.onAfterDownload = function (container, retStr) {
    CHP_SP.showMessageDiv(false);
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
            return;
        }
    }
    else {
        //use a hidden iframe that calls the download url maflet request
        var embedUrl = "<iframe tabindex=\"-1\" width=1 height=1 scrolling=no frameborder=0 src=\""
                 + retStr
                 + "\">IFrame support is required.</iframe>";
        //send the request to Message section to fire off the download
        CHP_SP.showMessageDiv(true, embedUrl, false);
        //hide the Message section, so a blank box does not show
        CHP_SP.showMessageDiv(false);
    }
}

CHP_SP.renderPageTypeGroupBox = function (container) {
    var groupBox_ForPageType = CHP_SP.createGroupBox(container, CHP_SP.constants.CHP_LABEL_SELECT_PAGETYPE);
    var sb = new PSStringBuffer();
    sb.append("<div align='").append(CHP_SP.isRTL ? "center" : "left").append("'>");
    sb.append("<input type='radio' tabindex=0 id='PageTypeURL' name='").append(CHP_SP.NAME_PAGE_TYPE).append("' value='").append(CHP_SP.PAGE_TYPE_URL).append("'");
    if (CHP_SP.ACCESSIBILITY) {
        sb.append(" role='radio' aria-label='").append(CHP_SP.constants.CHP_LABEL_RADIO_PAGEURL).append("'");
    }
    sb.append("/>");
    sb.append(CHP_SP.isRTL ? "&rlm;" : "").append(CHP_SP.constants.CHP_LABEL_RADIO_PAGEURL);
    sb.append("<input type='radio' role='radio' tabindex=0 id='PageTypeHtml' name='").append(CHP_SP.NAME_PAGE_TYPE).append("' value='").append(CHP_SP.PAGE_TYPE_HTMLCONTENT).append("'");
    if (CHP_SP.ACCESSIBILITY) {
        sb.append(" role='radio' aria-label='").append(CHP_SP.constants.CHP_LABEL_HTMLUPLOAD).append("'");
    }
    sb.append("/>");
    sb.append(CHP_SP.isRTL ? "&rlm;" : "").append(CHP_SP.constants.CHP_LABEL_HTMLUPLOAD);
    sb.append("</div>");
    groupBox_ForPageType.innerHTML = groupBox_ForPageType.innerHTML + sb;

    var pageTypeRadioElements = document.getElementsByName(CHP_SP.NAME_PAGE_TYPE);
    for (var i = 0; i < pageTypeRadioElements.length; i++) {
        if (!pageTypeRadioElements[i].addEventListener) {
            pageTypeRadioElements[i].attachEvent("onclick", CHP_SP.goChangePageType);
        }
        else {
            pageTypeRadioElements[i].addEventListener("click", CHP_SP.goChangePageType, false);
        }
    }
}

CHP_SP.createGroupBox = function (container, legendTitle) {
    var grpCustomVar = document.createElement("fieldset");
    grpCustomVar.setAttribute('class', 'GroupBox');
    grpCustomVar.setAttribute('style', 'padding:5px;margin:5px;');
    if (legendTitle != null) {
        var grpLegend = document.createElement("legend");
        var grpLegendTitle = document.createTextNode(legendTitle);
        grpLegend.appendChild(grpLegendTitle);
        grpCustomVar.appendChild(grpLegend);
    }
    container.appendChild(grpCustomVar);
    return grpCustomVar;
}

CHP_SP.getMenusArray = function () {
    var menusArray = new Array();

    menusArray.add({ outImg: window['E1RES_share_images_ulcm_save_ena_png'], overImg: window['E1RES_share_images_ulcm_save_hov_png'], title: CHP_SP.constants.UDO_SAVE, onclick: 'CHP_SP.onBeforeSave()', id: CHP_SP.ID_SAVE_ICON });
    if (!CHP_SP.isImage) {
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_saveas_ena_png'], overImg: window['E1RES_share_images_ulcm_saveas_hov_png'], title: CHP_SP.constants.UDO_SAVE_AS, onclick: 'CHP_SP.onBeforeSaveAs(this)', id: CHP_SP.ID_SAVE_AS_ICON });
    }
    if (CHP_SP.userHasPublishAuthority) {
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_requestpublish_ena_png'], overImg: window['E1RES_share_images_ulcm_requestpublish_hov_png'], title: CHP_SP.constants.UDO_REQUEST_PUBLISH, onclick: 'CHP_SP.goRequestPublishBegin(this)', id: CHP_SP.ID_REQUEST_PUBLISH_ICON });
    }
    if (CHP_SP.userHasReserveAuthority) {
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_reserve_ena_png'], overImg: window['E1RES_share_images_ulcm_reserve_hov_png'], title: CHP_SP.constants.UDO_RESERVE, onclick: 'CHP_SP.goReserveBegin(this)', id: CHP_SP.ID_RESERVE_ICON });
    }
    menusArray.add({ outImg: window['E1RES_share_images_ulcm_delete_ena_png'], overImg: window['E1RES_share_images_ulcm_delete_hov_png'], title: CHP_SP.constants.UDO_DELETE, onclick: 'CHP_SP.goDeleteE1page(this)', id: CHP_SP.ID_DELETE_ICON });
    menusArray.add({ outImg: window['E1RES_share_images_ulcm_notes_ena_png'], overImg: window['E1RES_share_images_ulcm_notes_hov_png'], title: CHP_SP.constants.UDO_NOTES, onclick: 'CHP_SP.goFetchNotes()', id: CHP_SP.ID_NOTES_ICON });

    return menusArray;
}

CHP_SP.renderSidePanelMenuItem = function (menuBarContainer_Row, i, id, title, outImg, overImg, eventHandler) {
    var iconContainer = menuBarContainer_Row.insertCell(i);
    iconContainer.className = 'SPMenuIconTD';
    CHP_SP.renderMotionImg(iconContainer, id, title, outImg, overImg, eventHandler);
}

// Private method: render the menu bar in the side panel
CHP_SP.renderSidePanelMenu = function (container) {
    var menusArray = CHP_SP.getMenusArray();
    var buttonBarTable = document.createElement("table");
    buttonBarTable.className = "SPMenuTable";
    buttonBarTable.id = "SPMenuBarTable";
    buttonBarTable.style.cellspacing = "0";
    buttonBarTable.style.cellpadding = "0";
    buttonBarTable.style.border = "0";
    buttonBarTable.style.width = "100%";
    buttonBar = buttonBarTable.insertRow(0);

    for (var i = 0; i < menusArray.length; i++) {
        var menu = menusArray[i];
        CHP_SP.renderSidePanelMenuItem(buttonBar, i, menu.id, menu.title, menu.outImg, menu.overImg, menu.onclick);
    }
    var iconContainer = buttonBar.insertCell(i++);
    var aboutFuncAction = 'alert(\"No contextual information available at this time.\")'; //Default to the default about function call
    var hover = "No hover info";
    if (CHP_SP.isImage) {
        hover = CHP_SP.constants.JS_SIDE_PANEL_IMG_HOVER;
    }
    else {
        hover = CHP_SP.constants.JS_SIDE_PANEL_CHP_HOVER;
    }
    aboutFuncAction = CHP_SP.viewInfo;

    var aboutIconContainer = buttonBar.insertCell(i++);
    aboutIconContainer.className = "SPMenuInfoIconTD";
    aboutIconContainer.align = 'right';
    CHP_SP.renderMotionImg(aboutIconContainer, 'SidePanelAbout', hover, window['E1RES_share_images_ulcm_info_ena_png'], window['E1RES_share_images_ulcm_info_hov_png'], aboutFuncAction);

    var closeFuncAction = CHP_SP.onClose;
    var closeIconContainer = buttonBar.insertCell(i++);
    closeIconContainer.className = "SPMenuCloseIconTD";
    closeIconContainer.align = 'right';
    CHP_SP.renderMotionImg(closeIconContainer, 'SidePanelClose', 'Close Side Panel', window['E1RES_share_images_ulcm_close_ena_png'], window['E1RES_share_images_ulcm_close_hov_png'], closeFuncAction);

    container.appendChild(buttonBarTable);
}

CHP_SP.renderMotionImg = function (iconContainer, id, title, outImg, overImg, eventHandler) {
    var img = document.createElement("img");
    img.className = 'clickableImage';
    if (localStorage.getItem("userAccessibilityMode"))
        CHP_SP.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;

    if (id)
        img.id = id;
    img.setAttribute("src", outImg);
    img.setAttribute("title", title);
    img.setAttribute("alt", title);
    img.setAttribute("outImg", outImg);
    img.setAttribute("overImg", overImg);
    img.setAttribute("tabIndex", 0);
    img.onmouseover = CHP_SP.onMouseOverEvent;
    img.onmouseout = CHP_SP.onMouseOutEvent;
    img.onclick = eventHandler;
    // attach onkeydown eventhandler for close button
    if (iconContainer.className == "SPMenuCloseIconTD")
        img.onkeydown = eventHandler;

    if (CHP_SP.ACCESSIBILITY) {
        img.setAttribute("role", "link");
        img.setAttribute("aria-label", title);
    }
    iconContainer.appendChild(img);
}

CHP_SP.onMouseOverEvent = function (e) {
    if (!e)
        element = this;
    else
        element = CHP_SP.getEventSource(e);
    if (element != null && element.tagName == 'IMG') {
        var imgSrc = element.getAttribute('overImg');
        if (imgSrc != null && imgSrc != this.src) {
            element.src = imgSrc;
        }
    }
}

CHP_SP.onMouseOutEvent = function (e) {
    if (!e)
        element = this;
    else
        element = CHP_SP.getEventSource(e);

    if (element != null && element.tagName == 'IMG') {
        var imgSrc = element.getAttribute('outImg');
        if (imgSrc != null && imgSrc != this.src) {
            element.src = imgSrc;
        }
    }
}

CHP_SP.getEventSource = function (e) {
    if (window.event) {
        //IE has simple method to access originating element from event
        this.getEventSource = function () { return window.event.srcElement; };
    }
    else {
        //Firefox needs a slightly more complex technique
        this.getEventSource = this.getEventSourceW3C;
    } return this.getEventSource(e);
}

// This method is the getEventSource redirection for non-IE browsers
CHP_SP.getEventSourceW3C = function (e) {
    var evtSource = e.currentTarget;
    return (evtSource && evtSource.nodeType == 3) ? evtSource.parentNode : evtSource;
}

CHP_SP.onOpen = function () {
    CHP_SP.updatePersonalizationMenuForCHP();
    hideAllDropdownMenus();
}

CHP_SP.onCloseHandler = function () {
    if (!CHP_SP.proceedWithoutSave())
        return;
    CHP_SP.MANAGEMENT_PANE_OPEN = false;
    CHP_SP.closeSidePanel();
    CHP_SP.updatePersonalizationMenuForCHP();
    CHP_SP.closeVisualAssistWindow();
}

CHP_SP.onClose = function (evt) {
    var evt = evt ? evt : window.event;
    //tabbing to the X button and hitting ENTER,close the panel
    if (evt.type == "keydown") {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) // capture Enter 
        {
            CHP_SP.onCloseHandler();
        }
    }
    else {
        CHP_SP.onCloseHandler();
    }
}

CHP_SP.closeSidePanel = function () {
    if (CHP_SP.MODE > 0) {
        CHP_SP.closePreview();
    }
    if (CHP_SP.vaWindow != null) {
        CHP_SP.vaWindow.close();
        CHP_SP.vaWindow = null;
    }
    CHP_SP.MODE = 0;
    CHP_SP.ISREADONLYMODE = false;
    if (document.getElementById('UdoIcons') != null) {
        window.openedPopup.onClose();
    }

    CHP_SP.sidePanelWidth = 350;
    var e1AppFrameContainer = document.getElementById("e1AppFrameContainer");
    var sidePanel = document.getElementById(CHP_SP.ID_FRAME_TABLE);
    removeChildren(sidePanel.firstChild);
    e1AppFrameContainer.removeChild(sidePanel);

    // reset the size
    var outerRCUX = document.getElementById("outerRCUX");

    var wAvailable;
    var totalWidth = JSSidePanelRender.isIOS ? window.innerWidth : document.body.offsetWidth;

    if (CARO.isVertical()) {
        wAvailable = (CARO.carousel.expanded == 1) ? (totalWidth - CARO.carousel.offsetWidth - 12) : totalWidth - 7;
    }
    else {
        wAvailable = totalWidth;
    }
    // reset the E1 containers size
    CHP_SP.resizeElem(outerRCUX, wAvailable);

    outerRCUX.style = null;
}

CHP_SP.closePreview = function () {
    if (CHP_SP.isImage) {
        CHP_SP.objectForEditing = null;
        //notify the server and clear the flags
        var dta = top.document.getElementById('e1menuAppIframe').contentWindow.JDEDTAFactory.getInstance(CHP_SP.namespace);
        dta.doAsynPostById("exitLCMMode", null, null);
    }
    else {
        var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
        e1UrlCreator.setURI('AjaxUIState');
        e1UrlCreator.setParameter('action', 'closeTab');
        var url = e1UrlCreator.toString();
        var req = e1UrlCreator.createXMLHttpRequest();
        req.open('POST', url, true);
        req.setRequestHeader("Content-Type", "text/xml");
        req.send('');
        CHP_SP.goHome();

        //This is to switch back to the aplication where the preview was launched from (p98220u/p98220w)
        //The runapp state returns an array with first element as compId of last opned app. 
        if (self.parent && self.parent.MENUUTIL) {
            var lastOpenApp_compId = self.jdeMenuParent.getRunAppState()[0];
            RunOldApp(lastOpenApp_compId, true);
        }
    }
}

CHP_SP.setDirty = function (dirty) {
    CHP_SP.dirty = dirty;
}
// Public method to check if the current active e1page is already modified without save
CHP_SP.isDirty = function () {
    return CHP_SP.dirty;
}

CHP_SP.goSaveMiddle = function () {
    if (!CHP_SP.isImage) {
        var nameEntered = document.getElementById(CHP_SP.ID_NAME_ENTERED).value.Trim();
        if (WebObjectUtil.isBlankOrEmptyName(nameEntered)) {
            alert(CHP_SP.constants.UDO_BAD_NAME);
            return false;
        }
        if (!WebObjectUtil.isValidName(nameEntered, CHP_SP.constants.UDO_BAD_NAME)) {
            return false;
        }
        CHP_SP.activeE1page.tabName = nameEntered;
        var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.CMD_SAVE);
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.onAfterSave, true);
        return true;
    }
    else {
        var formData = new FormData(top.document.getElementById("CHPUpload"));
        var fileName;
        var isTextOnly = false;
        var isNew = document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value == CHP_SP.constants.UDO_CREATE;
        if (isNew) {
            fileName = top.document.getElementById("E1PfileUpload").files[0].name;
            var uploadName = fileName.substring(0, fileName.lastIndexOf("."));
            if (!WebObjectUtil.isValidName(uploadName, CHP_SP.constants.UDO_BAD_NAME)) {
                return;
            }
        }
        else {
            fileName = document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value;
            if (!document.getElementById("E1PfileUpload").files[0]) {
                isTextOnly = true;
            }
        }
        var desc;
        if (document.getElementById(CHP_SP.ID_DESCRIPTION) != null) {
            desc = document.getElementById(CHP_SP.ID_DESCRIPTION).value;
            if (desc == "") {
                desc = fileName;
            }
        }

        var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
        e1URL.setURI(CHP_SP.MANAGER_SERVICE);
        e1URL.setParameter("cmd", CHP_SP.CMD_SAVE);
        e1URL.setParameter("objectType", CHP_SP.objectType);
        e1URL.setParameter("desc", desc);
        e1URL.setParameter("hasToken", CHP_SP.activeE1page.hasToken);
        e1URL.setParameter("tokenProjectName", CHP_SP.activeE1page.tokenProjectName);
        if (isTextOnly) {
            e1URL.setParameter("textOnly", isTextOnly);
        }
        var input = top.document.getElementById(CHP_SP.BLANK_E1PAGE_ID);
        if (input.getAttribute("omwObjectName") != null) {
            e1URL.setParameter("omwObjectName", input.getAttribute("omwObjectName"));
            e1URL.setParameter("user", input.getAttribute("user"));
            e1URL.setParameter("tabName", input.value);
        }
        else {
            e1URL.setParameter("id", CHP_SP.BLANK_E1PAGE_ID);
            e1URL.setParameter("tabName", fileName);
            //            e1URL.setParameter("prodCode", document.getElementById(CHP_SP.ID_PROD_CODE).value);
        }

        if (window.XMLHttpRequest) {
            var xmlRequest = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            var xmlRequest = new ActiveXObject("MSXML2.XMLHTTP");
        }
        xmlRequest.open('POST', e1URL.toString());
        var handlerFunction = WebObjectUtil.getReadyStateHandler(xmlRequest, CHP_SP.onAfterSave);
        xmlRequest.onreadystatechange = handlerFunction;
        xmlRequest.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        xmlRequest.setRequestHeader("Cache-Control", "no-cache");
        xmlRequest.send(formData);
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        return true;
    }
}

CHP_SP.onAfterSave = function (containerId, retStr) {
    CHP_SP.showMessageDiv(false);
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
            return;
        }
    }
    CHP_SP.setDirty(false);
    if (!CHP_SP.isImage) {
        CHP_SP.goFetchE1pagesList();
    }
    else {
        var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
        e1URL.setURI(CHP_SP.MANAGER_SERVICE);
        e1URL.setParameter("cmd", CHP_SP.CMD_GETLASTSAVEDIMAGE);
        var input = top.document.getElementById(CHP_SP.BLANK_E1PAGE_ID);
        if (input.getAttribute("omwObjectName") != null) {
            e1URL.setParameter("omwObjectName", input.getAttribute("omwObjectName"));
            //remove omwObjectName from the attribute
            input.removeAttribute("omwObjectName");
            input.removeAttribute("user");
        }
        e1URL.setParameter("buster", new Date().getTime());
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        WebObjectUtil.sendXMLReq("GET", null, e1URL.toString(), CHP_SP.updateActiveIcon, true);
    }
}

CHP_SP.onBeforeSave = function () {
    if (CHP_SP.isImage) {
        CHP_SP.goUploadAndSaveImage();
    }
    else {
        var pageType = CHP_SP.getPageTypeForActiveE1page();
        if (CHP_SP.PAGE_TYPE_URL == pageType && CHP_SP.isDirty()) {
            CHP_SP.detectFrameBreaker(CHP_SP.goSaveBegin);
        }
        else {
            CHP_SP.goSaveBegin();
        }
    }
}

CHP_SP.goUploadAndSaveImage = function () {
    //no file selectd
    if (!document.getElementById("E1PfileUpload").files[0] && document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value == CHP_SP.constants.UDO_CREATE) {
        alert(CHP_SP.constants.UDO_ICON_UPLOAD_IMAGE);
        return;
    }
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    CHP_SP.goSaveMiddle();
}

CHP_SP.goSaveBegin = function () {

    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    var saveLanguageRecord = false;
    if (CHP_SP.validateInput())
        return;
    var newLanguageCode = CHP_SP.activeE1page.langCode;
    if (CHP_SP.activeE1page.langCode && CHP_SP.trimStr(CHP_SP.activeE1page.langCode).length > 0) {
        saveLanguageRecord = true;
    }
    if (!CHP_SP.isDirty())
        return;

    if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
        //defaultName = CHP_SP.activeE1page.tabName;
        defaultName = CHP_SP.constants.UDO_PROMPT_DEFAULT_NAME;
        var labels = new Array(defaultName, CHP_SP.constants.UDO_PROMPT_NAME);
        JSSidePanelRender.renderPromptForName(document, CHP_SP.ID_SAVE_ICON, 2, labels, CHP_SP.NAME_LENGTH, CHP_SP.goSaveMiddle);
        // For accessibility users make the Close in the Name prompt accessible
        if (CHP_SP.ACCESSIBILITY) {
            CHP_SP.setAltForCloseInNamePrompt();
        }
    }
    else {
        if (saveLanguageRecord) {
            //Language Record
            var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.CMD_TRANSLATE);
            if (WebObjectUtil.isBlankOrEmptyName(CHP_SP.activeE1page.tabName)) {
                //alert(CHP_SP.constants.CHP_BLANK_NOT_ALLOWED);
                CHP_SP.showErrorWindow(document.getElementById(CHP_SP.ID_SAVE_ICON), CHP_SP.constants.CHP_BLANK_NOT_ALLOWED);
                return false;
            }
        }
        else {
            //Default domestic language record
            CHP_SP.activeE1page.langCode = " ";
            var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.CMD_SAVE);
        }
        CHP_SP.objectForEditing = CHP_SP.activeE1page.id;
        WebObjectUtil.setProcInd(CHP_SP.namespace);
        WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.onAfterSave, true);
    }
}

CHP_SP.onBeforeSaveAs = function (event) {
    var pageType = CHP_SP.getPageTypeForActiveE1page();
    if (CHP_SP.PAGE_TYPE_URL == pageType && CHP_SP.isDirty()) {
        CHP_SP.detectFrameBreaker(CHP_SP.goSaveAsBegin);
    }
    else {
        CHP_SP.goSaveAsBegin();
    }
    // For accessibility stop event bubbling else the saveas event is fired twice
    if (CHP_SP.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        GCMH.stopEventBubbling(evt);
    }
}

CHP_SP.goSaveAsBegin = function () {
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    // validateInput returns true if there was an error
    if (CHP_SP.validateInput('saveas'))
        return;

    var defaultName = CHP_SP.constants.UDO_PROMPT_DEFAULT_NAME;
    var labels = new Array(defaultName, CHP_SP.constants.UDO_PROMPT_NAME);
    JSSidePanelRender.renderPromptForName(document, CHP_SP.ID_SAVE_ICON, 2, labels, CHP_SP.NAME_LENGTH, CHP_SP.goSaveAsMiddle);
    // For accessibility users make the Close in the Name prompt accessible
    if (CHP_SP.ACCESSIBILITY) {
        CHP_SP.setAltForCloseInNamePrompt();
    }
}

CHP_SP.goSaveAsMiddle = function () {
    var nameEntered = document.getElementById(CHP_SP.ID_NAME_ENTERED).value.Trim();
    if (WebObjectUtil.isBlankOrEmptyName(nameEntered)) {
        alert(CHP_SP.constants.UDO_BAD_NAME);
        return false;
    }
    if (!WebObjectUtil.isValidName(nameEntered, CHP_SP.constants.UDO_BAD_NAME)) {
        return false;
    }
    var oidToCopy = CHP_SP.activeE1page.id;

    CHP_SP.activeE1page.tabName = nameEntered;
    var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.CMD_SAVEAS);
    //CHP_SP.activeE1page.id = CHP_SP.BLANK_E1PAGE_ID;
    e1URL.setParameter("id", CHP_SP.BLANK_E1PAGE_ID);
    //CHP_SP.activeE1page.tabName = nameEntered;
    e1URL.setParameter("tabName", CHP_SP.HTMLDecoder(nameEntered.Trim()));
    //CHP_SP.activeE1page.lineType = "1";
    e1URL.setParameter("lineType", "1");
    //CHP_SP.activeE1page.isCheckedOut = false;
    e1URL.setParameter("isCheckedOut", false);

    // if Save As is done on a Public E1page then change it to a Private E1page
    if (!CHP_SP.activeE1page.isPrivate) {
        CHP_SP.activeE1page.isPrivate = true;
    }

    e1URL.setParameter("oidToCopy", oidToCopy);
    e1URL.setParameter("hasNewHTMLContent", (CHP_SP.isUploadSuccessful == true) ? '1' : '0');

    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.goSaveAsEnd, true);
    return true;
}

// Process the response from the maflet service
CHP_SP.goSaveAsEnd = function (containerId, retStr) {
    CHP_SP.showMessageDiv(false);
    CHP_SP.showReservedBy();
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
            return;
        }
    }
    CHP_SP.activeE1page.id = CHP_SP.BLANK_E1PAGE_ID;
    CHP_SP.goFetchE1pagesList();
    CHP_SP.setDirty(false);
}

CHP_SP.goDeleteE1page = function (event) {
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    var cmd = CHP_SP.CMD_DELETE;
    var deleteConfirmation;
    if (!CHP_SP.isImage) {
        deleteConfirmation = CHP_SP.constants.CHP_CONFIRM_DELETE;
        if (CHP_SP.activeE1page.langCode.Trim().length > 0) {
            cmd = CHP_SP.CMD_DEL_TRANSLATE; //override cmd
        }

        var e1pageList = document.getElementById(CHP_SP.ID_E1PAGESLIST);
        var e1PageName = e1pageList.options[e1pageList.options.selectedIndex].innerHTML;
        var confirmToDelete = confirm(deleteConfirmation + " (" + CHP_SP.HTMLDecoder(e1PageName) + " " + CHP_SP.activeE1page.langCode.Trim() + ")");
    }
    else {
        deleteConfirmation = CHP_SP.constants.IMG_CONFIRM_DELETE;
        var e1PageName = CHP_SP.activeE1page.tabName;
        var confirmToDelete = confirm(deleteConfirmation + " (" + CHP_SP.HTMLDecoder(e1PageName) + ")");
    }
    if (confirmToDelete) {
        CHP_SP.sendDeleteE1pageCmd(cmd); // Delete from OMW tables
    }
    else {
        return false;
    }
    // For accessibility stop event bubbling else the delete event is fired twice
    if (CHP_SP.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        GCMH.stopEventBubbling(evt);
    }
}

CHP_SP.sendDeleteE1pageCmd = function (cmd) {
    if (CHP_SP.isImage) {
        var e1URL = CHP_SP.parameterizeActiveE1page(cmd);
        e1URL.setURI(CHP_SP.MANAGER_SERVICE);
        e1URL.setParameter("cmd", cmd);
    }
    else {
        var e1URL = CHP_SP.parameterizeActiveE1page(cmd);
        e1URL.setParameter("id", CHP_SP.activeE1page.id);
        e1URL.setParameter("name", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.name.Trim()));
        // passing omw object name 
        if (CHP_SP.activeE1page.omwObjectName != null) {
            e1URL.setParameter("omwObjectName", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.omwObjectName.Trim()));
        }
        if (CHP_SP.activeE1page.langCode.Trim().length > 0) {
            e1URL.setParameter("langCode", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.langCode.Trim()));
        }
        if (cmd == CHP_SP.CMD_DELETE) {
            e1URL.setParameter("objectType", CHP_SP.objectType);
        }
        e1URL.setParameter("desc", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.desc));
    }
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.goDeleteE1pageEnd);
}

CHP_SP.goDeleteE1pageEnd = function (container, retStr) {
    //set message Div
    CHP_SP.showMessageDiv(false);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
        }
    }
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    CHP_SP.setDirty(false);
    //CHP_SP.resetActiveE1Page();
    if (CHP_SP.isImage) {
        document.getElementById('newImage').checked = true;
        CHP_SP.clickNewIconRadioButton();
    }
    else {
        CHP_SP.goFetchE1pagesList();
    }
}

CHP_SP.goRequestPublishBegin = function (event) {
    CHP_SP.destroyErrorWindow();
    CHP_SP.showMessageDiv(false);
    if (CHP_SP.userHasPublishAuthority) {
        if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
            CHP_SP.showErrorWindow(document.getElementById(CHP_SP.ID_E1PAGESLIST), CHP_SP.constants.CHP_SELECT_E1PAGE);
        }
        else if (CHP_SP.isDirty()) {
            // save before publish
            CHP_SP.showErrorWindow(document.getElementById(CHP_SP.ID_E1PAGESLIST), CHP_SP.constants.CHP_CHANGES_NOT_SAVED);
        }
        else {
            if (CHP_SP.activeE1page.isPrivate && CHP_SP.activeE1page.isPersonal) {
                //No need to show PromptForName for request publish
                //pop up requestor note box
                CHP_SP.activeE1page.event = "requestPublish";
            }
            else {
                CHP_SP.activeE1page.event = "reRequestPublish";
            }
            if (CHP_SP.activeE1page.canShowPublishWarning) {
                var udoType;
                if (CHP_SP.isImage) {
                    udoType = CHP_SP.constants.UDO_TYPE_IMAGE;
                }
                else {
                    udoType = CHP_SP.constants.UDO_TYPE_E1PAGE;
                }
                var labels = new Array(CHP_SP.constants.UDO_ENTER_APPROVER_NOTE, CHP_SP.constants.UDO_CONFIRM_REQUEST_PUBLISH.format(udoType), CHP_SP.constants.UDO_CONFIRM_REQ_PUB_OPTION.format(udoType));
                JSSidePanelRender.renderPromptForName(document, CHP_SP.ID_SAVE_ICON, 1, labels, CHP_SP.NAME_LENGTH, CHP_SP.goRequestPublishMiddle, null, null);
            }
            else {
                CHP_SP.goRequestPublishMiddle("");
            }
        }
    }
    else {
        // We should not get here - button should have been disabled if no authority
        // if there is a way to get here, we should put the below string in OWResource.java
        CHP_SP.showErrorWindow(document.getElementById(CHP_SP.ID_REQUEST_PUBLISH_ICON), "You do not have authority to publish E1pages.");
    }
    // For accessibility stop event bubbling else the req publish event is fired twice
    if (CHP_SP.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        GCMH.stopEventBubbling(evt);
    }
    return false;
}

// Event handler called when after publish name prompt.
CHP_SP.goRequestPublishMiddle = function (approvernotes) {
    var e1URL = CHP_SP.parameterizeActiveE1page(CHP_SP.activeE1page.event);
    e1URL.setParameter("approvernotes", approvernotes);
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.goRequestPublishEnd);
    return true;
}

// Event handler called when the publish finishes
CHP_SP.goRequestPublishEnd = function (containerId, retStr) {
    CHP_SP.showMessageDiv(false);
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
        }
        return;
    }
    CHP_SP.setDirty(false);
    if (CHP_SP.isImage) {
        CHP_SP.refetchImage(0);
    }
    else {
        CHP_SP.goFetchE1pagesList();
    }
}

CHP_SP.goReserveBegin = function (event) {
    CHP_SP.showMessageDiv(false);
    if (CHP_SP.userHasReserveAuthority) // button should not show, or should have been disabled if no authority
    {
        if (CHP_SP.activeE1page.isRework || CHP_SP.activeE1page.isRequestPublished || !CHP_SP.activeE1page.isPrivate) {
            CHP_SP.activeE1page.event = "reserve";
            var e1URL = CHP_SP.parameterizeActiveE1page("reserve");
            WebObjectUtil.setProcInd(CHP_SP.namespace);
            WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.goReserveEnd, true);
        }
        else if (CHP_SP.activeE1page.isPrivate && CHP_SP.activeE1page.isCheckedOut)     //reserved
        {
            CHP_SP.activeE1page.event = "unreserve";
            var e1URL = CHP_SP.parameterizeActiveE1page("unreserve");
            WebObjectUtil.setProcInd(CHP_SP.namespace);
            WebObjectUtil.sendXMLReq("POST", null, e1URL.toString(), CHP_SP.goUnReserveEnd, true);
        }
        // For accessibility stop event bubbling else the reserve event is fired twice
        if (CHP_SP.ACCESSIBILITY) {
            var evt = event ? event : window.event;
            GCMH.stopEventBubbling(evt);
        }
        return true;
    }

    return false;
}

CHP_SP.goReserveEnd = function (containerId, retStr) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
            return;
        }
        else {
            CHP_SP.activeE1page.id = tag;
        }
    }
    if (CHP_SP.isImage) {
        CHP_SP.refetchImage(1);
    }
    else {
        CHP_SP.goFetchE1pagesList();
    }
    CHP_SP.setDirty(false);
}

CHP_SP.goUnReserveEnd = function (containerId, retStr) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    var sepIndex = retStr.indexOf(CHP_SP.SEP_CHAR);
    if (sepIndex > 0) {
        var tag = retStr.substring(0, sepIndex);
        if (tag == "ERROR") {
            CHP_SP.showMessageDiv(true, retStr.substring(sepIndex + 1), true);
        }
        else {
            if (!CHP_SP.isImage) {
                CHP_SP.activeE1page.id = tag;
            }
        }
    }
    if (CHP_SP.isImage) {
        CHP_SP.refetchImage(2);
    }
    else {
        CHP_SP.goFetchE1pagesList();
    }
    CHP_SP.setDirty(false);
}

CHP_SP.goAddTranslation = function () {

    CHP_SP.ISERROR = false;
    CHP_SP.langError = null;
    CHP_SP.prodError = null;
    var doc = document;
    CHP_SP.activeE1page = CHP_SP.getActiveE1Page(CHP_SP.e1PageList);
    if (CHP_SP.activeE1page.pageType != CHP_SP.PAGE_TYPE_URL && CHP_SP.activeE1page.isPrivate) {
        if (CHP_SP.activeE1page.isRequestPublished)
            return;
    }
    var objectToTranslate = new Object();
    objectToTranslate.name = CHP_SP.activeE1page.name;
    objectToTranslate.id = CHP_SP.activeE1page.id;
    objectToTranslate.prodCode = CHP_SP.activeE1page.prodCode;
    objectToTranslate.langCode = CHP_SP.activeE1page.langCode;
    objectToTranslate.pageType = CHP_SP.activeE1page.pageType;
    objectToTranslate.omwObjectName = CHP_SP.activeE1page.omwObjectName;
    objectToTranslate.tabName = CHP_SP.HTMLDecoder(CHP_SP.activeE1page.tabName);
    objectToTranslate.desc = CHP_SP.HTMLDecoder(CHP_SP.activeE1page.desc);

    CHP_SP.setDirty(false);
    var objectNameElem = doc.getElementById(CHP_SP.ID_OBJECT_NAME);
    objectNameElem.value = objectToTranslate.name;
    objectNameElem.disabled = true;

    var prodCodeElem = doc.getElementById(CHP_SP.ID_PROD_CODE);
    var prodCodeImg = doc.getElementById(CHP_SP.ID_PROD_CODE_IMG);
    prodCodeImg.style.display = "none";
    prodCodeElem.disabled = true;

    var langCodeContainer = doc.getElementById(CHP_SP.ID_LANG_CODE_CONTAINER);
    langCodeContainer.style.display = "table-row";

    var pageTitleListContainer = doc.getElementById(CHP_SP.ID_PAGE_TITLE_CONTAINER);
    pageTitleListContainer.style.display = "table-row";
    var pageTitle = doc.getElementById(CHP_SP.ID_PAGE_TITLE);
    pageTitle.value = objectToTranslate.tabName;
    var pageDescription = doc.getElementById(CHP_SP.ID_DESCRIPTION);
    pageDescription.value = objectToTranslate.desc;

    CHP_SP.setPageType(objectToTranslate.pageType);
    CHP_SP.goChangePageType(null, objectToTranslate.pageType, "");

    CHP_SP.activeE1page.omwObjectName = objectToTranslate.omwObjectName;
    objectToTranslate = null;

    var langCodeListContainer = doc.getElementById(CHP_SP.ID_LANG_CODE_CONTAINER);
    langCodeListContainer.style.display = "table-row";

    //disable save-as, reserve/unreserve , req publish, delete , notes icons, Page Type
    if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID) {
        CHP_SP.togglePageTypeBoxDisablement(false);
    }
    else {
        CHP_SP.togglePageTypeBoxDisablement(true);
    }
    CHP_SP.disableIcon(CHP_SP.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], CHP_SP.constants.UDO_SAVE_AS); // disable saveAs Icon
    CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], CHP_SP.constants.UDO_RESERVE);
    CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], CHP_SP.constants.UDO_REQUEST_PUBLISH);
    CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
    CHP_SP.disableIcon(CHP_SP.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], CHP_SP.constants.UDO_NOTES);
}

CHP_SP.isValidURL = function (pageurl) {
    var success = false;
    var urlregex = new RegExp("^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z0-9]{2,5}(:[0-9]{1,5})?(\/.*)?$"); //("^?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$");         
    success = urlregex.test(pageurl);
    if (!success) {
        if (pageurl.indexOf("/jde/") == 0)
            success = true;
    }
    return success;
}

CHP_SP.isUploadFileValid = function (fileName) {
    return /(\w+.zip)/.test(fileName);
}

CHP_SP.validateInput = function (cmd) {
    var errorExists = false;
    var doc = document;
    // set all of the activeE1page fields based on what is entered in the creation pane

    CHP_SP.activeE1page.id = CHP_SP.getCurrentlySelectedE1Page().id;
    CHP_SP.activeE1page.name = doc.getElementById(CHP_SP.ID_OBJECT_NAME).value; //CHP_SP.getCurrentlySelectedE1Page().name;
    CHP_SP.activeE1page.desc = CHP_SP.HTMLDecoder(doc.getElementById(CHP_SP.ID_DESCRIPTION).value);
    CHP_SP.activeE1page.tabName = CHP_SP.HTMLDecoder(doc.getElementById(CHP_SP.ID_PAGE_TITLE).value);
    CHP_SP.activeE1page.prodCode = doc.getElementById(CHP_SP.ID_PROD_CODE).value;
    var pageURLInput = document.getElementById(CHP_SP.ID_PAGE_URL);
    var langCodeContainer = document.getElementById(CHP_SP.ID_LANG_CODE_CONTAINER);
    if (langCodeContainer.style.display != "none")
        CHP_SP.activeE1page.langCode = doc.getElementById(CHP_SP.ID_LANG_CODE).value;

    var pageType = CHP_SP.getPageTypeForActiveE1page();

    CHP_SP.activeE1page.pageType = pageType;
    if (CHP_SP.PAGE_TYPE_URL == pageType) {
        var pageURL = pageURLInput.value;
        if (!CHP_SP.isValidURL(pageURL)) {
            CHP_SP.showErrorWindow(doc.getElementById(CHP_SP.ID_PAGE_URL), CHP_SP.constants.CHP_PAGEURL_INVALID);
            GUTIL.setFocus(pageURLInput, true);
            errorExists = true;
        }
        CHP_SP.activeE1page.pageURL = pageURL;
    }
    else if (CHP_SP.PAGE_TYPE_HTMLCONTENT == pageType) {
        var fileName = doc.getElementById(CHP_SP.ID_FILEUPLOAD).value;

        //case 1: filename null or empty
        //case 1 a: create new e1page (throw error)
        //case 1 b: save existing e1page (use old content - dont update)
        //case 2: file name not empty
        //case 2 a: create new e1page (success)
        //case 2 b: save existing e1page (update content)

        if (CHP_SP.activeE1page.id == CHP_SP.BLANK_E1PAGE_ID)    //new page
        {
            if (fileName && fileName.length > 0 && CHP_SP.isUploadFileValid(fileName)) {
                CHP_SP.activeE1page.fileName = fileName;    //success
            }
            else {
                CHP_SP.showUploadAlert(false);
            }
        }
        else    //update page  or 'save as'
        {
            if (fileName && fileName.length > 0) {
                if (!CHP_SP.isUploadFileValid(fileName)) {
                    CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
                    CHP_SP.showErrorWindow(doc.getElementById(CHP_SP.ID_FILEUPLOAD), CHP_SP.constants.CHP_UPLOADCONTENT_INVALID);
                    errorExists = true;
                }
            }
            CHP_SP.activeE1page.fileName = fileName;
        }
    }
    else {
        CHP_SP.showErrorWindow(doc.getElementsByName(CHP_SP.NAME_PAGE_TYPE)[0], CHP_SP.constants.CHP_PAGETYPE_INVALID);
        errorExists = true;
    }
    return errorExists;
}

CHP_SP.showReservedBy = function () {
    var reservedByDiv = document.getElementById(CHP_SP.ID_RESERVED_BY);
    //TODO - reserve E1page is reRequestPublish - isRequestPublished is not getting updated Correctly,need to check.
    if (!CHP_SP.activeE1page.isPrivate && !(CHP_SP.activeE1page.isRequestPublished) && !(CHP_SP.activeE1page.isRework)) {
        if (CHP_SP.activeE1page.reservedBy == undefined || CHP_SP.activeE1page.reservedBy == "") {
            reservedByDiv.style.display = "none";
            if (CHP_SP.activeE1page.hasToken) {
                var tokenHeldByProject = CHP_SP.constants.UDO_TOKEN_BY.format(CHP_SP.activeE1page.tokenProjectName);
                reservedByDiv.style.display = "block";
                // For accessibility users add the ReservedBy information. 
                // If the currently selected E1page is the first shared E1page then add "Shared E1Page" to the ReservedBy information.
                if ((CHP_SP.ACCESSIBILITY) && (CHP_SP.activeE1page.id == CHP_SP.publicItems[0].id)) {
                    reservedByDiv.innerHTML = "&nbsp;" + CHP_SP.constants.UDO_SHARED + "&nbsp;" + tokenHeldByProject;
                }
                else {
                    reservedByDiv.innerHTML = "&nbsp;" + tokenHeldByProject;
                }
            }
            return;
        }
        else {
            var reservedBy = CHP_SP.constants.CHP_RESERVED_BY.format(CHP_SP.activeE1page.reservedBy);
            reservedByDiv.style.display = "none";
            // show reserved by message for shared E1page Only
            if (!CHP_SP.activeE1page.isPrivate) {
                reservedByDiv.style.display = "block";
                reservedByDiv.innerHTML = "&nbsp;" + reservedBy;
            }
        }
    }
    else {
        reservedByDiv.style.display = "none";
    }
}

CHP_SP.showErrorWindow = function (errorElem, errorDescription) {
    // If IFYE is disable in jas.ini or if Accessibility is turned on , display the message in the message area
    if (CHP_SP.ACCESSIBILITY || !window.inyfeHandler) {
        CHP_SP.showMessageDiv(true, errorDescription, true, errorElem);
        return;
    }

    var winTop = 8 + getAbsoluteTopPos(errorElem, true) - getAbsoluteTopPos(CHP_SP.E1PmanageSidepanel, true);
    var winLeft = 8 + getAbsoluteLeftPos(errorElem, true) - getAbsoluteLeftPos(CHP_SP.E1PmanageSidepanel, true);
    //var winLeft = getAbsoluteLeftPos(errorElem, true);
    var winWidth = CHP_SP.DIALOG_WIDTH;
    var winHeight = CHP_SP.DIALOG_HEIGHT;
    // make the error box slightly bigger for longer error messages
    if (errorDescription.length > 70)
        winHeight = CHP_SP.BIG_DIALOG_HEIGHT;

    var sb = new PSStringBuffer();
    sb.append("<div valign=top style='margin:3;width:100%;height:100%;overflow: auto'>").append(errorDescription).append("</div>");
    var errContent = sb.toString();

    var eventListener = {};
    eventListener.onClose = function (e) {
        CHP_SP.iyfeWin.destory();
        CHP_SP.iyfeWin = null;
    }
    CHP_SP.iyfeWin = createPopup("AQ_IYFE", SHOW_POPUP_SIDEPANEL, true, false, true, true, CHP_SP.constants.CHP_TITLE_IYFE_DIALOG, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, errorElem, errContent, eventListener);
    //CHP_SP.iyfeWin = createPopup("AQ_IYFE", SHOW_POPUP, true, false, true, true, CHP_SP.constants.CHP_TITLE_IYFE_DIALOG, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, errorElem, errContent, eventListener);
}

CHP_SP.onErrorBoxClose = function () {
    if (!CHP_SP.iyfeWin)
        return;
    CHP_SP.iyfeWin.destory();
    CHP_SP.iyfeWin = null;
}
// private method to destory error window 
CHP_SP.destroyErrorWindow = function () {
    if (CHP_SP.iyfeWin)
        CHP_SP.iyfeWin.onClose();
}

// show or hide the message div
CHP_SP.showMessageDiv = function (showMessage, messageText, isError, element) {
    var doc = document;
    var messageDiv = doc.getElementById(CHP_SP.ID_MESSAGE_DIV);
    if (showMessage) {
        if (isError)
            messageDiv.style.borderColor = "Red";
        else
            messageDiv.style.borderColor = "Blue";
        messageDiv.style.display = "block";
        messageDiv.innerHTML = messageText;
        // If accessibilty is turned on and the element for which error/info has to be set is passed
        // then JAWS will read the specific error/info message 
        if (CHP_SP.ACCESSIBILITY && element != null) {
            element.setAttribute("aria-invalid", "true");
            messageDiv.setAttribute("role", "alert");
        }
        else if (CHP_SP.ACCESSIBILITY && element == null) {
            messageDiv.setAttribute("role", "alert");
        }
    }
    else {
        messageDiv.style.display = "none";
        // If accessibilty is turned on then set the "aria-invalid" attribute to false
        // for all the elements for which error could have been set.
        if (CHP_SP.ACCESSIBILITY) {
            if (doc.getElementById(CHP_SP.ID_OBJECT_NAME) != null) {
                doc.getElementById(CHP_SP.ID_OBJECT_NAME).setAttribute("aria-invalid", "false");
            }
            if (doc.getElementById(CHP_SP.ID_DESCRIPTION) != null) {
                doc.getElementById(CHP_SP.ID_DESCRIPTION).setAttribute("aria-invalid", "false");
            }
            if (doc.getElementById(CHP_SP.ID_LANG_CODE) != null) {
                doc.getElementById(CHP_SP.ID_LANG_CODE).setAttribute("aria-invalid", "false");
            }
            if (doc.getElementById(CHP_SP.ID_PROD_CODE) != null) {
                doc.getElementById(CHP_SP.ID_PROD_CODE).setAttribute("aria-invalid", "false");
            }
            if (doc.getElementById(CHP_SP.ID_PAGE_TITLE) != null) {
                doc.getElementById(CHP_SP.ID_PAGE_TITLE).setAttribute("aria-invalid", "false");
            }
        }
    }
}

// Private method to do a high interactive post to the server
CHP_SP.send = function (cmd) {
    JDEDTAFactory.getInstance("").post(cmd);
}

CHP_SP.goFetchNotes = function (e) {
    // calling RunNewApp() to open the MediaObject viewer to view UDO Notes, this scenario FormOwner is not available
    window.RunNewApp('lcmLoadMo', '0', "", 'APP', "", "", "1", "", "", "", "", "", "", "", "", "", "", "", CHP_SP.activeE1page.omwObjectName);
}

CHP_SP.parameterizeActiveE1page = function (cmd) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", cmd);
    e1URL.setParameter("id", CHP_SP.activeE1page.id);
    e1URL.setParameter("tabName", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.tabName.Trim()));
    e1URL.setParameter("prodCode", CHP_SP.activeE1page.prodCode);
    if (CHP_SP.activeE1page.omwObjectName != null) {
        e1URL.setParameter("omwObjectName", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.omwObjectName.Trim()));
    }
    e1URL.setParameter("desc", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.desc));
    e1URL.setParameter("fileName", CHP_SP.activeE1page.fileName);
    e1URL.setParameter("isCheckedOut", CHP_SP.activeE1page.isCheckedOut);
    e1URL.setParameter("hasToken", CHP_SP.activeE1page.hasToken);
    e1URL.setParameter("tokenProjectName", CHP_SP.activeE1page.tokenProjectName);
    if (cmd != CHP_SP.CMD_TRANSLATE && cmd != CHP_SP.CMD_PREVIEW && cmd != CHP_SP.CMD_DEL_TRANSLATE && cmd != CHP_SP.CMD_DOWNLOAD) {
        e1URL.setParameter("objectType", CHP_SP.objectType);
    }
    if (!CHP_SP.isImage) {
        e1URL.setParameter("name", CHP_SP.HTMLDecoder(CHP_SP.activeE1page.name.Trim()));
        e1URL.setParameter("langCode", CHP_SP.activeE1page.langCode);
        e1URL.setParameter("lineType", CHP_SP.activeE1page.lineType);
        e1URL.setParameter("pageType", CHP_SP.getPageTypeForActiveE1page());
        e1URL.setParameter("pageURL", CHP_SP.activeE1page.pageURL);
        e1URL.setParameter("appId", "");
        e1URL.setParameter("formId", "");
        e1URL.setParameter("taskId", "");
        e1URL.setParameter("version", CHP_SP.activeE1page.version);
        e1URL.setParameter("reportName", "");
    }
    return e1URL;
}

CHP_SP.setAltForCloseInNamePrompt = function () {
    var closeImg = document.getElementById("Close_Prompt");
    closeImg.setAttribute("alt", CHP_SP.constants.CHP_ALT_CLOSE_CHP_NAME);
}

CHP_SP.updateClassName = function (element, updateClass, addClass) {
    var className = element.className;
    if (addClass) {
        element.className = className + updateClass;
    }
    else {
        element.className = className.replace(updateClass, '');
    }
}

CHP_SP.onMouseOverButton = function (event) {
    event = event || window.event;
    var target = event.srcElement || event.target;
    CHP_SP.updateClassName(target, ' RCUXbuttonMouseOver', true);
}

CHP_SP.onMouseOutButton = function (event) {
    event = event || window.event;
    var target = event.srcElement || event.target;
    CHP_SP.updateClassName(target, ' RCUXbuttonMouseOver', false);
    CHP_SP.updateClassName(target, ' RCUXbuttonMouseDown', false);
}

CHP_SP.onMouseDownButton = function (event) {
    event = event || window.event;
    var target = event.srcElement || event.target;
    CHP_SP.updateClassName(target, ' RCUXbuttonMouseOver', false);
    CHP_SP.updateClassName(target, ' RCUXbuttonMouseDown', true);
}

CHP_SP.onMouseUpButton = function (event) {
    event = event || window.event;
    var target = event.srcElement || event.target;
    CHP_SP.updateClassName(target, ' RCUXbuttonMouseDown', false);
}

/*
Adds single item to the end of this array
*/
Array.prototype.add = function (item) {
    this[this.length] = item;
}

// private function decodes &#n; HTML chars
CHP_SP.HTMLDecoder = function (encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    if (div.firstChild != null)
        return div.firstChild.nodeValue;
    else
        return "";
}

//begin image APIs
CHP_SP.fileButtonClicked = function () {
    if (CHP_SP.isImage)//this is for Image only
    {
        //clear the description 
        var description = document.getElementById(CHP_SP.ID_DESCRIPTION);
        if (description) {
            description.value = "";
        }
    }
}

CHP_SP.renderIconViewer = function (e, lowerLimit, upperLimit) {
    CHP_SP.udoIconClicked = true;
    //need to reset the selected item name
    if (e != null) {
        document.getElementById(CHP_SP.BLANK_E1PAGE_ID).removeAttribute("omwObjectName");
        document.getElementById(CHP_SP.BLANK_E1PAGE_ID).removeAttribute("user");
        lowerLimit = null;
        upperLimit = null;
    }
    CHP_SP.iconList = null;

    //fetch icons
    CHP_SP.goFetchUdoIcons(lowerLimit, upperLimit);
    var nextPageExists = false;
    if (CHP_SP.iconList != null && CHP_SP.iconList.length > 0) {
        nextPageExists = (CHP_SP.iconList[CHP_SP.iconList.length - 1].omwObjectName == "NextPageExists");
        if (nextPageExists) {
            //a bogus item is added at the end of the list when records are fetched to server as the nextPageId, remove it 
            CHP_SP.iconList.pop();
            CHP_SP.iconSecEndId = CHP_SP.iconList[CHP_SP.iconList.length - 1].omwObjectName;
        }
        if (upperLimit == null)//next page, records are fetched based on ascending omwObjectName
        {
            CHP_SP.iconSecStartId = CHP_SP.iconList[0].omwObjectName;
            CHP_SP.iconSecEndId = CHP_SP.iconList[CHP_SP.iconList.length - 1].omwObjectName;
        }
        else//previous page, records are fetched based on descending omwObjectName
        {
            CHP_SP.iconSecStartId = CHP_SP.iconList[CHP_SP.iconList.length - 1].omwObjectName;
            CHP_SP.iconSecEndId = CHP_SP.iconList[0].omwObjectName;
        }

        //seperate them in sublists        
        CHP_SP.separateE1pageslistNames(CHP_SP.iconList);
        //build content in the popup window
        var htmlContent = new PSStringBuffer();
        //        htmlContent.append("<table style='background-color:white'>");
        htmlContent.append("<table style='border-spacing:0px' class='mainnav'>");
        htmlContent.append("<tbody>");

        //images
        htmlContent.append("<tr>");
        htmlContent.append("<td>");

        //calculate div height
        var totalImageRows = 0;
        var totalGroupNumber = 0;
        if (CHP_SP.privateItems.length > 0) {
            totalImageRows += Math.ceil(CHP_SP.privateItems.length / 10);
            totalGroupNumber++;
        }
        if (CHP_SP.pendingApprovalItems.length > 0) {
            totalImageRows += Math.ceil(CHP_SP.pendingApprovalItems.length / 10);
            totalGroupNumber++;
        }
        if (CHP_SP.reworkItems.length > 0) {
            totalImageRows += Math.ceil(CHP_SP.reworkItems.length / 10);
            totalGroupNumber++;
        }
        if (CHP_SP.reservedItems.length > 0) {
            totalImageRows += Math.ceil(CHP_SP.reservedItems.length / 10);
            totalGroupNumber++;
        }
        if (CHP_SP.publicItems.length > 0) {
            totalImageRows += Math.ceil(CHP_SP.publicItems.length / 10);
            totalGroupNumber++;
        }
        var imageDivHeight = totalGroupNumber * 22 + totalImageRows * 40;
        var renderVerticalScroll = false;
        if (imageDivHeight > 350) {
            renderVerticalScroll = true;
        }
        imageDivHeight = Math.min(imageDivHeight, 333);
        //24 is the popup window title bar height, 28 is the button row height
        var popupHeight = 24 + imageDivHeight + 28;
        var popupWidth = 385;
        if (renderVerticalScroll) {
            popupWidth = popupWidth + Agent.getScrollBarWidth();
        }

        htmlContent.append("<div style='width:");
        htmlContent.append(popupWidth - 2);
        htmlContent.append("px;height:");
        htmlContent.append(imageDivHeight);
        htmlContent.append("px;overflow-y:");
        if (renderVerticalScroll) {
            htmlContent.append("auto");
        }
        else {
            htmlContent.append("hidden");
        }
        htmlContent.append(";overflow-x:hidden'>");
        htmlContent.append("<table style='border-spacing:0px;width:100%'>");
        htmlContent.append("<tbody>");
        var isReversed = upperLimit != null;

        //build group tables 
        if (CHP_SP.privateItems.length > 0) {
            htmlContent.append(CHP_SP.buildIconGroupTable(CHP_SP.privateItems, 0, isReversed));
        }
        if (CHP_SP.pendingApprovalItems.length > 0) {
            htmlContent.append(CHP_SP.buildIconGroupTable(CHP_SP.pendingApprovalItems, 1, isReversed));
        }
        if (CHP_SP.reworkItems.length > 0) {
            htmlContent.append(CHP_SP.buildIconGroupTable(CHP_SP.reworkItems, 2, isReversed));
        }
        if (CHP_SP.reservedItems.length > 0) {
            htmlContent.append(CHP_SP.buildIconGroupTable(CHP_SP.reservedItems, 3, isReversed));
        }
        if (CHP_SP.publicItems.length > 0) {
            htmlContent.append(CHP_SP.buildIconGroupTable(CHP_SP.publicItems, 4, isReversed));
        }

        htmlContent.append("</tbody>");
        htmlContent.append("</table>");
        htmlContent.append("</td>");
        htmlContent.append("</tr>");

        //navigation buttons
        htmlContent.append("<tr>");
        htmlContent.append("<td>");
        htmlContent.append("<table style='border-spacing:0px'>");
        htmlContent.append("<tbody>");
        htmlContent.append("<tr>");
        htmlContent.append("<td style='width:27px;height:20px;text-align:right;vertical-align:top'>");
        htmlContent.append("<a style='TEXT-DECORATION: none;' title='");
        htmlContent.append(CHP_SP.constants.JH_AUTOSUGGEST_PREVIOUS_PAGE);
        htmlContent.append("'>");
        htmlContent.append("<img style='CURSOR: pointer' id='iconPrev' border='0' alt='");
        htmlContent.append(CHP_SP.constants.JH_AUTOSUGGEST_PREVIOUS_PAGE);
        htmlContent.append("' src='");
        if (CHP_SP.iconCurSecIndex == 0) {
            htmlContent.append(window["E1RES_img_alta_grid_prev_dis_png"]).append("'>");
        }
        else {
            htmlContent.append(window["E1RES_img_alta_grid_prev_ena_png"]).append("'");
            htmlContent.append(" onclick='CHP_SP.fetchAnotherPage(false);'");
            htmlContent.append(">");
        }
        htmlContent.append("</a>");
        htmlContent.append("</td>");
        htmlContent.append("<td width='371px'>");
        htmlContent.append("&nbsp;");
        htmlContent.append("</td>");
        htmlContent.append("<td style='width:27px;text-align:left;vertical-align:top'>");
        htmlContent.append("<a style='TEXT-DECORATION: none;' title='");
        htmlContent.append(CHP_SP.constants.JH_AUTOSUGGEST_Next_PAGE);
        htmlContent.append("'>");
        htmlContent.append("<img style='CURSOR: pointer' id='iconNext' border='0' alt='");
        htmlContent.append(CHP_SP.constants.JH_AUTOSUGGEST_Next_PAGE);
        htmlContent.append("' src='");
        if (!nextPageExists) {
            htmlContent.append(window["E1RES_img_alta_grid_next_dis_png"]).append("'>");
        }
        else {
            htmlContent.append(window["E1RES_img_alta_grid_next_ena_png"]).append("'");
            htmlContent.append(" onclick='CHP_SP.fetchAnotherPage(true);'");
            htmlContent.append(">");
        }
        htmlContent.append("</a>");
        htmlContent.append("</td>");
        htmlContent.append("</tr>");
        htmlContent.append("</tbody>");
        htmlContent.append("</table>");
        htmlContent.append("</div>");
        htmlContent.append("</td>");
        htmlContent.append("</tr>");
        htmlContent.append("</tbody>");
        htmlContent.append("</table>");

        createPopup("UdoIcons", UDO, true, false, true, false, CHP_SP.constants.IMG_LABEL_AVAILABLE, null, null, null, null, null, 80, 80, popupWidth, popupHeight, null, htmlContent.toString(), null, CHP_SP.namespace);
    }
    else//no udo icons available, prompt the user and switch to add 
    {
        document.getElementById('newImage').click();
        alert("There are no existing images");
    }

    /*
    var e1AppFrameContainer = document.getElementById("e1AppFrameContainer");
    
    if(window.blockPane == null) // create new
    {
    window.blockPane = new UIBlockingPane();
    window.blockPane.blockUI(document);
    }
    else // reuse the existing one
    {
    window.blockPane._showBlockingDiv(document);
    }
    var uiBlockingDiv = document.getElementById('UIBlockingDiv');
    uiBlockingDiv.style.height = e1AppFrameContainer.clientHeight + "px";
    uiBlockingDiv.style.width = e1AppFrameContainer.clientWidth + "px";
    uiBlockingDiv.style.top = 0 + 'px';
    
    var viewer = document.createElement('div');
    viewer.id = 'E1IconViewer';
    viewer.style.height = window.innerHeight - 200 + "px";
    viewer.style.width = window.innerWidth - 300 + "px";
    viewer.style.position = "relative";
    viewer.style.top = 100 + 'px';
    viewer.style.left = 150 + 'px';
    e1AppFrameContainer.appendChild(viewer);
    */
}

CHP_SP.buildIconGroupTable = function (list, groupId, isReversed) {
    var e1UrlCreator;
    var imagePath;
    var icon;
    var rows = Math.ceil(list.length / 10);
    var groupContent = new PSStringBuffer();
    var iconIndex = 0;
    var label;
    switch (groupId) {
        case 0:
            label = CHP_SP.constants.UDO_PERSONAL;
            break;
        case 1:
            label = CHP_SP.constants.UDO_PENDING_APPROVAL;
            break;
        case 2:
            label = CHP_SP.constants.UDO_REWORK;
            break;
        case 3:
            label = CHP_SP.constants.UDO_RESERVED;
            break;
        case 4:
            label = CHP_SP.constants.UDO_SHARED;
            break;
    }

    groupContent.append("<tr style='width:100%'>");
    groupContent.append("<td>");
    groupContent.append("<table style='width:100%;border-spacing:0px'>");
    groupContent.append("<tbody>");
    //group heading row
    //    groupContent.append("<tr class='RIPalletteRowHeading' style='width:100%;border-style:solid none;border-width:1px;border-color:black'>");
    groupContent.append("<tr class='iconViewer' style='width:100%'>");
    groupContent.append("<td class='RIGroupHeading'>");
    groupContent.append(label);
    groupContent.append("</td>");
    groupContent.append("</tr>");
    //icon table row
    groupContent.append("<tr>");
    groupContent.append("<td>");
    groupContent.append("<table style='border-spacing:0px'>");
    groupContent.append("<tbody>");

    for (var row = 0; row < rows; row++) {
        groupContent.append("<tr>");
        for (var i = 0; i < 10; i++) {
            if (row * 10 + i == list.length) {
                break;
            }
            groupContent.append("<td style='width:36px;height:36px;text-align:center;vertical-align:middle'>");
            iconIndex = row * 10 + i;
            if (isReversed)//previous page, the recrods are returned from DB descending, retrieve them in reverse order
            {
                iconIndex = CHP_SP.iconList.length - 1 - iconIndex;
            }
            icon = list[iconIndex];

            e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
            var fullName = icon.tabName;
            if (icon.isPrivate && !icon.isPersonal) {
                fullName = "res" + fullName;
            }
            imagePath = "share/images/udoicons/" + fullName + "?" + new Date().getTime();
            e1UrlCreator.setURI(imagePath);

            groupContent.append("<img id='");
            groupContent.append("UdoIcons" + groupId + "." + iconIndex);
            groupContent.append("' omwObjectName='");
            groupContent.append(icon.omwObjectName);
            groupContent.append("' user='");
            groupContent.append(icon.user);
            groupContent.append("' src='");
            groupContent.append(e1UrlCreator.toString());
            groupContent.append("' style='max-height:36px;max-width:36px' title='");
            groupContent.append(icon.desc.replace(/'/g, "\\u0027"));
            groupContent.append("'");
            groupContent.append(" fileName='");
            groupContent.append(icon.tabName);
            groupContent.append("' onclick='CHP_SP.pickIcon(");
            groupContent.append("this");
            groupContent.append(");'");
            groupContent.append(">");
            groupContent.append("</td>");
        }
        groupContent.append("</tr>");
    }
    groupContent.append("</tbody>");
    groupContent.append("</table>");
    groupContent.append("</td>");
    groupContent.append("</tr>"); //end of icon table row
    groupContent.append("</tbody>");
    groupContent.append("</table>");
    groupContent.append("</td>");
    groupContent.append("</tr>"); //end of group table

    return groupContent.toString();
}

CHP_SP.pickIcon = function (selectedIcon) {
    //fileName should be readonly
    var control = document.getElementById(CHP_SP.BLANK_E1PAGE_ID);
    control.value = selectedIcon.getAttribute("fileName");
    control.setAttribute("omwObjectName", selectedIcon.getAttribute("omwObjectName"));
    control.setAttribute("user", selectedIcon.getAttribute("user"));
    //    control.readOnly = true;

    //user can update description
    control = document.getElementById(CHP_SP.ID_DESCRIPTION);
    control.value = selectedIcon.title;
    window.openedPopup.onClose();

    //set the status for the picked image
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_GETBYNAME);
    e1URL.setParameter("name", selectedIcon.getAttribute("omwObjectName"));
    e1URL.setParameter("user", selectedIcon.getAttribute("user"));
    e1URL.setParameter("langCode", "");
    e1URL.setParameter("getImage", true);
    e1URL.setParameter("buster", new Date().getTime());
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("GET", null, e1URL.toString(), CHP_SP.updateActiveIcon, true);
}

CHP_SP.fetchAnotherPage = function (fetchNextPage) {
    window.openedPopup.onClose();
    var lowerLimit = null;
    var upperLimit = null;
    if (fetchNextPage) {
        CHP_SP.iconCurSecIndex++;
        lowerLimit = CHP_SP.iconSecEndId;
    }
    else if (CHP_SP.iconCurSecIndex >= 1)//fetch one page of data from the DB descendingly, the returned data will be in reversed order
    {
        upperLimit = CHP_SP.iconSecStartId;
        CHP_SP.iconCurSecIndex--;
    }
    CHP_SP.renderIconViewer(null, lowerLimit, upperLimit);
}

CHP_SP.goFetchUdoIcons = function (lowerLimit, upperLimit) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_GETLIST);
    e1URL.setParameter("getImage", true);
    if (lowerLimit != null) {
        e1URL.setParameter("lowerLimit", lowerLimit);
    }
    if (upperLimit != null) {
        e1URL.setParameter("upperLimit", upperLimit);
    }
    e1URL.setParameter("buster", new Date().getTime());
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("GET", null, e1URL.toString(), CHP_SP.updateUdoIconList, false);
}

CHP_SP.updateUdoIconList = function (containerId, e1PageModels) {
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    e1PageModels = e1PageModels.replace(/(\r\n|\n|\r)/gm, "");
    var data = eval(e1PageModels);
    CHP_SP.iconList = data[0].udoIcons;
}

CHP_SP.toggleUdoImage = function (e) {
    CHP_SP.showMessageDiv(false);
    if (e.target.id == 'newImage') {
        CHP_SP.clickNewIconRadioButton();
    }
    else {
        document.getElementById('newImage').checked = false;
        document.getElementById('udoIconVA').style.display = 'inline';
        document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value = "";
        document.getElementById(CHP_SP.BLANK_E1PAGE_ID).readOnly = false;
        document.getElementById(CHP_SP.BLANK_E1PAGE_ID).addEventListener("change", CHP_SP.getImageByFileName);
        document.getElementById(CHP_SP.ID_DESCRIPTION).value = "";
        document.getElementById(CHP_SP.ID_DESCRIPTION).readOnly = true;
        CHP_SP.clearUploadFileName();
        //        document.getElementById(CHP_SP.ID_PROD_CODE).value = "";
        //        document.getElementById(CHP_SP.ID_PROD_CODE).readOnly = true;
        CHP_SP.clearOmwName();
        //before an icon is selected, disable the save button, need to do it after clearOmwName()
        CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
    }
}

CHP_SP.updateActiveIcon = function (container, retStr) {
    CHP_SP.udoIconClicked = false;
    retStr = retStr.replace(/(\r\n|\n|\r)/gm, "");
    var data = eval(retStr);
    if (data == null)//getImageByFileName will return null if the file doesn't exist
    {
        CHP_SP.imageNotFound();
    }
    else {
        CHP_SP.activeE1page = data;
        var control = document.getElementById(CHP_SP.BLANK_E1PAGE_ID);
        control.setAttribute("omwObjectName", data.omwObjectName);
        if (data.user != "") {
            control.setAttribute("user", data.user);
        }
        control.value = CHP_SP.HTMLDecoder(data.fileName);
        //    control.readOnly = true;
        /*
        control = document.getElementById(CHP_SP.ID_PROD_CODE);
        control.value = data.prodCode;
        control.readOnly = true;
        control = document.getElementById(CHP_SP.ID_PROD_CODE_DESC);
        control.value = data.prodCodeDesc;
        control.readOnly = true;
        */
        control = document.getElementById(CHP_SP.ID_DESCRIPTION);
        control.value = CHP_SP.HTMLDecoder(data.desc);
        control.readOnly = true;
        control = document.getElementById('selectedUDOIcon');
        control.style.display = 'inline';
        e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
        var fullName = CHP_SP.HTMLDecoder(data.tabName);
        if (data.isPrivate && !data.isPersonal) {
            fullName = "res" + fullName;
        }
        imagePath = "share/images/udoicons/" + fullName + "?" + new Date().getTime();
        e1UrlCreator.setURI(imagePath);
        control.src = e1UrlCreator.toString();
        if (CHP_SP.MODE > 0) {
            control = document.getElementById('newImage');
            control.checked = false;
            control.disabled = true;
            control = document.getElementById('existingImage');
            control.checked = true;
            control.disabled = true;
            /* Maybe I don't need to do it here, it should call updateControlEnablement which shall take care of it, hopefully
            if(CHP_SP.MODE == 2)
            {
            document.getElementById(CHP_SP.ID_FILEUPLOAD).disabled = true;
            }
            */
        }
        if (document.getElementById('newImage').checked)//user just uploaded a new image
        {
            //if user just uploaded an image, switch to existing image
            document.getElementById('newImage').checked = false;
            document.getElementById('existingImage').checked = true;
            document.getElementById('udoIconVA').style.display = 'inline';
        }
        WebObjectUtil.clearProcInd(CHP_SP.namespace);
        CHP_SP.updateControlEnablement();
        if (data.user == "*PUBLIC" && CHP_SP.MODE != CHP_SP.PREVIEWMODE) {
            CHP_SP.showReservedBy();
        }
        else {
            document.getElementById(CHP_SP.ID_RESERVED_BY).style.display = 'none';
        }
    }
}

//clear the omwObjectName from the input field for the subsequent action
CHP_SP.clearOmwName = function () {
    //remove omwObjectName from the attribute
    var input = top.document.getElementById(CHP_SP.BLANK_E1PAGE_ID);
    if (input.getAttribute("omwObjectName") != null) {
        input.removeAttribute("omwObjectName");
        input.removeAttribute("user");
    }
    //reset activeE1Page and update the icons
    CHP_SP.resetActiveE1Page();
    CHP_SP.updateControlEnablement();
}

//this function could be triggered when the user switched back to the add new icon, OR after the user deletes an existing icon
CHP_SP.clickNewIconRadioButton = function () {
    document.getElementById('existingImage').checked = false;
    document.getElementById('udoIconVA').style.display = 'none';
    document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value = CHP_SP.constants.UDO_CREATE;
    document.getElementById(CHP_SP.BLANK_E1PAGE_ID).readOnly = true;
    document.getElementById(CHP_SP.BLANK_E1PAGE_ID).removeEventListener('change', CHP_SP.getImageByFileName);
    document.getElementById(CHP_SP.ID_DESCRIPTION).value = "";
    document.getElementById(CHP_SP.ID_DESCRIPTION).readOnly = false;
    document.getElementById('selectedUDOIcon').style.display = 'none';
    CHP_SP.clearUploadFileName();
    //    document.getElementById(CHP_SP.ID_PROD_CODE).value = "";
    //    document.getElementById(CHP_SP.ID_PROD_CODE).readOnly = false;
    CHP_SP.clearOmwName();
}

//this is a hack, to clear the upload file for ID_FILEUPLOAD, somehow, it works at least in Chrome
CHP_SP.clearUploadFileName = function () {
    document.getElementById(CHP_SP.ID_FILEUPLOAD).outerHTML = document.getElementById(CHP_SP.ID_FILEUPLOAD).outerHTML;
}

CHP_SP.refetchImage = function (state) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(CHP_SP.MANAGER_SERVICE);
    e1URL.setParameter("cmd", CHP_SP.CMD_GETLASTSAVEDIMAGE);
    var input = top.document.getElementById(CHP_SP.BLANK_E1PAGE_ID);
    e1URL.setParameter("omwObjectName", CHP_SP.activeE1page.omwObjectName);
    if (state == 1) {
        e1URL.setParameter("reserve", true);
    }
    else if (state == 2) {
        e1URL.setParameter("getPublic", true);
    }
    e1URL.setParameter("buster", new Date().getTime());
    WebObjectUtil.setProcInd(CHP_SP.namespace);
    WebObjectUtil.sendXMLReq("GET", null, e1URL.toString(), CHP_SP.updateActiveIcon, true);
}

CHP_SP.getImageByFileName = function () {
    if (!CHP_SP.udoIconClicked) {
        var input = document.getElementById(CHP_SP.BLANK_E1PAGE_ID).value;
        if (input != "") {
            var index = input.indexOf(".");
            if (index > 0) {
                var fileName = input.substring(0, index);
                var fileExt = input.substring(index + 1);
                var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
                e1URL.setURI(CHP_SP.MANAGER_SERVICE);
                e1URL.setParameter("cmd", CHP_SP.CMD_GETIMAGEBYNAME);
                e1URL.setParameter("fileName", fileName);
                e1URL.setParameter("fileExt", fileExt);
                e1URL.setParameter("designMode", true);
                e1URL.setParameter("buster", new Date().getTime());
                WebObjectUtil.setProcInd(CHP_SP.namespace);
                WebObjectUtil.sendXMLReq("GET", null, e1URL.toString(), CHP_SP.updateActiveIcon, true);
            }
            else {
                CHP_SP.imageNotFound();
            }
        }
    }
}

CHP_SP.imageNotFound = function () {
    //disable all ulcm icons
    CHP_SP.disableIcon(CHP_SP.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
    CHP_SP.disableIcon(CHP_SP.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"]);
    CHP_SP.disableIcon(CHP_SP.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"]);
    CHP_SP.disableIcon(CHP_SP.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
    CHP_SP.disableIcon(CHP_SP.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"]);
    //clear processing indicator
    WebObjectUtil.clearProcInd(CHP_SP.namespace);
    //hide selected udo icon
    var control = document.getElementById('selectedUDOIcon');
    control.style.display = 'none';
    //clear out the description field
    document.getElementById(CHP_SP.ID_DESCRIPTION).value = "";
}