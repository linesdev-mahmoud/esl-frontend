/*
** Copyright (c) Oracle Corporation 2006. All Rights Reserved.
**
*/
/**
* Agent capabilities
* @author Nan Li
*/
function Agent() {
    this.Init(Agent.UNKNOWN_PLATFORM, Agent.guessOS(), Agent.guessVersion());
}

// make Agent a subclass of RichObject
ClientObject.createSubclass(Agent);

/**
* Agent Platform constants
*/
Agent.IE_PLATFORM = "ie";
Agent.GECKO_PLATFORM = "gecko";
Agent.OPERA_PLATFORM = "opera";
Agent.UNKNOWN_PLATFORM = "unknown";

/**
* Agent OS constants
*/
Agent.WINDOWS_OS = "Windows";
Agent.SOLARIS_OS = "Solaris";
Agent.MAC_OS = "Mac";
Agent.UNKNOWN_OS = "Unknown";

Agent.gotScrollBarWidth = false;
Agent.SB_SIZE = 15;
Agent.getScrollBarWidth = function () {
    if (!Agent.gotScrollBarWidth);
    {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement("div");
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild(inner);

        document.body.appendChild(outer);
        var w1 = inner.offsetWidth;
        if (w1 == 0)
            w1 = 200;
        outer.style.overflow = "scroll";
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild(outer);
        Agent.gotScrollBarWidth = true;
        Agent.SB_SIZE = (w1 - w2 - 2); //remove 2px padding
    }
    return Agent.SB_SIZE;
}

Agent.getAgent = function () {
    if (!Agent._agent) {
        var agentName = navigator.userAgent.toLowerCase();

        var agent;

        // check opera first, since it likes to claim its mozilla and msie
        try {
            if (agentName.indexOf("msie") != -1) {
                agent = new IEAgent();
            }
            else if ((agentName.indexOf("applewebkit") != -1) || (agentName.indexOf("safari") != -1)) {
                agent = new Agent();
            }
            else if (agentName.indexOf("gecko/") != -1) {
                agent = new GeckoAgent(version);
            }
            else if (!agent) {
                agent = new Agent();
            }

            Agent._agent = agent;
        }
        catch (e) {
            Agent._agent = new Agent();
        }
    }

    return Agent._agent;
}

/**
* Guess the OS of the agent
*/
Agent.guessOS = function () {
    var agentName = navigator.userAgent.toLowerCase();

    if (agentName.indexOf('win') != -1) {
        return Agent.WINDOWS_OS;
    }
    else if (agentName.indexOf('mac') != -1) {
        return Agent.MAC_OS;
    }
    else if (agentName.indexOf('sunos') != -1) {
        return Agent.SOLARIS_OS;
    }
}

/**
* Guess the version of the Agent
*/
Agent.guessVersion = function () {
    return parseFloat(navigator.appVersion);
}

Agent.InitClass = function () {
    Agent._agent = null;
}

Agent.prototype.Init = function (platform, os, version) {
    //Agent.superclass.Init.call(this); 
    if (!platform)
        platform = Agent.UNKNOWN_PLATFORM;

    if (!os)
        os = Agent.UNKNOWN_OS;

    this._platform = platform;
    this._os = os;
    this._version = version;
}

Agent.prototype.getPlatform = function () {
    return this._platform;
}

Agent.prototype.getOS = function () {
    return this._os;
}

Agent.prototype.getVerson = function () {
    return this._version;
}

/**
* Returns the window in which the element appears
*/
Agent.prototype.getElementWindow = function (element) {
    return element.ownerDocument.contentWindow;
}

/**
* Returns the content height of the browser window
*/
Agent.prototype.getWindowHeight = function () {
    return window.innerHeight;
}

/**
* Returns the content width of the browser window
*/
Agent.prototype.getWindowWidth = function () {
    return window.innerWidth;
}

/**
* Returns the element's left side in Window coordinates
*/
Agent.prototype.getElementLeft = function (element) {

    // =-= bts Mozilla implementation
    var ownerWindow = this.getElementWindow(element);

    var currParent = element.offsetParent;
    var currLeft = element.offsetLeft;

    while (currParent != ownerWindow) {
        element = currParent;
        currLeft += element.offsetLeft;
        currLeft -= element.scrollLeft;
        currParent = currParent.offsetParent;
    }

    return currLeft;
}

/**
* Returns the element's top side in Window coordinates
*/
Agent.prototype.getElementTop = function (element) {
    // =-= bts Mozilla implementation
    var ownerWindow = this.getElementWindow(element);

    var currParent = element.offsetParent;
    var currTop = element.offsetTop;

    while (currParent != ownerWindow) {
        element = currParent;
        currTop += element.offsetTop;
        currTop -= element.scrollTop;
        currParent = currParent.offsetParent;
    }

    return currTop;
}

/**
* Returns the value of a property for the Agent's style object
*/
Agent.prototype.getStyleProperty = function (style, propertyName) {
    return style.getPropertyValue(propertyName);
}

/**
* Tries to return the current style, taking
* into account the inline styles and style sheets
*/
Agent.prototype.getComputedStyle = function (element) {
    return element.ownerDocument.defaultView.getComputedStyle(element, null);
}

/**
* Adds an event listener that fires in the non-Capture phases for the specified
* eventType.  There is no ordering guaranteee, nor is there a guarantee
* regarding the number of times that an event listener will be called if
* it is added to the same element multiple times.
*/
Agent.prototype.addBubbleEventListener = function (element, eventType, listener) {
    element.addEventListener(eventType, listener, false);
}

/**
* Removes an event listener that fired in the non-Capture phases for the specified
* eventType
*/
Agent.prototype.removeBubbleEventListener = function (element, eventType, listener) {
    element.removeEventListener(eventType, listener, false);
}

Agent.prototype.getEventTarget = function (event) {
    // standard dom mechanism for capturing events
    return event.target;
}

/**
* Disable the user's ability to select text in this component
*/
Agent.prototype.disableUserSelect = function (element) {
    // do nothing
}

Agent.prototype.stopPropagation = function (evt) {
    evt.stopPropagation();
}

Agent.prototype.preventDefault = function (evt) {
    evt.preventDefault();
}

Agent.prototype.eatEvent = function (evt) {
    this.stopPropagation(evt);
    this.preventDefault(evt);
}

/**
* Dispatches the specified event to the target, returning whether the
* dispatch was successful
* @targetElement Element to dispatch the event to
* @event Event to dispatch
*/
Agent.prototype.dispatchEvent = function (targetElement, event) {
    return targetElement.dispatchEvent(event);
}

/**
* Static callback handler for consuming a blocked event
*/
Agent.eatEventCallback = function (event) {
    var event = event ? event : window.event;

    // call with the correct instance
    Agent.getAgent().eatEvent(event);

    return false;
}

/**
* Static callback handler for preventing the default action
*/
Agent.preventDefaultCallback = function (event) {
    var event = event ? event : window.event;

    // call with the correct instance
    Agent.getAgent().preventDefault(event);

    return false;
}

/**
* Installs the keyboard blocking handlers for the agent. 
*/
Agent.prototype.installKeyboardBlocker = function (ownerDocument) {
    //ownerDocument.addEventListener("keypress", Agent.eatEventCallback, true);
    //ownerDocument.addEventListener("keyup", Agent.eatEventCallback, true);
    //ownerDocument.addEventListener("keydown", Agent.eatEventCallback, true);
    ownerDocument.onkeypress = Agent.eatEventCallback;
    ownerDocument.onkeydown = Agent.eatEventCallback;
}

/**
* sets opacity of a DOM element
* @param element         The element to set opacity for
* @param opacityPercent  opacity percentage
*/
Agent.prototype.setOpacity = function (element, opacityPercent) {
    element.style.opacity = (opacityPercent / 100);
}

/**
* Disables browser's context menu
*
* @param ownerDocument        DOM document
*/
Agent.prototype.disableBrowserContextMenu = function (ownerDocument) {
    ownerDocument.oncontextmenu = new Function("return false;");
}

/**
* Executes a script.
*
* @param ownerWindow  the DOM Window
* @param scriptText   the script text
*/
Agent.prototype.execScript = function (ownerWindow, scriptText) {
    ownerWindow.eval(scriptText);
}

Agent.prototype.getFormElementByName = function (formObj, elementName) {
    return formObj.elements[elementName];
}

/**
* IE Agent capabilities
*
*/


function IEAgent() {
    this.Init();
}

//inherit from Agent
//IEAgent.prototype = new Agent();
//IEAgent.prototype.constructor = IEAgent;
//IEAgent.superclass = Agent.prototype;

// make IEAgent a subclass of Agent
ClientObject.createSubclass(IEAgent, Agent);

IEAgent.prototype.Init = function () {
    var agentName = navigator.userAgent.toLowerCase();

    var a11 = agentName.match(/msie (\d+\.\d+);/);
    if (a11 == null) {
        a11 = agentName.match(/msie (\d+\.\d+)b;/); // expression for betas
    }
    var agentVersion = parseFloat(a11[1]);

    IEAgent.superclass.Init.call(this, Agent.IE_PLATFORM, Agent.guessOS(), agentVersion);
}

/**
* Returns the element's left side in Window coordinates
*/
IEAgent.prototype.getElementLeft = function (element) {
    // adjust for scrolling and 2 pixel window offset
    return element.getBoundingClientRect().left - element.ownerDocument.documentElement.scrollLeft - 2;
}

/**
* Returns the element's top side in Window coordinates
*/
IEAgent.prototype.getElementTop = function (element) {
    // adjust for scrolling and 2 pixel window offset
    return element.getBoundingClientRect().top - element.ownerDocument.documentElement.scrollTop - 2;
}

/**
* Tries to return the current style, taking
* into account the inline styles and style sheets
*/
IEAgent.prototype.getComputedStyle = function (element) {
    return element.currentStyle;
}

/**
* Returns the value of a property for the Agent's style object
*/
IEAgent.prototype.getStyleProperty = function (style, propertyName) {
    return style[propertyName];
}

/**
* Adds an event listener that fires in the non-Capture phases for the specified
* eventType.  There is no ordering guaranteee, nor is there a guarantee
* regarding the number of times that an event listener will be called if
* it is added to the same element multiple times.
*/
IEAgent.prototype.addBubbleEventListener = function (element, eventType, listener) {
    if (element && element.attachEvent) {
        element.attachEvent("on" + eventType, listener);
    }
}

/**
* Removes an event listener that fired in the non-Capture phases for the specified
* eventType
*/
IEAgent.prototype.removeBubbleEventListener = function (element, eventType, listener) {
    if (element && element.detachEvent) {
        element.detachEvent("on" + eventType, listener);
    }
}

IEAgent.prototype.getEventTarget = function (event) {
    // IE way of returning the source of an event
    return event.srcElement;
}

/**
* Dispatches the specified event to the target, returning whether the
* dispatch was successful
* @targetElement Element to dispatch the event to
* @event Event to dispatch
*/
IEAgent.prototype.dispatchEvent = function (targetElement, event) {
    return targetElement.fireEvent("on" + event.type, event);
}

IEAgent._doNothing = function () {
    var evt = window.event;
    evt.returnValue = false;
    return false;
}

/**
* Disable the user's ability to select text in this component
*/
IEAgent.prototype.disableUserSelect = function (element) {
    element.attachEvent("onselectstart", IEAgent._doNothing);
}

/**
* Enable the user's ability to select text in this component
*/
IEAgent.prototype.enableUserSelect = function (element) {
    element.detachEvent("onselectstart", IEAgent._doNothing);
}

IEAgent.prototype.stopPropagation = function (evt) {
    evt.cancelBubble = true;
}

IEAgent.prototype.preventDefault = function (evt) {
    evt.returnValue = false;

    var keyCode = evt.keyCode;

    // =-= bts check against the control keycode and don't slam that because
    // it's causing me errors in IE 6 every time I control-reload
    if (keyCode && (keyCode != 17) && (keyCode != 16)) {
        try {
            evt.keyCode = 0;
        }
        catch (e) {
            // =-= bts I occassionally see this error when hitting control-
            //Logger.LOGGER.warning("Error slamming keycode:",
            //                      UIUtils.getPropertiesAsString(evt));
        }
    }
}

/**
* Installs the keyboard blocking handlers for the agent. 
*/
IEAgent.prototype.installKeyboardBlocker = function (ownerDocument) {
    ownerDocument.onkeydown = Agent.eatEventCallback;
    ownerDocument.onkeyup = Agent.eatEventCallback;
    ownerDocument.onkeypress = Agent.eatEventCallback;
}

IEAgent.prototype.setOpacity = function (element, opacityPercent) {
    if (isIE8OrEarlier)
        element.style.filter = "alpha(opacity=" + opacityPercent + ")";
    else
        Agent.prototype.setOpacity(element, opacityPercent)
}

/**
* Executes a script.
*
* @param ownerWindow  the DOM Window
* @param scriptText   the script text
*/
IEAgent.prototype.execScript = function (ownerWindow, scriptText) {
    if (scriptText && (scriptText.length > 0))
        ownerWindow.execScript(scriptText);
}

/**
* Returns the content height of the browser window
*/
IEAgent.prototype.getWindowHeight = function () {
    return document.documentElement.clientHeight;
}

/**
* Returns the content width of the browser window
*/
IEAgent.prototype.getWindowWidth = function () {
    return document.documentElement.clientWidth;
}

IEAgent.prototype.getFormElementByName = function (formObj, elementName) {
    return formObj.elements.item(elementName);
}

/**
* Gecko Agent Agent capabilities
* @author Blake Sullivan
*/
function GeckoAgent(version) {
    if (arguments.length)
        this.Init(version);
}

//inherit from Agent
//GeckoAgent.prototype = new Agent();
//GeckoAgent.prototype.constructor = GeckoAgent;
//GeckoAgent.superclass = Agent.prototype;
ClientObject.createSubclass(GeckoAgent, Agent);

GeckoAgent.prototype.Init = function (version) {
    GeckoAgent.superclass.Init.call(this, Agent.GECKO_PLATFORM, Agent.guessOS(), version);
}

/**
* Disable the user's ability to select text in this component
*/
GeckoAgent.prototype.disableUserSelect = function (element) {
    element.style.MozUserSelect = 'none';
}

/**
* Enable the user's ability to select text in this component
*/
GeckoAgent.prototype.enableUserSelect = function (element) {
    element.style.MozUserSelect = '';
}

/**
* Returns the element's left side in Window coordinates
*/
GeckoAgent.prototype.getElementLeft = function (element) {
    // =-= bts the BoxObject (and even the boxObject.screenX and boxObject.screenY)
    //         doesn't take in account the element' scrolling.  
    var totalScrollLeft = 0;

    // =-= bts presumbaly, we need to apply scrolling all the way up to root
    var currElement = element;

    while (currElement != null) {
        totalScrollLeft += currElement.scrollLeft;

        currElement = currElement.offsetParent;
    }

    return element.ownerDocument.getBoxObjectFor(element).x - totalScrollLeft;
}


/**
* Returns the element's top side in Window coordinates
*/
GeckoAgent.prototype.getElementTop = function (element) {
    // =-= bts the BoxObject (and even the boxObject.screenX and boxObject.screenY)
    //         doesn't take in account the element' scrolling.  
    var totalScrollTop = 0;

    // =-= bts presumbaly, we need to apply scrolling all the way up to root
    var currElement = element;

    while (currElement != null) {
        totalScrollTop += currElement.scrollTop;

        currElement = currElement.offsetParent;
    }

    return element.ownerDocument.getBoxObjectFor(element).y - totalScrollTop;
}