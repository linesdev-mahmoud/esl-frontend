﻿if (!window.RIUTIL) {
    RIUTIL = new Object();
}

// Init the instance
var bUserAgent = navigator.userAgent.toLowerCase();
RIUTIL.isWebkit = (bUserAgent.indexOf("webkit") > -1);
RIUTIL.isIOS = (bUserAgent.indexOf("ipad") > -1) || (bUserAgent.indexOf("ipod") > -1) || (bUserAgent.indexOf("iphone") > -1);
RIUTIL.isTouchEnabled = (RIUTIL.isIOS == true) || (bUserAgent.indexOf("android") > -1);
RIUTIL.isNativeContainer = (bUserAgent.indexOf("jdecontainer") > -1);
RIUTIL.isIE = (document.documentMode !== undefined);
RIUTIL.isFirefox = (bUserAgent.indexOf("firefox") > -1);
RIUTIL.isChrome = bUserAgent.indexOf('chrome') > -1;
RIUTIL.isRTL = (document.documentElement.dir == "rtl"); // find out if it is Right to left layout
RIUTIL.namespace = "";
RIUTIL.impactedContainers = new Array();
RIUTIL.captureMode = RIUTIL.NO_CAPTURE; // default mode is no capture
RIUTIL.BAD_CHAR_SET = "~`!@#$%^&*()+={[}]|\\;:'\"<,>.?/";    //some char like " can not be used in folder name on BIP server.
RIUTIL.MAX_CHECK_SAFE_TO_EMBED = 15;
RIUTIL.MIN_DRAG_ACTION_TIME = 300; // the minimal ms for a drag and drop action.
RIUTIL.supportAnimation = true;
RIUTIL.ANIMATION_DURATION = 500;
RIUTIL.NAME_LENGTH = 50;
RIUTIL.SEP_CHAR = "|";
RIUTIL.ERROR_SIGNAITURE = "ERROR|";
RIUTIL.stopPublish = false;
RIUTIL.isPFFeatureEnabled = false;
RIUTIL.IndependentAppHeaderHeight = 26;

//composite page jas keep alive variables
RIUTIL.keepAliveTimer = -1;
RIUTIL.userAction = false;

// Frame Mode
RIUTIL.MODE_DISPLAY = 0;
RIUTIL.MODE_CREATE = 1;
RIUTIL.MODE_EDIT = 2;
RIUTIL.MODE_DETAIL = 3;

// Window State
RIUTIL.WIN_STATE_NORMALIZED = 0;
RIUTIL.WIN_STATE_MINIMIZED = 1;

// Product Codes
RIUTIL.PRODUCT_NEW = -2;
RIUTIL.PRODUCT_ALL = -1;
RIUTIL.PRODUCT_E1 = 0;
RIUTIL.PRODUCT_BEEHIVE = 1;
RIUTIL.PRODUCT_WEB_CENTER = 2;
RIUTIL.PRODUCT_GENERIC_URL = 3;

//path type
RIUTIL.PATHTYPE_URL = 0;
RIUTIL.PATHTYPE_OBIEE = 1;
RIUTIL.PATHTYPE_E1FORM = 6;

// UI Sizes
RIUTIL.CONTAINER_MIN_WIDTH = 70;
RIUTIL.CONTAINER_MIN_HEIGHT = 30;
RIUTIL.DEFAULT_FLY_FRAME_WIDTH = 100;
RIUTIL.DEFAULT_FLY_FRAME_HEIGHT = 40;
RIUTIL.PALETTE_MIN_HEIGHT = 100;
RIUTIL.PALETTE_MAX_HEIGHT = 600;
RIUTIL.PALETTE_PADDING_HEIGHT = 55;
RIUTIL.PALETTE_AVERAGE_HEIGHT = 55;
RIUTIL.PALETTE_WIDTH = 350;
RIUTIL.RULE_WIZARD_WIDTH = 250;
RIUTIL.RESIZE_DIV_SIZE = 4;
RIUTIL.DROPDOWN_INDENT = 120;
RIUTIL.DROPDOWN_PADDING = 5;
RIUTIL.DROPDOWN_MAX_WIDTH = 300;
RIUTIL.MINI_TAB_MAX_HEIGHT = 150;
RIUTIL.MINI_BAR_WIDTH = 15;
RIUTIL.ICON_OFFSET_X = -16;
RIUTIL.ICON_OFFSET_Y = 5;
RIUTIL.MINI_BAR_RIGHT_PADDING = 10;
RIUTIL.MINI_BAR_TOP_PADDING = 23;
RIUTIL.MINI_BAR_TOP_PADDING_NC_ADJ = 15;
RIUTIL.PANE_MENU_HEIGHT = 21;
RIUTIL.MENU_ICON_DISCOUNT = 81;
RIUTIL.MAX_FRAME_SELECT_TAG_WIDTH = 150;
RIUTIL.ERROR_DIALOG_WIDTH = 250;
RIUTIL.ERROR_DIALOG_HEIGHT = 100;
RIUTIL.PADDING_E1PANEDIV = 25;
RIUTIL.PADDING_E1PANEDIV_MODELESS_TAB = 49;

// Dom ID, Name and attributes
RIUTIL.ID_E1MENU_APP_FRAME = "e1menuAppIframe";
RIUTIL.ID_E1_FORM_DIV = "e1formDiv";
RIUTIL.ID_E1_PANE_DIV = "E1PaneDIV";
RIUTIL.ID_E1_PANE_FORM = "E1PaneForm";
RIUTIL.ID_MODELESS_TAB_DIV = "modelessTabDiv";
RIUTIL.ID_RI_FRAMEWOKRK = "RIFramework";
RIUTIL.ID_RI_PANE_DIV = "RIPaneDIV";
RIUTIL.ID_RI_LAYOUT_TABLE = "RILayoutTable";
RIUTIL.ID_RI_MENU_TABLE = "RIMenuTable";
RIUTIL.ID_RI_MENU_BAR = "RIMenuBar";
RIUTIL.ID_RI_PANE_FRAME = "RIPaneIFRAME";
RIUTIL.ID_ADD_ICON = "RIAdd";
RIUTIL.ID_MESSAGE_DIV = "RILayoutMessageDiv";
RIUTIL.ID_LAYOUT_SELECTION_TOP = "RILayoutSelectionTop";
RIUTIL.ID_LAYOUT_DESCRIPTION = "CMPLayoutDescription";
RIUTIL.ID_LAYOUT_PRODCODE = "CMPLayoutProdCode";
RIUTIL.ID_LAYOUT_SELECTION_FORM = "RILayoutSelectionForm";
RIUTIL.ID_LAYOUT_SELECTION_SPAN = "RILayoutSelectionSpan";
RIUTIL.ID_SAVE_ICON = "RISaveIcon";
RIUTIL.ID_SAVE_AS_ICON = "RISaveAsIcon";
RIUTIL.RI_DELETE_ICON = "RIDeleteIcon";
RIUTIL.RI_RESERVE_ICON = "RIReserveIcon";
RIUTIL.ID_NOTES_ICON = "RINotesIcon";
RIUTIL.ID_DESIGN_ACTIONS = "RIDesignActions";
RIUTIL.NAME_ACTION = "RIAction";
RIUTIL.DEFAULT_LAYOUT_ID = "(simple)"; // internal id for no layout in runtime mode or create new layout in design mode
RIUTIL.ATT_CLIENTL_ID = "RIClientID";
RIUTIL.ID_CONTENT_TABLE = "RIContentTable";
RIUTIL.ID_EVENT_LIST = "RIPanelEventList";
RIUTIL.ID_EVENT_RULES_CONTAINER = "RIEventRulesContainer";
RIUTIL.ID_EVENT_TRIGGER = "RIERTrigger";
RIUTIL.ID_EVENT_RULE_INSERT_MAKER = "RIERMarker";
RIUTIL.NAME_EVENT_RULE_INSERT_MAKER = "RIInsertMarker";
RIUTIL.ID_RULE_INSERT_INDICATOR = "RIRuleInsertIndicator";
RIUTIL.ID_RULE_DELETE_INDICATOR = "RIRuleDeleteIndicator";
RIUTIL.ID_NEW_EVENT_ICON = "RINewEvent";
RIUTIL.ID_VALIDATE_EVENT_ICON = "RIValidateEvent"
RIUTIL.ID_CLEAR_EVENT_ICON = "RIClearEvent"
RIUTIL.ID_DELETE_EVENT_ICON = "RIDeleteEvent"
RIUTIL.ID_REQUEST_PUBLISH_ICON = "RIRequestPublishIcon";
RIUTIL.ID_RI_CLOSE_DESIGN = "RI_CLOSE_DESIGN";
RIUTIL.ID_RI_ABOUT_DESIGN = "RI_ABOUT_DESIGN";
RIUTIL.RI_NEW_CONTENT = "RI_NEW_CONTENT";
RIUTIL.RI_OPEN_CONTENT = "RI_OPEN_CONTENT";
RIUTIL.ID_COMPOSITE_EDIT_ENTRY_POINT = 'PersonalizeCompositePageTable';

// CSS classes used
RIUTIL.CLASS_CONTROL_COMPONENT = "RIControlComponent";
RIUTIL.CLASS_SEPARATOR_COMPONENT = "RISeparatorComponent";
RIUTIL.CLASS_KEY_HIGHLIGHT = "RIKeyHighlight RILayoutCell";
RIUTIL.CLASS_PARAM_HIGHLIGHT = "RIParamHighlight RILayoutCell";
RIUTIL.CLASS_PARAM_VALUE_SPAN = "RIParamValueSpan";
RIUTIL.CLASS_LAYOUT_TD = "RILayoutCell";
RIUTIL.CLASS_LAYOUT_TABLE = "RILayoutTable";
RIUTIL.CLASS_LAYOUT_FRAME = "RIDragIndicator RIFloadingContainer RIHalfTransparent";
RIUTIL.CLASS_ENABLED_DESIGN_ACTIONS = "RIEnabledDesignActions";
RIUTIL.CLASS_DISABLED_DESIGN_ACTIONS = "RIDisabledDesignActions";
RIUTIL.CLASS_LAYOUT_CELL = "RILayoutCell RIVAlignTop";

// Ajax request action codes (same as the server)
RIUTIL.ACTION_AJAX = 1;
RIUTIL.ACTION_PAGE = 2
RIUTIL.ACTION_CONNECTION = 3;
RIUTIL.GETLAYOUTNAMES = 4;

// Ajax request commands
RIUTIL.AJAX_CMD_LAYOUT_DESIGN_MODE = 1000;
RIUTIL.AJAX_CMD_LAYOUT_RUNTIME_MODE = 1001;
RIUTIL.AJAX_CMD_CHANGE_SELECTION = 1002;
RIUTIL.AJAX_CMD_START_KEY_CAPTURE = 1003;
RIUTIL.AJAX_CMD_CAPTURE_KEY = 1004;
RIUTIL.AJAX_CMD_CAPTURE_FIELD = 1005;
RIUTIL.AJAX_CMD_RI_SHOW_HIDE_CHANGE = 1006;
RIUTIL.AJAX_CMD_RENDER_ALL = 1007;
RIUTIL.AJAX_CMD_DELETE_COMPONENT = 1008;
RIUTIL.AJAX_CMD_MINIMIZE_CONTENT = 1009;
RIUTIL.AJAX_CMD_REFRESH_CONTENT = 1010;
//RIUTIL.AJAX_CMD_CHANGE_PRODUCT = 1011;
RIUTIL.AJAX_CMD_REFRESH_LAYOUT = 1012;
RIUTIL.AJAX_CMD_CREATE_NEW = 1013;
RIUTIL.AJAX_CMD_SAVE_AS_CUSTOMIZED_LAYOUT = 1014;
//RIUTIL.AJAX_CMD_REMOVE_CONTENT = 1015;
RIUTIL.AJAX_CMD_LOAD_URL_TEMPLATES = 1016;
RIUTIL.AJAX_CMD_PARAM_STATIC = 1017;
RIUTIL.AJAX_CMD_PARAM_DYNAMIC = 1018;
RIUTIL.AJAX_CMD_START_PARM_CAPTURE = 1019;
RIUTIL.AJAX_CMD_RESET_PARM_CAPTURE = 1020;
RIUTIL.AJAX_CMD_PARAM_DISPLAY = 1021;
RIUTIL.AJAX_CMD_GET_WC_TEMPLATES = 1022;
RIUTIL.AJAX_CMD_SAVE_REF = 1023;
RIUTIL.AJAX_CMD_GET_CONTENT_PALETTE = 1024;
RIUTIL.AJAX_CMD_DISPLAY_FRAME_CONTENT = 1026;
RIUTIL.AJAX_CMD_CAPTURE_CUSTOMIZED_TEXT = 1027;
RIUTIL.AJAX_CMD_APPEND_COMPONENT = 1028;
RIUTIL.AJAX_CMD_SET_PATH = 1029;
RIUTIL.AJAX_CMD_POPULATE_KEY = 1030;
RIUTIL.AJAX_CMD_RESET_KEY_CAPTURE = 1031;
RIUTIL.AJAX_CMD_POPUP_URL = 1032;
RIUTIL.AJAX_CMD_VALIDATE_URL = 1033;
RIUTIL.AJAX_CMD_MOVE_IN_CONTAINER = 1034;
RIUTIL.AJAX_CMD_DEACTIVATE_CONTENT = 1035;
RIUTIL.AJAX_CMD_NORMALIZE_CONTENT = 1036
RIUTIL.AJAX_CMD_RESIZE_CONTAINER = 1037;
RIUTIL.AJAX_CMD_GET_CONTENT_FACTORY = 1038;
RIUTIL.AJAX_CMD_SAVE_CUSTOMIZED_LAYOUT = 1039;
RIUTIL.AJAX_CMD_SELECT_CUSTOMIZED_LAYOUT = 1040;
RIUTIL.AJAX_CMD_DELETE_CUSTOMIZED_LAYOUT = 1041;
RIUTIL.AJAX_CMD_CHANGE_APP_ID = 1042;
RIUTIL.AJAX_CMD_CHANGE_FORM_ID = 1043;
RIUTIL.AJAX_CMD_CHANGE_VERSION_ID = 1044;
RIUTIL.AJAX_CMD_LOAD_EVENT_LIST = 1045;
RIUTIL.AJAX_CMD_LOAD_EVENT_RULES = 1046;
RIUTIL.AJAX_CMD_CREATE_USER_EVENT = 1047;
RIUTIL.AJAX_CMD_REMOVE_EVENT = 1048;
RIUTIL.AJAX_CMD_EXECUTE_EVENT = 1049;
RIUTIL.AJAX_CMD_INSERT_TEMPLATE_TO_EVENT = 1050;
RIUTIL.AJAX_CMD_CLEAR_EVENT = 1051;
RIUTIL.AJAX_CMD_REMOVE_RULE_FROM_EVENT = 1052;
RIUTIL.AJAX_CMD_LOAD_WIZARD_LIST = 1053;
RIUTIL.AJAX_CMD_LINK_TO_FIELD = 1054;
RIUTIL.AJAX_CMD_SET_RULE_USER_VALUE = 1055;
RIUTIL.AJAX_CMD_SET_RULE_OPERATOR = 1056;
RIUTIL.AJAX_CMD_CHANGE_AUTO_FIND = 1057;
RIUTIL.AJAX_CMD_SET_FRAME_REF_OBJ = 1058;
RIUTIL.AJAX_CMD_SET_MENU_ID = 1059;
RIUTIL.AJAX_SET_SAT_STACK_ID = 1060;
RIUTIL.AJAX_CMD_CHANGE_MAX_GRID = 1061;
RIUTIL.AJAX_CMD_REREQPUBLISH_CUSTOMIZED_LAYOUT = 1062;
RIUTIL.AJAX_CMD_RESERVE_CUSTOMIZED_LAYOUT = 1063;
RIUTIL.AJAX_CMD_UNRESERVE_CUSTOMIZED_LAYOUT = 1064;
RIUTIL.AJAX_CMD_GETLAYOUTNAMES = 1065;
RIUTIL.AJAX_CMD_LAYOUT_PREVIEW_MODE = 1066;
RIUTIL.AJAX_CMD_REQPUBLISH_CUSTOMIZED_LAYOUT = 1067;
RIUTIL.AJAX_CMD_CHANGE_SHOW_TITLE_BAR = 1068;


//TODO: remove this commment from production -- add some gap here in case something added from staging view before project check in
RIUTIL.AJAX_CMD_SET_FREESTYLE_METADATA = 1072;
RIUTIL.AJAX_CMD_UPDATE_FSF_SECTION_STATE = 1073;
RIUTIL.AJAX_CMD_REQUEST_INIT = 1074;
RIUTIL.AJAX_CMD_CHANGE_REPORT_ID = 1075;
RIUTIL.AJAX_CMD_CHANGE_QUERY_ID = 1076;
RIUTIL.AJAX_CMD_CHANGE_E1PAGE_ID = 1077;
RIUTIL.AJAX_CMD_LOAD_E1_PAGE_LIST = 1078;
RIUTIL.AJAX_CMD_LAYOUT_MANAGER_DEACTIVE = 1079;
RIUTIL.AJAX_CMD_LOAD_ADF_LIST = 1080;
RIUTIL.AJAX_CMD_CHANGE_ADF_ID = 1081;
RIUTIL.AJAX_CMD_SHOW_COMPOSITE_INFO = 1082;
RIUTIL.AJAX_CMD_CLEANUP_AFTER_LCM = 1083;
RIUTIL.AJAX_CMD_SET_WL_PANE_METADATA = 1084;
RIUTIL.AJAX_CMD_UPDATE_WL_RUNTIME_FILTER = 1085;
RIUTIL.AJAX_CMD_SET_JAS_DOMAIN = 1086;
RIUTIL.AJAX_CMD_CHANGE_ADF_PROXY_FORM_ID = 1087;
RIUTIL.AJAX_CMD_CHANGE_ADF_PROXY_VERSION_ID = 1088;
RIUTIL.AJAX_CMD_SET_LP_PANE_METADATA = 1089;
RIUTIL.AJAX_CMD_GET_MISERVICE_STATUS = 1090;
RIUTIL.AJAX_CMD_REQUEST_INIT_CONSTANTS = 1091;

// Page requeset commmands
RIUTIL.PAGE_CMD_SHOW_LIST = 2001;
RIUTIL.PAGE_CMD_SHOW_DETAIL = 2002;
RIUTIL.PAGE_CMD_SHOW_CREATE = 2003;
RIUTIL.PAGE_CMD_SHOW_EDIT = 2004;
RIUTIL.PAGE_CMD_LOAD_FREESTYLE = 2005;
RIUTIL.PAGE_CMD_LOAD_WL_COMPONENT = 2006;
RIUTIL.PAGE_CMD_LOAD_LANDINGPAGE = 2007;

// Ajax request parameters
RIUTIL.CAFEONE_UDO_SERVICE = "CafeOneManagerService";
RIUTIL.FRONT_SERVLET = "CafeOneManagerService";
RIUTIL.PARAM_ACTION = "Action";
RIUTIL.PARAM_COMMAND = "cmd";
RIUTIL.PARAM_RI_COMMAND = "RI_CMD";
RIUTIL.PARAM_PRODUCT = "product";
RIUTIL.PARAM_CONTAINER_ID = "containerId";
RIUTIL.PARAM_CLIENTL_ID = "clientId";
RIUTIL.PARAM_STACK_ID = "stackId";
RIUTIL.PARAM_SAT_STACK_ID = "satStackId";
RIUTIL.PARAM_INDEX = "index";
RIUTIL.PARAM_OBJECT_DES = "objDescription";
RIUTIL.PARAM_OBJECT_NAME = "objName";
RIUTIL.PARAM_PUBLIC_URL = "publicLinkUrl";
RIUTIL.PARAM_PARTID = "partId";
RIUTIL.PARAM_CONFIRMED = "confirmed";
RIUTIL.PARAM_SEP = "separator";
RIUTIL.PARAM_CUSTOMIZE_TEXT = "customizedText";
RIUTIL.PARAM_OPERATOR = "operator";
RIUTIL.PARAM_PATH_TYPE = "pathType";
RIUTIL.PARAM_PATH = "path"
RIUTIL.PARAM_COMPONENT_INDEX = "componentIndex";
RIUTIL.PARAM_RESET_PART = "resetPart";
RIUTIL.PARAM_MOVING_CONTAINER = "movingContainer";
RIUTIL.PARAM_MOVING_OBJECT = "movingObject";
RIUTIL.PARAM_TARGET_CONTAINER = "targetContainer";
RIUTIL.PARAM_ZONE = "zone";
RIUTIL.PARAM_PERCENTAGE = "percentage";
RIUTIL.PARAM_X_RATIO = "x_ratio";
RIUTIL.PARAM_Y_RATIO = "y_ratio";
RIUTIL.PARAM_LAYOUT_NAME = "layoutName";
RIUTIL.PARAM_LAYOUT_DESCRIPTION = "layoutDescription";
RIUTIL.PARAM_LAYOUT_ID = "layoutId";
RIUTIL.PARAM_OBJECT_ID = "objectId";
RIUTIL.PARAM_USER_TYPED = "userTyped";
RIUTIL.PARAM_URL = "url";
RIUTIL.PARAM_APP_ID = "appId";
RIUTIL.PARAM_FORM_ID = "formId";
RIUTIL.PARAM_VERSION_ID = "versionId";
RIUTIL.PARAM_EVENT_INDEX = "eventIndex";
RIUTIL.PARAM_EVENT_NAME = "eventName";
RIUTIL.PARAM_RULE_ID = "eventRuleId";
RIUTIL.PARAM_RULE_ACTION = "ruleAction";
RIUTIL.PARAM_TEMPLATE_ID = "templateId";
RIUTIL.PARAM_WIZARD_ID = "wizardId";
RIUTIL.PARAM_BASE_ID = "baseId";
RIUTIL.PARAM_USER_VALUE = "userValue";
RIUTIL.PARAM_REF_OBJ_ID = "refObjId";
RIUTIL.PARAM_MENU_ID = "menuId";
RIUTIL.PARAM_AUTO_FIND = "autoFind";
RIUTIL.PARAM_MAX_GRID = "maxGrid";
RIUTIL.PARAM_OBJECT_TYPE = "objectType";
RIUTIL.PARAM_OMWOBJECTNAME = "omwObjectName";
RIUTIL.PARAM_HASTOKEN = "hasToken";
RIUTIL.PARAM_TOKENPROJECTNAME = "tokenProjectName";
RIUTIL.PARAM_SAVEAS_LAYOUT = "saveAsLayout";
RIUTIL.PARAM_Id = "id";
RIUTIL.PARAM_APPROVERNOTES = "approverNotes";
RIUTIL.PARAM_CLOSEDESIGNBAR = "isCloseDesignBar";
RIUTIL.PARAM_DESIGN_MODE = "design";
RIUTIL.PARAM_METADATA = "metadata";
RIUTIL.PARAM_REPORT_ID = "reportId";
RIUTIL.PARAM_QUERY_ID = "queryId";
RIUTIL.PARAM_E1PAGE_ID = "e1PageId";
RIUTIL.PARAM_ADF_ID = "ADFId";
RIUTIL.PARAM_JAS_DOMAIN = "JASDomain";
RIUTIL.PARAM_INCLUDE_ALL = "includeAll";
RIUTIL.PARAM_INIT_COLS = "initCols";
RIUTIL.PARAM_INIT_ROWS = "initRows";
RIUTIL.PARAM_SHOW_TITLE_BAR = "showTitleBar";

// Sub types for pathtype
RIUTIL.TYPE_GENERIC = 0;
RIUTIL.TYPE_OBIEE_REPORT = 1;
RIUTIL.TYPE_OBIEE_DASHBOAD = 2;
RIUTIL.TYPE_OBIEE_PAGE = 3;
RIUTIL.TYPE_OBIEE_FOLDER = 4;
RIUTIL.TYPE_WEBCENTER_SPACE = 5;
RIUTIL.TYPE_E1_FORM = 6;
RIUTIL.TYPE_FREESTYLE_FRAME = 7;
RIUTIL.TYPE_E1_PAGE_FRAME = 8;
RIUTIL.TYPE_OVR_FRAME = 9;
RIUTIL.TYPE_PORTLET_FRAME = 10; //deprecated
RIUTIL.TYPE_E1_NAV_FRAME = 11;
RIUTIL.TYPE_ADF_FRAME = 12;
RIUTIL.TYPE_WL_PANE = 13;
RIUTIL.TYPE_LANDING_PANE = 14;

// Types of URL part
RIUTIL.TYPE_LITERAL = 1;
RIUTIL.TYPE_CUSTOMIZED = 2;
RIUTIL.TYPE_OBIEE_FILTER = 3;

// Container id for pallettes
RIUTIL.CONTAINER_ID_FACTORY = -1;
RIUTIL.CONTAINER_ID_PALETTE = -2;
RIUTIL.CONTAINER_ID_WIZARD = -2;

// Capture State
RIUTIL.NO_CAPTURE = 0;
RIUTIL.KEY_CAPTURE = 1;
RIUTIL.PARAM_CAPTURE = 2;
RIUTIL.VISIBLE_FIELD_CAPTURE = 3;
RIUTIL.EDITABLE_FIELD_CAPTURE = 4;
RIUTIL.MENU_ITEM_CAPTURE = 5;

// Layout Types
RIUTIL.LAYOUT_VERTICAL = 0;
RIUTIL.LAYOUT_HORIZONTAL = 1;
RIUTIL.LAYOUT_SINGLE = 2;

// Containter Types
RIUTIL.CONATINAER_ROOT = 0;
RIUTIL.CONATINAER_E1 = 1;
RIUTIL.CONATINAER_RIAF = 2;
RIUTIL.CONATINAER_FREESTYLE = 3;
RIUTIL.CONATINAER_WL_COMPONENT = 4;
RIUTIL.CONATINAER_LANDING_PAGE = 5;

// Zone Types for drag and drop 
RIUTIL.ZONE_TOP = 1;
RIUTIL.ZONE_BOTTOM = 2;
RIUTIL.ZONE_LEFT = 3;
RIUTIL.ZONE_RIGHT = 4;

// Return code for fitTarget
RIUTIL.FIT_OK = 1;
RIUTIL.FIT_CLOSE = 2;
RIUTIL.FIT_OUT = 3;

// tag name for the rule render dispatch
RIUTIL.TAG_ACTIONS = "ACTIONS";
RIUTIL.TAG_TRIGGERS = "TRIGGERS";
RIUTIL.TAG_COMPOSITE = "COMPOSITE";
RIUTIL.TAG_LEAF = "LEAF";
RIUTIL.TAG_WIZARD = "WIZARD";

// base id for the basic rule components
RIUTIL.BID_EDITABLE_FIELD = 1;
RIUTIL.BID_VISIBLE_FIELD = 2;
RIUTIL.BID_MENU = 3;
RIUTIL.BID_AGGREGATE_OPERATOR = 4;
RIUTIL.BID_COMPARE_OPERATOR = 5;
RIUTIL.BID_VALUE = 6;
RIUTIL.BID_FRAME = 8
RIUTIL.BID_BUTTON = 9;

// Wizard ID
RIUTIL.WID_ACTIONS_BLOCK = 100;
RIUTIL.WID_LOGIC = 200;
RIUTIL.WID_DATA = 300;
RIUTIL.WID_CAPTURE_EDITABLE_FIELD = 400;
RIUTIL.WID_CAPTURE_BUTTON = 500;
RIUTIL.WID_TRIGGERS_BLOCK = 600;
RIUTIL.WID_CAPTURE_MENU = 700

// template id
RIUTIL.TID_SET = 101;
RIUTIL.TID_PUSH_MENU = 102;
RIUTIL.TID_IF_THEN = 103;
RIUTIL.TID_IF_ELSE_THEN = 104;
RIUTIL.TID_WHILE = 105;

RIUTIL.TID_COMPARE = 201;
RIUTIL.TID_BETWEEN = 202;
RIUTIL.TID_AND = 203;
RIUTIL.TID_OR = 204;
RIUTIL.TID_NOT = 205;

RIUTIL.TID_USER_VALUE = 301;
RIUTIL.TID_SOURCE_FIELD = 302;
RIUTIL.TID_SOURCE_AGGREGATE = 303;

RIUTIL.TID_USER_TRIGGER = 601;
RIUTIL.TID_FRAME_OPEN = 602;
RIUTIL.TID_ROW_SELECTION = 603;
RIUTIL.TID_DATA_CHANGE = 604;

// aggregate operator list
RIUTIL.AGGREGATE_OPERATORS = (["+", "-", "*", "/"]);
RIUTIL.COMPARE_OPERATORS = (["=", "<", ">", "<=", ">=", "<>"]);
RIUTIL.FILE_EXIT_ID_LIST = (["hc_OK", "hc_Cancel", "hc_Find", "hc_Select", "hc_Add", "hc_Copy", "hc_Delete", "hc_Close", "hc_Update", "hc_Next", "hc_Previous", "hc_Save", "hc_Clear", "hc_Edit"]);

RIUTIL.RULE_ACTION_INIT_INSERT = 1;
RIUTIL.RULE_ACTION_INSERT = 2;
RIUTIL.RULE_ACTION_REPLACE = 3;
RIUTIL.PUBLIC_LAYOUT_ID = "*PUBLIC";

RIUTIL.cmdMap = {
    1039: "save",
    1014: "saveas",
    1041: "delete",
    1067: "requestPublish",
    1062: "reRequestPublish",
    1063: "reserve",
    1064: "unreserve"
};

//---------------------------------------
// Render Methods
//---------------------------------------
// Public: invoked by viewmax.jsp to init framework constants only   
RIUTIL.requestInitConstants = function () {
    RIUTIL.postAjaxRequest("", RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_REQUEST_INIT_CONSTANTS);
}

// Public: invoked by welcome.js only to notify Server to kick off rendering process of Composite page given a prefered active composite layout id    
RIUTIL.requestInitCompositePage = function (activeCompositeLayoutId, lcmEditMode) {
    var isLCM = false;
    if (lcmEditMode !== undefined) {
        RIUTIL.lcmEdit = lcmEditMode;
        RIUTIL.lcmLayoutId = activeCompositeLayoutId;
        isLCM = true;
    }
    RIUTIL.postAjaxRequest("", RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_REQUEST_INIT, RIUTIL.PARAM_LAYOUT_ID, activeCompositeLayoutId, RIUTIL.PARAM_JAS_DOMAIN, top.location.origin, "isLCM", isLCM);
}

// Public: invoked by VTForm render to initialized CafeOne before rendering the workspace.
RIUTIL.init = function (json, constantOnly) {
    RIUTIL.postAjaxRequest("", RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_SET_JAS_DOMAIN, RIUTIL.PARAM_JAS_DOMAIN, top.location.origin);
    if (localStorage.getItem("userAccessibilityMode"))
        RIUTIL.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;

    RIUTIL.CONSTANTS = eval(json);
    if (top.RIUTIL && !top.RIUTIL.CONSTANTS)
        top.RIUTIL.CONSTANTS = RIUTIL.CONSTANTS;

    if (constantOnly)
        return;

    RIUTIL.requestRenderAll();
    RIUTIL.formTitle = document.title;
}

// Public: invoked by VTFormReanderAdaptee.java with false value for CafeOne and 
//         invoked by Server with true value for composite page
RIUTIL.setCompositePage = function (isCompositePage) {
    top.RIUTIL.isCompositePage = RIUTIL.isCompositePage = isCompositePage;
}

//  Used for Composite Page Launch
RIUTIL.setCompositePageAsApp = function (isCompositePageAsApp) {
    top.RIUTIL.isCompositePageAsApp = RIUTIL.isCompositePageAsApp = isCompositePageAsApp;
}

RIUTIL.initForPreview = function (json) {
    RIUTIL.CONSTANTS = eval(json);
    //RIUTIL.requestRenderAll();
    RIUTIL.formTitle = document.title;
}

// Public: invoked by VTForm render to remove layout manager during form interconnnect
RIUTIL.uninit = function () {
    RIUTIL.renderLayoutManager("");
}

// Public: invoked by Server to initialize the dom structure for the workspace
RIUTIL.renderCafeOne = function (json) {
    if (RIUTIL.oldLayoutJSON == json) // do not need to redraw the same json string
        return;

    var document = RIUTIL.getFrameworkDoc();
    // client height has to be stored before the E1 Pane div is removed from the dom to prevent using 0 height
    var clientHeight = RIUTIL.getClientHeight();
    RIUTIL.oldLayoutJSON = json; // cache the json string to improve the preformence and reduce the draw
    var oldLayout = RIUTIL.layout;
    RIUTIL.layout = eval(json);

    var isPreviousDefaultLayout = !oldLayout || !oldLayout.subContainers;
    // do not need to render anything for layout of cafeone without subcontainer and there is not preexisting previous default layout to switch from and we are in default layout for a full rendering
    // the overhead for moveing E1FormDiv will cause bad perfomrence for big list view
    if (!RIUTIL.isCompositePage && isPreviousDefaultLayout && !RIUTIL.layout.subContainers) {
        return;
    }

    RIUTIL.layout.isBlank = RIUTIL.isCompositePage && RIUTIL.layout.containerType == RIUTIL.CONATINAER_ROOT && !RIUTIL.layout.subContainers;

    // remember the current focus and we need to set the focus back after reconsturct the dom
    try // IE9 has bug to throw exception and we need to try catch to avoid it
    {
        var currentFocusCtrlId = document.activeElement ? document.activeElement.id : null;
    }
    catch (e) { }

    // body - E1 PaneFrom - Table (Title bar)    [keep]
    //                    - Table (WebMenuBar) [detached]
    //                    - E1FormDiv  [detached]
    // ->
    // body - E1 PaneFrom - Table (Title bar)    [keep]
    //                    - E1PaneDiv - Table (WebMenuBar) [attached]
    //                                - E1FormDiv  [attached]

    // detach e1PaneForm from dom and pass the reference down to
    // the constuction process and it will be attached back to the dom in the process
    if (RIUTIL.isCompositePage) // for composite page,  replace all resource in head part
    {
        var e1FormDiv = null;
        document.body.style.overflow = "hidden";
        // add css to blank frame
        var header = document.getElementsByTagName("head")[0];
        RIUTIL.removeAllChildren(header);
        RIUTIL.addDynamicLink(header, "css", "/jde/share/css/webclient.css");
        RIUTIL.addDynamicLink(header, "css", "/jde/share/css/webclient.jsp");
        RIUTIL.addDynamicLink(header, "js", "/jde/share/js/webgui.js");
        RIUTIL.removeAllChildren(document.body);

        //if this is running as an app render the header bar
        if (RIUTIL.isCompositePageAsApp) {
            RIUTIL.renderLaunchAsAppHeader(json);
        }
    }
    else // for cafeone, only ned to preserve e1FormDiv
    {
        var e1FormDiv = document.getElementById(JSSidePanelRender.ID_FRAME_TABLE);
        // with side panel opened
        if (e1FormDiv) {
            var e1FormDiv = e1FormDiv.parentNode.removeChild(e1FormDiv);
        }
        else // with side panel closed
        {
            var e1FormDiv = document.getElementById(RIUTIL.ID_E1_FORM_DIV);
            var e1FormDivParent = e1FormDiv.parentNode;
            while (e1FormDivParent != null && e1FormDivParent.tagName != "FORM") {
                e1FormDivParent = e1FormDivParent.parentNode;
            }
            e1FormDiv = e1FormDiv.parentNode.removeChild(e1FormDiv);

            // detach e1PaneForm from dom and pass the reference down to
            // the constuction process and it will be attached back to the dom in the process
            var webMenuBar = RIUTIL.getWebMenuBar();
            if (webMenuBar)
                webMenuBar = webMenuBar.parentNode.removeChild(webMenuBar);
        }

        // remove old e1PaneDIV if exist
        var e1PaneDIV = document.getElementById(RIUTIL.ID_E1_PANE_DIV);
        if (e1PaneDIV)
            e1PaneDIV.parentNode.removeChild(e1PaneDIV);
    }

    RIUTIL.miniContainerIds = RIUTIL.getFrameIds(RIUTIL.layout, new Array(), true);

    if (RIUTIL.isCompositePageAsApp) {
        var parentNode = document.body;
        frameworkHeight = clientHeight - RIUTIL.IndependentAppHeaderHeight;
    }
    else if (RIUTIL.isCompositePage) // for composite page, the parentNode of framework is body
    {
        var parentNode = document.body;
        var frameworkHeight = clientHeight;
    }
    else // for cafeone, the parentNode of framework is body is page element.
    {
        // if in a tab Form Inteconnect format, we need to use different parent node 
        var parentNode = document.getElementById(RIUTIL.ID_MODELESS_TAB_DIV);
        if (!parentNode) // without modeless tab
        {
            if (e1FormDivParent)
                parentNode = e1FormDivParent;
            else
                parentNode = document.getElementById(RIUTIL.ID_E1_PANE_FORM);

            var frameworkHeight = clientHeight - RIUTIL.PADDING_E1PANEDIV; // form title height
        }
        else //with modless tab
        {
            var frameworkHeight = clientHeight - RIUTIL.PADDING_E1PANEDIV_MODELESS_TAB;
        }
    }

    var clientWidth = document.body.clientWidth;
    var frameworkWidth = clientWidth - RIUTIL.MINI_BAR_RIGHT_PADDING;
    var workspaceHeight = frameworkHeight;
    var workspaceWidth = frameworkWidth - (RIUTIL.miniContainerIds.length > 0 ? RIUTIL.MINI_BAR_WIDTH : 0);

    RIUTIL.cleanupFramework(oldLayout);
    if (RIUTIL.layout.isBlank) {
        if (RIUTIL.isCompositePageAsApp) {
            RIUTIL.renderInvalidPageAsApp();
        }
        else {
            RIUTIL.renderBlankDesignPage();
        }
        return;
    }

    RIUTIL.adjustAllConatinersPercentage(RIUTIL.layout);
    RIUTIL.calculateAllContainerSize(workspaceHeight, workspaceWidth, RIUTIL.layout, RIUTIL.LAYOUT_HORIZONTAL);
    RIUTIL.constructFramework(parentNode, workspaceWidth, frameworkHeight, webMenuBar, e1FormDiv);
    if (!RIUTIL.isCompositePage) {
        JSCompMgr.onLoad("", true); // already resize before we render Cafeone

        // update the cafeone row cursor (since the change of key and parameter after layout switch may result in the different visibility of cursor row.
        var firstGrid = JDEDTAFactory.getInstance(RIUTIL.namespace).getFirstGrid();
        if (firstGrid != null && !isNaN(firstGrid.cafeOneCursorRow)) // Only supported if there is one grid on the screen
            firstGrid.updateCafeOneCursorIndicator(firstGrid.cafeOneCursorRow, true);

        // restore focus with some delay to give enough time for the DOM to settle down
        if (currentFocusCtrlId) {
            FCHandler.setFocusOnElementLater(currentFocusCtrlId, 200);
        }

        // For chrome, the horizontal scrollbar does not go away. this is a hack to get ride of this scrollbar.
        setTimeout("RIUTIL.removeScrollbar()", 1000);
    }
    else if (!RIUTIL.isTouchEnabled)
        document.body.onresize = RIUTIL.onResize;
}
//
RIUTIL.renderLaunchAsAppHeader = function (json) {
    var document = RIUTIL.getFrameworkDoc();
    var jsonObj = eval(json);
    var header = document.getElementsByTagName("head")[0];
    RIUTIL.addDynamicLink(header, "js", "/jde/js/Form.js");
    RIUTIL.addDynamicLink(header, "js", "/jde/js/JQuery/jquery-2.1.1.min.js");

    var script = document.createElement("script");
    script.innerHTML = jsonObj.appHeaderScripts;
    header.appendChild(script);
    // header.innerHTML = header.innerHTML + "<style>   a:hover {   text-decoration:none;   }   a:focus {   text-decoration:none;   }</style><script>function about(action){   var e1UrlCreator = parent._e1URLFactory.createInstance(parent._e1URLFactory.URL_TYPE_SERVICE);   e1UrlCreator.setURI('About');   e1UrlCreator.setParameter('SID','strSession');   e1UrlCreator.setParameter('id', '748e60f22f7433c3');   e1UrlCreator.setParameter('stackId', 1);   e1UrlCreator.setParameter('appName', 'P0801');   e1UrlCreator.setParameter('appVersion', 'ZJDE0001');   if (action)   {      e1UrlCreator.setParameter('action', action);   }      window.open(e1UrlCreator.toString(),'Application_Information1459870672072','width=400,height=435,scrollbars=1,resizable=yes,titlebar');   } </script><script>function showHelp(wideContextID,productCode)   {       e1UrlCreator = parent._e1URLFactory.createInstance(parent._e1URLFactory.URL_TYPE_SERVICE);       e1UrlCreator.setURI('Help');        e1UrlCreator.setParameter('ContextID', wideContextID);        e1UrlCreator.setParameter('PreContextID',wideContextID);        e1UrlCreator.setParameter('WideContextID', wideContextID);               e1UrlCreator.setParameter('ProductCode', productCode);               var stamp = new Date().getTime();       var helpName = 'OneWorldHelp' + stamp;       e1UrlCreator.setParameter('id', stamp);       window.open(e1UrlCreator.toString(),helpName,'toolbar=no,width=740,height=540,resizable,scrollbars=yes');   }</script><script type='text/javascript'>   document.onkeydown = document_onkeydown;      function document_onkeydown(e)   {   var evt=e?e:window.event;   if(isCtrlDown(evt))   {    if (evt.keyCode == 74)      {       about('FormInfo');        }       }   }   function isCtrlDown(keyEvent)   {   var evt=keyEvent?keyEvent:window.event;   return evt.ctrlKey;   }</script><script>function close()   {   parent.ExternalAppsHandler.closeExternalApplication('1');}</script>";
    var headerSpan = document.createElement('span');
    headerSpan.id = "externalAppHeader";
    headerSpan.innerHTML = jsonObj.appHeaderBody;
    document.body.appendChild(headerSpan);


}
// Private: render a blank page for secured page running as an app
RIUTIL.renderInvalidPageAsApp = function () {
    //header was already rendered, write invalid page error
    var document = RIUTIL.getFrameworkDoc();
    var heading = document.createElement("h2");
    heading.innerHTML = "Composite Page Not Found";
    document.body.appendChild(heading);
}

// Private: render a blank page for design mode of composite page to ask user to add content into it.
RIUTIL.renderBlankDesignPage = function () {
    var document = RIUTIL.getFrameworkDoc();
    RIUTIL.removeAllChildren(document.body);
    document.body.onresize = "";

    var h1 = document.createElement("h1");
    h1.className = "RIWelcomeHeading";
    h1.appendChild(document.createTextNode(RIUTIL.CONSTANTS.CP_BLANK_WELCOME))
    document.body.appendChild(h1);
    //h1.appendChild(document.createTextNode())

    var table = document.createElement("table");
    table.style.width = "100%";
    table.style.paddingLeft = '5px';
    table.style.paddingRight = '5px';
    var row = table.insertRow(-1);

    var cellOption1 = row.insertCell(-1);
    var image1 = document.createElement("img");
    image1.src = top['E1RES_share_images_alta_dash_createcontent_real_png'];
    cellOption1.appendChild(image1);

    var cellOption2 = row.insertCell(-1);
    var image2 = document.createElement("img");
    image2.src = top['E1RES_share_images_alta_dash_opencontent_real_png'];
    cellOption2.appendChild(image2);

    var row = table.insertRow(-1);

    var cellOption1 = row.insertCell(-1);
    var h3 = document.createElement("h2");
    h3.appendChild(document.createTextNode(RIUTIL.CONSTANTS.CP_BLANK_CREATE_CONTENT));
    cellOption1.appendChild(h3);
    var cellOption2 = row.insertCell(-1);
    var h3 = document.createElement("h2");
    h3.appendChild(document.createTextNode(RIUTIL.CONSTANTS.CP_BLANK_OPEN_CONTENT));
    cellOption2.appendChild(h3);

    document.body.appendChild(table);
}

// Public: invoked by Server to refresh whole page (for redraw page tab stubs)
RIUTIL.renderPageRefresh = function () {
    window.location.replace(window.location.href);
}

// Private: add a css link dynamiclly to the header
RIUTIL.addDynamicLink = function (header, filetype, URL) {
    var fileref = document.createElement("link");

    if (filetype == "js") {   // a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", URL)
    }
    else if (filetype == "css") {   // an external CSS file
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", URL)
    }

    header.appendChild(fileref);
}

// Private: a hack to remove extra scrollbar for chrome
RIUTIL.removeScrollbar = function () {
    var e1FormDiv = document.getElementById(RIUTIL.ID_E1_FORM_DIV);
    if (e1FormDiv)
        e1FormDiv.style.width = e1FormDiv.offsetWidth + "px";
}


// Public: Resize handler
RIUTIL.onResize = function () {
    try {
        var uiBlockingDiv = document.getElementById('UIBlockingDiv');
        if (uiBlockingDiv)
            uiBlockingDiv.style.width = top.innerWidth + "px";
        RIUTIL.resizeCafeOne(RIUTIL.oldLayoutJSON);
    }
    catch (e) {
    }
}

// Public: invoked by Server to initialize the dom structure for the workspace
// in firfox, this method is called more requently (for switch between runtiem and design mode)
RIUTIL.resizeCafeOne = function (json) {
    if (!json)
        return;

    // the client need to be updated based on the the server side model change
    var oldLayout = RIUTIL.layout;
    RIUTIL.oldLayoutJSON = json; // cache the json string to improve the preformence and reduce the draw
    RIUTIL.layout = eval(json);

    if (RIUTIL.isCompositePageAsApp) {
        var frameworkHeight = RIUTIL.getClientHeight() - RIUTIL.IndependentAppHeaderHeight;
        var frameworkWidth = RIUTIL.getFrameworkWindow().innerWidth;
    }
    else if (RIUTIL.isCompositePage) {
        var frameworkHeight = RIUTIL.getClientHeight();
        var frameworkWidth = RIUTIL.getFrameworkWindow().innerWidth;
    }
    else {
        // if in a tab Form Inteconnect format, we need to use different parent node 
        var parentNode = document.getElementById(RIUTIL.ID_MODELESS_TAB_DIV);
        if (!parentNode) // without modeless tab
        {
            parentNode = document.body;
            var frameworkHeight = RIUTIL.getClientHeight() - RIUTIL.PADDING_E1PANEDIV;
        }
        else //with modless tab
        {
            var frameworkHeight = RIUTIL.getClientHeight() - RIUTIL.PADDING_E1PANEDIV_MODELESS_TAB;
        }
        var frameworkWidth = document.body.clientWidth - RIUTIL.MINI_BAR_RIGHT_PADDING;
    }


    var workspaceHeight = frameworkHeight;
    var workspaceWidth = frameworkWidth - (RIUTIL.miniContainerIds.length > 0 ? RIUTIL.MINI_BAR_WIDTH : 0);

    RIUTIL.adjustAllConatinersPercentage(RIUTIL.layout);
    RIUTIL.calculateAllContainerSize(workspaceHeight, workspaceWidth, RIUTIL.layout, RIUTIL.LAYOUT_HORIZONTAL);
    RIUTIL.resizeFramework(parentNode, workspaceWidth, frameworkHeight);
    RIUTIL.setupEventReference();
    if (!RIUTIL.isCompositePage)
        JSCompMgr.onLoad("", true);
}

// Private: populate the container id list for all minimized frames.
RIUTIL.getFrameIds = function (container, idList, isMinimized) {
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            if (container.subContainers) {
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    RIUTIL.getFrameIds(subContainer, idList, isMinimized);
                }
            }
            break;

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            if (isMinimized) {
                if (RIUTIL.isMinimized(container) && !RIUTIL.isFilteredOut(container))
                    idList.push(container.id);
            }
            else {
                if (!RIUTIL.isMinimized(container) && !RIUTIL.isFilteredOut(container))
                    idList.push(container.id);
            }
    }
    return idList;
}

// Private: construct the framework of CafeOne
RIUTIL.constructFramework = function (parentNode, workspaceWidth, frameworkHeight, webMenuBar, e1FormDiv) {
    var table = RIUTIL.createTable();

    parentNode.appendChild(table);
    table.id = RIUTIL.ID_RI_FRAMEWOKRK;
    table.className = RIUTIL.CLASS_LAYOUT_TABLE;
    table.setAttribute('cellPadding', 0);
    table.setAttribute('cellSpacing', 0);

    var row = table.insertRow(-1);
    var workspaceCell = row.insertCell(-1);
    workspaceCell.style.width = workspaceWidth + "px";
    workspaceCell.className = RIUTIL.CLASS_LAYOUT_CELL;

    if (RIUTIL.miniContainerIds.length > 0) {
        var miniBarCell = row.insertCell(-1);
        miniBarCell.className = RIUTIL.CLASS_LAYOUT_CELL;
        miniBarCell.style.width = RIUTIL.MINI_BAR_WIDTH + "px";
        RIUTIL.constructMiniBar(miniBarCell, frameworkHeight, workspaceWidth);
    }

    RIUTIL.constructWorkspace(workspaceCell, RIUTIL.layout, webMenuBar, e1FormDiv);
}

// Private: resize the framework of CafeOne
RIUTIL.resizeFramework = function (parentNode, workspaceWidth, frameworkHeight) {
    var table = RIUTIL.getFrameworkDoc().getElementById(RIUTIL.ID_RI_FRAMEWOKRK);
    var workspaceCell = table.rows[0].cells[0];

    if (RIUTIL.miniContainerIds.length > 0) {
        var miniBarCell = table.rows[0].cells[1];
        RIUTIL.resizeMiniBar(miniBarCell, frameworkHeight, workspaceWidth);
    }

    RIUTIL.resizeWorkspace(workspaceCell, RIUTIL.layout);
}

// Private: construct Mini bar
RIUTIL.constructMiniBar = function (parentNode, frameworkHeight, workspaceWidth) {
    var document = RIUTIL.getFrameworkDoc();
    var table = RIUTIL.createTable();
    parentNode.appendChild(table);

    var tabDistance = RIUTIL.calculateTabDistance(frameworkHeight, RIUTIL.miniContainerIds);
    var tabLeft = RIUTIL.calculateTabLeft(workspaceWidth, tabDistance)

    for (var i = 0; i < RIUTIL.miniContainerIds.length; i++) {
        var containerId = RIUTIL.miniContainerIds[i];
        var container = RIUTIL.getContainer(containerId);
        var row = table.insertRow(-1);
        var cell = row.insertCell(-1);
        var name = container.frame ? (container.frame.name ? container.frame.name : container.frame.description) : RIUTIL.CONSTANTS.RI_DELETED_CONTENT;
        var description = "";
        if (container.frame) {
            if (container.frame.description)
                description = container.frame.description;
            else
                description = container.frame.name;
        }
        else {
            description = RIUTIL.CONSTANTS.RI_DELETED_CONTENT;
        }

        cell.title = description + "\n" + name;
        cell.containerId = containerId;
        RIUTIL.addEvent(cell, "click", RIUTIL.goNormalize);
        var div = document.createElement("div");
        div.id = "MiniTab" + containerId;
        RIUTIL.attachHoverEffect(div, "RITab RIRotation RINoWrapVetical", "RITabHover RIRotation RINoWrapVetical");
        var tabWidth = RIUTIL.calculateTabWidth(tabDistance);
        var tabTop = RIUTIL.calculateTabTop(tabDistance, i);

        div.style.width = tabWidth + "px";
        div.style.left = tabLeft + "px";
        div.style.top = tabTop + "px";

        cell.appendChild(div);
        var span = document.createElement("span");

        div.appendChild(span);
        span.appendChild(document.createTextNode(description));
        if (RIUTIL.ACCESSIBILITY && container.frame && container.frame.name) {
            span.setAttribute("role", "link");
            span.title = RIUTIL.CONSTANTS.RI_NORMAL_CONTENT + " - " + name;
            span.setAttribute("aria-label", span.title);
        }
    }
}

// Private: resize Mini bar
RIUTIL.resizeMiniBar = function (parentNode, frameworkHeight, workspaceWidth) {
    var table = parentNode.lastChild;

    var tabDistance = RIUTIL.calculateTabDistance(frameworkHeight, RIUTIL.miniContainerIds);
    var tabLeft = RIUTIL.calculateTabLeft(workspaceWidth, tabDistance)

    for (var i = 0; i < RIUTIL.miniContainerIds.length; i++) {
        var containerId = RIUTIL.miniContainerIds[i];
        var container = RIUTIL.getContainer(containerId);
        var row = table.rows[i];
        var cell = row.cells[0];
        var div = cell.firstChild;
        var tabWidth = RIUTIL.calculateTabWidth(tabDistance);
        var tabTop = RIUTIL.calculateTabTop(tabDistance, i);

        div.style.width = tabWidth + "px";
        div.style.left = tabLeft + "px";
        div.style.top = tabTop + "px";
    }
}

// Private: get tab left based on the workspace width and tab height
RIUTIL.calculateTabLeft = function (workspaceWidth, tabDistance) {
    return RIUTIL.isRTL ? -(isIE8OrEarlier ? 0 : tabDistance / 2 - RIUTIL.MINI_BAR_RIGHT_PADDING) : (workspaceWidth - (isIE8OrEarlier ? 0 : tabDistance / 2 - RIUTIL.MINI_BAR_RIGHT_PADDING));
}

// Private: get the top based on the workspace width and tab height
RIUTIL.calculateTabTop = function (tabDistance, index) {
    var document = RIUTIL.getFrameworkDoc();
    var tabTop = isIE8OrEarlier ? RIUTIL.MINI_BAR_TOP_PADDING : (tabDistance / 2 + 12); //IE and FF need to adjust differently because rotation offsets are different 

    if (RIUTIL.isCompositePage || document.getElementById(RIUTIL.ID_MODELESS_TAB_DIV)) {
        tabTop -= 22;
    }
    // shift down a little for Composite page as app so that close icon can be clicked.
    if (RIUTIL.isCompositePageAsApp)
        tabTop += RIUTIL.IndependentAppHeaderHeight;

    tabTop += tabDistance * index;

    if (RIUTIL.isNativeContainer) {
        tabTop += RIUTIL.MINI_BAR_TOP_PADDING_NC_ADJ;
    }

    return tabTop;
}

// Private: get tab distance based on the workspace heiht and list
RIUTIL.calculateTabDistance = function (frameworkHeight, miniContainerIds) {
    return Math.min(RIUTIL.MINI_TAB_MAX_HEIGHT, frameworkHeight / miniContainerIds.length);
}

// Private: get tab width based on tab distance
RIUTIL.calculateTabWidth = function (tabDistance) {
    return tabDistance - (isIE8OrEarlier ? 2 : 4);
}

// Priviate: return true for complete minimized subcontainer, otherwise, reture false
RIUTIL.adjustAllConatinersPercentage = function (container) {
    if (RIUTIL.isMinimized(container) || RIUTIL.isFilteredOut(container)) // collpase the frame with minimized or filterout
    {
        container.collapse = true;
        return container.collapse;
    }

    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            var cumulatedReducedPercentage = 0;
            // go over all sub containers and collected the cumulated reduced percentage before we allocate 0 percentage to them
            if (container.subContainers) {
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    RIUTIL.adjustAllConatinersPercentage(subContainer);
                    if (subContainer.collapse) {
                        cumulatedReducedPercentage += subContainer.percentage;
                        subContainer.precentage = 0;
                    }
                }

                // go over all no minimized containter and increase their percentage based how much we saved 
                container.collapse = true;
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (!subContainer.collapse) {
                        container.collapse = false;
                        subContainer.percentage *= 100 / (100 - cumulatedReducedPercentage);
                    }
                }
            }
            break;

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            container.collapse = false;
            break;
    }

    return container.collapse;
}

// Priviate: calcuatel the demension of all the subcontainers based on the given max height and width for the current container.
RIUTIL.calculateAllContainerSize = function (maxHeight, maxWidth, container) {
    if (!container || container.collapse) {
        container.height = 0;
        container.width = 0;
        return;
    }

    container.height = maxHeight;
    container.width = maxWidth;

    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
            {
                // go through all no minimized subcontainers
                maxHeight -= RIUTIL.getResizeBarSize(container);
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (!subContainer.collapse) {
                        var subMaxHeight = maxHeight * subContainer.percentage / 100;
                    }
                    RIUTIL.calculateAllContainerSize(subMaxHeight, maxWidth, subContainer, container.layoutType);
                }
                break;
            }

        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            {
                maxWidth -= RIUTIL.getResizeBarSize(container);
                // go through all no minimized subcontainers
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (!subContainer.collapse) {
                        var subMaxWidth = maxWidth * subContainer.percentage / 100;
                    }
                    RIUTIL.calculateAllContainerSize(maxHeight, subMaxWidth, subContainer, container.layoutType);
                }
                break;
            }

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            break;
    }
}

// Private: return the size of resize bar in px, return 0, if do need it
RIUTIL.getResizeBarSize = function (container) {
    var totalNormalizedContainer = 0;
    for (var i = 0; i < container.subContainers.length; i++) {
        var subContainer = container.subContainers[i];
        if (!subContainer.collapse) {
            totalNormalizedContainer++;
        }
    }
    return (totalNormalizedContainer - 1) * RIUTIL.RESIZE_DIV_SIZE;
}

// Private: tear down the existing cafeone framework before recreate it
RIUTIL.cleanupFramework = function (container) {
    var document = RIUTIL.getFrameworkDoc();
    if (!container)
        return;

    RIUTIL.cleanupWorkspace(container);

    var framework = document.getElementById(RIUTIL.ID_RI_FRAMEWOKRK);
    if (framework)
        framework.parentNode.removeChild(framework);
}

// Private: tear down the existing cafeone workspace before recreate it
RIUTIL.cleanupWorkspace = function (container) {
    if (!container)
        return;

    if (container.subContainers) {
        for (var i = 0; i < container.subContainers.length; i++) {
            var subContainer = container.subContainers[i];
            RIUTIL.cleanupWorkspace(subContainer);
        }
    }

    // clean up iframe
    var frame = RIUTIL.getRIPaneIFRAME(container.id);
    if (frame) {
        var frame = frame.parentNode.removeChild(frame);
    }

    // clean up RIPaneDiv
    var riPaneDiv = RIUTIL.getRIPaneDiv(container.id);
    if (riPaneDiv)
        riPaneDiv.parentNode.removeChild(riPaneDiv);

    // clean up layout table
    var layoutTable = document.getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + container.id);
    if (layoutTable)
        layoutTable.parentNode.removeChild(layoutTable);
}

// Private: build up the container by parent first order following the hierarch of the container tree
RIUTIL.constructWorkspace = function (parentNode, container, webMenuBar, e1FormDiv) {
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
            {
                var layoutTB = RIUTIL.constructWorkspaceTable(parentNode, container);
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (subContainer.collapse)
                        continue;

                    if (needResizeCell) // resize cell
                    {
                        var layoutRow = RIUTIL.constructWorkspaceRow(layoutTB, RIUTIL.RESIZE_DIV_SIZE + "px");
                        var layoutCell = RIUTIL.constructResizeCell(layoutRow, container, subContainer.width, i);
                    }

                    var layoutRow = RIUTIL.constructWorkspaceRow(layoutTB);
                    var layoutCell = RIUTIL.constructWorkspaceCell(layoutRow);

                    RIUTIL.constructWorkspace(layoutCell, subContainer, webMenuBar, e1FormDiv);
                    var needResizeCell = true;
                }
                break;
            }

        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            {
                var layoutTB = RIUTIL.constructWorkspaceTable(parentNode, container);
                var layoutRow = RIUTIL.constructWorkspaceRow(layoutTB);
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (subContainer.collapse)
                        continue;

                    if (needResizeCell) // resize cell
                    {
                        var layoutCell = RIUTIL.constructResizeCell(layoutRow, container, subContainer.height, i);
                    }

                    var layoutCell = RIUTIL.constructWorkspaceCell(layoutRow);
                    RIUTIL.constructWorkspace(layoutCell, subContainer, webMenuBar, e1FormDiv);
                    var needResizeCell = true;
                }
                break;
            }

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            {
                if (container.containerType == RIUTIL.CONATINAER_E1) // E1Form 
                    RIUTIL.constructE1PaneDiv(parentNode, container, webMenuBar, e1FormDiv);
                else // RIAF CafeOne Frame
                {
                    RIUTIL.constructRIPaneDiv(parentNode, container);
                    RIUTIL.updateMenuDisplay(container.id);
                    switch (container.mode) {
                        case RIUTIL.MODE_CREATE:
                            RIUTIL.goCreate(container.id, container.frame.pathType);
                            break;

                        case RIUTIL.MODE_EDIT:
                            RIUTIL.goEdit(container.id);
                            break;

                        case RIUTIL.MODE_DETAIL:
                            RIUTIL.goDetail(container.id);
                            break;

                        case RIUTIL.MODE_DISPLAY:
                            if (container.frame) // if user remove a frame but did not save the layout who reference it. we will get a blank frame
                            {
                                if (container.frame.hasServerAccessError) {
                                    RIUTIL.renderFrameError(container.id, RIUTIL.CONSTANTS.RI_ERROR_NO_OBIEE_ACCESS);
                                }
                                else {
                                    switch (container.containerType) {
                                        case RIUTIL.CONATINAER_FREESTYLE: // freestyle frame
                                            RIUTIL.renderFreeStyleFrameContent(container);
                                            break;

                                        case RIUTIL.CONATINAER_WL_COMPONENT: // wl component frame
                                            RIUTIL.renderWLPaneFrameContent(container);
                                            break;

                                        case RIUTIL.CONATINAER_LANDING_PAGE: // landing page frame
                                            RIUTIL.renderLPPaneFrameContent(container);
                                            break;

                                        default:
                                            var url = container.frame.url;
                                            RIUTIL.renderFrameContent(container.id, url);
                                            break;
                                    }
                                }
                            }
                            break;
                    }
                }
                break;
            }
    }
}

// Private: resizse the container by parent first order following the hierarch of the container tree
RIUTIL.resizeWorkspace = function (parentNode, container) {
    var document = RIUTIL.getFrameworkDoc();
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
            {
                var layoutTB = document.getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + container.id);
                layoutTB.style.width = container.width + "px";
                var index = 0;
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (subContainer.collapse)
                        continue;

                    if (needResizeCell) // resize cell
                    {
                        var layoutRow = layoutTB.rows[index];
                        var layoutCell = layoutRow.cells[0];
                        RIUTIL.resizeResizeCell(layoutCell, container, subContainer.width);
                        index++;
                    }

                    var layoutRow = layoutTB.rows[index];
                    var layoutCell = layoutRow.cells[0];

                    RIUTIL.resizeWorkspace(layoutCell, subContainer);
                    var needResizeCell = true;
                    index++;
                }
                break;
            }

        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            {
                var layoutTB = document.getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + container.id);
                layoutTB.style.width = container.width + "px";
                var layoutRow = layoutTB.rows[0];
                var index = 0;
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    if (subContainer.collapse)
                        continue;

                    if (needResizeCell) // resize cell
                    {
                        var layoutCell = layoutRow.cells[index];
                        RIUTIL.resizeResizeCell(layoutCell, container, subContainer.height);
                        index++;
                    }

                    var layoutCell = layoutRow.cells[index];

                    RIUTIL.resizeWorkspace(layoutCell, subContainer);
                    var needResizeCell = true;
                    index++;
                }
                break;
            }

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            {
                if (container.containerType == RIUTIL.CONATINAER_E1) // E1Form 
                    RIUTIL.resizeE1PaneDiv(parentNode, container);
                else // RIAF CafeOne Frame
                {
                    RIUTIL.resizeRIPaneDiv(parentNode, container);
                }
                break;
            }
    }
}


// Private: build up the E1 pane div
RIUTIL.constructE1PaneDiv = function (parentNode, container, webMenuBar, e1FormDiv) {
    var e1PaneDIV = document.createElement("DIV");
    e1PaneDIV.id = RIUTIL.ID_E1_PANE_DIV;
    e1PaneDIV.style.overflow = "hidden";
    e1PaneDIV.containerId = container.id;
    e1PaneDIV.style.height = Math.max(container.height, 0) + "px";
    e1PaneDIV.style.width = container.width + "px";
    if (webMenuBar) // with side panel closed
        e1PaneDIV.appendChild(webMenuBar);
    e1PaneDIV.appendChild(e1FormDiv);
    parentNode.appendChild(e1PaneDIV);
    parentNode.style.height = Math.max(container.height, 0) + "px";
}

// Private: build up the E1 pane div
RIUTIL.constructFreeStylePaneDiv = function (parentNode, container, webMenuBar, e1FormDiv) {
    var document = RIUTIL.getFrameworkDoc();
    var e1PaneDIV = document.createElement("DIV");
    e1PaneDIV.style.overflow = "hidden";
    e1PaneDIV.containerId = container.id;
    e1PaneDIV.style.height = Math.max(container.height, 0) + "px";
    e1PaneDIV.style.width = container.width + "px";
    e1PaneDIV.appendChild(e1FormDiv);
    parentNode.appendChild(e1PaneDIV);
    parentNode.style.height = Math.max(container.height, 0) + "px";
}

// Private: resize the E1 pane div
RIUTIL.resizeE1PaneDiv = function (parentNode, container) {
    var e1PaneDIV = parentNode.lastChild;
    e1PaneDIV.style.height = Math.max(container.height, 0) + "px";
    e1PaneDIV.style.width = container.width + "px";
    parentNode.style.height = Math.max(container.height, 0) + "px";
}

// Private: build a table for the container
RIUTIL.constructWorkspaceTable = function (parentNode, container) {
    var layoutTB = RIUTIL.createTable();
    layoutTB.className = RIUTIL.CLASS_LAYOUT_TABLE;
    layoutTB.id = RIUTIL.ID_RI_LAYOUT_TABLE + container.id;
    layoutTB.setAttribute('cellPadding', 0);
    layoutTB.setAttribute('cellSpacing', 0);
    layoutTB.style.width = container.width + "px";
    parentNode.appendChild(layoutTB);
    return layoutTB;
}

// Private: build a table row to hold subcontainer
RIUTIL.constructWorkspaceRow = function (parentTable, height) {
    var tr = parentTable.insertRow(-1);
    if (height) {
        tr.style.height = height;
    }
    return tr;
}

// Private: build a table cell to hold subcontainer
RIUTIL.constructWorkspaceCell = function (parentRow) {
    var td = parentRow.insertCell(-1);
    td.className = "RILayoutCell";
    return td;
}

// Private: build a resizable cell between subcontainers
RIUTIL.constructResizeCell = function (parentRow, container, length, subIndex) {
    var document = RIUTIL.getFrameworkDoc();
    var td = RIUTIL.constructWorkspaceCell(parentRow);
    var activeLayout = RIUTIL.getCurrentSelectedLayout(RIUTIL.layoutManager.currentLayoutId);

    td.containerId = container.id;
    td.subIndex = subIndex;

    // Allow resizing of content always in runtime mode.  Do not allow the re-sizing for shared and request to publish layout. 
    // Do not allow resize for mobile because the event may get triggered by IPAD as well and cause service side exception.
    var allowResize = !RIUTIL.isTouchEnabled && (!RIUTIL.layoutManager.designMode || ((RIUTIL.layoutManager.designMode && !(activeLayout.isRequestPublished || activeLayout.isPublished))))


    if (allowResize)
        RIUTIL.addEvent(td, "mousedown", RIUTIL.onMouseDownResizeEvent);

    switch (container.layoutType) {
        case RIUTIL.LAYOUT_HORIZONTAL:
            var div = document.createElement("div");
            div.style.width = RIUTIL.RESIZE_DIV_SIZE + "px";
            div.style.height = length ? (length + "px") : "100%";
            if (allowResize)
                RIUTIL.attachHoverEffect(div, "RIResizeHorizontal", "RIResizeHorizontalHover");
            td.appendChild(div);
            break

        case RIUTIL.LAYOUT_VERTICAL:
            var div = document.createElement("div");
            div.style.height = RIUTIL.RESIZE_DIV_SIZE + "px";
            div.style.width = length ? (length + "px") : "100%";
            if (allowResize)
                RIUTIL.attachHoverEffect(div, "RIResizeVertical", "RIResizeVerticalHover");
            td.appendChild(div);
            break
    }
    return td;
}

// Private: resize a resizable cell between subcontainers
RIUTIL.resizeResizeCell = function (td, container, length) {
    var div = td.firstChild;
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_HORIZONTAL:
            div.style.height = length ? (length + "px") : "100%";
            break

        case RIUTIL.LAYOUT_VERTICAL:
            div.style.width = length ? (length + "px") : "100%";
            break
    }
    return td;
}

// Private: build a subcontainer to hold generic URL
RIUTIL.constructRIPaneDiv = function (parentNode, container) {
    var riPaneDIV = RIUTIL.createRIPaneDIV(container);

    // riMenuTable
    var riMenuTable = document.getElementById(RIUTIL.ID_RI_MENU_TABLE + container.id);
    if (riMenuTable)
        riMenuTable.style.width = container.width - 2 + "px";
    parentNode.appendChild(riPaneDIV);
    parentNode.style.height = container.height + "px"
}

// Private: resize a subcontainer to hold generic URL
RIUTIL.resizeRIPaneDiv = function (parentNode, container) {
    var document = RIUTIL.getFrameworkDoc();
    var containerId = container.id;

    // resizse container div
    parentNode.style.height = container.height + "px";

    // resizse RIPaneDiv
    var riPaneDIV = document.getElementById(RIUTIL.ID_RI_PANE_DIV + containerId);
    riPaneDIV.style.width = container.width + "px";
    riPaneDIV.style.height = container.height + "px";

    // Resize Menu table
    var riMenuTable = document.getElementById(RIUTIL.ID_RI_MENU_TABLE + containerId);
    if (riMenuTable)
        riMenuTable.style.width = container.width - 2 + "px";

    var paneTitle = document.getElementById("paneTitle" + containerId);
    if (paneTitle)
        paneTitle.style.width = container.width - RIUTIL.MENU_ICON_DISCOUNT + "px";

    // resizse iframe
    var frame = document.getElementById(RIUTIL.ID_RI_PANE_FRAME + containerId);
    var contentMenuHeight = RIUTIL.getFrameTitleBarHeight(container);
    var contentHeight = container.height - contentMenuHeight;
    frame.style.height = contentHeight + "px";
    frame.style.width = container.width + "px";

    if (RIUTIL.isIOS) //iOS has a parent holder div which is required to enable scrolling in content panes on iPad
    {
        frame.parentNode.style.height = contentHeight + "px";
        frame.parentNode.style.width = container.width + "px";
    }
}

// Public: called by JSCompoent to retrieve the E1 size 
RIUTIL.caculateE1Size = function () {
    var e1Container = RIUTIL.getE1Container(RIUTIL.layout);
    return { width: e1Container.width, height: e1Container.height };
}

// Private: search in the layout to find container for the E1 form
RIUTIL.getE1Container = function (container) {
    if (container && container.containerType == RIUTIL.CONATINAER_E1)
        return container

    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL:
        case RIUTIL.LAYOUT_HORIZONTAL:
            {
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    var result = RIUTIL.getE1Container(subContainer);
                    if (result)
                        return result;
                }
            }
        default:
            return null;
    }

    return null;
}

// Private: Render RIPaneDiv containing Title Bar, Menu Bar and Iframe
RIUTIL.createRIPaneDIV = function (container) {
    var containerId = container.id;
    var riPaneDIV = document.createElement("DIV");
    riPaneDIV.id = RIUTIL.ID_RI_PANE_DIV + containerId;
    riPaneDIV.className = "RIPaneDiv";
    riPaneDIV.style.width = container.width + "px";
    riPaneDIV.style.height = container.height + "px";

    var table = RIUTIL.createTable();
    table.setAttribute('cellPadding', 0);
    table.setAttribute('cellSpacing', 0);
    table.id = RIUTIL.ID_RI_LAYOUT_TABLE + containerId;
    table.className = RIUTIL.CLASS_LAYOUT_TABLE;
    table.style.width = "100%";
    table.style.height = "100%";

    riPaneDIV.appendChild(table);

    if (RIUTIL.hasFrameTitleBar(container)) // hide menubar for runtime version of composite page withtout minimizeSupport
    {
        //(2) TR for menu bar
        var trMenu = table.insertRow(-1);
        trMenu.style.height = "20px";
        trMenu.id = RIUTIL.ID_RI_MENU_BAR + containerId;
        trMenu.appendChild(RIUTIL.createMenuBarTD(container));
    }

    // do not render content tr if window state is minimized
    if (RIUTIL.isMinimized(container))
        return riPaneDIV;

    //(3) TR for iframe
    var trIframe = table.insertRow(-1);
    trIframe.className = RIUTIL.isIOS ? "TabBody tabbody" : "TabBody";
    if (RIUTIL.isIOS) {
        trIframe.style.border = 'none';
    }
    var tdIframe = trIframe.insertCell(-1);
    tdIframe.style.borderStyle = "solid";
    tdIframe.style.borderWidth = "1px";
    tdIframe.style.borderColor = "#E8F3FD";
    tdIframe.style.borderTopWidth = "0px";
    tdIframe.colSpan = 17;
    var frameName = container.frame.name;

    var src = "";
    var contentMenuHeight = RIUTIL.getFrameTitleBarHeight(container);
    var iframeHeight = container.height - contentMenuHeight;

    var iframeName = "RIFrame" + RIUTIL.layoutManager.currentLayoutId.replace(/\(/g, "").replace(/\)/g, "") + '_' + containerId;
    // give the frame a name help ADFContainer to preserve the ADF application state for the next re-visit
    if (!RIUTIL.isIOS)
        tdIframe.innerHTML = '<iframe name="' + iframeName + '" id="' + RIUTIL.ID_RI_PANE_FRAME + containerId + '" src="' + src + '" frameborder="0" width="100%" height="' + iframeHeight + 'px" title="' + frameName + '"></iframe>';
    else // for IPAD, we need to specify the height and width of the frame
    {
        tdIframe.innerHTML = '<div style=\"border: 1px solid #C4CED7; overflow: scroll;-webkit-overflow-scrolling:touch;height:' + iframeHeight + 'px; width:' + container.width + 'px;\"><iframe name="' + iframeName + '" id="' + RIUTIL.ID_RI_PANE_FRAME + containerId + '" src="' + src + '" frameborder="0" style="height:' + iframeHeight + 'px;width:' + container.width + 'px;overflow:scroll;-webkit-overflow-scrolling:touch;"></iframe></div>';
    }

    return riPaneDIV;
}

// Private: get the content frame menu height 
RIUTIL.getFrameTitleBarHeight = function (container) {
    return RIUTIL.hasFrameTitleBar(container) ? RIUTIL.PANE_MENU_HEIGHT : 0;
}

// Private: always true to support max at least
RIUTIL.hasFrameTitleBar = function (container) {
    return true;
}

// Private: create menu bar td tag contain create, close, save, detail, delete, edit and pane title area
RIUTIL.createMenuBarTD = function (container) {
    var document = RIUTIL.getFrameworkDoc();
    var containerId = container.id;
    var pathType = container.frame.pathType;
    var td = document.createElement("TD");

    var table = RIUTIL.createTable();
    table.id = RIUTIL.ID_RI_MENU_TABLE + containerId;
    table.setAttribute('cellPadding', 0);
    table.setAttribute('cellSpacing', 0);
    var isMimimized = RIUTIL.isMinimized(container);

    if (RIUTIL.layoutManager.designMode) // design mode frame
    {
        if (RIUTIL.layoutManager.allowChangeLayout)
            RIUTIL.attachHoverEffect(table, "RILayoutTable RIMenuTableDesign", "RILayoutTable RIMenuTableHover");
        else
            table.className = "RILayoutTable RIMenuTableDesign";
    }
    else // runtime mode frame
    {
        if (RIUTIL.isCompositePage)
            table.className = "RILayoutTable CompositeTitleBar";
        else
            table.className = RIUTIL.hasGridLevelParameter(container) ? "RILayoutTable RIMenuTableDynamic" : "RILayoutTable CompositeTitleBar";
    }

    table.style.height = "20px";
    table.style.borderWidth = "0px";

    td.appendChild(table);
    var tbody = document.createElement("TBODY");
    table.appendChild(tbody);
    var tr = document.createElement("TR");
    tr.setAttribute("valign", "center");
    tbody.appendChild(tr);

    //(1) space and create
    tr.appendChild(RIUTIL.createPaddingTD("objCreateSpace" + containerId));
    tr.appendChild(RIUTIL.createImageTD(container, "objCreate", top['E1RES_share_images_ulcm_createlink_ena_png'], RIUTIL.getMenuItemHandler("goCreate", containerId, pathType), RIUTIL.CONSTANTS.RI_HOVER_TEXT_CREATE, false, true, 20));

    //(2) space and close
    tr.appendChild(RIUTIL.createPaddingTD("objBackSpace" + containerId));
    tr.appendChild(RIUTIL.createImageTD(container, "objBack", top['E1RES_share_images_ulcm_cancel_ena_png'], RIUTIL.getMenuItemHandler("goBack", containerId, pathType), RIUTIL.CONSTANTS.RI_HOVER_TEXT_CANCEL, false, true, 20));

    //(3) space and save
    tr.appendChild(RIUTIL.createPaddingTD("objSaveSpace" + containerId));
    tr.appendChild(RIUTIL.createImageTD(container, "objSave", top['E1RES_share_images_ulcm_save_ena_png'], RIUTIL.getMenuItemHandler("goSaveObj", containerId, pathType), RIUTIL.CONSTANTS.RI_HOVER_TEXT_SAVE, false, true, 20));

    //(4) space and detail
    tr.appendChild(RIUTIL.createPaddingTD("objDetailSpace" + containerId));
    if (RIUTIL.isCompositePage) // for composite page, the detail and edit are present with the same icon but only one of them will show up
        tr.appendChild(RIUTIL.createImageTD(container, "objDetail", top['E1RES_share_images_ulcm_edit_ena_png'], RIUTIL.getMenuItemHandler("goDetail", containerId, pathType), RIUTIL.CONSTANTS.RI_HOVER_TEXT_INSPECT, false, true, 20));
    else
        tr.appendChild(RIUTIL.createImageTD(container, "objDetail", top['E1RES_share_images_ulcm_info_ena_png'], RIUTIL.getMenuItemHandler("goDetail", containerId, pathType), RIUTIL.CONSTANTS.RI_HOVER_TEXT_DETAIL, false, true, 20));

    //(5) space and edit
    tr.appendChild(RIUTIL.createPaddingTD("objEditSpace" + containerId));
    tr.appendChild(RIUTIL.createImageTD(container, "objEdit", top['E1RES_share_images_ulcm_edit_ena_png'], RIUTIL.getMenuItemHandler("goEdit", containerId, pathType), (RIUTIL.isCompositePage ? RIUTIL.CONSTANTS.RI_HOVER_TEXT_INSPECT : RIUTIL.CONSTANTS.RI_HOVER_TEXT_EDIT), false, true, 20));

    //(6) space and title
    tr.appendChild(RIUTIL.createPaddingTD("paneTitleSpace" + containerId));
    var container = RIUTIL.getContainer(containerId);
    if (container && container.frame)
        var title = RIUTIL.HTMLDecoder(container.frame.description);

    var paneTitleTD = RIUTIL.createTitleTD(containerId, title);
    tr.appendChild(paneTitleTD);

    //(7) space and popup
    tr.appendChild(RIUTIL.createPaddingTD("objPopupSpace" + containerId));
    var maxHandler = RIUTIL.getMaximizeHandler(container);
    tr.appendChild(RIUTIL.createImageTD(container, "objPopup", top['E1RES_img_alta_grid_func_maximize_16_ena_png'], RIUTIL.getMenuItemHandler(maxHandler, containerId, pathType), RIUTIL.CONSTANTS.RI_HOVER_TEXT_POPUP, false, true, 20));

    //(8) space and unfloat
    tr.appendChild(RIUTIL.createPaddingTD("objUnfloatSpace" + containerId));
    var eventHandler = RIUTIL.getMenuItemHandler("goUnfloat", containerId, pathType);
    tr.appendChild(RIUTIL.createImageTD(container, "objUnfloat", top['E1RES_img_alta_grid_func_shuttlecopyvertical_16_ena_png'], eventHandler, RIUTIL.CONSTANTS.RI_NORMAL_CONTENT, false, true, 20));

    //(9) space and deactivate
    tr.appendChild(RIUTIL.createPaddingTD("objDeactivateSpace" + containerId));
    var eventHandler = RIUTIL.supportAnimation ? RIUTIL.getMenuItemHandler("goDeactivateBegin", containerId, pathType) : RIUTIL.getMenuItemHandler("goDeactivate", containerId, pathType);
    tr.appendChild(RIUTIL.createImageTD(container, "objDeactivate", top["E1RES_share_images_ulcm_delete_ena_png"], eventHandler, RIUTIL.CONSTANTS.RI_DELETE_CONTENT, true, true, 20));

    //(10) space and minimize
    tr.appendChild(RIUTIL.createPaddingTD("objMinimizeSpace" + containerId));
    var eventHandler = RIUTIL.supportAnimation ? RIUTIL.getMenuItemHandler("goMinimizeWithDirtyCheck", containerId, pathType) : RIUTIL.getMenuItemHandler("goMinimize", containerId, pathType);
    tr.appendChild(RIUTIL.createImageTD(container, "objMinimize", top['E1RES_share_images_alta_func_windowarrow_16_ena'], eventHandler, RIUTIL.CONSTANTS.RI_MINI_CONTENT, !RIUTIL.isCompositePage, true, 20));

    //(11) close icon for inpsect dialog of composite content
    tr.appendChild(RIUTIL.createPaddingTD("objCloseSpace" + containerId));
    var eventHandler = RIUTIL.getMenuItemHandler("goCloseDialog", containerId, pathType);
    tr.appendChild(RIUTIL.createImageTD(container, "objClose", top["E1RES_img_jdeclose_ena_png"], eventHandler, RIUTIL.CONSTANTS.JH_CLOSE, false, true, 20));

    return td;
}

// Private: reusable event handler builder given method of handler.
RIUTIL.getMenuItemHandler = function (method, containerId, type) {
    var prefix = RIUTIL.isCompositePage && !RIUTIL.isCompositePageAsApp ? "top.RIUTIL." : "RIUTIL.";
    return prefix + method + "(" + containerId + "," + type + ")";
}

// Private: get maximize event handler
RIUTIL.getMaximizeHandler = function (container) {
    var isADFFrame = container.frame.pathType == RIUTIL.TYPE_ADF_FRAME;
    return RIUTIL.isCompositePage || isADFFrame ? "goFloat" : "goPopup";
}

RIUTIL.shouldFloatOnMaximize = function (container) {
    var isADFFrame = container.frame.pathType == RIUTIL.TYPE_ADF_FRAME;
    return RIUTIL.isCompositePage || isADFFrame;
}

// Private: create clickable/non-clickable visble/hidden image icon on RI toolbar.
// id: the dom id
// src: the URL(rewritten) for image
// onclick: the event handler for click action
// title: the hover text
// display: hide or show
// width: the fixed width of the image if not null
// className: the extra stylesheet besides RILayoutCell
RIUTIL.createImageTD = function (container, idPrefix, src, onclick, title, display, visible, width, className) {
    var id = idPrefix + container.id;
    var td = document.createElement("TD");
    td.className = RIUTIL.CLASS_LAYOUT_TD + " " + (className ? className : "");

    var div = document.createElement("DIV");
    td.appendChild(div);
    div.id = id;
    div.style.display = display ? "inline" : "none";
    div.style.visibility = visible ? "visible" : "hidden";

    var size = ""; //hack for larger processing image
    if (id.indexOf("sepProcessIndicator") == 0)
        size = " width=16 height=16 ";

    var altText = title;
    if (RIUTIL.ACCESSIBILITY && container.frame && container.frame.name) {
        altText = title + " - " + container.frame.name;
    }

    var html = '<img border="0" role="link" valign="bottom" id=' + id + 'img    src="' + src + '"' + size + ((onclick && !RIUTIL.isTouchEnabled) ? ' onmouseover="showMotion(this)" onmouseout="removeMotion(this)"' : ' ') + ((onclick) ? ((!RIUTIL.isTouchEnabled) ? ('" onclick="' + onclick + '"') : (' ontouchend="' + onclick + '"')) : '') + 'title="' + title + '" alt="' + altText + '"/>';
    div.innerHTML = html;

    if (width)
        div.width = width;
    return td;
}

// Private: Render 10px padding cell without collapse
RIUTIL.createPaddingTD = function (id, className) {
    var td = document.createElement("TD");
    td.innerHTML = '<div id="' + id + '"' + ' style="padding-left:5px"><img src="' + top['E1RES_img_RI_transp_gif'] + '"/></div>';
    td.className = "RIBlankCell";
    if (className)
        td.className = " " + className;
    return td;
}

// Private: create a message area with default text
RIUTIL.createTitleTD = function (containerId, text) {
    var activeLayout = RIUTIL.getCurrentSelectedLayout(RIUTIL.layoutManager.currentLayoutId);
    var container = RIUTIL.getContainer(containerId);
    var isMimimized = RIUTIL.isMinimized(container);
    var td = document.createElement("TD");
    td.width = "100%";
    td.align = "center";

    // Do not allow the drag and drop for shared / request to publish layout.
    var allowDrag = RIUTIL.layoutManager.designMode && !(activeLayout.isRequestPublished || activeLayout.isPublished) && RIUTIL.getLeafContainerCount(RIUTIL.layout) > 1

    if (allowDrag)
        td.className = "RIDragable RINoWrap";
    else
        td.className = RIUTIL.isIOS ? "RINoWrap rinowrap" : "RINoWrap";

    td.containerId = containerId;

    if (allowDrag) {
        RIUTIL.addEvent(td, "mousedown", RIUTIL.onMouseDownDragEvent);
    }

    var div = document.createElement("div");
    td.appendChild(div);
    div.id = "paneTitle" + containerId;
    div.className = RIUTIL.isIOS ? "RINoWrap rinowrap" : "RINoWrap";
    div.style.width = Math.max(container.width - RIUTIL.MENU_ICON_DISCOUNT, 0) + "px"; // discount the 2 icons (>= 0) and left is for the text
    div.appendChild(document.createTextNode(text));
    div.title = text;

    return td;
}

// Private: jaggle the iframe to allow the thrid part to adjust the size of the content (like google map)
RIUTIL.repaintIframe = function (cotnainerId) {
    var riPaneIFRAME = RIUTIL.getRIPaneIFRAME(cotnainerId);
    if (riPaneIFRAME) {
        riPaneIFRAME.style.width = Math.max(0, riPaneIFRAME.offsetWidth - 1) + "px";
        riPaneIFRAME.style.width = riPaneIFRAME.offsetWidth + 1 + "px";
    }
}

// Private: show and hide a key indicator with a given controlId of a E1 text field control
RIUTIL.updateControlIndicator = function (controlId, className, highlight) {
    var keyControl = document.getElementById(controlId);
    if (keyControl) {
        GUTIL.updateClassInfo(keyControl, className, highlight);
    }
}

// Private: show and hide all key indicator on E1 pane.
RIUTIL.setCaptureMode = function (captureMode, inAllForms) {
    var e1PaneIFRAME = RIUTIL.getE1PaneIFrame();
    if (!e1PaneIFRAME)
        return;

    // do nothing if change from hidden to hidden
    if (RIUTIL.captureMode == RIUTIL.NO_CAPTURE && captureMode == RIUTIL.NO_CAPTURE)
        return;

    // reset all keys on the client side of change from no-capture to capture mode
    if (RIUTIL.captureMode == RIUTIL.NO_CAPTURE && captureMode == RIUTIL.KEY_CAPTURE)
        RIUTIL.unregisterAllKeys();

    RIUTIL.captureMode = captureMode;

    // update start and stop icon
    RIUTIL.updateKeyPickerIcon();

    // for the Master form
    RIUTIL.updateFormView();

    // for all satellite form 
    if (inAllForms) {
        RIUTIL.updateViewForAllSubContainers(RIUTIL.getContainer(RIUTIL.layout.id), captureMode);
    }
}


RIUTIL.updateViewForAllSubContainers = function (container, captureMode) {
    if (container.subContainers) // composite container
    {
        for (var i = 0; i < container.subContainers.length; i++) {
            RIUTIL.updateViewForAllSubContainers(container.subContainers[i], captureMode);
        }
    }
    else // leaf container
    {
        if (RIUTIL.isE1SatelliteContainer(container)) {
            var containerId = container.id;
            var contentWindow = RIUTIL.getContainerWinObject(containerId);
            if (contentWindow.RIUTIL) {
                contentWindow.RIUTIL.captureMode = captureMode;
                contentWindow.RIUTIL.updateFormView();
                contentWindow.RIUTIL.updateAppMenuView();
            }
        }
    }
}

// Private: update the start key capture and stop key capture icon based on model
RIUTIL.updateKeyPickerIcon = function () {
    var containerId = RIUTIL.inspectedContainerId;
    if (!containerId)
        return;
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var container = RIUTIL.getContainer(containerId);
    var isEditOrCreate = container.mode == RIUTIL.MODE_CREATE || container.mode == RIUTIL.MODE_EDIT;
    var showStartIcon = !(RIUTIL.captureMode == RIUTIL.KEY_CAPTURE) && isEditOrCreate;
    var showStopIcon = RIUTIL.captureMode == RIUTIL.KEY_CAPTURE && isEditOrCreate;
    RIUTIL.setDisplayById(doc, "keyPickerStart", showStartIcon);
    RIUTIL.setDisplayById(doc, "keyPickerStop", showStopIcon);
}

// Private: set a flag in the model to indicate if there is any E1 key on the inspect tab page.
// the client uses this information to determine to display the key table or not
RIUTIL.setTransactionKeyDefined = function (containerId, b) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    if (doc) {
        RIUTIL.setDisplayById(doc, "divKeyTable", b);
        RIUTIL.setDisplayById(doc, "noKeyText", !b);
    }
}

// Private: clean up all the existing key information table
RIUTIL.resetKeyInfo = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    RIUTIL.keyControls = new Array();
    var keyTable = doc.getElementById("keyTable");

    if (!keyTable)
        return;
    var keyTableBody = keyTable.lastChild; // for firefox, there are two nodes, tbody is the last one; for IE only one node.
    RIUTIL.removeAllChildren(keyTableBody, 1);
    RIUTIL.setTransactionKeyDefined(false);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_RESET_KEY_CAPTURE);
}

// Private: update the view (active page stub and inactive page stub) based on the model
RIUTIL.updateTabPageFilterOption = function (containerId, hasTabPageFilter) {
    // update the radio button
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var options = doc.getElementsByName("tabPageFilterOption");
    options[0].checked = !hasTabPageFilter;
    options[1].checked = hasTabPageFilter;

    // update the visibility of filter capture controls
    RIUTIL.setDisplayById(doc, "tabPageFitlerCaptureContorls", hasTabPageFilter);
    RIUTIL.updateKeyPickerIcon();
}

// Private: delete drag indicator
RIUTIL.removeDragIndicator = function () {
    if (RIUTIL.dragIndicator) {
        RIUTIL.getFrameworkDoc().body.removeChild(RIUTIL.dragIndicator);
        RIUTIL.dragIndicator = null;
    }
}

// Private: change the display of drag indicator
RIUTIL.setDragIndicatorDisplay = function (show) {
    var e = RIUTIL.getDragIndicator();
    e.style.display = (show) ? "inline" : "none";
}

// Private: move Drag Tab Indicator to new position
RIUTIL.moveDragIndicator = function (x, y) {
    var moveRegion = RIUTIL.getDragIndicator();
    moveRegion.moveOutside = RIUTIL.layoutNewRegion(RIUTIL.layout, moveRegion, x, y);

    moveRegion.style.left = moveRegion.left + "px";
    moveRegion.style.top = moveRegion.top + "px";
    moveRegion.style.width = moveRegion.width + "px";
    moveRegion.style.height = moveRegion.height + "px";
    return moveRegion.moveOutside;
}

// Private: get the demotion from the give tag
RIUTIL.getRegion = function (containerTag) {
    var region = new Object();
    region.left = getAbsoluteLeftPos(containerTag);
    region.top = getAbsoluteTopPos(containerTag);
    region.width = containerTag.offsetWidth;
    region.height = containerTag.offsetHeight;
    return region;
}

// Private: update the E1 form view based on the model
RIUTIL.updateFormView = function () {
    if (!window.JSCompMgr)
        return;

    // update indicator for condition enabled control and QBE
    var rootContainer = JSCompMgr.getRootContainer(RIUTIL.namespace);
    rootContainer.updateUIForTextField(RIUTIL.updateFormViewAddIconForContorl);
    var jdeGrids = JDEDTAFactory.getInstance(RIUTIL.namespace).jdeGrids;
    if (jdeGrids && jdeGrids.length > 0) {
        for (var i = 0; i < jdeGrids.length; i++) {
            gridObj = jdeGrids[i];
            if (gridObj)
                RIUTIL.updateFormViewAddIconForGrid(gridObj.gridId);
        }
    }
}

// Private: update the E1 form menu view based on the model
RIUTIL.updateAppMenuView = function () {
    var menuIdList = RIUTIL.FILE_EXIT_ID_LIST;
    for (var i = 0; i < menuIdList.length; i++) {
        var clientId = menuIdList[i];
        var menuImg = document.getElementById(clientId);
        if (!menuImg)
            continue;

        var addIconId = RIUTIL.ID_ADD_ICON + clientId;
        var addIcon = document.getElementById(addIconId);
        if (addIcon) {
            addIcon.parentNode.removeChild(addIcon);
        }

        var show = RIUTIL.captureMode == RIUTIL.MENU_ITEM_CAPTURE;
        if (show)
            RIUTIL.renderAddIcon(menuImg, addIconId, clientId, 'menu');
    }
}

// Private: update the add icons for the grid on the form
RIUTIL.updateFormViewAddIconForGrid = function (gridId) {
    var gridObj = GCMH.findGrid(gridId);
    if (!gridObj)
        return;

    var show = RIUTIL.captureMode == RIUTIL.PARAM_CAPTURE || RIUTIL.captureMode == RIUTIL.KEY_CAPTURE || RIUTIL.captureMode == RIUTIL.VISIBLE_FIELD_CAPTURE || RIUTIL.captureMode == RIUTIL.EDITABLE_FIELD_CAPTURE;
    var columns = gridObj.columns;
    for (var i = 0; i < columns.length; i++) {
        var column = columns[i];
        if ((column.isText || column.isHyperlink) && !column.isHidden() && !column.isDataMasked && !column.isSecurityDisabled)
            RIUTIL.updateFormViewAddIconForGridColumn(gridId, column.colIndex, show);
    }
}

// Private: support controls on both power form and sbu form level
RIUTIL.isCafeOneEnabledContorl = function (controlId) {
    return true;
}

// Private: if a gridId id contain two different "_", it means it is in a subform, and we do not support subform gridId yet.
// TODO: it need to return when we beging to support grid on subform
RIUTIL.isCafeOneEnabledGrid = function (gridId) {
    //return true;
    return gridId.indexOf("_") == gridId.lastIndexOf("_");
}

// Private: update the add icons for the grid column on the form
RIUTIL.updateFormViewAddIconForGridColumn = function (gridId, colIndex, show) {
    // we do not support sub form control yet as parameter or key
    if (!RIUTIL.isCafeOneEnabledGrid(gridId))
        return;

    var gridObj = JDEDTAFactory.getInstance(RIUTIL.namespace).resolveGrid(gridId);
    if (gridObj.listViewMetadata) {
        //TODO: suppport plus in CafeOne design for list view
    }
    else {
        // clean up the icon first
        var clientId = "GC" + gridId + "." + colIndex;
        var addIconId = RIUTIL.ID_ADD_ICON + clientId;
        var addIcon = document.getElementById(addIconId);
        if (addIcon) {
            addIcon.parentNode.removeChild(addIcon);
        }

        if (!show)
            return;

        // display the icon
        var referenceTag = EUR.getReferenceElementByName(clientId);
        RIUTIL.renderAddIcon(referenceTag, addIconId, clientId, 'grid');
    }
}

// Public call back from updateUIForTextField of JSControl or JSContainer in JSComponent 
RIUTIL.updateFormViewAddIconForContorl = function (id) {
    var containerId = RIUTIL.inspectedContainerId;
    var clientId = JSConstant.pfControl + id;
    var iconId = RIUTIL.ID_ADD_ICON + clientId;

    var inputTag = document.getElementById(clientId);
    if (inputTag) // input control exist on the form
    {
        // for view detail only mode, only display check mark for all key controls registered
        if (containerId && RIUTIL.getContainer(containerId).mode == RIUTIL.MODE_DETAIL) {
            // do not show checkmark for now, which doe not have too much value but leading some inconsistence between UI
            // var show = RIUTIL.isRegisteredKey(controlId);
            // var isCheckMark = true;
        }
        // for edit mode, display green plus for key capture mode and origin plus for parameter capture mode
        else {
            var show = RIUTIL.captureMode == RIUTIL.PARAM_CAPTURE ||
                       RIUTIL.captureMode == RIUTIL.VISIBLE_FIELD_CAPTURE ||
                      (RIUTIL.captureMode == RIUTIL.EDITABLE_FIELD_CAPTURE && inputTag.readOnly == false && inputTag.disabled == false) ||
                      (RIUTIL.captureMode == RIUTIL.KEY_CAPTURE && !RIUTIL.isRegisteredKey(clientId));
        }

        if (show) // display add or check icon
        {
            // we do not support sub form control yet as parameter or key
            if (RIUTIL.isCafeOneEnabledContorl(clientId) && !document.getElementById(iconId))
                RIUTIL.renderAddIcon(inputTag, iconId, clientId, 'form');
        }
        else // hide icon
        {
            RIUTIL.reomveAddIcon(clientId)
        }

        if (document.getElementById(iconId))
            RIUTIL.addjustOffsetOfIcon(inputTag, show);
    }
}

// Private: remove the add icon for given controlId (start with "C")
RIUTIL.reomveAddIcon = function (controlId) {
    var addIconId = RIUTIL.ID_ADD_ICON + controlId;
    var addIcon = document.getElementById(addIconId);
    if (addIcon) {
        addIcon.parentNode.removeChild(addIcon);
    }
}

// Private: adjust the left shift offset for the VA icon
RIUTIL.addjustOffsetOfIcon = function (inputTag, show) {
    var vaIcon = null;
    var childList = inputTag.parentNode.childNodes;
    for (var j = 0; j < childList.length; j++) {
        if ((childList[j].name && childList[j].name.indexOf("va", 0) == 0) || childList[j].className == "JSVisualAssist") {
            vaIcon = childList[j];
            break;
        }
    }

    if (vaIcon) {
        vaIcon.style.position = "relative";
        vaIcon.style.left = (show ? RIUTIL.ICON_OFFSET_X : 0) + "px";
    }
}

// Private: set the layout of left and right section based on if the parameter capture section is displayed.
RIUTIL.updateParameterSectionDisplay = function (containerId) {
    var showParameterSection = RIUTIL.isURL || RIUTIL.pathType == RIUTIL.TYPE_OBIEE_REPORT || RIUTIL.isE1Form || RIUTIL.isADFFrame;
    var doc = RIUTIL.getRIPaneDoc(containerId);
    RIUTIL.setDisplayById(doc, "inputForParameterOption", showParameterSection);
    var keyCaptureTD = doc.getElementById("keyCaptureTD");
    keyCaptureTD.style.width = showParameterSection ? "50%" : "100%";

    var parameterCaptureTD = doc.getElementById("parameterCaptureTD");
    parameterCaptureTD.style.width = showParameterSection ? "50%" : "0%";
}

// Private: return true if find a target container out side or the source container
RIUTIL.layoutNewRegion = function (container, moveRegion, x, y) {
    if (container.isBase)
        return false;

    if (RIUTIL.fitTarget(container, moveRegion, x, y) == RIUTIL.FIT_OK)
        return true;

    RIUTIL.setDefaultSize(moveRegion, x, y);
    return false;
}

// Private: set the default region
RIUTIL.setDefaultSize = function (moveRegion, x, y) {
    // default demotion for move region
    moveRegion.left = x;
    moveRegion.top = y;
    moveRegion.width = RIUTIL.DEFAULT_FLY_FRAME_WIDTH;
    moveRegion.height = RIUTIL.DEFAULT_FLY_FRAME_HEIGHT;
}

// Private: return true, if x, y fall into the target container or its subcontainer and the demotion of moveRegion will be upated as well.
// return false if not in this container at all.
RIUTIL.fitTarget = function (container, moveRegion, x, y) {
    if (container.isBase) // base is not real visible
        return RIUTIL.FIT_OUT;

    var targetTag = RIUTIL.getContainerTag(container);
    var targetRegion = RIUTIL.getRegion(targetTag);

    // out side of the target container
    if (!RIUTIL.isInRegion(targetRegion, x, y))
        return RIUTIL.FIT_OUT;

    // target container is it source container, do not need to move
    if (moveRegion.movingContainerId == container.id)
        return RIUTIL.FIT_CLOSE;

    // inside target container but too close to the container side 
    if (RIUTIL.isTooCloseToSide(targetRegion, x, y))
        return RIUTIL.FIT_CLOSE;

    // in size of self container, and let us try all the chilren containers
    if (container.subContainers) {
        for (var i = 0; i < container.subContainers.length; i++) {
            var subContainer = container.subContainers[i];
            var fit = RIUTIL.fitTarget(subContainer, moveRegion, x, y)
            if (fit != RIUTIL.FIT_OUT) // if out side of this sub container, try other sub containers
                return fit;
        }
    }

    var isLeftBottom = (y - targetRegion.top) / (x - targetRegion.left) > (targetRegion.height / targetRegion.width);
    var isRightBottom = (y - targetRegion.top - targetRegion.height) / (x - targetRegion.left) > (-targetRegion.height / targetRegion.width);
    if (isLeftBottom) {
        if (isRightBottom) {
            // bottom
            moveRegion.left = targetRegion.left;
            moveRegion.top = y;
            moveRegion.width = targetRegion.width;
            moveRegion.height = targetRegion.height + targetRegion.top - y;
            moveRegion.targetZone = RIUTIL.ZONE_BOTTOM;
            moveRegion.percentage = RIUTIL.getPercentage(moveRegion.height, targetRegion.height);

            targetRegion.height -= moveRegion.height;
        }
        else {
            // left side
            moveRegion.left = targetRegion.left;
            moveRegion.top = targetRegion.top;
            moveRegion.width = x - targetRegion.left;
            moveRegion.height = targetRegion.height;
            moveRegion.targetZone = RIUTIL.isRTL ? RIUTIL.ZONE_RIGHT : RIUTIL.ZONE_LEFT;
            moveRegion.percentage = RIUTIL.getPercentage(moveRegion.width, targetRegion.width);
            targetRegion.left = moveRegion.width;
            targetRegion.width -= moveRegion.width;
        }
    }
    else {
        if (isRightBottom) {
            // right
            moveRegion.left = x;
            moveRegion.top = targetRegion.top;
            moveRegion.width = targetRegion.left + targetRegion.width - x;
            moveRegion.height = targetRegion.height;
            moveRegion.targetZone = RIUTIL.isRTL ? RIUTIL.ZONE_LEFT : RIUTIL.ZONE_RIGHT;
            moveRegion.percentage = RIUTIL.getPercentage(moveRegion.width, targetRegion.width);
            targetRegion.width -= moveRegion.width;
        }
        else {
            //top
            moveRegion.left = targetRegion.left;
            moveRegion.top = targetRegion.top;
            moveRegion.width = targetRegion.width;
            moveRegion.height = y - targetRegion.top;
            moveRegion.percentage = RIUTIL.getPercentage(moveRegion.height, targetRegion.height);
            moveRegion.targetZone = RIUTIL.ZONE_TOP;

            targetRegion.height -= moveRegion.height;
            targetRegion.top = moveRegion.height;
        }
    }
    moveRegion.targetContainerId = container.id;
    return RIUTIL.FIT_OK
}

// Private: return true if the point is inside of region
RIUTIL.isInRegion = function (region, x, y) {
    return (x >= region.left && x <= region.left + region.width && y >= region.top && y <= region.top + region.height);
}

// Private: return if the point is too close the edge of the region, so that we should not create new container for such location.
RIUTIL.isTooCloseToSide = function (region, x, y) {
    return Math.abs(x - region.left) < RIUTIL.CONTAINER_MIN_WIDTH ||
             Math.abs(region.left + region.width - x) < RIUTIL.CONTAINER_MIN_WIDTH ||
             Math.abs(y - region.top) < RIUTIL.CONTAINER_MIN_HEIGHT ||
             Math.abs(region.top + region.height - y) < RIUTIL.CONTAINER_MIN_HEIGHT;
}

// Private: get the percetage value (number before %)
RIUTIL.getPercentage = function (small, big) {
    return Math.round(small / big * 10000) / 100; // keep tow decimal
}

// Private: get Drag Tab Indicator (the clone tab moving along with the mouse)
RIUTIL.getDragIndicator = function () {
    return RIUTIL.dragIndicator
}

// Private: get the current active product code
RIUTIL.getActiveProduct = function () {
    return RIUTIL.activeProduct;
}

// Private: add the template part into url preview format 
RIUTIL.addURLTemplatePart = function (containerId, partId, type, baseValue, description, readonly) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var prevewForURLSpan = doc.getElementById("prevewForURLSpan");

    if (type == 1)// static
    {
        var prefixSpan = doc.createElement("span");
        prefixSpan.appendChild(doc.createTextNode(baseValue));
        prevewForURLSpan.appendChild(prefixSpan);
    }
    else // dynamic partPreview span include partPreviewBase span and partPreviewComponents span
    {
        // part preview span
        var partPreview = doc.createElement("span");
        partPreview.id = RIUTIL.getPartReviewDomID(partId);
        prevewForURLSpan.appendChild(partPreview);
        partPreview.className = RIUTIL.CLASS_PARAM_VALUE_SPAN;

        // part preview component span
        var previewComponentsSpan = doc.createElement("span");
        previewComponentsSpan.id = RIUTIL.getPartReviewComponentDomID(partId);
        previewComponentsSpan.appendChild(doc.createTextNode(""));
        partPreview.appendChild(previewComponentsSpan);

        // part preview base span
        var previewBaseSpan = doc.createElement("span");
        previewBaseSpan.id = RIUTIL.getPartReviewBaseDomID(partId);
        previewBaseSpan.appendChild(doc.createTextNode(baseValue));
        partPreview.appendChild(previewBaseSpan);
    }
}

// Private: add the template part into table format for OBIEE filter or customizable part of generic URL
RIUTIL.addParameterTablePart = function (containerId, partId, type, baseValue, description, readOnly, operator, acceptMore) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var table = doc.getElementById("paramTable");
    var newRow = table.insertRow(-1);
    newRow.id = RIUTIL.getPartRowDomID(partId);
    newRow.partId = partId;
    newRow.onmouseover = RIUTIL.onMouseOverParameterPart;
    newRow.onmouseout = RIUTIL.onMouseOutParameterPart;
    newRow.acceptMore = acceptMore;

    // description
    var newParamName = newRow.insertCell(-1);
    newParamName.appendChild(doc.createTextNode(description));

    // part optoin selection (default/map or all/filter)
    var partOptionTD = newRow.insertCell(-1);
    var partOption = RIUTIL.createPartOption(doc, type, partId, readOnly, operator);
    partOptionTD.appendChild(partOption);

    // part value table
    var partValueTD = newRow.insertCell(-1);
    partValueTD.className = RIUTIL.CLASS_LAYOUT_TD;
    var partValueTable = doc.createElement("table");
    partValueTable.className = RIUTIL.CLASS_LAYOUT_TABLE;
    partValueTD.appendChild(partValueTable);
    partValueTable.partId = partId;
    var partValueRow = partValueTable.insertRow(-1);

    // base value div
    var partbaseValueTD = partValueRow.insertCell(-1);
    partbaseValueTD.className = RIUTIL.CLASS_LAYOUT_TD;
    var partbaseValueDiv = doc.createElement("div");
    partbaseValueTD.appendChild(partbaseValueDiv);
    partbaseValueDiv.id = RIUTIL.getPartBaseValueDivDomID(partId);
    partbaseValueDiv.appendChild(doc.createTextNode(baseValue));
    partbaseValueDiv.style.display = (operator == 0) ? "inline" : "none";

    // components table
    var componentsTD = partValueRow.insertCell(-1);
    componentsTD.className = RIUTIL.CLASS_LAYOUT_TD;
    var componentsTable = doc.createElement("table");
    componentsTable.className = "RIActionsContainer";
    componentsTable.id = RIUTIL.getPartComponentsTableDomID(partId);
    componentsTD.appendChild(componentsTable);
    var componentsRow = componentsTable.insertRow(-1);
    componentsRow.id = RIUTIL.getPartComponentsRowDomID(partId);

    // help div
    var partHelpTD = partValueRow.insertCell(-1);
    partHelpTD.className = RIUTIL.CLASS_LAYOUT_TD;
    var partHelpDiv = doc.createElement("div");
    partHelpTD.appendChild(partHelpDiv);
    partHelpDiv.id = RIUTIL.getPartHelpDomID(partId)
    partHelpDiv.style.display = "none";
    var helpIcon = doc.createElement("img");
    JSSidePanelRender.setMotionImage(helpIcon, top['E1RES_img_jdehelp_gif'], top['E1RES_img_jdehelp_gif'], null);
    helpIcon.title = RIUTIL.CONSTANTS.RI_CREATE_CLICK;
    partHelpDiv.appendChild(helpIcon);
    //partHelpDiv.appendChild(doc.createTextNode(RIUTIL.CLICK));

    if (type == RIUTIL.TYPE_OBIEE_FILTER) {
        var partOption = doc.getElementById(RIUTIL.getPartOptionDomID(partId));
        partOption.options[operator].selected = true; // force map to option 
    }

    // add append icon
    var partAppendTD = partValueRow.insertCell(-1);
    partAppendTD.className = RIUTIL.CLASS_LAYOUT_TD;
    var partAppendDiv = doc.createElement("div");
    partAppendTD.appendChild(partAppendDiv);
    partAppendDiv.id = RIUTIL.getPartAppendDivDomID(partId);
    partAppendDiv.style.display = "none";
    var appendIcon = doc.createElement("img");
    JSSidePanelRender.setMotionImage(appendIcon, top['E1RES_img_AQ_addvaluen_gif'], top['E1RES_img_AQ_addvaluenmo_gif'], RIUTIL.goAppendMore);
    appendIcon.title = RIUTIL.CONSTANTS.RI_APPEND_COMPONENT;
    appendIcon.partId = partId;
    partAppendDiv.appendChild(appendIcon);
}

// Private: initialized configuration of the a part based on the type
RIUTIL.createPartOption = function (doc, type, partId, readOnly, operator) {
    if (type == RIUTIL.TYPE_CUSTOMIZED) {
        var partOption = doc.createElement("SELECT");
        partOption.options[0] = new Option(RIUTIL.CONSTANTS.RI_CREATE_REMAIN_AS, "default", false, false);
        partOption.options[1] = new Option(RIUTIL.CONSTANTS.RI_CREATE_MAP_TO, "custmized", false, false);
        partOption.options[operator].selected = true;
        if (!readOnly)
            partOption.onchange = RIUTIL.goChangeParameterConfig;
    }
    else if (type == RIUTIL.TYPE_OBIEE_FILTER) {
        var partOption = doc.getElementById("operatorTemplate").cloneNode(true);
        partOption.style.display = "inline";
        partOption.options[0].selected = true;
        if (!readOnly)
            partOption.onchange = RIUTIL.goChangeFilterOperator;
    }

    partOption.id = RIUTIL.getPartOptionDomID(partId);
    partOption.partId = partId;
    if (readOnly)
        partOption.setAttribute('disabled', readOnly);

    return partOption;
}

// Private: clean up all the existing template parts
RIUTIL.clearTemplateParts = function (containerId) {
    RIUTIL.resetParameterTable(containerId);
    RIUTIL.resetURLTemplate(containerId);
    RIUTIL.goStopKeyCapture();
}

// Private: clean up parameter table
RIUTIL.resetParameterTable = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var table = doc.getElementById("paramTable");
    RIUTIL.removeAllChildren(table.lastChild, 1);
}

// Private: clean up URL preview 
RIUTIL.resetURLTemplate = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var prevewForURLSpan = doc.getElementById("prevewForURLSpan");
    RIUTIL.removeAllChildren(prevewForURLSpan);
}

// Public: called by Server. framebeak detection popup launcher to popup a new window contain a iframe of generic URL.
// the onload event handler of iframe will set safeToEmbedded to true in the opener window.
// for a generic url containning framebreaker, the onload event handler does not get chance to triggered before the whole page 
// was override by the framebreaker code. So the flag safeToEmbedded will not get chance to be set for ever. We shall give 
// the iframe enough time to load its content (15 seconds), before we determine if there is framebreaker at all.
RIUTIL.detectFrameBreaker = function (containerId, targetUrl, callbackfn) {
    window.safeToEmbedded = false;
    window.currentCheckSafeToEmbed = 0;
    if (typeof callbackfn != 'undefined' && callbackfn != null) {
        setTimeout(function () {
            RIUTIL.checkSafeToEmbed(containerId, callbackfn);
        }, 1000);
    }
    else {
        setTimeout(function () {
            RIUTIL.checkSafeToEmbed(containerId);
        }, 1000);
    }

    var e1UrlCreator = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_RES);
    e1UrlCreator.setURI("owhtml/FBDection.jsp");
    e1UrlCreator.setParameter(RIUTIL.PARAM_URL, targetUrl);
    var popupUrl = e1UrlCreator.toString();
    window.FBDetectionWin = window.open(e1UrlCreator.toString(), '_blank', 'location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=yes,dependent=no');
}

// Private: framebeak detection timer to see if the popup contain framebreaker very 1 seconds and only run for maximal 15 seconds
RIUTIL.checkSafeToEmbed = function (containerId, callbackfn) {
    if (window.safeToEmbedded) // popup onload event is triggered before timeout of detection process
    {
        RIUTIL.setSaveAllowed(containerId, true);
        if (typeof callbackfn != 'undefined' && callbackfn != null)
            callbackfn();
        else
            alert(RIUTIL.CONSTANTS.RI_CREATE_VALIDATE_OK);
    }
    // when user close the window by its own, we do not need to continue with the detection any more.
    else if (window.FBDetectionWin.closed) {
        return;
    }
    else if (RIUTIL.MAX_CHECK_SAFE_TO_EMBED <= window.currentCheckSafeToEmbed++) // the onload of popup never get chance to be invoke due to frame breaker
    {
        window.FBDetectionWin.close();
        RIUTIL.setSaveAllowed(containerId, false);
        alert(RIUTIL.CONSTANTS.RI_CREATE_VALIDATE_FAIL);
        RIUTIL.enableInspectDialog();
        // roll back to static url so that user can fix the bad url dirrectly
        var doc = RIUTIL.getRIPaneDoc(containerId);
        var optionStatic = doc.getElementById("optionStatic");
        optionStatic.checked = true;

        var optionDynamic = doc.getElementById("optionDynamic");
        optionDynamic.checked = false;

        RIUTIL.goChangeContexualParamOption(false);
    }
    else {
        setTimeout(function () {
            RIUTIL.checkSafeToEmbed(containerId, callbackfn);
        }, 1000);
    }
}

// Private: change the visibility of url template dropdown icon
RIUTIL.iniURLTemplateDropDown = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var dropdownIcon = doc.getElementById("urlTemplateDropdownIcon");
    dropdownIcon.onmouseover = RIUTIL.onMouseOverDropdownListIcon;
    dropdownIcon.onmouseout = RIUTIL.onMouseOutDropdownListIcon;
    dropdownIcon.doc = doc;
    dropdownIcon.containerId = containerId;
    dropdownIcon.initItemTag = function (containerId, doc, tdTag, item) {
        tdTag.appendChild(doc.createTextNode(item.name));
        tdTag.url = item.url;
        tdTag.description = item.description;
        tdTag.name = item.name;
        tdTag.containerId = containerId;
        tdTag.onclick = RIUTIL.goSelectTemplate;
        tdTag.containerId = containerId;
        tdTag.doc = doc;
    }
}

// Public called by Server: set the display for dropdown icon
RIUTIL.setURLTemplateDropDownDisplay = function (containerId, display) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var dropdownIcon = doc.getElementById("urlTemplateDropdownIcon");
    dropdownIcon.style.display = display ? "inline" : "none";
}

// Private: change the visibility of OBIEE VA Icon
RIUTIL.updateOBIEEVADisplay = function (containerId, display) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var vaIcon = doc.getElementById("OBIEEObjectSelectIcon");
    vaIcon.style.display = display ? "inline" : "none";
}

// Private: change the visibility of the left scrolling tab icon
RIUTIL.setLeftShiftVisible = function (containerId, visible) {
    var leftTab = document.getElementById("leftShiftIcon");
    if (leftTab)
        leftTab.style.visibility = visible ? "visible" : "hidden";
}

// Private: change the visibility of the right scrolling tab icon
RIUTIL.setRightShiftVisible = function (containerId, visible) {
    var rightTab = document.getElementById("rightShiftIcon");
    if (rightTab)
        rightTab.style.visibility = visible ? "visible" : "hidden";
}

// Public: onload event handler from inspect page to init the visibility, enablement of each GUI components (menu icons and input field)
// mode: view/edit/create
// paneTitle: the translated displayed title for RIPane
// linkId: the index id of object in the list of context related objects
// isDynamic: used for generic url to distiguish the static url or dynamic url option
// keyControlIds: for view or edit mode, the existing E1 keys are passed in from the server.
// isEditor: the editor access of the object determine the avaiability of delete and edit icons
RIUTIL.initInspectPage = function (containerJSON) {
    var container = eval(containerJSON);
    var frame = container.frame;
    RIUTIL.replaceContainer(container);
    var isDomesticLanguageUser = (RIUTIL.layoutManager.langCode.Trim().length == 0);
    var containerId = container.id;
    var doc = RIUTIL.getRIPaneDoc(containerId);

    RIUTIL.inspectedContainerId = containerId;
    RIUTIL.pathType = frame.pathType;
    RIUTIL.parameterControls = new Array(); // will be populated with a ajax post later
    RIUTIL.keyControls = new Object();
    RIUTIL.setPaneTitle(containerId, container.frame.description);
    RIUTIL.isURL = RIUTIL.pathType == RIUTIL.TYPE_GENERIC;
    RIUTIL.isOBIEE = RIUTIL.pathType == RIUTIL.TYPE_OBIEE_REPORT || RIUTIL.pathType == RIUTIL.TYPE_OBIEE_REPORT || RIUTIL.pathType == RIUTIL.TYPE_OBIEE_PAGE || RIUTIL.pathType == RIUTIL.TYPE_OBIEE_FOLDER;
    RIUTIL.isE1Form = RIUTIL.pathType == RIUTIL.TYPE_E1_FORM;
    RIUTIL.isOVR = RIUTIL.pathType == RIUTIL.TYPE_OVR_FRAME;
    RIUTIL.isE1Page = RIUTIL.pathType == RIUTIL.TYPE_E1_PAGE_FRAME;
    RIUTIL.isADFFrame = RIUTIL.pathType == RIUTIL.TYPE_ADF_FRAME;
    RIUTIL.isWLComponent = RIUTIL.pathType == RIUTIL.TYPE_WL_PANE;
    RIUTIL.isLandingPage = RIUTIL.pathType == RIUTIL.TYPE_LANDING_PANE;

    RIUTIL.isPFFeatureEnabled = typeof (PF) != "undefined";

    var prodCode = RIUTIL.PRODUCT_GENERIC_URL;
    var isDynamic = frame.isDynamic;
    var mode = container.mode;
    var isCreate = (mode == RIUTIL.MODE_CREATE);
    var isEdit = (mode == RIUTIL.MODE_EDIT);
    var isDetail = (mode == RIUTIL.MODE_DETAIL);
    var showFISetting = RIUTIL.isE1Form;
    var editable = isCreate || isEdit;

    if (RIUTIL.isLcmPreview()) {
        editable = false;
    }
    var showURLInput = RIUTIL.isOBIEE || RIUTIL.isURL;
    var enableURLInput = editable && RIUTIL.isURL;
    var showOBIEEWizard = editable && RIUTIL.isOBIEE;
    var showSeperator = editable && (RIUTIL.isURL || RIUTIL.isE1Form)
    var showCustomizedText = editable;

    RIUTIL.setActiveProduct(prodCode);
    RIUTIL.setEnableById(doc, "objName", editable && (isCreate || isDomesticLanguageUser));
    RIUTIL.setEnableById(doc, "objDescription", editable && (isCreate || isDomesticLanguageUser));
    RIUTIL.setDisplayById(doc, "inputForPfId", showFISetting && RIUTIL.isPFFeatureEnabled);
    RIUTIL.setDisplayById(doc, "inputForE1AppId", showFISetting || RIUTIL.isOVR);
    RIUTIL.setDisplayById(doc, "inputForE1FormId", showFISetting || RIUTIL.isOVR);
    RIUTIL.setDisplayById(doc, "inputForE1VersionId", showFISetting || RIUTIL.isOVR);
    RIUTIL.setDisplayById(doc, "inputForReportId", RIUTIL.isOVR);
    RIUTIL.setDisplayById(doc, "inputForQueryId", RIUTIL.isOVR);
    RIUTIL.setDisplayById(doc, "inputForAutoFind", showFISetting);
    RIUTIL.setDisplayById(doc, "inputForShowTitleBar", RIUTIL.isE1Form);
    RIUTIL.setDisplayById(doc, "inputForMaximizeGrid", showFISetting);
    RIUTIL.setDisplayById(doc, "inputForE1PageId", RIUTIL.isE1Page);
    RIUTIL.setDisplayById(doc, "inputForADFTaskId", RIUTIL.isADFFrame);
    RIUTIL.setDisplayById(doc, "inputForProxyAppId", RIUTIL.isADFFrame && !RIUTIL.isCompositePage);
    RIUTIL.setDisplayById(doc, "inputForProxyFormId", RIUTIL.isADFFrame && !RIUTIL.isCompositePage);
    RIUTIL.setDisplayById(doc, "inputForProxyVersionId", RIUTIL.isADFFrame && !RIUTIL.isCompositePage);
    RIUTIL.setDisplayById(doc, "inputForIncludeAll", isCreate && RIUTIL.isWLComponent);

    //landing page task context/show springboard
    RIUTIL.setDisplayById(doc, "inputForTaskMenu", RIUTIL.isLandingPage);
    RIUTIL.setEnableById(doc, "objTaskMenu", editable);

    RIUTIL.setEnableById(doc, "objAutoFind", editable);
    RIUTIL.setEnableById(doc, "objMaxGrid", editable);
    RIUTIL.setEnableById(doc, "objShowTitleBar", editable);
    RIUTIL.setDisplayById(doc, "ContexualConfigureTable", !RIUTIL.isCompositePage && !RIUTIL.isADFFrame);


    RIUTIL.setDisplayById(doc, "inputForURL", showURLInput);
    if (showURLInput) {
        // adjust the width of input url field to fit the current width of frame
        doc.getElementById("publicLinkUrl").style.width = doc.getElementById("objName").offsetWidth - 30 + "px";
        RIUTIL.setEnableById(doc, "publicLinkUrl", enableURLInput);
        RIUTIL.iniURLTemplateDropDown(containerId);
    }

    RIUTIL.goStopKeyCapture();
    RIUTIL.unregisterAllKeys(containerId);

    RIUTIL.updateOverideTextAndButton(containerId, RIUTIL.isURL);
    RIUTIL.updateParameterSectionDisplay(containerId);
    RIUTIL.updateCustomizedTextDisplay(containerId, showCustomizedText);
    RIUTIL.updateSeparatorDisplay(containerId, showSeperator);
    RIUTIL.updateOBIEEVADisplay(containerId, showOBIEEWizard);
    RIUTIL.updateParameterSectionDisplay(containerId);
    RIUTIL.updateMenuDisplay(containerId);

    // only for generic url (edit/view) mode, the client need to ask the server to populate the URL paramter on the client side.
    if (isEdit || isDetail) {
        RIUTIL.setContexualParamOption(containerId, isDynamic);
        RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_PARAM_DISPLAY);
        if (container.frame && container.frame.hasFrameFilter) {
            RIUTIL.populateKeyTables(containerId, true);
        }
        if (RIUTIL.isE1Form)
            RIUTIL.goChangeApplicationId();
    }

    if (RIUTIL.isURL && editable)
        RIUTIL.loadURLTemplateList(containerId);

    if (RIUTIL.isOBIEE && isCreate)
        RIUTIL.goLaunchOBIEEObjectSelectionWindow();

    if (RIUTIL.isE1Form || RIUTIL.isOVR)
        RIUTIL.goChangeApplicationId();

    if (RIUTIL.isE1Page)
        RIUTIL.loadE1PageList();

    if (RIUTIL.isADFFrame)
        RIUTIL.loadADFIdList();

    RIUTIL.preventMutipleEditting(RIUTIL.layout, containerId);
    RIUTIL.layout.isEditting = true;

    // move all controls to popup div for composite page
    if (RIUTIL.isCompositePage)
        RIUTIL.floatInspectDialog(containerId, mode);

    // adjust the width of url to prevent it bleed out of the table
    var $publicLinkUrl = $(doc.getElementById("publicLinkUrl"));
    $publicLinkUrl.width(0);
    $publicLinkUrl.width($publicLinkUrl.closest("table").width() - 25);
}

// Private: remove resize indicator
RIUTIL.removeResizeIndicator = function () {
    var document = RIUTIL.getFrameworkDoc();
    RIUTIL.dragMode = false;
    if (RIUTIL.resizeIndicator) {
        document.body.removeChild(RIUTIL.resizeIndicator);
        RIUTIL.resizeIndicator = null;
    }
}

// resize the given two sub container in the given parent Container Id. subIndex is the index number of the second subcontainer
RIUTIL.resizeContaienr = function (containerId, percentage) {
    var container = RIUTIL.getContainer(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_RESIZE_CONTAINER, RIUTIL.PARAM_PERCENTAGE, percentage);
}

// Private: return true if the path is valid for URL type
RIUTIL.isValidURL = function (containerId) {
    // for embedded E1 form, as long as the form list has something in it, the path is valid
    if (RIUTIL.pathType == RIUTIL.TYPE_E1_FORM
    || RIUTIL.pathType == RIUTIL.TYPE_FREESTYLE_FRAME
    || RIUTIL.pathType == RIUTIL.TYPE_WL_PANE
    || RIUTIL.pathType == RIUTIL.TYPE_LANDING_PANE
    || RIUTIL.pathType == RIUTIL.TYPE_OVR_FRAME
    || RIUTIL.pathType == RIUTIL.TYPE_E1_PAGE_FRAME
    || RIUTIL.pathType == RIUTIL.TYPE_ADF_FRAME) {
        return true;
    }

    var doc = RIUTIL.getRIPaneDoc(containerId);
    var path = doc.getElementById("publicLinkUrl").value;
    if (path == null || path == "")
        return false;

    if (RIUTIL.pathType != RIUTIL.TYPE_GENERIC) // for OBIEE
    {
        return true;
    }
    // for normal URL
    if (path.indexOf("http://") == -1 && path.indexOf("https://") == -1 && path.indexOf("ftp://") == -1) {
        path = "http://" + path;
        doc.getElementById("publicLinkUrl").value = path;
    }
    //var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/i; 
    var regexp = /^(ftp|http|https):\/\//i;
    return regexp.test(path);
}

// Public Server invoke the client to display a prompt window to all user to override OBIEE connection info
RIUTIL.goLaunchOBIEEObjectSelectionWindow = function () {
    var clientWidth = document.body.clientWidth;
    var clientHeight = RIUTIL.getClientHeight();
    var winWidth = clientWidth * 0.8;
    var winHeight = clientHeight * 0.8;
    var processIncoLeft = clientWidth / 2 - 22;
    var processIncoTop = clientHeight / 2 - 22;
    var framework = new PSStringBuffer();
    var title = RIUTIL.CONSTANTS.RI_SELECT_OBIEE;

    framework.append('<img id="OBIEEProcessingDiv" style="position: absolute; z-index:20;left:').append(processIncoLeft).append('px;top:').append(processIncoTop).append('px;" src=').append(top['WEBGUIRES_images_loading_52x52_gif']).append('></img>');
    framework.append('<iframe frameborder="0" title="').append(title).append('" width="').append(winWidth).append('px" height="').append(winHeight).append('px" src="servlet/com.jdedwards.runtime.virtual.VCServlet?launchAction=launchBICatalog">');
    framework.append('</iframe>');

    RIUTIL.launchOBIEEWizardWindow(RIUTIL.inspectedContainerId, "OBIEE_OPEN_SELECTION", framework.toString());
}

// Public Server invoke the client to display a prompt window to all user to override OBIEE connection info
RIUTIL.launchOBIEEWizardWindow = function (containerId, popupId, frameHtml) {
    var clientWidth = document.body.clientWidth;
    var clientHeight = RIUTIL.getClientHeight();
    var winWidth = clientWidth * 0.8;
    var winHeight = clientHeight * 0.8;
    var winTop = clientHeight * 0.1;
    var winLeft = clientWidth * 0.1;
    var title = RIUTIL.CONSTANTS.RI_SELECT_OBIEE;
    var eventListener = {};
    eventListener.onClose = function (e) {
        JSSidePanelRender.hideUIBlock();
    }

    JSSidePanelRender.showUIBlock();
    RIUTIL.OBIEEObjectSelectionWindow = createPopup(popupId, SHOW_POPUP, true, false, true, false, title, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, null, frameHtml, eventListener);
    RIUTIL.OBIEEObjectSelectionWindow.winWidth = winWidth;
    RIUTIL.OBIEEObjectSelectionWindow.winHeight = winHeight;
    RIUTIL.OBIEEObjectSelectionWindow.containerId = containerId;
}

// Public: call back from popup window
RIUTIL.callbackFromBIControl = function (path, pathType) {
    var containerId = RIUTIL.OBIEEObjectSelectionWindow.containerId;
    RIUTIL.OBIEEObjectSelectionWindow.onClose();
    var doc = RIUTIL.getRIPaneDoc(containerId);
    doc.getElementById('publicLinkUrl').value = path;
    var lastSlash = path.lastIndexOf("/");
    if (lastSlash > 0) {
        var fileName = path.substring(lastSlash + 1);
        doc.getElementById('objDescription').value = fileName;
        doc.getElementById('objName').value = fileName;
    }
    RIUTIL.pathType = pathType;
    var prodCode = RIUTIL.getActiveProduct();
    RIUTIL.updateParameterSectionDisplay(containerId);
    RIUTIL.postAjaxRequest(containerId, prodCode, RIUTIL.AJAX_CMD_SET_PATH, RIUTIL.PARAM_PATH_TYPE, pathType, RIUTIL.PARAM_PATH, path, RIUTIL.PARAM_USER_TYPED, false);
}


// Private update view: Overide text and button
RIUTIL.updateOverideTextAndButton = function (containerId, isURL) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    RIUTIL.setDisplayById(doc, "overrideParameterSpan", isURL);
    RIUTIL.setDisplayById(doc, "overrideFilterSpan", !isURL);
    RIUTIL.setDisplayById(doc, "validate1", isURL);
    RIUTIL.setDisplayById(doc, "validate2", isURL);
    RIUTIL.setDisplayById(doc, "prevewForURLSpan", isURL);
    RIUTIL.setDisplayById(doc, "previewTable", isURL);
}

// Private update view: the visibility of separator
RIUTIL.updateSeparatorDisplay = function (containerId, showSeparator) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var container = RIUTIL.getContainer(containerId);
    RIUTIL.setDisplayById(doc, "separatorDiv", showSeparator);
}

// Private update view: the visibility of separator
RIUTIL.updateCustomizedTextDisplay = function (containerId, showCustomizedText) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    RIUTIL.setDisplayById(doc, "customizedTextrDiv", showCustomizedText);
}

// Private: higlight or dehilight given part id in parameter table
RIUTIL.highlightParameterPart = function (containerId, partId, highlight) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var partRow = doc.getElementById(RIUTIL.getPartRowDomID(partId));
    partRow.className = highlight ? RIUTIL.CLASS_PARAM_HIGHLIGHT : "";

    var partSpan = doc.getElementById(RIUTIL.getPartReviewDomID(partId));
    if (partSpan)
        partSpan.className = highlight ? RIUTIL.CLASS_PARAM_HIGHLIGHT : RIUTIL.CLASS_PARAM_VALUE_SPAN;
}


// Private: higlight or dehilight given compenent id and given part id in parameter table
RIUTIL.highlightComponent = function (containerId, controlId, partId, componentIndex, highlight) {
    // control highlighter
    RIUTIL.updateControlIndicator(controlId, "RIHighlightParameterContorl", highlight);
    var container = RIUTIL.getContainer(containerId);

    // delete icon for component
    if (container.mode == RIUTIL.MODE_EDIT || container.mode == RIUTIL.MODE_CREATE) {
        var doc = RIUTIL.getRIPaneDoc(containerId);
        var iconId = RIUTIL.getIconIdForDeleteComponent(partId, componentIndex);
        RIUTIL.setDisplayById(doc, iconId, highlight);
    }
}

// Private: the dropdown is supposed to be hidden again if
// the mouse move out of the area for a certain period of time
RIUTIL.decideDropdownListVisibility = function (containerId) {
    if (!RIUTIL.isOverDropDownList) {
        RIUTIL.removeElemById("dropdownDiv", RIUTIL.getRIPaneDoc(containerId));
        clearInterval(RIUTIL.intervalId);
        RIUTIL.intervalId = null
    }
}

// Public: call by from.js for hotkey support for Page Runtime Mode only 
// index 0 is for "no layout", 1 for the first personal layout, ... 9 for the 9th personal layout.
RIUTIL.goHotkeySelectCustomizedLayout = function (index) {
    // if satellite form intercept the hotkey for layout change, the request shall be forward the parent frame (Mast Frame).
    if (parent.RIUTIL)
        return parent.RIUTIL.goHotkeySelectCustomizedLayout(index);

    if (RIUTIL.layoutManager.designMode)
        return;

    var layouts = RIUTIL.layoutManager.customizedLayouts;
    for (var i = 0, myLayoutIndex = 0; i < layouts.length; i++) {
        if (layouts[i].isPersonal) {
            if (index == myLayoutIndex) {
                var layoutId = RIUTIL.layoutManager.customizedLayouts[i].id;
                RIUTIL.selectCustomizedLayout(layoutId);
                return;
            }
            myLayoutIndex++;
        }
    }
}


// Public: Server ask the client to popup URL in a browser window for preview
// open only one popu for each tab.
RIUTIL.popupURL = function (url, objectId) {
    objectId = objectId.replace(/-/g, "");
    url = url.replace(/EmbeddedE1FormLaunch/g, "ShortcutLauncher"); //TODO: need to do it for only embedded E1 form
    return window.open(url, objectId);
}

// Private: create a drag indicator for drag and drop process
RIUTIL.createDragIndicator = function (srcElem) {
    var document = RIUTIL.getFrameworkDoc();
    var infoTag = RIUTIL.searchParentByPorpName(srcElem, "containerId");
    var containerId = infoTag.containerId;
    var objectName = infoTag.objectName; // open container
    if (!objectName) // moving container
    {
        var container = RIUTIL.getContainer(containerId);
        var objectName = container.frame.name;
    }
    var div = document.createElement("div");
    div.style.position = 'absolute';

    var textNode = document.createTextNode(objectName);
    div.appendChild(textNode);
    div.className = RIUTIL.CLASS_LAYOUT_FRAME;
    div.movingContainerId = containerId;
    div.movingObjectId = container && container.frame ? container.frame.objId : infoTag.objectId;
    div.pathType = infoTag.pathType;
    div.style.left = -5000 + "px"; // do not show it now.
    div.style.zIndex = 3000;
    document.body.appendChild(div);
    return div;
}

// Private: create a event blocker to capture all the mouse move and up event during drag and drop process
RIUTIL.createEventBlocker = function (containerId, eventToAdded) {
    var document = RIUTIL.getFrameworkDoc();
    var id = "eventBlocker" + containerId;
    var elem = document.getElementById(id);
    if (!elem) {
        var container = RIUTIL.getContainer(containerId);
        var containerTag = RIUTIL.getContainerTag(container);
        var containerRegion = RIUTIL.getRegion(containerTag);

        elem = document.createElement("div");
        elem.className = "RIDragging RIAlmostTransparent";
        elem.style.position = "absolute";
        elem.style.left = containerRegion.left + "px";
        elem.style.top = containerRegion.top + "px";
        elem.style.height = containerRegion.height + "px";
        elem.style.width = containerRegion.width + "px";
        elem.style.zIndex = 2000000001;
        //elem.style.cursor = "move";
        elem.style.visibility = "visible";
        elem.id = id;
        elem.style.overflow = "hidden";

        var table = RIUTIL.createTable();
        var tbody = document.createElement("tbody");
        var td = document.createElement("td");
        var tr = document.createElement("tr");
        var div = document.createElement("div");
        div.style.height = containerRegion.height + "px";
        div.style.width = containerRegion.width + "px";
        td.appendChild(div);
        tr.appendChild(td);
        tbody.appendChild(tr);
        table.appendChild(tbody);
        elem.appendChild(table);

        document.body.appendChild(elem);
        RIUTIL.addEvent(top.document.body, "mouseup", eventToAdded);
        if (!RIUTIL.isCompositePage) {
            RIUTIL.addEvent(RIUTIL.getE1PaneForm(), "mouseup", eventToAdded);
        }
    }
    return elem;
}

// Private: remove UI for event blocker
RIUTIL.removeEventBlocker = function (id, eventToBeRemoved) {
    var document = RIUTIL.getFrameworkDoc();
    var elem = document.getElementById("eventBlocker" + id);
    if (elem) {
        elem.parentNode.removeChild(elem);
        RIUTIL.removeEvent(top.document.body, "mouseup", eventToBeRemoved);
        if (!RIUTIL.isCompositePage) {
            RIUTIL.removeEvent(RIUTIL.getE1PaneForm(), "mouseup", eventToBeRemoved);
        }
    }
}

// Public: called by jdemenu.js to redraw E1 and RI Pane after user preference change RI pane visibility
RIUTIL.reloadE1AndRI = function () {
    // we can not redraw right away, because jas need some time to refresh the data cache for option.
    setTimeout("window.location.replace('" + window.location.href + "')", 1000);
}

// Public: call by server when detect session timeout in ajax or page controler.
// refreshing the whole window will bring user back to login window.
RIUTIL.timeout = function (message) {
    if (message)
        alert(message);
    top.location.replace(top.location.href);
}

// Public: Server tell the client to update the description field on inspect page
RIUTIL.updateFormInfo = function (containerId, name, description, supportAutoFind, isAutoFind, supportMaxGrid, isMaxGrid) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var objName = doc.getElementById("objName");
    objName.value = name;
    var objDescription = doc.getElementById("objDescription");
    objDescription.value = description;
    RIUTIL.setDisplayById(doc, "inputForAutoFind", supportAutoFind); //TODO: adjust height
    doc.getElementById("objAutoFind").checked = isAutoFind;
    RIUTIL.setDisplayById(doc, "inputForMaximizeGrid", supportMaxGrid);
    doc.getElementById("objMaxGrid").checked = isMaxGrid;

    // update the height again since some control because of the visibility change
    if (RIUTIL.isCompositePage)
        RIUTIL.adjustInspectDialogHeight(containerId);
}

RIUTIL.updateADFProxyAppId = function (containerId, proxyAppId) {
    var objADFProxyAppId = RIUTIL.getADFProxyAppIdTag();
    objADFProxyAppId.value = proxyAppId;
}

RIUTIL.updateADFProxyFormList = function (json) {
    var formSelection = eval(json);
    var selectTag = RIUTIL.getADFProxyFormSelectionTag();
    if (selectTag)
        selectTag.options.length = 0;

    if (formSelection == null)
        return;
    var formList = formSelection.formList;
    for (var i = 0; i < formList.length; i++) {
        var formId = formList[i][0];
        var formDescription = formList[i][1];
        selectTag.options[i] = new Option(formDescription + "(" + formId + ")", formId, false, false);
        if (formId == formSelection.formId)
            selectTag.options[i].selected = true;
    }

    if (formList.length == 0 && RIUTIL.getAppIdTag().value != "")
        alert(RIUTIL.CONSTANTS.RI_INVLIAD_APP_ID);
    else
        RIUTIL.goChangeADFProxyFormSelection();
}

RIUTIL.updateADFProxyVersionList = function (json) {
    RIUTIL.renderGenericSelection(json, RIUTIL.getADFProxyVersionSelectionTag(), "versionList", "versionId");
    RIUTIL.goChangeADFProxyVersionSelection();
}

// Public: Server tell the client to show RIPane title 
RIUTIL.setPaneTitle = function (containerId, title) {
    var paneTitle = document.getElementById("paneTitle" + containerId);
    if (paneTitle) {
        paneTitle.firstChild.nodeValue = (title != "") ? title : " ";
        paneTitle.title = title;
    }
}

// Public: server tell the client to display the key information the client just captured 
// or populate key information already existing in the current link for edit or view mode
RIUTIL.renderCaptureKey = function (containerId, controlId, desAlias, value, warning) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var keyTable = doc.getElementById("keyTable");

    var newKeyRow = keyTable.insertRow(-1);
    var newKeyDesAliasTD = newKeyRow.insertCell(-1);
    newKeyDesAliasTD.appendChild(doc.createTextNode(desAlias));
    newKeyDesAliasTD.className = RIUTIL.CLASS_LAYOUT_TD;
    var newKeyValueTD = newKeyRow.insertCell(-1);
    newKeyValueTD.className = RIUTIL.CLASS_LAYOUT_TD;
    if (warning)// this is a hidden field on the current E1 form
    {
        newKeyValueTD.style.color = "red";
        value = warning;
    }
    newKeyValueTD.appendChild(doc.createTextNode(value));
    newKeyRow.setAttribute("controlId", controlId);
    newKeyRow.onmouseover = RIUTIL.onMouseOverKey;
    newKeyRow.onmouseout = RIUTIL.onMouseOutKey;

    // register the controlId as the key and hide selected add icon for key
    RIUTIL.registerKey(controlId);
    RIUTIL.reomveAddIcon(controlId);
}

// Private: register the control id of the transation key on the client side, so that we use it to determin if we need to render add icon for it
RIUTIL.registerKey = function (controlId) {
    RIUTIL.keyControlIdList.push(controlId);
}

// Private: clean up all registered transaction keys on the client side
RIUTIL.unregisterAllKeys = function () {
    RIUTIL.keyControlIdList = new Array();
}

// Private: search for the registered key given controlId,
RIUTIL.isRegisteredKey = function (controlId) {
    var list = RIUTIL.keyControlIdList;
    if (!list)
        return false;

    for (var i = 0; i < list.length; i++) {
        if (controlId == list[i])
            return true;
    }
    return false;
}

// Private: search for the registered key given controlId,
RIUTIL.cleanupKeys = function (contaienrId) {
    var list = RIUTIL.keyControlIdList
    if (list && list.length > 0) {
        for (var i = 0; i < list.length; i++) {
            var controlId = list[i];
            RIUTIL.reomveAddIcon(controlId);
        }
        RIUTIL.unregisterAllKeys();
    }
}

// Public: called by jdedta.js response to certain server GUI change event
// Currently we only handle tab switch event and make sure key indicators still show up after tab switch.
RIUTIL.recieveGUIUpdateNotification = function () {
    var containerId = RIUTIL.inspectedContainerId;
    if (containerId) // content definition process
    {
        var container = RIUTIL.getContainer(containerId);
        if (!container)
            return;

        if (RIUTIL.isRIDisplayed() && (container.mode == RIUTIL.MODE_EDIT || container.mode == RIUTIL.MODE_DETAIL || container.mode == RIUTIL.MODE_CREATE)) {
            RIUTIL.setCaptureMode(RIUTIL.captureMode, (RIUTIL.captureMode == RIUTIL.VISIBLE_FIELD_CAPTURE || RIUTIL.captureMode == RIUTIL.EDITABLE_FIELD_CAPTURE));
        }

        if (container.mode == RIUTIL.MODE_DETAIL)
            RIUTIL.updateFormView();
    }
    else // event rule editing process
    {
        RIUTIL.updateFormView();
    }
}

// Private: Server tell the client the current active product code
RIUTIL.setActiveProduct = function (activeProduct) {
    RIUTIL.activeProduct = activeProduct;
}

// Private: Server tell the client the current active system product code
RIUTIL.setProductSysCode = function (productSysCode) {
    RIUTIL.productSysCode = productSysCode;
}

// Private: get the current active system product code
RIUTIL.getProductSysCode = function () {
    return RIUTIL.productSysCode;
}

// Public: Server sends the client the error messsage for Ajax request
RIUTIL.displayUserError = function (message) {
    RIUTIL.enableInspectDialog();
    alert(message);
}

RIUTIL.enableInspectDialog = function () {
    // remove uiblock on inspect dialg if exist
    if (RIUTIL.isCompositePage && RIUTIL.floattingDialog) {
        RIUTIL.floattingDialog.removeClass("belowUIBlock").addClass("aboveUIBlock");
    }
}

RIUTIL.disableInspectDialog = function () {
    // remove uiblock on inspect dialg if exist
    if (RIUTIL.isCompositePage && RIUTIL.floattingDialog) {
        RIUTIL.floattingDialog.removeClass("aboveUIBlock").addClass("belowUIBlock");
    }
}

// Public: invoked by Server to update the URL of the existing container.
RIUTIL.renderFrameContent = function (containerId, url) {
    var contentWindow = RIUTIL.getContainerWinObject(containerId);
    if (contentWindow) // do not need to update minimized frame content
    {
        RIUTIL.markFrameNotInSynch(containerId, false);

        var container = RIUTIL.getContainer(containerId);
        container.frame.url = url; // update the url in frame object, so that popup action will work correctly after server update the frame url 

        if (!RIUTIL.isMinimized(container)) {
            var frame = RIUTIL.getRIPaneIFRAME(containerId);
            if (frame)
                frame.src = url;
        }
    }
}

// Private: notify the server to render the freestyle content in the freestyle frame
RIUTIL.renderFreeStyleFrameContent = function (container) {
    var designMode = RIUTIL.layoutManager.designMode || (RIUTIL.lcmEdit !== undefined && RIUTIL.lcmEdit);
    RIUTIL.postPageRequest(container.id, RIUTIL.PAGE_CMD_LOAD_FREESTYLE, RIUTIL.PARAM_OBJECT_ID, container.frame.objectId, RIUTIL.PARAM_DESIGN_MODE, designMode);
}

// Private: notify the server to render the watchlist component content in the frame
RIUTIL.renderWLPaneFrameContent = function (container) {
    var designMode = RIUTIL.layoutManager.designMode || (RIUTIL.lcmEdit !== undefined && RIUTIL.lcmEdit);
    RIUTIL.postPageRequest(container.id, RIUTIL.PAGE_CMD_LOAD_WL_COMPONENT, RIUTIL.PARAM_OBJECT_ID, container.frame.objectId, RIUTIL.PARAM_DESIGN_MODE, designMode);
}

// Private: notify the server to render the landing page content in the frame
RIUTIL.renderLPPaneFrameContent = function (container) {
    var designMode = RIUTIL.layoutManager.designMode || (RIUTIL.lcmEdit !== undefined && RIUTIL.lcmEdit);
    RIUTIL.postPageRequest(container.id, RIUTIL.PAGE_CMD_LOAD_LANDINGPAGE, RIUTIL.PARAM_OBJECT_ID, container.frame.objectId, RIUTIL.PARAM_DESIGN_MODE, designMode);
}

// Public: invoked by Server to update the URL of the existing container.
RIUTIL.renderFrameError = function (containerId, errorMessage) {
    var riPaneIFRAME = RIUTIL.getRIPaneIFRAME(containerId);
    if (riPaneIFRAME) // do not need to update minimized frame content
    {
        var frame = (riPaneIFRAME.contentWindow) ? riPaneIFRAME.contentWindow : (riPaneIFRAME.contentDocument.document) ? riPaneIFRAME.contentDocument.document : riPaneIFRAME.contentDocument;
        var doc = frame.document;
        doc.open();
        doc.write(errorMessage);
        doc.close();
    }
}

// Public: Server allow the client to show save icon
RIUTIL.setSaveAllowed = function (containerId, allowed) {
    var container = RIUTIL.getContainer(containerId);
    container.frame.saveAllowed = allowed;
}

// Public: Server allow the client to show detail icon
RIUTIL.setDetailAllowed = function (containerId, allowed) {
    var container = RIUTIL.getContainer(containerId);
    container.frame.detailAllowed = allowed;
}

// Public: Server allow the client to show close icon
RIUTIL.setBackAllowed = function (containerId, allowed) {
    var container = RIUTIL.getContainer(containerId);
    container.frame.backAllowed = allowed;
}

// Public: Server allow the client to show edit icon
RIUTIL.setEditAllowed = function (containerId, allowed) {
    var container = RIUTIL.getContainer(containerId);
    container.frame.editAllowed = allowed;
}

// Public: Server allow the client to show popup icon
RIUTIL.setPopupAllowed = function (containerId, allowed) {
    var container = RIUTIL.getContainer(containerId);
    container.frame.popupAllowed = allowed;
}

// Public: Server allow the client to show create icon
RIUTIL.setCreateAllowed = function (containerId, allowed) {
    var container = RIUTIL.getContainer(containerId);
    container.frame.createAllowed = allowed;
}

// Public: Server force the client the update the view (menu icons) based on the current model.
RIUTIL.updateMenuDisplay = function (containerId) {
    var container = RIUTIL.getContainer(containerId);
    var frame = container.frame;
    if (!frame)
        return;

    var activeLayout = RIUTIL.getCurrentSelectedLayout(RIUTIL.layoutManager.currentLayoutId);
    var doc = RIUTIL.getFrameworkDoc();

    RIUTIL.setDisplayById(doc, "objBack" + containerId, frame.backAllowed);
    RIUTIL.setDisplayById(doc, "objBackSpace" + containerId, frame.backAllowed);
    frame.editAllowed = frame.editAllowed && !RIUTIL.isLcmPreview() && !(activeLayout.isPublished || activeLayout.isRequestPublished || activeLayout.isRework);
    if (RIUTIL.isCompositePage && RIUTIL.layoutManager.designMode) // for composite page, we only show one of detail and edit option instead of both, and they are present with the same icon
    {
        if (frame.editAllowed) {
            var showEditAction = true;
            var showDetailAction = false;
        }
        else if (frame.detailAllowed) {
            var showEditAction = false;
            var showDetailAction = true;
        }
    }
    else // old logic for cafeOne
    {
        var showEditAction = frame.editAllowed;
        var showDetailAction = frame.detailAllowed;
    }

    RIUTIL.setDisplayById(doc, "objDetail" + containerId, showDetailAction);
    RIUTIL.setDisplayById(doc, "objDetailSpace" + containerId, showDetailAction);
    RIUTIL.setDisplayById(doc, "objEdit" + containerId, showEditAction);
    RIUTIL.setDisplayById(doc, "objEditSpace" + containerId, showEditAction);
    RIUTIL.setDisplayById(doc, "objPopup" + containerId, frame.popupAllowed);
    RIUTIL.setDisplayById(doc, "objPopupSpace" + containerId, frame.popupAllowed);
    RIUTIL.setDisplayById(doc, "objSave" + containerId, frame.saveAllowed);
    RIUTIL.setDisplayById(doc, "objSaveSpace" + containerId, frame.saveAllowed);
    RIUTIL.setDisplayById(doc, "objCreate" + containerId, frame.createAllowed);
    RIUTIL.setDisplayById(doc, "objCreateSpace" + containerId, frame.createAllowed);
    frame.deactivateAllowed = frame.deactivateAllowed && !RIUTIL.isLcmPreview()
                                && !(activeLayout.isPublished || activeLayout.isRequestPublished || activeLayout.isRework);
    RIUTIL.setDisplayById(doc, "objDeactivate" + containerId, frame.deactivateAllowed);
    RIUTIL.setDisplayById(doc, "objDeactivateSpace" + containerId, frame.deactivateAllowed);
    RIUTIL.setDisplayById(doc, "objMinimize" + containerId, frame.minimizeAllowed);
    RIUTIL.setDisplayById(doc, "objMinimizeSpace" + containerId, frame.minimizeAllowed);

    RIUTIL.setDisplayById(doc, "paneTitle" + containerId, true);
}

// Public: Server notify the client to change the Contexual parameter option 
// and client adjust view to confirm the change
RIUTIL.setContexualParamOption = function (containerId, isDynamic) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var container = RIUTIL.getContainer(containerId);

    var urlParamOptions = doc.getElementsByName("urlParamOption");
    urlParamOptions[1].checked = isDynamic;
    urlParamOptions[0].checked = !isDynamic;

    RIUTIL.setDisplayById(doc, "parameterMapAndPreviewForURL", isDynamic);
    if (RIUTIL.isURL)
        RIUTIL.setDisplayById(doc, "inputForURL", !isDynamic);

    var isCreate = (container.mode == RIUTIL.MODE_CREATE);
    var isEdit = (container.mode == RIUTIL.MODE_EDIT);

    if (RIUTIL.isE1Form) {
        var enableFISetting = (isCreate || isEdit) && !isDynamic;
        RIUTIL.setEnableById(doc, "objApplicationID", enableFISetting);
        RIUTIL.setDisplayById(doc, "objApplicationIDSearch", enableFISetting);
        RIUTIL.setEnableById(doc, "objFormSeletion", enableFISetting);
        RIUTIL.setEnableById(doc, "objVersionSeletion", enableFISetting);
        RIUTIL.setEnableById(doc, "objPfSeletion", enableFISetting && RIUTIL.isPFFeatureEnabled);
    }
    else if (RIUTIL.isOVR) {
        var enableOVRSetting = isCreate || isEdit;
        RIUTIL.setEnableById(doc, "objApplicationID", enableOVRSetting);
        RIUTIL.setDisplayById(doc, "objApplicationIDSearch", enableOVRSetting);
        RIUTIL.setEnableById(doc, "objFormSeletion", enableOVRSetting);
        RIUTIL.setEnableById(doc, "objVersionSeletion", enableOVRSetting);
        RIUTIL.setEnableById(doc, "objReportSeletion", enableOVRSetting);
        RIUTIL.setEnableById(doc, "objQuerySelection", enableOVRSetting);
    }
    else if (RIUTIL.isE1Page) {
        RIUTIL.setEnableById(doc, "objE1PageIdSelection", isCreate || isEdit);
    }
    else if (RIUTIL.isADFFrame) {
        var enableFISetting = (isCreate || isEdit) && !isDynamic;
        RIUTIL.setEnableById(doc, "objADFTaskIdSelection", enableFISetting);
        RIUTIL.setEnableById(doc, "objADFProxyFormSelection", enableFISetting);
        RIUTIL.setEnableById(doc, "objADFProxyVersionSelection", enableFISetting);
    }
}

// Public: Server notify the client to clean up all paramters
RIUTIL.clearAllParts = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var prevewForURLSpan = doc.getElementById("prevewForURLSpan");
    RIUTIL.removeAllChildren(prevewForURLSpan);
    RIUTIL.resetParameterTable(containerId);
}

// Public: the server notify the client to add new template part
RIUTIL.addTemplatePart = function (containerId, partId, type, baseValue, description, operator, acceptMore) {
    var container = RIUTIL.getContainer(containerId);
    var readOnly = (container.mode == RIUTIL.MODE_DETAIL);

    if (type != RIUTIL.TYPE_OBIEE_FILTER) // for filter of OBIEE, we do not need preview URL part
        RIUTIL.addURLTemplatePart(containerId, partId, type, baseValue, description, readOnly);

    if (type != RIUTIL.TYPE_LITERAL) // customized part of the normal URL or filter part of OBIEE report
    {
        RIUTIL.addParameterTablePart(containerId, partId, type, baseValue, description, readOnly, operator, acceptMore);
    }
}

// Public: set the template part with default components and help text
RIUTIL.setTemplatePart = function (containerId, partId, operator, acceptMore, isFocuse) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    if (!doc)
        return;

    // help text
    var allowAddMoreNow = operator != 0 && acceptMore && isFocuse;
    RIUTIL.setElemDisplay(doc, RIUTIL.getPartHelpDomID(partId), allowAddMoreNow);
    RIUTIL.setCaptureMode(allowAddMoreNow ? RIUTIL.PARAM_CAPTURE : RIUTIL.NO_CAPTURE, false);

    // separator add icon
    RIUTIL.setElemDisplay(doc, "addCustomizedTextIcon", allowAddMoreNow);

    // operator part
    var partOption = doc.getElementById(RIUTIL.getPartOptionDomID(partId));
    if (partOption)
        partOption.options[operator].selected = true; // force map to option 

    // value part
    RIUTIL.setElemDisplay(doc, RIUTIL.getPartBaseValueDivDomID(partId), operator == 0);
    RIUTIL.setElemDisplay(doc, RIUTIL.getPartReviewBaseDomID(partId), true);

    var componentsTable = doc.getElementById(RIUTIL.getPartComponentsTableDomID(partId));
    if (componentsTable) {
        componentsTable.removeChild(componentsTable.firstChild);
        var componentsRow = componentsTable.insertRow(-1);
        componentsRow.id = RIUTIL.getPartComponentsRowDomID(partId);
    }

    // preview url
    RIUTIL.setElemDisplay(doc, RIUTIL.getPartReviewBaseDomID(partId), true);

    var partPreview = doc.getElementById(RIUTIL.getPartReviewComponentDomID(partId));
    if (partPreview)
        partPreview.firstChild.nodeValue = "";

    var partRow = doc.getElementById(doc, RIUTIL.getPartRowDomID(partId));
    if (partRow) {
        partRow.acceptMore = acceptMore;
        partRow.isFocuse = isFocuse;
    }
}

// Private: add a generic component into the both parameter mapping and preview URL
RIUTIL.addComponent = function (containerId, partId, previewValue, mappingValue, className, controlId, componentIndex) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    if (!doc)
        return;

    // value part
    RIUTIL.setElemDisplay(doc, RIUTIL.getPartBaseValueDivDomID(partId), false); // hide base value in mapping
    var partComponentsRow = doc.getElementById(RIUTIL.getPartComponentsRowDomID(partId));
    if (!partComponentsRow)
        return;

    var partComponentCell = partComponentsRow.insertCell(-1);
    partComponentCell.className = className;
    partComponentCell.setAttribute("componentIndex", componentIndex);
    partComponentCell.setAttribute("partId", partId);
    partComponentCell.appendChild(doc.createTextNode(mappingValue));
    partComponentCell.onmouseover = RIUTIL.onMouseOverComponent;
    partComponentCell.onmouseout = RIUTIL.onMouseOutComponent;

    // preview part
    RIUTIL.setElemDisplay(doc, RIUTIL.getPartReviewBaseDomID(partId), false);
    var previewComponent = doc.getElementById(RIUTIL.getPartReviewComponentDomID(partId));
    if (previewComponent) {
        previewComponent.firstChild.nodeValue += previewValue;
        previewComponent.className = RIUTIL.CLASS_CONTROL_COMPONENT;
    }
    if (controlId) // control component need to have hover highlight
    {
        partComponentCell.setAttribute("controlId", controlId);
    }

    // add a hidden delete component icon into TD
    var deleteIconId = RIUTIL.getIconIdForDeleteComponent(partId, componentIndex);
    var deleteIcon = doc.createElement("img");
    deleteIcon.id = deleteIconId;
    deleteIcon.setAttribute("componentIndex", componentIndex);
    deleteIcon.setAttribute("partId", partId);
    deleteIcon.setAttribute("controlId", controlId);
    deleteIcon.onclick = RIUTIL.goDeleteComponent;
    deleteIcon.style.display = "none";
    deleteIcon.title = RIUTIL.CONSTANTS.RI_REMOVE_COMPONENT;

    JSSidePanelRender.setMotionImage(deleteIcon, top['E1RES_img_RI_delcom_gif'], top['E1RES_img_RI_delcommo_gif'], null);
    partComponentCell.appendChild(deleteIcon);
}

// Public: add a control component to the parameter mapping 
RIUTIL.addControlComponent = function (containerId, partId, value, controlId, ddItem, componentIndex) {
    RIUTIL.addComponent(containerId, partId, value, ddItem, RIUTIL.CLASS_CONTROL_COMPONENT, controlId, componentIndex);
}

// Public: add a separator component to the parameter mapping 
RIUTIL.addSeparatorComponent = function (containerId, partId, separtor, componentIndex) {
    RIUTIL.addComponent(containerId, partId, encodeURIComponent(separtor), separtor, RIUTIL.CLASS_SEPARATOR_COMPONENT, null, componentIndex);
}

// Public: Server notify the client to clean up web center template list before populate it
RIUTIL.clearWCTemplateList = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var templateSelection = doc.getElementById("templateSelection");
    templateSelection.options.length = 0;
}

// Public: Server notify the client to add a new groupspace template name in the list
RIUTIL.addWCTemplate = function (containerId, name) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var templateSelection = doc.getElementById("templateSelection");
    templateSelection.options[templateSelection.options.length] = new Option(name, name, false, false);
}

// Public: render all tab pages from scratch, triggered for metadata collection change event of Generic URL
// do not need to be called by Web Center or Beehive
RIUTIL.requestRenderAll = function () {
    if (RIUTIL.isCompositePage && RIUTIL.lcmLayoutId !== undefined && RIUTIL.lcmLayoutId != null) {
        RIUTIL.postAjaxRequest(null, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LAYOUT_PREVIEW_MODE, RIUTIL.PARAM_LAYOUT_ID, RIUTIL.lcmLayoutId, "lcmMode", true, "editMode", RIUTIL.lcmEdit);
        RIUTIL.setPageDesignBarEnable(true);
        RIUTIL.lcmLayoutId = null;
    }
    else if (!RIUTIL.isCompositePageAsApp) // composite page as app already launched this call and we do not want it to repeat
    {
        RIUTIL.postAjaxRequest(null, RIUTIL.PRODUCT_ALL, RIUTIL.AJAX_CMD_RENDER_ALL);
    }
}

// Public: called by Server to ask the user to confirm updatting the context link for generic url with approval required 
RIUTIL.renderUpdateConfirmation = function (containerId, message) {
    var answer = confirm(message)
    if (answer) {
        RIUTIL.goSaveObj(containerId, true); // save with confirm flag 
    }
}

// Private: return true at page runtime mode, the layout container gridId as parameter and the frame is normalized. 
// otherwise return false.
RIUTIL.needToShowRowCursor = function (gridId) {
    if (!RIUTIL.layoutManager)
        return false;

    if (RIUTIL.layoutManager.designMode)
        return true;

    var id = "G" + gridId;
    if (RIUTIL.layoutManager.registeredKeyControls[id])
        return true;

    return RIUTIL.containGridAsParameter(RIUTIL.layout, id);
}

// Priviate: return ture if the grid is used as parameter for a normlized frame in the current container or sub container
RIUTIL.containGridAsParameter = function (container, gridId) {
    if (container.subContainers) // check all the sub containers recursively
    {
        for (var i = 0; i < container.subContainers.length; i++) {
            var subContainer = container.subContainers[i];
            if (RIUTIL.containGridAsParameter(subContainer, gridId))
                return true;
        }
    }
    else if (container.frame && container.frame.registeredParamControls && container.frame.registeredParamControls[gridId] /* && !RIUTIL.isMinimized(container) */) // self contain the grid contorl as parameter and normorlize
    {
        return true;
    }

    return false;

}


// Private: check if the container contain the control id in itself or its decandence
// for impacted container, add container id into the set of impactedContainers, and the set will be used to refresh containers later
// after jdedta processes all the gui change events.
RIUTIL.collectImpactedContainers = function (container, id) {
    // do not need to refresh in we are in view detail, edit or create mode
    var mode = container.mode;
    if (mode == RIUTIL.MODE_CREATE || mode == RIUTIL.MODE_EDIT || mode == RIUTIL.MODE_DETAIL)
        return;

    if (container.subContainers) // check all the sub containers recursively
    {
        for (var i = 0; i < container.subContainers.length; i++) {
            var subContainer = container.subContainers[i];
            RIUTIL.collectImpactedContainers(subContainer, id);
        }
    }
    else if (container.frame && container.frame.registeredParamControls && container.frame.registeredParamControls[id]) // self contain the control
    {
        RIUTIL.impactedContainers.push(container.id);
    }
}

// Private: verify if the layout is impacted by control value change based on registered traction key, only impacted for design mode.
RIUTIL.collectImpactedLayout = function (id) {
    RIUTIL.impactLayout = RIUTIL.impactLayout || RIUTIL.layoutManager.registeredKeyControls[id];
}

// Public: called by JDEDTA to refresh all impacted containers and cleanup the impacted list.
RIUTIL.refreshImpactedContainers = function () {
    if (!RIUTIL.layout)
        return

    // it is possible that we are still in the middle of refresh layout, we want to wait until it is complete before refresh it again to avoid racing condition.
    if (!RIUTIL.areAllSatelliteFormsCompletelyLoaded()) {
        setTimeout("RIUTIL.refreshImpactedContainers()", 1000);
        return;
    }

    if (RIUTIL.impactLayout) // layout need to be updated with a full refresh.
    {
        if (RIUTIL.confirmContinueWithLayoutDirtyCheck())
            RIUTIL.refreshLayout();
    }
    else // no layout change, but there may be content change for some impacted container
    {
        // only indivisual frame content need to be close and synchronized
        RIUTIL.synchronizeImpactedContainersWithDirtyCheck();
    }
}

// Public: fully refresh layout and clean up cache
RIUTIL.refreshLayout = function () {
    RIUTIL.postAjaxRequest(0, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_REFRESH_LAYOUT);
}

// Private: go through all impacted containers and ask user to confirmation to continue synchronize each of container which is dirty. 
// Rejectting any confirmation leaves that container unchnaged.
// Acceptting any confirmation makes that container synchronized with change lost.
RIUTIL.synchronizeImpactedContainersWithDirtyCheck = function () {
    var containerList = RIUTIL.impactedContainers;
    for (var i = 0; i < containerList.length; i++) {
        var containerId = containerList[i];
        var container = RIUTIL.getContainer(containerId);

        if (container && !container.collapse && container.containerType == RIUTIL.CONATINAER_RIAF && RIUTIL.continueWithDirtyCheck(container, true))
            RIUTIL.synchronizeContainerWithCleanUp(containerId);
    }

    // clean up
    RIUTIL.impactedContainers = new Array();
    RIUTIL.impactLayout = false;
}

// Private: if there is any dirty container in the normal window state, we need user to conform continue. 
// user could can stay in the layout without continue if reject the confimation
RIUTIL.confirmContinueWithLayoutDirtyCheck = function () {
    var dirtyContainerIdList = RIUTIL.getDirtyContainerIds();

    // clean up
    RIUTIL.impactedContainers = new Array();
    RIUTIL.impactLayout = false;

    // automatically continue if there is no dirty contianer. ask the user if want to confirm to continue if there is any dirty container. 
    RIUTIL.hilightContainerList(dirtyContainerIdList, true);
    var continueSyncLayout = dirtyContainerIdList.length == 0 || confirm(RIUTIL.CONSTANTS.RI_WARN_UNCOMMIT);
    RIUTIL.hilightContainerList(dirtyContainerIdList, false);
    return continueSyncLayout;
}

// Private: find out all the dirty container from the normalized container list
RIUTIL.getDirtyContainerIds = function () {
    var dirtyContainerIdList = new Array();
    if (RIUTIL.layout) {
        var normalContainerIds = RIUTIL.getFrameIds(RIUTIL.layout, new Array(), false);
        for (var i = 0; i < normalContainerIds.length; i++) {
            var containerId = normalContainerIds[i];
            var container = RIUTIL.getContainer(containerId);

            if (container && !container.collapse && RIUTIL.isE1SatelliteContainer(container)) {
                var contentWindow = RIUTIL.getContainerWinObject(containerId);
                if (contentWindow && contentWindow.JDEDTAFactory) {
                    var dtaInstance = contentWindow.JDEDTAFactory.getInstance("");
                    if (dtaInstance.isDirty)
                        dirtyContainerIdList.push(containerId);
                }
            }
        }
    }
    return dirtyContainerIdList;
}

// Private: find the container by container id
RIUTIL.getContainer = function (containerId) {
    return RIUTIL.searchContainer(RIUTIL.layout, containerId);
}

// Private method deep search for a container with given container id
RIUTIL.searchContainer = function (container, containerId) {
    if (!containerId)
        return null;

    if (container.id == containerId)
        return container;

    if (!container.subContainers || container.subContainers.length == 0)
        return null;

    for (var i = 0; i < container.subContainers.length; i++) {
        var result = RIUTIL.searchContainer(container.subContainers[i], containerId)
        if (result)
            return result;
    }

    return null;
}

// Private: search and replace a given container in the model by identify it using container id.
RIUTIL.replaceContainer = function (container) {
    if (!RIUTIL.layout)
        return false;

    if (RIUTIL.layout.id == container.id) {
        RIUTIL.layout = container;
        return true;
    }

    RIUTIL.searchAndSetContainer(RIUTIL.layout, container);
}

// private: find the container with the same id of replacementContainer from the rootContainer and replace it.
RIUTIL.searchAndSetContainer = function (rootContainer, replacementContainer) {
    if (!rootContainer)
        return false;

    if (!rootContainer.subContainers || rootContainer.subContainers.length == 0)
        return false;

    for (var i = 0; i < rootContainer.subContainers.length; i++) {
        var subContainer = rootContainer.subContainers[i];
        if (subContainer.id == replacementContainer.id) {
            rootContainer.subContainers[i] = replacementContainer;
            return true;
        }

        if (RIUTIL.searchAndSetContainer(subContainer, replacementContainer))
            return true;
    }

    return;
}

// init the client model for object type palette
RIUTIL.createContentFactoryModel = function (contentFactoryJSON, hoverText) {
    var contentFactory = eval(contentFactoryJSON);
    contentFactory.itemList = new Array();

    if (contentFactory.acceptFreeStyleForm) {
        var item = { id: "paletteFreeStyleForm", iconSrc: window["E1RES_share_images_alta_qual_freestylecontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_FREESTYLE_OBJ, description: RIUTIL.CONSTANTS.RI_ADD_FREESTYLE_OBJ, hoverText: hoverText, pathType: RIUTIL.TYPE_FREESTYLE_FRAME };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptOVR && RIUTIL.geContentTotalByType(RIUTIL.layout, RIUTIL.TYPE_OVR_FRAME) == 0) {
        var item = { id: "paletteOVRPage", iconSrc: window["E1RES_share_images_alta_qual_ovrcontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_OVR_OBJ, description: RIUTIL.CONSTANTS.RI_ADD_OVR_OBJ, hoverText: hoverText, pathType: RIUTIL.TYPE_OVR_FRAME };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptE1Form) {
        if (RIUTIL.isCompositePage) {
            var item = { id: "paletteE1Form", iconSrc: window["E1RES_share_images_alta_qual_e1formcontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_E1_APP, description: RIUTIL.CONSTANTS.RI_ADD_E1_APP, hoverText: hoverText, pathType: RIUTIL.TYPE_E1_FORM };
        }
        else {
            var item = { id: "paletteE1Form", iconSrc: window["E1RES_share_images_alta_qual_e1formcontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_E1_FORM, description: RIUTIL.CONSTANTS.RI_ADD_E1_FORM, hoverText: hoverText, pathType: RIUTIL.TYPE_E1_FORM };
        }
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptE1Page) {
        var item = { id: "paletteE1Page", iconSrc: window["E1RES_share_images_alta_qual_e1pagecontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_E1PAGE_OBJ, description: RIUTIL.CONSTANTS.RI_ADD_E1PAGE_OBJ, hoverText: hoverText, pathType: RIUTIL.TYPE_E1_PAGE_FRAME };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptURL) {
        var item = { id: "paletteURL", iconSrc: window["E1RES_share_images_alta_qual_urlcontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_URL, description: RIUTIL.CONSTANTS.RI_ADD_REGUAL_URL, hoverText: hoverText, pathType: RIUTIL.TYPE_GENERIC };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptDVT && RIUTIL.geContentTotalByType(RIUTIL.layout, RIUTIL.TYPE_ADF_FRAME) == 0)// do not allow user to add second ADF content if there is one already in the page
    {
        var item = { id: "paletteDVTPage", iconSrc: window["E1RES_share_images_alta_qual_dvtcontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_DVT_OBJ, description: RIUTIL.CONSTANTS.RI_ADD_DVT_OBJ, hoverText: hoverText, pathType: RIUTIL.TYPE_ADF_FRAME };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptOBIEE) {
        var item = { id: "paletteOBIEE", iconSrc: window["E1RES_share_images_alta_qual_obieecontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_OBIEE, description: RIUTIL.CONSTANTS.RI_ADD_OBIEE, hoverText: hoverText, pathType: RIUTIL.TYPE_OBIEE_REPORT };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptWLPane && RIUTIL.geContentTotalByType(RIUTIL.layout, RIUTIL.TYPE_WL_PANE) == 0) {
        var item = { id: "paletteWLCompoinent", iconSrc: window["E1RES_share_images_alta_qual_e1menucontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_WL_COMPONENT_OBJ, description: RIUTIL.CONSTANTS.RI_ADD_WL_COMPONENT_OBJ, hoverText: hoverText, pathType: RIUTIL.TYPE_WL_PANE };
        contentFactory.itemList.push(item);
    }

    if (contentFactory.acceptLandingPage) {
        var item = { id: "paletteLandingPage", iconSrc: window["E1RES_share_images_alta_qual_lpcontent_24_ena_png"], name: RIUTIL.CONSTANTS.RI_LANDING_OBJ, description: RIUTIL.CONSTANTS.RI_ADD_LANDING_OBJ, hoverText: hoverText, pathType: RIUTIL.TYPE_LANDING_PANE };
        contentFactory.itemList.push(item);
    }

    return contentFactory;
}

RIUTIL.cleanupPreferenceMenu = function () {
    var ManageUDOMenuContainer = top.document.getElementById("ManageUDOMenuContainer");
    if (ManageUDOMenuContainer)
        ManageUDOMenuContainer.style.display = "none";

    var existItem = top.document.getElementById('PersonalizationTable');
    if (existItem)
        existItem.parentNode.removeChild(existItem);
}

// Private: update preference menu with either Manage Composite page option or Edit Current Form or neither (but we never show both at the same time)
RIUTIL.updatePreferenceMenu = function (isSidePanelOpened) {
    var ManageUDOMenuContainer = top.document.getElementById("ManageUDOMenuContainer");
    if (ManageUDOMenuContainer)
        ManageUDOMenuContainer.style.display = RIUTIL.isCompositePage && !isSidePanelOpened && !RIUTIL.isCompositePageAsApp ? "inline" : "none";

    if (!RIUTIL.isCompositePage && !top.isSimplifiedNavigation)
        RIUTIL.renderEditCurrentFormMenu();

    //for composite page as app remove the cafe one edit layout menu
    if (RIUTIL.isCompositePageAsApp)
        RIUTIL.renderEditCurrentFormMenu();
}

// Private: update preference to include or exclude "edit current page" option.
RIUTIL.renderEditCurrentFormMenu = function () {
    try {
        var existItem = top.document.getElementById('PersonalizationTable');
    } catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document. 
        return;
    }
    if (existItem)
        existItem.parentNode.removeChild(existItem);

    // we do not support page design mode for mobile devices, because no drag a drop behavior.
    if (RIUTIL.layoutManager && RIUTIL.layoutManager.allowCreateLayout && !RIUTIL.isTouchEnabled && !RIUTIL.isCompositePageAsApp) {
        var commonParent = top.document.getElementById('preferenceDiv');
        if (!commonParent)
            return;

        var prefDiv = commonParent.childNodes[1];
        var newItem = prefDiv.cloneNode(true);
        var personalizeTable = newItem.firstChild;
        personalizeTable.id = "PersonalizationTable";
        personalizeTable.title = RIUTIL.CONSTANTS.RI_EDIT_CURRENT_PAGE;
        personalizeTable.removeAttribute("onclick");
        personalizeTable.removeAttribute("aria-labelledby");
        // disable 'EditFormLayout' option in other UDO preview mode
        if (RIUTIL.layoutManager.isOtherUDOPreview) {
            //Disable Manage Layout link
            personalizeTable.onclick = null;
            personalizeTable.onmouseover = null;
            personalizeTable.onkeydown = null;
            //disable Manage pages link.
            RIUTIL.showManagePages(commonParent, true);
        }
        else {
            //enable Manage pages link.
            RIUTIL.showManagePages(commonParent, false);
            personalizeTable.onclick = RIUTIL.goLayoutDesignMode;
        }

        var textTD = newItem.firstChild.firstChild.firstChild.firstChild;
        if (RIUTIL.ACCESSIBILITY) {
            var textSpan = textTD.firstChild;
            textSpan.id = "EditFormLayout";
            textSpan.innerHTML = RIUTIL.CONSTANTS.RI_EDIT_CURRENT_PAGE;
        }
        else {
            textTD.innerHTML = RIUTIL.CONSTANTS.RI_EDIT_CURRENT_PAGE;
        }
        commonParent.appendChild(newItem);
    }
}

// Private: disable/enable the 'Manage Pages' Option under the  personalization section
RIUTIL.showManagePages = function (prefDiv, disable) {
    var tables = prefDiv.getElementsByClassName('MenuNormal');
    for (var i = 0, len = tables.length; i < len; i++) {
        if (tables[i].id == 'ManageCHPTable') {
            if (disable) {
                tables[i].style.display = 'none';
                tables[i].onclick = null;
                tables[i].onmouseover = null;
                tables[i].onkeydown = null;
                break;
            }
            else {
                tables[i].style.display = '';
                tables[i].setAttribute("onclick", "javascript:showHomePageManagementPane();");
                tables[i].setAttribute("onmouseover", "this.className='HoverMenuItem';");
                tables[i].setAttribute("onkeydown", "return doMenuKeyDown(this,event,false,'');");
                break;
            }
        }
    }
}

// Public: invoked by Server to updte the view based on object layout palette model from the server
RIUTIL.renderLayoutManager = function (json, resizeOnly) {
    //alert('renderLayoutList::'+json);
    // creat model
    RIUTIL.layoutManager = eval(json);
    //get productCode
    if (RIUTIL.layoutManager) {
        //alert(RIUTIL.layoutManager.isActive)
        var prodCode = RIUTIL.layoutManager.prodCode;
        RIUTIL.setProductSysCode(prodCode);
    }
    RIUTIL.updatePreferenceMenu();
    RIUTIL.updatePageDesignBar();
    RIUTIL.updateshowHideChpTabsRigthPosition();
    RIUTIL.sortLayoutList();
    RIUTIL.updateLayoutSelections();
    //enable/disable Icons
    if (RIUTIL.layoutManager) {
        var layoutId = RIUTIL.layoutManager.currentLayoutId;
        RIUTIL.updateControlEnablement(layoutId);
    }
    //disable layout dropdown in Form on preview of other UDOs
    if ((RIUTIL.layoutManager && RIUTIL.layoutManager.isOtherUDOPreview) || RIUTIL.isLcmPreview()) {
        var layoutSelectTag = document.getElementById(RIUTIL.ID_LAYOUT_SELECTION_FORM);
        if (layoutSelectTag)
            layoutSelectTag.disabled = true;
    }
    //CafeOne UDO Edit/Preview 
    if (typeof cafeOneUDOObject != 'undefined') {
        if (RIUTIL.isLcmPreview()) {
            //disable all toolbar Icons
            RIUTIL.disableToolbarICons();
            //disable the seletion list 
            var layoutSelectionTag = top.document.getElementById(RIUTIL.ID_LAYOUT_SELECTION_TOP);
            if (layoutSelectionTag)
                layoutSelectionTag.disabled = true;
        }
        //Remove the RIUTIL.isLcmPreview() check from IF, as in current design - Cafe1 layout drop down will be un-editable in preview mode.
        if (RIUTIL.isLcmEdit()) {
            var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
            //reset the isLcmEdit,isLcmPreview flag
            RIUTIL.setLcmEdit(false);
            //if the activelayout is different from the previewed layout, switch the layout, use ID
            if (activeLayout.id != cafeOneUDOObject.WebObjectId) {
                RIUTIL.cleanupAllSatelliteForms();
                RIUTIL.postAjaxRequest(null, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SELECT_CUSTOMIZED_LAYOUT, RIUTIL.PARAM_LAYOUT_ID, cafeOneUDOObject.WebObjectId);
                RIUTIL.setPageDesignBarEnable(true);
                RIUTIL.oldLayoutJSON = null;
                return true;
            }
        }
    }
    if (RIUTIL.isCompositePage && RIUTIL.lcmEdit !== undefined && !RIUTIL.lcmEdit) {
        //disable all toolbar Icons
        RIUTIL.disableToolbarICons();
        //disable the seletion list 
        var layoutSelectionTag = top.document.getElementById(RIUTIL.ID_LAYOUT_SELECTION_TOP);
        if (layoutSelectionTag) {
            layoutSelectionTag.disabled = true;
        }
        document.getElementById(RIUTIL.ID_LAYOUT_DESCRIPTION).readOnlye = true;
        document.getElementById(RIUTIL.ID_LAYOUT_PRODCODE).readOnlye = true;
    }


    // update design frame visiblity only for design mode of composite page 
    if (RIUTIL.isCompositePage && RIUTIL.layoutManager && !resizeOnly && !RIUTIL.isCompositePageAsApp)
        RIUTIL.resetCompositeFrame();

    RIUTIL.oldLayoutJSON = null; // discard the cache of workspace
    RIUTIL.adjustMessageDiv();
}

// Private: notify server the layout manager is deactive or deactive due the landing page change to E1 page 
RIUTIL.setLayoutManagerDeactive = function () {
    RIUTIL.layoutManager.isActive = false;
    RIUTIL.postAjaxRequest(null, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LAYOUT_MANAGER_DEACTIVE);
}

// Private: clean up composite frame content and set its position based on the design mode. (show for design, hide for runtime)
RIUTIL.resetCompositeFrame = function () {
    var appframe = document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME);
    if (appframe == null) {
        return;
    }
    var topFrame = document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME).contentWindow;
    var compositeFrame = topFrame.document.getElementById('wcFrameHolderNew');
    var isDesignMode = RIUTIL.layoutManager.designMode || (RIUTIL.lcmEdit !== undefined);
    if (RIUTIL.lcmEdit === undefined) {
        compositeFrame.style.left = isDesignMode ? '0%' : '100%';
        var body = compositeFrame.children[0].contentDocument.body;
        if (body && body.firstChild != null)
            body.removeChild(body.firstChild);
    }
}

// Private: sort layout by alphbet order
RIUTIL.sortLayoutList = function () {
    if (!RIUTIL.layoutManager)
        return;

    var items = RIUTIL.layoutManager.customizedLayouts;
    for (var endIndex = items.length - 1; endIndex >= 1; endIndex--) {
        for (var i = 0; i < endIndex; i++) {
            var name1 = items[i].name;
            var name2 = items[i + 1].name;
            if (name1 > name2) {
                var item = items[i];
                items[i] = items[i + 1];
                items[i + 1] = item;
            }
        }
    }
}

// Private: render or destory page design bar base the design or rutnime mode
RIUTIL.updatePageDesignBar = function () {
    if (RIUTIL.layoutManager && (RIUTIL.layoutManager.designMode || (RIUTIL.lcmEdit !== undefined)))
        RIUTIL.renderPageDesignBar();
    else
        RIUTIL.destoryPageDesignBar();
}

// Private: reposition the hide page tab UI to avoid overlapping with composite page UI of deactive frame 
RIUTIL.updateshowHideChpTabsRigthPosition = function () {
    if (!RIUTIL.isCompositePage)
        return;

    var appFrame = document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME);
    if (appFrame == null) {
        return;
    }
    var win = appFrame.contentWindow;
    var doc = win.document;
    var CHP = win.CHP;


    var showHideChpTabs = doc.getElementById("showHideChpTabs");

    if (!showHideChpTabs)
        return;

    if (RIUTIL.layoutManager && RIUTIL.layoutManager.designMode)
        showHideChpTabs.style.right = CHP.CONSTANTS_SHOW_HIDE_RIGHT_SPACER + 10 + 'px';
    else
        showHideChpTabs.style.right = CHP.CONSTANTS_SHOW_HIDE_RIGHT_SPACER + 60 + 'px'; // add more right padding for the tab to avoid overlapping with max and min icon of content frame
}

// Private: update the view of layout manager selections
RIUTIL.updateLayoutSelections = function () {
    var showSelectionTags = (RIUTIL.layoutManager != null);
    RIUTIL.setDisplayById(document, RIUTIL.ID_LAYOUT_SELECTION_SPAN, showSelectionTags);

    if (showSelectionTags) // RIAF enabled form
    {
        var layoutSpanTag = document.getElementById(RIUTIL.ID_LAYOUT_SELECTION_SPAN);
        if (layoutSpanTag) {
            var layoutLabelTag = layoutSpanTag.firstChild;
            layoutLabelTag.innerHTML = RIUTIL.CONSTANTS.RI_LAYOUT;
        }

        RIUTIL.updateLayoutSelectionTag(false, !RIUTIL.layoutManager.designMode);
        RIUTIL.updateLayoutSelectionTag(true, RIUTIL.layoutManager.designMode);
    }
}

// Private: update the one of two layout selection tag based on the layout manager model
RIUTIL.updateLayoutSelectionTag = function (isTop, enabled) {
    try {
        var tagId = isTop ? RIUTIL.ID_LAYOUT_SELECTION_TOP : RIUTIL.ID_LAYOUT_SELECTION_FORM;
        var doc = isTop ? top.document : document;
    }
    catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
        return;
    }

    var layoutSelectionTag = doc.getElementById(tagId);
    if (!layoutSelectionTag)
        return;

    if (RIUTIL.ACCESSIBILITY)
        layoutSelectionTag.title = RIUTIL.CONSTANTS.RI_LAYOUT;

    var customizedLayouts = RIUTIL.layoutManager.customizedLayouts;
    var selectedIndex = RIUTIL.layoutManager.selectedIndex;
    RIUTIL.removeAllChildren(layoutSelectionTag);

    layoutSelectionTag.onchange = RIUTIL.goSelectCustomizedLayout;
    RIUTIL.separateLayoutNames(customizedLayouts);

    // With hidden labels for header dropdown boxes, use a group option as an indicator of what the content represents.
    if (RIUTIL.isIOS) {
        AQ.createOptionGroupTag(layoutSelectionTag, RIUTIL.CONSTANTS.RI_LAYOUT);
    }

    // populate private items
    // Always populate private options because we need to add "Create New" in that section
    var personalOptionGroupTag = AQ.createOptionGroupTag(layoutSelectionTag, RIUTIL.CONSTANTS.UDO_PERSONAL);
    RIUTIL.createOptionTag(personalOptionGroupTag, RIUTIL.privateItems);
    // populate requestPublish (OR) pendingApproval Items 
    if (RIUTIL.pendingApprovalItems.length > 0) {
        var pendingApprovalOptionGroupTag = AQ.createOptionGroupTag(layoutSelectionTag, RIUTIL.CONSTANTS.UDO_PENDING_APPROVAL);
        RIUTIL.createOptionTag(pendingApprovalOptionGroupTag, RIUTIL.pendingApprovalItems);
    }
    //populate rework Items
    if (RIUTIL.reworkItems.length > 0) {
        var reworkOptionGroupTag = AQ.createOptionGroupTag(layoutSelectionTag, RIUTIL.CONSTANTS.UDO_REWORK);
        RIUTIL.createOptionTag(reworkOptionGroupTag, RIUTIL.reworkItems);
    }
    // populate reserved items
    if (RIUTIL.reservedItems.length > 0) {
        var reservedOptionGroupTag = AQ.createOptionGroupTag(layoutSelectionTag, RIUTIL.CONSTANTS.UDO_RESERVED);
        RIUTIL.createOptionTag(reservedOptionGroupTag, RIUTIL.reservedItems);
    }
    // populate public items
    if (RIUTIL.publicItems.length > 0) {
        var sharedOptionGroupTag = AQ.createOptionGroupTag(layoutSelectionTag, RIUTIL.CONSTANTS.UDO_SHARED);
        RIUTIL.createOptionTag(sharedOptionGroupTag, RIUTIL.publicItems);
    }
    RIUTIL.setEnableById(doc, tagId, enabled);
}

// Private method to distribute items array to publicItems,privateItems array
RIUTIL.separateLayoutNames = function (customizedLayouts) {
    if (customizedLayouts != null) {
        RIUTIL.privateItems = [];
        RIUTIL.pendingApprovalItems = [];
        RIUTIL.reworkItems = [];
        RIUTIL.reservedItems = [];
        RIUTIL.publicItems = [];

        for (var i = 0, len = customizedLayouts.length; i < len; i++) {
            var item = customizedLayouts[i];
            if (item.id == RIUTIL.DEFAULT_LAYOUT_ID) {
                RIUTIL.privateItems.push(item);
                continue;
            }

            if (item.isPrivate) {
                if (item.isRequestPublished) {
                    RIUTIL.pendingApprovalItems.push(item);
                }
                else if (item.isRework) {
                    RIUTIL.reworkItems.push(item);
                }
                else if (item.isPersonal) {
                    RIUTIL.privateItems.push(item);
                }
                else {
                    RIUTIL.reservedItems.push(item);
                }
            }
            else {
                RIUTIL.publicItems.push(item);
            }
        }
    }
}

// Private: utiliy method to create option tag under given option group tag with given value and display
// return the new created option tag
RIUTIL.createOptionTag = function (optionGroupTag, items) {
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        var option = document.createElement("option");

        option.value = item.id;
        option.user = item.woUser;
        var selected = (RIUTIL.layoutManager.currentLayoutId == item.id);
        if (selected) {
            if (!RIUTIL.isCompositePage || (RIUTIL.layoutManager.currentLayoutUser == item.woUser)) {
                option.setAttribute("selected", "selected");
                if (RIUTIL.isCompositePage && !RIUTIL.inProcessOfRollBackSelectionChange) {
                    document.getElementById(RIUTIL.ID_LAYOUT_DESCRIPTION).value = item.description;
                    document.getElementById(RIUTIL.ID_LAYOUT_PRODCODE).value = item.prodCode;
                }
            }
        }
        option.appendChild(document.createTextNode(item.name.replace(/ /g, "\u00a0")));
        optionGroupTag.appendChild(option);
    }
}

// Private method to retrieve user selected Layout id
RIUTIL.getCurrentSelectedLayoutId = function (e) {
    var elem = RIUTIL.getEventSource(e);
    var layoutId = elem.options[elem.options.selectedIndex].value;
    return layoutId;
}

// Private method to retrieve user selected Layout Object
RIUTIL.getCurrentSelectedLayout = function (layoutId) {
    if (RIUTIL.layoutManager) {
        var layouts = RIUTIL.layoutManager.customizedLayouts;
        for (var i = 0, len = layouts.length; i < len; i++) {
            var myLayoutId = RIUTIL.layoutManager.customizedLayouts[i].id;
            if (myLayoutId == layoutId) {
                if (!RIUTIL.isCompositePage || RIUTIL.layoutManager.customizedLayouts[i].woUser == RIUTIL.layoutManager.currentLayoutUser) {
                    return RIUTIL.layoutManager.customizedLayouts[i];
                }
            }
        }
    }
}

// Event handler called when Layout name selection list is changed.
RIUTIL.updateControlEnablement = function (layoutId) {
    //if content security access is not available, disable Create New Content,Open Existing Content icons
    if (!RIUTIL.isCompositePage && !RIUTIL.layoutManager.acceptE1Form && !RIUTIL.layoutManager.acceptOBIEE && !RIUTIL.layoutManager.acceptURL) {
        RIUTIL.disableIcon(RIUTIL.RI_NEW_CONTENT, window["E1RES_share_images_ulcm_addcontent_dis_png"], RIUTIL.CONSTANTS.RI_CREATE_CONTENT);
        RIUTIL.disableIcon(RIUTIL.RI_OPEN_CONTENT, window["E1RES_share_images_ulcm_opencontent_dis_png"], RIUTIL.CONSTANTS.RI_OPEN_CONTENT);
    }

    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
    if (activeLayout != undefined) {
        if (activeLayout.notes) {
            RIUTIL.enableIcon(RIUTIL.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_ena_png"], window["E1RES_share_images_ulcm_notes_hov_png"], RIUTIL.goFetchNotes, RIUTIL.CONSTANTS.UDO_NOTES);
        }
        else {
            RIUTIL.disableIcon(RIUTIL.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], RIUTIL.CONSTANTS.UDO_NOTES);
        }

        if (activeLayout.isPrivate) {
            if (activeLayout.id != RIUTIL.DEFAULT_LAYOUT_ID) {
                // Layout is in Reserved section : enable save, saveAs, unreserve, Request Publish Icons
                if (!activeLayout.isPersonal && !(activeLayout.isRequestPublished) && !(activeLayout.isRework)) {
                    RIUTIL.enableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window["E1RES_share_images_ulcm_save_hov_png"], RIUTIL.goSaveLayout);
                    if (activeLayout.canCopy && activeLayout.canAddToProject) {
                        RIUTIL.enableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window["E1RES_share_images_ulcm_saveas_hov_png"], RIUTIL.goSaveAsLayout); //enable saveAs Icon
                    }
                    else {
                        RIUTIL.disableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], RIUTIL.CONSTANTS.UDO_SAVE_AS); // disable saveAs Icon
                    }
                    if (activeLayout.canUnReserve && activeLayout.canRemoveFromProject) {
                        RIUTIL.enableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_unreserve_ena_png"], window["E1RES_share_images_ulcm_unreserve_hov_png"], RIUTIL.goUnReserveLayoutBegin, RIUTIL.CONSTANTS.UDO_UNRESERVE);
                    }
                    else {
                        RIUTIL.disableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_img_EUR_unreservedis_gif"], RIUTIL.CONSTANTS.UDO_UNRESERVE);
                    }
                    RIUTIL.enableIcon(RIUTIL.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_ena_png"], window["E1RES_share_images_ulcm_requestpublish_hov_png"], RIUTIL.goRequestPublishLayoutBegin, RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH);
                    RIUTIL.disableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                }
                // Layout in Pending Promotion section/Rework section : enable SaveAs, reserve Icons
                else if (activeLayout.isRequestPublished || activeLayout.isRework) {
                    RIUTIL.disableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);

                    if (activeLayout.canCopy && activeLayout.canAddToProject) {
                        RIUTIL.enableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window["E1RES_share_images_ulcm_saveas_hov_png"], RIUTIL.goSaveAsLayout); //enable saveAs Icon
                    }
                    else {
                        RIUTIL.disableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], RIUTIL.CONSTANTS.UDO_SAVE_AS); // disable saveAs Icon
                    }

                    RIUTIL.enableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_ena_png"], top['E1RES_share_images_ulcm_reserve_hov_png'], RIUTIL.goReserveLayoutBegin, RIUTIL.CONSTANTS.UDO_RESERVE);
                    RIUTIL.disableIcon(RIUTIL.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH);
                    RIUTIL.disableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                }
                else {  // personal section
                    RIUTIL.enableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], top['E1RES_share_images_ulcm_save_hov_png'], RIUTIL.goSaveLayout);

                    if (activeLayout.canCopy && activeLayout.canAddToProject) {
                        RIUTIL.enableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], top['E1RES_share_images_ulcm_saveas_hov_png'], RIUTIL.goSaveAsLayout); //enable saveAs Icon
                    }
                    else {
                        RIUTIL.disableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], RIUTIL.CONSTANTS.UDO_SAVE_AS); // disable saveAs Icon
                    }
                    RIUTIL.disableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], RIUTIL.CONSTANTS.UDO_RESERVE);
                    RIUTIL.enableIcon(RIUTIL.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_ena_png"], top['E1RES_share_images_ulcm_requestpublish_hov_png'], RIUTIL.goRequestPublishLayoutBegin, RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH);
                    // if NO allowed actions for the user to CanDelete or CanRemoveObjectFromProject disable delete icon
                    if (activeLayout.canDelete && activeLayout.canRemoveFromProject) {
                        RIUTIL.enableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_ena_png"], top['E1RES_share_images_ulcm_delete_hov_png'], RIUTIL.goDeleteLayout);
                    }
                    else {
                        RIUTIL.disableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                    }
                }
            }
            else {
                //layout is selected with default
                if (activeLayout.canAdd && activeLayout.canAddToProject) {
                    RIUTIL.enableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], top['E1RES_share_images_ulcm_save_hov_png'], RIUTIL.goSaveLayout);
                }
                else {
                    RIUTIL.disableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
                }
                RIUTIL.disableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], RIUTIL.CONSTANTS.UDO_SAVE_AS); // disable saveAs Icon
                RIUTIL.disableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], RIUTIL.CONSTANTS.UDO_RESERVE);
                RIUTIL.disableIcon(RIUTIL.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH);
                RIUTIL.disableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                RIUTIL.disableIcon(RIUTIL.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], RIUTIL.CONSTANTS.UDO_NOTES);
            }
        }
        else {
            //layout is in shared Layouts section
            RIUTIL.disableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
            if (activeLayout.canCopy && activeLayout.canAddToProject) {
                RIUTIL.enableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], top['E1RES_share_images_ulcm_saveas_hov_png'], RIUTIL.goSaveAsLayout); //enable saveAs Icon
            }
            else {
                RIUTIL.disableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], RIUTIL.CONSTANTS.UDO_SAVE_AS); // disable saveAs Icon
            }

            if (activeLayout.canCheckout && !activeLayout.isCheckedOut) {
                RIUTIL.enableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_ena_png"], top['E1RES_share_images_ulcm_reserve_hov_png'], RIUTIL.goReserveLayoutBegin, RIUTIL.CONSTANTS.UDO_RESERVE);
            }
            else {
                RIUTIL.disableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], RIUTIL.CONSTANTS.UDO_RESERVE);
            }
            RIUTIL.disableIcon(RIUTIL.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH);
            RIUTIL.disableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
        }
    }
}

// Private: create a icon cell for palette
RIUTIL.createPaletteIconCell = function (doc, containerId, row, item, setDefaultContent) {
    var cell = row.insertCell(-1);
    cell.className = "RIPaletteIconColumn";
    var innerClassName = "RIPaletteIcon";
    var div = RIUTIL.createPaletteDiv(doc, item.id, item.iconSrc, item.hoverText, innerClassName);
    cell.appendChild(div);
}

// Private: create a text cell for palette
RIUTIL.createPaletteTextCell = function (doc, containerId, row, item) {
    var cell = row.insertCell(-1);
    cell.className = "RIPaletteTextColumn";
    var table = RIUTIL.createTable();
    cell.appendChild(table);

    // name
    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.className = "RINoWrap RITitle";
    var nameElem = document.createTextNode(item.name);
    cell.appendChild(nameElem);

    // descrioption
    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.style.whiteSpace = "normal";
    var descriptionElem = document.createTextNode(item.description);
    cell.appendChild(descriptionElem);

    // timestamp for content only
    if (item.createDate) {
        var row = table.insertRow(-1);
        var cell = row.insertCell(-1);
        var createTime = "" + item.createTime;
        if (createTime.length == 6)
            var timeString = createTime.substring(0, 2) + ":" + createTime.substring(2, 4) + ":" + createTime.substring(4, 6);
        else
            var timeString = createTime.substring(0, 1) + ":" + createTime.substring(1, 3) + ":" + createTime.substring(3, 5);

        var timestampElem = document.createTextNode(item.createDate + " " + timeString);
        cell.appendChild(timestampElem);
    }
}

// Private: create a text cell for palette
RIUTIL.createPaletteTextCellForLayoutName = function (doc, containerId, row, layout) {
    var cell = row.insertCell(-1);
    cell.colSpan = 2;
    var table = RIUTIL.createTable();
    cell.appendChild(table);

    // name
    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.className = "RINoWrap RIGroupHeading";
    var nameElem = document.createTextNode(layout.name + "  (" + layout.status + ")");
    cell.appendChild(nameElem);

}

// Private: create a text cell for palette
/*RIUTIL.createPaletteActionCell = function (doc, containerId, row, item, id)
{
var cell = row.insertCell(-1);
var img = document.createElement("img");
cell.appendChild(img);
cell.style.width = "20px";
img.objectId = item.id;
img.className = "RIActionImage";
img.id = id;
img.style.display = "none";
if(item.allowDelete)
{
JSSidePanelRender.setMotionImage(img, top['E1RES_img_RI_delete_gif'], top['E1RES_img_RI_deletemo_gif'], RIUTIL.goRemoveContent);
img.title = RIUTIL.CONSTANTS.RI_HOVER_TEXT_REMOVE;
}
else
img.src = top['E1RES_img_RI_deletedis_gif'];
}*/


// Private: to create generic div tag for icon of palette
RIUTIL.createPaletteDiv = function (doc, id, src, title, normalClassName) {
    var div = doc.createElement("DIV");
    div.id = id;
    div.style.backgroundImage = "url('" + src + "')";
    div.title = title;
    div.className = normalClassName;
    return div;
}

// Private: render page design bar
RIUTIL.renderPageDesignBar = function () {
    var doc = top.document;
    var barDiv = doc.getElementById("PerseonalizationBar");
    if (barDiv)
        barDiv.parentNode.removeChild(barDiv);
    else
        var fromHideToShow = true;

    var topnav = doc.getElementById('topnav');
    var normMenu = doc.getElementById('normMenu');
    var caroHolder = doc.getElementById('caroHolder');

    var topBarHeight = caroHolder ? caroHolder.offsetTop - 2 : 0; //TODO: in E1Page mode, the carousel does not exist, we need cover both banner and E1 page tab bar.
    var breadcrumbBar = doc.getElementById("breadcrumbBar");
    var endTopOffset = -1 * (62 + (breadcrumbBar ? breadcrumbBar.offsetHeight : 0));
    var topBarWidth = topnav.offsetWidth;
    var barDiv = doc.createElement("DIV");
    barDiv.className = "e1gridtoolbar";
    if (breadcrumbBar && breadcrumbBar.offsetHeight > 0)
        barDiv.className += " withBreadcrumbs";
    var startTopOffset = endTopOffset - topBarHeight;
    barDiv.id = "PerseonalizationBar";
    var barDivHeight = endTopOffset + 5;
    doc.body.appendChild(barDiv);

    var table = doc.createElement("table");
    table.id = RIUTIL.ID_DESIGN_ACTIONS;
    table.className = RIUTIL.CLASS_ENABLED_DESIGN_ACTIONS;
    barDiv.appendChild(table);

    // remove all the children the tab container, breadcrumb and hide carousel from design mode of composite page and they will be re-rendered during full page refresh when come back from design to runtime mode
    if (RIUTIL.isCompositePage) {
        var e1FrameDoc = document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME).contentWindow.document;

        var tabBar = e1FrameDoc.getElementById('tabContainer');
        if (tabBar)
            RIUTIL.removeAllChildren(tabBar);

        var bchistory = doc.getElementById('bchistory');
        if (bchistory)
            RIUTIL.removeAllChildren(bchistory);

        var carousel = doc.getElementById('carousel');
        if (carousel)
            carousel.style.visibility = "hidden";

        var showHideChpTabs = e1FrameDoc.getElementById('showHideChpTabs');
        if (showHideChpTabs)
            showHideChpTabs.style.display = "none";
    }

    var row = table.insertRow(-1);

    var cell = row.insertCell(-1);
    RIUTIL.renderLogo(doc, cell);

    // content section
    if (RIUTIL.lcmEdit === undefined || RIUTIL.lcmEdit) {
        RIUTIL.renderContentActions(doc, row);
    }
    RIUTIL.renderMessageArea(doc, row);
    //show token proj name and reserved by
    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);

    if (RIUTIL.lcmEdit !== undefined) {
        RIUTIL.renderMessage(false);
    }
    else {
        if (activeLayout.isPrivate) {
            RIUTIL.renderMessage(false);
        }
        else {
            if (activeLayout.reservedBy != undefined && activeLayout.reservedBy.Trim().length > 0) {
                var reservedByText = RIUTIL.CONSTANTS.EUR_RESERVED_BY.format(activeLayout.reservedBy);
                RIUTIL.renderMessage(true, reservedByText, false);
            }
            else if (activeLayout.hasToken) {
                var tokenHeldByProjectText = RIUTIL.CONSTANTS.UDO_TOKEN_BY.format(activeLayout.tokenProjectName);
                RIUTIL.renderMessage(true, tokenHeldByProjectText, false);
            }
        }
    }

    // layout section
    RIUTIL.renderLayoutActions(doc, row);

    // animiate slide in if this is NOT LCM preview/edit mode
    if (RIUTIL.lcmEdit === undefined) {
        if (RIUTIL.supportAnimation && fromHideToShow)
            top.ANIM.animate(barDiv, 'sinSlideY', startTopOffset, endTopOffset, RIUTIL.ANIMATION_DURATION, 0, "RIUTIL.adjustMessageDiv()");
    }
    //start a new thread to keep the server session alive
    if (RIUTIL.isCompositePage) {
        if (RIUTIL.keepAliveTimer != -1) {
            clearInterval(RIUTIL.keepAliveTimer);
        }
        RIUTIL.keepAliveTimer = setInterval(RIUTIL.keepAlive, RIUTIL.layoutManager.keepAliveInterval);
    }
}

// Private: adjust the message div size to fit as much as it can in the screen without push other UI of window.
RIUTIL.adjustMessageDiv = function () {
    var messageDiv = top.document.getElementById(RIUTIL.ID_MESSAGE_DIV);
    if (messageDiv) {
        messageDiv.style.width = '0px';
        messageDiv.style.width = Math.max(50, messageDiv.parentNode.offsetWidth - 4) + "px";
    }
}

// Private: render oracle logo for page design mode
RIUTIL.renderLogo = function (doc, parentNode) {
    var table = doc.createElement("table");
    parentNode.appendChild(table);
    table.className = "AFBrandingBar";
    parentNode.appendChild(table);

    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    var logo = doc.createElement("img");
    logo.src = window["E1RES_share_images_alta_oracle_logo_sm_png"];
    logo.id = 'oracleImage';
    cell.appendChild(logo);

    cell = row.insertCell(-1);
    var e1ProductName = doc.createElement("span");
    e1ProductName.className = "appname";
    e1ProductName.appendChild(doc.createTextNode(RIUTIL.CONSTANTS.JH_LOGINTITLE));
    cell.appendChild(e1ProductName);
}

// Private: render all content related actions
RIUTIL.renderContentActions = function (doc, row) {
    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
    if (RIUTIL.layoutManager.allowCreateLayout && !activeLayout.isPublished && !activeLayout.isRework && !activeLayout.isRequestPublished && !RIUTIL.isLcmPreview()) {
        // new content
        RIUTIL.renderActionIcon(doc, row, "RI_NEW_CONTENT", window["E1RES_share_images_ulcm_addcontent_ena_png"], window["E1RES_share_images_ulcm_addcontent_hov_png"], RIUTIL.CONSTANTS.RI_CREATE_CONTENT, RIUTIL.goShowContentFactory);
    }

    if (RIUTIL.layoutManager.allowCreateLayout && !activeLayout.isPublished && !activeLayout.isRework && !activeLayout.isRequestPublished && !RIUTIL.isLcmPreview()) {
        // open existing content
        RIUTIL.renderActionIcon(doc, row, "RI_OPEN_CONTENT", window["E1RES_share_images_ulcm_opencontent_ena_png"], window["E1RES_share_images_ulcm_opencontent_hov_png"], RIUTIL.CONSTANTS.RI_OPEN_CONTENT, RIUTIL.goShowContentPalette);
    }
}

RIUTIL.renderMessageArea = function (doc, row) {
    cell = row.insertCell(-1);
    cell.setAttribute("valign", "center");
    var messageDiv = doc.createElement("div");
    messageDiv.className = "SPMessageDiv";
    messageDiv.id = RIUTIL.ID_MESSAGE_DIV;
    cell.style.width = "100%";
    messageDiv.style.textAlign = "center";
    messageDiv.style.display = "none";
    messageDiv.style.overflow = "hidden";
    messageDiv.style.textOverflow = "ellipsis";
    messageDiv.style.whiteSpace = "nowrap";
    messageDiv.style.padding = "5px 0px";
    cell.appendChild(messageDiv);
}

// show or hide the message div
RIUTIL.renderMessage = function (showMessage, message, isError) {
    var messageDiv = top.document.getElementById(RIUTIL.ID_MESSAGE_DIV);
    if (showMessage) {
        RIUTIL.removeAllChildren(messageDiv);

        if (RIUTIL.ACCESSIBILITY)
            messageDiv.setAttribute("role", "alert")
        RIUTIL.adjustMessageDiv();
        messageDiv.appendChild(document.createTextNode(message));
        messageDiv.title = message;
        messageDiv.style.borderColor = (isError) ? "Red" : "Blue";
        //set text Color in message div
        messageDiv.style.color = "#4A598C";
        messageDiv.style.display = "block";
    }
    else {
        messageDiv.style.display = "none";
        return;
    }
}

// Private: render all layout related actions
RIUTIL.renderLayoutActions = function (doc, row) {
    //layout name
    var tCell = row.insertCell(-1);
    tCell.className = "RIAction";
    tCell.style.cursor = "default";
    tCell.style.width = '45px';
    tCell.appendChild(document.createTextNode(RIUTIL.CONSTANTS.UDO_LABEL_NAME));
    // layout list
    var td = row.insertCell(-1);
    td.className = "RIAction";
    var selectLayout = doc.createElement("select");
    selectLayout.id = RIUTIL.ID_LAYOUT_SELECTION_TOP;
    selectLayout.style.width = '120px';
    //For Accesibility Users render Label for the select tag
    if (RIUTIL.ACCESSIBILITY) {
        selectLayout.setAttribute("aria-label", "Select Layout");
    }
    td.appendChild(selectLayout);

    //layout description and product code
    if (RIUTIL.isCompositePage) {
        //description
        var tCell = row.insertCell(-1);
        tCell.className = "RIAction";
        tCell.style.cursor = "default";
        tCell.style.width = '70px';
        tCell.appendChild(document.createTextNode(RIUTIL.CONSTANTS.UDO_LABEL_DESCRIPTION));
        // layout list
        var td = row.insertCell(-1);
        td.className = "RIAction";
        var description = doc.createElement("input");
        description.id = RIUTIL.ID_LAYOUT_DESCRIPTION;
        description.style.width = '120px';
        description.maxLength = 200;
        description.onchange = RIUTIL.onChnageLayoutDesicription;
        td.appendChild(description);

        //product code
        var tCell = row.insertCell(-1);
        tCell.className = "RIAction";
        tCell.style.cursor = "default";
        tCell.style.width = '70px';
        tCell.appendChild(document.createTextNode(RIUTIL.CONSTANTS.CP_PRODUCT_CODE));
        // layout list
        var td = row.insertCell(-1);
        td.className = "RIAction";
        var prodCode = doc.createElement("input");
        prodCode.id = RIUTIL.ID_LAYOUT_PRODCODE;
        prodCode.style.width = '25px';
        prodCode.maxLength = 4;
        prodCode.onchange = RIUTIL.onChnageLayoutProdCode;
        td.appendChild(prodCode);
    }

    // save button for perseonal layout only
    RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_SAVE_ICON, top['E1RES_share_images_ulcm_save_ena_png'], top['E1RES_share_images_ulcm_save_hov_png'], RIUTIL.CONSTANTS.UDO_SAVE, RIUTIL.goSaveLayout);
    // save as button
    RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_SAVE_AS_ICON, top['E1RES_share_images_ulcm_saveas_ena_png'], top['E1RES_share_images_ulcm_saveas_hov_png'], RIUTIL.CONSTANTS.UDO_SAVE_AS, RIUTIL.goSaveAsLayout);

    // req publish as button
    if (RIUTIL.layoutManager.allowPublishLayout) {
        RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_REQUEST_PUBLISH_ICON, top['E1RES_share_images_ulcm_requestpublish_ena_png'], top['E1RES_share_images_ulcm_requestpublish_hov_png'], RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH, RIUTIL.goRequestPublishLayoutBegin);
    }
    // reserve button
    if (RIUTIL.layoutManager.allowReserveLayout) {
        RIUTIL.renderActionIcon(doc, row, RIUTIL.RI_RESERVE_ICON, top['E1RES_share_images_ulcm_reserve_ena_png'], top['E1RES_share_images_ulcm_reserve_hov_png'], RIUTIL.CONSTANTS.UDO_RESERVE, RIUTIL.goReserveLayoutBegin);
    }
    // delete button
    //    if(RIUTIL.layoutManager.allowDeleteLayout)
    //{
    RIUTIL.renderActionIcon(doc, row, RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_ena_png"], window["E1RES_share_images_RI_deletemo_gif"], RIUTIL.CONSTANTS.UDO_DELETE, RIUTIL.goDeleteLayout);
    //    }
    // notes button
    RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_ena_png"], window["E1RES_share_images_ulcm_notes_hov_png"], RIUTIL.CONSTANTS.UDO_NOTES, RIUTIL.goFetchNotes);

    // About button
    var aboutText = RIUTIL.isCompositePage ? RIUTIL.CONSTANTS.JS_SIDE_PANEL_CMP_HOVER : RIUTIL.CONSTANTS.JS_SIDE_PANEL_RI_HOVER;
    RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_RI_ABOUT_DESIGN, top['E1RES_share_images_ulcm_info_ena_png'], top['E1RES_share_images_ulcm_info_hov_png'], aboutText, RIUTIL.about);
    // close button
    if (RIUTIL.lcmEdit !== undefined) {
        RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_RI_CLOSE_DESIGN, window["E1RES_share_images_ulcm_close_ena_png"], window["E1RES_share_images_ulcm_close_hov_png"], RIUTIL.CONSTANTS.JH_CLOSE, RIUTIL.exitLCMMode);
    }
    else {
        RIUTIL.renderActionIcon(doc, row, RIUTIL.ID_RI_CLOSE_DESIGN, window["E1RES_share_images_ulcm_close_ena_png"], window["E1RES_share_images_ulcm_close_hov_png"], RIUTIL.CONSTANTS.JH_CLOSE, RIUTIL.supportAnimation ? RIUTIL.goLayoutRuntimeModeBegin : RIUTIL.goLayoutRuntimeMode);
    }
}

// Private: render the action icon for page design mode
RIUTIL.renderActionIcon = function (doc, row, id, src, overSrc, title, onClickHandler) {
    var cell = row.insertCell(-1);
    cell.normalClassName = cell.className = "RIAction";
    cell.hoverClassName = "RIAction";
    cell.id = id;
    cell.name = RIUTIL.NAME_ACTION;

    var img = doc.createElement("img");
    img.src = src;
    img.outSrc = src;
    img.overSrc = overSrc;
    if (RIUTIL.ACCESSIBILITY)
        img.setAttribute("role", "link");

    img.onclick = img.onclickHandler = onClickHandler;
    img.onmouseover = img.onmouseoverHandler = RIUTIL.onMouseOverActionEvent;
    img.onmouseout = RIUTIL.onMouseOutActionEvent;

    cell.appendChild(img);

    if (title) {
        img.title = title;
        img.alt = title;
    }
}


// Private: confirm user if want to continue with risk of loss unsaved change
RIUTIL.cancelActionForDirtyCheck = function () {
    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
    return (RIUTIL.layoutManager.isDirty && !activeLayout.isRequestPublished && !activeLayout.isRework && !activeLayout.isPublished && !confirm(RIUTIL.CONSTANTS.RI_CONFIRM_DISCARD));
}

// Private: tear down page disign bar
RIUTIL.destoryPageDesignBar = function () {
    //stop the thread in runtime mode
    if (RIUTIL.isCompositePage) {
        clearInterval(RIUTIL.keepAliveTimer);
    }
    try {
        var doc = top.document;
        var perseonalizationBar = doc.getElementById('PerseonalizationBar');
        if (perseonalizationBar) {
            perseonalizationBar.parentNode.removeChild(perseonalizationBar);
        }
    } catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document. 
    }
}

// Private: enable/disable all the action on the page design 
RIUTIL.setPageDesignBarEnable = function (enable) {
    var doc = top.document;
    var pageDesignActions = doc.getElementById(RIUTIL.ID_DESIGN_ACTIONS);
    if (pageDesignActions) {
        pageDesignActions.className = enable ? RIUTIL.CLASS_ENABLED_DESIGN_ACTIONS : RIUTIL.CLASS_DISABLED_DESIGN_ACTIONS;
        var cells = pageDesignActions.getElementsByTagName("TD");
        for (var i = 0; i < cells.length; i++) {
            var cell = cells[i];
            if (cell.name == RIUTIL.NAME_ACTION) {
                if (cell.onclickHandler) {
                    cell.onclick = enable ? cell.onclickHandler : null;
                    cell.onmouseover = enable ? cell.onmouseoverHandler : null;
                }
                else {
                    cell.firstChild.onclick = enable ? cell.firstChild.onclickHandler : null;
                    cell.firstChild.onmouseover = enable ? cell.firstChild.onmouseoverHandler : null;
                }
            }
        }
    }
}

/*
// Private: return true if the given name was already used in the existing layout list of the form
RIUTIL.existLayoutName = function (newName)
{
var customizedLayouts = RIUTIL.layoutManager.customizedLayouts;        
for (i=0; i<customizedLayouts.length; i++)
{
var name = customizedLayouts[i].name;
if(name == newName)
return true;
}
return false;
}
*/

// Private: init the client model for object type palette
RIUTIL.createContentPaletteModel = function (json, hoverText) {
    var contentPalette = eval(json);
    var itemList = RIUTIL.sortContentList(contentPalette.itemList);

    for (var i = 0; i < itemList.length; i++) {
        var item = itemList[i];
        switch (item.pathType) {
            case RIUTIL.TYPE_GENERIC:
                item.iconSrc = window["E1RES_share_images_alta_qual_urlcontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_OBIEE_REPORT:
            case RIUTIL.TYPE_OBIEE_DASHBOAD:
            case RIUTIL.TYPE_OBIEE_PAGE:
            case RIUTIL.TYPE_OBIEE_FOLDER:
                item.iconSrc = window["E1RES_share_images_alta_qual_obieecontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_E1_FORM:
                item.iconSrc = window["E1RES_share_images_alta_qual_e1formcontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_FREESTYLE_FRAME:
                item.iconSrc = window["E1RES_share_images_alta_qual_freestylecontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_WL_PANE:
                item.iconSrc = window["E1RES_share_images_alta_qual_e1menucontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_LANDING_PANE:
                item.iconSrc = window["E1RES_share_images_alta_qual_freestylecontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_OVR_FRAME:
                item.iconSrc = window["E1RES_share_images_alta_qual_ovrcontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_E1_PAGE_FRAME:
                item.iconSrc = window["E1RES_share_images_alta_qual_e1pagecontent_24_ena_png"];
                break;
            case RIUTIL.TYPE_ADF_FRAME:
                item.iconSrc = window["E1RES_share_images_alta_qual_dvtcontent_24_ena_png"];
                break;
        }

        item.hoverText = hoverText;
    }

    return contentPalette;
}

// Private: sort  content item list in alphbet order (type + name + descirption + timestamp)
RIUTIL.sortContentList = function (itemList) {
    for (var endIndex = itemList.length - 1; endIndex >= 1; endIndex--) {
        for (var i = 0; i < endIndex; i++) {
            var item1 = itemList[i];
            var item2 = itemList[i + 1];
            if (RIUTIL.getHashName(item1) > RIUTIL.getHashName(item2)) {
                itemList[i] = item2;
                itemList[i + 1] = item1;
            }
        }
    }
    return itemList;
}

// Private: get a hash name used for ordering (type + name + descirption + timestamp)
RIUTIL.getHashName = function (item) {
    return "" + item.pathType + item.name + item.description + item.updateDate + item.updateTime;
}

// Private: calcuate the height needed to display the content palette or content factory based on the total number of item
// but there is minimal height and maximal height
RIUTIL.caculatePaletteHeight = function (itemTotal, numberOfLayouts) {
    var maxHeight = Math.min(RIUTIL.PALETTE_MAX_HEIGHT, RIUTIL.getClientHeight());
    return Math.max(Math.min(maxHeight, RIUTIL.PALETTE_PADDING_HEIGHT + itemTotal * RIUTIL.PALETTE_AVERAGE_HEIGHT + numberOfLayouts * 20), RIUTIL.PALETTE_MIN_HEIGHT);
}

// Public: display content factory based on the server model
RIUTIL.renderContentFactory = function (contentFactoryJSON) {
    var setDefaultContent = RIUTIL.layout.isBlank;
    var containerId = RIUTIL.CONTAINER_ID_FACTORY;
    var popupTitle = setDefaultContent ? RIUTIL.CONSTANTS.RI_SEL_DEFAULT_CONTENT : RIUTIL.CONSTANTS.RI_DRAG_CREATE;

    // creat model
    var contentFactory = RIUTIL.createContentFactoryModel(contentFactoryJSON, popupTitle);
    var clientWidth = document.body.clientWidth;
    var clientHeight = RIUTIL.getClientHeight();
    var winWidth = RIUTIL.PALETTE_WIDTH;
    var winHeight = RIUTIL.caculatePaletteHeight(contentFactory.itemList.length, 0);
    var winTop = clientHeight / 2 - winHeight / 2;
    var winLeft = clientWidth / 2 - winWidth / 2;
    var eventListener = {};
    eventListener.onClose = function (e) {
        JSSidePanelRender.hideUIBlock();
        RIUTIL.setPageDesignBarEnable(true);
    }

    // generate content for content factory
    var framework = "<div id=RIContentFactoryDiv style='overflow:auto'><table id=RIContentFactoryTable ></table></div>";

    JSSidePanelRender.showUIBlock();

    var win = createPopup("CONTENT_FACTORY", SHOW_POPUP, true, false, true, false, popupTitle, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, null, framework, eventListener);
    RIUTIL.contentFactoryWin = win;

    var table = document.getElementById("RIContentFactoryTable");
    table.popupWin = win;
    table.containerId = containerId;
    table.className = RIUTIL.CLASS_LAYOUT_TABLE;
    table.setAttribute('cellPadding', 0);
    table.setAttribute('cellSpacing', 0);

    for (var i = 0; i < contentFactory.itemList.length; i++) {
        var item = contentFactory.itemList[i];
        var row = RIUTIL.createPalletteRowForContent(table, containerId, item, setDefaultContent);
        RIUTIL.createPaletteIconCell(document, containerId, row, item, setDefaultContent);
        RIUTIL.createPaletteTextCell(document, containerId, row, item);
    }

    var div = document.getElementById("RIContentFactoryDiv");
    div.style.height = winHeight - 40 + "px";
    div.style.width = winWidth + "px";
}

// Public: invoked by Server to render the content palette
RIUTIL.renderContentPalette = function (json) {
    var setDefaultContent = RIUTIL.layout.isBlank;
    var popupTitle = setDefaultContent ? RIUTIL.CONSTANTS.RI_SEL_EXISTING_CONTENT : RIUTIL.CONSTANTS.RI_DRAG_ACTIVATE;
    var contentPalette = RIUTIL.createContentPaletteModel(json, popupTitle);
    var itemList = contentPalette.itemList;
    var containerId = RIUTIL.CONTAINER_ID_PALETTE;

    var layoutList = RIUTIL.groupContentsByLayout(itemList);

    var clientWidth = document.body.clientWidth;
    var clientHeight = RIUTIL.getClientHeight();
    var winWidth = RIUTIL.PALETTE_WIDTH;
    var winHeight = RIUTIL.caculatePaletteHeight(itemList.length, layoutList.length);
    var winTop = clientHeight / 2 - winHeight / 2;
    var winLeft = clientWidth / 2 - winWidth / 2;
    var eventListener = {};
    eventListener.onClose = function (e) {
        JSSidePanelRender.hideUIBlock();
        RIUTIL.setPageDesignBarEnable(true);
    }

    // generate content for content factory
    var framework = "<div id=RIContentPaletteDiv style='overflow:auto'><table style='width:100%;' id=RIContentPaletteTable ><tr><td>" + RIUTIL.CONSTANTS.RI_EMPTY_CONTENT_PALETTE + "</td></tr></table></div>";

    JSSidePanelRender.showUIBlock();

    var win = createPopup("CONTENT_PALETTE", SHOW_POPUP, true, false, true, false, popupTitle, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, null, framework, eventListener);
    win.winWidth = winWidth;
    win.winHeight = winHeight;
    RIUTIL.contentPaletteWin = win;

    var table = document.getElementById("RIContentPaletteTable");
    table.popupWin = win;
    table.containerId = containerId;
    table.className = RIUTIL.CLASS_LAYOUT_TABLE;
    table.setAttribute('cellPadding', 0);
    table.setAttribute('cellSpacing', 0);

    if (itemList.length > 0 && layoutList.length > 0) {
        table.deleteRow(0);
        for (var i = 0; i < layoutList.length; i++) {
            if (layoutList[i].contentList.length > 0) {
                var row = RIUTIL.createPalletteRowForLayoutName(table);
                RIUTIL.createPaletteTextCellForLayoutName(document, containerId, row, layoutList[i]);
            }
            for (var j = 0; j < layoutList[i].contentList.length; j++) {
                var item = layoutList[i].contentList[j];
                // do not allow user to add second ADF or OVR content if there is one already in the page
                var allowToAdd = (item.pathType == RIUTIL.TYPE_ADF_FRAME && RIUTIL.geContentTotalByType(RIUTIL.layout, RIUTIL.TYPE_ADF_FRAME) == 0)
                                 || (item.pathType == RIUTIL.TYPE_OVR_FRAME && RIUTIL.geContentTotalByType(RIUTIL.layout, RIUTIL.TYPE_OVR_FRAME) == 0)
                                 || (item.pathType != RIUTIL.TYPE_ADF_FRAME && item.pathType != RIUTIL.TYPE_OVR_FRAME)
                if (allowToAdd) {
                    var row = RIUTIL.createPalletteRowForContent(table, containerId, item, setDefaultContent);
                    // row.deleteContentIconId = "RIContentDelete" + i + "_" + j;
                    RIUTIL.addEvent(row, "mouseover", RIUTIL.onMouseOverContentPaletteRow);
                    RIUTIL.addEvent(row, "mouseout", RIUTIL.onMouseOutContentPaletteRow);

                    RIUTIL.createPaletteIconCell(document, containerId, row, item, setDefaultContent);
                    RIUTIL.createPaletteTextCell(document, containerId, row, item);
                }
            }
        }
    }

    var div = document.getElementById("RIContentPaletteDiv");
    div.style.height = winHeight - 47 + "px";
    div.style.width = winWidth + "px";
}

// Private: get total number of subcontainer in this container which is ADF Container.
RIUTIL.geContentTotalByType = function (container, pathTypeFilter) {
    var total = 0;
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            if (container.subContainers) {
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    total += RIUTIL.geContentTotalByType(subContainer, pathTypeFilter);
                }
            }
            break;

        case RIUTIL.LAYOUT_SINGLE: // singluar page
            if (!container.frame)
                return 0;

            var pathType = container.frame.pathType;
            if (pathTypeFilter == RIUTIL.TYPE_ADF_FRAME && pathType == RIUTIL.TYPE_E1_FORM && container.frame.subType == "ADFAPP") // EXAPP ADF is also count
                return 1;

            return (pathType == pathTypeFilter ? 1 : 0)
            break;
    }
    return total;
}

RIUTIL.groupContentsByLayout = function (itemList) {
    //create a two dimensional array - kind of map to list of content array
    /*
    [
    layout1: [c1, c2, c3]
    layout2: [c1, c2]
    layout3: [c1, c2, c3, c1, c2, c3]
    ]
    */
    var layoutArray = [];
    var customizedLayouts = RIUTIL.layoutManager.customizedLayouts;
    var layoutIndex = 0;
    var dynamicContentList = itemList.slice(0);
    for (var index = 0; index < customizedLayouts.length; index++) {
        // ignore the (simple) layout
        if (RIUTIL.DEFAULT_LAYOUT_ID == customizedLayouts[index].id || customizedLayouts[index].id == RIUTIL.layoutManager.currentLayoutId)
            continue;
        // Ignore rework and request published layout content
        if (customizedLayouts[index].isRework || customizedLayouts[index].isRequestPublished)
            continue;
        var layout = new Object();
        layout.obnm = customizedLayouts[index].omwObjectName;
        layout.name = customizedLayouts[index].name;
        layout.status = RIUTIL.getLayoutStatus(customizedLayouts[index]);
        layout.contentList = RIUTIL.getAllContentsforLayout(layout, dynamicContentList);
        if (layout.contentList.length > 0)
            layoutArray[layoutIndex++] = layout;
    }
    return layoutArray;
}

RIUTIL.getLayoutStatus = function (layout) {
    var status = "";
    if (layout.isPrivate) {
        if (layout.isRequestPublished) {
            status = RIUTIL.CONSTANTS.UDO_PENDING_APPROVAL;
        }
        else if (layout.isCheckedOut && !layout.isPersonal && !layout.isRequestPublished && !layout.isRework) {
            status = RIUTIL.CONSTANTS.UDO_RESERVED;
        }
        else if (layout.isRework) {
            status = RIUTIL.CONSTANTS.UDO_REWORK;
        }
        else {
            status = RIUTIL.CONSTANTS.UDO_PERSONAL;
        }
    }
    else {
        status = RIUTIL.CONSTANTS.UDO_SHARED;
    }
    return status;
}

/*
get All contents for a layout from the contents list by layout ID
*/
RIUTIL.getAllContentsforLayout = function (layout, itemList) {
    var contentList = [];
    for (var i = 0, len = itemList.length; i < len; i++) {
        if (typeof itemList[i] != 'undefined' && itemList[i].layoutId == layout.obnm) {
            var pathType = itemList[i].pathType;
            var isURL = pathType == RIUTIL.TYPE_GENERIC;
            var isOBIEE = pathType == RIUTIL.TYPE_OBIEE_REPORT || pathType == RIUTIL.TYPE_OBIEE_DASHBOAD || pathType == RIUTIL.TYPE_OBIEE_PAGE || pathType == RIUTIL.TYPE_OBIEE_FOLDER;
            var isE1Form = pathType == RIUTIL.TYPE_E1_FORM;
            var isOVR = pathType == RIUTIL.TYPE_OVR_FRAME;
            var isWebCenterSpace = pathType == RIUTIL.TYPE_WEBCENTER_SPACE;
            var isE1Page = pathType == RIUTIL.TYPE_E1_PAGE_FRAME;
            var isADFFrame = pathType == RIUTIL.TYPE_ADF_FRAME

            //filter per content security
            if ((isURL && RIUTIL.layoutManager.acceptURL)
                || (isOBIEE && RIUTIL.layoutManager.acceptOBIEE)
                || (isE1Form && RIUTIL.layoutManager.acceptE1Form)
                || (isOVR && RIUTIL.layoutManager.acceptOVR)
                || (isE1Page && RIUTIL.layoutManager.acceptE1Page)
                || (isADFFrame && RIUTIL.layoutManager.acceptADFFrame)
                || isWebCenterSpace) {
                if ((layout.status == RIUTIL.CONSTANTS.UDO_SHARED && itemList[i].layoutUser == RIUTIL.PUBLIC_LAYOUT_ID) || (layout.status == RIUTIL.CONSTANTS.UDO_RESERVED && itemList[i].layoutUser != RIUTIL.PUBLIC_LAYOUT_ID)
                || (layout.status == RIUTIL.CONSTANTS.UDO_PERSONAL)) {
                    contentList[contentList.length] = itemList[i];
                }
            }
        }
    }
    itemList = null;

    return contentList;
}
RIUTIL.createPalletteRowForContent = function (table, containerId, item, setDefaultContent) {
    var row = table.insertRow(-1);
    var normalClassName = "RIPaletteItemRow " + (setDefaultContent ? "RIClickable " : "RIDragable ");
    RIUTIL.attachHoverEffect(row, normalClassName, normalClassName + "RIPaletteItemRowHover");

    row.isContentFactory = true;
    row.pathType = item.pathType;
    row.containerId = containerId;
    row.objectName = item.name;
    row.objectId = item.id;
    if (setDefaultContent) {
        RIUTIL.addEvent(row, "mousedown", RIUTIL.onClickDefaultContentEvent); // do not need to drag and drop but click action, however for the user get use to do drag and drop, they do not feel strange if we trigger action when they start drag and drop.
    }
    else {
        RIUTIL.addEvent(row, "mousedown", RIUTIL.onMouseDownDragEvent);
        RIUTIL.addEvent(row, "mousedown", RIUTIL.onMouseDownPaletteEvent);
    }

    return row;
}

RIUTIL.createPalletteRowForLayoutName = function (table) {
    var row = table.insertRow(-1);
    row.style.height = '15px';
    //RIUTIL.attachHoverEffect(row, "RIAction", "RIAction RIPalletteRowHover");
    row.className = "RIAction RIPalletteRowHeading";
    return row;
}

RIUTIL.isLcmPreview = function () {
    if (typeof cafeOneUDOObject != 'undefined') {
        return cafeOneUDOObject.isLcmPreview || (RIUTIL.lcmEdit !== undefined && !RIUTIL.lcmEdit);
    }
    return false;
}

RIUTIL.isLcmEdit = function () {
    if (typeof cafeOneUDOObject != 'undefined') {
        return cafeOneUDOObject.isLcmEdit || (RIUTIL.lcmEdit !== undefined && RIUTIL.lcmEdit);
    }
    return false;
}

RIUTIL.setLcmPreview = function (isPreview) {
    if (typeof cafeOneUDOObject != 'undefined') {
        cafeOneUDOObject.isLcmPreview = isPreview;
    }
}

RIUTIL.setLcmEdit = function (isEdit) {
    if (typeof cafeOneUDOObject != 'undefined') {
        cafeOneUDOObject.isLcmEdit = isEdit;
    }
}

// Private: return true for window state is minimized in runtime mode
RIUTIL.isMinimized = function (container) {
    return !RIUTIL.layoutManager.designMode && container.winState == RIUTIL.WIN_STATE_MINIMIZED;
}

// Private: return true for transaction key related filter out container in runtime mode
RIUTIL.isFilteredOut = function (container) {
    return !RIUTIL.layoutManager.designMode && container.filteredOut;
}

// Private: return true if the container contain grid id as registered param controls
RIUTIL.hasGridLevelParameter = function (container) {
    if (container.frame && container.frame.registeredParamControls) {
        var registeredParamControls = container.frame.registeredParamControls;
        for (var prop in registeredParamControls) {
            if (prop.indexOf("G") == 0 && prop.indexOf("GC") != 0)
                return true;
        }
        return false;
    }
}

// Public: called by server to reload the page when user preference changed.
RIUTIL.reloadPage = function () {
    window.location.replace(window.location.href);
}

// Private: we do not want the other frames to allowed be enter into detail/edit/create mode and we shall disable two palettes before this frame back to display mode.
RIUTIL.preventMutipleEditting = function (rootContainer, workingContainerId) {
    var containerId = rootContainer.id;
    // hide all menu icons of other containers
    if (rootContainer.containerType == RIUTIL.CONATINAER_RIAF && containerId != workingContainerId) {
        RIUTIL.setDisplayById(null, "objDetail" + containerId, false);
        RIUTIL.setDisplayById(null, "objPopup" + containerId, false);
        RIUTIL.setDisplayById(null, "objDeactivate" + containerId, false);
    }

    if (rootContainer.subContainers) {
        // recursively call sub container to do the same
        for (var i = 0; i < rootContainer.subContainers.length; i++) {
            var subContainer = rootContainer.subContainers[i];
            RIUTIL.preventMutipleEditting(subContainer, workingContainerId);
        }
    }
}

// Private: simulate the add key process to ask the server to populate key table
RIUTIL.populateKeyTables = function (containerId, hasTabPageFilter) {
    RIUTIL.updateTabPageFilterOption(containerId, hasTabPageFilter);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_POPULATE_KEY);
}

// Private: ask the server to get the list of URL templates
RIUTIL.loadURLTemplateList = function (containerId) {
    RIUTIL.templateList = new Array();
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LOAD_URL_TEMPLATES);
}

// Private: ask the server to get the list of E1 Pages 
RIUTIL.loadE1PageList = function (containerId) {
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LOAD_E1_PAGE_LIST);
}

// Private: ask the server to get the list of E1 Pages 
RIUTIL.loadADFIdList = function (containerId) {
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LOAD_ADF_LIST);
}

// Public: invoked by Server to add template
RIUTIL.addURLTemplate = function (containerId, name_, description_, url_) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var dropdownIcon = doc.getElementById("urlTemplateDropdownIcon");
    if (!dropdownIcon.itemList)
        dropdownIcon.itemList = new Array();

    var tempalte = { name: name_, description: description_, url: url_ };
    dropdownIcon.itemList.push(tempalte);
}

// Private: create a resize UI for parent container 
RIUTIL.createResizeIndicator = function (containerId) {
    var document = RIUTIL.getFrameworkDoc();
    var container = RIUTIL.getContainer(containerId);
    var containerTag = RIUTIL.getContainerTag(container);
    var containerRegion = RIUTIL.getRegion(containerTag);
    var initialPercentage = container.subContainers[0].percentage;
    var layoutType = container.layoutType;

    var div = document.createElement("div");
    div.style.position = 'absolute';
    div.className = RIUTIL.CLASS_LAYOUT_FRAME;
    div.containerId = containerId;

    div.style.left = containerRegion.left + "px";
    div.style.top = containerRegion.top + "px";
    div.style.width = containerRegion.width + "px";
    div.style.height = containerRegion.height + "px";

    var sepDiv = document.createElement("div");
    sepDiv.style.position = 'relative';
    sepDiv.className = layoutType == RIUTIL.LAYOUT_VERTICAL ? "RIResizeVerticalMove" : "RIResizeHorizontalMove";
    sepDiv.style.left = ((layoutType == RIUTIL.LAYOUT_VERTICAL) ? 0 : containerRegion.width * initialPercentage / 100) + "px";
    sepDiv.style.top = ((layoutType == RIUTIL.LAYOUT_HORIZONTAL) ? 0 : containerRegion.height * initialPercentage / 100) + "px";
    sepDiv.style.width = ((layoutType == RIUTIL.LAYOUT_VERTICAL) ? containerRegion.width : RIUTIL.RESIZE_DIV_SIZE) + "px";
    sepDiv.style.height = ((layoutType == RIUTIL.LAYOUT_HORIZONTAL) ? containerRegion.height : RIUTIL.RESIZE_DIV_SIZE) + "px";
    div.appendChild(sepDiv);

    document.body.appendChild(div);
    RIUTIL.resizeIndicator = div;

}

// Private: move the resize indicator to show the desired layout
RIUTIL.moveResizeIndicator = function (x, y) {
    var resizeIndicator = RIUTIL.resizeIndicator;
    if (!resizeIndicator)
        return;

    var container = RIUTIL.getContainer(resizeIndicator.containerId);
    var containerTag = RIUTIL.getContainerTag(container);
    var containerRegion = RIUTIL.getRegion(containerTag);
    var sepDiv = resizeIndicator.firstChild;

    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL:
            var separatorY = Math.max(containerRegion.top + RIUTIL.CONTAINER_MIN_HEIGHT, Math.min(containerRegion.top + containerRegion.height - RIUTIL.CONTAINER_MIN_HEIGHT, y));
            subHeight = separatorY - containerRegion.top;
            sepDiv.style.top = subHeight + "px";
            resizeIndicator.precentage = subHeight / containerRegion.height * 100;
            break;

        case RIUTIL.LAYOUT_HORIZONTAL:
            var separatorX = Math.max(containerRegion.left + RIUTIL.CONTAINER_MIN_WIDTH, Math.min(containerRegion.left + containerRegion.width - RIUTIL.CONTAINER_MIN_WIDTH, x));
            var subWidth = separatorX - containerRegion.left;
            if (RIUTIL.isRTL) {
                sepDiv.style.left = -containerRegion.width + subWidth + "px";
                resizeIndicator.precentage = (1 - subWidth / containerRegion.width) * 100;
            }
            else {
                sepDiv.style.left = subWidth + "px";
                resizeIndicator.precentage = subWidth / containerRegion.width * 100;
            }

            break;
    }
}

// Privatet: reset parameter
RIUTIL.resetParameterCapture = function (containerId, currentPartId) {
    if (!RIUTIL.isCompositePage) {
        var doc = RIUTIL.getRIPaneDoc(containerId);
        //alert('resetParameterCapture'+currentPartId);
        RIUTIL.setElemDisplay(doc, "addCustomizedTextIcon", false);
        RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_RESET_PARM_CAPTURE, RIUTIL.PARAM_PARTID, isNaN(currentPartId) ? -1 : currentPartId);
    }
}

// Privatet: user starts the parameter capture mode
RIUTIL.startParameterCapture = function (containerId, currentPartId, selectedOperator) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    RIUTIL.setElemDisplay(doc, "addCustomizedTextIcon", true);
    RIUTIL.goStopKeyCapture();
    var doc = RIUTIL.getRIPaneDoc(containerId);

    RIUTIL.setElemDisplay(doc, RIUTIL.getPartBaseValueDivDomID(currentPartId), false);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_START_PARM_CAPTURE, RIUTIL.PARAM_PARTID, currentPartId, RIUTIL.PARAM_OPERATOR, selectedOperator);
}

// Public method to render the form selection and state
RIUTIL.renderFormSelection = function (json) {
    var formSelection = eval(json);
    var selectTag = RIUTIL.getFormSeletionTag();
    if (selectTag)
        selectTag.options.length = 0;

    if (formSelection == null)
        return;
    var formList = formSelection.formList;
    for (var i = 0; i < formList.length; i++) {
        var formId = formList[i][0];
        var formDescription = formList[i][1];
        var option = new Option(formDescription + "(" + formId + ")", formId, false, false);
        option.subType = formList[i][2];
        selectTag.options[i] = option;
        if (formId == formSelection.formId) {
            selectTag.options[i].selected = true;
            RIUTIL.updateViewForShowTitleBarOption(option);
        }
    }

    if (formList.length == 0 && RIUTIL.getAppIdTag().value != "") {
        RIUTIL.updateViewForShowTitleBarOption(null);
        alert(RIUTIL.CONSTANTS.RI_INVLIAD_APP_ID);
    }
    else
        RIUTIL.goChangeFormSeletion();

}

// Public method to render the form selection and state
RIUTIL.updateViewForShowTitleBarOption = function (option) {
    var dsiplayTitleBarOption = (option && option.subType == "EXJ");
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    RIUTIL.setDisplayById(doc, "inputForShowTitleBar", dsiplayTitleBarOption);
}

// Public method to render the version selection and state
RIUTIL.renderVersionSelection = function (json) {
    RIUTIL.renderGenericSelection(json, RIUTIL.getVersionSeletionTag(), "versionList", "versionId");
    RIUTIL.goChangeVersionSeletion();
}

// Public method to render the pf selection and state
RIUTIL.renderPfSelection = function (json) {
    if (!RIUTIL.isPFFeatureEnabled) {
        return;
    }

    var result = JSON.parse(eval(json).pfList);
    var obj = result;
    var bCount = false;
    var activeObj = (obj.activeObject) ? obj.activeObject : {};
    var selectedPfId = eval(json).pfId;
    var selectedPfUser = eval(json).pfUser;
    $oPFList = obj.udoObjects;
    var sList = "";
    var sCombo = "";
    var splitLit = "^";
    if (activeObj.user) {
        sCombo = "<option user='" + activeObj.user + "' value='" + activeObj.user + splitLit + "null' >" + "Default" + "</option>";
    }
    $.each($oPFList, function (i) {

        sList = "";
        $.each(this.items, function (j) {
            var thisName = this.name;
            if (this.omwObjectName === null) {
                thisName = "Default";
            }
            var val = this.user + splitLit + this.omwObjectName;
            var actObjVal = selectedPfUser + splitLit + selectedPfId;
            sList += "<option user='" + this.user + "' value='" + val + "' " + (actObjVal === val ? "selected" : "") + ">" + thisName + "</option>";
        }
            );
        if (sList != "") {
            var sGrp_Open = "<optgroup label='" + this.group + "' >";
            var sGrp_Close = "</optgroup>";
            sCombo += sGrp_Open + sList + sGrp_Close;
            bCount = true;
        }
    }
        );
    if (bCount == false) // When no formats were created. We have to show Default
    {
        sCombo = "<option user='" + activeObj.user + "' value='" + activeObj.user + splitLit + activeObj.omwObjectName + "' " + ">" + "Default" + "</option>";
    }
    $(RIUTIL.getPfSeletionTag()).html(sCombo);
}

// Public method to render the report selection and state
RIUTIL.renderReportSelection = function (json) {
    RIUTIL.renderGenericSelection(json, RIUTIL.getReportSelectionTag(), "reportList", "reportId");
}

// Public method to render the query selection and state
RIUTIL.renderQuerySelection = function (json) {
    RIUTIL.renderGenericSelection(json, RIUTIL.getQuerySelectionTag(), "queryList", "queryId");
}

// Public method to render the E1 page selection and state
RIUTIL.renderE1PageIdSelection = function (json) {
    RIUTIL.renderGenericSelection(json, RIUTIL.getE1PageIdSelectionTag(), "e1PageList", "e1PageId");
}

// Public method to render the ADF selection and state
RIUTIL.renderADFIdSelection = function (json) {
    RIUTIL.renderGenericSelection(json, RIUTIL.getADFIdSelectionTag(), "ADFList", "ADFId");
    RIUTIL.renderGenericSelection(json, RIUTIL.getADFProxyFormSelectionTag(), "ADFProxyFormList", "ADFProxyFormId");
    RIUTIL.renderGenericSelection(json, RIUTIL.getADFProxyVersionSelectionTag(), "ADFProxyVersionList", "ADFProxyVersionId");
    var adfSelection = eval(json);
    RIUTIL.updateADFProxyAppId(RIUTIL.inspectedContainerId, adfSelection["ADFProxyAppId"]);
}

// Public method to render the report selection and state
RIUTIL.renderGenericSelection = function (json, selectTag, listName, idName) {
    var selection = eval(json);
    selectTag.options.length = 0;
    if (selection == null)
        return;

    var list = selection[listName];
    for (var i = 0; i < list.length; i++) {
        var id = list[i][0];
        var description = list[i][1];
        selectTag.options[i] = new Option(description + "(" + id + ")", id, false, false);
        if (id == selection[idName])
            selectTag.options[i].selected = true;
    }
}

// Public method to render the data sctruture mapping
RIUTIL.renderFormDS = function (json) {
    RIUTIL.formDs = eval(json);
}

RIUTIL.cleanUpAllE1SatelliteContainers = function () {
    if (RIUTIL.layout)
        var containerList = RIUTIL.getFrameIds(RIUTIL.layout, new Array(), false);
    for (var i = 0; undefined != containerList && i < containerList.length; i++) {
        var container = RIUTIL.getContainer(containerList[i]);
        if (RIUTIL.isE1SatelliteContainer(container)) {
            RIUTIL.cleanupContainer(containerList[i])
        }
    }
}
// Private: wrapper method for synchronizeContainer, since we need to cleanup
RIUTIL.synchronizeContainerWithCleanUp = function (containerId) {
    var container = RIUTIL.getContainer(containerId);
    if (RIUTIL.isE1SatelliteContainer(container))
        RIUTIL.cancelContainer(containerId);
    else
        RIUTIL.synchronizeContainer(containerId);
}

// private: return true if the container is satellite E1 Form
RIUTIL.isE1SatelliteContainer = function (container) {
    return container.containerType == RIUTIL.CONATINAER_RIAF && container.frame && container.frame.pathType == RIUTIL.TYPE_E1_FORM && !container.frame.subType;
}

// private: return true if the container is satellite freestyle Form
RIUTIL.isFreeStyleSatelliteContainer = function (container) {
    return container.containerType == RIUTIL.CONATINAER_FREESTYLE;
}

// Private: called from Master Frame to synchronize frame based on the current contexual data without triggering unload handler of the frame by reset the unload handler.
RIUTIL.synchronizeContainer = function (containerId) {
    var contentWindow = RIUTIL.getContainerWinObject(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_REFRESH_CONTENT);
}

// Private method called from Master to trigger cancel action of Satellite form
RIUTIL.cancelContainer = function (containerId) {
    var contentWindow = RIUTIL.getContainerWinObject(containerId);
    contentWindow.RIUTIL.executeCancel();
}

// Private method called from Master to tell server dirrectly to remove owclient object from the owvirtualgroup map to prevent leak. since it is too lake to trigger cancel action of the frame.
RIUTIL.cleanupContainer = function (containerId) {
    var container = RIUTIL.getContainer(containerId);
    if (container && container.frame && container.frame.pathType == RIUTIL.TYPE_E1_FORM && !container.frame.subType) {
        var contentWindow = RIUTIL.getContainerWinObject(containerId);
        if (contentWindow && contentWindow.RIUTIL) {
            contentWindow.RIUTIL.executeCleanup();
        }
    }
}


// Private method called from Master Frame to defer the request to callback method in Satellite Frame unitl the dirty flag is sent back by the Satellite form.
RIUTIL.continueWithDirtyCheck = function (container, showSyncInfo) {
    if (container.frame.pathType != RIUTIL.TYPE_E1_FORM)
        return true;

    if (container.frame.subType) // ADF or E1Page subtype of ExAPP
        return true;

    var containerId = container.id;
    var contentWindow = RIUTIL.getContainerWinObject(containerId);

    // in some speical double refresh racing condition, content window is already under refresh, we do not need to refresh it again
    if (!contentWindow.JDEDTAFactory)
        return false;

    var dtaInstance = contentWindow.JDEDTAFactory.getInstance("");
    RIUTIL.hilightContainer(containerId, true);
    var continueAction = true;
    contentWindow.ChangeConf.isConfProcessed = false;
    var currTimeStmp = new Date().getTime();
    var diffTime = (currTimeStmp - ((contentWindow.RIUTIL && contentWindow.RIUTIL.dirtyRIFormBegTimeStmp) ? contentWindow.RIUTIL.dirtyRIFormBegTimeStmp : 0));
    if (dtaInstance.isDirty || contentWindow.dirtyCafeOne || diffTime < 300) {
        contentWindow.RIUTIL.dirtyRIFormBegTimeStmp = null;
        if (!contentWindow.ChangeConf.isChangeConfEnabledForm ||
            contentWindow.ChangeConf.isCancelConf) {
            continueAction = confirm(RIUTIL.CONSTANTS.RI_WARN_UNCOMMIT);
            contentWindow.ChangeConf.isConfProcessed = true;
        }

        if (showSyncInfo)
            RIUTIL.markFrameNotInSynch(containerId, !continueAction);
    }
    RIUTIL.hilightContainer(containerId, false);
    return continueAction;
}

// Private method called from Master Frame to get hold of the window object of Satellite Frame
RIUTIL.getContainerWinObject = function (containerId) {
    var frame = RIUTIL.getRIPaneIFRAME(containerId);
    if (!frame)
        return null;
    return frame.contentWindow;
}

// Private method indicate the satellite frame is out of sync with the master frame
RIUTIL.markFrameNotInSynch = function (containerId, outOfSynch) {
    var paneTitle = document.getElementById("paneTitle" + containerId);
    if (paneTitle) {
        var container = RIUTIL.getContainer(containerId)
        var title = container.frame.description;
        if (outOfSynch && paneTitle.childNodes.length == 1)
            paneTitle.appendChild(document.createTextNode(" " + RIUTIL.CONSTANTS.RI_OUT_OF_SYNC));
        else if (!outOfSynch && paneTitle.childNodes.length == 2)
            paneTitle.removeChild(paneTitle.lastChild);
    }
}

// Private method to highlight the container for confirmation process
RIUTIL.hilightContainer = function (containerId, hightlight) {
    var paneTitle = document.getElementById("paneTitle" + containerId);
    if (paneTitle) {
        paneTitle.className = hightlight ? "RIHighlightForProcess" : "";
    }
}

// Private method to highlight the container for confirmation process
RIUTIL.hilightContainerList = function (containerIdList, hightlight) {
    for (var i = 0; i < containerIdList.length; i++) {
        RIUTIL.hilightContainer(containerIdList[i], hightlight);
    }
}

// Private method to trigger cancel action of the form in seperate thread. Server (HTMLClientHandler) with empty call stack will response it with JDEDTA event SYNCH_TO_MASTER to trigger synchToMasterForm
RIUTIL.executeCancel = function () {
    if (document.getElementById('hc_Cancel'))
        setTimeout("RIUTIL.executeMenu('hc_Cancel')", 0);
    else if (document.getElementById('hc_Close'))
        setTimeout("RIUTIL.executeMenu('hc_Close')", 0);
    else if (window.formCloseOrCancelDomID && document.getElementById(window.formCloseOrCancelDomID))
        setTimeout("document.getElementById('" + window.formCloseOrCancelDomID + "').click()", 0);
}

// Private method to trigger cleanup action of the form and notify server to clean up maflet and owvirtual, E1Client intercept this request based on the request parameter jdeLoginAction=SatelliteClose
RIUTIL.executeCleanup = function () {
    window.handleOnBrowserClose = function () { };
    var closeURL = window["mafCloseURL"] + "&jdeLoginAction=SatelliteClose&RID=" + JDEDTAFactory.getInstance(RIUTIL.namespace).RID;
    if (RIUTIL.isChrome) // for chrome, we can not get close URL to work by dirrectly assign to the window, so workaround it by post from child iframe as workaround.
    {
        var iframe = document.createElement("iframe");
        window.document.body.appendChild(iframe)
        iframe.src = closeURL;
    }
    else
        window.location.href = closeURL;
}

RIUTIL.preventBrowserCloseProcess = function () {
    window.handleOnBrowserClose = function () { };
}

// Private: get the client height of the current window
RIUTIL.getClientHeight = function () {
    var clientHeight = 0;
    if (isNativeContainer) {
        clientHeight = Math.min(window.screen.availWidth, window.screen.availHeight) - 2 * 44 - 20; //topbar=bottombar=44; cafeone title bar=20
    }
    else if (isIOS) {
        if (RIUTIL.isCompositePage) {
            clientHeight = RIUTIL.getFrameworkWindow().innerHeight + 2; // make it two pixel larger to allow scrolling to work in iframe
        }
        else {
            var orientation = ORIENTATION.getDeviceOrientation();
            if (orientation == ORIENTATION.LANDSCAPE) {
                clientHeight = Math.min(window.screen.availWidth, window.screen.availHeight);
            }
            else {
                clientHeight = Math.max(window.screen.availWidth, window.screen.availHeight);
            }
        }
    }
    else // web client
    {
        if (RIUTIL.isCompositePage)
            clientHeight = RIUTIL.getFrameworkWindow().innerHeight;
        else
            clientHeight = window.innerHeight || document.body.clientHeight || document.documentElement.clientHeight;
    }
    return clientHeight;
}

//---------------------------------------
// Event Handler meothods
//---------------------------------------

// Event handler: when user click on the append more icon of a part
RIUTIL.goAppendMore = function () {
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_APPEND_COMPONENT, RIUTIL.PARAM_PARTID, this.partId);
}

// Event Handler: mouse up outside workspace during the drag and drop create/add/move process.
RIUTIL.onMouseUpDragOutterEvent = function (e) {
    RIUTIL.onMouseUpDragEvent(e);
}

// Event Handler: mouse up outside workspace during the drag and drop create/add/move process.
RIUTIL.onMouseUpResizeOutterEvent = function (e) {
    RIUTIL.onMouseUpResizeEvent(e);
}

// Event Handler: click start key capture icon
RIUTIL.goStartKeyCapture = function (event) {
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.goStopKeyCapture();
    RIUTIL.resetKeyInfo(containerId);
    RIUTIL.resetParameterCapture(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_START_KEY_CAPTURE);
}

// Event Handler: click on E1 control in key capture mode
RIUTIL.goCaptureKey = function (e) {
    var containerId = RIUTIL.inspectedContainerId;
    var elem = RIUTIL.getEventSource(e);
    var id = elem.getAttribute(RIUTIL.ATT_CLIENTL_ID);
    var prodCode = RIUTIL.getActiveProduct();
    RIUTIL.postAjaxRequest(containerId, prodCode, RIUTIL.AJAX_CMD_CAPTURE_KEY, RIUTIL.PARAM_CLIENTL_ID, id);
}

// Event handler: for mouse over the drop down list
RIUTIL.onMouseOverDropdownList = function () {
    RIUTIL.isOverDropDownList = true;
    return true;
}

// Event handler: for mouse out the drop down list
RIUTIL.onMouseOutDropdownList = function () {
    RIUTIL.isOverDropDownList = false;
    return true;
}

// Event handler: for mouse over the link on the drop down list
RIUTIL.onMouseOverDropdownLink = function () {
    this.parentNode.className = "MenuActive";
    return true;
}

// Event handler: for mouse out the link on the drop down list
RIUTIL.onMouseOutDropdownLink = function () {
    this.parentNode.className = "";
    return true;
}

// Event handler: mouse over a element, the class of the element need to be replace to the predefiend classes for hover effect. we support two level of element tree.
RIUTIL.onMouseOverClassUpdateEvent = function (e) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "hoverClassName");
    if (!elem)
        return;

    elem.className = elem.hoverClassName;

    if (elem.bubbleup) // bubble effect to next level
    {
        //search one more level up for possible another layer of hover effect
        var elem = RIUTIL.searchParentByPorpName(elem.parentNode, "hoverClassName");
        if (elem)
            elem.className = elem.hoverClassName;
    }

    return RIUTIL.stopEventBubbling(e);
}

RIUTIL.onMouseOutClassUpdateEvent = function (e) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "normalClassName");
    if (!elem)
        return;

    elem.className = elem.normalClassName;

    if (elem.bubbleup) // bubble effect to next level
    {
        var elem = RIUTIL.searchParentByPorpName(elem.parentNode, "normalClassName");
        if (elem)
            elem.className = elem.normalClassName;
    }
    return RIUTIL.stopEventBubbling(e);
}

// Event handler: drag and drop onMouseMoveResizeEvent of RI toolbar.
RIUTIL.onMouseMoveResizeEvent = function (e) {
    var curPos = RIUTIL.getCurrentPosition(e, false);
    var resizeRegion = RIUTIL.moveResizeIndicator(curPos.x, curPos.y);
}

// Event handler: drag and drop onMouseMoveResizeEvent of RI toolbar.
RIUTIL.onMouseDownResizeEvent = function (e) {
    var document = RIUTIL.getFrameworkDoc();
    if (RIUTIL.layout.isEditting) {
        alert(RIUTIL.CONSTANTS.RI_CHANGE_LAYOUT_WARN);
        return;
    }
    var elem = RIUTIL.getEventSource(e);
    var td = RIUTIL.searchParentByTagName(elem, "TD");
    var eventBlocker = RIUTIL.createEventBlocker(RIUTIL.layout.id, RIUTIL.onMouseUpResizeOutterEvent);
    RIUTIL.dragMode = true;

    RIUTIL.addEvent(eventBlocker, "mouseup", RIUTIL.onMouseUpResizeEvent);
    RIUTIL.addEvent(eventBlocker, "mousemove", RIUTIL.onMouseMoveResizeEvent);
    RIUTIL.disableDOMHighlighting(document.body);
    RIUTIL.disableDOMHighlighting(top.document.body);
    RIUTIL.createResizeIndicator(td.containerId);
}

// Event handler: drag and drop mouse for resize of RI toolbar.
RIUTIL.onMouseUpResizeEvent = function (e) {
    var document = RIUTIL.getFrameworkDoc();
    var resizeIndicator = RIUTIL.resizeIndicator;
    var precentage = resizeIndicator.precentage;
    var containerId = resizeIndicator.containerId;

    RIUTIL.enableDOMHighlighting(document.body);
    RIUTIL.enableDOMHighlighting(top.document.body);
    RIUTIL.removeEventBlocker(RIUTIL.layout.id, RIUTIL.onMouseUpResizeOutterEvent);
    RIUTIL.removeResizeIndicator();
    RIUTIL.resizeContaienr(containerId, precentage);
}

// Event Handler: create object or link action
RIUTIL.goSaveObj = function (containerId, confirmed) {
    RIUTIL.goStopKeyCapture();
    //RIUTIL.resetParameterCapture(); // do not need to post to the server to reset paramter for page refresh

    if (RIUTIL.getActiveProduct() == RIUTIL.PRODUCT_GENERIC_URL) {
        if (!RIUTIL.isValidURL(containerId)) {
            alert(RIUTIL.CONSTANTS.RI_CREATE_INVALID_URL);
            return;
        }
    }
    var pathType = RIUTIL.pathType;
    if (pathType == RIUTIL.TYPE_GENERIC) {
        var doc = RIUTIL.getRIPaneDoc(containerId);
        var path = doc.getElementById("publicLinkUrl").value;

        RIUTIL.disableInspectDialog();
        RIUTIL.detectFrameBreaker(containerId, path, function () {
            RIUTIL.onSaveObj(containerId, confirmed);
        });
    }
    else {
        RIUTIL.onSaveObj(containerId, confirmed);
    }
}

RIUTIL.onSaveObj = function (containerId, confirmed) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var objName = doc.getElementById("objName");
    var objDescription = doc.getElementById("objDescription");
    var publicLinkUrl = doc.getElementById("publicLinkUrl");
    var pathType = RIUTIL.pathType;
    var omwObjectName = null;
    var udo_user = null;
    if (RIUTIL.isPFFeatureEnabled) {
        omwObjectName = PF.getPFDesignName($(doc.getElementById("objPfSeletion")));
        udo_user = $('option:selected', $(doc.getElementById("objPfSeletion"))).attr('user');
    }

    var objInculdeAll;
    if (pathType == RIUTIL.TYPE_WL_PANE)//for watchlist, get the value from the radio buttons
    {
        //find the selected radio button
        var radioButtons = doc.getElementsByName("includeAll");
        for (var i = 0; i < radioButtons.length; i++) {
            if (radioButtons[i].checked) {
                objInculdeAll = radioButtons[i].value;
                break;
            }
        }
    }
    else if (pathType == RIUTIL.TYPE_LANDING_PANE)//for landing page, get the value from input field
    {
        objInculdeAll = doc.getElementById("objTaskMenu").value.toUpperCase();
    }
    var container = RIUTIL.getContainer(containerId);

    if (container.mode == RIUTIL.MODE_CREATE) {
        RIUTIL.setPageDesignBarEnable(true);
        var cols = doc.getElementById("objCols").value;
        var rows = doc.getElementById("objRows").value;
        if (RIUTIL.isPFFeatureEnabled) {
            RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CREATE_NEW, RIUTIL.PARAM_OBJECT_NAME, objName.value, RIUTIL.PARAM_OBJECT_DES, objDescription.value, RIUTIL.PARAM_PUBLIC_URL, publicLinkUrl.value, RIUTIL.PARAM_PATH_TYPE, pathType, RIUTIL.PARAM_INIT_COLS, cols, RIUTIL.PARAM_INIT_ROWS, rows, RIUTIL.PARAM_INCLUDE_ALL, objInculdeAll, "pfId", omwObjectName, "pfUser", udo_user);
        }
        else {
            RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CREATE_NEW, RIUTIL.PARAM_OBJECT_NAME, objName.value, RIUTIL.PARAM_OBJECT_DES, objDescription.value, RIUTIL.PARAM_PUBLIC_URL, publicLinkUrl.value, RIUTIL.PARAM_PATH_TYPE, pathType, RIUTIL.PARAM_INIT_COLS, cols, RIUTIL.PARAM_INIT_ROWS, rows, RIUTIL.PARAM_INCLUDE_ALL, objInculdeAll);
        }
    }
    else if (container.mode == RIUTIL.MODE_EDIT) {
        RIUTIL.setPageDesignBarEnable(true);
        if (RIUTIL.getActiveProduct() == RIUTIL.PRODUCT_GENERIC_URL) // name, decription and url template can be edit in generic URL
        {
            if (RIUTIL.isPFFeatureEnabled) {
                RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SAVE_REF, RIUTIL.PARAM_CONTAINER_ID, containerId, RIUTIL.PARAM_OBJECT_NAME, objName.value, RIUTIL.PARAM_OBJECT_DES, objDescription.value, RIUTIL.PARAM_PUBLIC_URL, publicLinkUrl.value, RIUTIL.PARAM_PATH_TYPE, pathType, RIUTIL.PARAM_CONFIRMED, confirmed == true, RIUTIL.PARAM_INCLUDE_ALL, objInculdeAll, "pfId", omwObjectName, "pfUser", udo_user);
            }
            else {
                RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SAVE_REF, RIUTIL.PARAM_CONTAINER_ID, containerId, RIUTIL.PARAM_OBJECT_NAME, objName.value, RIUTIL.PARAM_OBJECT_DES, objDescription.value, RIUTIL.PARAM_PUBLIC_URL, publicLinkUrl.value, RIUTIL.PARAM_PATH_TYPE, pathType, RIUTIL.PARAM_CONFIRMED, confirmed == true, RIUTIL.PARAM_INCLUDE_ALL, objInculdeAll);
            }
        }
        else {
            RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SAVE_REF, RIUTIL.PARAM_CONTAINER_ID, containerId, RIUTIL.PARAM_PUBLIC_URL, publicLinkUrl.value);
        }
    }

    return false;
}


// Event Handler: for change the path
RIUTIL.goChangePath = function (bySelectTemplate) {
    RIUTIL.changePath(true);
}

// Private" for change the path by user type in or select from the list of OBIEE or Template
RIUTIL.changePath = function (userTyped) {
    var containerId = RIUTIL.inspectedContainerId;
    if (!RIUTIL.isValidURL(containerId)) {
        alert(RIUTIL.CONSTANTS.RI_CREATE_INVALID_URL);
        return;
    }
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var publicLinkUrl = doc.getElementById("publicLinkUrl").value;
    var prodCode = RIUTIL.getActiveProduct();
    RIUTIL.postAjaxRequest(containerId, prodCode, RIUTIL.AJAX_CMD_SET_PATH, RIUTIL.PARAM_PATH_TYPE, 0, RIUTIL.PARAM_PATH, publicLinkUrl, RIUTIL.PARAM_USER_TYPED, userTyped);
}


// Event Handler: for validate button clicked
RIUTIL.goValidate = function () {
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.goStopKeyCapture();
    RIUTIL.resetParameterCapture(containerId);

    if (!RIUTIL.isValidURL(containerId)) {
        alert(RIUTIL.CONSTANTS.RI_CREATE_INVALID_URL);
        return;
    }
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_VALIDATE_URL);
}

// Event Handler: remove content from content repository
/*RIUTIL.goRemoveContent = function (e)
{
var answer = confirm(RIUTIL.CONSTANTS.RI_PR_WARN)
if (answer)
{
var srcElem = RIUTIL.getEventSource(e);
var table = RIUTIL.searchParentByPorpName(srcElem , 'popupWin');
table.popupWin.onClose()
    
var prodCode = RIUTIL.getActiveProduct();
var infoTag = RIUTIL.searchParentByPorpName(srcElem, "objectId");
RIUTIL.postAjaxRequest(-1, prodCode, RIUTIL.AJAX_CMD_REMOVE_CONTENT, RIUTIL.PARAM_OBJECT_ID, infoTag.objectId);
return false;
}
else 
{
return false;
}
}*/

// Event Handler: click stop key capture icon
RIUTIL.goStopKeyCapture = function () {
    if (!RIUTIL.isCompositePage)
        RIUTIL.setCaptureMode(RIUTIL.NO_CAPTURE, true);
}

// Event Handler: user click the checkbox to reset parameter or initiat the paramter capture mode
RIUTIL.goChangeParameterConfig = function () {
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.goStopKeyCapture();

    if (this.selectedIndex > 0) {
        RIUTIL.startParameterCapture(containerId, this.partId, 1);
    }
    else {
        RIUTIL.resetParameterCapture(containerId, this.partId);
    }
}

// Event Handler: change layout description
RIUTIL.onChnageLayoutDesicription = function () {
    RIUTIL.layoutManager.isDirty = true;
}

// Event Handler: change layout description
RIUTIL.onChnageLayoutProdCode = function () {
    RIUTIL.layoutManager.isDirty = true;
}

// Event Handler: user select the filter operator to reset parameter or initiat the paramter capture mode
RIUTIL.goChangeFilterOperator = function () {
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.goStopKeyCapture();

    if (this.selectedIndex > 0) {
        var selectedOperator = this.options[this.selectedIndex].value;
        RIUTIL.startParameterCapture(containerId, this.partId, selectedOperator);
    }
    else {
        RIUTIL.resetParameterCapture(containerId, this.partId);
    }
}

// Event Handler: for detail icon 
RIUTIL.goDetail = function (containerId) {
    RIUTIL.cleanupContainer(containerId);
    RIUTIL.oldLayoutJSON = null; // discard the cache of workspace
    RIUTIL.setPageDesignBarEnable(false);
    if (RIUTIL.isCompositePage) {
        RIUTIL.saveFreeStyleFrames();
        RIUTIL.saveWLComponentFrames();
        RIUTIL.saveLPContentFrames();
    }
    RIUTIL.postPageRequest(containerId, RIUTIL.PAGE_CMD_SHOW_DETAIL);
}

// Event Handler: for edit icon 
RIUTIL.goEdit = function (containerId) {
    RIUTIL.cleanupContainer(containerId);
    RIUTIL.oldLayoutJSON = null; // discard the cache of workspace
    RIUTIL.setPageDesignBarEnable(false);
    RIUTIL.postPageRequest(containerId, RIUTIL.PAGE_CMD_SHOW_EDIT);
}

// Event Handler: for create icon 
RIUTIL.goCreate = function (containerId, pathType) {
    RIUTIL.oldLayoutJSON = null; // discard the cache of workspace
    RIUTIL.setPageDesignBarEnable(false);
    RIUTIL.setActiveProduct(RIUTIL.PRODUCT_GENERIC_URL);
    var container = RIUTIL.getContainer(containerId);
    RIUTIL.postPageRequest(containerId, RIUTIL.PAGE_CMD_SHOW_CREATE, RIUTIL.PARAM_PATH_TYPE, pathType);
}

// Event Handler: for delete component
RIUTIL.goDeleteComponent = function () {
    RIUTIL.goStopKeyCapture();
    var controlId = this.getAttribute("controlId");
    var partId = this.getAttribute("partId");
    var componentIndex = this.getAttribute("componentIndex");
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.highlightComponent(containerId, controlId, partId, componentIndex, false);
    RIUTIL.highlightParameterPart(containerId, partId, false);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_DELETE_COMPONENT, RIUTIL.PARAM_PARTID, partId, RIUTIL.PARAM_COMPONENT_INDEX, componentIndex);
}

// Event Handler: for mouse over the URL parameter part
RIUTIL.onMouseOverParameterPart = function () {
    var containerId = RIUTIL.inspectedContainerId;
    var container = RIUTIL.getContainer(containerId);
    var partId = this.partId;
    RIUTIL.highlightParameterPart(containerId, partId, true);
    var showAppendOption = (container.mode == RIUTIL.MODE_EDIT || container.mode == RIUTIL.MODE_CREATE) && this.acceptMore && !this.isFocuse;
    if (showAppendOption)
        RIUTIL.setDisplayById(RIUTIL.getRIPaneDoc(containerId), RIUTIL.getPartAppendDivDomID(partId), true);
}

// Event Handler: for mouse out the URL parameter part
RIUTIL.onMouseOutParameterPart = function () {
    var containerId = RIUTIL.inspectedContainerId;
    var partId = this.partId;
    RIUTIL.highlightParameterPart(containerId, partId, false);
    RIUTIL.setDisplayById(RIUTIL.getRIPaneDoc(containerId), RIUTIL.getPartAppendDivDomID(partId), false);
}

// Event Handler: for mouse over the key
RIUTIL.onMouseOverKey = function () {
    var containerId = RIUTIL.inspectedContainerId;
    var controlId = this.getAttribute("controlId");
    RIUTIL.updateControlIndicator(controlId, "RIHightlightKeyControl", true);
    if (this.orginalclassName != this.className)
        this.orginalclassName = this.className
    this.className = RIUTIL.CLASS_KEY_HIGHLIGHT;
}

// Event Handler: for mouse out the key
RIUTIL.onMouseOutKey = function () {
    var containerId = RIUTIL.inspectedContainerId;
    var controlId = this.getAttribute("controlId");
    RIUTIL.updateControlIndicator(controlId, "RIHightlightKeyControl", false);
    this.className = this.orginalclassName;
}

// Event Handler: for mouse over the URL parameter component part
RIUTIL.onMouseOverComponent = function () {
    var containerId = RIUTIL.inspectedContainerId;
    var controlId = this.getAttribute("controlId");
    var partId = this.getAttribute("partId");
    var componentIndex = this.getAttribute("componentIndex");
    RIUTIL.highlightComponent(containerId, controlId, partId, componentIndex, true);
}

// Event Handler: for mouse out the URL parameter component part
RIUTIL.onMouseOutComponent = function () {
    var containerId = RIUTIL.inspectedContainerId;
    var controlId = this.getAttribute("controlId");
    var partId = this.getAttribute("partId");
    var componentIndex = this.getAttribute("componentIndex");
    RIUTIL.highlightComponent(containerId, controlId, partId, componentIndex, false);
}

// Event Handler: Client notify the server the user want to change the contextual parameter option
// and wait for the sever to populate the other information to the client
RIUTIL.goChangeContexualParamOption = function (isDynamic) {
    var containerId = RIUTIL.inspectedContainerId;
    var doc = RIUTIL.getRIPaneDoc(containerId);

    var urlParamOptions = doc.getElementsByName("urlParamOption");

    if (RIUTIL.pathType == RIUTIL.TYPE_GENERIC && !RIUTIL.isValidURL(containerId)) // generic url
    {
        alert(RIUTIL.CONSTANTS.RI_CREATE_INVALID_URL);
        urlParamOptions[1].checked = false;
        urlParamOptions[0].checked = true;
        return;
    }

    RIUTIL.clearTemplateParts(containerId);
    var prodCode = RIUTIL.getActiveProduct();
    RIUTIL.postAjaxRequest(containerId, prodCode, isDynamic ? RIUTIL.AJAX_CMD_PARAM_DYNAMIC : RIUTIL.AJAX_CMD_PARAM_STATIC, RIUTIL.PARAM_PARTID, -1, RIUTIL.PARAM_RESET_PART, true);
}

// Event Handler: enable or disable tab page filter option
RIUTIL.goTabPageFilterOption = function (hasTabPageFilter) {
    var containerId = RIUTIL.inspectedContainerId;
    var doc = RIUTIL.getRIPaneDoc(containerId);
    RIUTIL.updateTabPageFilterOption(containerId, hasTabPageFilter);
    RIUTIL.goStopKeyCapture();
    RIUTIL.resetKeyInfo(containerId);
    RIUTIL.resetParameterCapture(containerId);
}

// Event Handler: click on E1 control in key capture mode
// client notify the server which control is capture and wait for confirmation or error messsage from the server
RIUTIL.goCaptureParameter = function (e) {
    var containerId = RIUTIL.inspectedContainerId;
    var elem = RIUTIL.getEventSource(e);
    var prodCode = RIUTIL.getActiveProduct();
    var id = elem.getAttribute(RIUTIL.ATT_CLIENTL_ID);
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var objSeparator = doc.getElementById("objSeparator");
    var separator = objSeparator.value;

    RIUTIL.postAjaxRequest(containerId, prodCode, RIUTIL.AJAX_CMD_CAPTURE_FIELD, RIUTIL.PARAM_CLIENTL_ID, id, RIUTIL.PARAM_SEP, separator);
}

// Event Handler: click on add icon for the customized text
RIUTIL.goCaptureCustomizedText = function (evt) {
    var containerId = RIUTIL.inspectedContainerId;
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var objCustomizedText = doc.getElementById("objCustomizedText");
    var customizedTex = objCustomizedText.value;
    var objSeparator = doc.getElementById("objSeparator");
    var separator = objSeparator.value;
    var prodCode = RIUTIL.getActiveProduct();
    RIUTIL.postAjaxRequest(containerId, prodCode, RIUTIL.AJAX_CMD_CAPTURE_CUSTOMIZED_TEXT, RIUTIL.PARAM_CUSTOMIZE_TEXT, customizedTex, RIUTIL.PARAM_SEP, separator);
}

// Event Handler: mouse over dropdown list
RIUTIL.onMouseOverDropdownListIcon = function () {
    var dropdownListTab = this;
    var doc = this.doc;
    var initItemTag = this.initItemTag;
    var itemList = this.itemList;
    var iconHeight = dropdownListTab.offsetHeight;
    var iconWidth = dropdownListTab.offsetHeight;
    var iconLeft = getAbsoluteLeftPos(dropdownListTab);
    var iconTop = getAbsoluteTopPos(dropdownListTab);
    var containerId = RIUTIL.inspectedContainerId;
    var dropdownDiv = doc.createElement("DIV");
    var cientHeight = doc.body.clientHeight;
    var cientWidth = doc.body.clientWidth;

    var isInIframe = doc == RIUTIL.getRIPaneDoc(containerId);
    var dropdownHeight = Math.min(150, Math.max(0, cientHeight - iconTop - iconHeight + (isInIframe ? 20 : 0)));

    var dropdownWidth = Math.max(0, cientWidth - iconLeft + RIUTIL.DROPDOWN_INDENT - RIUTIL.DROPDOWN_PADDING);
    var dropdownWidthMax = Math.min(RIUTIL.DROPDOWN_MAX_WIDTH, dropdownWidth);
    dropdownDiv.id = "dropdownDiv";
    dropdownDiv.className = "RIDropDownOuterDiv";

    dropdownDiv.style.top = iconTop + iconHeight + "px";
    dropdownDiv.style.left = iconLeft - RIUTIL.DROPDOWN_INDENT + "px";
    //dropdownDiv.style.height = dropdownHeight + "px"
    dropdownDiv.style.width = dropdownWidth + "px";
    dropdownDiv.style.maxWidth = dropdownWidthMax + "px";
    dropdownDiv.style.maxHeight = dropdownHeight + "px";

    dropdownDiv.onmouseover = RIUTIL.onMouseOverDropdownList;
    dropdownDiv.onmouseout = RIUTIL.onMouseOutDropdownList;
    doc.body.appendChild(dropdownDiv);

    var listDiv = doc.createElement("DIV");
    listDiv.className = "RIDropDownInnerDiv";
    listDiv.style.width = dropdownWidth + "px";
    dropdownDiv.appendChild(listDiv);

    var listTable = doc.createElement("TABLE");
    listTable.style.width = "100%";
    listTable.className = RIUTIL.CLASS_LAYOUT_TABLE;
    listDiv.appendChild(listTable);

    var listTableBody = doc.createElement("TBODY");
    listTable.appendChild(listTableBody);

    for (var i = 0; i < itemList.length; i++) {
        var item = itemList[i];
        var listTR = doc.createElement("TR");
        listTableBody.appendChild(listTR);
        var listTD = doc.createElement("TD");
        listTR.appendChild(listTD);
        listTD.onmouseover = RIUTIL.onMouseOverDropdownLink;
        listTD.onmouseout = RIUTIL.onMouseOutDropdownLink;
        initItemTag(containerId, doc, listTD, item);
    }
    RIUTIL.isOverDropDownList = true;
}

// Event hanndler: for mouse out the dropdown icon
RIUTIL.onMouseOutDropdownListIcon = function () {
    RIUTIL.isOverDropDownList = false;
    RIUTIL.intervalId = setInterval("RIUTIL.decideDropdownListVisibility(" + RIUTIL.inspectedContainerId + ")", 500);
}

// Event handler: for select the URL template
RIUTIL.goSelectTemplate = function () {
    var doc = this.doc;
    var containerId = RIUTIL.inspectedContainerId;
    RIUTIL.isOverDropDownList = false;
    RIUTIL.decideDropdownListVisibility(containerId);
    var objName = doc.getElementById('objName');

    doc.getElementById('publicLinkUrl').value = this.url;
    doc.getElementById('objDescription').value = this.description;
    objName.value = this.name;

    RIUTIL.changePath(false);
}

// Event Handler: for show maximized mode of content in floating div 
RIUTIL.goFloat = function (containerId, type) {
    var document = RIUTIL.getFrameworkDoc();

    var paneDiv = RIUTIL.getRIPaneDiv(containerId);
    $(paneDiv).addClass("floatingMaxDiv");

    var layoutTable = document.getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + containerId)
    $(layoutTable).addClass("popupWindow floatingMaxTable").removeClass(RIUTIL.CLASS_LAYOUT_TABLE);

    var frame = RIUTIL.getRIPaneIFRAME(containerId);
    $(frame).addClass("floatingMaxFrame");

    if (!RIUTIL.isIOS) {
        $(frame).parent().addClass("floatingMaxTD");
    }
    else {
        $(frame).parent().addClass("floatingMaxiOSiFrameWrapperDiv");
    }

    var riMenuTable = document.getElementById(RIUTIL.ID_RI_MENU_TABLE + containerId);
    $(riMenuTable).addClass("floatingMaxMenuBar");

    var riPaneTitle = document.getElementById("paneTitle" + containerId);
    $(riPaneTitle).addClass("floatingmaxrinowrap");

    var riPaneTitle = document.getElementById("paneTitle" + containerId);
    $(riPaneTitle).addClass("floatingmaxrinowrap");

    RIUTIL.setElemDisplay(document, "objPopup" + containerId, false);
    RIUTIL.setElemDisplay(document, "objUnfloat" + containerId, true);
    RIUTIL.setElemDisplay(document, "objMinimize" + containerId, false);
}

// Event Handler: for show frame content inspect dialog
RIUTIL.floatInspectDialog = function (containerId, mode) {
    var document = RIUTIL.getFrameworkDoc();

    // add some color as placeholder for the original area of inspect frame.
    var paneDiv = RIUTIL.getRIPaneDiv(containerId);
    $(paneDiv).addClass("RIFloadingContainer");

    // make layout float out on the window
    RIUTIL.floattingDialog = $(document.getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + containerId));
    RIUTIL.floattingDialog.addClass("popupWindow floatingInpsctTable aboveUIBlock").removeClass(RIUTIL.CLASS_LAYOUT_TABLE);

    var riMenuTable = document.getElementById(RIUTIL.ID_RI_MENU_TABLE + containerId);
    $(riMenuTable).addClass("floatingMaxMenuBar");

    // clean up menu table
    RIUTIL.removeEvent(riMenuTable, "mouseover", RIUTIL.onMouseOverClassUpdateEvent);
    RIUTIL.removeEvent(riMenuTable, "mouseout", RIUTIL.onMouseOutClassUpdateEvent);
    $(riMenuTable).addClass("popupTitleBar").removeClass("RIMenuTableDesign").removeClass("RIMenuTableHover");

    // show close icon
    RIUTIL.setElemDisplay(document, "objClose" + containerId, true);
    RIUTIL.setElemDisplay(document, "objCloseSpace" + containerId, true);

    // clean up title td
    var titleDiv = document.getElementById("paneTitle" + containerId);
    $(titleDiv).css("width", "100%").css("text-align", "left");
    var titleTD = titleDiv.parentNode;
    RIUTIL.removeEvent(titleTD, "mousedown", RIUTIL.onMouseDownDragEvent);
    $(titleTD).removeClass("RIDragable").addClass("popupTitle");

    // setup event handler for action buttons
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var actionButtonsDiv = doc.getElementById("actionButtonsDiv");
    actionButtonsDiv.style.display = "block";
    var okButton = doc.getElementById("okButton");
    var cancelButton = doc.getElementById("cancelButton");
    switch (mode) {
        case RIUTIL.MODE_CREATE:
            okButton.onclick = function () { parent.parent.parent.RIUTIL.goSaveObj(containerId) };
            cancelButton.onclick = function () { parent.parent.parent.RIUTIL.goDeactivateBegin(containerId) };
            break;

        case RIUTIL.MODE_EDIT:
            okButton.onclick = function () { parent.parent.parent.RIUTIL.goSaveObj(containerId) };
            cancelButton.onclick = function () { parent.parent.parent.RIUTIL.goBack(containerId) };
            break;

        case RIUTIL.MODE_DETAIL:
            okButton.onclick = function () { parent.parent.parent.RIUTIL.goBack(containerId) };
            cancelButton.onclick = function () { parent.parent.parent.RIUTIL.goBack(containerId) };
            break;
    }

    // adjust dialog height and related parent element
    RIUTIL.adjustInspectDialogHeight(containerId);

    // create a blocking div for frame content inspect dialog
    var blockPane = new UIBlockingPane();
    blockPane.blockUI(document);

    // make the content visible after all the visual change made
    $(doc.body).removeClass("RIBody").css("visibility", "visible");
}

// Event Handler: for restore maximized mode of content in floating div back to original mode
RIUTIL.goCloseDialog = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var cancelButton = doc.getElementById("cancelButton");
    cancelButton.click();
}

// Private: adjust height of dialog of inspect form based on the height of content.
RIUTIL.adjustInspectDialogHeight = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var $frame = $(RIUTIL.getRIPaneIFRAME(containerId));
    $bodyContainer = $(doc.getElementById("BodyContainer"));
    var contentHeight = $bodyContainer.height();
    $(doc.body).height(contentHeight);
    $frame.height(contentHeight + 8);
    $frame.width("100%");
    $frame.parent().height(contentHeight + 8)
}

// Event Handler: for restore maximized mode of content in floating div back to original mode
RIUTIL.goUnfloat = function (containerId) {
    var document = RIUTIL.getFrameworkDoc();

    var paneDiv = RIUTIL.getRIPaneDiv(containerId);
    $(paneDiv).removeClass("floatingMaxDiv");

    var layoutTable = document.getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + containerId)
    $(layoutTable).removeClass("floatingMaxTable popupWindow").addClass(RIUTIL.CLASS_LAYOUT_TABLE);

    var frame = RIUTIL.getRIPaneIFRAME(containerId);
    $(frame).removeClass("floatingMaxFrame");

    if (!RIUTIL.isIOS) {
        $(frame).parent().removeClass("floatingMaxTD");
    }
    else {
        $(frame).parent().removeClass("floatingMaxiOSiFrameWrapperDiv");
    }

    var riMenuTable = document.getElementById(RIUTIL.ID_RI_MENU_TABLE + containerId);
    $(riMenuTable).removeClass("floatingMaxMenuBar");

    var riPaneTitle = document.getElementById("paneTitle" + containerId);
    $(riPaneTitle).removeClass("floatingmaxrinowrap");

    RIUTIL.setElemDisplay(document, "objPopup" + containerId, true);
    RIUTIL.setElemDisplay(document, "objUnfloat" + containerId, false);
    RIUTIL.setElemDisplay(document, "objMinimize" + containerId, true);
}

// Event Handler: for show maximized mode of content in new browser window
RIUTIL.goPopup = function (containerId) {
    RIUTIL.resetParameterCapture(containerId);
    var contaienr = RIUTIL.getContainer(containerId);
    if (contaienr.frame.url) {
        //RIUTIL.popupWinMaping[contaienr.frame.objId] = RIUTIL.popupURL(contaienr.frame.url, contaienr.frame.objId);
        var aURL = contaienr.frame.url.slice(0);
        var index = aURL.indexOf("&embededInCompositePage=false");
        if (index > 0) {
            aURL = aURL.replace("&embededInCompositePage=false", "");
        }
        RIUTIL.popupURL(aURL, contaienr.frame.objId);
        return;
    }

    var prodCode = RIUTIL.getActiveProduct();
    RIUTIL.postAjaxRequest(containerId, prodCode, RIUTIL.AJAX_CMD_POPUP_URL);
}

// Event Handler: for save the current layout
RIUTIL.goSelectCustomizedLayout = function (e) {
    JSSidePanelRender.reqClose();
    RIUTIL.setPageDesignBarEnable(false);
    var elem = RIUTIL.getEventSource(e);
    var layoutId = elem.options[elem.options.selectedIndex].value;
    if (RIUTIL.isCompositePage) {
        var user = elem.options[elem.options.selectedIndex].user;
        return RIUTIL.selectCustomizedLayout(layoutId, user);
    }
    else {
        return RIUTIL.selectCustomizedLayout(layoutId);
    }
}

// Private: use intent to select layoutid but need confirmations
RIUTIL.selectCustomizedLayout = function (layoutId, user) {
    if (layoutId.indexOf("|") == -1 && RIUTIL.isCompositePage) {
        layoutId = layoutId + "|" + user;
    }
    if (RIUTIL.isCompositePage && (RIUTIL.layoutManager === undefined || !RIUTIL.layoutManager.designMode)) // for composite page runtime mode, do not need to clean up server side owvirtual because we want to reuse it when later switch back
    {
        RIUTIL.postAjaxRequest(null, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SELECT_CUSTOMIZED_LAYOUT, RIUTIL.PARAM_LAYOUT_ID, layoutId);
        RIUTIL.setPageDesignBarEnable(true);
        return true;
    }
    else // cafeone logic and design mode composite logic
    {
        var continueSyncLayout = !RIUTIL.layoutManager.designMode || !RIUTIL.cancelActionForDirtyCheck();
        if (continueSyncLayout)
            continueSyncLayout = RIUTIL.confirmContinueWithLayoutDirtyCheck();

        if (continueSyncLayout) {
            RIUTIL.cleanupAllSatelliteForms();
            if (RIUTIL.isCompositePage) {
                RIUTIL.postAjaxRequest(null, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SELECT_CUSTOMIZED_LAYOUT, RIUTIL.PARAM_LAYOUT_ID, layoutId);
            }
            else {
                RIUTIL.postAjaxRequest(null, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SELECT_CUSTOMIZED_LAYOUT, RIUTIL.PARAM_LAYOUT_ID, layoutId);
            }
            RIUTIL.setPageDesignBarEnable(true);
            return true;
        }
        else {
            RIUTIL.inProcessOfRollBackSelectionChange = true;
            RIUTIL.updateLayoutSelections();
            RIUTIL.inProcessOfRollBackSelectionChange = false;
            RIUTIL.setPageDesignBarEnable(true);
            return false;
        }
    }
}

// Event Handler: for test the preview version of URL for popup icon
RIUTIL.goDeactivate = function (containerId) {
    RIUTIL.goStopKeyCapture();
    var contaienr = RIUTIL.getContainer(containerId);

    if (RIUTIL.isCompositePage) {
        RIUTIL.saveFreeStyleFrames();
        RIUTIL.saveWLComponentFrames();
        RIUTIL.saveLPContentFrames();
    }

    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_DEACTIVATE_CONTENT);
}

// Event Handler: begin of annimation version for deactive event handler
RIUTIL.goDeactivateBegin = function (containerId) {
    RIUTIL.goStopKeyCapture();
    var container = RIUTIL.getContainer(containerId);

    if (RIUTIL.isE1SatelliteContainer(container)) // special cleanup for embedded E1 form
    {
        RIUTIL.cleanupContainer(containerId);
    }

    var paneDiv = RIUTIL.getRIPaneDiv(containerId);

    // start point
    var sourceState = RIUTIL.getElementDemension(paneDiv);

    // end point
    var openContentButton = top.document.getElementById("RI_OPEN_CONTENT");
    var targetState =
        {
            left: getAbsoluteLeftPos(openContentButton) - (RIUTIL.isRTL ? openContentButton.offsetWidth : 0),
            top: 0,
            width: 16,
            height: 16
        }

    // animated div
    paneDiv.style.display = "none"; // remove it from dom will cause the closeURL fail to work in FF and Chrome

    var flyDiv = RIUTIL.createFlyDiv(containerId, null);
    if (RIUTIL.isCompositePage) {
        RIUTIL.saveFreeStyleFrames();
        RIUTIL.saveWLComponentFrames();
        RIUTIL.saveLPContentFrames();
    }

    RIUTIL.animateDemensionChange(flyDiv, sourceState, targetState, "RIUTIL.goDeactivateEnd(" + containerId + ")");
}

// Event Handler: end of annimation version for deactive event handler
RIUTIL.goDeactivateEnd = function (containerId) {
    RIUTIL.destoryFlyDiv(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_DEACTIVATE_CONTENT);
}

// Event Handler: no annimation version for minimized event handler
RIUTIL.goMinimize = function (containerId) {
    var contaienr = RIUTIL.getContainer(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_MINIMIZE_CONTENT);
}

// Event Handler: begin of annimation version for minimized event handler
RIUTIL.goMinimizeWithDirtyCheck = function (containerId) {
    var container = RIUTIL.getContainer(containerId);
    if (RIUTIL.continueWithDirtyCheck(container, false)) // do not need to update sync info for this case
        RIUTIL.goMinimizeBegin(containerId, true);
}

// Event Handler: begin of annimation version for minimized event handler
RIUTIL.goMinimizeBegin = function (containerId, continueAction) {
    if (!continueAction)
        return;

    var container = RIUTIL.getContainer(containerId);
    var paneDiv = RIUTIL.getRIPaneDiv(containerId);
    if (RIUTIL.isE1SatelliteContainer(container)) // special cleanup for embedded E1 form
    {
        RIUTIL.cleanupContainer(containerId);
    }

    // start point
    var sourceState = RIUTIL.getElementDemension(paneDiv);

    // end point
    container.winState = RIUTIL.WIN_STATE_MINIMIZED;
    var futureMiniContainerIds = RIUTIL.getFrameIds(RIUTIL.layout, new Array(), true);
    var currentMiniContainerIds = RIUTIL.miniContainerIds;
    var targetState = RIUTIL.getMiniTabDemension(futureMiniContainerIds, containerId, false);

    // animated div
    paneDiv.style.display = "none"; // remove it from dom will cause the closeURL fail to work in FF and Chrome

    var flyDiv = RIUTIL.createFlyDiv(containerId, null);
    RIUTIL.animateDemensionChange(flyDiv, sourceState, targetState, "RIUTIL.goMinimizeEnd(" + containerId + ")");

    for (var i = 0; i < currentMiniContainerIds.length; i++) {
        var containerId = currentMiniContainerIds[i];
        var sourceState = RIUTIL.getMiniTabDemension(currentMiniContainerIds, containerId, true);
        var targetState = RIUTIL.getMiniTabDemension(futureMiniContainerIds, containerId, true);
        var tabDiv = document.getElementById("MiniTab" + containerId);
        if (tabDiv)
            RIUTIL.animateDemensionChange(tabDiv, sourceState, targetState);
    }
}

// Event Handler: end of annimation version for minimized event handler
RIUTIL.goMinimizeEnd = function (containerId) {
    RIUTIL.destoryFlyDiv(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_MINIMIZE_CONTENT);
}

// Event Handler: for normalize frame
RIUTIL.goNormalize = function (e) {
    var srcElem = RIUTIL.getEventSource(e);
    RIUTIL.removeEvent(srcElem, "click", RIUTIL.goNormalize);
    var infoTag = RIUTIL.searchParentByPorpName(srcElem, "containerId");
    var containerId = infoTag.containerId;
    var contaienr = RIUTIL.getContainer(containerId);
    RIUTIL.oldMiniContainerIds = RIUTIL.miniContainerIds;
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_NORMALIZE_CONTENT);
}

// Public: invoke by server for animation of normalize process after server process nomoralize 
RIUTIL.goNormalizeBegin = function (containerId) {
    if (!RIUTIL.supportAnimation)
        return;

    var contaienr = RIUTIL.getContainer(containerId);
    var paneDiv = RIUTIL.getRIPaneDiv(containerId);
    var currentMiniContainerIds = RIUTIL.miniContainerIds;

    // start point
    var oldMiniContainerIds = RIUTIL.oldMiniContainerIds
    var sourceState = RIUTIL.getMiniTabDemension(oldMiniContainerIds, containerId, false);

    // end point
    var targetState = RIUTIL.getElementDemension(paneDiv);

    // animated div
    var clonePaneDiv = paneDiv.cloneNode(false); // shallow clone to avoid duplicated shortcut launcher for embedded E1 form, which cause racing condition of two concurrency same request.
    clonePaneDiv.id = "tempFlyPane";
    paneDiv.style.visibility = "hidden";

    var flyDiv = RIUTIL.createFlyDiv(containerId, clonePaneDiv);
    RIUTIL.animateDemensionChange(flyDiv, sourceState, targetState, "RIUTIL.goNormalizeEnd(" + containerId + ")");

    for (var i = 0; i < oldMiniContainerIds.length; i++) {
        var containerId = currentMiniContainerIds[i];
        var sourceState = RIUTIL.getMiniTabDemension(oldMiniContainerIds, containerId, true);
        var targetState = RIUTIL.getMiniTabDemension(currentMiniContainerIds, containerId, true);
        var tabDiv = document.getElementById("MiniTab" + containerId);
        if (tabDiv)
            RIUTIL.animateDemensionChange(tabDiv, sourceState, targetState);
    }
}

// Public: end of annimation version for minimized event handler
RIUTIL.goNormalizeEnd = function (containerId) {
    RIUTIL.destoryFlyDiv(containerId);
    var paneDiv = RIUTIL.getRIPaneDiv(containerId);
    paneDiv.style.visibility = "visible";
}

// Pirvate: create a fly div and it contain the elem in it
RIUTIL.createFlyDiv = function (containerId, elem) {
    var div = document.createElement("div");
    div.style.position = 'absolute';
    div.style.zIndex = 1000;
    div.style.overflow = "hidden";
    div.id = "animated_" + containerId;
    div.className = "RIFloadingContainer";
    if (elem) {
        elem.className = "RIHalfTransparent";
        div.appendChild(elem);
    }
    document.body.appendChild(div);
    return div;
}

// Pirvate: create a fly div
RIUTIL.destoryFlyDiv = function (containerId) {
    // clean up the animation
    var div = document.getElementById("animated_" + containerId);
    if (div)
        div.parentNode.removeChild(div);
}

// Private: get left, top, width, height of a given mini tab name based on the position in the list
RIUTIL.getMiniTabDemension = function (miniContainerIds, containerId, rotated) {
    // setup default values
    var found = false;

    // if in a tab Form Inteconnect format, we need to use different parent node 
    var parentNode = document.getElementById(RIUTIL.ID_MODELESS_TAB_DIV);
    if (!parentNode) // without modeless tab
    {
        var frameworkHeight = RIUTIL.getClientHeight() - RIUTIL.PADDING_E1PANEDIV;
    }
    else //with modless tab
    {
        var frameworkHeight = RIUTIL.getClientHeight() - RIUTIL.PADDING_E1PANEDIV_MODELESS_TAB;
    }

    var topOffset = RIUTIL.getTransitionTopOffset();
    var _left = RIUTIL.isRTL ? RIUTIL.MINI_BAR_RIGHT_PADDING + RIUTIL.getTransitionLeftOffset() : document.body.clientWidth - RIUTIL.MINI_BAR_WIDTH - RIUTIL.MINI_BAR_RIGHT_PADDING - RIUTIL.getTransitionRightOffset();
    var _top = topOffset;
    var _width = RIUTIL.MINI_BAR_WIDTH + RIUTIL.MINI_BAR_RIGHT_PADDING;
    var _height = RIUTIL.MINI_TAB_MAX_HEIGHT;

    for (var i = 0; i < miniContainerIds.length; i++) {
        if (containerId == miniContainerIds[i]) {
            found = true;
            break;
        }
    }

    if (!found) // use default if not found
    {
        return {
            top: _top,
            left: _left,
            width: _width,
            height: _height
        }
    }
    else // if found caculate the demension
    {
        if (rotated) // the target is rotated and we only need to change the height (width after rotated) and top
        {
            var tabDistance = RIUTIL.calculateTabDistance(frameworkHeight, miniContainerIds);
            var _top = RIUTIL.calculateTabTop(tabDistance, i);
            var _width = RIUTIL.calculateTabWidth(tabDistance);

            return {
                top: _top,
                width: _width
            }
        }
        else // normal oritation without rotated
        {
            var _height = Math.min(RIUTIL.MINI_TAB_MAX_HEIGHT, frameworkHeight / miniContainerIds.length);

            var _top = _height * i + topOffset;

            return {
                top: _top,
                left: _left,
                width: _width,
                height: _height
            }
        }
    }
}

// Private: get the left, top, width, height of a given element.
RIUTIL.getElementDemension = function (elem) {
    return {
        left: getAbsoluteLeftPos(elem) + RIUTIL.getTransitionLeftOffset(),
        top: getAbsoluteTopPos(elem) + RIUTIL.getTransitionTopOffset(),
        width: elem.offsetWidth,
        height: elem.offsetHeight
    }
}

// Private: get vertical offset of mini bar starting position
RIUTIL.getTransitionTopOffset = function () {
    var offset = RIUTIL.MINI_BAR_TOP_PADDING;
    try {
        if (RIUTIL.isCompositePage) {
            var $tabContainer = $(RIUTIL.getE1PaneIFrame().contentWindow.document.getElementById("tabContainer"));
            var tabContainerConsumedHeight = ($tabContainer.css("display") == 'block' ? $tabContainer.height() : 0); // when tab container is opened an not collpased
            var $carousel = $("#carousel")
            var carouselConsumedHeight = ($carousel.hasClass("caroTop") && $carousel.hasClass("caroExpanded")) ? $carousel.height() : 0; // when carousel expend on the top
            offset = 9 + $("#brandingDiv").height() + $("#breadcrumbBarDiv").height() + carouselConsumedHeight + tabContainerConsumedHeight;
        }
    }
    catch (e) {
    }

    return offset;
}

// Private: get horizontal right offset of mini bar to discount the carousel
RIUTIL.getTransitionRightOffset = function () {
    var offset = 0;
    try {
        if (RIUTIL.isCompositePage) {
            var $carousel = $("#carousel")
            offset = ($carousel.hasClass("caroRight") && $carousel.hasClass("caroExpanded")) ? $carousel.width() : 0; // when carousel expend on the right
        }
    }
    catch (e) {
    }

    return offset;
}

// Private: get horizontal left offset of frame element to discount the carousel
RIUTIL.getTransitionLeftOffset = function () {
    var offset = 0;
    try {
        if (RIUTIL.isCompositePage) {
            var $carousel = $("#carousel")
            offset = ($carousel.hasClass("caroLeft") && $carousel.hasClass("caroExpanded")) ? $carousel.width() : 0; // when carousel expend on the left
        }
    }
    catch (e) {
    }

    return offset;
}

// Private: animate the demension change
RIUTIL.animateDemensionChange = function (elem, sourceState, targetState, callback) {
    var duration = RIUTIL.ANIMATION_DURATION;
    if (!isNaN(sourceState.height) && !isNaN(targetState.height))
        ANIM.animate(elem, 'changeHeight', sourceState.height, targetState.height, duration, 0);
    if (!isNaN(sourceState.width) && !isNaN(targetState.width))
        ANIM.animate(elem, 'changeWidth', sourceState.width, targetState.width, duration, 0);
    if (!isNaN(sourceState.left) && !isNaN(targetState.left))
        ANIM.animate(elem, 'sinSlideX', sourceState.left, targetState.left, duration, 0);
    if (!isNaN(sourceState.top) && !isNaN(targetState.top))
        ANIM.animate(elem, 'sinSlideY', sourceState.top, targetState.top, duration, 0, callback);
}

// Event Handler: to select the default  content type (the first content frame) for a composite page
RIUTIL.onClickDefaultContentEvent = function (e) {
    var srcElem = RIUTIL.getEventSource(e);
    var infoTag = RIUTIL.searchParentByPorpName(srcElem, "containerId");
    var pathType = infoTag.pathType;
    var movingContainerId = infoTag.containerId;
    var movingObjectId = infoTag.objectId;
    var targetContainerId = RIUTIL.layout.id;
    var targetZone = RIUTIL.ZONE_BOTTOM;
    var percentage = 100;
    RIUTIL.setActiveProduct(RIUTIL.PRODUCT_GENERIC_URL);
    RIUTIL.postAjaxRequest(targetContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_MOVE_IN_CONTAINER, RIUTIL.PARAM_MOVING_CONTAINER, movingContainerId, RIUTIL.PARAM_MOVING_OBJECT, movingObjectId, RIUTIL.PARAM_TARGET_CONTAINER, targetContainerId, RIUTIL.PARAM_ZONE, targetZone, RIUTIL.PARAM_PERCENTAGE, percentage, RIUTIL.PARAM_PATH_TYPE, pathType);
    var table = RIUTIL.searchParentByPorpName(srcElem, 'popupWin');
    table.popupWin.onClose()
}

// Event Handler: for mouse down event of drag and drop of change displayed tab order
RIUTIL.onMouseDownDragEvent = function (e) {
    var document = RIUTIL.getFrameworkDoc();
    if (RIUTIL.layout.isEditting) {
        alert(RIUTIL.CONSTANTS.RI_CHANGE_LAYOUT_WARN);
        return;
    }

    RIUTIL.removeDragIndicator();
    var srcElem = RIUTIL.getEventSource(e);

    RIUTIL.dragIndicator = RIUTIL.createDragIndicator(srcElem);
    RIUTIL.dragIndicator.startTime = (new Date()).getTime();
    var eventBlocker = RIUTIL.createEventBlocker(RIUTIL.layout.id, RIUTIL.onMouseUpDragOutterEvent);

    RIUTIL.dragMode = true;
    RIUTIL.addEvent(eventBlocker, "mouseup", RIUTIL.onMouseUpDragEvent);
    RIUTIL.addEvent(eventBlocker, "mousemove", RIUTIL.onMouseMoveDragEvent);
    RIUTIL.disableDOMHighlighting(document.body);
    RIUTIL.disableDOMHighlighting(top.document.body);
    if (RIUTIL.isCompositePage) {
        RIUTIL.saveFreeStyleFrames();
        RIUTIL.saveWLComponentFrames();
        RIUTIL.saveLPContentFrames();
    }
    return RIUTIL.stopEventBubbling(e);
}

// Event Handler: for mouse move event of drag and drop of change displayed tab order
RIUTIL.onMouseMoveDragEvent = function (e) {
    if (RIUTIL.dragMode) {
        var srcElem = RIUTIL.getEventSource(e);
        var movingContainerId = RIUTIL.dragIndicator.movingContainerId;
        var curPos = RIUTIL.getCurrentPosition(e, false);
        //window.status = curPos.x + "," + curPos.y
        //RIUTIL.setDragIndicatorDisplay(true);
        var moveOutside = RIUTIL.moveDragIndicator(curPos.x, curPos.y);
        var container = RIUTIL.getContainer(movingContainerId);
    }
    return RIUTIL.stopEventBubbling(e);
}

// Event Handler for mouse up event of drag and drop of change displayed tab order
RIUTIL.onMouseUpDragEvent = function (e) {
    var document = RIUTIL.getFrameworkDoc();
    var moveRegion = RIUTIL.getDragIndicator();
    var startTime = moveRegion.startTime;
    var currentTime = (new Date()).getTime();
    // if use replease mouse too quick like a click action, we will give it a warning message to ask for drag and drop action instead, and we do not process this action.
    if (currentTime - startTime < RIUTIL.MIN_DRAG_ACTION_TIME) {
        alert(RIUTIL.CONSTANTS.RI_DRAG_DROP_WARN)
    }
    else {
        if (moveRegion.moveOutside) // move outside of the original region
        {
            var movingContainerId = moveRegion.movingContainerId;
            var movingObjectId = moveRegion.movingObjectId;
            var targetContainerId = moveRegion.targetContainerId;
            var targetZone = moveRegion.targetZone;
            var percentage = moveRegion.percentage;
            var pathType = RIUTIL.dragIndicator.pathType;
            RIUTIL.setActiveProduct(RIUTIL.PRODUCT_GENERIC_URL);
            RIUTIL.postAjaxRequest(targetContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_MOVE_IN_CONTAINER, RIUTIL.PARAM_MOVING_CONTAINER, movingContainerId, RIUTIL.PARAM_MOVING_OBJECT, movingObjectId, RIUTIL.PARAM_TARGET_CONTAINER, targetContainerId, RIUTIL.PARAM_ZONE, targetZone, RIUTIL.PARAM_PERCENTAGE, percentage, RIUTIL.PARAM_PATH_TYPE, pathType);
        }
    }

    // clean up
    RIUTIL.dragMode = false;
    RIUTIL.removeDragIndicator();
    RIUTIL.removeEvent(top.document.body, "mouseup", RIUTIL.onMouseUpDragOutterEvent);
    RIUTIL.enableDOMHighlighting(document.body);
    RIUTIL.enableDOMHighlighting(top.document.body);
    RIUTIL.removeEventBlocker(RIUTIL.layout.id, RIUTIL.onMouseUpDragOutterEvent);
}

// Event Handler: close icon clicked on inpsect Page
RIUTIL.goBack = function (containerId) {
    RIUTIL.resetParameterCapture(containerId);
    RIUTIL.goStopKeyCapture();
    RIUTIL.cleanupKeys(containerId);
    var container = RIUTIL.getContainer(containerId);
    RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_DISPLAY_FRAME_CONTENT);
}

// Event Handler: mouse dow event 
RIUTIL.onMouseDownPaletteEvent = function (e) {
    var srcElem = RIUTIL.getEventSource(e);
    var table = RIUTIL.searchParentByPorpName(srcElem, 'popupWin');
    table.popupWin.onClose()
}

// Event Handler: mouse move over of the action button of Page Design Mode
RIUTIL.onMouseOverActionEvent = function (e) {
    var img = this.firstChild;
    if (img) {
        if (img.overSrc != null)
            img.src = img.overSrc;
        else if (img.disabled)
            img.src = img.outSrc;
    }
    this.className = this.hoverClassName;
}

// Event Handler: mouse move out of the action button of Page Design Mode
RIUTIL.onMouseOutActionEvent = function (e) {
    var img = this.firstChild;
    if (img)
        img.src = img.outSrc;
    this.className = this.normalClassName;
}

// Event Handler: switch to runtime mode with confirmation
RIUTIL.goLayoutRuntimeMode = function () {
    RIUTIL.setPageDesignBarEnable(false);
    if (RIUTIL.cancelActionForDirtyCheck()) {
        RIUTIL.setPageDesignBarEnable(true);
        return;
    }

    RIUTIL.setPageDesignBarEnable(true);
    JSSidePanelRender.close();
    RIUTIL.postAjaxRequest(null, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_LAYOUT_RUNTIME_MODE);
}

// Event Handler: beging to switch to runtime mode with confirmation (animation version)
RIUTIL.goLayoutRuntimeModeBegin = function () {
    RIUTIL.setPageDesignBarEnable(false);
    if (RIUTIL.cancelActionForDirtyCheck()) {
        RIUTIL.setPageDesignBarEnable(true);
        return;
    }

    RIUTIL.setPageDesignBarEnable(true);
    JSSidePanelRender.close();

    var barDiv = top.document.getElementById("PerseonalizationBar");
    var startTop = parseInt(barDiv.style.top); // have to get the top value from sytle instead of offsetTop, which is incorrect value.
    var endTop = startTop - barDiv.offsetHeight;
    top.ANIM.animate(barDiv, 'sinSlideY', startTop, endTop, RIUTIL.ANIMATION_DURATION, 0, null);

    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);

    if (!RIUTIL.isCompositePage && RIUTIL.isLcmPreview() && (activeLayout.isPersonal || activeLayout.isRequestPublished || activeLayout.isRework)) {
        RIUTIL.postAjaxRequest(null, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_LAYOUT_PREVIEW_MODE, RIUTIL.PARAM_LAYOUT_ID, cafeOneUDOObject.WebObjectId, RIUTIL.PARAM_LAYOUT_NAME, "", RIUTIL.PARAM_CLOSEDESIGNBAR, true);
    }
    else {
        setTimeout("RIUTIL.goLayoutRuntimeModeEnd()", RIUTIL.ANIMATION_DURATION);
    }
}

// Event Handler: end to switch to runtime mode with confirmation (animation version)
RIUTIL.goLayoutRuntimeModeEnd = function () {
    RIUTIL.cleanupAllSatelliteForms();
    RIUTIL.postAjaxRequest(null, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_LAYOUT_RUNTIME_MODE);
}

// Event Handler: send request to server to iniiate the rednering of the content factory
RIUTIL.goShowContentFactory = function () {
    RIUTIL.setPageDesignBarEnable(false);
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_GET_CONTENT_FACTORY);
}

// Event Handler: send request to server to iniiate the rednering of the content pallette
RIUTIL.goShowContentPalette = function () {
    RIUTIL.setPageDesignBarEnable(false);
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_GET_CONTENT_PALETTE);
}

// Event Handler: start the end user event edit process
RIUTIL.goEditEndUserEvent = function () {
    JSSidePanelRender.reqOpen(JSSidePanelRender.MODE_EDIT_END_USER_EVENT);
}

// Event Handler: save the existing layout
RIUTIL.goSaveLayout = function () {
    // validate the current opened event rules before save
    var error = RIUTIL.validateUserEventRules()
    if (error) {
        RIUTIL.renderError(error);
        return;
    }

    RIUTIL.setPageDesignBarEnable(false);
    if (RIUTIL.isCompositePage) {
        RIUTIL.saveFreeStyleFrames();
        RIUTIL.saveWLComponentFrames();
        RIUTIL.saveLPContentFrames();
    }
    var currentLayoutId = RIUTIL.layoutManager.currentLayoutId;
    var newLayoutDescription = "";
    if (currentLayoutId == RIUTIL.DEFAULT_LAYOUT_ID) {
        return RIUTIL.goSaveAsLayoutBegin();
    }
    else if (RIUTIL.isCompositePage)//for composite page, update the description and the system code
    {
        if (RIUTIL.layout.isBlank) {
            alert(RIUTIL.CONSTANTS.RI_SAVE_BLANK_LAYOUT);
            RIUTIL.setPageDesignBarEnable(true);
            return;
        }
        if (document.getElementById(RIUTIL.ID_LAYOUT_DESCRIPTION) != null) {
            newLayoutDescription = document.getElementById(RIUTIL.ID_LAYOUT_DESCRIPTION).value;
        }


        var prodCode = document.getElementById(RIUTIL.ID_LAYOUT_PRODCODE).value;
        if (RIUTIL.isValidProdCode(prodCode)) {
            RIUTIL.setProductSysCode(prodCode);
        }
        else {
            RIUTIL.setPageDesignBarEnable(true);
            return;
        }
    }
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_SAVE_CUSTOMIZED_LAYOUT, RIUTIL.PARAM_LAYOUT_DESCRIPTION, newLayoutDescription);
}

// Private: save the metadata of all freestyle frames in the workspace 
RIUTIL.saveFreeStyleFrames = function () {
    var containerList = new Array();
    RIUTIL.getFreeStyleSatelliteContainerList(RIUTIL.layout, containerList, true);
    for (var i = 0; i < containerList.length; i++) {
        var container = containerList[i];
        var win = RIUTIL.getContainerWinObject(container.id);
        var containerId = container.id;
        if (!win)
            return false;
        //var jsonString = "{test:true}"//
        if (win.FreeStyle) // skip the create mode of freestyle
        {
            var jsonString = win.FreeStyle.getJSONString();
            RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_FREESTYLE_METADATA, RIUTIL.PARAM_CONTAINER_ID, containerId, RIUTIL.PARAM_METADATA, jsonString);
        }
    }
    return true;
}

// Private: save the metadata of all watchlist component frames in the workspace 
RIUTIL.saveWLComponentFrames = function () {
    var containerList = new Array();
    RIUTIL.getWLComponentSatelliteContainerList(RIUTIL.layout, containerList, true);
    for (var i = 0; i < containerList.length; i++) {
        var container = containerList[i];
        var win = RIUTIL.getContainerWinObject(container.id);
        var containerId = container.id;
        if (!win)
            return false;

        if (win.WLComponent) {
            var jsonString = win.WLComponent.getJSONString();
            RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_WL_PANE_METADATA, RIUTIL.PARAM_CONTAINER_ID, containerId, RIUTIL.PARAM_METADATA, jsonString);
        }
    }
    return true;
}

// Private: save the metadata of all watchlist component frames in the workspace 
RIUTIL.saveLPContentFrames = function () {
    var containerList = new Array();
    RIUTIL.getLPComponentSatelliteContainerList(RIUTIL.layout, containerList, true);
    for (var i = 0; i < containerList.length; i++) {
        var container = containerList[i];
        var win = RIUTIL.getContainerWinObject(container.id);
        var containerId = container.id;
        if (!win)
            return false;

        if (win.LPContent) {
            var jsonString = win.LPContent.getJSONString();
            RIUTIL.postAjaxRequest(containerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_LP_PANE_METADATA, RIUTIL.PARAM_CONTAINER_ID, containerId, RIUTIL.PARAM_METADATA, jsonString);
        }
    }
    return true;
}

// Private: deep recursively search and get collections of all freestyle frames given the root container 
RIUTIL.getFreeStyleSatelliteContainerList = function (container, containerList, normalizedContentOnly) {
    RIUTIL.getSatelliteContainerListByType(RIUTIL.CONATINAER_FREESTYLE, container, containerList, normalizedContentOnly);
}

// Private: deep recursively search and get collections of all wl component frames given the root container 
RIUTIL.getWLComponentSatelliteContainerList = function (container, containerList, normalizedContentOnly) {
    RIUTIL.getSatelliteContainerListByType(RIUTIL.CONATINAER_WL_COMPONENT, container, containerList, normalizedContentOnly);
}

// Private: deep recursively search and get collections of all wl component frames given the root container 
RIUTIL.getLPComponentSatelliteContainerList = function (container, containerList, normalizedContentOnly) {
    RIUTIL.getSatelliteContainerListByType(RIUTIL.CONATINAER_LANDING_PAGE, container, containerList, normalizedContentOnly);
}
// Private: deep recursively search and get collections of all frames given the root container and container type
RIUTIL.getSatelliteContainerListByType = function (targetContainerType, container, containerList, normalizedContentOnly) {
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            if (container.subContainers) {
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    RIUTIL.getSatelliteContainerListByType(targetContainerType, subContainer, containerList, normalizedContentOnly);
                }
            }
            break;

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            if (container.containerType == targetContainerType) {
                containerList.push(container);
            }
            break;
    }
}

// Event Handler: begin process of save layout action
RIUTIL.goSaveAsLayout = function () {
    if (RIUTIL.isCompositePage) {
        RIUTIL.saveFreeStyleFrames();
        RIUTIL.saveWLComponentFrames();
        RIUTIL.saveLPContentFrames();
    }
    RIUTIL.goSaveAsLayoutBegin();
}

// Event Handler: begin process of save layout action
RIUTIL.goSaveAsLayoutBegin = function () {
    if (RIUTIL.layout.isBlank) {
        alert(RIUTIL.CONSTANTS.RI_SAVE_BLANK_LAYOUT);
        RIUTIL.setPageDesignBarEnable(true);
        return;
    }

    // validate the current opened event before save
    var error = RIUTIL.validateUserEventRules();
    var stopSave = false;
    if (error) {
        RIUTIL.renderError(error);
        return;
    }

    RIUTIL.setPageDesignBarEnable(false);
    var onClose = function (e) {
        JSSidePanelRender.hideUIBlock();
        // since goSaveAsLayoutMiddle exectued first before this line, we can use that flag to determine if we want to enable menu again, because we do not want
        // user to click around before the loadding process is complete.
        if (!RIUTIL.goSaveAsLayoutMiddleStarted)
            RIUTIL.setPageDesignBarEnable(true);
    };

    if (RIUTIL.isCompositePage) {
        var prodCode = document.getElementById(RIUTIL.ID_LAYOUT_PRODCODE).value;
        if (!RIUTIL.isValidProdCode(prodCode)) {
            RIUTIL.setPageDesignBarEnable(true);
            stopSave = true;
        }
        else {
            RIUTIL.setProductSysCode(prodCode);
        }
    }

    if (!stopSave) {
        RIUTIL.goSaveAsLayoutMiddleStarted = false;
        var labels = new Array(RIUTIL.CONSTANTS.UDO_PROMPT_DEFAULT_NAME, RIUTIL.CONSTANTS.UDO_PROMPT_NAME);
        JSSidePanelRender.renderPromptForName(top.document, null, 2, labels, 30, RIUTIL.goSaveAsLayoutMiddle, null, onClose);
    }
}

// Event Handler: middle process of save layout action
RIUTIL.goSaveAsLayoutMiddle = function () {
    RIUTIL.goSaveAsLayoutMiddleStarted = true;
    var newLayoutName = document.getElementById(JSSidePanelRender.ID_NAME_ENTERED).value;
    var newLayoutDescription = "";
    if (document.getElementById(RIUTIL.ID_LAYOUT_DESCRIPTION) != null) {
        newLayoutDescription = document.getElementById(RIUTIL.ID_LAYOUT_DESCRIPTION).value;
    }
    if (newLayoutDescription == "") {
        newLayoutDescription = newLayoutName;
    }

    if (!RIUTIL.isValidName(newLayoutName)) {
        return;
    }

    RIUTIL.cleanupAllSatelliteForms();
    var e1URL = RIUTIL.parameterizeActiveLayout(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_SAVE_AS_CUSTOMIZED_LAYOUT, newLayoutName, null, newLayoutDescription);
    RIUTIL.sendXMLReq("POST", RIUTIL.layout.id, e1URL.toString(), RIUTIL.goSaveAsLayoutEnd);
    return true;
}

// Event Handler: end process of save layout action
RIUTIL.goSaveAsLayoutEnd = function (containerId, retStr) {
    var errMsg = RIUTIL.retrieveErrorMessage(retStr);
    if (errMsg) {
        RIUTIL.renderMessage(true, errMsg, true);
    }
    else {
        try {
            eval(retStr);
        }
        catch (e) {
            RIUTIL.renderMessage(true, retStr, true);
        }
    }
    RIUTIL.setPageDesignBarEnable(true);
    if (RIUTIL.isCompositePage) {
        RIUTIL.resetKeepAlive();
    }
}

// Event Handler: for save the current layout
RIUTIL.goDeleteLayout = function (evt) {
    RIUTIL.setPageDesignBarEnable(false);

    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);

    var confimToDelete = confirm((RIUTIL.isCompositePage ? RIUTIL.CONSTANTS.RI_DEL_PAGE_CONFORM : RIUTIL.CONSTANTS.RI_DEL_LAYOUT_CONFORM) + " (" + activeLayout.name + ")")
    if (confimToDelete) {
        RIUTIL.cleanupAllSatelliteForms();
        RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_DELETE_CUSTOMIZED_LAYOUT);
    }
    else {
        RIUTIL.setPageDesignBarEnable(true);
        return RIUTIL.stopEventBubbling(evt);
    }
}

RIUTIL.goLayoutEditOrPreviewMode = function (cafeOneUDOObject) {
    if (RIUTIL.isLcmEdit()) {
        RIUTIL.goLayoutDesignMode();
    }
    else if (RIUTIL.isLcmPreview()) {
        RIUTIL.goLayoutPreviewMode(cafeOneUDOObject);
    }
}

RIUTIL.goLayoutPreviewMode = function (cafeOneUDOObject) {
    RIUTIL.cleanupAllSatelliteForms();
    if (typeof cafeOneUDOObject != 'undefined') {
        RIUTIL.postAjaxRequest(null, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_LAYOUT_PREVIEW_MODE, RIUTIL.PARAM_LAYOUT_ID, cafeOneUDOObject.WebObjectId);
    }
}

// Event Handler: called from dropdownmenus.js to switch to page design mdoe
RIUTIL.goLayoutDesignMode = function () {
    JSSidePanelRender.reqClose();
    if (!RIUTIL.isCompositePage) {
        RIUTIL.cleanupAllSatelliteForms();
    }

    RIUTIL.postAjaxRequest(null, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_LAYOUT_DESIGN_MODE);

    if (RIUTIL.isLcmPreview() && !RIUTIL.isCompositePage) {
        var layoutId = RIUTIL.layoutManager.currentLayoutId;
        var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
        if (activeLayout.isPersonal || activeLayout.isRequestPublished || activeLayout.isRework) {
            RIUTIL.postAjaxRequest(null, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_LAYOUT_PREVIEW_MODE, RIUTIL.PARAM_LAYOUT_ID, cafeOneUDOObject.WebObjectId, RIUTIL.PARAM_LAYOUT_NAME, "", RIUTIL.PARAM_CLOSEDESIGNBAR, false);
        }
    }
}

// Public: called by JDEDTA to collect container id need to to be refreshed due to a control change given form header control id
RIUTIL.onFormHeaderChange = function (id) {
    RIUTIL.isRIDisplayed() && RIUTIL.onControlChange("C" + id);
}

// Public: called by JDEDTA to collect container id need to to be refreshed due to a control change given grid column
RIUTIL.onGridColChange = function (gridObj, colIndex) {
    var id = "GC" + gridObj.gridId + "_" + colIndex;
    RIUTIL.isRIDisplayed() && RIUTIL.onControlChange(id);
}

// Public: called by JDEGrid to collect container id need to to be refreshed due to cafeone row cursor change
RIUTIL.onGridRowChange = function (gridObj) {
    var id = "G" + gridObj.gridId;
    RIUTIL.isRIDisplayed() && RIUTIL.onControlChange(id);
}

// Priivate: collect all impacted containers due to the control value change
RIUTIL.onControlChange = function (id) {
    id = id.replace(/\./g, "_");
    RIUTIL.collectImpactedContainers(RIUTIL.layout, id);
    if (!RIUTIL.layoutManager.designMode) // only check impact of layout for runtime mode
        RIUTIL.collectImpactedLayout(id);
}

// Event Handler: mouser over the item row of content palette
RIUTIL.onMouseOverContentPaletteRow = function (e) {
    var srcElem = RIUTIL.getEventSource(e);
    var infoTag = RIUTIL.searchParentByPorpName(srcElem, "deleteContentIconId");
    RIUTIL.setDisplayById(document, infoTag.deleteContentIconId, true);
}

// Event Handler: mouser out the item row of content palette
RIUTIL.onMouseOutContentPaletteRow = function (e) {
    var srcElem = RIUTIL.getEventSource(e);
    var infoTag = RIUTIL.searchParentByPorpName(srcElem, "deleteContentIconId");
    RIUTIL.setDisplayById(document, infoTag.deleteContentIconId, false);
}

// Event Handler for change application id
RIUTIL.goChangeApplicationId = function () {
    var appId = RIUTIL.getAppIdTag().value.toUpperCase();
    RIUTIL.getAppIdTag().value = appId;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_APP_ID, RIUTIL.PARAM_APP_ID, appId);
}

// Event Handler for change form id
RIUTIL.goChangeFormSeletion = function () {
    var formSelection = RIUTIL.getFormSeletionTag();
    if (formSelection.selectedIndex == -1) {
        RIUTIL.updateViewForShowTitleBarOption(null);
        return;
    }
    var option = formSelection.options[formSelection.selectedIndex];
    var formId = option.value;
    RIUTIL.updateViewForShowTitleBarOption(option);
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_FORM_ID, RIUTIL.PARAM_FORM_ID, formId);
}

// Event Handler for change in ADF proxy form id
RIUTIL.goChangeADFProxyFormSelection = function () {
    var formSelection = RIUTIL.getADFProxyFormSelectionTag();
    if (formSelection.selectedIndex == -1)
        return;
    var formId = formSelection.options[formSelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_ADF_PROXY_FORM_ID, RIUTIL.PARAM_FORM_ID, formId);
}

// Event Handler for change version id
RIUTIL.goChangeVersionSeletion = function () {
    var versionSelection = RIUTIL.getVersionSeletionTag();
    if (versionSelection.selectedIndex == -1)
        return;
    var versionId = versionSelection.options[versionSelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_VERSION_ID, RIUTIL.PARAM_VERSION_ID, versionId);
}

// Event Handler for change in ADF proxy version id
RIUTIL.goChangeADFProxyVersionSelection = function () {
    var versionSelection = RIUTIL.getADFProxyVersionSelectionTag();
    if (versionSelection.selectedIndex == -1)
        return;
    var versionId = versionSelection.options[versionSelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_ADF_PROXY_VERSION_ID, RIUTIL.PARAM_VERSION_ID, versionId);
}

// Event Handler for change report id
RIUTIL.goChangeReportId = function () {
    var reportSelection = RIUTIL.getReportSelectionTag();
    var reportId = reportSelection.options[reportSelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_REPORT_ID, RIUTIL.PARAM_REPORT_ID, reportId);
}

// Event Handler for change query id
RIUTIL.goChangeQueryId = function () {
    var querySelection = RIUTIL.getQuerySelectionTag();
    var queryId = querySelection.options[querySelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_QUERY_ID, RIUTIL.PARAM_QUERY_ID, queryId);
}

// Event Handler for change E1 page id
RIUTIL.goChangeE1PageId = function () {
    var pageSelection = RIUTIL.getE1PageIdSelectionTag();
    var e1PageId = pageSelection.options[pageSelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_E1PAGE_ID, RIUTIL.PARAM_E1PAGE_ID, e1PageId);
}

// Event Handler for change ADF id
RIUTIL.goChangeADFTaskId = function () {
    var pageSelection = RIUTIL.getADFIdSelectionTag();
    var ADFId = pageSelection.options[pageSelection.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_ADF_ID, RIUTIL.PARAM_ADF_ID, ADFId);
}


// Event Handler for change auto find
RIUTIL.goChangeAutoFind = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    var autoFindTag = doc.getElementById("objAutoFind");
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_AUTO_FIND, RIUTIL.PARAM_AUTO_FIND, autoFindTag.checked);
}

// Event Handler for change auto find
RIUTIL.goChangeMaxGrid = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    var maxGridTag = doc.getElementById("objMaxGrid");
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_MAX_GRID, RIUTIL.PARAM_MAX_GRID, maxGridTag.checked);
}

// Event Handler for change show title bar option
RIUTIL.goChangeShowTitleBar = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    var showTitleBarTag = doc.getElementById("objShowTitleBar");
    RIUTIL.postAjaxRequest(RIUTIL.inspectedContainerId, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CHANGE_SHOW_TITLE_BAR, RIUTIL.PARAM_SHOW_TITLE_BAR, showTitleBarTag.checked);
}

// Event Handler: clean up the current opened event rules
RIUTIL.goClearEndUserRules = function () {
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_CLEAR_EVENT);
    RIUTIL.layoutManager.isDirty = true;
}

// Event Handler: delete the current opened event rules
RIUTIL.goDeleteEndUserEvent = function () {
    var eventListTag = document.getElementById(RIUTIL.ID_EVENT_LIST);
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_REMOVE_EVENT, RIUTIL.PARAM_EVENT_INDEX, eventListTag.selectedIndex);
    RIUTIL.layoutManager.isDirty = true;
}

// Event Handler: reloacte the insert and delete indicator based on the mouse position.
RIUTIL.goMouseMoveRootActionsContainer = function (e) {
    var mouse = RIUTIL.getCurrentPosition(e, false);
    var sidePanelOffset = RIUTIL.getSidePanelScrollOffset();
    mouse.x = mouse.x + sidePanelOffset.x;
    mouse.y = mouse.y + sidePanelOffset.y;

    // set the rule insert marker to the closest markers
    var retObj = RIUTIL.findClosestElemenent(RIUTIL.ruleInsertMarkers, mouse);
    if (retObj) {
        RIUTIL.relocateRuleInsertIndicator(sidePanelOffset, retObj.closestElem.ruleId, retObj.closestElem.wizardId, retObj.closestRegion);
    }

    // set the rule delete marker to the closest rule
    var retObj = RIUTIL.findClosestElemenent(RIUTIL.ruleMarkers, mouse);
    if (retObj) {
        RIUTIL.relocateRuleDeleteIndicator(sidePanelOffset, retObj.closestElem.ruleId, retObj.closestRegion, mouse);
    }
}

// Event Handler: mouse out of action container
RIUTIL.goMouseOutRootActionsContainer = function (e) {
    var rootActionsContainer = document.getElementById(RIUTIL.ID_EVENT_RULES_CONTAINER);
    var mouse = RIUTIL.getCurrentPosition(e, false);
    var region = RIUTIL.getRegion(rootActionsContainer);
    if (!RIUTIL.isInRegion(region, mouse.x, mouse.y))
        RIUTIL.cleanUpRuleIndicators();
}

// Event Handler: select menu item in the push item action template
RIUTIL.goMenuItemSelection = function (e) {
    var elem = RIUTIL.getEventSource(e);
    var menuId = elem.options[elem.selectedIndex].value;
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_MENU_ID, RIUTIL.PARAM_RULE_ID, elem.ruleId, RIUTIL.PARAM_REF_OBJ_ID, refObjId, RIUTIL.PARAM_MENU_ID, menuId);
}

// Event handler: select frame from the list
RIUTIL.goFrameSelection = function (e) {
    var elem = RIUTIL.getEventSource(e);
    RIUTIL.sentFrameSelection(elem);
}

// Event Handler: when event selection is changed.
RIUTIL.goChangeEventSelection = function () {
    // validate the current event before switch to a different event
    var error = RIUTIL.validateUserEventRules()
    if (error) {
        RIUTIL.renderError(error);
        var eventListTag = document.getElementById(RIUTIL.ID_EVENT_LIST);
        eventListTag.selectedIndex = RIUTIL.selectedEventIndex; // roll back to the original index
        return;
    }

    RIUTIL.changeEventSelection();
}

// Event Handler: execute the end user event at runtime
RIUTIL.goExecuteEvent = function (e) {
    JSSidePanelRender.reqClose();
    var elem = RIUTIL.getEventSource(e);
    var userEvent = RIUTIL.eventList[elem.eventIndex];
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_EXECUTE_EVENT, RIUTIL.PARAM_EVENT_INDEX, elem.eventIndex);
}

// Event Handler: add a rule template (given wizardId and tempalte Index) into the current event with given the location
RIUTIL.goInsertRuleToEvent = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "templateId");
    var eventListTag = document.getElementById(RIUTIL.ID_EVENT_LIST);
    var sourceEventIndex = eventListTag.selectedIndex;
    RIUTIL.wizardWin.onClose();
    RIUTIL.layoutManager.isDirty = true;
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_INSERT_TEMPLATE_TO_EVENT,
        RIUTIL.PARAM_RULE_ID, elem.ruleId, RIUTIL.PARAM_RULE_ACTION, elem.ruleAction, RIUTIL.PARAM_TEMPLATE_ID, elem.templateId, RIUTIL.PARAM_WIZARD_ID, elem.wizardId);
}

// Event Handler: delete a rule given ruleId from the current event
RIUTIL.goDeleteRuleFromEvent = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    RIUTIL.layoutManager.isDirty = true;
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_REMOVE_RULE_FROM_EVENT, RIUTIL.PARAM_RULE_ID, elem.ruleId);
}

// Event Handler: called from master
RIUTIL.goCaptureVisibleFieldBegin = function (e) {
    RIUTIL.wizardWin.onClose();
    RIUTIL.goCaptureGenericControlBegin(e, RIUTIL.VISIBLE_FIELD_CAPTURE);
}

// Event Handler: called from master 
RIUTIL.goCaptureEditableFieldBegin = function (e) {
    RIUTIL.goCaptureGenericControlBegin(e, RIUTIL.EDITABLE_FIELD_CAPTURE);
}

// Event Handler: called from master 
RIUTIL.goCaptureMenuItemBegin = function (e) {
    RIUTIL.goCaptureGenericControlBegin(e, RIUTIL.MENU_ITEM_CAPTURE);
}

// Private: generic event handler for given capture mode (visible field, editable field and menu item
RIUTIL.goCaptureGenericControlBegin = function (e, captureMode) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "ruleId");
    RIUTIL.inspectedRuleId = elem.ruleId;
    RIUTIL.inspectedBaseId = elem.baseId;
    RIUTIL.setCaptureMode(captureMode, true);
    return RIUTIL.stopEventBubbling(e);
}

// Event Handler: called either from master for satellite form form capture edit, visble field or menu item
RIUTIL.goCaptureGenericControlEnd = function (e) {
    var elem = RIUTIL.getEventSource(e);
    var clientId = elem.getAttribute(RIUTIL.ATT_CLIENTL_ID);
    var rootRIUTIL = RIUTIL.getRootRIUTIL();
    var ruleId = rootRIUTIL.inspectedRuleId;
    var baseId = rootRIUTIL.inspectedBaseId;
    var refContainerId = RIUTIL.getContainerIdFromIframe();
    var refObjectId = "";
    if (rootRIUTIL != RIUTIL) // Satellite Form
    {
        var refContainer = rootRIUTIL.getContainer(refContainerId);
        refObjectId = refContainer.frame.objId;
    }

    rootRIUTIL.setCaptureMode(RIUTIL.NO_CAPTURE, true);
    rootRIUTIL.layoutManager.isDirty = true;
    rootRIUTIL.postAjaxRequest(rootRIUTIL.layout.id, RIUTIL.getActiveProduct(), rootRIUTIL.AJAX_CMD_LINK_TO_FIELD, RIUTIL.PARAM_REF_OBJ_ID, refObjectId, RIUTIL.PARAM_RULE_ID, ruleId, RIUTIL.PARAM_CLIENTL_ID, clientId, RIUTIL.PARAM_BASE_ID, baseId);
}

// Event Handler:
RIUTIL.goCaptureVisibleButton = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    RIUTIL.layoutManager.isDirty = true;
    return RIUTIL.stopEventBubbling(e);
}

// Event Handler: select a rule template to replace the current rule template or wizard placeholder
RIUTIL.goChangeTemplate = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    if (elem.tagName == "INPUT" || elem.tagName == "SELECT" || elem.tagName == "IMG")
        return RIUTIL.stopEventBubbling(e);

    var elem = RIUTIL.searchParentByPorpName(elem, "wizardId");

    RIUTIL.renderEventWizard(elem, elem.ruleId, elem.wizardId, RIUTIL.RULE_ACTION_REPLACE);
    return RIUTIL.stopEventBubbling(e);
}

// Event Handler: for firefox we do not want click event on frame select bubble to the teamplate level to trigger wizard window for replace the template
RIUTIL.goPreventClickEventBubbling = function (e) {
    return RIUTIL.stopEventBubbling(e);
}


// Event Handler: beging to change user defined constant value
RIUTIL.goChangeRuleUserValueBegin = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "ruleId");

    RIUTIL.renderUserValueWhenEdit(elem.parentNode, elem.value, elem.ruleId);
    return RIUTIL.stopEventBubbling(e);
}

// Event Handler: end to change user defined constant value
RIUTIL.goChangeRuleUserValueEnd = function (e) {
    var elem = RIUTIL.getEventSource(e);
    RIUTIL.renderUserValueBeforeEdit(elem.parentNode, elem.value, elem.ruleId, false);
    RIUTIL.layoutManager.isDirty = true;
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_RULE_USER_VALUE, RIUTIL.PARAM_RULE_ID, elem.ruleId, RIUTIL.PARAM_USER_VALUE, elem.value);
}

// Event Handler: beging to chagne the operator in the rule
RIUTIL.goChangeRuleOperatorBegin = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "ruleId");
    RIUTIL.layoutManager.isDirty = true;
    RIUTIL.renderOperatorListWhenEdit(elem.parentNode, elem.ruleId, elem.itemList, elem.selectedIndex);
}

// Event Handler: end to chagne the operator in the rule
RIUTIL.goChangeRuleOperatorEnd = function (e) {
    var elem = RIUTIL.getEventSource(e);
    var selectedIndex = elem.selectedIndex;
    RIUTIL.renderOperatorListBeforeEdit(elem.parentNode, elem.ruleId, elem.itemList, selectedIndex, false);
    RIUTIL.layoutManager.isDirty = true;
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_RULE_OPERATOR, RIUTIL.PARAM_RULE_ID, elem.ruleId, RIUTIL.PARAM_OPERATOR, selectedIndex);
}

// Event Handler: mouse over a RI field in the event editor
RIUTIL.onMouseOverField = function (e) {
    RIUTIL.setFieldHighlighter(e, true);
}

// Event Handler: mouse out a RI field in the event editor
RIUTIL.onMouseOutField = function (e) {
    RIUTIL.setFieldHighlighter(e, false);
}

// Event Handler: mouse out a RI field in the event editor
RIUTIL.goAddNewEventBegin = function () {
    RIUTIL.goStopKeyCapture();
    // validate the current opened event rules before save
    var error = RIUTIL.validateUserEventRules()
    if (error) {
        RIUTIL.renderError(error);
        return;
    }

    var onClose = function (e) { JSSidePanelRender.hideUIBlock() };
    var labels = new Array("user event 1", RIUTIL.CONSTANTS.RI_ENTER_EVENT_NAME);
    JSSidePanelRender.renderPromptForName(top.document, null, 2, labels, 30, RIUTIL.goAddNewEventEnd, null, onClose);
}

// Event Handler: end process of save layout action
RIUTIL.goAddNewEventEnd = function () {
    var targetEventName = document.getElementById(JSSidePanelRender.ID_NAME_ENTERED).value;
    if (!RIUTIL.isValidName(targetEventName)) {
        return;
    }

    if (RIUTIL.existEventName(targetEventName)) {
        alert(RIUTIL.CONSTANTS.UDO_DUPLICATED_NAME);
        return;
    }

    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_CREATE_USER_EVENT, RIUTIL.PARAM_EVENT_NAME, targetEventName);
    RIUTIL.layoutManager.isDirty = true;
    return true;
}

// Event Handler: when event selection is changed.
RIUTIL.goLaunchWizard = function (e) {
    RIUTIL.goStopKeyCapture();
    var elem = RIUTIL.getEventSource(e);
    //alert('goLaunchWizard:' + elem.ruleId);
    RIUTIL.renderEventWizard(elem, elem.ruleId, elem.wizardId, elem.ruleAction);
    return RIUTIL.stopEventBubbling(e);
}

//--------------------------------------------
// Dom element access methods
//--------------------------------------------
// Public: check if Cafeone framework is applied
RIUTIL.isRIDisplayed = function () {
    return RIUTIL.getE1PaneDIV() != null;
}

// Private: get element
RIUTIL.getProdSelectionTag = function (containerId) {
    var doc = RIUTIL.getRIPaneDoc(containerId);
    var prodSelection = doc.getElementById("prodSelection");
    return prodSelection;
}

// Private: get the dom id of part preview tag
RIUTIL.getPartReviewDomID = function (partId) {
    return "partPreview_" + partId;
}

// Private: get the dom id of part preview tag
RIUTIL.getPartReviewComponentDomID = function (partId) {
    return "partPreviewComponents_" + partId;
}

// Private: get the dom id of part preview base tag
RIUTIL.getPartReviewBaseDomID = function (partId) {
    return "partPreviewBase_" + partId;
}

// Private: get the dom id of part row
RIUTIL.getPartRowDomID = function (partId) {
    return "partRow_" + partId;
}

// Private: get the dom id of    part base value div
RIUTIL.getPartBaseValueDivDomID = function (partId) {
    return "partBaseValueDiv_" + partId
}

// Private: get the dom id of part component table
RIUTIL.getPartComponentsTableDomID = function (partId) {
    return "partComponentsTable_" + partId
}

// Private: get the dom id of part component table
RIUTIL.getPartComponentsRowDomID = function (partId) {
    return "partComponentsRow_" + partId;
}

// Private: get the dom id of part help 
RIUTIL.getPartHelpDomID = function (partId) {
    return "partHelpDiv_" + partId;
}

// Private: get the dom id of part option
RIUTIL.getPartOptionDomID = function (partId) {
    return "partOption_" + partId;
}

// Private: get the dom id of part append div
RIUTIL.getPartAppendDivDomID = function (partId) {
    return "partAppendDiv_" + partId;
}

// Private: get the iframe object which hold the runtime or design layout framework
RIUTIL.getFrameworkFrame = function () {
    if (RIUTIL.isCompositePageAsApp) {
        var e1ExternalAppIframe = top.document.getElementById('e1ExternalAppIframe');
        if (e1ExternalAppIframe != null) {
            return e1ExternalAppIframe;
        }
    }
    var e1menuAppIframeAbove = top.document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME);
    var tabBar = e1menuAppIframeAbove.contentDocument.getElementById('tabBar');
    var tab;
    var frameId;
    if (RIUTIL.layoutManager.designMode || (RIUTIL.lcmEdit !== undefined)) // use only wcFrameNew for design
    {
        return e1menuAppIframeAbove.contentDocument.getElementById('wcFrameNew');
    }
    else // use existing frame for runtime
    {
        if (tabBar) // tab bar only display when there are more than 1 tab.
        {
            for (var i = 0; i < tabBar.children.length; i++) {
                tab = tabBar.children[i];
                if (tab.getAttribute('layoutId') == (RIUTIL.layoutManager.currentLayoutId + "|" + RIUTIL.layoutManager.currentLayoutUser)) {
                    frameId = tab.getAttribute('frameIndex');
                    break;
                }
            }
        }
        else // no tabs for single page display
        {
            return e1menuAppIframeAbove.contentDocument.getElementById('defaultFrame');
        }
    }
    if (e1menuAppIframeAbove.contentDocument.getElementById('wcFrame' + frameId) != null) {
        return e1menuAppIframeAbove.contentDocument.getElementById('wcFrame' + frameId);
    }
    else {
        return null;
    }
}

RIUTIL.getFrameworkWindow = function () {
    if (RIUTIL.isCompositePage) {
        var frame = RIUTIL.getFrameworkFrame();
        if (frame)
            return frame.contentWindow;
    }

    return window;
}

RIUTIL.getFrameworkDoc = function () {
    return RIUTIL.getFrameworkWindow().document;
}

// Private: get the container table given container model
RIUTIL.getContainerTag = function (container) {
    switch (container.containerType) {
        case RIUTIL.CONATINAER_E1:
            return RIUTIL.getE1PaneDIV();
        case RIUTIL.CONATINAER_RIAF:
        case RIUTIL.CONATINAER_ROOT:
        case RIUTIL.CONATINAER_FREESTYLE:
        case RIUTIL.CONATINAER_WL_COMPONENT:
        case RIUTIL.CONATINAER_LANDING_PAGE:
            return RIUTIL.getFrameworkDoc().getElementById(RIUTIL.ID_RI_LAYOUT_TABLE + container.id);
    }
}

// Private: return div tag of E1Pane
RIUTIL.getE1PaneDIV = function () {
    return document.getElementById(RIUTIL.ID_E1_PANE_DIV);
}

// Public: called by JSComppent to get the actual width of the E1 Pane
RIUTIL.getE1PaneWidth = function () {
    return RIUTIL.getE1PaneDIV().offsetWidth;
}

// Private: return iframe tag of E1Pane
RIUTIL.getE1PaneIFrame = function () {
    return top.document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME);
}

// Private: get MenuBarTR
RIUTIL.getRIMenuBarTR = function (containerId) {
    if (!containerId)
        containerId = "";
    return RIUTIL.getFrameworkDoc().getElementById(RIUTIL.ID_RI_MENU_BAR + containerId);
}

// Private: return iframe tag of E1Pane
RIUTIL.getRIPaneIFRAME = function (containerId) {
    if (!containerId)
        containerId = "";
    return RIUTIL.getFrameworkDoc().getElementById(RIUTIL.ID_RI_PANE_FRAME + containerId);
}

// Private: return RIPaneDIV
RIUTIL.getRIPaneDiv = function (containerId) {
    if (!containerId)
        containerId = "";
    return RIUTIL.getFrameworkDoc().getElementById(RIUTIL.ID_RI_PANE_DIV + containerId);
}

// Private: return document object of RI pane
RIUTIL.getRIPaneDoc = function (containerId) {
    try {
        var contentWindow = RIUTIL.getContainerWinObject(containerId);
        if (contentWindow)
            return contentWindow.document;
    }
    catch (e)// access deny exception occur when try to access into a cross-doman iframe (generic URL)
    {
    }
    return null
}

// Private: return pre-existent form element object of e1 pane
RIUTIL.getE1PaneForm = function () {
    return document.getElementById(RIUTIL.ID_E1_PANE_FORM);
}

// Private: return pre-existent form element object of e1 pane
RIUTIL.getWebMenuBar = function () {
    return document.getElementById("WebMenuBarFrame");
}

// Private: get the id of the delete icon for a component
RIUTIL.getIconIdForDeleteComponent = function (partId, componentIndex) {
    return "deleteComponent" + partId + "_" + componentIndex;
}

RIUTIL.getAppIdTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objApplicationID");
}

RIUTIL.getFormSeletionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objFormSeletion");
}

RIUTIL.getVersionSeletionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objVersionSeletion");
}

RIUTIL.getPfSeletionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objPfSeletion");
}

RIUTIL.getReportSelectionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objReportSeletion");
}

RIUTIL.getQuerySelectionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objQuerySelection");
}

RIUTIL.getE1PageIdSelectionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objE1PageIdSelection");
}

RIUTIL.getADFIdSelectionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objADFTaskIdSelection");
}

RIUTIL.getADFProxyAppIdTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objADFProxyAppId");
}

RIUTIL.getADFProxyFormSelectionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objADFProxyFormSelection");
}

RIUTIL.getADFProxyVersionSelectionTag = function () {
    var doc = RIUTIL.getRIPaneDoc(RIUTIL.inspectedContainerId);
    return doc.getElementById("objADFProxyVersionSelection");
}
//---------------------------------------
// Utility meothods
//---------------------------------------

// Private: get the offset of E1 App Iframe to the top window
RIUTIL.getE1AppFrameOffset = function () {
    var e1Frame = top.document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME);
    var offsetX = getAbsoluteLeftPos(e1Frame);
    var offsetY = getAbsoluteTopPos(e1Frame);
    return { x: offsetX, y: offsetY };
}

// Private: stop event bubbing
RIUTIL.stopEventBubbling = function (e) {
    if (e == null) {
        window.event.cancelBubble = true;
        window.event.returnValue = false;
    }
    else {
        if (e.stopPropagation)
            e.stopPropagation();
        if (e.preventDefault)
            e.preventDefault();

        e.cancelBubble = true;
        e.returnValue = false;
    }
    return false;
}

// Private: set display style given element id
RIUTIL.setDisplayById = function (doc, id, display) {
    if (!doc)
        doc = document;
    var elem = doc.getElementById(id);
    if (elem) {
        var type = elem.tagName == "DIV" ? "block" : "inline";
        elem.style.display = (display) ? type : "none";
    }
}

// Private: set enablement given element id
RIUTIL.setEnableById = function (doc, id, enable) {
    if (!doc)
        doc = document;
    var elem = doc.getElementById(id);
    if (elem) {
        RIUTIL.setElemEnablement(elem, enable);
    }
}

// Private: set enablement for dom element
RIUTIL.setElemEnablement = function (element, enable) {
    if (enable) {
        element.removeAttribute('disabled');
    }
    else {
        element.setAttribute('disabled', true);
    }
    if (element.id == RIUTIL.ID_LAYOUT_SELECTION_FORM) {
        element.className = enable ? "RCUXComboBox" : "RCUXComboBoxDisabled";
    }
    else {
        element.className = enable ? "" : "tfdisabled";
    }
}

// Private: set enablement for dom element
RIUTIL.setElemDisplay = function (doc, elementName, display) {
    if (!doc)
        return;
    var element = doc.getElementById(elementName);
    if (element)
        element.style.display = display ? "inline" : "none";
}

// Private: set enablement for dom element
RIUTIL.setElemVisible = function (elementName, visible) {
    var element = document.getElementById(elementName);
    if (element)
        element.style.visibility = visible ? "visible" : "hidden";
}

// Private: generic uitility method to remove a element from its parent give the id of the child
RIUTIL.removeElemById = function (id, doc) {
    if (!doc)
        doc = document;
    var elem = doc.getElementById(id);
    if (elem)
        elem.parentNode.removeChild(elem);
}

// Private: generic utility method to clean up all chidren with on certain number of child left
RIUTIL.removeAllChildren = function (parentNode, left) {
    if (!parentNode)
        return;
    var i;
    if (!left)
        left = 0;
    while ((i = parentNode.childNodes.length - 1) >= left) {
        var child = parentNode.childNodes[i];
        parentNode.removeChild(child);
    }
}

// Private: add event for all browsers 
RIUTIL.addEvent = function (elem, eventType, eventFunction) {
    if (elem.addEventListener) //can use the "addEventListener" syntax
    {
        //Firefox
        this.addEvent = this.addEventWithListener;
    }
    else {
        //IE
        this.addEvent = this.addEventWithAttach;
    }

    this.addEvent(elem, eventType, eventFunction);
}

// Private: This method is the addEvent redirection for IE only
RIUTIL.addEventWithAttach = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.attachEvent('on' + eventType, eventFunction);
}

// Private: This method is the addEvent redirection for Firefox
RIUTIL.addEventWithListener = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.addEventListener(eventType, eventFunction, false); //false = capture on bubbling
}

// Private: This method is the removeEvent redirection for all browsers
RIUTIL.removeEvent = function (elem, eventType, eventFunction) {
    if (elem.removeEventListener) //can use the "addEventListener" syntax
    {
        //Firefox
        this.removeEvent = this.removeEventWithListener;
    }
    else {
        //IE
        this.removeEvent = this.removeEventWithDetach;
    }

    this.removeEvent(elem, eventType, eventFunction);
}

// Private: This method is the removeEvent redirection for IE
RIUTIL.removeEventWithDetach = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.detachEvent('on' + eventType, eventFunction);
}

// Private: This method is the removeEvent redirection for Firefox
RIUTIL.removeEventWithListener = function (elem, eventType, eventFunction) {
    if (!elem || !eventType || !eventFunction) {
        return;
    }

    elem.removeEventListener(eventType, eventFunction, false); //false = capture on bubbling
}

// Private: This method is cross-browser to get event source
RIUTIL.getEventSource = function (event) {
    if (window.event || top.event) {
        //IE has simple method to access originating element from event
        this.getEventSource = function () {
            var myWindow = RIUTIL.isCompositePage ? RIUTIL.getFrameworkWindow() : window;
            return myWindow.event ? myWindow.event.srcElement : top.event.srcElement;
        };
    }
    else {
        //Firefox needs a slightly more complex technique
        this.getEventSource = this.getEventSourceW3C;
    }

    return this.getEventSource(event);
}

// Private: This method is the getEventSource redirection for non-IE browsers
RIUTIL.getEventSourceW3C = function (event) {
    var evtSource = event.currentTarget;

    if (evtSource && evtSource.nodeType == 3) {
        return evtSource.parentNode;
    }
    else {
        return evtSource;
    }
}

// Private: get the mouse position 
RIUTIL.getMousePosition = function (e) {
    e = e ? e : window.event;
    var position =
    {
        'x': e.clientX,
        'y': e.clientY
    }
    return position;
}

// Private: get the position of the offset scrolling position of the current page
RIUTIL.getScrollPosition = function () {
    var document = RIUTIL.getFrameworkDoc();
    var x = 0;
    var y = 0;

    if (window && window.pageYOffset !== undefined) {
        x = window.pageXOffset;
        y = window.pageYOffset;
    }
    // IE8 workarounds in "else ifs"
    else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        x = document.documentElement.scrollLeft;
        y = document.documentElement.scrollTop;
    }
    else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        x = document.body.scrollLeft;
        y = document.body.scrollTop;
    }

    var position =
    {
        'x': x,
        'y': y
    }

    return position;
}

// Private: disable dom selection before drag and drop
RIUTIL.disableDOMHighlighting = function (element) {
    if (RIUTIL.isIE) {
        RIUTIL._onselectstart = element.onselectstart;
        element.onselectstart = function () { return false }
    }
    else if (RIUTIL.isFirefox) {
        RIUTIL._MozUserSelect = element.style.MozUserSelect;
        element.style.MozUserSelect = "none";
    }
    else (RIUTIL.isWebkit)
    {
        RIUTIL._webkitUserSelect = element.style.webkitUserSelect;
        element.style.webkitUserSelect = "none";
    }
}

// Private: recover dom selection after drag and drop
RIUTIL.enableDOMHighlighting = function (element) {
    if (RIUTIL.isIE) {
        element.onselectstart = RIUTIL._onselectstart
    }
    else if (RIUTIL.isFirefox) {
        element.style.MozUserSelect = RIUTIL._MozUserSelect;
    }
    else (RIUTIL.isWebkit)
    {
        element.style.webkitUserSelect = RIUTIL._webkitUserSelect;
    }

}

// Private: get the current postion of the mouse considering relate to the E1 App iframe.
RIUTIL.getCurrentPosition = function (e, discountE1AppFrame) {
    var srcElem = RIUTIL.getEventSource(e);
    var p = RIUTIL.getMousePosition(e);
    var s = RIUTIL.getScrollPosition();
    var x = p.x + s.x;
    var y = p.y + s.y;

    if (discountE1AppFrame) {
        var offset = RIUTIL.getE1AppFrameOffset();
        x -= offset.x;
        y -= offset.y;
    }

    return { x: x, y: y };
}

// Private: return true if the it is a valid name for layout name
RIUTIL.isValidName = function (name) {
    if (name == null || name.Trim().length == 0) {
        alert(RIUTIL.CONSTANTS.UDO_BAD_NAME);
        return false;
    }

    for (var i = 0; i < RIUTIL.BAD_CHAR_SET.length; i++) {
        if (name.indexOf(RIUTIL.BAD_CHAR_SET.charAt(i)) != -1) {
            alert(RIUTIL.CONSTANTS.UDO_BAD_NAME);
            return false;
        }
    }

    return true;
}

// Private: return true if the it is a valid name for layout name
RIUTIL.isValidProdCode = function (code) {
    if (code == null || code.Trim().length == 0) {
        alert(RIUTIL.CONSTANTS.CP_BAD_PRODCODE);
        return false;
    }

    for (var i = 0; i < RIUTIL.BAD_CHAR_SET.length; i++) {
        if (code.indexOf(RIUTIL.BAD_CHAR_SET.charAt(i)) != -1) {
            alert(RIUTIL.CONSTANTS.CP_BAD_PRODCODE);
            return false;
        }
    }

    return true;
}
// Private: attach event handler to toggle the css classname for mouse hover effect
RIUTIL.attachHoverEffect = function (elem, normalClassName, hoverClassName, bubbleup) {
    elem.className = elem.normalClassName = normalClassName;
    elem.hoverClassName = hoverClassName;
    elem.bubbleup = bubbleup;
    if (!isIE8OrEarlier) {
        elem.style.filter = "none";
    }
    RIUTIL.addEvent(elem, "mouseover", RIUTIL.onMouseOverClassUpdateEvent);
    RIUTIL.addEvent(elem, "mouseout", RIUTIL.onMouseOutClassUpdateEvent);
}

// Private: search up the dom tree to find certain tagName and return this node
RIUTIL.searchParentByPorpName = function (beginNode, porpName) {
    var currentNode = beginNode;
    while (currentNode && currentNode.tagName != "BODY" && !eval("currentNode." + porpName)) {
        currentNode = currentNode.parentNode;
    }
    return currentNode;
}

// Private: search up the dom tree to find certain tagName and return this node
RIUTIL.searchParentByTagName = function (beginNode, tagName) {
    var currentNode = beginNode;
    while (currentNode && currentNode.tagName != "BODY" && currentNode.tagName != tagName) {
        currentNode = currentNode.parentNode;
    }
    return currentNode;
}

// Private: create a table element with accessiblity
RIUTIL.createTable = function () {
    var table = document.createElement("table");
    if (RIUTIL.ACCESSIBILITY) {
        table.setAttribute("role", "presentation");
    }
    return table;
}

// Private: get the side panel scrolling offset
RIUTIL.getSidePanelScrollOffset = function () {
    var spFormDiv = document.getElementById("SPFormDiv");
    return { x: spFormDiv.scrollLeft, y: spFormDiv.scrollTop };
}

// Private: set the side panel scrolling offset
RIUTIL.setSidePanelScrollOffset = function (offset) {
    var spFormDiv = document.getElementById("SPFormDiv");
    spFormDiv.scrollTop = offset.y;
    spFormDiv.scrollLeft = offset.x;
}

// Private: get the dom id of the elment for a given rule id
RIUTIL.getDomIdByRuleId = function (rulleId) {
    return "RIRule" + rulleId;
}

// Private: get the dom id of the elment for a given rule id
RIUTIL.isRuleEditable = function () {
    return (JSSidePanelRender.mode == JSSidePanelRender.MODE_EDIT_END_USER_EVENT);
}

// Private: search container object given refObjId
RIUTIL.getContainerByRefObjId = function (container, refObjId) {
    if (RIUTIL.isMinimized(container) || RIUTIL.isFilteredOut(container)) // collpase the frame with minimized or filterout
    {
        return null;
    }

    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout

            for (var i = 0; i < container.subContainers.length; i++) {
                var subContainer = container.subContainers[i];
                var retContainer = RIUTIL.getContainerByRefObjId(subContainer, refObjId);
                if (retContainer)
                    return retContainer;
            }

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            if (container.frame && container.frame.objId == refObjId) {
                return container;
            }
            return null;
    }
}

// Private: search container object given refObjId, if passing in null refobjid, return the current window, return null if could not find it.
RIUTIL.getContainerWinByRefObjId = function (container, refObjId) {
    if (!refObjId)
        return window;

    var container = RIUTIL.getContainerByRefObjId(container, refObjId);
    if (!container)
        return null;

    return RIUTIL.getContainerWinObject(container.id);
}

// private: set field highlighter give event and boolean value 
RIUTIL.setFieldHighlighter = function (e, highlight) {
    var elem = RIUTIL.getEventSource(e);
    var elem = RIUTIL.searchParentByPorpName(elem, "clientId");
    var clientId = elem.clientId;
    var containerWin = RIUTIL.getContainerWinByRefObjId(RIUTIL.layout, elem.refObjId);
    if (containerWin)
        containerWin.RIUTIL.updateControlIndicator(clientId, "RISourceBG RIHighlightReferenceBG", highlight);
}

// Private: valid if the row index is valid
RIUTIL.isValidRowIndex = function (rowIndex) {
    return (rowIndex != null && rowIndex >= 0)
}
//---------------------------------------
// Comunication meothods
//---------------------------------------
// Private: page post with action parameter to the server for RI pane iframe
RIUTIL.postPageRequest = function (containerId, command) {
    try {
        RIUTIL.goStopKeyCapture();
        RIUTIL.resetParameterCapture(containerId);
    }
    catch (e) {
    }

    var e1UrlCreator = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_SERVICE);
    if (window.JDEDTAFactory) {
        var stackId = JDEDTAFactory.getInstance(RIUTIL.namespace).stackId; // not poral support so far
        e1UrlCreator.setParameter(RIUTIL.PARAM_STACK_ID, stackId);
    }

    e1UrlCreator.setURI(RIUTIL.FRONT_SERVLET);
    e1UrlCreator.setParameter(RIUTIL.PARAM_ACTION, RIUTIL.ACTION_PAGE);
    e1UrlCreator.setParameter(RIUTIL.PARAM_COMMAND, command);
    e1UrlCreator.setParameter(RIUTIL.PARAM_CONTAINER_ID, containerId);
    e1UrlCreator.setParameter(RIUTIL.PARAM_STACK_ID, RIUTIL.layout.stackId);
    var prodCode = RIUTIL.getActiveProduct();
    e1UrlCreator.setParameter(RIUTIL.PARAM_PRODUCT, prodCode);

    // add other dynamic request parameters into the url
    for (var i = 2; i < arguments.length; ) {
        e1UrlCreator.setParameter(arguments[i++], arguments[i++]);
    }

    var targetUrl = e1UrlCreator.toString();
    var container = RIUTIL.getContainer(containerId);
    var contentWindow = RIUTIL.getContainerWinObject(containerId);
    if (contentWindow)
        contentWindow.location.href = targetUrl;
}

// Private: ajax post with cmd parameter to the server for RI pane iframe
RIUTIL.postAjaxRequest = function (containerId, product, cmd) {
    RIUTIL.cleanUpRuleIndicators();

    var e1UrlCreator = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_SERVICE);
    var prodSysCode = RIUTIL.getProductSysCode();
    //Cafeone Layout save, saveas 
    if (RIUTIL.cmdMap[cmd] != null) {
        var layoutId = RIUTIL.layoutManager.currentLayoutId;
        var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
        e1UrlCreator.setURI(RIUTIL.CAFEONE_UDO_SERVICE);
        if (RIUTIL.isCompositePage) {
            e1UrlCreator.setParameter(RIUTIL.PARAM_OBJECT_TYPE, "COMPOSITE");
        }
        else {
            e1UrlCreator.setParameter(RIUTIL.PARAM_OBJECT_TYPE, "CAFE1");
        }
        e1UrlCreator.setParameter(RIUTIL.PARAM_COMMAND, RIUTIL.cmdMap[cmd]);
        e1UrlCreator.setParameter(RIUTIL.PARAM_APP_ID, RIUTIL.layoutManager.appId);
        e1UrlCreator.setParameter(RIUTIL.PARAM_FORM_ID, RIUTIL.layoutManager.formId);
        e1UrlCreator.setParameter(RIUTIL.PARAM_VERSION_ID, RIUTIL.layoutManager.versionId);
        e1UrlCreator.setParameter(RIUTIL.PARAM_Id, RIUTIL.layoutManager.currentLayoutId);
        // passing omwObjectName 
        if (activeLayout.omwObjectName != null) {
            e1UrlCreator.setParameter(RIUTIL.PARAM_OMWOBJECTNAME, activeLayout.omwObjectName);
        }
        e1UrlCreator.setParameter(RIUTIL.PARAM_HASTOKEN, activeLayout.hasToken);
        e1UrlCreator.setParameter(RIUTIL.PARAM_TOKENPROJECTNAME, activeLayout.tokenProjectName);
        e1UrlCreator.setParameter(RIUTIL.PARAM_LAYOUT_NAME, activeLayout.name);
    }
    else {
        e1UrlCreator.setURI(RIUTIL.FRONT_SERVLET);
        e1UrlCreator.setParameter(RIUTIL.PARAM_COMMAND, cmd);
    }
    e1UrlCreator.setParameter(RIUTIL.PARAM_RI_COMMAND, cmd);
    e1UrlCreator.setParameter(RIUTIL.PARAM_ACTION, RIUTIL.ACTION_AJAX);
    e1UrlCreator.setParameter(RIUTIL.PARAM_PRODUCT, prodSysCode);
    e1UrlCreator.setParameter(RIUTIL.PARAM_CONTAINER_ID, containerId);

    if (window.JDEDTAFactory) {
        var dtaInstance = JDEDTAFactory.getInstance(RIUTIL.namespace);
        if (dtaInstance)
            var stackId = dtaInstance.stackId; // not poral support so far

        e1UrlCreator.setParameter(RIUTIL.PARAM_STACK_ID, stackId);
    }
    else if (RIUTIL.isCompositePageAsApp) // for composite page as app, the statck id is stored as global var
    {
        var stackId = window.stackId; //alert(stackId);
        e1UrlCreator.setParameter(RIUTIL.PARAM_STACK_ID, stackId);
    }

    var data = new FormData();
    var formDataPoulated = false;
    // TODO: QZ: these extra parameter introduced from ULCM in the method newLayoutName, isCloseDesignBar going to messup the following code and need to be fixed.
    // add other dynamic request parameters into the url
    for (var i = 3; i < arguments.length; ) {
        var name = arguments[i++];
        var value = arguments[i++];
        if (name == RIUTIL.PARAM_METADATA) {
            data = encodeURIComponent(value);
            formDataPoulated = true;
        }
        else
            e1UrlCreator.setParameter(name, value);
    }

    if (RIUTIL.isIOS) // IPAD cache the ajax request and return the same cached response for the same request, so we need to make request unique for each time.
        e1UrlCreator.setParameter("timestamp", (new Date()).getTime());

    //TODO: the formdata cause falure in IE10 in sandbox server but not in JDEV server and we need to reduce chance of failure by only providing formdata when neccessary.
    RIUTIL.sendXMLReq("POST", containerId, e1UrlCreator.toString(), RIUTIL.handlerAjaxRequest, formDataPoulated ? data : null);
}

// Private: Ajax send request
RIUTIL.sendXMLReq = function (method, containerId, action, handler, data) {
    // code for IE 7, FireFox, Safari, etc.
    if (window.XMLHttpRequest) {
        var xmlReqObject = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        var xmlReqObject = new ActiveXObject("MSXML2.XMLHTTP");
    }

    if (xmlReqObject != null) {
        xmlReqObject.open(method, action, true);
        var handlerFunction = RIUTIL.getReadyStateHandler(xmlReqObject, handler, containerId);
        xmlReqObject.onreadystatechange = handlerFunction;
        if (!data)
            xmlReqObject.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        //Webkit browsers through Javascript error while setting connection header
        if (!RIUTIL.isWebkit) {
            xmlReqObject.setRequestHeader("Connection", "close");
        }
        try {
            //Ajax post with empty body cause problem in webcenter, as workaround send dummy param & value with Ajax body
            if (window.E1URLFactory && window.gContainerId == E1URLFactory.prototype.CON_ID_WSRP) {
                var paramValue = 'dummyval';
                xmlReqObject.send(encodeURI('bParam=' + paramValue));
            }
            else {
                xmlReqObject.send(data);
            }
        }
        catch (e) { } // SSO logout from web center may cause interaction of E1 failure
    }
}

// Private: generic callback handler for ajax
RIUTIL.getReadyStateHandler = function (req, responseHandler, containerId) {
    // Return an anonymous function that listens to the 
    // XMLHttpRequest instance
    return function () {
        // If the request's status is "complete"
        if (req.readyState == 4) {
            // Check that a successful server response was received
            switch (req.status) {
                case 200:
                    // Pass the XML payload of the response to the 
                    // handler function

                    if (responseHandler)
                        responseHandler(containerId, req.responseText);
                    break;

                case 122: // SSO logout already from web center but E1 is still opened
                    RIUTIL.timeout();
                    break;

                case 404: // possible XSS attack 
                    RIUTIL.timeout(RIUTIL.CONSTANTS.RI_EXIT_FOR_XSS)
                    break;

                // Safari intermittently encounters status 0 states. 
                // Bypass the alert error in the default below with a case for this 
                // state with no specific logic for it. 
                case 0:
                    break;

                default:
                    // An HTTP problem has occurred
                    alert("HTTP error: " + req.status);
            }
        }
    }
}

// Public: generic ajax request handler
RIUTIL.handlerAjaxRequest = function (containerId, retStr) {
    //try 
    {
        if (retStr) {
            //for composite page, save will result an error if prod code is invalid
            var errMsg = RIUTIL.retrieveErrorMessage(retStr);
            if (errMsg) {
                RIUTIL.renderMessage(true, errMsg, true);
                RIUTIL.setPageDesignBarEnable(true);
            }
            else {
                if (retStr.substring(0, 1) == "<")// when SSO logout or timeout, we may get the html login page back
                    RIUTIL.timeout();
                else if (retStr.indexOf("050WO") == 0) // UDOManagerService return this error code when session expired
                    RIUTIL.timeout();
                else
                    eval(retStr);
            }
        }
    }
    //catch (e)
    {
        //alert("handlerAjaxRequest failed:" + e.name + ":" + retStr + ":" + e.message);
    }
}

//---------------------------------------
// Satelitte method 
//---------------------------------------
// Public method init Satellite Form
RIUTIL.initEmbeddedE1Form = function () {
    RIUTIL.isEmbedded = true;
    var containerId = RIUTIL.getContainerIdFromIframe();
    if (isNaN(containerId))
        return;
    var stackId = JDEDTAFactory.getInstance(RIUTIL.namespace).stackId;
    var rootRIUTIL = RIUTIL.getRootRIUTIL();
    if (rootRIUTIL)
        rootRIUTIL.postAjaxRequest(containerId, RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_SET_SAT_STACK_ID, RIUTIL.PARAM_SAT_STACK_ID, stackId);
}

// Private method called from Satellite Form to get the container id when it is run inside of iframe (embedded E1 form)
RIUTIL.getContainerIdFromIframe = function () {
    if (!window.frameElement)
        return null;

    var frameId = window.frameElement.getAttribute("id");
    var containerId = parseInt(frameId.substring(RIUTIL.ID_RI_PANE_FRAME.length));
    return containerId;
}

// Public Server Event inside of Satelitte to synchronize it with Master
RIUTIL.synchToMasterForm = function () {
    // cleanup the old maflet
    RIUTIL.executeCleanup();

    // notify server to refresh the satellite frame
    var containerId = RIUTIL.getContainerIdFromIframe();
    if (top.RIUTIL.isCompositePage && !top.RIUTIL.isCompositePageAsApp) {
        parent.setTimeout("top.RIUTIL.synchronizeContainer(" + containerId + ")", 0);
    }
    else {
        parent.setTimeout("RIUTIL.synchronizeContainer(" + containerId + ")", 0);
    }
}

// Private: get dom element from the current window (in satellite or master form) given client Id
// return element info object, which may contain partial information instead of all of the informaiton
// return null if it is bad client id, which shall never happen
RIUTIL.getElementInfoByClientId = function (ruleId, clientId) {
    if (clientId.substring(0, 1) == "C") // form control
    {
        var elem = document.getElementById(clientId);
        return {
            elem: elem,
            isFormControl: true,
            frameName: RIUTIL.getFrameName()
        };
    }
    else if (clientId.substring(0, 2) == "GC") // grid column
    {
        // TODO: when we begin to support grid in the subform, we will have to search for subform by fullqulify id and then locate the grid.
        var pos = clientId.indexOf(".");
        var gridId = clientId.substring(2, pos);
        var colIndex = clientId.substring(pos + 1);
        var gridObj = JDEDTAFactory.getInstance(RIUTIL.namespace).resolveGrid(gridId);
        var rowIndex = RIUTIL.getReferenceGridRowIndex(ruleId, gridObj);
        if (rowIndex == null)
            cellElem = null;
        else {
            var cellElem = gridObj.getDataCell(rowIndex, colIndex);
            if (cellElem == null) // try one more time with focus move to the cell in case the target cell is outside of the vituralization area
            {
                gridObj.setFocusOnCell(rowIndex, colIndex);
                cellElem = gridObj.getDataCell(rowIndex, colIndex);
            }
            if (cellElem)
                cellElem = cellElem.getElementsByTagName("input")[0];
        }

        return {
            elem: cellElem,
            isGridCell: true,
            gridObj: gridObj,
            colIndex: colIndex,
            rowIndex: rowIndex,
            frameName: RIUTIL.getFrameName()
        }
    }
    else if (clientId.substring(0, 2) == "hc") // menu item
    {
        var elem = document.getElementById(clientId);
        return {
            elem: elem,
            isMenuItem: true,
            frameName: RIUTIL.getFrameName()
        };
    }
    alert("unsupported client id:" + clientId)
    return null;
}

// Private: get value from the current window (in satellite or master form) given client Id
RIUTIL.getValueByClientId = function (ruleId, clientId, rule) {
    if (clientId.substring(0, 1) == "C") {
        var elemInfo = RIUTIL.getElementInfoByClientId(ruleId, clientId);
        if (elemInfo == null || elemInfo.elem == null)
            return null;
        return elemInfo.elem.value;
    }
    else if (clientId.substring(0, 2) == "GC") {
        // TODO: when we begin to support grid in the subform, we will have to search for subform by fullqulify id and then locate the grid.
        var pos = clientId.indexOf(".");
        var gridId = clientId.substring(2, pos);
        var colIndex = clientId.substring(pos + 1);
        var gridObj = JDEDTAFactory.getInstance(RIUTIL.namespace).resolveGrid(gridId);
        var rowIndex = RIUTIL.getReferenceGridRowIndex(ruleId, gridObj);
        if (rowIndex == null)
            return null;
        if (gridObj.getColumnByColIndex(colIndex).isHidden()) {
            var rootRIUTIL = RIUTIL.getRootRIUTIL();
            rootRIUTIL.throwRuntimeError(ruleId, rootRIUTIL.CONSTANTS.RI_R_ERR_ACCESS_COL.format(rule.description, RIUTIL.getFrameName()));
            return null;
        }
        var cellValue = gridObj.getValueAt(rowIndex, colIndex);
        return cellValue;
    }
    else alert("unsupported client id:" + clientId)
}


// Private: get the offset of Master Form to E1 window(especially when the E1 form is on the right bottom area of the layout), (from satellite form or master form)
// Private: get dom element from the current window (in satellite or master form) given client Id
RIUTIL.getReferenceGridRowIndex = function (ruleId, gridObj) {
    if (RIUTIL.isValidRowIndex(gridObj.cafeOneCursorRow))
        return gridObj.cafeOneCursorRow;

    if (RIUTIL.isValidRowIndex(gridObj.refencedRowIndex)) {
        return gridObj.refencedRowIndex;
    }

    if (RIUTIL.isValidRowIndex(gridObj.lastSelectedRowIndex))
        return gridObj.lastSelectedRowIndex;

    for (var rowIndex = 0; rowIndex < gridObj.gridData.length; rowIndex++) {
        if (gridObj.isRowSelected(rowIndex))
            return rowIndex;
    }

    var rootRIUTIL = RIUTIL.getRootRIUTIL();
    var message = rootRIUTIL.CONSTANTS.RI_R_ERR_ACCESS_ROW.format(RIUTIL.getFrameName());
    rootRIUTIL.throwRuntimeError(ruleId, message);
    return null;
}

// Private: called from Satellite Form to get its own frame name
RIUTIL.getFrameName = function () {
    if (RIUTIL.getRootRIUTIL() == RIUTIL)
        return RIUTIL.getMasterFrameTitle();
    else
        return RIUTIL.getRootRIUTIL().getContainer(RIUTIL.getContainerIdFromIframe()).frame.description;
}


// Private: get the offset of Master Form to E1 window(especially when the E1 form is on the right bottom area of the layout), (from satellite form or master form)
// so that position of form below grid can be caculated correctly in grid size manager
// The Satellite form does not have offset againest their parent iframe, so always return 0 offset for them.
RIUTIL.getFrameOffset = function () {
    if (!RIUTIL.isEmbedded) // only care the E1 Master Form in Layout
    {
        var rootRIUTIL = RIUTIL.getRootRIUTIL();
        if (rootRIUTIL && rootRIUTIL.layout) {
            var e1PaneDiv = rootRIUTIL.getE1PaneDIV();
            if (e1PaneDiv) {
                var offsetX = getAbsoluteLeftPos(e1PaneDiv);
                var offsetY = getAbsoluteTopPos(e1PaneDiv);
                return { x: offsetX, y: offsetY };
            }
        }
    }

    return { x: 0, y: 0 };
}

// Private: get root RITUIL (from satellite form or master form)
// (the child RITUIL does not access to translation text, so for some time, we have to go back to the root)
RIUTIL.getRootRIUTIL = function () {
    var e1menuAppIframeAbove = top.document.getElementById(RIUTIL.ID_E1MENU_APP_FRAME);
    if (!e1menuAppIframeAbove)
        return null;

    var e1Win = e1menuAppIframeAbove.contentWindow;
    if (!e1Win.RIUTIL) // in E1 Page, the rootRIUTIL is not e1menuappiframe any more but one of it's child frame.
    {
        for (var i = 0; i < e1Win.frames.length; i++) {
            var frame = e1Win.frames[i];
            if (frame == parent || frame == window) {
                return frame.RIUTIL; ;
            }
        }
        return null; // error condition
    }
    return e1Win.RIUTIL;
}


// Private: render Add icon for all capture mode (from satellite form or master form)
RIUTIL.renderAddIcon = function (referenceTag, iconId, clientId, location) {
    var icon = document.createElement("img");
    icon.title = RIUTIL.getRootRIUTIL().CONSTANTS.RI_ADD_COMPONENT;
    icon.id = iconId;
    icon.setAttribute(RIUTIL.ATT_CLIENTL_ID, clientId);
    icon.style.zIndex = referenceTag.style.zIndex + 1;

    switch (location) {
        case 'grid':
            icon.style.position = "absolute";
            icon.style.right = 0 + "px";
            icon.style.top = 0 + "px";
            break;

        case 'form':
            icon.style.left = RIUTIL.ICON_OFFSET_X + "px";
            icon.style.top = RIUTIL.ICON_OFFSET_Y + "px";
            icon.style.position = "relative";
            icon.setAttribute("valign", "center");
            break;

        case 'menu':
            icon.style.left = getAbsoluteLeftPos(referenceTag) + 10 + "px";
            icon.style.top = getAbsoluteTopPos(referenceTag) + 10 + "px";
            icon.style.position = "absolute";
            icon.setAttribute("valign", "center");
            break;
    }

    switch (RIUTIL.captureMode) {
        case RIUTIL.PARAM_CAPTURE:
            JSSidePanelRender.setMotionImage(icon, top['E1RES_img_RI_add_component_gif'], top['E1RES_img_RI_add_componentmo_gif'], RIUTIL.goCaptureParameter);
            break;

        case RIUTIL.KEY_CAPTURE:
            JSSidePanelRender.setMotionImage(icon, top['E1RES_share_images_alta_func_add_small_16_ena_png'], top['E1RES_share_images_alta_func_add_small_16_hov_png'], RIUTIL.goCaptureKey);
            break;

        case RIUTIL.VISIBLE_FIELD_CAPTURE:
        case RIUTIL.EDITABLE_FIELD_CAPTURE:
        case RIUTIL.MENU_ITEM_CAPTURE:
            JSSidePanelRender.setMotionImage(icon, top['E1RES_img_RI_add_component_gif'], top['E1RES_img_RI_add_componentmo_gif'], RIUTIL.goCaptureGenericControlEnd);
            break;
    }

    AQ.insertAfter(icon, referenceTag);
}


//---------------------------------------
// Call back meothods as side panel
//---------------------------------------

// Public Interface to hock up Size Panel. It supply the tabs model to Side Panel for rendering
RIUTIL.getTabsArray = function () {
    var tabsArray = new Array();
    tabsArray.add({ isActive: true,
        title: RIUTIL.CONSTANTS.RI_END_USER_EVENT,
        eventHandler: " "
    });

    return tabsArray;
}

// Public Interface for feature: callback from side panel to get the array of all menu item on the menu bar for this feature
RIUTIL.getMenusArray = function () {
    var menusArray = new Array();
    var isNew = true;

    menusArray.add({ outImg: top['E1RES_img_RI_addevent_gif'], overImg: top['E1RES_img_RI_addeventmo_gif'], title: RIUTIL.CONSTANTS.RI_ADD_NEW_EVENT, onclick: 'RIUTIL.goAddNewEventBegin()', id: RIUTIL.ID_NEW_EVENT_ICON });
    menusArray.add({ outImg: top['E1RES_share_images_ulcm_clearcolumns_ena_png'], overImg: top['E1RES_share_images_ulcm_clearcolumns_hov_png'], title: RIUTIL.CONSTANTS.RI_CLEAR_RULES, onclick: 'RIUTIL.goClearEndUserRules()', id: RIUTIL.ID_CLEAR_EVENT_ICON });
    menusArray.add({ outImg: top['E1RES_share_images_ulcm_delete_ena_png'], overImg: top['E1RES_share_images_ulcm_delete_hov_png'], title: RIUTIL.CONSTANTS.RI_DELETE_EVENT, onclick: 'RIUTIL.goDeleteEndUserEvent()', id: RIUTIL.ID_DELETE_EVENT_ICON });
    return menusArray;
}

// Public Interface for feature: callback from JSSidePanel to return HTML content for this feature
RIUTIL.getHTMLForTabBody = function (sb) {
    var alignStr = RIUTIL.isRTL ? "right" : "left";
    sb.append("<table class='SPLayoutTable' id='").append(RIUTIL.ID_CONTENT_TABLE).append("'>\
                <tr>\
                  <td style='width:20%'>\
                    <div >\
                      <nobr>&nbsp;").append(RIUTIL.CONSTANTS.RI_EVENT_LIST).append("</nobr>\
                    </div>\
                  </td>\
                  <td align='").append(RIUTIL.isRTL ? "center" : "left").append("' style='width:80%'> \
                    <select id=").append(RIUTIL.ID_EVENT_LIST).append(" onchange='RIUTIL.goChangeEventSelection()' > </select>");
    if (RIUTIL.ACCESSIBILITY) {
        sb.append("<label class='accessibility' for='").append(RIUTIL.ID_EVENT_LIST).append("'>").append(RIUTIL.ID_EVENT_LIST).append("</label>");
    }
    sb.append("     </td>\
                </tr>\
                <tr>\
                    <td colSpan=2>\
                    <fieldset class='").append("GroupBox ").append(RIUTIL.isRTL ? "SPFloat_right" : "").append("' align='").append(alignStr).append("'>\
                      <legend>").append(RIUTIL.CONSTANTS.RI_END_USER_RULES).append("</legend>\
                        <div class='RIEventRulesRootContainer' id='").append(RIUTIL.ID_EVENT_RULES_CONTAINER).append("'></div>\
                    </fieldset>\
                    </td>\
                </tr>\
              </table>"
            );
}

// Public Interface for feature: callback from JSSidePanel to return HTML content for this feature
RIUTIL.afterModeChange = function () {
    switch (JSSidePanelRender.getMode()) {
        case JSSidePanelRender.MODE_EDIT_END_USER_EVENT: // open user event editor
            RIUTIL.loadEventList();
            break;

        case JSSidePanelRender.MODE_VIEW_END_USER_EVENT: // open user event editor
            RIUTIL.setElemVisible(RIUTIL.ID_NEW_EVENT_ICON, false);
            RIUTIL.setElemVisible(RIUTIL.ID_VALIDATE_EVENT_ICON, false);
            RIUTIL.setElemVisible(RIUTIL.ID_CLEAR_EVENT_ICON, false);
            RIUTIL.setElemVisible(RIUTIL.ID_DELETE_EVENT_ICON, false);
            var eventList = document.getElementById(RIUTIL.ID_EVENT_LIST);
            eventList.options[0] = new Option(RIUTIL.currentEvent.id, 0, false, false);
            eventList.disabled = true;
            break;

        default:   // close user event editor
            RIUTIL.goStopKeyCapture();
            RIUTIL.cleanUpRuleIndicators();
    }
}

// Public Interface for feature: callback from JSSidePanel before close the side panel
RIUTIL.onClose = function () {
    if (RIUTIL.isRuleEditable()) {
        var error = RIUTIL.validateUserEventRules()
        if (error) // only display events when there is an validation error
        {
            RIUTIL.renderError(error);
        }
        else {
            RIUTIL.currentEvent = null;
            JSSidePanelRender.close();
        }
    }
    else {
        if (RIUTIL.iyfeWin) {
            RIUTIL.iyfeWin.destory();
            RIUTIL.iyfeWin = null;
        }
        JSSidePanelRender.close();
    }
}

// Public: called by grid in the satellite from when grid row is selected
RIUTIL.notifyGridRowSelectionChange = function (containerId) {
    var container = RIUTIL.getContainer(containerId);
    if (!container.gridRowSelectionEvents)
        return;

    for (var i = 0; i < container.gridRowSelectionEvents.length; i++) {
        var eventIndex = container.gridRowSelectionEvents[i];
        RIUTIL.executeUserEvent(eventIndex);
    }
}


//---------------------------------------
// Event Rule Render Execution and Validation
//---------------------------------------
// Private: request render the event list from the server after all satelliter form are competely loaded, otherwise wait a while and check later
RIUTIL.loadEventList = function () {
    if (!RIUTIL.areAllSatelliteFormsCompletelyLoaded()) {
        setTimeout("RIUTIL.loadEventList()", 1000);
        return;
    }

    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LOAD_EVENT_LIST);
}

// Private: return ture if all SatelliteForm are completely loadded
RIUTIL.areAllSatelliteFormsCompletelyLoaded = function () {
    var containerList = new Array();
    RIUTIL.getE1SatelliteContainerList(RIUTIL.layout, containerList, true);
    for (var i = 0; i < containerList.length; i++) {
        var container = containerList[i];
        if (container.frame.subType) // skip ADF and E1PAGE subtype EXAPP
            continue;
        var win = RIUTIL.getContainerWinObject(container.id);
        if (!win || !win.JDEDTAFactory)
            return false;
    }
    return true;
}

// Public: invoked by handleOnBrowserClose() or layout change
RIUTIL.cleanupAllSatelliteForms = function () {
    if (!RIUTIL.layout)
        return;

    var containerList = new Array();
    RIUTIL.getE1SatelliteContainerList(RIUTIL.layout, containerList, true);
    for (var i = 0; i < containerList.length; i++) {
        var container = containerList[i];
        if (container.frame.subType) // skip ADF and E1PAGE subtype EXAPP
            continue;

        var win = RIUTIL.getContainerWinObject(container.id); //TODO: it return null in composite page 
        if (win && win.JDEDTAFactory) {
            RIUTIL.cleanupContainer(container.id);
        }
    }
    return true;
}

// Private: request render the wizard list from the server
RIUTIL.loadWizardList = function () {
    RIUTIL.postAjaxRequest(0, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LOAD_WIZARD_LIST);
}

// Public: invoked by Server to render the user event selection tag
RIUTIL.updateEventList = function (json) {
    var obj = eval(json);
    RIUTIL.eventList = obj.eventList;
    RIUTIL.selectedEventIndex = obj.selectedEventIndex;
    RIUTIL.setupEventReference();
}

// Private: setup event reference in containers and form titile area
RIUTIL.setupEventReference = function () {
    var eventList = RIUTIL.eventList;
    var selectedEventIndex = RIUTIL.selectedEventIndex;

    // cleanup user trigger event
    var formTitleSpan = document.getElementById('jdeFormTitle0');
    if (!formTitleSpan)
        return;

    var parentTag = formTitleSpan.parentNode;
    RIUTIL.removeAllChildren(parentTag, 1);

    if (RIUTIL.layoutManager.designMode) // design mode
    {
        var eventListTag = document.getElementById(RIUTIL.ID_EVENT_LIST);
        if (eventListTag == null)
            return;

        RIUTIL.removeAllChildren(eventListTag);
        RIUTIL.currentEvent = null;

        for (var i = 0; i < eventList.length; i++) {
            var userEvent = eventList[i];
            var eventName = userEvent.id;
            eventListTag.options[i] = new Option(eventName, i, false, false);
            if (i == selectedEventIndex) {
                eventListTag.options[i].selected = true;
                RIUTIL.currentEvent = userEvent;
            }
        }
        RIUTIL.changeEventSelection();
    }
    else // runtime mode
    {
        parentTag.appendChild(document.createTextNode("  "));
        if (eventList) {
            for (var i = 0; i < eventList.length; i++) {
                var userEvent = eventList[i];
                RIUTIL.setupEndUserTriggers(userEvent, i, parentTag);
                RIUTIL.setupFrameOpenTriggers(userEvent, i);
                RIUTIL.setupGridRowSelctionTriggers(userEvent, i);
            }
        }
    }
}

// Private: exectue all open event for all E1 Satelliter Forms
RIUTIL.executeAllOpenEvent = function () {
    var containerList = new Array();
    RIUTIL.getE1SatelliteContainerList(RIUTIL.layout, containerList, true);
    for (var i = 0; i < containerList.length; i++) {
        var container = containerList[i];
        RIUTIL.executeOpenEventInContainer(container.id);
    }
}

// Private: exectue all open event for all E1 Satelliter Forms
RIUTIL.executeOpenEventInContainer = function (containerId) {
    if (!RIUTIL.areAllSatelliteFormsCompletelyLoaded()) {
        setTimeout("RIUTIL.executeOpenEventInContainer(" + containerId + ")", 1000);
        return;
    }

    var container = RIUTIL.getContainer(containerId);
    if (container.openEvents) {
        for (var j = 0; j < container.openEvents.length; j++) {
            var eventIndex = container.openEvents[j];
            RIUTIL.executeUserEvent(eventIndex);
        }
    }
}

// Private: set up end user triggers for runtime
RIUTIL.setupEndUserTriggers = function (userEvent, eventIndex, parentTag) {
    var triggers = RIUTIL.getTriggersByType(userEvent, RIUTIL.TID_USER_TRIGGER);
    if (triggers.length > 0) {
        var eventName = userEvent.id;
        var button = document.createElement("input");
        button.type = 'button';
        button.value = eventName;
        button.eventIndex = eventIndex;
        button.normalClassName = button.className = "button RIRoundBorder";
        button.hoverClassName = "button RCUXbuttonMouseOver RIRoundBorder";

        button.onclick = RIUTIL.goExecuteEvent;
        button.onmouseover = RIUTIL.onMouseOverActionEvent;
        button.onmouseout = RIUTIL.onMouseOutActionEvent;
        parentTag.appendChild(button);
    }
}

// Private: set up frame opening triggers for runtime
RIUTIL.setupFrameOpenTriggers = function (userEvent, eventIndex) {
    var triggers = RIUTIL.getTriggersByType(userEvent, RIUTIL.TID_FRAME_OPEN);
    for (var i = 0; i < triggers.length; i++) {
        var trigger = triggers[i];
        var refObjId = trigger.parts[0].refObjId;
        var container = RIUTIL.getContainerByRefObjId(RIUTIL.layout, refObjId);
        if (container) {
            if (!container.openEvents)
                container.openEvents = new Array();

            container.openEvents.push(eventIndex);
        }
    }
}

// Private: set up grid row selection change triggers for runtime
RIUTIL.setupGridRowSelctionTriggers = function (userEvent, eventIndex) {
    var triggers = RIUTIL.getTriggersByType(userEvent, RIUTIL.TID_ROW_SELECTION);
    for (var i = 0; i < triggers.length; i++) {
        var trigger = triggers[i];
        var refObjId = trigger.parts[0].refObjId;
        var container = RIUTIL.getContainerByRefObjId(RIUTIL.layout, refObjId);
        if (container) {
            if (!container.gridRowSelectionEvents)
                container.gridRowSelectionEvents = new Array();

            container.gridRowSelectionEvents.push(eventIndex);
        }
    }
}

// Private: return the sub set of event triggers which has the matched trigger template Id
RIUTIL.getTriggersByType = function (userEvent, triggerTemplateId) {
    var triggers = RIUTIL.getTriggersFromEvent(userEvent);
    var retTriggers = new Array();
    if (triggers) {
        for (var i = 0; i < triggers.length; i++) {
            var trigger = triggers[i];
            var templateId = trigger.templateId;
            if (templateId == triggerTemplateId)
                retTriggers.push(trigger);
        }
    }
    return retTriggers;
}

// Private: get the trigger collection given user event
RIUTIL.getTriggersFromEvent = function (userEvent) {
    return userEvent.eventRules.parts[0].parts;
}

// Public: invoked by Server to render the user event rules in design mode
RIUTIL.clearEventRules = function () {
    var eventRulesContainer = document.getElementById(RIUTIL.ID_EVENT_RULES_CONTAINER);
    RIUTIL.removeEvent(eventRulesContainer, "mousemove", RIUTIL.goMouseMoveRootActionsContainer);
    RIUTIL.removeEvent(eventRulesContainer, "mouseout", RIUTIL.goMouseOutRootActionsContainer);
    RIUTIL.removeAllChildren(eventRulesContainer);

    RIUTIL.cleanUpRuleIndicators();
    RIUTIL.ruleInsertMarkers = new Array();
    RIUTIL.ruleMarkers = new Array();
}

// Public: invoked by Server to render the user event rules in design mode with validation or not
RIUTIL.updateUserEventRules = function (json) {
    RIUTIL.currentEvent = eval(json);
    RIUTIL.renderUserEventRules();
}

// Public: invoked by Server to execute the user event rules in runtime mode
RIUTIL.executeUserEventRules = function (json) {
    RIUTIL.currentEvent = eval(json);
    try {
        var result = RIUTIL.executeRule("_1", RIUTIL.currentEvent.eventRules.parts[1]);
    }
    catch (e) // capture the first runtime execution error and stop to run
    {
        if (e.errorMessage)
            RIUTIL.showRuntimeError(e);
        else if (e.message)
            alert(e.message);
        else
            alert(e);
    }
}

// Private: show error object
RIUTIL.showRuntimeError = function (error) {
    JSSidePanelRender.reqOpen(JSSidePanelRender.MODE_VIEW_END_USER_EVENT);
    var eventRulesContainer = document.getElementById(RIUTIL.ID_EVENT_RULES_CONTAINER);
    RIUTIL.renderRule(eventRulesContainer, "", RIUTIL.currentEvent.eventRules, false);
    RIUTIL.renderError(error);
}

// Private: render user event after clean up
RIUTIL.renderUserEventRules = function () {
    var sidePanelOffset = RIUTIL.getSidePanelScrollOffset();
    RIUTIL.clearEventRules();
    var eventRulesContainer = document.getElementById(RIUTIL.ID_EVENT_RULES_CONTAINER);
    if (RIUTIL.currentEvent) {
        RIUTIL.addEvent(eventRulesContainer, "mousemove", RIUTIL.goMouseMoveRootActionsContainer);
        RIUTIL.addEvent(eventRulesContainer, "mouseout", RIUTIL.goMouseOutRootActionsContainer);
        RIUTIL.renderRule(eventRulesContainer, "", RIUTIL.currentEvent.eventRules, false);
        RIUTIL.setSidePanelScrollOffset(sidePanelOffset);
    }
}

// Private: validate user event and return error object if there is any validation error, otherwise return null
RIUTIL.validateUserEventRules = function () {
    if (RIUTIL.currentEvent) {
        return RIUTIL.validateRule("", RIUTIL.currentEvent.eventRules);
    }
    return null;
}

// Private: search in elements to find the closet elelement region to mouse. 
// when mutiple region contain the mouse, use the smallest one.
RIUTIL.findClosestElemenent = function (elements, mouse) {
    var closestElem = null;
    var closestRegion = null;
    var closetDistance = 1000000000;

    //window.status = mouse.x + "," + mouse.y + ":";
    for (var i = 0; i < elements.length; i++) {
        var elem = elements[i];
        var region = RIUTIL.getRegion(elem);
        var isInCurrentRegion = RIUTIL.isInRegion(region, mouse.x, mouse.y);

        if (closetDistance == 0) // we already find a region to contain the mouse
        {
            if (isInCurrentRegion && RIUTIL.isInRegion(closestRegion, region.left, region.top)) // the new inside region is inside the closest region, so the new one is the closest one
            {
                closestElem = elem;
                closestRegion = region;
            }
        }
        else // so far, we have not found any region contain the mouse
        {
            if (isInCurrentRegion) // we found the first region to contain the mouse and we will use it as the closest one
            {
                closetDistance = 0;
                closestElem = elem;
                closestRegion = region;
            }
            else // do a distance comparision between the mouse and the closet point of the region to determine who is the closest one.
            {
                if (mouse.x >= region.left && mouse.x <= region.left + region.width) // mouse x is between left and right edge of the region
                    var deltaX = 0;
                else // mouse x is not between left and right edge of the region
                    var deltaX = Math.min(Math.abs(mouse.x - region.left), Math.abs(mouse.x - region.left - region.width));

                if (mouse.y >= region.top && mouse.y <= region.top + region.height) // mouse y is betweeen top and bottom of the region
                    var deltaY = 0;
                else // mouse y is not betweeen top and bottom of the region
                    var deltaY = Math.min(Math.abs(mouse.y - region.top), Math.abs(mouse.y - region.top - region.height));

                var distance = deltaX * deltaX + deltaY * deltaY;

                if (distance < closetDistance) {
                    closetDistance = distance;
                    closestElem = elem;
                    closestRegion = region;
                }
            }
        }
    }

    if (!closestElem)
        return null;
    else
        return { closestElem: closestElem, closestRegion: closestRegion };
}

// Private: render given event rules detail recursively inside of given rule container
RIUTIL.renderRule = function (parentNode, ruleId, rule, inWizard) {
    var tagName = rule.tagName;
    switch (tagName) {
        case RIUTIL.TAG_ACTIONS:
        case RIUTIL.TAG_TRIGGERS:
            RIUTIL.renderRuleCollection(parentNode, ruleId, rule, inWizard);
            break;

        case RIUTIL.TAG_COMPOSITE:
            RIUTIL.renderRuleComposite(parentNode, ruleId, rule, inWizard);
            break;

        case RIUTIL.TAG_LEAF:
            RIUTIL.renderRuleLeaf(parentNode, ruleId, rule, inWizard);
            break;

        case RIUTIL.TAG_WIZARD:
            RIUTIL.renderWizardPlaceHolder(parentNode, ruleId, rule, inWizard);
            break;
    }
}

// Private: validate given event rules detail recursively
RIUTIL.validateRule = function (ruleId, rule) {
    var tagName = rule.tagName;
    switch (tagName) {
        case RIUTIL.TAG_ACTIONS:
        case RIUTIL.TAG_TRIGGERS:
            return RIUTIL.validateRuleCollection(ruleId, rule);

        case RIUTIL.TAG_COMPOSITE:
            return RIUTIL.validateRuleComposite(ruleId, rule);

        case RIUTIL.TAG_LEAF:
            return RIUTIL.validateRuleLeaf(ruleId, rule);

        case RIUTIL.TAG_WIZARD:
            return RIUTIL.validateWizardPlaceHolder(ruleId, rule);
    }
    return null;
}

// Private: render rule collection 
RIUTIL.renderRuleCollection = function (parentNode, ruleId, rule, inWizard) {
    var parts = rule.parts;
    var wizardId = rule.wizardId;
    var table = document.createElement("TABLE");
    parentNode.appendChild(table);
    if (RIUTIL.isRuleEditable())
        RIUTIL.renderRuleInsertMarker(table, ruleId + "_" + 0, wizardId);

    if (!parts)
        return;

    for (var partId = 0; partId < parts.length; partId++) {
        var childRule = parts[partId];
        var childRuleId = ruleId + "_" + partId;
        var type = childRule.type;
        var row = table.insertRow(-1);
        var cell = row.insertCell(-1);
        cell.ruleId = childRuleId;
        cell.wizardId = childRule.wizardId;
        if (RIUTIL.isRuleEditable() && RIUTIL.ruleMarkers)
            RIUTIL.ruleMarkers.push(cell);
        RIUTIL.renderRule(cell, childRuleId, childRule, inWizard);
        if (RIUTIL.isRuleEditable())
            RIUTIL.renderRuleInsertMarker(table, ruleId + "_" + (partId + 1), wizardId);
    }
}

// Private: validate rule collection 
RIUTIL.validateRuleCollection = function (ruleId, rule) {
    var parts = rule.parts;
    var wizardId = rule.wizardId;

    if (!parts)
        return null;

    for (var partId = 0; partId < parts.length; partId++) {
        var childRule = parts[partId];
        var childRuleId = ruleId + "_" + partId;
        var result = RIUTIL.validateRule(childRuleId, childRule);
        if (result)
            return result;
    }
    return null;
}

// outer table
//   outer row
//      outer cell
//           innner table
//             inner row 
//                inner cell [if]  + inner cell [condition] +  inner cell [then]
//   outer row
//      outer cell
//           [rule block]
//   outer row
//      outer cell
//           innner table
//             inner row 
//                inner cell [if]  + inner cell [condition] +  inner cell [then]
//   outer row
//      outer cell
//           [rule block]
RIUTIL.renderRuleComposite = function (parentNode, ruleId, rule, inWizard) {
    var parts = rule.parts;
    var domContext = new Object();
    domContext.outerTable = document.createElement("table");
    var text = RIUTIL.CONSTANTS[rule.textId];
    if (!inWizard && ruleId) {
        domContext.outerTable.wizardId = rule.wizardId;
        domContext.outerTable.ruleId = ruleId;
        if (RIUTIL.isRuleEditable()) {
            RIUTIL.addEvent(domContext.outerTable, "click", RIUTIL.goChangeTemplate);
            RIUTIL.setTitleBaseOnWizardId(domContext.outerTable);
        }
    }

    RIUTIL.attachHoverEffect(domContext.outerTable, RIUTIL.getRuleComponentClassName(rule, inWizard, false), RIUTIL.getRuleComponentClassName(rule, inWizard, true));
    domContext.outerTable.setAttribute('cellPadding', 1);
    domContext.outerTable.setAttribute('cellSpacing', 1);
    domContext.outerRow = domContext.outerTable.insertRow(-1);
    domContext.outerCell = domContext.outerRow.insertCell(-1);
    parentNode.appendChild(domContext.outerTable);

    domContext.innerTable = RIUTIL.renderRuleInnerLayoutTable(domContext.outerCell);
    domContext.innerRow = domContext.innerTable.insertRow(-1);

    // fomate example: a grid row on {0}[a frame] is selected
    var pos1 = text.indexOf("{");
    while (pos1 != -1) {
        var pos2 = text.indexOf("}", pos1 + 1);
        var beforeText = text.substring(0, pos1);
        var partId = text.substring(pos1 + 1, pos2);

        var pos3 = text.indexOf("[", pos2 + 1);
        if (pos3 == -1 || pos3 != pos2 + 1) // not hint text 
        {
            var hintText = null;
            var text = text.substring(pos2 + 1);
        }
        else // exist hint text
        {
            var pos4 = text.indexOf("]", pos2 + 1);
            var hintText = text.substring(pos3 + 1, pos4);
            var text = text.substring(pos4 + 1);
        }

        RIUTIL.renderTextPart(domContext, beforeText);
        RIUTIL.renderCompositePart(domContext, parts, ruleId, partId, inWizard, hintText);
        var pos1 = text.indexOf("{");
    }

    RIUTIL.renderTextPart(domContext, text);
    return;
}

// Private: validate rule composite
RIUTIL.validateRuleComposite = function (ruleId, rule) {
    var parts = rule.parts;
    if (!parts)
        return null;
    for (var partId = 0; partId < parts.length; partId++) {
        var result = RIUTIL.validateCompositePart(parts, ruleId, partId);
        if (result)
            return result;
    }

    return null;
}


// Private: render a certain part of composite ule template give part id and the rule id for the tempalte
RIUTIL.renderCompositePart = function (domContext, parts, ruleId, partId, inWizard, hintText) {
    var childRule = parts[partId];
    var childRuleId = ruleId + "_" + partId;
    var type = childRule.type;
    childRule.hintText = hintText;
    if (childRule.wizardId == RIUTIL.WID_ACTIONS_BLOCK || childRule.wizardId == RIUTIL.WID_TRIGGERS_BLOCK) // for rule block, we have to use a new table row for it
    {
        domContext.outerRow = domContext.outerTable.insertRow(-1);
        domContext.outerCell = domContext.outerRow.insertCell(-1);
        RIUTIL.renderRule(domContext.outerCell, childRuleId, childRule, inWizard);

        domContext.outerRow = domContext.outerTable.insertRow(-1);
        domContext.outerCell = domContext.outerRow.insertCell(-1);
        domContext.innerTable = RIUTIL.renderRuleInnerLayoutTable(domContext.outerCell);
        domContext.innerRow = domContext.innerTable.insertRow(-1);
    }
    else {
        domContext.innerCell = domContext.innerRow.insertCell(-1);
        RIUTIL.renderRule(domContext.innerCell, childRuleId, childRule, inWizard);
    }
}

// Private: validate a certain part of composite ule template give part id and the rule id for the tempalte
RIUTIL.validateCompositePart = function (parts, ruleId, partId) {
    var childRule = parts[partId];
    var childRuleId = ruleId + "_" + partId;
    return RIUTIL.validateRule(childRuleId, childRule);
}


// Private: render a text piece as part of composite ule template
RIUTIL.renderTextPart = function (domContext, text) {
    if (!text)
        return;

    text = text.replace(/\s/g, " \u00A0");
    domContext.innerCell = domContext.innerRow.insertCell(-1);
    domContext.innerCell.appendChild(document.createTextNode(text));
}

// Private: get the css class name for the rule component 
RIUTIL.getRuleComponentClassName = function (rule, inWizard, isHover) {
    var className = RIUTIL.getFinalClass("RIRulesCompositeTable RIOutBorder");

    switch (rule.wizardId) {
        case RIUTIL.WID_ACTIONS_BLOCK:
            if (isHover)
                return className + " RIRuleShape RICompositeHoverBG";
            else
                return className + " RIRuleShape RIRuleBG";

        case RIUTIL.WID_TRIGGERS_BLOCK:
            if (isHover)
                return className + " RITriggerShape RICompositeHoverBG";
            else
                return className + " RITriggerShape RITriggerBG";

        case RIUTIL.WID_LOGIC:
            if (isHover)
                return className + " RILogicShape RICompositeHoverBG";
            else
                return className + " RILogicShape RILogicBG";

        case RIUTIL.WID_DATA:
            if (isHover)
                return className + " RISourceShape RICompositeHoverBG";
            else
                return className + " RISourceShape RISourceBG";
    }

    return "";
}

// Private: render rule inner layout table
RIUTIL.renderRuleInnerLayoutTable = function (parentNode) {
    var table = document.createElement("table");
    table.className = "RITransparentLayoutTable";
    table.setAttribute('cellPadding', 0);
    table.setAttribute('cellSpacing', 0);
    parentNode.appendChild(table);
    return table
}

// Private: render rule leaf node
RIUTIL.renderRuleLeaf = function (parentNode, ruleId, rule, inWizard) {
    if ((rule.hideInWizard && inWizard) || (rule.hideInline && !inWizard))
        return;

    switch (rule.baseId) {
        case RIUTIL.BID_EDITABLE_FIELD:
        case RIUTIL.BID_VISIBLE_FIELD:
            RIUTIL.renderFieldSelection(parentNode, ruleId, rule, inWizard);
            break;

        case RIUTIL.BID_MENU:
            RIUTIL.renderMenuItem(parentNode, ruleId, rule, inWizard);
            break;

        case RIUTIL.BID_BUTTON:

            break;

        case RIUTIL.BID_AGGREGATE_OPERATOR:
            RIUTIL.renderOperatorListBeforeEdit(parentNode, ruleId, RIUTIL.AGGREGATE_OPERATORS, rule.operatorId, inWizard);
            break;

        case RIUTIL.BID_COMPARE_OPERATOR:
            RIUTIL.renderOperatorListBeforeEdit(parentNode, ruleId, RIUTIL.COMPARE_OPERATORS, rule.operatorId, inWizard);
            break;

        case RIUTIL.BID_VALUE:
            var value = rule.value ? rule.value : rule.hintText;
            RIUTIL.renderUserValueBeforeEdit(parentNode, value, ruleId, inWizard);
            break;

        case RIUTIL.BID_FRAME:
            RIUTIL.renderFrameSelection(parentNode, ruleId, rule, inWizard);
            break;
    }
}

// Private: validate rule leaf node
RIUTIL.validateRuleLeaf = function (ruleId, rule) {
    switch (rule.baseId) {
        case RIUTIL.BID_EDITABLE_FIELD:
        case RIUTIL.BID_VISIBLE_FIELD:
        case RIUTIL.BID_MENU:
        case RIUTIL.BID_BUTTON:
        case RIUTIL.BID_AGGREGATE_OPERATOR:
        case RIUTIL.BID_COMPARE_OPERATOR:
        case RIUTIL.BID_VALUE:
            // always valid
            break;

        case RIUTIL.BID_FRAME:
            if (!rule.refObjId)
                return { ruleId: ruleId, errorMessage: RIUTIL.CONSTANTS.RI_R_ERR_NO_FRAME };
            break;
    }

    return null;
}

// Private: render captured visible or editable field in the rule
RIUTIL.renderFieldSelection = function (parentNode, ruleId, rule, inWizard) {
    var divTag = document.createElement("div");
    var text = inWizard ? rule.hintText : rule.description;
    divTag.appendChild(document.createTextNode(text));
    if (!inWizard) {
        divTag.id = RIUTIL.getDomIdByRuleId(ruleId);
        divTag.refObjId = rule.refObjId;
        divTag.clientId = rule.clientId;
        divTag.ruleId = ruleId;
        divTag.wizardId = RIUTIL.WID_DATA;
        divTag.ruleAction = RIUTIL.RULE_ACTION_REPLACE;
        divTag.baseId = rule.baseId;
        if (RIUTIL.isRuleEditable()) {
            RIUTIL.addEvent(divTag, "click", rule.baseId == RIUTIL.BID_EDITABLE_FIELD ? RIUTIL.goCaptureEditableFieldBegin : RIUTIL.goLaunchWizard);
            RIUTIL.setTitleBaseOnWizardId(divTag);
        }
        RIUTIL.attachHoverEffect(divTag, RIUTIL.getFinalClass("RILeafBorder RISourceBG RISourceShape"), RIUTIL.getFinalClass("RILeafHoverBorder RISourceShape RICompositeHoverBG"));
        RIUTIL.addEvent(divTag, "mouseover", RIUTIL.onMouseOverField);
        RIUTIL.addEvent(divTag, "mouseout", RIUTIL.onMouseOutField);
    }
    parentNode.appendChild(divTag);
}

// Private: setup title (hover text) based on the information of the elem (wizardId)
RIUTIL.setTitleBaseOnWizardId = function (elem, titleText) {
    if (!titleText)
        titleText = RIUTIL.CONSTANTS.RI_HOVER_WIZAR_ICON.format(RIUTIL.getWizardNameById(elem.wizardId));
    elem.title = titleText;
}

// Private: return wizard name in translated text given wizard id
RIUTIL.getWizardNameById = function (wizardId) {
    switch (wizardId) {
        case RIUTIL.WID_ACTIONS_BLOCK:
            return RIUTIL.CONSTANTS.RI_ACTION_WIZARD;
        case RIUTIL.WID_LOGIC:
            return RIUTIL.CONSTANTS.RI_LOGIC_WIZARD;
        case RIUTIL.WID_DATA:
            return RIUTIL.CONSTANTS.RI_DATA_WIZARD;
        case RIUTIL.WID_TRIGGERS_BLOCK:
            return RIUTIL.CONSTANTS.RI_TRIGGER_WIZARD;

        case RIUTIL.WID_CAPTURE_MENU:
        case RIUTIL.WID_CAPTURE_EDITABLE_FIELD:
        case RIUTIL.WID_CAPTURE_BUTTON:
            alert("should not be displayed")
            return null;
    }
}

// Private: return the css style classname if it is editable end user rule mode
RIUTIL.getFinalClass = function (originalClasses) {
    return originalClasses + (RIUTIL.isRuleEditable() ? " RIClickable" : "");
}

// Private: render menu sellection
RIUTIL.renderMenuItem = function (parentNode, ruleId, rule, inWizard) {
    var refObjId = rule.refObjId;
    var containerWin = RIUTIL.getContainerWinByRefObjId(RIUTIL.layout, refObjId);
    if (containerWin) {
        var doc = containerWin.document;
        var src = doc.getElementById(rule.clientId).src;
    }
    else {
        var src = top['E1RES_img_RI_menuitemwizard_gif'];
    }
    var displayImg = document.createElement("img");
    var normalImageSrc = src;
    displayImg.id = RIUTIL.getDomIdByRuleId(ruleId);
    displayImg.refObjId = rule.refObjId;
    displayImg.clientId = rule.clientId;
    displayImg.ruleId = ruleId;
    displayImg.wizardId = rule.wizardId;
    displayImg.ruleAction = RIUTIL.RULE_ACTION_REPLACE;
    displayImg.baseId = rule.baseId;
    displayImg.src = normalImageSrc;
    if (RIUTIL.isRuleEditable()) {
        RIUTIL.addEvent(displayImg, "click", RIUTIL.goCaptureMenuItemBegin);
        RIUTIL.setTitleBaseOnWizardId(displayImg, RIUTIL.CONSTANTS.RI_CAPUTURE_MENU_ITEM);
    }
    RIUTIL.attachHoverEffect(displayImg, RIUTIL.getFinalClass("RILeafBorder RISourceShape RISourceBG"), RIUTIL.getFinalClass("RILeafHoverBorder RISourceShape RICompositeHoverBG"));
    RIUTIL.addEvent(displayImg, "mouseover", RIUTIL.onMouseOverField);
    RIUTIL.addEvent(displayImg, "mouseout", RIUTIL.onMouseOutField);

    parentNode.appendChild(displayImg);
}

// Private: render E1 Satellite Form seletion
RIUTIL.renderFrameSelection = function (parentNode, ruleId, rule, inWizard) {
    var selectTag = document.createElement("SELECT");
    selectTag.ruleId = ruleId;
    selectTag.id = RIUTIL.getDomIdByRuleId(ruleId);
    selectTag.wizardId = rule.wizardId;
    if (inWizard) // wizard
    {
        selectTag.options[0] = new Option(rule.hintText, 0, false, false);
        selectTag.disabled = true;
    }
    else // edit or debug
    {
        if (RIUTIL.isRuleEditable()) {
            RIUTIL.addEvent(selectTag, "change", RIUTIL.goFrameSelection);
            RIUTIL.addEvent(selectTag, "click", RIUTIL.goPreventClickEventBubbling);
            RIUTIL.setTitleBaseOnWizardId(selectTag, RIUTIL.CONSTANTS.RI_SELECT_SATELLITE);
        }
        else
            selectTag.disabled = true;

        var containerList = new Array();
        RIUTIL.getE1SatelliteContainerList(RIUTIL.layout, containerList, false);
        for (var i = 0; i < containerList.length; i++) {
            var container = containerList[i];
            var refObjId = container.frame.objId;
            var display = container.frame.description;
            selectTag.options[i] = new Option(display, refObjId, false, false);
            if (refObjId == rule.refObjId)
                selectTag.options[i].selected = true;
        }
        if (RIUTIL.isRuleEditable())
            rule.refObjId = RIUTIL.sentFrameSelection(selectTag);

    }

    parentNode.appendChild(selectTag);
    if (selectTag.offsetWidth > RIUTIL.MAX_FRAME_SELECT_TAG_WIDTH)
        selectTag.style.width = RIUTIL.MAX_FRAME_SELECT_TAG_WIDTH + "px";

}

// Private: let server know what is the current selection for frame
RIUTIL.sentFrameSelection = function (elem) {
    if (elem.options.length > 0) {
        var refObjId = elem.options[elem.selectedIndex].value;
        RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_SET_FRAME_REF_OBJ, RIUTIL.PARAM_RULE_ID, elem.ruleId, RIUTIL.PARAM_REF_OBJ_ID, refObjId);
        return refObjId;
    }
    return null;
}

// Private: get the all E1 Satellite Form information excluding minimized or filter out container
RIUTIL.getE1SatelliteContainerList = function (container, containerList, normalizedContentOnly) {
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout
            if (container.subContainers) {
                for (var i = 0; i < container.subContainers.length; i++) {
                    var subContainer = container.subContainers[i];
                    RIUTIL.getE1SatelliteContainerList(subContainer, containerList, normalizedContentOnly);
                }
            }
            break;

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            // only care about normalized window if normalizedContentOnly is true, otherwise both minimized or normal content is ok
            var allowedWindowState = (normalizedContentOnly && !RIUTIL.isMinimized(container)) || !normalizedContentOnly;
            if (RIUTIL.isE1SatelliteContainer(container) && allowedWindowState && !RIUTIL.isFilteredOut(container)) {
                containerList.push(container);
            }
            break;
    }
}

// Private: get the counter of leaf containers recursively under given container
RIUTIL.getLeafContainerCount = function (container) {
    var count = 0;
    switch (container.layoutType) {
        case RIUTIL.LAYOUT_VERTICAL: // vertical layout
        case RIUTIL.LAYOUT_HORIZONTAL: // horizonal layout

            for (var i = 0; i < container.subContainers.length; i++) {
                var subContainer = container.subContainers[i];
                count += RIUTIL.getLeafContainerCount(subContainer);
            }
            break;

        case RIUTIL.LAYOUT_SINGLE: // singluar page (E1 form)
            count++;
            break;
    }
    return count;
}

// Private: render user value in div, when user click it, it change to input field.
RIUTIL.renderUserValueBeforeEdit = function (parentNode, value, ruleId, inWizard) {
    RIUTIL.removeAllChildren(parentNode);

    var divTag = document.createElement("div");
    divTag.appendChild(document.createTextNode(value));
    if (!inWizard) {
        divTag.ruleId = ruleId;
        divTag.value = value;
        if (RIUTIL.isRuleEditable()) {
            RIUTIL.addEvent(divTag, "click", RIUTIL.goChangeRuleUserValueBegin);
            RIUTIL.setTitleBaseOnWizardId(divTag, RIUTIL.CONSTANTS.RI_CHANGE_CONSTANT);
        }
        RIUTIL.attachHoverEffect(divTag, RIUTIL.getFinalClass("RILeafBorder RISourceShape"), RIUTIL.getFinalClass("RILeafHoverBorder RISourceShape RICompositeHoverBG"));
    }

    parentNode.appendChild(divTag);
}

// Private: render user value as input field when editting 
RIUTIL.renderUserValueWhenEdit = function (parentNode, value, ruleId) {
    RIUTIL.removeAllChildren(parentNode);

    var inputTag = document.createElement("input");
    RIUTIL.addEvent(inputTag, "click", RIUTIL.goPreventClickEventBubbling);
    inputTag.value = value;
    inputTag.style.width = "60px";
    inputTag.ruleId = ruleId;
    RIUTIL.addEvent(inputTag, "blur", RIUTIL.goChangeRuleUserValueEnd);
    parentNode.appendChild(inputTag);
    inputTag.select();
}

// Private: render operator list as div before edit 
RIUTIL.renderOperatorListBeforeEdit = function (parentNode, ruleId, itemList, selectedIndex, inWizard) {
    RIUTIL.removeAllChildren(parentNode);
    var value = itemList[selectedIndex];
    var divTag = document.createElement("div");
    divTag.appendChild(document.createTextNode(value));
    if (!inWizard) {
        divTag.ruleId = ruleId;
        divTag.value = value;
        divTag.selectedIndex = selectedIndex;
        divTag.itemList = itemList;
        if (RIUTIL.isRuleEditable()) {
            RIUTIL.addEvent(divTag, "click", RIUTIL.goChangeRuleOperatorBegin);
            RIUTIL.setTitleBaseOnWizardId(divTag, RIUTIL.CONSTANTS.RI_CHANGE_OPERATOR);
        }
        RIUTIL.attachHoverEffect(divTag, RIUTIL.getFinalClass("RILeafBorder RISourceShape"), RIUTIL.getFinalClass("RILeafHoverBorder RISourceShape RICompositeHoverBG"));
    }
    parentNode.appendChild(divTag);
}

// Private: render operator list as select tag when edit 
RIUTIL.renderOperatorListWhenEdit = function (parentNode, ruleId, itemList, selectedIndex) {
    RIUTIL.removeAllChildren(parentNode);
    var selectTag = document.createElement("select");

    selectTag.ruleId = ruleId;
    selectTag.itemList = itemList;

    RIUTIL.addEvent(selectTag, "blur", RIUTIL.goChangeRuleOperatorEnd);

    for (var i = 0; i < itemList.length; i++) {
        var display = itemList[i];
        var value = i;
        selectTag.options[i] = new Option(display, value, false, false);
        if (value == selectedIndex)
            selectTag.options[i].selected = true;
    }

    parentNode.appendChild(selectTag);
    selectTag.focus();
}

// Private: render the place holder of wizard give wizard id at the position ruleId.partId 
RIUTIL.renderWizardPlaceHolder = function (parentNode, targetRuleId, rule, inWizard) {
    var wizardId = rule.wizardId;
    switch (wizardId) {
        case RIUTIL.WID_TRIGGERS_BLOCK:
            var icon = RIUTIL.renderWizardIcon(parentNode, targetRuleId, wizardId, inWizard, top['E1RES_img_RI_addtriggerwizard_gif'], top['E1RES_img_RI_addtriggerwizardmo_gif'], RIUTIL.goLaunchWizard, RIUTIL.RULE_ACTION_INIT_INSERT);
            break;

        case RIUTIL.WID_ACTIONS_BLOCK:
            var icon = RIUTIL.renderWizardIcon(parentNode, targetRuleId, wizardId, inWizard, top['E1RES_img_RI_ruleblockwizard_gif'], top['E1RES_img_RI_ruleblockwizardmo_gif'], RIUTIL.goLaunchWizard, RIUTIL.RULE_ACTION_INIT_INSERT);
            break;

        case RIUTIL.WID_LOGIC:
            var icon = RIUTIL.renderWizardIcon(parentNode, targetRuleId, wizardId, inWizard, top['E1RES_img_RI_logicexpressionwizard_gif'], top['E1RES_img_RI_logicexpressionwizardmo_gif'], RIUTIL.goLaunchWizard, RIUTIL.RULE_ACTION_REPLACE);
            break;

        case RIUTIL.WID_DATA:
            var icon = RIUTIL.renderWizardIcon(parentNode, targetRuleId, wizardId, inWizard, top['E1RES_img_RI_visiblefieldwizard_gif'], top['E1RES_img_RI_visiblefieldwizardmo_gif'], RIUTIL.goLaunchWizard, RIUTIL.RULE_ACTION_REPLACE);
            break;

        case RIUTIL.WID_CAPTURE_EDITABLE_FIELD:
            var icon = RIUTIL.renderWizardIcon(parentNode, targetRuleId, wizardId, inWizard, top['E1RES_img_RI_editablefieldwizard_gif'], top['E1RES_img_RI_editablefieldwizardmo_gif'], RIUTIL.goCaptureEditableFieldBegin, RIUTIL.RULE_ACTION_REPLACE, RIUTIL.CONSTANTS.RI_CAPTURE_EDIT_FIELD);
            icon.baseId = RIUTIL.BID_EDITABLE_FIELD;
            break;

        case RIUTIL.WID_CAPTURE_MENU:
        case RIUTIL.WID_CAPTURE_BUTTON:
            var icon = RIUTIL.renderWizardIcon(parentNode, targetRuleId, wizardId, inWizard, top['E1RES_img_RI_menuitemwizard_gif'], top['E1RES_img_RI_menuitemwizardmo_gif'], RIUTIL.goCaptureMenuItemBegin, RIUTIL.RULE_ACTION_REPLACE, RIUTIL.CONSTANTS.RI_CAPUTURE_MENU_ITEM);
            icon.baseId = RIUTIL.BID_MENU;
            break;

        default:
            return;
    }
}

// Private: render the place holder of wizard give wizard id at the position ruleId.partId 
RIUTIL.validateWizardPlaceHolder = function (targetRuleId, rule) {
    return { ruleId: targetRuleId, errorMessage: RIUTIL.CONSTANTS.RI_R_ERR_UNIT_WIZARD };
}

// Private: render a validation erorr for the component
RIUTIL.renderError = function (error) {
    var errorElemDomId = RIUTIL.getDomIdByRuleId(error.ruleId);
    var errorDescription = error.errorMessage;
    setTimeout("RIUTIL.showErrorWindow('" + errorElemDomId + "','" + errorDescription + "')", 500);
}

// Private: popup a IYFE window given the focused input element and error description
// it reuses the existing the similar method in AQ
RIUTIL.showErrorWindow = function (errorElemDomId, errorDescription) {
    var errorElem = document.getElementById(errorElemDomId);
    if (!errorElem)
        return;

    var winTop = getAbsoluteTopPos(errorElem, true);
    var winLeft = getAbsoluteLeftPos(errorElem, true);
    var winWidth = RIUTIL.ERROR_DIALOG_WIDTH;
    var winHeight = RIUTIL.ERROR_DIALOG_HEIGHT;

    var sb = new PSStringBuffer();
    sb.append("<div class='popupErrorText'>").append(errorDescription).append("</div>");
    var framework = sb.toString();

    var eventListener = {};
    eventListener.onClose = function (e) {
        RIUTIL.iyfeWin.destory();
        RIUTIL.iyfeWin = null;
        JSSidePanelRender.hideUIBlock();
    }

    windowTitle = (RIUTIL.CONSTANTS) ? RIUTIL.isRuleEditable() ? RIUTIL.CONSTANTS.RI_RULE_VALIDATION_ERROR : RIUTIL.CONSTANTS.RI_RULE_RUNTIME_ERROR : RIUTIL.CONSTANTS.RI_VALIDATE_ERROR;
    if (RIUTIL.stopPublish) {
        windowTitle = RIUTIL.CONSTANTS.RI_VALIDATE_ERROR;
    }
    RIUTIL.iyfeWin = createPopup(AQ.ID_IYFE_WIN, ADVANCED_QUERY, true, false, true, true, windowTitle, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, errorElem, framework, eventListener);
    JSSidePanelRender.showUIBlock();
}


// Private: render wizard icon given all the detail information
RIUTIL.renderWizardIcon = function (parentNode, ruleId, wizardId, inWizard, normalImageSrc, hoverImageSrc, clickHandler, ruleAction, hoverText) {
    var icon = document.createElement("img");
    icon.ruleId = ruleId;
    icon.id = RIUTIL.getDomIdByRuleId(ruleId);
    icon.wizardId = wizardId;
    if (inWizard)
        icon.src = normalImageSrc;
    else {
        if (RIUTIL.isRuleEditable()) {
            JSSidePanelRender.setMotionImage(icon, normalImageSrc, hoverImageSrc, clickHandler);
            RIUTIL.setTitleBaseOnWizardId(icon, hoverText);
        }
        RIUTIL.attachHoverEffect(icon, "RIClickable", "RIClickable");
        icon.ruleAction = ruleAction;
    }

    parentNode.appendChild(icon);
    return icon;
}

// Private: move the rule wizard insert icon to a new location under given rule id
RIUTIL.renderRuleInsertMarker = function (table, ruleId, wizardId) {
    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.id = RIUTIL.ID_EVENT_RULE_INSERT_MAKER + ruleId;
    cell.ruleId = ruleId;
    cell.wizardId = wizardId;
    if (RIUTIL.ruleInsertMarkers)
        RIUTIL.ruleInsertMarkers.push(cell);
}

// Private: move the rule wizard insert icon to a new location under given rule id
RIUTIL.relocateRuleInsertIndicator = function (sidePanelOffset, ruleId, wizardId, region) {
    var indicatorDiv = document.getElementById(RIUTIL.ID_RULE_INSERT_INDICATOR);
    if (!indicatorDiv) {
        indicatorDiv = document.createElement("div");
        indicatorDiv.id = RIUTIL.ID_RULE_INSERT_INDICATOR;
        indicatorDiv.className = "RIRuleIndicator";
        document.body.appendChild(indicatorDiv);

        var indicatorIcon = document.createElement("img");
        JSSidePanelRender.setMotionImage(indicatorIcon, top['E1RES_img_RI_addRule_gif'], top['E1RES_img_RI_addRulemo_gif'], RIUTIL.goLaunchWizard);
        indicatorIcon.ruleAction = RIUTIL.RULE_ACTION_INSERT;
        indicatorDiv.appendChild(indicatorIcon);
    }
    else {
        var indicatorIcon = indicatorDiv.firstChild;
    }
    indicatorIcon.ruleId = ruleId;
    indicatorIcon.ruleAction = RIUTIL.RULE_ACTION_INSERT;
    indicatorIcon.wizardId = wizardId;
    indicatorIcon.title = RIUTIL.CONSTANTS.RI_HOVER_INSERT_INDICATOR.format(RIUTIL.getWizardNameById(wizardId));
    indicatorDiv.style.left = region.left - sidePanelOffset.x + "px";
    indicatorDiv.style.top = region.top + (region.height / 2 - indicatorIcon.offsetHeight / 2) - sidePanelOffset.y + "px";
}

// Private: move the rule wizard insert icon to a new location under given rule id
RIUTIL.relocateRuleDeleteIndicator = function (sidePanelOffset, ruleId, region, mouse) {
    var indicatorDiv = document.getElementById(RIUTIL.ID_RULE_DELETE_INDICATOR);
    if (!indicatorDiv) {
        indicatorDiv = document.createElement("div");
        indicatorDiv.id = RIUTIL.ID_RULE_DELETE_INDICATOR;
        indicatorDiv.className = "RIRuleIndicator";
        document.body.appendChild(indicatorDiv);

        var indicatorIcon = document.createElement("img");
        indicatorIcon.title = RIUTIL.CONSTANTS.RI_HOVER_DELETE_INDICATOR;
        JSSidePanelRender.setMotionImage(indicatorIcon, top['E1RES_img_RI_delrule_gif'], top['E1RES_img_RI_delrulemo_gif'], RIUTIL.goDeleteRuleFromEvent);
        indicatorDiv.appendChild(indicatorIcon);
    }
    else {
        var indicatorIcon = indicatorDiv.firstChild;
    }
    indicatorIcon.ruleId = ruleId;
    indicatorDiv.style.left = region.left + region.width - indicatorIcon.offsetWidth / 2 - sidePanelOffset.x + "px";
    indicatorDiv.style.top = region.top - 3 - sidePanelOffset.y + "px";
}


// Private: render a wizard given wizard model id to allow user to select any template from this wizard and assign this template to the given ruleId
RIUTIL.renderEventWizard = function (elem, targetRuleId, wizardId, ruleAction) {
    var wizardContent = RIUTIL.createWizardContent(targetRuleId, wizardId, ruleAction);
    var eventListener = {};
    eventListener.onClose = function (e) {
        JSSidePanelRender.hideUIBlock();
    }

    // generate content for content factory
    var framework = "<div id=RIWizardTemplateTableDiv style='overflow:auto'></div>";

    JSSidePanelRender.showUIBlock();
    RIUTIL.wizardWin = createPopup("EVENT_WIZARD", SHOW_POPUP, true, false, true, true, wizardContent.wizardName, null, null, null, null, null, wizardContent.left, wizardContent.top, wizardContent.width, wizardContent.height, elem, framework, eventListener);

    var parentNode = document.getElementById("RIWizardTemplateTableDiv")
    parentNode.appendChild(wizardContent.contentElem);
    parentNode.popupWin = RIUTIL.wizardWin;
}

// Private: create dom content of wizard form with return object contain the window demension and content element.
RIUTIL.createWizardContent = function (targetRuleId, wizardId, ruleAction) {
    var table = document.createElement("table");
    table.containerId = RIUTIL.CONTAINER_ID_WIZARD;
    table.className = RIUTIL.CLASS_LAYOUT_TABLE;
    table.style.width = "100%";
    table.style.height = "0px";

    var templateList = RIUTIL.templateList;
    for (var i = 0; i < templateList.length; i++) {
        var template = templateList[i];
        if (template.wizardId == wizardId)
            RIUTIL.renderTemplateItem(table, targetRuleId, template, ruleAction);
    }

    var clientWidth = document.body.clientWidth;
    var clientHeight = document.body.clientHeight;
    var winWidth = RIUTIL.RULE_WIZARD_WIDTH;

    // tempary add it to doc in order to get its real height.
    document.body.appendChild(table);
    var winHeight = table.offsetHeight + 47;
    document.body.removeChild(table);

    var winTop = clientHeight / 2 - winHeight / 2;
    var winLeft = clientWidth / 2 - winWidth / 2;
    return { wizardName: RIUTIL.getWizardNameById(wizardId), top: winTop, left: winLeft, width: winWidth, height: winHeight, contentElem: table };
}

// Private: change event selection and vsibilities of a few menu item on the side panel
RIUTIL.changeEventSelection = function () {
    RIUTIL.goStopKeyCapture();
    var eventListTag = document.getElementById(RIUTIL.ID_EVENT_LIST);

    if (eventListTag.options.length > 0) {
        RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_LOAD_EVENT_RULES, RIUTIL.PARAM_EVENT_INDEX, eventListTag.selectedIndex, RIUTIL.PARAM_WITH_VALIDATION, false);
        RIUTIL.setElemVisible(RIUTIL.ID_VALIDATE_EVENT_ICON, true);
        RIUTIL.setElemVisible(RIUTIL.ID_CLEAR_EVENT_ICON, true);
        RIUTIL.setElemVisible(RIUTIL.ID_DELETE_EVENT_ICON, true);
    }
    else {
        RIUTIL.clearEventRules();
        RIUTIL.setElemVisible(RIUTIL.ID_VALIDATE_EVENT_ICON, false);
        RIUTIL.setElemVisible(RIUTIL.ID_CLEAR_EVENT_ICON, false);
        RIUTIL.setElemVisible(RIUTIL.ID_DELETE_EVENT_ICON, false);
    }
}

// Private: check name duplication for event name
RIUTIL.existEventName = function (targetEventName) {
    if (!RIUTIL.eventList)
        return false;

    var eventList = RIUTIL.eventList;
    for (var i = 0; i < eventList.length; i++) {
        var eventName = eventList[i].id;
        if (targetEventName == eventName)
            return true;
    }
    return false;
}

// Private: create a template item in any wizard into parent TD tag
RIUTIL.renderTemplateItem = function (table, targetRuleId, template, ruleAction) {
    var row = table.insertRow(-1);
    var cell = row.insertCell(-1);
    cell.className = "RIClickable";
    cell.ruleId = targetRuleId;
    cell.wizardId = template.wizardId;
    cell.templateId = template.templateId;
    cell.ruleAction = ruleAction;
    if (template.templateId == RIUTIL.TID_SOURCE_FIELD)
        cell.baseId = RIUTIL.BID_VISIBLE_FIELD;
    var clickHandler = (template.templateId == RIUTIL.TID_SOURCE_FIELD) ? RIUTIL.goCaptureVisibleFieldBegin : RIUTIL.goInsertRuleToEvent;
    RIUTIL.renderRuleComposite(cell, targetRuleId, template, true, false);
    RIUTIL.addEvent(cell, "click", clickHandler);
}

// Private: clean up all UI indicator related to User Rule edit panel 
RIUTIL.cleanUpRuleIndicators = function () {
    var indicatorDiv = document.getElementById(RIUTIL.ID_RULE_DELETE_INDICATOR);
    if (indicatorDiv)
        indicatorDiv.parentNode.removeChild(indicatorDiv);

    var indicatorDiv = document.getElementById(RIUTIL.ID_RULE_INSERT_INDICATOR);
    if (indicatorDiv)
        indicatorDiv.parentNode.removeChild(indicatorDiv);
}

// Public method to set the full template list on the client side.
RIUTIL.setTemplateList = function (json) {
    //alert(json)
    var obj = eval(json);
    RIUTIL.templateList = obj.templateList;

    // this is a hack to help with a over side popup shadow showing issue for the very first time of displaying wizard popup. 
    // run this method inadvance will reslove the issue, but we do not know why yet.
    RIUTIL.createWizardContent("", RIUTIL.WID_ACTIONS_BLOCK, RIUTIL.RULE_ACTION_REPLACE);
}

// Private: execute user event object
RIUTIL.executeUserEvent = function (eventIndex) {
    RIUTIL.postAjaxRequest(RIUTIL.layout.id, RIUTIL.getActiveProduct(), RIUTIL.AJAX_CMD_EXECUTE_EVENT, RIUTIL.PARAM_EVENT_INDEX, eventIndex);
}

// Private: show error with rule id
RIUTIL.throwRuntimeError = function (ruleId, message) {
    if (ruleId) {
        throw { ruleId: ruleId, errorMessage: message };
    }
}

// Private: generic execution method for all types of rule
// return false if incorrect input
RIUTIL.executeRule = function (ruleId, rule) {
    var tagName = rule.tagName;
    switch (tagName) {
        case RIUTIL.TAG_ACTIONS:
            return RIUTIL.executeRuleCollection(ruleId, rule);

        case RIUTIL.TAG_COMPOSITE:
            return RIUTIL.executeRuleComposite(ruleId, rule);

        case RIUTIL.TAG_LEAF:
            return RIUTIL.executeRuleLeaf(ruleId, rule);

        case RIUTIL.TAG_WIZARD:
            RIUTIL.throwRuntimeError(ruleId, RIUTIL.CONSTANTS.RI_R_ERR_UNIT_WIZARD);
            return false;

    }
    return false;
}

// Private: execute collection rule
// return true, indicates execute successfully of all; otherwise, return false
RIUTIL.executeRuleCollection = function (ruleId, rule) {
    var parts = rule.parts;
    var wizardId = rule.wizardId;
    if (!parts)
        return true;

    for (var partId = 0; partId < parts.length; partId++) {
        var childRule = parts[partId];
        var childRuleId = ruleId + "_" + partId;
        var result = RIUTIL.executeRule(childRuleId, childRule);
        if (!result)
            return false;
    }

    return true;
}

// Private: execute composite rule
// return object, indicates execute successfully of all; otherwise, return null
RIUTIL.executeRuleComposite = function (ruleId, rule) {
    var parts = rule.parts;
    var templateId = rule.templateId;
    switch (templateId) {
        case RIUTIL.TID_SET:
            return RIUTIL.execute_set_with(ruleId, rule);

        case RIUTIL.TID_PUSH_MENU:
            return RIUTIL.execute_push_menu(ruleId, rule);

        case RIUTIL.TID_PUSH_BUTTON:
            return RIUTIL.execute_push_button(ruleId, rule);

        case RIUTIL.TID_IF_THEN:
            return RIUTIL.execute_if_then(ruleId, rule);

        case RIUTIL.TID_IF_ELSE_THEN:
            return RIUTIL.execute_if_then_else(ruleId, rule);

        case RIUTIL.TID_COMPARE:
            return RIUTIL.execute_compare(ruleId, rule);

        case RIUTIL.TID_BETWEEN:
            return RIUTIL.execute_between(ruleId, rule);

        case RIUTIL.TID_AND:
            return RIUTIL.executeSubRule(ruleId, rule, 0) && RIUTIL.executeSubRule(ruleId, rule, 1);

        case RIUTIL.TID_OR:
            return RIUTIL.executeSubRule(ruleId, rule, 0) || RIUTIL.executeSubRule(ruleId, rule, 1);

        case RIUTIL.TID_NOT:
            return !RIUTIL.executeSubRule(ruleId, rule, 0);

        case RIUTIL.TID_USER_VALUE:
            return RIUTIL.executeSubRule(ruleId, rule, 0);

        case RIUTIL.TID_SOURCE_AGGREGATE:
            return RIUTIL.execute_source_aggregate(ruleId, rule);

        default:
            alert("template id:" + templateId + ' is not implemented yet.');
    }
    return null;
}

// Private method to trigger menu item
RIUTIL.executeMenu = function (menuId) {
    var menuItem = document.getElementById(menuId);
    if (menuItem) {
        if (RIUTIL.isTouchEnabled) // simulate a touchstart event for this menu item for touch enabled device because it does not have click event handler
        {
            var event = $.Event("touchstart");
            $(menuItem).trigger(event);
        }
        else
            menuItem.click();
    }
}

// Private: execute set with
RIUTIL.execute_set_with = function (ruleId, rule) {
    var source = RIUTIL.executeSubRule(ruleId, rule, 1);
    var targetInfo = RIUTIL.executeSubRule(ruleId, rule, 0);
    if (targetInfo) {
        targetInfo.elem.value = source;
        if (targetInfo.isFormControl) // form header
            targetInfo.elem.onblur.call(targetInfo.elem);
        else if (targetInfo.isGridCell)
            targetInfo.gridObj.onCellChange(targetInfo.rowIndex, targetInfo.colIndex, targetInfo.elem);
        return true;
    }
}

// Private: execute source aggregate
RIUTIL.execute_source_aggregate = function (ruleId, rule) {
    var source1 = RIUTIL.getSourceFloat(ruleId, rule, 0);
    var source2 = RIUTIL.getSourceFloat(ruleId, rule, 2);
    var operator = RIUTIL.executeSubRule(ruleId, rule, 1);

    switch (operator) {
        case 0: return source1 + source2;
        case 1: return source1 - source2;
        case 2: return source1 * source2;
        case 3: return source1 / source2;
    }
    return false;
}

// Private: execute source aggregate
RIUTIL.getSourceFloat = function (ruleId, rule, index) {
    var source = RIUTIL.executeSubRule(ruleId, rule, index);
    if (typeof source == "string" && !isNaN(source))
        source = parseFloat(source);
    return source;
}

// Private: execute if then
RIUTIL.execute_if_then = function (ruleId, rule) {
    var logic = RIUTIL.executeSubRule(ruleId, rule, 0);
    if (logic)
        RIUTIL.executeSubRule(ruleId, rule, 1);
    return true;
}

// Private: execute if then else
RIUTIL.execute_if_then_else = function (ruleId, rule) {
    var logic = RIUTIL.executeSubRule(ruleId, rule, 0);
    if (logic)
        RIUTIL.executeSubRule(ruleId, rule, 1);
    else
        RIUTIL.executeSubRule(ruleId, rule, 2);
    return true;
}

// Private: execute push button
RIUTIL.execute_push_button = function (ruleId, rule) {
    var targetInfo = RIUTIL.executeSubRule(ruleId, rule, 0);
    if (targetInfo) {
        targetInfo.elem.click();
        return true;
    }
    return false;
}

// Private: execute push menu
RIUTIL.execute_push_menu = function (ruleId, rule) {
    var targetInfo = RIUTIL.executeSubRule(ruleId, rule, 0);
    if (targetInfo) {
        targetInfo.elem.click();
        return true;
    }
    return false;
}

// Private: execute compare
RIUTIL.execute_compare = function (ruleId, rule) {
    var operator = RIUTIL.executeSubRule(ruleId, rule, 1);
    var source1 = RIUTIL.getSourceFloat(ruleId, rule, 0);
    var source2 = RIUTIL.getSourceFloat(ruleId, rule, 2);

    switch (operator) {
        case 0: return (source1 == source2);
        case 5: return (source1 != source2);
        case 1: return (source1 < source2);
        case 2: return (source1 > source2);
        case 3: return (source1 <= source2);
        case 4: return (source1 >= source2);
    }
    return false;
}

// Private: execute between
RIUTIL.execute_between = function (ruleId, rule) {
    var source1 = RIUTIL.getSourceFloat(ruleId, rule, 0);
    var source2 = RIUTIL.getSourceFloat(ruleId, rule, 1);
    var source3 = RIUTIL.getSourceFloat(ruleId, rule, 2);
    return source1 > source2 && source1 < source3;
}


// Private: execute sub rule given part index of parent rule and parent rule id
RIUTIL.executeSubRule = function (ruleId, rule, index) {
    return RIUTIL.executeRule(ruleId + "_" + index, rule.parts[index]);
}

// Private: execute leaf rule
// return object, indicates execute successfully; otherwise, return null
RIUTIL.executeRuleLeaf = function (ruleId, rule) {
    var baseId = rule.baseId;
    switch (baseId) {
        case RIUTIL.BID_EDITABLE_FIELD:
            var elemInfo = RIUTIL.getReferenceElementInfo(ruleId, rule.refObjId, rule.clientId);
            if (elemInfo != null) {
                var elem = elemInfo.elem;
                if (elem && elem.tagName == "INPUT" && elem.disabled == false && elem.readOnly == false) // validate it is editable
                    return elemInfo;

                if (elemInfo.isGridCell)
                    RIUTIL.throwRuntimeError(ruleId, RIUTIL.CONSTANTS.RI_R_ERR_WRITE_COL.format(rule.description, elemInfo.frameName));
                else
                    RIUTIL.throwRuntimeControlError(ruleId, RIUTIL.CONSTANTS.RI_R_ERR_WRITE_CONTROL, elemInfo, rule);
            }

            return null;

        case RIUTIL.BID_VISIBLE_FIELD:
            return RIUTIL.getReferenceValue(ruleId, rule.refObjId, rule.clientId, rule);

        case RIUTIL.BID_MENU:
            return RIUTIL.getReferenceElementInfo(ruleId, rule.refObjId, rule.clientId);

        case RIUTIL.BID_BUTTON:
            return RIUTIL.getReferenceElementInfo(ruleId, rule.refObjId, rule.clientId);

        case RIUTIL.BID_AGGREGATE_OPERATOR:
            return rule.operatorId;

        case RIUTIL.BID_COMPARE_OPERATOR:
            return rule.operatorId;

        case RIUTIL.BID_VALUE:
            return rule.value;
    }
    return null;
}

// Private: throw error object based given error template and elem info
RIUTIL.throwRuntimeControlError = function (ruleId, errorTemplate, elemInfo, rule) {
    var displayedControl = (elemInfo && elemInfo.elem && elemInfo.elem.title) ? elemInfo.elem.title : (rule.description ? rule.description : rule.clientId);
    if (rule.refObjId) // sate
    {
        var frameContainer = RIUTIL.getContainerByRefObjId(RIUTIL.layout, rule.refObjId)
        var displayedFrame = (frameContainer && frameContainer.frame) ? frameContainer.frame.description : "";
    }
    else {
        var displayedFrame = RIUTIL.getMasterFrameTitle();
    }
    RIUTIL.throwRuntimeError(ruleId, errorTemplate.format(displayedControl, displayedFrame));
}

// Private: get the title of the master form.
RIUTIL.getMasterFrameTitle = function () {
    return RIUTIL.getRootRIUTIL().formTitle;
}

// Private: get reference dom element given reofObjId and clientId
RIUTIL.getReferenceElementInfo = function (ruleId, refObjId, clientId) {
    var containerWin = RIUTIL.getContainerWinByRefObjId(RIUTIL.layout, refObjId);
    if (!containerWin)
        RIUTIL.throwRuntimeError(ruleId, RIUTIL.CONSTANTS.RI_R_ERR_ACCESS_FRAME);

    return containerWin.RIUTIL.getElementInfoByClientId(ruleId, clientId);
}

// Private: get value given reofObjId and clientId
RIUTIL.getReferenceValue = function (ruleId, refObjId, clientId, rule) {
    var containerWin = RIUTIL.getContainerWinByRefObjId(RIUTIL.layout, refObjId);
    if (!containerWin)
        RIUTIL.throwRuntimeError(ruleId, RIUTIL.CONSTANTS.RI_R_ERR_ACCESS_FRAME);

    var value = containerWin.RIUTIL.getValueByClientId(ruleId, clientId, rule);

    if (value == null)
        RIUTIL.throwRuntimeControlError(ruleId, RIUTIL.CONSTANTS.RI_R_ERR_ACESS_CONTROL, null, rule)

    return value;
}

RIUTIL.parameterizeActiveLayout = function (containerId, product, cmd, newLayoutName, approverNotes, newLayoutDescription) {
    //get the Current Active Layout Object
    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
    RIUTIL.cleanUpRuleIndicators();
    //check is "saveAs" on Existing CafeOne Layout
    var saveAsExistingLayout = (layoutId == RIUTIL.DEFAULT_LAYOUT_ID) ? false : true;
    var prodSysCode = RIUTIL.getProductSysCode();
    var e1UrlCreator = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_SERVICE);

    e1UrlCreator.setURI(RIUTIL.CAFEONE_UDO_SERVICE);
    if (RIUTIL.isCompositePage) {
        e1UrlCreator.setParameter(RIUTIL.PARAM_OBJECT_TYPE, "COMPOSITE");
    }
    else {
        e1UrlCreator.setParameter(RIUTIL.PARAM_OBJECT_TYPE, "CAFE1");
    }
    e1UrlCreator.setParameter(RIUTIL.PARAM_COMMAND, RIUTIL.cmdMap[cmd]);
    e1UrlCreator.setParameter(RIUTIL.PARAM_APP_ID, RIUTIL.layoutManager.appId);
    e1UrlCreator.setParameter(RIUTIL.PARAM_FORM_ID, RIUTIL.layoutManager.formId);
    e1UrlCreator.setParameter(RIUTIL.PARAM_VERSION_ID, RIUTIL.layoutManager.versionId);
    //passing the omwObjectName
    if (activeLayout.omwObjectName != null) {
        e1UrlCreator.setParameter(RIUTIL.PARAM_OMWOBJECTNAME, activeLayout.omwObjectName);
    }
    e1UrlCreator.setParameter(RIUTIL.PARAM_Id, activeLayout.id);
    if (newLayoutName != null) {
        e1UrlCreator.setParameter(RIUTIL.PARAM_LAYOUT_NAME, newLayoutName);
    }
    else {
        e1UrlCreator.setParameter(RIUTIL.PARAM_LAYOUT_NAME, activeLayout.name);
    }
    if (newLayoutDescription != null) {
        e1UrlCreator.setParameter(RIUTIL.PARAM_LAYOUT_DESCRIPTION, newLayoutDescription);
    }
    else {
        e1UrlCreator.setParameter(RIUTIL.PARAM_LAYOUT_DESCRIPTION, activeLayout.description);
    }
    e1UrlCreator.setParameter(RIUTIL.PARAM_APPROVERNOTES, approverNotes);
    e1UrlCreator.setParameter(RIUTIL.PARAM_RI_COMMAND, cmd);
    e1UrlCreator.setParameter(RIUTIL.PARAM_ACTION, RIUTIL.ACTION_AJAX);
    e1UrlCreator.setParameter(RIUTIL.PARAM_PRODUCT, prodSysCode);
    e1UrlCreator.setParameter(RIUTIL.PARAM_CONTAINER_ID, containerId);
    e1UrlCreator.setParameter(RIUTIL.PARAM_HASTOKEN, activeLayout.hasToken);
    e1UrlCreator.setParameter(RIUTIL.PARAM_TOKENPROJECTNAME, activeLayout.tokenProjectName);
    e1UrlCreator.setParameter(RIUTIL.PARAM_SAVEAS_LAYOUT, saveAsExistingLayout);

    //append stackId and other params
    e1UrlCreator = RIUTIL.setStackIdParameter(e1UrlCreator);

    return e1UrlCreator;
}

RIUTIL.checkPrivatePf = function (layoutJson) {
    var subContainersArray = [];
    if (layoutJson.subContainers) {
        subContainersArray = layoutJson.subContainers;
        for (var i = 0, len = subContainersArray.length; i < len; i++) {
            RIUTIL.checkPrivatePf(subContainersArray[i]);
        }
    }
    else {
        if (layoutJson.frame) {
            var frame = layoutJson.frame;
            var pfUser = (frame.pfUser) ? frame.pfUser : "";
            var pfOmwObjectName = (frame.pfOmwObjectName) ? frame.pfOmwObjectName : "";
            if ((pfUser.length > 0 && pfOmwObjectName.length > 0 && !(pfOmwObjectName === "null" || pfUser === "*PUBLIC"))) {
                RIUTIL.stopPublish = true;
            }
        }
    }


}

// Event handler called when the Request Publish button is clicked.
RIUTIL.goRequestPublishLayoutBegin = function (e) {
    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
    var layoutJson = eval(RIUTIL.oldLayoutJSON);
    RIUTIL.stopPublish = false;
    RIUTIL.checkPrivatePf(layoutJson);

    if (RIUTIL.stopPublish) {
        RIUTIL.showErrorWindow("formTitleTable", RIUTIL.CONSTANTS.RI_PUBLISH_ERROR);
        RIUTIL.stopPublish = false;
    }
    else if (!activeLayout.isPersonal) {
        RIUTIL.goReqPublishLayoutBegin("reRequestPublish");
    }
    else {
        RIUTIL.goReqPublishLayoutBegin("requestPublish");
    }
}

// Event handler called to render Request publish Notes prompt 
RIUTIL.goReqPublishLayoutBegin = function (eventName) {
    var layoutId = RIUTIL.layoutManager.currentLayoutId;
    var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
    var udoType;
    if (RIUTIL.isCompositePage) {
        udoType = RIUTIL.CONSTANTS.UDO_TYPE_COMPOSITEPAGE;
    }
    else {
        udoType = RIUTIL.CONSTANTS.UDO_TYPE_CAFEONE;
    }
    if (eventName == "requestPublish") {
        if (activeLayout.canShowPublishWarning) {
            var labels = new Array(RIUTIL.CONSTANTS.UDO_ENTER_APPROVER_NOTE, RIUTIL.CONSTANTS.UDO_CONFIRM_REQUEST_PUBLISH.format(udoType), RIUTIL.CONSTANTS.UDO_CONFIRM_REQ_PUB_OPTION.format(udoType));
            JSSidePanelRender.renderPromptForName(document, RIUTIL.ID_REQUEST_PUBLISH_ICON, 1, labels, RIUTIL.NAME_LENGTH, RIUTIL.goRequestPublishLayout, '', '');
        }
        else {
            RIUTIL.goRequestPublishLayout("");
        }
    }
    else if (eventName == "reRequestPublish") {
        if (activeLayout.canShowPublishWarning) {
            var labels = new Array(RIUTIL.CONSTANTS.UDO_ENTER_APPROVER_NOTE, RIUTIL.CONSTANTS.UDO_CONFIRM_REQUEST_PUBLISH.format(udoType), RIUTIL.CONSTANTS.UDO_CONFIRM_REQ_PUB_OPTION.format(udoType));
            JSSidePanelRender.renderPromptForName(document, RIUTIL.ID_REQUEST_PUBLISH_ICON, 1, labels, RIUTIL.NAME_LENGTH, RIUTIL.goReRequestPublishLayout, '', '');
        }
        else {
            RIUTIL.goReRequestPublishLayout("");
        }
    }
}

// Event handler called when after publish name prompt.
RIUTIL.goRequestPublishLayout = function (notes) {
    var e1URL = RIUTIL.parameterizeActiveLayout(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_REQPUBLISH_CUSTOMIZED_LAYOUT, null, notes, null);
    RIUTIL.sendXMLReq("POST", RIUTIL.layout.id, e1URL.toString(), RIUTIL.goRequestPublishLayoutEnd);
    return true;
}

// Event handler called to reRequestPublish a checkedOut layout.
RIUTIL.goReRequestPublishLayout = function (notes) {
    var e1URL = RIUTIL.parameterizeActiveLayout(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_REREQPUBLISH_CUSTOMIZED_LAYOUT, null, notes, null);
    RIUTIL.sendXMLReq("POST", RIUTIL.layout.id, e1URL.toString(), RIUTIL.goRequestPublishLayoutEnd);
    return true;
}

// Private: return error message part form given string, return null if these is no error signagure.
RIUTIL.retrieveErrorMessage = function (retStr) {
    var pos = retStr.indexOf(RIUTIL.ERROR_SIGNAITURE);
    if (pos == 0) {
        return retStr.substring(RIUTIL.ERROR_SIGNAITURE.length);
    }
    return null;
}

// Event handler called when the Request publish layout finishes
RIUTIL.goRequestPublishLayoutEnd = function (containerId, retStr) {
    var errMsg = RIUTIL.retrieveErrorMessage(retStr);
    if (errMsg) {
        RIUTIL.renderMessage(true, "Request publish Layout failed:" + errMsg, true);
    }
    else {
        if (RIUTIL.isCompositePage) {
            try {
                eval(retStr);
            }
            catch (e) {
                RIUTIL.renderMessage(true, retStr, true);
            }
        }
        else {
            RIUTIL.goFetchLayouts();
        }
    }
    if (RIUTIL.isCompositePage) {
        RIUTIL.resetKeepAlive();
    }
    return true;
}

// Event handler called when the reserve Layout button is clicked.
RIUTIL.goReserveLayoutBegin = function () {
    var e1URL = RIUTIL.parameterizeActiveLayout(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_RESERVE_CUSTOMIZED_LAYOUT);
    RIUTIL.sendXMLReq("POST", RIUTIL.layout.id, e1URL.toString(), RIUTIL.goReserveLayoutEnd);
    return true;
}
// Event handler called when the Unreserve Layout button is clicked.
RIUTIL.goUnReserveLayoutBegin = function () {
    var e1URL = RIUTIL.parameterizeActiveLayout(RIUTIL.layout.id, RIUTIL.getProductSysCode(), RIUTIL.AJAX_CMD_UNRESERVE_CUSTOMIZED_LAYOUT);
    RIUTIL.sendXMLReq("POST", RIUTIL.layout.id, e1URL.toString(), RIUTIL.goReserveLayoutEnd);
}

// Event handler called when the reserve/unreserve layout finishes
RIUTIL.goReserveLayoutEnd = function (containerId, retStr) {
    var errMsg = RIUTIL.retrieveErrorMessage(retStr);
    if (errMsg) {
        RIUTIL.renderMessage(true, errMsg, true);
        return;
    }
    else {
        try {
            eval(retStr);
        }
        catch (e) {
            RIUTIL.renderMessage(true, retStr, true);
        }
    }
    if (RIUTIL.isCompositePage) {
        RIUTIL.resetKeepAlive();
    }
    return true;
}

// Get LayoutNames available on this form
RIUTIL.goFetchLayouts = function () {
    var e1URL = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_SERVICE);
    e1URL.setURI(RIUTIL.CAFEONE_UDO_SERVICE);

    e1URL.setParameter(RIUTIL.PARAM_ACTION, RIUTIL.ACTION_AJAX);
    e1URL.setParameter(RIUTIL.PARAM_RI_COMMAND, RIUTIL.AJAX_CMD_GETLAYOUTNAMES);
    e1URL.setParameter(RIUTIL.PARAM_APP_ID, RIUTIL.layoutManager.appId);
    e1URL.setParameter(RIUTIL.PARAM_FORM_ID, RIUTIL.layoutManager.formId);
    e1URL.setParameter(RIUTIL.PARAM_VERSION_ID, RIUTIL.layoutManager.versionId);

    e1URL = RIUTIL.setStackIdParameter(e1URL);

    RIUTIL.sendXMLReq("POST", null, e1URL.toString(), RIUTIL.handlerAjaxRequest);
}
// setStackID Parameter to the E1URL
RIUTIL.setStackIdParameter = function (e1URL) {
    if (window.JDEDTAFactory) {
        var dtaInstance = JDEDTAFactory.getInstance(RIUTIL.namespace); // not poral support so far
        if (dtaInstance)
            var stackId = dtaInstance.stackId;

        e1URL.setParameter(RIUTIL.PARAM_STACK_ID, stackId);
    }

    // add other dynamic request parameters into the url
    for (var i = 3; i < arguments.length; ) {
        e1URL.setParameter(arguments[i++], arguments[i++]);
    }

    if (RIUTIL.isIOS) // IPAD cache the ajax request and return the same cached response for the same request, so we need to make request unique for each time.
        e1URL.setParameter("timestamp", (new Date()).getTime());

    return e1URL;
}

RIUTIL.disableIcon = function (id, outImg, title) {
    // get the <TD> of the Icon
    try {
        var iconCell = top.document.getElementById(id);
    } catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.
        return;
    }
    if (iconCell) {
        iconCell.onclick = null;
        iconCell.onclickHandler = null;
        iconCell.onmouseover = null;
        iconCell.onmouseout = null;
        iconCell.style.cursor = "default";
        var icon = iconCell.firstChild;
        if (icon != null) {
            JSSidePanelRender.setMotionImage(icon, outImg, null, null);
            icon.outSrc = outImg;
            icon.overSrc = null;
            icon.className = "";
            icon.hoverClassName = "";
            icon.onclick = null;
            icon.disabled = true;

            if (title) {
                icon.title = title;
                icon.alt = title;
            }
        }
    }
}

RIUTIL.enableIcon = function (id, outImg, overImg, eventHandler, title) {
    var iconCell = top.document.getElementById(id);
    if (iconCell) {
        var icon = iconCell.firstChild;
        JSSidePanelRender.setMotionImage(icon, outImg, overImg, null);
        icon.outSrc = outImg;
        icon.overSrc = overImg;
        icon.className = "RIAction";
        icon.onclick = eventHandler;
        icon.disabled = false;
        if (title) {
            icon.title = title;
            icon.alt = title;
        }
    }
}

// this function disable all the CAFEONE Panel Icons in the ULCM Preview mode
RIUTIL.disableToolbarICons = function () {
    RIUTIL.disableIcon(RIUTIL.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
    RIUTIL.disableIcon(RIUTIL.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], RIUTIL.CONSTANTS.UDO_SAVE_AS); // disable saveAs Icon
    RIUTIL.disableIcon(RIUTIL.RI_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], RIUTIL.CONSTANTS.UDO_RESERVE);
    RIUTIL.disableIcon(RIUTIL.ID_REQUEST_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], RIUTIL.CONSTANTS.UDO_REQUEST_PUBLISH);
    RIUTIL.disableIcon(RIUTIL.RI_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
    RIUTIL.disableIcon(RIUTIL.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], RIUTIL.CONSTANTS.UDO_NOTES);
}

// Private method to do a high interactive post to the server
RIUTIL.send = function (cmd) {
    JDEDTAFactory.getInstance(RIUTIL.namespace).post(cmd);
}

// Private Event Handler : to show if any notes attached to Layout 
RIUTIL.goFetchNotes = function () {
    if (RIUTIL.isCompositePage) {
        //since this will be a full page refresh, prompt the user if there is unsaved data
        if (RIUTIL.cancelActionForDirtyCheck()) {
            return;
        }
        else {
            var layoutId = RIUTIL.layoutManager.currentLayoutId;
            var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
            // calling RunNewApp() to open the MediaObject viewer to view UDO Notes, this scenario FormOwner is not available
            window.RunNewApp('lcmLoadMo', '0', "", 'APP', "", "", "1", "", "", "", "", "", "", "", "", "", "", "", activeLayout.omwObjectName);
        }
    }
    else {
        //get the Current Active Layout Object
        var layoutId = RIUTIL.layoutManager.currentLayoutId;
        var activeLayout = RIUTIL.getCurrentSelectedLayout(layoutId);
        RIUTIL.send("lcmLoadMo." + activeLayout.id + RIUTIL.SEP_CHAR + activeLayout.omwObjectName);
    }
}

RIUTIL.alert = function (message) {
    alert(message);
}

RIUTIL.about = function () {
    if (RIUTIL.isCompositePage) {
        var e1URL = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_SERVICE);
        e1URL.setURI(RIUTIL.FRONT_SERVLET);
        e1URL.setParameter(RIUTIL.PARAM_COMMAND, RIUTIL.AJAX_CMD_SHOW_COMPOSITE_INFO);
        e1URL.setParameter(RIUTIL.PARAM_ACTION, RIUTIL.ACTION_AJAX);
        e1URL.setParameter(RIUTIL.PARAM_CONTAINER_ID, "");
        window.open(e1URL.toString(), "", "width=400,height=435,scrollbars=1,resizable=yes,titlebar");
    }
    else {
        about("RIInfo");
    }
}

RIUTIL.exitLCMMode = function () {
    if (RIUTIL.cancelActionForDirtyCheck()) {
        RIUTIL.setPageDesignBarEnable(true);
        return;
    }
    //reset the layout manager
    RIUTIL.postAjaxRequest("", RIUTIL.PRODUCT_GENERIC_URL, RIUTIL.AJAX_CMD_CLEANUP_AFTER_LCM);
    var dta = top.document.getElementById('e1menuAppIframe').contentWindow.dta;
    //this is a hack, to bypass the mo processing in DTA submission
    dta.lcmMode = true;
    dta.doAsynPostById("exitLCMMode");
}

// private function decodes &#n; HTML chars
RIUTIL.HTMLDecoder = function (encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    if (div.firstChild != null)
        return div.firstChild.nodeValue;
    else
        return "";
}

//keep jas alive
RIUTIL.keepAlive = function () {
    //if there is user action, post to the server
    if (RIUTIL.userAction) {
        var e1URL = top._e1URLFactory.createInstance(top._e1URLFactory.URL_TYPE_SERVICE);
        e1URL.setURI('LoginVerification');
        e1URL.setParameter('KeepAlive', true);
        RIUTIL.sendXMLReq("POST", null, e1URL.toString(), RIUTIL.resetKeepAlive);
    }
}

RIUTIL.resetKeepAlive = function () {
    if (RIUTIL.layoutManager && RIUTIL.layoutManager.designMode) {
        RIUTIL.userAction = false;
        //only reset the timer, NOT the thread
        clearInterval(RIUTIL.keepAliveTimer);
        RIUTIL.keepAliveTimer = setInterval(RIUTIL.keepAlive, RIUTIL.layoutManager.keepAliveInterval);
    }
}

RIUTIL.goChangeInitialColRow = function (element) {
    element.previousSibling.value = element.value;
}

RIUTIL.goLaunchLPContextSelectionWindow = function (vaIcon) {
    var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI("ToolsVAFormService");

    var AdhocQuery = '{"condition": [{"value": [{"specialValueId": "0","content": "00"}, {"specialValueId": "0","content": "07"}],"controlId": "TASKTYPE","operator": "12"}],"autoFind": true,"matchType": 0,"autoClear": true}';

    var request =
    {
        isComposerRequest: true,
        targetName: "F9000",
        targetType: "table",
        MaxPageSize: 10,
        ReturnControlIDs: "TASKID|TASKNM|LNGTASK",
        AdhocQuery: AdhocQuery,
        showFirstColumn: false
    }
    var requestString = JSON.stringify(request);
    e1UrlCreator.setParameter("cmd", 1);
    e1UrlCreator.setParameter("formRequest", requestString);

    var rcuxDiv = document.getElementById('innerRCUX');
    var height = rcuxDiv.clientHeight;
    var width = rcuxDiv.clientWidth;

    RIUTIL.adjustUIBlockIndex(true);

    var parentFrame = RIUTIL.getFrameworkWindow();
    parentFrame.onVASelectCallback = RIUTIL.onVASelect;
    parentFrame.onVACloseCallback = RIUTIL.onVAClose;
    parentFrame.onVAResizeCallback = RIUTIL.onVAResize;

    var vaDiv = parentFrame.document.createElement('div');
    vaDiv.id = 'ToolsVAForm';
    vaDiv.className = 'popupWindow';
    vaDiv.style.position = 'absolute';
    vaDiv.style.zIndex = 200001;
    vaDiv.style.height = '380px';
    vaDiv.style.width = '420px';
    vaDiv.style.top = (height - 400) / 2 + 'px';
    vaDiv.style.left = (width - 400) / 2 + 'px';


    var vaFrame = parentFrame.document.createElement("iframe");
    vaFrame.style.height = '100%';
    vaFrame.style.width = '100%';
    vaFrame.src = e1UrlCreator.toString();

    vaDiv.appendChild(vaFrame);
    parentFrame.document.body.appendChild(vaDiv);
}

RIUTIL.adjustUIBlockIndex = function (increase) {
    var doc = RIUTIL.getFrameworkDoc();
    var UIBlockingDiv = doc.getElementById("UIBlockingDiv");
    UIBlockingDiv.style.zIndex = increase ? 100001 : 99999;
}

RIUTIL.onVAResize = function (width, height) {
    var vaDiv = RIUTIL.getFrameworkWindow().document.getElementById('ToolsVAForm');
    vaDiv.style.width = width + 'px';
    vaDiv.style.height = height + 'px';
}

RIUTIL.onVAClose = function () {
    //remove handler from parent window
    var parentFrame = RIUTIL.getFrameworkWindow();
    parentFrame.onVASelectCallback = null;
    parentFrame.onVACloseCallback = null;
    parentFrame.onVAResizeCallback = null;
    RIUTIL.adjustUIBlockIndex(false);
    var vaDiv = parentFrame.document.getElementById('ToolsVAForm');
    parentFrame.document.body.removeChild(vaDiv);
}

RIUTIL.onVASelect = function (value) {
    var doc = RIUTIL.getFrameworkDoc();
    var containerId = RIUTIL.inspectedContainerId;
    var taskInput = doc.getElementById('RIPaneIFRAME' + containerId).contentDocument.getElementById('objTaskMenu');
    if (taskInput) {
        taskInput.value = value;
    }
}

RIUTIL.LPDisabledIcons = []; //booleans to hold original icon status, save/saveAs/reserve/unreserve/requestPublish
for (var i = 0; i < 5; i++) {
    RIUTIL.LPDisabledIcons.push(false);
}