var STATE = new Object();

//Constants
STATE.CONSTANTS = new Object();
STATE.CONSTANTS.AJAX_UISTATE_MAF = 'AjaxUIState';
STATE.CONSTANTS.PARAM_ACTION = 'action';



STATE.xmlHttpRequestHandler = new Object();
STATE.xmlHttpRequestHandler.createXmlHttpRequest = function () {
    var XmlHttpRequestObject;
    if (typeof XMLHttpRequest != "undefined") {
        XmlHttpRequestObject = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        // look up the highest possible MSXML version
        var tryPossibleVersions = ["MSXML2.XMLHttp.5.0",
                          "MSXML2.XMLHttp.4.0",
                          "MSXML2.XMLHttp.3.0",
                          "MSXML2.XMLHttp",
                          "Microsoft.XMLHttp"];

        for (i = 0; i < tryPossibleVersions.length; i++) {
            try {
                XmlHttpRequestObject = new ActiveXObject(tryPossibleVersions[i]);
                break;
            }
            catch (xmlHttpRequestObjectError) {
                //ignore
            }
        }
    }
    return XmlHttpRequestObject;
}


/** 
* Returns an XMLHttpRequest object in a browser independent way.  Code grabbed from
* http://www.oracle.com/technology/pub/articles/nimphius-ajax.html
* @return an XMLHttpRequest object, or null if not available for the current browser
*/
STATE.getXMLHttpRequest = function () {
    return STATE.xmlHttpRequestHandler.createXmlHttpRequest();
}

STATE.getXMLDocument = function () {
    if (document.all)
        return new ActiveXObject("Microsoft.XMLDOM");
    else if (document.implementation)
        return document.implementation.createDocument("", "", null);
    else
        alert("Your browser has not been implemented yet. [getXMLDocument()]");
}

STATE.setXMLDocument = function (stateType, element, value) {
    var rootDoc = STATE.getXMLDocument();
    var root = rootDoc.appendChild(rootDoc.createElement(stateType));
    var cur = root.appendChild(rootDoc.createElement(element));
    cur.setAttribute('value', value);
    return rootDoc;
}

STATE.setXMLDocumentForBatch = function (stateType, properties) {
    var rootDoc = STATE.getXMLDocument();
    var root = rootDoc.appendChild(rootDoc.createElement(stateType));
    if (properties) {
        for (var i = 0; i < properties.length; i++) {
            var elem = properties[i].element;
            if (elem) {
                var cur = root.appendChild(rootDoc.createElement(elem));
                var value = properties[i].value;
                if (!value) {
                    value = '';
                }
                cur.setAttribute('value', value);
            }
        }
    }
    return rootDoc;
}

STATE.createStateUrl = function () {
    var e1UrlCreator = parent._e1URLFactory.createInstance(parent._e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI(STATE.CONSTANTS.AJAX_UISTATE_MAF);
    //    return e1UrlCreator.toStringAsync();
    return e1UrlCreator.toString();
}

STATE.createCloseUrl = function (action) {
    var e1UrlCreator = parent._e1URLFactory.createInstance(parent._e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI(STATE.CONSTANTS.AJAX_UISTATE_MAF);
    e1UrlCreator.setParameter(STATE.CONSTANTS.PARAM_ACTION, action);
    return e1UrlCreator.toString();
}

STATE.sendStateUpdate = function (doc, url) {

    if (url == null || doc == null || url == undefined || doc == undefined) {
        return; //Can't do anything if any of these values are not set
    }

    var req = STATE.getXMLHttpRequest();

    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send(doc);
}

STATE.sendCloseUpdate = function (url) {

    if (url == null || url == undefined) {
        return; //Can't do anything if any of these values are not set
    }

    var req = STATE.getXMLHttpRequest();

    req.open('POST', url, true);
    req.setRequestHeader("Content-Type", "text/xml");
    req.send();
}

function STATEPROPERTY(element, value) {
    this.element = element;
    this.value = value;
}