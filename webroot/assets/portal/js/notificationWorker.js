
var postURL = "",
    pingInterval = 300000,
    paused = false,
    timeout,
    FETCH_DATA = "cmd=0",
    debug = false;

function startNotificationThread() {
    setTimeout(function () { getNotifications(); }, 1000);
}

function getNotifications() {
    var respText = sendSyncXMLReq("POST", postURL, FETCH_DATA);

    if (paused == false) {
        if (debug) {
            console.log("Thread: Ask Notification Controller with resp " + respText);
        }
        try {
            var responseObj = JSON.parse(respText);
            if (Object.getOwnPropertyNames(responseObj.notificationReply).size == 1 && responseObj.notificationReply.idle != undefined) {
                // do nada
            }
            else {
                // only post if more than 'idle' is in the array
                self.postMessage(respText);
            }
        }
        catch (err) {
            if (debug) {
                console.log("Thread: Error in Notification Controller response");
            }
        }
    }
    else {
        if (debug) {
            console.log("Thread: Skip");
        }
    }

    timeout = setTimeout(function () { getNotifications(); }, pingInterval);
}

function sendSyncXMLReq(method, action, doc) {
    // code for IE 7, FireFox, Safari, etc.
    if (XMLHttpRequest) {
        var xmlReqObject = new XMLHttpRequest();
    }

    if (xmlReqObject != null) {
        xmlReqObject.open(method, action, false);
        xmlReqObject.send(doc);
        if (xmlReqObject.status === 200) {
            try {
                return xmlReqObject.responseText;
            }
            catch (e) { }
        }
    }
}

self.addEventListener("message", function (e) {
    var obj = JSON.parse(e.data);
    if (obj.notificationURL) {
        postURL = obj.notificationURL;
        pingInterval = obj.pingInterval;
        startNotificationThread();
    }
    else if (obj.pause == true) {
        if (debug) {
            console.log("Pausing Nofification Worker");
        }
        clearTimeout(timeout);
        paused = true;
    }
    else if (obj.pause == false) {
        if (paused == true) {
            if (debug) {
                console.log("Resuming Nofification Worker");
            }
            paused = false;
            startNotificationThread();
        }
    }
    else if (obj.reset == true) {
        //Get get notifications data immmediately.
        if (!paused) {
            clearTimeout(timeout);
            startNotificationThread();
        }
    }
}, false);


