/*
** Copyright (c) Oracle Corporation 2006. All Rights Reserved.
** author: Nan Li
*/
//the baseclass of all objects at clientside
function ClientObject() {
}

ClientObject.createSubclass = function (subclass, baseclass) {
    if (baseclass == null)
        baseclass = ClientObject;
    //inherit from baseclass
    subclass.prototype = new baseclass();
    subclass.prototype.constructor = subclass;
    subclass.superclass = baseclass.prototype;
}