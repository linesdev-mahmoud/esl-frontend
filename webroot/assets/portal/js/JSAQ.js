var AQ;
if (AQ == null) {
    AQ = new function () {
    }
    AQ.MANAGER_SERVICE = "AQManagerService";
    AQ.SEP_CHAR = "|";
    AQ.userHasCreateAuthority = false;
    AQ.userHasPublishAuthority = false;
    AQ.userHasReserveAuthority = false;
    // added flag for Modified AQ
    AQ.isModifiedAQForm = false;
    AQ.isOtherUDOPreview = false;
    AQ.isRTL = (document.documentElement.dir == "rtl");
    // The following flag allows us to move the dropdown label off the title bar and into the dropdown itself as a "header-like" option group.
    AQ.compressSelectLabel = true;
    AQ.SEP_LINE = "\n";
    AQ.ICON_OFFSET_X = AQ.isRTL ? 16 : -16;
    AQ.ICON_OFFSET_Y = 5;

    // The id of the operator list (sync with server)
    AQ.OP_STR_EQUAL = 0;
    AQ.OP_STR_NOT_EQUAL = 1;
    AQ.OP_STR_START_WITH = 2;
    AQ.OP_STR_END_WITH = 3;
    AQ.OP_STR_CONTAIN = 4;
    AQ.OP_NUM_EQUAL = 5;
    AQ.OP_NUM_NOT_EQUAL = 6;
    AQ.OP_NUM_LESS = 7;
    AQ.OP_NUM_LESS_EQUAL = 8;
    AQ.OP_NUM_GREATER = 9;
    AQ.OP_NUM_GREATER_EQUAL = 10;
    AQ.OP_BETWEEN = 11;
    AQ.OP_LIST = 12;
    AQ.OP_BLANK = 13;
    AQ.OP_NOT_BLANK = 14;
    AQ.OP_STR_LESS = 15;
    AQ.OP_STR_LESS_EQUAL = 16;
    AQ.OP_STR_GREATER = 17;
    AQ.OP_STR_GREATER_EQUAL = 18;

    // data type for the right values of a condition (sync with server)
    AQ.TYPE_NUM = 0;
    AQ.TYPE_STRING = 1;
    AQ.TYPE_JDEDATE = 2;
    AQ.TYPE_JDEUTIME = 3;

    // special value id
    // TODO: need to be translated.

    AQ.SV_LITERAL = 0;
    AQ.SV_LOGIN_USER = 1;
    AQ.SV_TODAY = 2;
    AQ.SV_TODAY_PLUS_DAY = 3;
    AQ.SV_TODAY_MINUS_DAY = 4;
    AQ.SV_TODAY_PLUS_MONTH = 5;
    AQ.SV_TODAY_MINUS_MONTH = 6;
    AQ.SV_TODAY_PLUS_YEAR = 7;
    AQ.SV_TODAY_MINUS_YEAR = 8;
    AQ.SV_RESET = 9;
    // mapping from math unit to sepcial value id
    AQ.mapMUToSvId = new Object();
    AQ.mapMUToSvId['ad'] = AQ.SV_TODAY_PLUS_DAY;
    AQ.mapMUToSvId['md'] = AQ.SV_TODAY_MINUS_DAY;
    AQ.mapMUToSvId['am'] = AQ.SV_TODAY_PLUS_MONTH;
    AQ.mapMUToSvId['mm'] = AQ.SV_TODAY_MINUS_MONTH;
    AQ.mapMUToSvId['ay'] = AQ.SV_TODAY_PLUS_YEAR;
    AQ.mapMUToSvId['my'] = AQ.SV_TODAY_MINUS_YEAR;

    // all hard coded id name and attribute name
    AQ.NAME_CONDITION_ROW = "AQConditionRow";
    AQ.NAME_VALUE_INPUT = "AQValueInput";
    AQ.NAME_INPUT_TR = "AQInputTR";
    AQ.NAME_ADD_MORE = "AQAddMoreValue";

    AQ.ID_IYFE_WIN = "AQ_IYFE";
    AQ.ID_CONTENT_TABLE = "AQContentTable";
    AQ.ID_CONDITIONS_TABLE = "AQConditionsTable";
    AQ.ID_FIELDS_SELECTION_TABLE = "AQFieldsSelectionTable";
    AQ.ID_SV_VALUE = "AQSVValue";
    AQ.ID_SV_MATH = "AQSVMath";
    AQ.ID_SV_UNIT = "AQSVUnit";
    AQ.ID_QUERY_LIST_SP = "AQPanelQueryList";
    AQ.ID_QUERY_LIST_E1 = "AQFormQueryList";
    AQ.ID_DESIGN_ICON = "AQDesignIcon";
    AQ.ID_CLEAR_ICON = "AQClearIcon";
    AQ.ID_DEFAULT_QUERY = "AQDefaultQuery";
    AQ.ID_DEFAULT_QUERY_LABEL = "AQDefaultQueryLabel";
    AQ.ID_AUTO_FIND = "AQAutoFind";
    AQ.ID_AUTO_FIND_LABEL = "AQAutoFindLabel";
    AQ.ID_AUTO_CLEAR = "AQAutoClear";
    AQ.ID_AUTO_CLEAR_LABEL = "AQAutoClearLabel";
    AQ.ID_LOGIC = "AQLogic";
    AQ.ID_SAVE_ICON = "AQSaveIcon";
    AQ.ID_SAVE_AS_ICON = "AQSaveAsIcon";
    AQ.ID_DELETE_ICON = "AQDeleteIcon";
    AQ.ID_POPULATE_QUERY = "AQPopulateIcon";
    AQ.ID_PUBLISH_ICON = "AQPublishIcon";
    AQ.ID_RESERVE_ICON = "AQReserveIcon";
    AQ.ID_NOTES_ICON = "AQNotesIcon";
    AQ.ID_MESSAGE_DIV = "AQMessageDiv";
    AQ.ID_RESERVED_BY = "AQReservedBy";
    AQ.ID_ADD_ICON = "AQAddIcon{0}";
    AQ.ID_ADD_ICON_IN_FIELD_TABLE = "AQAddIconInFieldTable{0}";
    AQ.ID_MATCH_ALL = "AQMatchAll";
    AQ.ID_MATCH_ALL_LABEL = "AQMatchAllLabel";
    AQ.ID_MATCH_ANY = "AQMatchAny";
    AQ.ID_MATCH_ANY_LABEL = "AQMatchAnyLabel";
    AQ.ID_FIELDS_SELECTION_ICON = "AQExpendFieldIcon";
    AQ.ID_FIELDS_SELECTION_TABLE = "AQExpendFieldTable";

    AQ.ATT_CONTROL_ID = "AQControlId";
    AQ.ATT_ANCHOR_CONTROL_ID = "AQAnchorId";
    AQ.ATT_AQINFOTOP = "AQInfoTop";
    AQ.ATT_DATATYPE = "AQDataType";
    AQ.ATT_ROWTITLE = "AQRowTitle";
    AQ.ATT_SVID = "AQSvId";
    AQ.ATT_CONDITION_INDEX = "AQConditionIndex";
    AQ.ATT_VALUE_INDEX = "AQValueIndex";

    // default width and height for popup dialog for special value
    AQ.SV_DIALOG_WIDTH = 220;
    AQ.SV_DIALOG_HEIGHT = 80;

    AQ.VE_DIALOG_WIDTH = 250;
    AQ.VE_DIALOG_HEIGHT = 150;

    AQ.BG_COLOR_IN_FOCUSED = "#EDDDFF";
    AQ.BG_COLOR_OUT_FOCUSED = "";
    AQ.BG_COLOR_VALIDTION_ERROR = "red";
    AQ.BG_COLOR_VALIDTION_OK = "";

    AQ.BLANK_QUERY_ID = "(new)";
    AQ.PUBLIC_QUERY_ID = "*PUBLIC";
    AQ.NAME_LENGTH = 30;
    AQ.ACCESSIBILITY = false;
    if (localStorage.getItem("userAccessibilityMode"))
        AQ.ACCESSIBILITY = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;
    AQ.BAD_CHAR_SET = "~`!@#$%^&*()+={[}]|\\;:'\"<,>.?/";  //some special char like these can not be used in UDO name" 
}

// Private method to check if AQ is in Query Management Mode
AQ.isManagementMode = function () {
    return (JSSidePanelRender.getMode() == JSSidePanelRender.MODE_QUERY)
}

// Private method to check if AQ is in Mormal Mode
AQ.isNormalMode = function () {
    return (JSSidePanelRender.getMode() == JSSidePanelRender.MODE_NORMAL)
}

// Public method called by FormRenderAdaptee during form full rendering for low interactivity
AQ.initAdvancedQueryForLow = function (namespace, constants, queryList, defaultId, query) {
    AQ.namespace = namespace;
    AQ.setDirty(false); // do we want to preserve the state after a form interconnect or VA.
    AQ.updateConstants(constants);
    AQ.updateQueryList(queryList);
    AQ.updateQueryModel(query, defaultId, false)
}

// Public method called by FormRenderAdaptee during form full rendering for high interactivity
AQ.initAdvancedQueryForHigh = function (namespace) {
    AQ.namespace = namespace;
    AQ.setDirty(false); // do we want to preserve the state after a form interconnect or VA.
    setTimeout("AQ.loadQueryListAndConstants()", 0); // use existing cache
}

// Private method to request the left operands condidates and existing query list (name and id pair) from the server
AQ.loadQueryListAndConstants = function () {
    var clearCache = AQ.isDirty();
    AQ.setDirty(false);
    AQ.send("aqloadConstants");
    AQ.send("aqloadquerylist." + clearCache);
}

// Private method to request the left operands condidates and existing query list (name and id pair) from the server
AQ.loadLeftList = function () {
    AQ.send("aqloadleftlist");
}

// Public Server Notification to update translated constants on the client side model based on the server side model
AQ.updateConstants = function (constants) {
    AQ.constants = constants;

    // Operator map from operator id to operator text
    AQ.opMap = new Object();
    AQ.opMap[AQ.OP_STR_EQUAL] = constants.AQ_STR_EQUAL;
    AQ.opMap[AQ.OP_STR_NOT_EQUAL] = constants.AQ_STR_NOT_EQUAL;
    AQ.opMap[AQ.OP_STR_START_WITH] = constants.AQ_STR_START_WITH;
    AQ.opMap[AQ.OP_STR_END_WITH] = constants.AQ_STR_END_WITH;
    AQ.opMap[AQ.OP_STR_CONTAIN] = constants.AQ_STR_CONTAIN;
    AQ.opMap[AQ.OP_STR_LESS] = constants.AQ_STR_LESS;
    AQ.opMap[AQ.OP_STR_LESS_EQUAL] = constants.AQ_STR_LESS_EQUAL;
    AQ.opMap[AQ.OP_STR_GREATER] = constants.AQ_STR_GREATER;
    AQ.opMap[AQ.OP_STR_GREATER_EQUAL] = constants.AQ_STR_GREATER_EQUAL;
    AQ.opMap[AQ.OP_NUM_EQUAL] = constants.AQ_NUM_EQUAL;
    AQ.opMap[AQ.OP_NUM_NOT_EQUAL] = constants.AQ_NUM_NOT_EQUAL;
    AQ.opMap[AQ.OP_NUM_LESS] = constants.AQ_NUM_LESS;
    AQ.opMap[AQ.OP_NUM_LESS_EQUAL] = constants.AQ_NUM_LESS_EQUAL;
    AQ.opMap[AQ.OP_NUM_GREATER] = constants.AQ_NUM_GREATER;
    AQ.opMap[AQ.OP_NUM_GREATER_EQUAL] = constants.AQ_NUM_GREATER_EQUAL;
    AQ.opMap[AQ.OP_BETWEEN] = constants.AQ_BETWEEN;
    AQ.opMap[AQ.OP_LIST] = constants.AQ_LIST;
    AQ.opMap[AQ.OP_BLANK] = constants.AQ_BLANK;
    AQ.opMap[AQ.OP_NOT_BLANK] = constants.AQ_NOT_BLANK;

    // mapping from sepcial value id to display string
    AQ.mapSvIdToDisplay = new Object();
    AQ.mapSvIdToDisplay[AQ.SV_TODAY_PLUS_DAY] = constants.AQ_TODAY_PLUS_DAY;
    AQ.mapSvIdToDisplay[AQ.SV_TODAY_MINUS_DAY] = constants.AQ_TODAY_MINUS_DAY;
    AQ.mapSvIdToDisplay[AQ.SV_TODAY_PLUS_MONTH] = constants.AQ_TODAY_PLUS_MONTH;
    AQ.mapSvIdToDisplay[AQ.SV_TODAY_MINUS_MONTH] = constants.AQ_TODAY_MINUS_MONTH;
    AQ.mapSvIdToDisplay[AQ.SV_TODAY_PLUS_YEAR] = constants.AQ_TODAY_PLUS_YEAR;
    AQ.mapSvIdToDisplay[AQ.SV_TODAY_MINUS_YEAR] = constants.AQ_TODAY_MINUS_YEAWR;

    AQ.userHasCreateAuthority = constants.createAuthority;
    AQ.userHasPublishAuthority = constants.publishAuthority;
    AQ.userHasReserveAuthority = constants.reserveAuthority;
    AQ.isOtherUDOPreview = constants.isOtherUDOPreview;
}

// Public Server Notification to update left operands list based on the server side model and open side panel
AQ.updateLeftList = function (leftOperandList) {
    AQ.leftOperandList = leftOperandList;
    AQ.sortListByProp(AQ.leftOperandList, 'display');
    var conditions = AQ.getActiveQuery().conditions;
    for (var i = 0, len = conditions.length; i < len; i++) {
        var condition = conditions[i];
        condition.readOnly = !AQ.isValueInTheList(condition.leftId, AQ.leftOperandList);
    }
    JSSidePanelRender.open(JSSidePanelRender.MODE_QUERY);
    if (typeof queryObject != 'undefined' && queryObject.isLcmPreview) {
        AQ.disableAllAQPanelIcons();
    }
}

// Private utility method to search for a value in the list, return true if found, otherwise return false
AQ.isValueInTheList = function (value, list) {
    for (var i = 0, listlen = list.length; i < listlen; i++) {
        if (value == list[i].id)
            return true;
    }
    return false;
}

// Public Server Notification to update Query List on the client side model based on the server side model
// It shall automatically request the query model from the server based on the default current selection of the query list
AQ.updateQueryList = function (queryList) {
    AQ.queryList = queryList;
    AQ.separateQueryList()
    AQ.sortListByProp(AQ.queryList.publicItems, "name");
    AQ.sortListByProp(AQ.queryList.privateItems, "name");
    AQ.sortListByProp(AQ.queryList.pendingApprovalItems, "name");
    AQ.sortListByProp(AQ.queryList.reservedItems, "name");
    AQ.sortListByProp(AQ.queryList.reworkItems, "name");
    AQ.updateControlView();
    var callLoadQueryModel = true;
    //open the Query side panel during Preview / Edit from P98220W / P98220U LCM App
    if (typeof queryObject != 'undefined' && !AQ.isManagementMode()) {
        var webObjectName = queryObject.WebObjectName;
        var isPrivateAQ = queryObject.isPrivate;
        var querySelectionChanged = true;
        if (queryObject.isLcmEdit || queryObject.isLcmPreview) {
            callLoadQueryModel = false;
            AQ.goToggleAdvancedQuery();
            AQ.send("aqloadquerymodel." + webObjectName + AQ.SEP_LINE + querySelectionChanged + AQ.SEP_LINE + AQ.isManagementMode() + AQ.SEP_LINE + isPrivateAQ);
        }
    }
    else if (typeof modifiedQueryObject != 'undefined' && modifiedQueryObject != null && !AQ.isManagementMode()) {
        AQ.isModifiedAQForm = true;
        if (modifiedQueryObject.WebObjectName == AQ.queryList.activeId) {
            var webObjectName = modifiedQueryObject.WebObjectName;
        }
        else {
            var webObjectName = AQ.queryList.activeId;
            modifiedQueryObject.WebObjectName = AQ.queryList.activeId;
        }
        var isPrivateAQ = true;
        var querySelectionChanged = true;
        callLoadQueryModel = false;
        AQ.goToggleAdvancedQuery();
        AQ.send("aqloadquerymodel." + webObjectName + AQ.SEP_LINE + querySelectionChanged + AQ.SEP_LINE + AQ.isManagementMode() + AQ.SEP_LINE + isPrivateAQ);
    }
    if (callLoadQueryModel && JDEDTAFactory.getInstance(AQ.namespace).iLevel != ILEVEL_LOW) // only need to load query model in different around trip for high interactivity
        setTimeout("AQ.loadQueryModel(false)", 0);
}

// Private method to distribute items array to publicItems and privateItems array, and we do not use items array any more after that
AQ.separateQueryList = function () {
    var items = AQ.queryList.items;
    AQ.queryList.publicItems = [];
    AQ.queryList.privateItems = [];
    AQ.queryList.pendingApprovalItems = [];
    AQ.queryList.reservedItems = [];
    AQ.queryList.reworkItems = [];

    for (var i = 0, len = items.length; i < len; i++) {
        var item = items[i];
        if (item.isPrivate) {
            if (item.isRequestPublished) {
                AQ.queryList.pendingApprovalItems.push(item);
            }
            else if (item.isRework) {
                AQ.queryList.reworkItems.push(item);
            }
            else if (item.isCheckedOut && !(item.isRequestPublished) && !(item.isRework)) {
                AQ.queryList.reservedItems.push(item);
            }
            else {
                AQ.queryList.privateItems.push(item);
            }
        }
        else {
            AQ.queryList.publicItems.push(item);
        }
    }
    AQ.queryList.items = null;
}

// Private method to sort the given query list items by alphbet order
AQ.sortListByProp = function (items, prop) {
    for (var endIndex = items.length - 1; endIndex >= 1; endIndex--) {
        for (var i = 0; i < endIndex; i++) {
            var name1 = items[i][prop];
            var name2 = items[i + 1][prop];
            if (name1 > name2) {
                var item = items[i];
                items[i] = items[i + 1];
                items[i + 1] = item;
            }
        }
    }
}

// Public Server Notification to update the active query of the client side model based on the server side model.
AQ.updateQueryModel = function (query, defaultId, querySelectionChanged) {
    AQ.setDirty(false);
    AQ.queryList.defaultId = defaultId;
    //For shared/Public AQ append "*PUBLIC" to query WOBNM, Check if "*PUBLIC" is appended to WOBNM if so don't add again
    if (!query.isPrivate && query.omwObjectName.indexOf(AQ.PUBLIC_QUERY_ID) == -1) {
        query.omwObjectName = AQ.PUBLIC_QUERY_ID + query.omwObjectName;
    }
    AQ.setActiveQuery(query);
    if (AQ.isManagementMode())// request to open side panel if it is already in query management mode
    {
        JSSidePanelRender.reqOpen(JSSidePanelRender.MODE_QUERY);
    }
    else if (querySelectionChanged) {
        // in normal mode and entry form, we can trigger auto find if setting is on. However, the server need to
        // make sure to render false for this flag when the form is opened by Form Interconnect, so we can
        // skip the following code for such use case
        // For full page render after scroll to end or OCL Swith, we do not want to trigger find a again.
        // But for query selection change action, we do want to trigger auto find.
        if (query.autoFind && query.id != AQ.BLANK_QUERY_ID) {
            AQ.triggerHotkeyForFind();
        }
    }
    AQ.clearProcInd();

    //open the WatchList Pane, when the user Clicks the "Preview Object" button in P98220W UCLM APP   WL.isManagementMode()
    if (typeof UDOPrefObject != 'undefined' && (JSSidePanelRender.mode == JSSidePanelRender.MODE_NORMAL)) {
        var watchlistOID = UDOPrefObject.WebObjectId;
        if (UDOPrefObject.isLcmEdit || UDOPrefObject.isLcmPreview) {
            WL.goToggleWatchlist();
            WL.goFetchWatchlistById(watchlistOID);
            WL.send("wlloadwatchlist." + watchlistOID);
        }
    }
}

AQ.triggerHotkeyForFind = function () {
    // simulate cotrl alt I for find hotkey
    var rootContainer = JSCompMgr.getRootContainer(AQ.namespace);
    var activeElement = document.getElementById(AQ.ID_QUERY_LIST_E1);
    rootContainer.processHotkey(AQ.namespace, activeElement, 73, true, true, false); // trigger ctrl_alt_I
}

// Private method to retrieve user selected query id in the query list
// There are two query list on the form, but only one of them can be interacted by the end user depend on the current mode.
// we only need to get the current selection from the active query list and sychrnoize the other one.
AQ.getCurrentSelectedQueryId = function () {
    var id = AQ.isManagementMode() ? AQ.ID_QUERY_LIST_SP : AQ.ID_QUERY_LIST_E1;
    var queryList = document.getElementById(id);
    var queryOBNM;
    // Query drop-down might be hidden based on AQ security, so check if Query drop-down exists before performing any operation on it
    if (queryList) {
        queryOBNM = queryList.options[queryList.options.selectedIndex].value;
    }
    return queryOBNM;
}

// Private method to check if we are current working with the blank query.
AQ.isBlankQuery = function () {
    return (AQ.activeQuery.id == AQ.BLANK_QUERY_ID)
}

// Private method to update view for Advanced Saved Query Control
AQ.updateControlView = function () {
    AQ.updateQuerySelectTag(AQ.ID_QUERY_LIST_E1, JSSidePanelRender.MODE_QUERY);
    AQ.updateControlEnablement();
    AQ.updateAQFormHeaderButton();
    var doc = document;
    if (AQ.isOtherUDOPreview) {
        var AQFormQuerylList = doc.getElementById(AQ.ID_QUERY_LIST_E1);
        var AQDesignIcon = doc.getElementById(AQ.ID_DESIGN_ICON);
        if (AQFormQuerylList) {
            AQFormQuerylList.disabled = true;
        }
        AQ.disableIcon(AQ.ID_DESIGN_ICON, window["E1RES_share_images_ulcm_AQ_designdis_png"], null);
        AQDesignIcon.style.cursor = "default";
    }
    if (typeof queryObject != 'undefined' && queryObject.isLcmPreview) {
        var AQFormQuerylList = doc.getElementById(AQ.ID_QUERY_LIST_E1);
        if (AQFormQuerylList) {
            AQFormQuerylList.disabled = true;
        }
    }
}

// private method to update the form header button
AQ.updateAQFormHeaderButton = function () {
    var doc = document;
    var AQFormQueryList = doc.getElementById(AQ.ID_QUERY_LIST_E1);
    var AQFormHeaderButton = doc.getElementById(AQ.ID_DESIGN_ICON);
    // Query drop-down might be hidden based on AQ security
    if (AQFormQueryList) {
        AQFormQueryList.title = AQ.constants.JH_SELECTAQUERY;
        AQFormQueryList.previousSibling.innerHTML = AQ.constants.AQ_LABEL_QUERY_LIST;
    }
    if (AQFormHeaderButton) // AQ Icon might be hidden based on AQ security
    {
        AQFormHeaderButton.alt = AQ.constants.AQ_BUTTON_HOVER;
        AQFormHeaderButton.title = AQ.constants.AQ_BUTTON_HOVER;
    }
}

// Private method to update the enablement of Advanced Saved Query Control on the view
AQ.updateControlEnablement = function () {
    var disabled = AQ.isManagementMode();
    var doc = document;
    var AQFormQuerylList = doc.getElementById(AQ.ID_QUERY_LIST_E1);
    // Query drop-down might be hidden based on AQ security
    if (AQFormQuerylList) {
        AQFormQuerylList.disabled = disabled;
    }
    var AQDesignIcon = doc.getElementById(AQ.ID_DESIGN_ICON);
    var AQCleanIcon = doc.getElementById(AQ.ID_CLEAR_ICON);
    if (AQDesignIcon) // AQ Icon might be hidden based on AQ security
        if (JDEDTAFactory.getInstance(AQ.namespace).iLevel == ILEVEL_LOW) // do not allow design and create query for low interactivity
        {
            //JSSidePanelRender.setMotionImage(AQDesignIcon, window["E1RES_img_jdequery_div_png"], null, null);
            AQDesignIcon.disabled = true;
        }
        else {
            JSSidePanelRender.setMotionImage(AQDesignIcon, window['E1RES_img_jdequery_ena_png'], window['E1RES_img_jdequery_hov_png'], null);
            if (AQCleanIcon)
                JSSidePanelRender.setMotionImage(AQCleanIcon, window['E1RES_img_jdeclear_ena_png'], window['E1RES_img_jdeclear_hov_png'], null);
            AQDesignIcon.disabled = false;
        }

    if (AQ.isManagementMode()) {
        if (AQ.activeQuery.notes) {
            AQ.enableIcon(AQ.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_ena_png"], window['E1RES_share_images_ulcm_notes_hov_png'], AQ.showNotes, AQ.constants.UDO_NOTES);
        }
        else {
            AQ.disableIcon(AQ.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], AQ.constants.UDO_NOTES);
        }
        if (AQ.activeQuery.isPrivate) {
            if (AQ.activeQuery.id != AQ.BLANK_QUERY_ID) {
                // AQ is in reserved section
                if (AQ.activeQuery.isCheckedOut && !(AQ.activeQuery.isRequestPublished) && !(AQ.activeQuery.isRework)) {
                    if (AQ.userHasPublishAuthority && AQ.userHasReserveAuthority) {
                        AQ.enableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], AQ.goSaveQuery, AQ.constants.UDO_SAVE);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
                        AQ.disableCtrls();
                    }
                    if (AQ.activeQuery.canCopy && AQ.activeQuery.canAddToProject) {
                        AQ.enableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], AQ.goSaveAsQueryBegin);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], AQ.constants.UDO_SAVE_AS);
                    }
                    if (AQ.activeQuery.canUnReserve && AQ.activeQuery.canDelete && AQ.activeQuery.canRemoveFromProject) {
                        AQ.enableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_unreserve_ena_png"], window['E1RES_share_images_ulcm_unreserve_hov_png'], AQ.goReserveQuery, AQ.constants.UDO_UNRESERVE);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_unreserve_dis_png"], AQ.constants.UDO_UNRESERVE);
                    }
                    if (AQ.userHasPublishAuthority) {
                        AQ.enableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_ena_png"], window['E1RES_share_images_ulcm_requestpublish_hov_png'], AQ.goRequestPublishQueryBegin, AQ.constants.UDO_REQUEST_PUBLISH);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], AQ.constants.UDO_REQUEST_PUBLISH);
                    }
                    AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                }
                // AQ in Pending Promotion section/Rework
                else if (AQ.activeQuery.isRequestPublished || AQ.activeQuery.isRework) {
                    AQ.disableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
                    AQ.disableCtrls();
                    if (AQ.activeQuery.canCopy && AQ.activeQuery.canAddToProject) {
                        AQ.enableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], AQ.goSaveAsQueryBegin);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], AQ.constants.UDO_SAVE_AS);
                    }
                    if (AQ.activeQuery.canCheckout && AQ.userHasPublishAuthority && AQ.userHasReserveAuthority) {
                        AQ.enableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_ena_png"], window['E1RES_share_images_ulcm_reserve_hov_png'], AQ.goReserveQuery, AQ.constants.UDO_RESERVE);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], AQ.constants.UDO_RESERVE);
                    }
                    AQ.disableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], AQ.constants.UDO_REQUEST_PUBLISH);
                    AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                }
                // AQ is in Personal section
                else {
                    AQ.enableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], AQ.goSaveQuery, AQ.constants.UDO_SAVE);
                    if (AQ.activeQuery.canCopy && AQ.activeQuery.canAddToProject) {
                        AQ.enableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], AQ.goSaveAsQueryBegin, AQ.constants.UDO_SAVE_AS); //enable saveAs Icon
                    }
                    else {
                        AQ.disableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], AQ.constants.UDO_SAVE_AS);
                    }
                    AQ.disableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], AQ.constants.UDO_RESERVE);
                    if (AQ.userHasPublishAuthority) {
                        AQ.enableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_ena_png"], window['E1RES_share_images_ulcm_requestpublish_hov_png'], AQ.goRequestPublishQueryBegin, AQ.constants.UDO_REQUEST_PUBLISH);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], AQ.constants.UDO_REQUEST_PUBLISH);
                    }
                    if (AQ.activeQuery.canDelete && AQ.activeQuery.canRemoveFromProject) {
                        AQ.enableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_ena_png"], window['E1RES_share_images_ulcm_delete_hov_png'], AQ.goDeleteQuery, AQ.constants.UDO_DELETE);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"], AQ.constants.UDO_DELETE);
                    }
                }
            }
            else {
                // AQ is selected with "(new)" option/default
                //If Modified AQ, always enable save icons.
                if (AQ.isModifiedAQForm) {
                    AQ.enableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], AQ.goSaveModifiedQuery);
                    var queryOBNM = AQ.getActiveQueryId();
                    if (queryOBNM == AQ.BLANK_QUERY_ID)
                        AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"], AQ.constants.UDO_DELETE);
                    else
                        AQ.enableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_ena_png"], window['E1RES_share_images_ulcm_delete_hov_png'], AQ.goDelModifiedQuery, AQ.constants.UDO_DELETE);
                }
                else // Normal AQ
                {
                    if (AQ.activeQuery.canAdd && AQ.activeQuery.canAddToProject) {
                        AQ.enableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_ena_png"], window['E1RES_share_images_ulcm_save_hov_png'], AQ.goSaveAsQueryBegin);
                    }
                    else {
                        AQ.disableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
                        AQ.disableCtrls();
                    }
                    AQ.disableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], AQ.constants.UDO_SAVE_AS); // disable saveAs Icon
                    AQ.disableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], AQ.constants.UDO_RESERVE);
                    AQ.disableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], AQ.constants.UDO_REQUEST_PUBLISH);
                    AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
                }
            } //else close.
        } //end of private section
        else {
            // AQ is in PUBLIC section
            AQ.disableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
            AQ.disableCtrls();
            if (AQ.activeQuery.canCopy && AQ.activeQuery.canAddToProject) {
                AQ.enableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_ena_png"], window['E1RES_share_images_ulcm_saveas_hov_png'], AQ.goSaveAsQueryBegin, AQ.constants.UDO_SAVE_AS);
            }
            else {
                AQ.disableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], AQ.constants.UDO_SAVE_AS);
            }
            if (!AQ.activeQuery.isCheckedOut && AQ.activeQuery.canCheckout && AQ.userHasPublishAuthority && AQ.userHasReserveAuthority) {
                AQ.enableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_ena_png"], window['E1RES_share_images_ulcm_reserve_hov_png'], AQ.goReserveQuery, AQ.constants.UDO_RESERVE);
            }
            else {
                AQ.disableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], AQ.constants.UDO_RESERVE);
            }
            AQ.disableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], AQ.constants.UDO_REQUEST_PUBLISH);
            AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
        }
    }
    GUTIL.setFocus(doc.getElementById(AQ.ID_QUERY_LIST_SP), true);
    // Disable : In modified AQ case the combo box should be always disable, as it always be one query.
    if (AQ.isModifiedAQForm) {
        AQ.disableQueryListComboForModAQ();
    }

}

// Private method to update the view of the one of two query list based on the model given the id of the select tag
// This ia also called from JSWL.js using different input mode
AQ.updateQuerySelectTag = function (selectTagId, mode) {
    // empty the select tag first
    var selectTag = document.getElementById(selectTagId);
    var feature = JSSidePanelRender.getFeatureModel(mode);
    if (selectTag) {
        JSSidePanelRender.removeAllChildren(selectTag);
        selectTag.onchange = feature.goChangeQueryList;

        // With hidden labels for header dropdown boxes, use a group option as an indicator of what the content represents.
        if (isIOS && feature.compressSelectLabel) {
            AQ.createOptionGroupTag(selectTag, AQ.constants.AQ_LABEL_QUERY_LIST);
        }
    }

    var selectedIndex = 0;

    // populate private items
    var privateItems = AQ.queryList.privateItems;
    if (privateItems.length > 0) {
        var privateOptionGroupTag = AQ.createOptionGroupTag(selectTag, AQ.constants.UDO_PERSONAL);
        selectedIndex = AQ.populateOptions(privateOptionGroupTag, privateItems, selectedIndex, mode, true);
    }

    // populate requestPublish (OR) pendingApproval items
    var pendingApprovalItems = AQ.queryList.pendingApprovalItems;
    if (pendingApprovalItems.length > 0) {
        var pendingApprovalOptionGroupTag = AQ.createOptionGroupTag(selectTag, AQ.constants.UDO_PENDING_APPROVAL);
        selectedIndex = AQ.populateOptions(pendingApprovalOptionGroupTag, pendingApprovalItems, selectedIndex, mode, true);
    }

    // populate rework items
    var reworkItems = AQ.queryList.reworkItems;
    if (reworkItems.length > 0) {
        var reworkOptionGroupTag = AQ.createOptionGroupTag(selectTag, AQ.constants.UDO_REWORK);
        selectedIndex = AQ.populateOptions(reworkOptionGroupTag, reworkItems, selectedIndex, mode, true);
    }

    // populate reserved items
    var reservedItems = AQ.queryList.reservedItems;
    if (reservedItems.length > 0) {
        var reservedOptionGroupTag = AQ.createOptionGroupTag(selectTag, AQ.constants.UDO_RESERVED);
        selectedIndex = AQ.populateOptions(reservedOptionGroupTag, reservedItems, selectedIndex, mode, false);
    }

    // populate public items
    var publicItems = AQ.queryList.publicItems;
    if (publicItems.length > 0) {
        var publicOptionGroupTag = AQ.createOptionGroupTag(selectTag, AQ.constants.UDO_SHARED);
        selectedIndex = AQ.populateOptions(publicOptionGroupTag, publicItems, selectedIndex, mode, true);
    }

    // set the selected query in the list
    if (selectTag) {
        selectTag.selectedIndex = selectedIndex;
    }
    if (selectTagId == AQ.ID_QUERY_LIST_SP && mode == (JSSidePanelRender.MODE_QUERY || JSSidePanelRender.MODE_WATCHLIST))
        GUTIL.setFocus(selectTag, true);
}

// Private method to populate the items into option group tag as children options.
// selectedIndex is passed in and may be updated if the active query id is in the list.
// indexOffset is the previous total number of items in the list
AQ.populateOptions = function (optionGroupTag, items, selectedIndex, mode, allowDefault) {
    var activeQueryOBNM = AQ.getActiveQueryId();
    var selectTag = optionGroupTag.parentNode;
    var indexOffset = selectTag.options.length;
    // default the selection
    for (var i = 0, len = items.length; i < len; i++) {
        var item = items[i];
        var queryId = item.id;
        var queryOBNM = item.omwObjectName;
        var queryOBNMCopy = queryOBNM;
        var isPrivate = item.isPrivate;
        //append the *PUBLIC to the WOBNM for Public AQ
        if (!isPrivate) {
            queryOBNM = AQ.PUBLIC_QUERY_ID + queryOBNM;
        }
        if (queryOBNM == activeQueryOBNM) {
            selectedIndex = i + indexOffset;
        }

        if (queryId == AQ.BLANK_QUERY_ID) {
            if (mode == JSSidePanelRender.MODE_WATCHLIST)
                var display = WL.constants.WL_NO_QUERY;
            // For ModifiedAQ scenario set the passed in AQ name as the default query to be loaded      
            else if (AQ.isModifiedAQForm && item.name)
                var display = item.name;
            else
                var display = (AQ.ID_QUERY_LIST_E1 == selectTag.id) ? AQ.constants.AQ_ALL_RECORDS : AQ.constants.UDO_CREATE;
        }
        else
            var display = item.name;
        // Below changes are required to mark the Default Shared Query with * in front of Query name
        if (!isPrivate && queryOBNMCopy != null && queryOBNMCopy.substring(0, 7) == AQ.PUBLIC_QUERY_ID) {
            //remove the "*PUBLIC" from the query WOBNM 
            queryOBNMCopy = queryOBNMCopy.substring(7, queryOBNM.length);
        }
        if ((queryOBNM == AQ.queryList.defaultId || (queryOBNMCopy == AQ.queryList.defaultId && !isPrivate)) && queryOBNM != AQ.BLANK_QUERY_ID && allowDefault) {
            //In case of Modified AQ, Do not append * in front of default query
            if (!AQ.isModifiedAQForm)
                display = "*" + display;
        }

        //use query OBNM 
        var option = AQ.createOptionTag(optionGroupTag, queryOBNM, display);
    }
    return selectedIndex;
}

// Private utiliy method to create option group tag under given select tag with given group label name
// return the new created option group tag
AQ.createOptionGroupTag = function (selectTag, groupLablel) {
    var optionGroupTag = document.createElement("optgroup");
    optionGroupTag.label = groupLablel;
    if (selectTag)
        selectTag.appendChild(optionGroupTag);
    return optionGroupTag;
}

// Private utiliy method to create option tag under given option group tag with given value and display
// return the new created option tag
AQ.createOptionTag = function (optionGroupTag, optionValue, optionDisplay) {
    var option = document.createElement("option");
    option.value = optionValue;
    option.appendChild(document.createTextNode(optionDisplay));
    optionGroupTag.appendChild(option);
    return option;
}

// Private method to updatre the selection of the existing query list based on current active query in the model
AQ.updateQueryListSelection = function (selectTag) {
    var activeQueryId = AQ.getActiveQueryId();
    var options = selectTag.options;
    for (var i = 0; i < options.length; i++) {
        var option = options[i];
        if (option.value == activeQueryId) {
            selectTag.selectedIndex = i;
            return;
        }
    }
}

// Private method to update all UI of Query Panel (inside of Side Panel) based on the model
AQ.updateQueryView = function () {
    AQ.updateQuerySelectTag(AQ.ID_QUERY_LIST_SP, JSSidePanelRender.MODE_QUERY);
    var query = AQ.getActiveQuery();
    var queryOBNM = query.omwObjectName;
    var isReservedQuery = false;
    var isDefaultQuery = false;
    var doc = document;
    // Below changes are required to mark the Default check-box for Shared Query
    if (!query.isPrivate) {
        if (queryOBNM != null && queryOBNM.substring(0, 7) == AQ.PUBLIC_QUERY_ID) {
            //remove the "*PUBLIC" from the query WOBNM 
            queryOBNM = queryOBNM.substring(7, queryOBNM.length);
        }
    }
    // Do not mark the Default Query checkbox for a Reserved Private Query
    else if (query.isCheckedOut && !query.isRequestPublished && !query.isRework) {
        isReservedQuery = true;
    }
    // update default query checkbox
    // A new query on a ModifiedAQ Data Selection Form is always default query
    if (AQ.isModifiedAQForm && queryOBNM == AQ.BLANK_QUERY_ID) {
        isDefaultQuery = true;
    }
    else {
        isDefaultQuery = ((AQ.queryList.defaultId == query.omwObjectName && !isReservedQuery) || (AQ.queryList.defaultId == queryOBNM && !query.isPrivate));
    }

    var elem = doc.getElementById(AQ.ID_DEFAULT_QUERY);
    // add null check
    if (elem)
        elem.checked = isDefaultQuery;

    // update auto find checkbox
    var isAutoFind = query.autoFind;
    var elem = doc.getElementById(AQ.ID_AUTO_FIND);
    // add null check 
    if (elem)
        elem.checked = isAutoFind;

    // update auto clear checkbox
    var isAutoClear = query.autoClear;
    var elem = doc.getElementById(AQ.ID_AUTO_CLEAR);
    // add null check 
    if (elem)
        elem.checked = isAutoClear;

    // update logic radio button
    var isLogicAnd = query.isLogicAnd;
    var elems = doc.getElementsByName(AQ.ID_LOGIC);
    if (elems) {
        elems[0].checked = isLogicAnd;
        elems[1].checked = !isLogicAnd;
    }

    // update save icon and delete icon
    var saveIcon = doc.getElementById(AQ.ID_SAVE_ICON);
    if (saveIcon && query.isPrivate) {
        saveIcon.style.display = "";
    }
    else {
        AQ.disableIcon(AQ.ID_SAVE_ICON, window["E1RES_img_WL_savedis_png"]);
    }
    var deleteIcon = doc.getElementById(AQ.ID_DELETE_ICON);
    if (deleteIcon && query.isPrivate) {
        deleteIcon.style.display = "";
    }
    else {
        AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
    }
    AQ.showReservedBy();

    // update condition list
    if (query.conditions.length == 0)
        return;

    for (var conditionIndex = 0; conditionIndex < query.conditions.length; conditionIndex++)
        AQ.updateQueryViewForAdd(conditionIndex);

    AQ.updateValidation(null);
}

// Public Server Notification to add a new condition to both model and view
AQ.addCondition = function (condition) {
    var conditionIndex = AQ.addConditionToModel(condition);
    AQ.updateQueryViewForAdd(conditionIndex);
}

// Private method to add a new condtion into the model
AQ.addConditionToModel = function (condition) {
    var conditions = AQ.getActiveQuery().conditions;
    var conditionIndex = conditions.length;
    conditions[conditionIndex] = condition;
    return conditionIndex;
}

// Private method to update the view after adding a new condition into the model
// Table AQConditionTable - TR(AQConditionRow) - TD (removeIcon)
//                                             - TD (left)
//                                             - TD - SELECT (operator)
//                                             - TD - TABLE (valueListTable)
AQ.updateQueryViewForAdd = function (conditionIndex) {
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var leftId = condition.leftId;
    var operatorId = condition.operatorId;
    var operatorList = condition.operatorList;
    var readOnly = condition.readOnly;
    var doc = document;

    var table = doc.getElementById(AQ.ID_CONDITIONS_TABLE);
    if (AQ.ACCESSIBILITY) {
        table.setAttribute("role", "presentation");
    }
    var newRow = table.insertRow(-1);
    newRow.setAttribute(AQ.ATT_CONTROL_ID, leftId);
    newRow.setAttribute(AQ.ATT_CONDITION_INDEX, conditionIndex);
    newRow.setAttribute(AQ.ATT_DATATYPE, condition.dataType);
    newRow.setAttribute(AQ.ATT_ROWTITLE, condition.display);
    newRow.vAlign = "top";
    newRow.style.height = "24px";
    newRow.style.width = "100%";
    newRow.name = AQ.NAME_CONDITION_ROW;

    var isSaveEnabled = AQ.isSaveEnabled();

    // remove condition icon
    var removeCell = newRow.insertCell(-1);
    var removeIcon = doc.createElement("img");

    var rowIndex = table.firstChild.childNodes.length - 1;
    removeIcon.rowIndex = rowIndex;
    if (readOnly || !isSaveEnabled) {
        removeIcon.src = window['E1RES_share_images_alta_func_close_small_16_dis_png'];
        removeIcon.disabled = true;
    }
    else {
        JSSidePanelRender.setMotionImage(removeIcon, window['E1RES_share_images_alta_func_close_small_16_ena_png'], window['E1RES_share_images_alta_func_close_small_16_hov_png'], AQ.goRemoveCondition);
    }

    if (AQ.ACCESSIBILITY) {
        var removeIconLink = document.createElement("a");
        removeIconLink.onclick = AQ.goRemoveCondition;
        removeCell.appendChild(removeIconLink);
        removeIconLink.appendChild(removeIcon);
        removeIcon.alt = AQ.constants.AQ_LABEL_REMOVE_ICON_PREFIX + " " + AQ.retrieveAQRowTitle(removeIcon) + " " + AQ.constants.AQ_LABEL_REMOVE_ICON_SUFFIX;
    }
    else {
        removeCell.appendChild(removeIcon);
    }

    // left Operant
    var leftOperant = newRow.insertCell(-1);
    leftOperant.innerHTML = condition.display;

    // Comprison Operator
    var operatorCell = newRow.insertCell(-1);
    var operatorSelectTag = AQ.createOperatorSelectTag(operatorList, operatorId);
    if (readOnly || !isSaveEnabled) {
        operatorSelectTag.disabled = true;
    }
    else {
        operatorSelectTag.onfocus = AQ.goFocusCondition;
        operatorSelectTag.onblur = AQ.goBlurCondition;
        operatorSelectTag.onchange = AQ.goChangeOperator;
    }
    if (AQ.ACCESSIBILITY) {
        var operatorSelectTagId = "Operator" + conditionIndex;
        operatorSelectTag.setAttribute("id", operatorSelectTagId);
        var operatorSelectTagLabel = document.createElement("label");
        operatorSelectTagLabel.setAttribute("for", operatorSelectTagId);
        operatorSelectTagLabel.className = "accessibility";
        operatorCell.appendChild(operatorSelectTagLabel);
        operatorSelectTagLabel.innerHTML = AQ.constants.AQ_LABEL_COMPARISON_OPERATOR + " " + AQ.retrieveAQRowTitle(operatorSelectTagLabel);
    }
    operatorCell.appendChild(operatorSelectTag);

    // Right Operant literal value or list value only
    var rightOperandTR = newRow.insertCell(-1);
    var valueListTable = document.createElement("table");

    if (readOnly || !isSaveEnabled)
        valueListTable.disabled = true;

    valueListTable.className = "AQValueLayoutTable";
    valueListTable.setAttribute('cellPadding', 0);
    valueListTable.setAttribute('cellSpacing', 0);

    rightOperandTR.appendChild(valueListTable);
    AQ.updateQueryViewForRightValues(conditionIndex);
}

// Public Server Notification to update the existing condition given condition index and condition object
// both model and view need to be updated.
AQ.updateCondition = function (conditionIndex, condition) {
    AQ.updateConditionInModel(conditionIndex, condition);
    AQ.updateQueryViewForUpdate(conditionIndex);
}

// Public Server Notification to remove the existing condition given condition index and condition object
AQ.removeCondition = function (conditionIndex) {
    AQ.getActiveQuery().conditions.splice(conditionIndex, 1);
    AQ.updateQueryViewForRemove(conditionIndex);
}

// Private method to update the condition in the model
AQ.updateConditionInModel = function (conditionIndex, condition) {
    AQ.getActiveQuery().conditions[conditionIndex] = condition;
}

// Private method to update the view (condition only) after model update for a certain condition
AQ.updateQueryViewForUpdate = function (conditionIndex) {
    // update left id
    // update operator id
    // update right values only for now
    AQ.updateQueryViewForRightValues(conditionIndex);
}

// Private method to update the right values UI for a given condition index based on the model
AQ.updateQueryViewForRightValues = function (conditionIndex) {
    var valueListTable = AQ.getValueListTable(conditionIndex);

    if (!valueListTable)
        return;

    if (valueListTable.firstChild)
        JSSidePanelRender.removeAllChildren(valueListTable.firstChild);

    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    switch (parseInt(condition.operatorId)) {
        // two right values 
        case AQ.OP_BETWEEN:
            AQ.appendInputTR(conditionIndex, 0);
            AQ.appendInputTR(conditionIndex, 1);
            break;

        // mutiple right values 
        case AQ.OP_LIST:
            var rightOperand = condition.rightOperand
            for (var i = 0; i < rightOperand.length; i++) {
                AQ.appendInputTR(conditionIndex, i);
            }
            AQ.appendAddMoreTR(conditionIndex);
            break;

        // not right values 
        case AQ.OP_BLANK:
        case AQ.OP_NOT_BLANK:
        case AQ.OP_IS_USER:
        case AQ.OP_IS_TODAY:
            break;

        default:
            AQ.appendInputTR(conditionIndex, 0);
            break
    }
}

// Public Server Notification to update the user input focus on the panel so that it help user continue to work on the form after each view update.
// we also need to go over all right value to set validation color for these invalid values which is verified or not in current condtion.
AQ.updateFocus = function (focusedConditionIndex, focusedValueIndex) {
    var rigthValueInput = AQ.getRightValueInputTag(focusedConditionIndex, focusedValueIndex);

    if (rigthValueInput)
        GUTIL.setFocus(rigthValueInput, true);

    AQ.updateValidation(focusedConditionIndex, true);
}

// Private method all validation error on the view given the current condition index
AQ.updateValidation = function (currentCondition, ignoreWhiteSpace) {
    var conditions = AQ.getActiveQuery().conditions;
    var errorExists = false;
    for (var conditionIndex = 0; conditionIndex < conditions.length; conditionIndex++) {
        var rightOperand = conditions[conditionIndex].rightOperand;
        if (rightOperand) {
            for (var valueIndex = 0; valueIndex < rightOperand.length; valueIndex++) {
                var rightValue = rightOperand[valueIndex];
                if (ignoreWhiteSpace == undefined || (ignoreWhiteSpace && !isEmptyString(rightValue.value))) {
                    if (conditionIndex != currentCondition) // set verify flags for all condtions which is not current condition
                        rightValue.verified = true;

                    var inputTag = AQ.getRightValueInputTag(conditionIndex, valueIndex);
                    if (inputTag) {
                        var displayValidationError = AQ.shallDisplayValidationError(rightValue);
                        if (displayValidationError && !errorExists)
                            errorExists = displayValidationError;
                        if (AQ.ACCESSIBILITY) {
                            var doc = document;
                            var elemLabelId = "label" + inputTag.id;
                            if (doc.getElementById(elemLabelId) != null) {
                                doc.getElementById(elemLabelId).innerHTML = doc.getElementById(elemLabelId).title + " " + rightValue.errorDescription;
                            }
                        }
                        inputTag.style.borderColor = displayValidationError ? AQ.BG_COLOR_VALIDTION_ERROR : AQ.BG_COLOR_VALIDTION_OK;
                    }
                }
            }
        }
    }
    return errorExists;
}

// private method to decide if we should display validation error or not
AQ.shallDisplayValidationError = function (rightValue) {
    return !rightValue.valid && rightValue.verified;
}

// Private method to mark the current active query as dirty whenever some change is made
AQ.setDirty = function (dirty) {
    AQ.dirty = dirty;
}

// Public method to check if the current active query is already modified without save
AQ.isDirty = function () {
    return AQ.dirty;
}

// Public Event Handler for mouse moving over a condition
AQ.goFocusCondition = function (e) {
    AQ.destoryErrorWindow();
    AQ.updateHighlightCondition(e, true);
    var elem = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(elem);
    var conditionIndex = infoObj.conditionIndex;
    AQ.updateValidation(conditionIndex, true);

    // display in your face error popup window for the elem contain error
    var valueIndex = infoObj.valueIndex;
    if (valueIndex != null) {
        var condition = AQ.getActiveQuery().conditions[conditionIndex];
        var rightValue = condition.rightOperand[valueIndex];
        if (AQ.shallDisplayValidationError(rightValue) && rightValue.value != null && rightValue.value != "") {
            AQ.showErrorWindow(elem, rightValue.errorDescription);
        }
    }
}

// private method to popup a IYFE window given the focused input element and error description
AQ.showErrorWindow = function (errorElem, errorDescription) {
    if (!window.inyfeHandler)
        return;

    var winTop = getAbsoluteTopPos(errorElem, true);
    var winLeft = getAbsoluteLeftPos(errorElem, true);
    var winWidth = AQ.VE_DIALOG_WIDTH;
    var winHeight = AQ.VE_DIALOG_HEIGHT;

    var sb = new PSStringBuffer();
    sb.append("<div class='popupErrorText'>").append(errorDescription).append("</div>");
    var framework = sb.toString();

    var eventListener = {};
    eventListener.onClose = function (e) {
        //alert('destory')
        AQ.iyfeWin.destory();
        AQ.iyfeWin = null;
    }
    //alert('createPopup')
    AQ.iyfeWin = createPopup(AQ.ID_IYFE_WIN, ADVANCED_QUERY, true, false, true, true, AQ.constants.AQ_TITLE_IYFE_DIALOG, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, errorElem, framework, eventListener);
}

// private method to destory error window
AQ.destoryErrorWindow = function () {
    if (!window.inyfeHandler)
        return;

    if (AQ.iyfeWin)
        AQ.iyfeWin.onClose();
}

// Public Event Handler for mouse moving out of a condition
AQ.goBlurCondition = function (e) {
    AQ.destoryErrorWindow();
    AQ.updateHighlightCondition(e, false);

    var elem = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(elem);
    var conditionIndex = infoObj.conditionIndex;
    var valueIndex = infoObj.valueIndex;
    if (valueIndex == null)
        return;

    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var rightValue = condition.rightOperand[valueIndex];
    if (!rightValue.verified) {
        rightValue.verified = true;
        AQ.goChangeRightValue(e, true);
    }
}

// Private method to update the highlight indication for the focused condition
AQ.updateHighlightCondition = function (e, highlighted) {
    var elem = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(elem);
    var conditionIndex = infoObj.conditionIndex;
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var controlId = condition.leftId;

    if (highlighted) {
        AQ.updateHighlightConditionTR(AQ.lastHightedConditionIndex, AQ.BG_COLOR_OUT_FOCUSED);
        AQ.updateHighlightConditionTR(conditionIndex, AQ.BG_COLOR_IN_FOCUSED);
        AQ.lastHightedConditionIndex = conditionIndex;

        AQ.updateHighlightConditionControl(AQ.lastHightedControlId, AQ.BG_COLOR_OUT_FOCUSED);
        AQ.updateHighlightConditionControl(controlId, AQ.BG_COLOR_IN_FOCUSED);
        AQ.lastHightedControlId = controlId;
    }
    else {
        AQ.updateHighlightConditionTR(conditionIndex, AQ.BG_COLOR_OUT_FOCUSED);
        AQ.lastHightedConditionIndex = null;

        AQ.updateHighlightConditionControl(controlId, AQ.BG_COLOR_OUT_FOCUSED);
        AQ.lastHightedControlId = null;
    }
}

// Private method to update the highlight indication for a condition
AQ.updateHighlightConditionTR = function (conditionIndex, color) {
    if (conditionIndex == null)
        return;

    var conditionRow = AQ.getCondtionRow(conditionIndex);
    if (conditionRow == null)
        return;

    conditionRow.style.backgroundColor = color;
}

// Private method to update the highlight indication for a control
AQ.updateHighlightConditionControl = function (controlId, color) {
    if (controlId == null)
        return;

    var inputTag = AQ.getInputTagById(controlId);
    if (inputTag == null)
        return;

    inputTag.style.backgroundColor = color;
}

// Private utility method to search up dom tree for a element with a given name
// return null if fail to find.
AQ.searchUpElem = function (elem, name) {
    while (elem && elem.name != name && elem != elem.parentNode)
        elem = elem.parentNode;
    return elem;
}

// Private utility method to search down dom tree for a element with a given name
// it only search for the first child of each node only.
// return null if not found
AQ.searchDownElem = function (elem, name) {
    while (elem && elem.name != name)
        elem = elem.firstChild;
    return elem;
}

// Private utility method to find the child index in the parent element
AQ.getIndexInParent = function (elem) {
    var parent = elem.parentNode;
    for (var i = 0; i < parent.childNodes.length; i++) {
        if (parent.childNodes[i] == elem)
            return i;
    }
    return -1;
}

// Private method to check if a given id in leftOperatorList
AQ.isInLeftOperandList = function (id) {
    var leftOperandList = AQ.leftOperandList;
    for (var i = 0; i < leftOperandList.length; i++) {
        if (leftOperandList[i].id == id) {
            return true;
        }
    }
    return false;
}

// Private utility method to render a new select tag given the list of operator id and selected operator id
AQ.createOperatorSelectTag = function (list, selectedValue) {
    var selectTag = document.createElement("select");

    var length = list.length;
    for (var i = 0; i < length; i++) {
        var id = list[i];
        var text = AQ.opMap[id];
        selectTag.options[i] = new Option(text, id, false, false);
        if (id == selectedValue)
            var selectedIndex = i;
    }
    if (isNaN(selectedIndex) || selectedIndex < 0 || selectedIndex >= length)
        selectedIndex = 0;
    selectTag.options[selectedIndex].selected = true; // it is safer to set selected item after populate the list
    return selectTag;
}

// Private method to find dom element (QBE or control) to match the given control/qbe id
// return null if not found
AQ.getInputTagById = function (id) {
    var isQBE = (id.indexOf("qbe") == 0);
    var doc = document;
    if (isQBE) {
        var elems = doc.getElementsByName("f" + id);
        if (elems && elems.length > 0)
            return elems[elems.length - 1];

        var elems = doc.getElementsByName(id);
        if (elems && elems.length > 0)
            return elems[elems.length - 1];
    }
    else {
        return doc.getElementById(id);
    }
    return null;
}

// Private method to update the E1 form view based on the model
AQ.updateFormView = function () {
    // update indicator for condition enabled control and QBE
    var showAddIcon = AQ.isManagementMode();
    var doc = document;
    var saveIcon = doc.getElementById(AQ.ID_SAVE_ICON);
    if ((saveIcon && saveIcon.disabled) || !AQ.isSaveEnabled()) {
        showAddIcon = false;
    }
    var leftOperandList = AQ.leftOperandList;
    if (leftOperandList) {
        for (var i = 0; i < leftOperandList.length; i++) {
            var left = leftOperandList[i];
            var id = left.id;
            var inputTag = AQ.getInputTagById(id);
            if (inputTag) {
                inputTag.style.backgroundColor = "";

                // add icon
                var addIconId = AQ.ID_ADD_ICON.format(id);
                var addIcon = doc.getElementById(addIconId);
                if (addIcon) {
                    addIcon.parentNode.removeChild(addIcon);
                }

                if (showAddIcon) {
                    addIcon = AQ.createAddIcon(AQ.ID_ADD_ICON, id, inputTag);
                    addIcon.style.top = AQ.ICON_OFFSET_Y + "px";
                    addIcon.style.left = AQ.ICON_OFFSET_X + "px";
                }

                // adjust the left shift offset for the VA icon
                if (addIcon) {
                    var vaIcon = null;
                    var childList = inputTag.parentNode.childNodes;
                    for (var j = 0; j < childList.length; j++) {
                        if ((childList[j].name && childList[j].name.indexOf("va", 0) == 0) || childList[j].className == "JSVisualAssist") {
                            vaIcon = childList[j];
                            break;
                        }
                    }

                    if (vaIcon) {
                        vaIcon.style.position = "relative";
                        vaIcon.style.left = showAddIcon ? AQ.ICON_OFFSET_X + "px" : 0;
                    }
                }
            }
        }
    }
}

// Public Event Handler for capture new condition by user
AQ.goCaptureCondition = function (e) {
    AQ.destoryErrorWindow();
    AQ.setDirty(true);
    var elem = GUTIL.getEventSource(e);
    var id = elem.getAttribute(AQ.ATT_CONTROL_ID);
    if (id == null) // Accessibility users
    {
        id = elem.getAttribute(AQ.ATT_ANCHOR_CONTROL_ID);
    }
    if (AQ.isInLeftOperandList(id)) {
        AQ.send("aqcapture." + id);
    }
}

// Public Event Handler for uncapture(remove) an existing condition by user
AQ.goRemoveCondition = function (e) {
    AQ.destoryErrorWindow();
    AQ.setDirty(true);
    var elem = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(elem);
    AQ.send("aquncapture." + infoObj.conditionIndex);
}

// Private method to get real condition index by element
// TODO: we need to resort condition list
AQ.getConditionIndexByElement = function (elem) {
    var rowElem = elem.parentNode.parentNode;
    var tBodyElem = rowElem.parentNode;
    for (var conditionIndex = 0; conditionIndex < tBodyElem.childNodes.length; conditionIndex++)
        if (tBodyElem.childNodes[conditionIndex] == rowElem)
            break;
    return conditionIndex;
}

// Public event handler for change the operator selection of a certain condtion
AQ.goChangeOperator = function (e) {
    AQ.destoryErrorWindow();
    AQ.setDirty(true);
    var elem = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(elem);
    var conditionIndex = infoObj.conditionIndex;
    var operatorId = elem.options[elem.selectedIndex].value;

    AQ.send("aqchangeoperator." + conditionIndex + AQ.SEP_LINE + operatorId);
}

// Public event handler for change a certain right value of a certain condtion
AQ.goChangeRightValue = function (e, isBlur) {
    AQ.destoryErrorWindow();
    AQ.setDirty(true);
    var elem = GUTIL.getEventSource(e);
    var value = elem.value;
    var infoObj = AQ.retrieveAQAttributes(elem);
    var valueIndex = infoObj.valueIndex;
    var conditionIndex = infoObj.conditionIndex;
    var condition = AQ.getActiveQuery().conditions[conditionIndex];

    if (isEmptyString(value) && condition.operatorId == AQ.OP_LIST && condition.rightOperand.length > 1 && !isBlur) {
        // set a value to empty means delete this value from the list of "in list" condition
        // however, we do not want to remove the last value in the list even it is empty
        // because empty list for "in list" conditin is not a valid condition
        AQ.send("aqremovevalue." + conditionIndex + AQ.SEP_LINE + valueIndex);
        condition.rightOperand.splice(valueIndex, 1);
        AQ.updateQueryViewForUpdate(conditionIndex);
        AQ.updateValidation(conditionIndex);
    }
    else {
        var rightValue = condition.rightOperand[infoObj.valueIndex];
        rightValue.value = value;
        rightValue.verified = true;
        rightValue.valid = true; // temparory make the client believe the input value is valid before server post back new validation information
        AQ.send("aqchangevalue." + conditionIndex + AQ.SEP_LINE + valueIndex + AQ.SEP_LINE + value);
    }
}

// Private method to append a Input TR to the end Right Value Input Table with given conditionIndex and default value
// TR(inputTR) - TD(inputTD) - text (possible prefix)
//                           - text (user input value)
//                           - text (possible postfix)
//              - TD ( posible popup selection for special value)
AQ.appendInputTR = function (conditionIndex, valueIndex) {
    var valueListTable = AQ.getValueListTable(conditionIndex);
    var inputTR = valueListTable.insertRow(-1);
    inputTR.setAttribute(AQ.ATT_VALUE_INDEX, valueIndex);
    inputTR.name = AQ.NAME_INPUT_TR;
    var inputTD = inputTR.insertCell(-1);
    inputTD.className = "RINoWrap";
    AQ.populateInputTD(inputTD, conditionIndex, valueIndex);

    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var dataType = condition.dataType;

    // add dropdown for select special value id for editable condition with date and string type left operand
    if (!condition.readOnly && (dataType == AQ.TYPE_JDEDATE || dataType == AQ.TYPE_JDEUTIME || dataType == AQ.TYPE_STRING))
        AQ.appendSpeicalValueLanuchTD(inputTR);
}

// Private method to render the input TD for single vlaue
AQ.populateInputTD = function (inputTD, conditionIndex, valueIndex) {
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var rightValue = condition.rightOperand[valueIndex];
    var svId = rightValue.svId;
    inputTD.parentNode.setAttribute(AQ.ATT_SVID, svId); // neet to set this value to parent TR so that svlaunchTD can reuse this value later
    inputTD.className = "SPPaddingText";

    switch (svId) {
        case AQ.SV_RESET:
        case AQ.SV_LITERAL:
            var inputTag = AQ.populateInputTDForLiteral(inputTD, conditionIndex, valueIndex);
            break;
        case AQ.SV_LOGIN_USER:
        case AQ.SV_TODAY:
            var inputTag = AQ.populateInputTDForText(inputTD, conditionIndex, valueIndex);
            break;
        default: // date related
            var inputTag = AQ.populateInputTDForExpression(inputTD, conditionIndex, valueIndex);
            break;
    }
}

// Pirvate method to append a input TR (for one value) for literal (not special value)
AQ.populateInputTDForLiteral = function (inputTD, conditionIndex, valueIndex) {
    var valueListTable = AQ.getValueListTable(conditionIndex);
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var rightValue = condition.rightOperand[valueIndex];

    var inputTag = document.createElement("input");

    inputTag.style.width = condition.rightWidth + "px";
    inputTag.maxLength = condition.maxLength;
    inputTag.value = rightValue.value;
    inputTag.name = AQ.NAME_VALUE_INPUT;
    inputTag.disabled = valueListTable.disabled;

    // set event handler for the input tag
    if (!valueListTable.disabled) // editable condition
    {
        inputTag.onchange = AQ.goChangeRightValue;
        inputTag.onfocus = AQ.goFocusCondition;
        inputTag.onblur = AQ.goBlurCondition;
    }

    // set stylesheet for the input tag
    //  if(!rightValue.valid)
    //    inputTag.style.backgroundColor = AQ.BG_COLOR_VALIDTION_ERROR;
    if (AQ.ACCESSIBILITY) {
        var inputTagId = "inputLiteral" + conditionIndex + "." + valueIndex;
        inputTag.setAttribute("id", inputTagId);
        var inputTagLabel = document.createElement("label");
        var inputTagLabelId = "labelinputLiteral" + conditionIndex + "." + valueIndex;
        inputTagLabel.setAttribute("id", inputTagLabelId);
        inputTagLabel.setAttribute("for", inputTagId);
        inputTagLabel.className = "accessibility";
        inputTD.appendChild(inputTagLabel);
        inputTagLabel.innerHTML = AQ.constants.AQ_LABEL_RIGHT_OPERANT + " " + AQ.retrieveAQRowTitle(inputTagLabel);
        inputTagLabel.setAttribute("title", AQ.constants.AQ_LABEL_RIGHT_OPERANT + " " + AQ.retrieveAQRowTitle(inputTagLabel));
    }
    inputTD.appendChild(inputTag);
}

// Pirvate method to append a input TR (for one value) for login user id and today which is pure text
// only be called when svid is AQ.SV_LOGIN_USER or AQ.SV_TODAY
AQ.populateInputTDForText = function (inputTD, conditionIndex, valueIndex) {
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var rightValue = condition.rightOperand[valueIndex];
    var svId = rightValue.svId;
    var text = (svId == AQ.SV_LOGIN_USER) ? AQ.constants.AQ_LOGIN_USER : AQ.constants.AQ_TODAY;
    var textNode = document.createTextNode(text);
    if (AQ.ACCESSIBILITY) {
        var textNodeId = "textNode" + conditionIndex + "." + valueIndex;
        textNode.setAttribute("id", textNodeId);
        var textNodeLabel = document.createElement("label");
        var textNodeLabelId = "labeltextNode" + conditionIndex + "." + valueIndex;
        textNodeLabelId.setAttribute("id", textNodeLabelId);
        textNodeLabel.setAttribute("for", textNodeId);
        textNodeLabel.className = "accessibility";
        inputTD.appendChild(textNodeLabel);
        textNodeLabel.innerHTML = AQ.constants.AQ_LABEL_RIGHT_OPERANT + " " + AQ.retrieveAQRowTitle(textNodeLabel);
        textNodeLabel.setAttribute("title", AQ.constants.AQ_LABEL_RIGHT_OPERANT + " " + AQ.retrieveAQRowTitle(textNodeLabel));
    }
    inputTD.appendChild(textNode);
}

// Pirvate method to append a input TR (for one value) for expression contian the input value
AQ.populateInputTDForExpression = function (inputTD, conditionIndex, valueIndex) {
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var rightValue = condition.rightOperand[valueIndex];
    var svId = rightValue.svId;
    var text = AQ.mapSvIdToDisplay[svId];
    text = text.replace(/%/g, "{0}");
    text = text.format(rightValue.value);
    var textNodeExp = document.createTextNode(text);

    if (AQ.ACCESSIBILITY) {
        var textNodeExpId = "textNodeExp" + conditionIndex + "." + valueIndex;
        textNodeExp.setAttribute("id", textNodeExpId);
        var textNodeExpLabel = document.createElement("label");
        var textNodeExpLabelId = "labeltextNodeExp" + conditionIndex + "." + valueIndex;
        textNodeExpLabelId.setAttribute("id", textNodeExpLabelId);
        textNodeExpLabel.setAttribute("for", textNodeExpId);
        textNodeExpLabel.className = "accessibility";
        inputTD.appendChild(textNodeExpLabel);
        textNodeExpLabel.innerHTML = AQ.constants.AQ_LABEL_RIGHT_OPERANT + " " + AQ.retrieveAQRowTitle(textNodeExpLabel);
        textNodeExpLabel.setAttribute("title", AQ.constants.AQ_LABEL_RIGHT_OPERANT + " " + AQ.retrieveAQRowTitle(textNodeExpLabel));
    }
    inputTD.appendChild(textNodeExp);
}

// Private method to add td for special value dropdown
AQ.appendSpeicalValueLanuchTD = function (valueRow) {
    var td = valueRow.insertCell(-1);
    var img = document.createElement("IMG");
    var saveIcon = document.getElementById(AQ.ID_SAVE_ICON);
    if ((saveIcon && saveIcon.disabled) || !AQ.isSaveEnabled()) {
        img.disabled = true;
        JSSidePanelRender.setMotionImage(img, window['E1RES_img_RI_downmo_gif']);
        img.className = "";
    }
    else {
        JSSidePanelRender.setMotionImage(img, window['E1RES_img_RI_down_gif'], window['E1RES_img_RI_downmo_gif'], AQ.goShowSVWin);
    }
    if (AQ.ACCESSIBILITY) {
        var imgAnchor = document.createElement("a");
        imgAnchor.onclick = AQ.goShowSVWin;
        imgAnchor.appendChild(img);
        td.appendChild(imgAnchor);
        img.alt = AQ.constants.AQ_LABEL_SPECIAL_OPERANT + " " + AQ.retrieveAQRowTitle(img);
    }
    else {
        td.appendChild(img);
    }
}

// Private method to append TR tag for the "Add More Icon"
AQ.appendAddMoreTR = function (conditionIndex) {
    var valueListTable = AQ.getValueListTable(conditionIndex);
    if (valueListTable.disabled)
        return;

    var newRow = valueListTable.insertRow(-1);
    newRow.name = AQ.NAME_ADD_MORE;
    var newCell = newRow.insertCell(-1);
    var addValueIcon = document.createElement("img");
    JSSidePanelRender.setMotionImage(addValueIcon, window['E1RES_img_AQ_addvaluen_gif'], window['E1RES_img_AQ_addvaluenmo_gif'], AQ.goAddMoreValue);
    addValueIcon.setAttribute("title", AQ.constants.AQ_HOVER_ADD_MORE);

    if (AQ.ACCESSIBILITY) {
        var addValueIconLabel = document.createElement("a");
        addValueIconLabel.onclick = AQ.goAddMoreValue;
        addValueIconLabel.appendChild(addValueIcon);
        newCell.appendChild(addValueIconLabel);
        addValueIcon.alt = AQ.constants.AQ_ADD_MORE_TO + " " + AQ.retrieveAQRowTitle(addValueIcon);
    }
    else {
        newCell.appendChild(addValueIcon);
    }
}

// Public Event Handler for trigger add more value appended to the end of value list of "Mutiple Input Operator" (list)
AQ.goAddMoreValue = function (e) {
    AQ.destoryErrorWindow();
    AQ.setDirty(true);
    var elem = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(elem);
    var conditionIndex = infoObj.conditionIndex;
    var rightOperand = AQ.getActiveQuery().conditions[conditionIndex].rightOperand;
    var rightValue = rightOperand[rightOperand.length - 1];

    if (rightValue.svId == 0 && isEmptyString(rightValue.value)) {
        AQ.updateValidation(conditionIndex);
    }
    else {
        AQ.send("aqaddvalue." + conditionIndex);
    }
}

// Private method to remove TR tag for "Add More Value Icon" form the parent table element
AQ.removeAddMore = function (valueListTable) {
    valueListTable.deleteRow(valueListTable.rows.length - 1);
}

// Private method to update view of condition list based on the conditon removed from the model
// TODO: the condition index need to re indexed before gape in the between, which does not match server side model
AQ.updateQueryViewForRemove = function (conditionIndex) {
    var conditionRow = AQ.getCondtionRow(conditionIndex);
    var parentNode = conditionRow.parentNode;
    var nextRowElem = conditionRow.nextSibling;
    while (nextRowElem && nextRowElem.setAttribute) {
        nextRowElem.setAttribute(AQ.ATT_CONDITION_INDEX, conditionIndex++);
        var nextRowElem = nextRowElem.nextSibling;
    }

    parentNode.removeChild(conditionRow);
}

// Public Event Handler for "Delete Query Icon"
AQ.goDeleteQuery = function (event) {
    AQ.destoryErrorWindow();
    var confimToDelete = confirm(AQ.constants.AQ_CONFRIM_DELETE + " (" + AQ.getActiveQuery().name + ")");
    if (confimToDelete)
        AQ.sendDeleteQueryCmd("delete");
    // For accessibility stop event bubbling else the delete event is fired twice
    if (AQ.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        WL.stopEventBubbling(evt);
    }
}

// Public Event Handler for "Save Query Icon"
AQ.goSaveQuery = function () {
    AQ.destoryErrorWindow();
    if (!AQ.updateValidation(null))
        AQ.send("aqsave");
}

// Public Event Handler for "Save Query As Icon" before new name enter prompt
AQ.goSaveAsQueryBegin = function (event) {
    AQ.destoryErrorWindow();
    if (!AQ.updateValidation(null)) {
        var labels = new Array(AQ.constants.UDO_PROMPT_DEFAULT_NAME, AQ.constants.UDO_PROMPT_NAME);
        JSSidePanelRender.renderPromptForName(document, AQ.ID_SAVE_AS_ICON, 2, labels, AQ.NAME_LENGTH, AQ.goSaveAsQueryEnd);
    }
    // For accessibility stop event bubbling else the saveAs event is fired twice
    if (AQ.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        WL.stopEventBubbling(evt);
    }
}

// Public Event Handler for "Save Query As Icon" after new name enter prompt
AQ.goSaveAsQueryEnd = function () {
    var name = document.getElementById(JSSidePanelRender.ID_NAME_ENTERED).value;
    if (AQ.isValidName(name)) {
        AQ.setProcInd();
        AQ.activeQuery.id = AQ.BLANK_QUERY_ID;
        var e1URL = AQ.parameterizeActiveQuery("saveas");
        e1URL.setParameter("name", name);
        RIUTIL.sendXMLReq("POST", null, e1URL.toString(), RIUTIL.handlerAjaxRequest);
        return true;
    }
    return false;
}

AQ.goFindQuery = function () {
    AQ.triggerHotkeyForFind();
}

// Private method to do a high interactive post to the server
AQ.send = function (cmd) {
    JDEDTAFactory.getInstance(AQ.namespace).post(cmd);
}

AQ.setProcInd = function () {
    JDEDTAFactory.getInstance(AQ.namespace).processProcIndRequest(JDEDTAFactory.getInstance(AQ.namespace).DISP_PROC_INDICATOR, true);
}

AQ.clearProcInd = function () {
    JDEDTAFactory.getInstance(AQ.namespace).processProcIndRequest(JDEDTAFactory.getInstance(AQ.namespace).NO_PROC_INDICATOR);
}

// Private utility method to add a new element after sibliing node
AQ.insertAfter = function (newElement, targetElement) {	//target is what you want it to go after. Look for this elements parent.
    var parent = targetElement.parentNode; 	//if the parents lastchild is the targetElement...
    if (parent.lastchild == targetElement) {		//add the newElement after the target element.
        parent.appendChild(newElement);
    }
    else {
        // else the target has siblings, insert the new element between the target and it's next sibling.
        parent.insertBefore(newElement, targetElement.nextSibling);
    }
}

// Public Event Handler for selection change of two query lists.
// When the current active query is dirty (modified), we need to confirm with the user before we discard the changes
// If the user want to keep the change, he has to save first.
AQ.goChangeQueryList = function (e) {
    AQ.updateValidation(null);
    var selectTag = GUTIL.getEventSource(e);
    if (isNativeContainer) {
        setTimeout(function () { AQ.handleQueryListChange(selectTag); }, 300);
    }
    else {
        AQ.handleQueryListChange(selectTag);
    }
}

AQ.handleQueryListChange = function (selectTag) {
    if (AQ.isDirty()) // the current active query was already modified
    {
        var proceedWithoutSave = confirm(AQ.constants.AQ_CONFIRM_SWITCH);
        if (proceedWithoutSave) // discard the change and reload all models
        {
            var selectedQueryId = AQ.getCurrentSelectedQueryId();
            // pass an extra flag (isPrivate) to load the correct AQ in query model
            var isPrivateAQ = (selectedQueryId.substring(0, 7) == AQ.PUBLIC_QUERY_ID) ? false : true;
            if (!isPrivateAQ) {
                //remove the "*PUBLIC" from the query WOBNM 
                selectedQueryId = selectedQueryId.substring(7, selectedQueryId.length);
            }
            AQ.send("aqloadquerylist." + selectedQueryId + AQ.SEP_LINE + isPrivateAQ);
            return true;
        }
        else // roll back to the original selection based on the model
        {
            AQ.updateQueryListSelection(selectTag);
            return false;
        }
    }
    AQ.queryList.activeId = AQ.getCurrentSelectedQueryId();
    AQ.loadQueryModel(true);
}

// Private method to load query model from server based on the current activeId in client model
AQ.loadQueryModel = function (querySelectionChanged) {
    var queryOBNM = AQ.getActiveQueryId();
    //var isPrivate = AQ.getQueryPrivateById(queryId);    
    if (queryOBNM == AQ.BLANK_QUERY_ID) {
        AQ.send("aqloadquerymodel." + queryOBNM + AQ.SEP_LINE + querySelectionChanged + AQ.SEP_LINE + AQ.isManagementMode());
    }
    else {
        // paass and extra flag (isPrivate) to load the correct AQ in query model
        var isPrivateAQ = (queryOBNM.substring(0, 7) == AQ.PUBLIC_QUERY_ID) ? false : true;

        if (!isPrivateAQ) {
            //remove the "*PUBLIC" from the query WOBNM 
            queryOBNM = queryOBNM.substring(7, queryOBNM.length);
        }
        AQ.send("aqloadquerymodel." + queryOBNM + AQ.SEP_LINE + querySelectionChanged + AQ.SEP_LINE + AQ.isManagementMode() + AQ.SEP_LINE + isPrivateAQ);
    }
    //AQ.send("aqloadquerymodel."+queryId + AQ.SEP_LINE + isPrivate);
}

// Public Event Handler for click on "Set Default Query" setting
AQ.goSetDefault = function () {
    AQ.destoryErrorWindow();
    AQ.updateValidation(null);
    AQ.setDirty(true);
    var elem = document.getElementById(AQ.ID_DEFAULT_QUERY);
    if (AQ.queryList.defaultId == AQ.getActiveQuery().omwObjectName && !elem.checked) {
        AQ.queryList.defaultId = null;
    }
    else if (elem.checked) {
        AQ.queryList.defaultId = AQ.getActiveQuery().omwObjectName;
    }

    AQ.send("aqsetdefault." + elem.checked);
}

// Public Event Handler for click on "Automatically Running" setting
AQ.goSetAutoFind = function () {
    AQ.destoryErrorWindow();
    AQ.updateValidation(null);
    AQ.setDirty(true);
    var elem = document.getElementById(AQ.ID_AUTO_FIND);
    AQ.getActiveQuery().autoFind = elem.checked;
    AQ.send("aqsetautofind." + elem.checked);
}

// Public Event Handler for click on "Automatically Clear" setting
AQ.goSetAutoClear = function () {
    AQ.destoryErrorWindow();
    AQ.updateValidation(null);
    AQ.setDirty(true);
    var elem = document.getElementById(AQ.ID_AUTO_CLEAR);
    AQ.getActiveQuery().autoClear = elem.checked;
    AQ.send("aqsetautoclear." + elem.checked);
}

// Public Event Handler for click on "Match All and Match Any" setting
AQ.goSetLogic = function () {
    AQ.destoryErrorWindow();
    AQ.updateValidation(null);
    AQ.setDirty(true);
    var elems = document.getElementsByName(AQ.ID_LOGIC);
    var islogicAnd = (elems[0].checked == true)
    AQ.getActiveQuery().isLogicAnd = islogicAnd;
    AQ.send("aqsetlogic." + islogicAnd);
}

// Private method to set a certain query model to be the current active query model (model change)
AQ.setActiveQuery = function (queryModel) {
    AQ.activeQuery = queryModel;
    //use query OBNM 
    AQ.queryList.activeId = queryModel.omwObjectName;
}

// Private method to get the current active query model
AQ.getActiveQuery = function () {
    return AQ.activeQuery;
}

// Private method to get the id of the current active query model
AQ.getActiveQueryId = function () {
    return AQ.queryList.activeId;
}
// Private method to find query model from the query list based on the given QueryOBNM
AQ.getQueryModelById = function (queryOBNM) {
    var privateItems = AQ.queryList.privateItems;
    for (var i = 0, len = privateItems.length; i < len; i++) {
        if (privateItems[i].omwObjectName == queryOBNM) {
            return privateItems[i];
        }
    }
    var publicItems = AQ.queryList.publicItems;
    for (var i = 0, len1 = publicItems.length; i < len1; i++) {
        if (publicItems[i].omwObjectName == queryOBNM) {
            return publicItems[i];
        }
    }
    var pendingApprovalItems = AQ.queryList.pendingApprovalItems;
    for (var i = 0, len2 = pendingApprovalItems.length; i < len2; i++) {
        if (pendingApprovalItems[i].omwObjectName == queryOBNM) {
            return pendingApprovalItems[i];
        }
    }
    var reservedItems = AQ.queryList.reservedItems;
    for (var i = 0, len3 = reservedItems.length; i < len3; i++) {
        if (reservedItems[i].omwObjectName == queryOBNM) {
            return reservedItems[i];
        }
    }
    var reworkItems = AQ.queryList.reworkItems;
    for (var i = 0, len4 = reworkItems.length; i < len4; i++) {
        if (reworkItems[i].omwObjectName == queryOBNM) {
            return reworkItems[i];
        }
    }
    return null;
}

// Public Event Handler for click on "Populate Query Iocn"
AQ.goPopulateQuery = function () {
    AQ.destoryErrorWindow();
    AQ.resetConditions();
    AQ.send("aqpopulate");
}

// Public Event Handler for click on "Special Value Dropdown Iocn"
AQ.goShowSVWin = function (e) {
    AQ.destoryErrorWindow();
    AQ.updateHighlightCondition(e, true);
    var ctrl = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(ctrl);
    var conditionIndex = infoObj.conditionIndex;
    var valueIndex = infoObj.valueIndex;
    var condition = AQ.getActiveQuery().conditions[conditionIndex];
    var right = condition.rightOperand[valueIndex];
    var value = right.value;
    var svId = infoObj.svId;
    var dataType = infoObj.dataType;
    var sb = new PSStringBuffer();
    sb.append("<table ").append(AQ.ATT_AQINFOTOP).append("=true");
    sb.append(" ").append(AQ.ATT_CONDITION_INDEX).append("=").append(conditionIndex);
    sb.append(" ").append(AQ.ATT_VALUE_INDEX).append("=").append(valueIndex);
    sb.append(" ").append(AQ.ATT_DATATYPE).append("=").append(dataType).append(">");

    if (dataType == AQ.TYPE_STRING) {
        sb.append("<tr><td colspan=2>").append(AQ.constants.AQ_LOGIN_USER).append("</td></tr>");
    }
    else {
        sb.append("<tr>");
        sb.append("<td colspan=2 valign=top><nobr>");
        sb.append("<span class='SPPaddingText'>").append(AQ.constants.AQ_TODAY).append("<span>");
        sb.append("<select id='").append(AQ.ID_SV_MATH).append("'><option value='a'>+</option><option value='m'>-</option></select>&nbsp;");
        sb.append("<INPUT class=textfield style='width:30px' id='").append(AQ.ID_SV_VALUE).append("'></input>&nbsp;");
        sb.append("<select id='").append(AQ.ID_SV_UNIT).append("'><option value='d'>").append(AQ.constants.AQ_DAYS).append("</option><option value='m'>").append(AQ.constants.AQ_MONTHS).append("</option><option value='y'>").append(AQ.constants.AQ_YEARS).append("</option></select>");
        sb.append("</nobr></td></tr>");
    }
    sb.append("<tr>");
    sb.append("<td><input class=button type='button' id='ok' value='").append(AQ.constants.AQ_OK).append("' onClick='AQ.goSetSpecialValue(event)'></input></td>");
    sb.append("<td><input class=button type='button' id='reset' value='").append(AQ.constants.AQ_RESET).append("' onClick='AQ.goResetSpecialValue(event)'></input></td>");
    sb.append("</tr>");
    sb.append("</table>");
    var framework = sb.toString();

    var eventListener = {};
    eventListener.onClose = function (e) {
        JSSidePanelRender.hideUIBlock();
    }

    var winTop = getAbsoluteTopPos(ctrl, true);
    var winLeft = getAbsoluteLeftPos(ctrl, true);
    var winWidth = AQ.SV_DIALOG_WIDTH;
    var winHeight = AQ.SV_DIALOG_HEIGHT;
    JSSidePanelRender.showUIBlock();
    AQ.svWin = createPopup("SpecialValueAssistantDialog", ADVANCED_QUERY, true, false, true, false, AQ.constants.AQ_TITLE_SV_DIALOG, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, ctrl, framework, eventListener);

    if (dataType != AQ.TYPE_STRING)
        AQ.updateViewForSVWin(svId, value);
}

// private method to upate the date expression view in the popup speical value window based on the given svId and value
AQ.updateViewForSVWin = function (svId, value) {
    // convert the current literal value to unit value for date expression
    if (value == "*" || isNaN(value))
        value = 0;

    var mathSelectdIndex = 0;
    var unitSelectedIndex = 0;

    switch (svId) {
        case AQ.SV_TODAY:
            value = 0;
            break;

        case AQ.SV_TODAY_PLUS_DAY:
            mathSelectdIndex = 0;
            unitSelectedIndex = 0;
            break;

        case AQ.SV_TODAY_MINUS_DAY:
            mathSelectdIndex = 1;
            unitSelectedIndex = 0;
            break;

        case AQ.SV_TODAY_PLUS_MONTH:
            mathSelectdIndex = 0;
            unitSelectedIndex = 1;
            break;

        case AQ.SV_TODAY_MINUS_MONTH:
            mathSelectdIndex = 1;
            unitSelectedIndex = 1;
            break;

        case AQ.SV_TODAY_PLUS_YEAR:
            mathSelectdIndex = 0;
            unitSelectedIndex = 2;
            break;

        case AQ.SV_TODAY_MINUS_YEAR:
            mathSelectdIndex = 1;
            unitSelectedIndex = 2;
            break;
    }
    var doc = document;
    var valueTag = doc.getElementById(AQ.ID_SV_VALUE)
    valueTag.value = value;

    var mathSelectTag = doc.getElementById(AQ.ID_SV_MATH)
    mathSelectTag.selectedIndex = mathSelectdIndex;

    var unitSelectedTag = doc.getElementById(AQ.ID_SV_UNIT)
    unitSelectedTag.selectedIndex = unitSelectedIndex;
}

// Private utility method to retrieve all advanced query related information from the current dom element and its acandant
AQ.retrieveAQAttributes = function (elm) {
    var conditionIndex = null;
    var valueIndex = null;
    var svId = null;
    var dataType = null;

    while (elm != null && elm.getAttribute) //Tip: nodeName return more information than tagName and it is better chose.
    {
        if (conditionIndex == null && elm.getAttribute(AQ.ATT_CONDITION_INDEX) != null)
            conditionIndex = parseInt(elm.getAttribute(AQ.ATT_CONDITION_INDEX));

        if (valueIndex == null && elm.getAttribute(AQ.ATT_VALUE_INDEX) != null)
            valueIndex = parseInt(elm.getAttribute(AQ.ATT_VALUE_INDEX));

        if (svId == null && elm.getAttribute(AQ.ATT_SVID) != null)
            svId = parseInt(elm.getAttribute(AQ.ATT_SVID));

        if (dataType == null && elm.getAttribute(AQ.ATT_DATATYPE) != null)
            dataType = parseInt(elm.getAttribute(AQ.ATT_DATATYPE));

        if (elm.getAttribute('aqInfoTop')) // we are reaching the top element for searching
            break;

        elm = elm.parentNode;
    };

    var infoObj =
    {
        conditionIndex: conditionIndex,
        valueIndex: valueIndex,
        svId: svId,
        dataType: dataType
    };

    return infoObj;
}

// Private utility method to retrieve AQ Row Title.
AQ.retrieveAQRowTitle = function (elm) {
    while (elm != null) {
        if (elm.getAttribute('AQRowTitle')) {
            var AQRowTitle = elm.getAttribute('AQRowTitle');
            break;
        }
        elm = elm.parentNode;
    };
    return AQRowTitle;
}

// Public Event Handler for ok button to set sepcial value
AQ.goSetSpecialValue = function (e) {
    AQ.setDirty(true);
    var ctrl = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(ctrl);
    var message = "aqsetsv." + infoObj.conditionIndex + AQ.SEP_LINE + infoObj.valueIndex + AQ.SEP_LINE
    if (infoObj.dataType == AQ.TYPE_STRING) {
        message += AQ.SV_LOGIN_USER;
    }
    else {
        var doc = document;
        var svValue = doc.getElementById(AQ.ID_SV_VALUE).value;
        var svMathSelect = doc.getElementById(AQ.ID_SV_MATH);
        var svMath = svMathSelect.options[svMathSelect.selectedIndex].value;
        var svUnitSelect = doc.getElementById(AQ.ID_SV_UNIT);
        var svUnit = svUnitSelect.options[svUnitSelect.selectedIndex].value;
        var svId = AQ.convertToSvId(svUnit, svMath, svValue);
        message += svId + AQ.SEP_LINE + svValue;
    }

    AQ.svWin.onClose();
    AQ.send(message);
}

//referenced from JSSidePanel.js
AQ.about = function () {
    // this call is handled by AboutServlet.java on the server side
    window[WL.namespace + "about"]("AQInfo");
}

// Public Event Handler for reset button to reset sepcial value back to literal
AQ.goResetSpecialValue = function (e) {
    AQ.setDirty(true);
    var ctrl = GUTIL.getEventSource(e);
    var infoObj = AQ.retrieveAQAttributes(ctrl);
    AQ.svWin.onClose();
    AQ.send("aqsetsv." + infoObj.conditionIndex + AQ.SEP_LINE + infoObj.valueIndex + AQ.SEP_LINE + AQ.SV_RESET);
}

// Privarte method to map client side speical value parts into single id
AQ.convertToSvId = function (svUnit, svMath, svValue) {
    if (svValue == null || svValue == "" || svValue == 0 || svValue == "*")
        return AQ.SV_TODAY;

    return AQ.mapMUToSvId[svMath + svUnit];
}

// Private method to clean up all the existing condition from the model
AQ.resetConditions = function () {
    AQ.getActiveQuery().conditions = new Array();
    AQ.updateQeryViewForReset();
    AQ.updateFormView();
}

// Private method to update view with all conditions clean up
AQ.updateQeryViewForReset = function () {
    var table = AQ.getConditionsListTable();
    var tbody = table.getElementsByTagName("TBODY")[0];
    while (tbody.childNodes.length > 0) {
        var child = tbody.firstChild;
        tbody.removeChild(child);
    }
}

// Public event handler for toggle advanced query mode
// close side panel if it contain advanced query, otherwise, open advanced query in side panel
AQ.goToggleAdvancedQuery = function () {
    if (AQ.isManagementMode()) {
        JSSidePanelRender.reqClose();
    }
    else {
        //we want to do the send message here.  This will be send once.
        //If we do it in the JSSidePanelRender, The message will always be send
        //And the list will call the AllowAction BSFN 
        AQ.send("aqloadobjectallowaction.1");
        JSSidePanelRender.reqOpen(JSSidePanelRender.MODE_QUERY);
    }
}

AQ.onClose = function (e) {
    if (AQ.proceedWithoutSave()) {
        JSSidePanelRender.close();
        AQ.setDirty(false);
    }
}

// Method to check if the current active AQ is already modified without save
AQ.proceedWithoutSave = function () {
    if (AQ.isDirty()) {
        var proceedWithoutSave = confirm(AQ.constants.AQ_CONFIRM_SWITCH);
        return proceedWithoutSave; // true if user OKs, false if user cancels
    }
    return true;
}

AQ.goClearForm = function () {
    AQ.send("aqclear");
}


// -------------------------
// inherit methods
// -------------------------
// Public Interface to hock up Size Panel. Trigger after a mode change occur
AQ.afterModeChange = function () {
    AQ.destoryErrorWindow();
    if (JSSidePanelRender.getMode() == JSSidePanelRender.MODE_NORMAL) {
        AQ.updateControlView();
        AQ.updateFormView();
    }
    else {
        AQ.updateQueryView();
        AQ.updateControlView();
        AQ.updateFormView();
    }
}

// Public Interface to hock up Size Panel. It supply the tabs model to Side Panel for rendering
AQ.getTabsArray = function () {
    var tabsArray = new Array()
    var activeQueryTab = AQ.isManagementMode();
    var showQueryTab = activeQueryTab;

    if (showQueryTab) // query tab active
    {
        tabsArray.add({ isActive: activeQueryTab,
            title: AQ.constants.AQ_TITLE_TAB,
            eventHandler: " "
        });
    }

    return tabsArray;
}

// Public Interface to hock up Size Panel. It supply the menu item model to Side Panel for rendering
AQ.getMenusArray = function () {
    var menusArray = new Array();

    /*If its new modified AQ, only SAVE icon is reqired*/
    if (AQ.isModifiedAQForm) {
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_save_ena_png'], overImg: window['E1RES_share_images_ulcm_save_hov_png'], title: AQ.constants.UDO_SAVE, onclick: 'AQ.goSaveModifiedQuery()', id: AQ.ID_SAVE_ICON });
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_delete_ena_png'], overImg: window['E1RES_share_images_ulcm_delete_hov_png'], title: AQ.constants.UDO_DELETE, onclick: 'AQ.goDelModifiedQuery(this)', id: AQ.ID_DELETE_ICON });
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_search_ena_png'], overImg: window['E1RES_share_images_ulcm_search_hov_png'], title: AQ.constants.JH_FIND, onclick: 'AQ.goFindQuery()', hideForAccessibility: true }); // hide extra find button for acessibility user
        var privateAQItems = AQ.queryList.privateItems[0];
        if (AQ.isBlankOBNM() && privateAQItems && (privateAQItems.modifiedAQPreivew == false)) {
            menusArray.add({ outImg: window['E1RES_share_images_ulcm_populate_ena_png'], overImg: window['E1RES_share_images_ulcm_populate_hov_png'], title: AQ.constants.AQ_HOVER_POPULATE_QUERY, onclick: 'AQ.goPopulateQuery()', id: AQ.ID_POPULATE_QUERY });
        }
    }
    /* Else normal flow*/
    else {
        if (AQ.isBlankQuery()) {
            menusArray.add({ outImg: window['E1RES_share_images_ulcm_save_ena_png'], overImg: window['E1RES_share_images_ulcm_save_hov_png'], title: AQ.constants.UDO_SAVE, onclick: 'AQ.goSaveAsQueryBegin(this)', id: AQ.ID_SAVE_ICON });
        }
        else {
            menusArray.add({ outImg: window['E1RES_share_images_ulcm_save_ena_png'], overImg: window['E1RES_share_images_ulcm_save_hov_png'], title: AQ.constants.UDO_SAVE, onclick: 'AQ.goSaveQuery()', id: AQ.ID_SAVE_ICON });
        }
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_saveas_ena_png'], overImg: window['E1RES_share_images_ulcm_saveas_hov_png'], title: AQ.constants.UDO_SAVE_AS, onclick: 'AQ.goSaveAsQueryBegin(this)', id: AQ.ID_SAVE_AS_ICON });
        if (AQ.userHasPublishAuthority) {
            menusArray.add({ outImg: window['E1RES_share_images_ulcm_requestpublish_ena_png'], overImg: window['E1RES_share_images_ulcm_requestpublish_hov_png'], title: AQ.constants.UDO_REQUEST_PUBLISH, onclick: 'AQ.goRequestPublishQueryBegin()', id: AQ.ID_PUBLISH_ICON });
        }
        if (AQ.userHasReserveAuthority) {
            menusArray.add({ outImg: window['E1RES_share_images_ulcm_reserve_ena_png'], overImg: window['E1RES_share_images_ulcm_reserve_hov_png'], title: AQ.constants.UDO_RESERVE, onclick: 'AQ.goReserveQuery()', id: AQ.ID_RESERVE_ICON });
        }
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_delete_ena_png'], overImg: window['E1RES_share_images_ulcm_delete_hov_png'], title: AQ.constants.UDO_DELETE, onclick: 'AQ.goDeleteQuery(this)', id: AQ.ID_DELETE_ICON });
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_notes_ena_png'], overImg: window['E1RES_share_images_ulcm_notes_hov_png'], title: AQ.constants.UDO_NOTES, onclick: 'AQ.showNotes()', id: AQ.ID_NOTES_ICON });
        menusArray.add({ outImg: window['E1RES_share_images_ulcm_search_ena_png'], overImg: window['E1RES_share_images_ulcm_search_hov_png'], title: AQ.constants.JH_FIND, onclick: 'AQ.goFindQuery()', hideForAccessibility: true }); // hide extra find button for acessibility user
        if (AQ.isBlankQuery()) {
            menusArray.add({ outImg: window['E1RES_share_images_ulcm_populate_ena_png'], overImg: window['E1RES_share_images_ulcm_populate_hov_png'], title: AQ.constants.AQ_HOVER_POPULATE_QUERY, onclick: 'AQ.goPopulateQuery()' });
        }
    }
    return menusArray;
}

// Public Interface to hock up Size Panel. It supply UI inside active tab page of Size Panel for rendering
AQ.getHTMLForTabBody = function (sb) {
    var alignStr = AQ.isRTL ? "right" : "left";
    sb.append("<table ").append(AQ.ATT_AQINFOTOP).append("=true class='SPLayoutTable' role='presentation' id='").append(AQ.ID_CONTENT_TABLE).append("'>");
    sb.append("<tr>\
                  <td colspan=2>\
                    <div style='display:none' id='").append(AQ.ID_MESSAGE_DIV).append("' class='SPMessageDiv'></div></td></tr>");
    // HTML div placeholder for reserved by area
    sb.append("<tr>\
                  <td colspan=2>\
                    <div style='display:none' id='").append(AQ.ID_RESERVED_BY).append("' class='SPMessageDiv'");
    sb.append("></div>");
    sb.append("</td></tr>");
    sb.append("<tr><td style='width:20%'>\
                    <div >\
                      <nobr>&nbsp;").append(AQ.constants.UDO_LABEL_NAME).append("</nobr>\
                    </div>\
                  </td>\
                  <td align='").append(AQ.isRTL ? "center" : "left").append("' style='width:80%'> \
                    <select ").append(AQ.isModifiedAQForm ? " disabled " : "").append(" id=").append(AQ.ID_QUERY_LIST_SP);
    if (AQ.ACCESSIBILITY) {
        sb.append(" aria-label='").append(AQ.constants.UDO_LABEL_NAME).append("'");
    }
    sb.append("> </select>");

    // Add below controls only for normal AQ - Begin
    if (!AQ.isModifiedAQForm) {
        sb.append("</td>\
                </tr> \
                <tr>\
                  <td style='width:20%'>\
                    <div >\
                      <nobr>&nbsp;").append(AQ.constants.AQ_LABEL_DEFAULT_QUERY).append("</nobr>\
                    </div>\
                  </td>\
                  <td align='").append(alignStr).append("' style='width:80%'>");
        var isULCMPreview = false;
        if (typeof queryObject != 'undefined' && queryObject.isLcmPreview) {
            isULCMPreview = true;
        }
        var isDisabled = AQ.getActiveQuery().isCheckedOut || !AQ.getActiveQuery().isPrivate || isULCMPreview;
        if (AQ.ACCESSIBILITY) {
            sb.append("<input type=checkbox role='checkbox' id='").append(AQ.ID_DEFAULT_QUERY).append("' ");
            if (isDisabled) {
                sb.append(" disabled ");
            }
            sb.append(" aria-label=\"").append(AQ.constants.AQ_LABEL_DEFAULT_QUERY).append("\" onclick='AQ.goSetDefault()'> </input>");
        }
        else {
            sb.append("<input type=checkbox id='").append(AQ.ID_DEFAULT_QUERY).append("' ");
            if (isDisabled) {
                sb.append(" disabled ");
            }
            sb.append(" onclick='AQ.goSetDefault()'> </input>")
        }

        sb.append("</td>\
                </tr> \
                <tr>\
                  <td style='width:20%'>\
                    <div >\
                      <nobr>&nbsp;").append(AQ.constants.AQ_LABEL_AUTO_FIND).append("</nobr>\
                    </div>\
                  </td>\
                  <td align='").append(alignStr).append("' style='width:80%'>");
        if (AQ.ACCESSIBILITY) {
            sb.append("<input type=checkbox role='checkbox' id='").append(AQ.ID_AUTO_FIND).append("' aria-label=\"").append(AQ.constants.AQ_LABEL_AUTO_FIND).append("\" onclick='AQ.goSetAutoFind()'> </input>");
        }
        else {
            sb.append("<input type=checkbox id='").append(AQ.ID_AUTO_FIND).append("' onclick='AQ.goSetAutoFind()'> </input>");
        }


        sb.append("</td>\
                </tr> \
                <tr>\
                  <td style='width:20%'>\
                    <div >\
                      <nobr>&nbsp;").append(AQ.constants.AQ_LABEL_AUTO_CLEAR).append("</nobr>\
                    </div>\
                  </td>\
                  <td align='").append(alignStr).append("' style='width:80%'>");
        if (AQ.ACCESSIBILITY) {
            sb.append("<input type=checkbox role='checkbox' id='").append(AQ.ID_AUTO_CLEAR).append("' aria-label='").append(AQ.constants.AQ_LABEL_AUTO_CLEAR).append("' onclick='AQ.goSetAutoClear()'> </input>");
        }
        else {
            sb.append("<input type=checkbox id='").append(AQ.ID_AUTO_CLEAR).append("' onclick='AQ.goSetAutoClear()'> </input>");
        }
    } //End

    sb.append("</td>\
                </tr>\
                <tr>\
                  <td colspan=2 align='left'> \
                    <fieldset class='").append("GroupBox ").append(AQ.isRTL ? "SPFloat_right" : "").append("' style='width:95%:' align='").append(alignStr).append("'>\
                      <legend>").append(AQ.constants.AQ_LEGEND_CONDITIONS).append("</legend>\
                      <div align='").append(alignStr).append("'>");
    if (AQ.ACCESSIBILITY) {
        sb.append("<input type=radio role='radio' value='and' checked=true id='").append(AQ.ID_MATCH_ALL).append("' aria-labelledby='").append(AQ.ID_MATCH_ALL_LABEL).append("' name='").append(AQ.ID_LOGIC).append("' onclick='AQ.goSetLogic()'>").append(AQ.constants.AQ_LABEL_MATCH_ALL).append("</input>");
        sb.append("<label class='accessibility' for='").append(AQ.ID_MATCH_ALL).append("'>").append(AQ.constants.AQ_LABEL_MATCH_ALL).append("</label>");
        sb.append("<input type=radio  role='radio' value='or' id='").append(AQ.ID_MATCH_ANY).append("' aria-labelledby='").append(AQ.ID_MATCH_ANY_LABEL).append("' name='").append(AQ.ID_LOGIC).append("' onclick='AQ.goSetLogic()'>").append(AQ.constants.AQ_LABEL_MATCH_ANY).append("</input>");
        sb.append("<label class='accessibility' for='").append(AQ.ID_MATCH_ANY).append("'>").append(AQ.constants.AQ_LABEL_MATCH_ANY).append("</label>");
    }
    else {
        sb.append("<input type=radio value='and' checked=true name='").append(AQ.ID_LOGIC).append("' onclick='AQ.goSetLogic()'>").append(AQ.constants.AQ_LABEL_MATCH_ALL).append("</input>");
        sb.append("<input type=radio value='or' name='").append(AQ.ID_LOGIC).append("' onclick='AQ.goSetLogic()'>").append(AQ.constants.AQ_LABEL_MATCH_ANY).append("</input>");
    }
    sb.append("</div>\
                      <table class='SPLayoutTable SPMargin' id='").append(AQ.ID_CONDITIONS_TABLE).append("'>\
                        <tbody>\
                        </tbody>\
                      </table>\
                    </fieldset>\
                  </td>\
                </tr>");

    // fields selection
    sb.append("  <tr>\
                  <td colspan=2>\
                     <table class='RILayoutTable'>\
                       <tr><td width='1%'>");
    var doc = document;
    var saveIcon = doc.getElementById(AQ.ID_SAVE_ICON);
    if ((saveIcon && saveIcon.disabled) || !AQ.isSaveEnabled()) {
        JSSidePanelRender.renderMotionImg(sb, AQ.ID_FIELDS_SELECTION_ICON, AQ.constants.AQ_EXPAND_TIP, window['E1RES_img_EUR_opensidepanel_gif'], window['E1RES_img_EUR_opensidepanelmo_gif']);
        var img = doc.getElementById(AQ.ID_FIELDS_SELECTION_ICON);
        if (img) {
            img.disabled = true;
            img.className = "";
        }
    }
    else {
        JSSidePanelRender.renderMotionImg(sb, AQ.ID_FIELDS_SELECTION_ICON, AQ.constants.AQ_EXPAND_TIP, window['E1RES_img_EUR_opensidepanel_gif'], window['E1RES_img_EUR_opensidepanelmo_gif'], 'AQ.goToggleFullFieldList()');
    }
    sb.append("         </td><td>");
    sb.append(AQ.constants.AQ_FIELDS_SELECTION);
    sb.append("         </td></tr>\
                     </table>\
                   </td>\
                </tr>\
                <tr>\
                  <td colspan=2>\
                      <table class='SPLayoutTable SPMargin' style='width:100%' id='").append(AQ.ID_FIELDS_SELECTION_TABLE).append("'>\
                        <tbody>\
                        </tbody>\
                      </table>\
                  </td>\
                </tr>\
              </table>"
            );
    AQ.FullFieldListExpanded = false;
}

// Event Handler to toggle the visibility of the fields selection list
AQ.goToggleFullFieldList = function () {
    var doc = document;
    var fieldSelIcon = doc.getElementById(AQ.ID_FIELDS_SELECTION_ICON);
    var fieldSelTable = doc.getElementById(AQ.ID_FIELDS_SELECTION_TABLE);
    if (AQ.FullFieldListExpanded == true) {
        JSSidePanelRender.removeAllChildren(fieldSelTable);
        AQ.FullFieldListExpanded = false;
        fieldSelIcon.setAttribute("alt", AQ.constants.AQ_EXPAND_TIP);
        fieldSelIcon.setAttribute("title", AQ.constants.AQ_EXPAND_TIP);
        fieldSelIcon.setAttribute("overImg", window['E1RES_img_EUR_opensidepanelmo_gif']);
        fieldSelIcon.setAttribute("outImg", window['E1RES_img_EUR_opensidepanel_gif']);
    }
    else {
        fieldSelIcon.setAttribute("overImg", window['E1RES_img_EUR_closesidepanelmo_gif']);
        fieldSelIcon.setAttribute("outImg", window['E1RES_img_EUR_closesidepanel_gif']);
        fieldSelIcon.setAttribute("alt", AQ.constants.AQ_COLLAPSE_TIP);
        fieldSelIcon.setAttribute("title", AQ.constants.AQ_COLLAPSE_TIP);
        AQ.FullFieldListExpanded = true;
        AQ.loadBSVWFieldList(fieldSelTable);
    }
}

// Private method to requset the full field list from server and server send back updateBSVWFieldList
AQ.loadBSVWFieldList = function (fieldTable) {
    for (var i = 0; i < AQ.leftOperandList.length; i++) {
        var left = AQ.leftOperandList[i];
        var id = left.id;
        var display = left.display;
        var row = fieldTable.insertRow(-1);
        var cell = row.insertCell(-1);
        cell.style.width = "1%";
        var addIcon = AQ.createAddIcon(AQ.ID_ADD_ICON_IN_FIELD_TABLE, id, null);

        cell.appendChild(addIcon);
        var cell = row.insertCell(-1);
        cell.innerHTML = display;
    }
}

AQ.createAddIcon = function (iconIdFormat, id, inputTag) {
    var addIconId = iconIdFormat.format(id);
    var addIcon = document.createElement("img");
    JSSidePanelRender.setMotionImage(addIcon, window['E1RES_share_images_alta_func_add_small_16_ena_png'], window['E1RES_share_images_alta_func_add_small_16_hov_png'], AQ.goCaptureCondition);
    addIcon.setAttribute("valign", "center");
    addIcon.id = addIconId;
    addIcon.setAttribute(AQ.ATT_CONTROL_ID, id);
    if (inputTag) {
        addIcon.style.position = "relative";
        addIcon.style.top = AQ.ICON_OFFSET_Y;
        addIcon.style.left = AQ.ICON_OFFSET_X;
        addIcon.style.zIndex = inputTag.style.zIndex + 1;
    }

    if (AQ.ACCESSIBILITY) {
        var addIconLink = document.createElement("a");
        addIconLink.setAttribute(AQ.ATT_ANCHOR_CONTROL_ID, id);
        addIconLink.onclick = AQ.goCaptureCondition;
        var sb = new PSStringBuffer();
        sb.append(AQ.constants.AQ_HOVER_ADD_PREFIX);
        sb.append(" ");
        if (inputTag != null && inputTag.title)
            sb.append(inputTag.title);
        sb.append(" ");
        sb.append(AQ.constants.AQ_HOVER_ADD_SUFFIX);
        addIcon.alt = sb.toString();

        addIconLink.appendChild(addIcon);
        if (inputTag)
            AQ.insertAfter(addIconLink, inputTag);

        return addIconLink;
    }
    else {
        if (inputTag)
            AQ.insertAfter(addIcon, inputTag);

        return addIcon;
    }
}

// Public Callback from SidePanel to give the feature object a chance to prepare the side panel opening or close
AQ.onOpen = function (mode) {
    setTimeout("AQ.loadLeftList()", 0);
}

// Public Server notification to update validation status of given condition index and value index and reformate value
AQ.updateValue = function (conditionIndex, valueIndex, rightValue) {
    inputTD = AQ.getRightValueInputTD(conditionIndex, valueIndex);
    if (inputTD) {
        // update model
        var condition = AQ.getActiveQuery().conditions[conditionIndex];
        var originalRightValue = condition.rightOperand[valueIndex];
        condition.rightOperand[valueIndex] = rightValue;

        // repaint the right value when its special value type changed. also for date(svId > 2), the user could be changing the value itself, validation error can be updated later
        if (originalRightValue.svId != rightValue.svId || originalRightValue.svId > 2) {
            JSSidePanelRender.removeAllChildren(inputTD);
            AQ.populateInputTD(inputTD, conditionIndex, valueIndex);
        }
        else // when only value change, we can update value attribute on of the input tag
        {
            var inputTag = AQ.getRightValueInputTag(conditionIndex, valueIndex);
            //if inputTag is null, the right operand contains special value. There're only 2 special values, User ID AND Today, those are for 2 different DD types
            //and can never appear in the right operand of the same DD, so the user is NOT changing the value in this case, just ignore.
            if (inputTag != null)
                inputTag.value = rightValue.value;
        }
    }

    AQ.updateValidation(conditionIndex, true);
}


// Private method to get the dom element for the TBODY tag of the condition list
AQ.getConditionsListTable = function () {
    return document.getElementById(AQ.ID_CONDITIONS_TABLE);
}

// Private method to get condtion row (TR)
AQ.getCondtionRow = function (conditionIndex) {
    var conditionListTable = AQ.getConditionsListTable();

    if (!conditionListTable)
        return null;

    if (conditionIndex < 0 || conditionIndex >= conditionListTable.rows.length)
        return null;

    return conditionListTable.rows[conditionIndex];
}

// Private method to get the dom element to hold the input values (as TABLE) for given condition index.
// conditionListTbody -> conditionRow -> last TD -> first Table
AQ.getValueListTable = function (conditionIndex) {
    var conditionRow = AQ.getCondtionRow(conditionIndex);

    if (conditionRow == null)
        return null;
    return conditionRow.getElementsByTagName('TABLE')[0];
}

// Private method to get the input tag given condition index and value index
// valueListTable -> valueInputTR -> valueInputTD
AQ.getRightValueInputTD = function (conditionIndex, valueIndex) {
    var valueListTable = AQ.getValueListTable(conditionIndex);
    if (!valueListTable)
        return null;

    var valueInputTRList = valueListTable.getElementsByTagName("TR");
    if (valueIndex >= valueInputTRList.length)
        return null;

    var valueInputTR = valueInputTRList[valueIndex];
    var valueInputTD = valueInputTR.firstChild;
    return valueInputTD;
}

// Private method to get the input tag given condition index and value index
// valueInputTD -> input
AQ.getRightValueInputTag = function (conditionIndex, valueIndex) {
    var valueInputTD = AQ.getRightValueInputTD(conditionIndex, valueIndex);
    if (!valueInputTD)
        return null;

    var inputTagList = valueInputTD.getElementsByTagName("input");
    if (inputTagList == null || inputTagList.length == 0)
        return null;
    return inputTagList[0];
}

// Any changes related to setParameter code below needs tobe done in AQ.sendDeleteQueryCmd() function as well
AQ.parameterizeActiveQuery = function (cmd) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);

    e1URL.setURI(AQ.MANAGER_SERVICE);
    e1URL.setParameter("cmd", cmd);
    if (AQ.activeQuery.id != null) {
        e1URL.setParameter("id", AQ.activeQuery.id);
    }
    var queryWOBNM = AQ.getActiveQuery().omwObjectName.Trim();
    if (queryWOBNM != null) {
        //Check "*PUBLIC" is appended to WOBNM,if it's appended remove the "*PUBLIC" from the query WOBNM 
        if (queryWOBNM.indexOf(AQ.PUBLIC_QUERY_ID) != -1) {
            queryWOBNM = queryWOBNM.substring(7, queryWOBNM.length);
        }
        e1URL.setParameter("omwObjectName", queryWOBNM);
    }
    e1URL.setParameter("objectType", "QUERY");
    e1URL.setParameter("isCheckedOut", AQ.activeQuery.isCheckedOut);
    e1URL.setParameter("appId", AQ.activeQuery.appId);
    e1URL.setParameter("formId", AQ.activeQuery.formId);
    if (AQ.activeQuery.version != null) {
        e1URL.setParameter("version", AQ.activeQuery.version);
    }
    var dtaInstance = JDEDTAFactory.getInstance(AQ.namespace);
    if (dtaInstance)
        var stackId = dtaInstance.stackId;

    e1URL.setParameter("stackId", stackId);
    e1URL.setParameter("hasToken", AQ.activeQuery.hasToken);
    if (typeof AQ.activeQuery.tokenProjectName != 'undefined' && AQ.activeQuery.tokenProjectName != null) {
        e1URL.setParameter("tokenProjectName", AQ.activeQuery.tokenProjectName);
    }
    return e1URL;
}

AQ.sendDeleteQueryCmd = function (cmd) {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);

    e1URL.setURI(AQ.MANAGER_SERVICE);
    e1URL.setParameter("cmd", cmd);
    e1URL.setParameter("id", AQ.activeQuery.id);
    //e1URL.setParameter("name", WL.HTMLDecoder(WL.activeQuery.name.Trim())); TODO Do we need to trim AQ name?
    e1URL.setParameter("name", AQ.activeQuery.name.Trim());
    if (AQ.activeQuery.omwObjectName != null) {
        e1URL.setParameter("omwObjectName", AQ.activeQuery.omwObjectName.Trim());
    }
    e1URL.setParameter("objectType", "QUERY");
    //e1URL.setParameter("desc", WL.HTMLDecoder(WL.activeWatchlist.desc));
    e1URL.setParameter("appId", AQ.activeQuery.appId);
    e1URL.setParameter("formId", AQ.activeQuery.formId);
    if (AQ.activeQuery.version != null) {
        e1URL.setParameter("version", AQ.activeQuery.version);
    }
    var dtaInstance = JDEDTAFactory.getInstance(AQ.namespace);
    if (dtaInstance)
        var stackId = dtaInstance.stackId;

    e1URL.setParameter("stackId", stackId);
    AQ.setProcInd();
    RIUTIL.sendXMLReq("POST", null, e1URL.toString(), RIUTIL.handlerAjaxRequest);
}

AQ.disableIcon = function (id, outImg, title) {
    var icon = document.getElementById(id);
    if (icon != null) {
        JSSidePanelRender.setMotionImage(icon, outImg, null, null);
        JSSidePanelRender.setAnchorEventHandler(icon, null);
        icon.className = "";
        icon.onclick = null;
        icon.disabled = true;
        if (title) {
            icon.title = title;
            icon.alt = title;
        }
    }
}

AQ.enableIcon = function (id, outImg, overImg, eventHandler, title) {
    var icon = document.getElementById(id);
    if (icon != null) {
        JSSidePanelRender.setMotionImage(icon, outImg, overImg, null);
        JSSidePanelRender.setAnchorEventHandler(icon, eventHandler);
        icon.className = "clickableImage";
        icon.onclick = eventHandler;
        icon.disabled = false;
        if (title) {
            icon.title = title;
            icon.alt = title;
        }
    }
}

AQ.disableAllAQPanelIcons = function () {
    AQ.disableIcon(AQ.ID_SAVE_ICON, window["E1RES_share_images_ulcm_save_dis_png"]);
    AQ.disableIcon(AQ.ID_SAVE_AS_ICON, window["E1RES_share_images_ulcm_saveas_dis_png"], AQ.constants.UDO_SAVE_AS);
    AQ.disableIcon(AQ.ID_AUTO_FIND, window["E1RES_share_images_ulcm_search_ena_png"], AQ.constants.JH_FIND);
    AQ.disableIcon(AQ.ID_RESERVE_ICON, window["E1RES_share_images_ulcm_reserve_dis_png"], AQ.constants.UDO_RESERVE);
    AQ.disableIcon(AQ.ID_PUBLISH_ICON, window["E1RES_share_images_ulcm_requestpublish_dis_png"], AQ.constants.UDO_REQUEST_PUBLISH);
    AQ.disableIcon(AQ.ID_DELETE_ICON, window["E1RES_share_images_ulcm_delete_dis_png"]);
    AQ.disableIcon(AQ.ID_NOTES_ICON, window["E1RES_share_images_ulcm_notes_dis_png"], AQ.constants.UDO_NOTES);
    AQ.disableIcon(AQ.ID_POPULATE_QUERY, window["E1RES_share_images_ulcm_populate_dis_png"]);
    var queryListDropDown = document.getElementById(AQ.ID_QUERY_LIST_SP);
    if (queryListDropDown)
        queryListDropDown.disabled = true;
    AQ.disableCtrls();
}

// Event handler called when the Request Publish Query button is clicked.
AQ.goRequestPublishQueryBegin = function (event) {
    AQ.destoryErrorWindow();
    var doc = document;
    if (AQ.userHasPublishAuthority) {
        if (!AQ.activeQuery.isPrivate) {
            // This is not used right now, since the publish button should be disabled
            // can't publish a public AQ
            AQ.showErrorWindow(doc.getElementById(AQ.ID_QUERY_LIST_SP), AQ.constants.AQ_PUBLIC_QUERY);
        }
        else if (AQ.isDirty()) {
            // save before publish
            AQ.showErrorWindow(doc.getElementById(AQ.ID_QUERY_LIST_SP), AQ.constants.AQ_CHANGES_NOT_SAVED);
        }
        else {
            if (AQ.activeQuery.isPrivate && AQ.activeQuery.isCheckedOut) {
                AQ.activeQuery.event = "reRequestPublish";
            }
            else if (AQ.activeQuery.isPersonal) {
                AQ.activeQuery.event = "requestPublish";
            }
            if (AQ.activeQuery.canShowPublishWarning) {
                var labels = new Array(AQ.constants.UDO_ENTER_APPROVER_NOTE, AQ.constants.UDO_CONFIRM_REQUEST_PUBLISH.format(AQ.constants.UDO_TYPE_AQ), AQ.constants.UDO_CONFIRM_REQ_PUB_OPTION.format(AQ.constants.UDO_TYPE_AQ));
                JSSidePanelRender.renderPromptForName(document, AQ.ID_SAVE_AS_ICON, 1, labels, AQ.NAME_LENGTH, AQ.goRequestPublishQueryEnd, '', '');
            }
            else {
                AQ.goRequestPublishQueryEnd("");
            }
        }
    }
    else {
        // We should not get here - button should have been disabled if no authority
        // if there is a way to get here, show the un-authorized message
        AQ.showErrorWindow(doc.getElementById(AQ.ID_PUBLISH_ICON), AQ.constants.AQ_REQ_PUBLISH_AUTHORITY);
    }
    // For accessibility stop event bubbling else the req publish event is fired twice
    if (AQ.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        WL.stopEventBubbling(evt);
    }
    return false;
}

// Event handler called when the Request Publish button is clicked.
AQ.goRequestPublishQueryEnd = function (notesEntered) {
    var e1URL;
    if (AQ.activeQuery.event == "requestPublish") {
        e1URL = AQ.parameterizeActiveQuery("requestPublish");
    }
    else if (AQ.activeQuery.event == "reRequestPublish") {
        e1URL = AQ.parameterizeActiveQuery("reRequestPublish");
    }
    if (AQ.activeQuery.event == "requestPublish" || AQ.activeQuery.event == "reRequestPublish") {
        e1URL.setParameter("name", AQ.activeQuery.name);
        e1URL.setParameter("approvernotes", notesEntered);
        AQ.setProcInd();
        RIUTIL.sendXMLReq("POST", null, e1URL.toString(), RIUTIL.handlerAjaxRequest);
        return true;
    }
    else {
        return false;
    }
}

// Event handler called when the Reserve Query button is clicked.
AQ.goReserveQuery = function (event) {
    if (AQ.activeQuery.isPrivate && AQ.activeQuery.isCheckedOut && !(AQ.activeQuery.isRequestPublished) && !(AQ.activeQuery.isRework)) {
        AQ.activeQuery.event = "unreserve";
        var e1URL = AQ.parameterizeActiveQuery("unreserve");
    }
    else {
        AQ.activeQuery.event = "reserve";
        var e1URL = AQ.parameterizeActiveQuery("reserve");
    }
    e1URL.setParameter("name", AQ.activeQuery.name);
    AQ.setProcInd();
    RIUTIL.sendXMLReq("POST", null, e1URL.toString(), RIUTIL.handlerAjaxRequest);
    // For accessibility stop event bubbling else the reserve/unreserve event is fired twice
    if (AQ.ACCESSIBILITY) {
        var evt = event ? event : window.event;
        WL.stopEventBubbling(evt);
    }
}

AQ.showNotes = function () {
    var activeQueryId = AQ.activeQuery.id;
    var queryOBNM = AQ.activeQuery.omwObjectName;
    // To remove *PUBLIC from activeQueryId and queryOBNM
    if (activeQueryId.substring(0, 7) == AQ.PUBLIC_QUERY_ID)
        activeQueryId = activeQueryId.substring(7, activeQueryId.length)

    if (queryOBNM.substring(0, 7) == AQ.PUBLIC_QUERY_ID)
        queryOBNM = queryOBNM.substring(7, queryOBNM.length)

    AQ.send("lcmLoadMo." + activeQueryId + AQ.SEP_CHAR + queryOBNM);

}

AQ.showULCMErrorWindow = function (errorDescription) {
    var errorElem = document.getElementById(AQ.ID_QUERY_LIST_SP);
    if (errorElem) {
        AQ.showErrorWindow(errorElem, errorDescription);
    }
    AQ.clearProcInd();
}

AQ.showMessageDiv = function (messageText) {
    var messageDiv = document.getElementById(AQ.ID_MESSAGE_DIV);
    if (messageDiv) {
        messageDiv.style.borderColor = "Red";
        messageDiv.style.display = "block";
        messageDiv.innerHTML = messageText;
    }
    AQ.clearProcInd();
}

// show or hide the reserved by div
AQ.showReservedBy = function () {
    var reservedByDiv = document.getElementById(AQ.ID_RESERVED_BY);
    if (!AQ.activeQuery.isPrivate && !(AQ.activeQuery.isRequestPublished) && !(AQ.activeQuery.isRework))//TODO  && AQ.userHasPublishAuthority)
    {
        if (AQ.activeQuery.reservedBy == undefined || AQ.activeQuery.reservedBy == "") {
            reservedByDiv.style.display = "none";
            if (AQ.activeQuery.hasToken) {
                var tokenHeldByProject = AQ.constants.UDO_TOKEN_BY.format(AQ.activeQuery.tokenProjectName); // TODO Do we need to call ".format" when AQ.activeQuery.tokenProjectName is NULL
                reservedByDiv.style.display = "block";
                // For accessibility users add the ReservedBy information. 
                // If the currently selected Query is the first shared Query then add "Shared Query" to the ReservedBy information.
                if ((AQ.ACCESSIBILITY) && (AQ.queryList.publicItems != null && AQ.queryList.publicItems.length > 0 && AQ.activeQuery.id == AQ.queryList.publicItems[0].id)) {
                    reservedByDiv.innerHTML = "&nbsp;" + AQ.constants.UDO_SHARED + "&nbsp;" + tokenHeldByProject;
                }
                // else just add the ReservedBy information.
                else {
                    reservedByDiv.innerHTML += "&nbsp;" + tokenHeldByProject;
                }
            }
            return;
        }
        else {
            var reservedBy = AQ.constants.EUR_RESERVED_BY.format(AQ.activeQuery.reservedBy);
            reservedByDiv.style.display = "block";
            // For accessibility users add the ReservedBy information. 
            // If the currently selected Query is the first shared Query then add "Shared Query" to the ReservedBy information.
            if ((AQ.ACCESSIBILITY) && (AQ.queryList.publicItems != null && AQ.queryList.publicItems.length > 0 && AQ.activeQuery.id == AQ.queryList.publicItems[0].id)) {
                reservedByDiv.innerHTML = "&nbsp;" + AQ.constants.UDO_SHARED + "&nbsp;" + reservedBy;
            }
            // else just add the ReservedBy information.
            else {
                reservedByDiv.innerHTML += "&nbsp;" + reservedBy;
            }
        }
    }
    else {
        reservedByDiv.style.display = "none";
    }
}

// Make sure the name does not contain forbiden chars. return true if it is ok.
AQ.isValidName = function (name) {
    // do not allow blank or empty name
    if (name == null || name.Trim().length == 0) {
        alert(AQ.constants.UDO_BAD_NAME);
        return false;
    }
    //do not allow to use a certain set of characters in the name
    for (var i = 0, len = AQ.BAD_CHAR_SET.length; i < len; i++) {
        if (name.indexOf(AQ.BAD_CHAR_SET.charAt(i)) != -1) {
            alert(AQ.constants.UDO_BAD_NAME);
            return false;
        }
    }
    return true;
}
AQ.disableCtrls = function () {
    var doc = document;
    var defCheckBox = doc.getElementById(AQ.ID_DEFAULT_QUERY);
    if (defCheckBox)
        defCheckBox.disabled = true;
    var runWhenSelect = doc.getElementById(AQ.ID_AUTO_FIND);
    if (runWhenSelect)
        runWhenSelect.disabled = true;
    var autoClearForm = doc.getElementById(AQ.ID_AUTO_CLEAR);
    if (autoClearForm)
        autoClearForm.disabled = true;
    var matchElems = doc.getElementsByName(AQ.ID_LOGIC);
    if (matchElems) {
        if (matchElems[0])
            matchElems[0].disabled = true;
        if (matchElems[1])
            matchElems[1].disabled = true;
    }
    var expandFieldsSelection = doc.getElementById(AQ.ID_FIELDS_SELECTION_ICON);
    if (expandFieldsSelection) {
        expandFieldsSelection.disabled = true;
        expandFieldsSelection.onclick = null;
        expandFieldsSelection.className = "";
    }
}

AQ.isSaveEnabled = function () {
    var isSaveEnabled = true;
    if (AQ.isManagementMode()) {
        if (AQ.activeQuery.isPrivate) {
            if (AQ.activeQuery.id != AQ.BLANK_QUERY_ID) {
                // AQ is in reserved section
                if (AQ.activeQuery.isCheckedOut && !(AQ.activeQuery.isRequestPublished) && !(AQ.activeQuery.isRework)) {
                    if (!AQ.userHasPublishAuthority || !AQ.userHasReserveAuthority) {
                        isSaveEnabled = false;
                    }
                }
                // AQ in Pending Promotion section/Rework
                else if (AQ.activeQuery.isRequestPublished || AQ.activeQuery.isRework) {
                    isSaveEnabled = false;
                }
            }
            else {
                // AQ is selected with "(new)" option/default
                if (!AQ.activeQuery.canAdd || !AQ.activeQuery.canAddToProject) {
                    isSaveEnabled = false;
                }
            }
        }
        else {
            // AQ is in PUBLIC section
            isSaveEnabled = false;
        }
    }
    if (typeof queryObject != 'undefined' && queryObject.isLcmPreview) {
        isSaveEnabled = false;
    }

    if (AQ.isModifiedAQForm) {
        var privateAQItems = AQ.queryList.privateItems[0];
        if (privateAQItems && privateAQItems.modifiedAQPreivew)
            isSaveEnabled = false;
    }
    return isSaveEnabled;
}


// Public Event Handler for "Save Modified Query Icon"
AQ.goSaveModifiedQuery = function () {
    var query = AQ.getActiveQuery();
    if (query.conditions.length == 0) {
        var message = AQ.constants.AQ_EMPTY_CONDITION;
        AQ.RenderModifiedAlert(message, "MESSAGE");
    }
    else {
        if (!AQ.updateValidation(null)) {
            AQ.send("aqmodifiedsave");
        }
    }
}

// Public Event Handler for "Save Modified Delete Query Icon"
AQ.goDelModifiedQuery = function () {
    var message = AQ.constants.AQ_CONFRIM_DELETE + " (" + AQ.getActiveQuery().name + ")"
    AQ.RenderModifiedAlert(message, "OKCONFIRM");
}

// Private method to disable the dropdown combo-box for modified AQ Panel.
AQ.disableQueryListComboForModAQ = function () {
    var doc = document;
    var queryListDropDown = doc.getElementById(AQ.ID_QUERY_LIST_SP);
    if (queryListDropDown)
        queryListDropDown.disabled = true;
    var AQFormQueryList = doc.getElementById(AQ.ID_QUERY_LIST_E1);
    if (AQFormQueryList)
        AQFormQueryList.disabled = true;

    var privateAQItems = AQ.queryList.privateItems[0];
    if (privateAQItems && privateAQItems.modifiedAQPreivew) {
        AQ.disableAllAQPanelIcons();
    }
}

// Private method to check if we are current working with the blank OBNM.
AQ.isBlankOBNM = function () {
    return (AQ.activeQuery.omwObjectName == AQ.BLANK_QUERY_ID)
}

AQ.RenderModifiedAlert = function (message, alertType) {
    var winWidth = 300;
    var winHeight = 150;
    var winTop = document.body.clientHeight / 3;
    var winLeft = document.body.clientWidth / 2 - winWidth / 2
    var sb = new PSStringBuffer();
    if (alertType == "OKCONFIRM") {
        sb.append("<form><div align='center'><table align='center'><tr><td colspan=2><label class=FavoritesPopupLabel>").append(message).append("</label></td></tr><tr><td colspan=2 align='right'>");
        sb.append("<input type='button' class='FAVbutton' onmouseover=javascript:className='FAVbuttonMouseOver'; onmouseout=javascript:className='FAVbutton'; onmousedown=javascript:className='FAVbuttonMouseDown'; id='ok' value='OK'  onClick='AQ.send(\"aqmodifieddel\");' </input>");
        sb.append("&nbsp;&nbsp;");
        sb.append("<input type='button' class='FAVbutton' onmouseover=javascript:className='FAVbuttonMouseOver'; onmouseout=javascript:className='FAVbutton'; onmousedown=javascript:className='FAVbuttonMouseDown'; id='Cancel' value='Cancel' onClick=\"AQ.iyfeWin.onClose()\"</input>");
        sb.append("</td></tr></table></div></form>");
    }
    else if (alertType == "MESSAGE") {
        sb.append("<form><div align='center'><table align='center'><tr><td colspan=1><label class=FavoritesPopupLabel>").append(message).append("</label></td></tr><tr><td colspan=1 align='center'>");
        sb.append("<input type='button' class='FAVbutton' onmouseover=javascript:className='FAVbuttonMouseOver'; onmouseout=javascript:className='FAVbutton'; onmousedown=javascript:className='FAVbuttonMouseDown'; id='Cancel' value='OK' onClick=\"AQ.iyfeWin.onClose()\"</input>");
        sb.append("</td></tr></table></div></form>");
    }
    else {
        alert("Invalid Style argument passed.")
    }
    var framework = sb.toString()
    var eventListener = {};
    eventListener.onClose = function (e) {
        //alert('Close');
        AQ.iyfeWin.destory();
        AQ.iyfeWin = null;
    }
    // Create the popup
    AQ.iyfeWin = createPopup(AQ.ID_IYFE_WIN, ADVANCED_QUERY, true, false, true, false, AQ.constants.AQ_TITLE_TAB, null, null, null, null, null, winLeft, winTop, winWidth, winHeight, null, framework, eventListener);
}