/* 
This javascript library takes an X and Y array of touch co-ordinates and returns the movement performed
*/
var TouchLib;
if (TouchLib == null) {
    TouchLib = new function () {
        this.debugGesture = false;

        // Movement and simple gestures    
        this.NOTHING = 0;
        this.LEFT = 1;
        this.RIGHT = 2;
        this.UP = 3;
        this.DOWN = 4;

        // Simple Gestures
        this.UP_LEFT = 5;
        this.UP_RIGHT = 6;
        this.DOWN_LEFT = 7;
        this.DOWN_RIGHT = 8;
        // Complex Gestures
        this.TICK = 9;
        this.CROSS = 10;
        this.CAROT = 11;
        this.LETTERL = 12;

        this.UNDEF = -1;
        this.DIRECTION_SENSITIVITY_HIGH = 5;
        this.DIRECTION_SENSITIVITY_MEDIUM = 20;
        this.DIRECTION_SENSITIVITY_LOW = 40;

        this.SENSITIVITY_HIGH = this.DIRECTION_SENSITIVITY_HIGH;
        this.SENSITIVITY_LOW = this.SENSITIVITY_HIGH * -1;

        this.moveXArray = [];
        this.moveYArray = [];

        this.keyA = 65;
        this.keyI = 73;
        this.keyL = 76
        this.keyO = 79;
        this.keyS = 83;

        if (this.debugGesture == true) {
            if (navigator.userAgent.toUpperCase().indexOf("IPAD") > -1) {
                this.canvas = document.getElementById("gesturePad");
                if (!this.canvas) {
                    this.canvas = parent.document.getElementById("gesturePad");
                }
                if (this.canvas) {
                    this.canvas.style.display = "block";
                    this.myContext = this.canvas.getContext("2d");
                    this.myContext.clearRect(0, 0, 250, 250);
                    this.canvas.width = this.canvas.width;
                }
                this.gestureText = document.getElementById("gestureText");
                if (!this.gestureText) {
                    this.gestureText = parent.document.getElementById("gestureText");
                }
                if (this.gestureText) {
                    this.gestureText.style.display = "block";
                    this.gestureText.innerHTML = "";
                }
            }
            else {
                this.debugGesture = false;
            }
        }
    }
}

TouchLib.init = function (XArray, YArray) {
    this.moveXArray = XArray;
    this.moveYArray = YArray;
}

TouchLib.clean = function () {
    this.moveXArray = [];
    this.moveYArray = [];
}

TouchLib.determineAndPerformGesture = function (e, sensitivity) {
    return this.performGenericGesture(e, this.determineGesture(sensitivity));
}

TouchLib.determineGesture = function (sensitivity) {
    if (!sensitivity) {
        sensitivity = this.DIRECTION_SENSITIVITY_HIGH;
    }
    this.SENSITIVITY_HIGH = sensitivity;
    this.SENSITIVITY_LOW = sensitivity * -1;

    if (TouchLib.debugGesture == true) {
        TouchLib.initDebug();
    }

    if (this.moveXArray.length == 0 || this.moveYArray.length == 0) {
        TouchLib.clear();
        return;
    }

    var lastX = this.moveXArray[0];
    var lastY = this.moveYArray[0];
    var angleArray = [];

    var usedX = [];
    var usedY = [];
    usedX.push(this.moveXArray[0]);
    usedY.push(this.moveYArray[0]);

    if (TouchLib.debugGesture == true && parent.CARO) {
        parent.CARO.caroDebugPane.innerHTML = "";
        parent.CARO.debug("X0=" + lastX + " - " + "Y0=" + lastY);
    }

    for (var i = 1; i < this.moveXArray.length; i++) {
        var valueX = this.moveXArray[i];
        var valueY = this.moveYArray[i];

        var diffX = valueX - lastX;
        var diffY = valueY - lastY;

        // if no real movement on both X and Y axis then discard
        if ((diffX <= this.SENSITIVITY_HIGH && diffX >= this.SENSITIVITY_LOW) &&
            (diffY <= this.SENSITIVITY_HIGH && diffY >= this.SENSITIVITY_LOW)) {
            continue;
        }

        usedX.push(valueX);
        usedY.push(valueY);

        var angle = Math.atan2(valueY - lastY, valueX - lastX);
        var degrees = (2 * angle) * 180 / Math.PI;

        if (degrees >= 45 && degrees <= 155) {
            angleArray.push(this.DOWN_RIGHT);
        }
        else if (degrees > 155 && degrees < 205) {
            angleArray.push(this.DOWN);
        }
        else if (degrees >= 205 && degrees <= 315) {
            angleArray.push(this.DOWN_LEFT);
        }
        else if (degrees <= -45 && degrees >= -155) {
            angleArray.push(this.UP_RIGHT);
        }
        else if (degrees < -155 && degrees > -205) {
            angleArray.push(this.UP);
        }
        else if (degrees <= -205 && degrees >= -315) {
            angleArray.push(this.UP_LEFT);
        }
        else if (degrees > -180 && degrees < 180) {
            angleArray.push(this.RIGHT);
        }
        else {
            angleArray.push(this.LEFT);
        }

        if (TouchLib.debugGesture == true && parent.CARO) {
            parent.CARO.debug("X" + i + "=" + this.moveXArray[i] + " - " + "Y" + i + "=" + this.moveYArray[i] + " == Angle: " + degrees + " :: " + angleArray[angleArray.length - 1]);
        }

        lastX = valueX;
        lastY = valueY;
    }

    var gestureType = this.doGestureMagic(angleArray);

    if (TouchLib.debugGesture == true) {
        TouchLib.drawGestureForDebug(gestureType, usedX, usedY);
    }

    this.clean();

    return gestureType;
}

TouchLib.doGestureMagic = function (angleArray) {
    switch (angleArray[0]) {
        case this.DOWN:
            switch (angleArray[angleArray.length - 1]) {
                case this.DOWN:
                    // start down, end down
                    return this.DOWN;
                    break;
                case this.RIGHT:
                    // start down, end right
                    return this.LETTERL;
                    break;
            }
            break;
        case this.UP:
            switch (angleArray[angleArray.length - 1]) {
                case this.UP:
                    // start up, end up
                    return this.UP;
                    break;
            }
            break;
        case this.RIGHT:
            switch (angleArray[angleArray.length - 1]) {
                case this.RIGHT:
                    // start right, end right
                    return this.RIGHT;
                    break;
            }
            break;
        case this.LEFT:
            switch (angleArray[angleArray.length - 1]) {
                case this.LEFT:
                    // start left, end left
                    return this.LEFT;
                    break;
            }
            break;
        case this.UP_RIGHT:
            switch (angleArray[angleArray.length - 1]) {
                case this.DOWN_RIGHT:
                    // start up/right, end down/right
                    return this.CAROT;
                    break;
                case this.UP_RIGHT:
                    return this.UP_RIGHT;
                    break;
            }
            break;
        case this.DOWN_RIGHT:
            switch (angleArray[angleArray.length - 1]) {
                case this.UP_RIGHT:
                    // start down/right, end up/right
                    return this.TICK;
                    break;
                case this.DOWN_LEFT:
                case this.DOWN:
                    // if there is a up in the middle then it must be a cross    
                    for (var i = 1; i < angleArray.length; i++) {
                        if (angleArray[i] == this.UP) {
                            return this.CROSS;
                        }
                    }
                    break;
                case this.DOWN_RIGHT:
                    return this.DOWN_RIGHT;
                    break;
            }
            break;
        case this.UP_LEFT:
            switch (angleArray[angleArray.length - 1]) {
                case this.UP_LEFT:
                    return this.UP_LEFT;
                    break;
            }
            break;
        case this.DOWN_LEFT:
            switch (angleArray[angleArray.length - 1]) {
                case this.DOWN_LEFT:
                    return this.DOWN_LEFT;
                    break;
            }
            break;
    }
    return this.NOTHING;
}

TouchLib.performGenericGesture = function (e, gesture) {
    switch (gesture) {
        case TouchLib.LEFT:
        case TouchLib.RIGHT:
            e.preventDefault();
            if (self.parent && self.parent.MENUUTIL) {
                self.parent.MENUUTIL.processChildTouchEvents(e, gesture);
            }
            return this.stopEventBubbling(e);
            break;
    }
}

TouchLib.stopEventBubbling = function (e) {
    if (e == null) //meaning isIE
    {
        window.event.cancelBubble = true;
        window.event.returnValue = false;
    }
    else {
        e.stopPropagation();
        e.preventDefault();

        if (e.cancelBubble) {
            e.cancelBubble = true;
        }
    }
    return false;
}

TouchLib.processGesture = function (gesture, myWindow, appDoc, e) {
    if (appDoc) {
        var activeElement = appDoc.all ? appDoc.activeElement : e.target;
        if (myWindow.JDEDTAFactory && myWindow.JSCompMgr) {
            var namespace = myWindow.JDEDTAFactory.getFirstNamespace();
            var rootContainer = myWindow.JSCompMgr.getRootContainer(namespace);
            if (rootContainer && rootContainer != null) {
                TouchLib.procGestureHotKey(gesture, rootContainer, namespace, activeElement);
            }
            else {
                TouchLib.procGestureButton(gesture, appDoc, e);
            }
        }
        else {
            TouchLib.procGestureButton(gesture, appDoc, e);
        }

    }
    TouchLib.clean();
}

TouchLib.procGestureHotKey = function (gesture, rootContainer, namespace, activeElement) {
    switch (gesture) {
        case TouchLib.TICK:
            var evtHandled = rootContainer.processHotkey(namespace, activeElement, this.keyS, true, true, false);
            if (evtHandled != true) {
                evtHandled = rootContainer.processHotkey(namespace, activeElement, this.keyO, true, true, false);
            }
            break;
        case TouchLib.CROSS:
        case TouchLib.LETTERL:
            var evtHandled = rootContainer.processHotkey(namespace, activeElement, this.keyL, true, true, false);
            break;
        case TouchLib.CAROT:
            var evtHandled = rootContainer.processHotkey(namespace, activeElement, this.keyA, true, true, false);
            break;
        case TouchLib.DOWN:
            var evtHandled = rootContainer.processHotkey(namespace, activeElement, this.keyI, true, true, false);
            break;
        case TouchLib.LEFT:
            break;
        default:
            break;
    }
}

TouchLib.procGestureButton = function (gesture, appDoc, e) {
    switch (gesture) {
        case TouchLib.TICK:
            var button = appDoc.getElementById("hc_Select");
            if (!button) {
                button = appDoc.getElementById("hc_OK");
            }
            if (!button) {
                button = appDoc.getElementById("hc1");
            }
            if (!button) {
                button = appDoc.getElementById("BUTTONSAVEEXIT");
            }
            if (button) {
                button.onclick(e);
            }
            break;
        case TouchLib.CROSS:
            var button = appDoc.getElementById("hc_Close");
            if (!button) {
                button = appDoc.getElementById("hc_Cancel");
            }
            if (!button) {
                button = appDoc.getElementById("hc2");
            }
            if (!button) {
                button = appDoc.getElementById("BUTTONCANCEL");
            }
            if (button) {
                button.onclick(e);
            }
            break;
        case TouchLib.LETTERL:
            var button = appDoc.getElementById("hc_Close");
            if (!button) {
                button = appDoc.getElementById("hc_Cancel");
            }
            if (!button) {
                button = appDoc.getElementById("hc2");
            }
            if (!button) {
                button = appDoc.getElementById("BUTTONCANCEL");
            }
            if (button) {
                button.onclick(e);
            }
            break;
        default:
            break;
    }
}

TouchLib.initDebug = function () {
    if (!this.canvas) {
        this.canvas = document.getElementById("gesturePad");
    }
    if (this.canvas) {
        this.myContext = this.canvas.getContext("2d");
        this.myContext.clearRect(0, 0, 250, 250);
        this.canvas.width = this.canvas.width;
    }
    if (!this.gestureText) {
        this.gestureText = document.getElementById("gestureText");
    }
    if (this.gestureText) {
        this.gestureText.innerHTML = "";
    }
}

TouchLib.drawGestureForDebug = function (gestureType, usedX, usedY) {
    var highY = this.moveYArray[0];
    var lowY = this.moveYArray[0];
    var highX = this.moveXArray[0];
    var lowX = this.moveXArray[0];
    for (var i = 0; i < usedY.length; i++) {
        if (usedY[i] < lowY) { lowY = usedY[i]; }
        if (usedY[i] > highY) { highY = usedY[i]; }
    }
    lowY -= 5;
    highY += 5;
    for (var i = 0; i < usedX.length; i++) {
        if (usedX[i] < lowX) { lowX = usedX[i]; }
        if (usedX[i] > highX) { highX = usedX[i]; }
    }
    lowX -= 5;
    highX += 5;
    var multiplierY = 1;
    var multiplierX = 1;
    if ((highY - lowY) > this.canvas.height) {
        multiplierY = this.canvas.height / (highY - lowY);
    }
    if ((highX - lowX) > this.canvas.width) {
        multiplierX = this.canvas.width / (highX - lowX);
    }
    this.myContext.moveTo((this.moveXArray[0] - lowX) * multiplierX, (this.moveYArray[0] - lowY) * multiplierY);
    for (var i = 0; i < usedX.length; i++) {
        this.myContext.lineTo((usedX[i] - lowX) * multiplierX, (usedY[i] - lowY) * multiplierY);
        this.myContext.strokeStyle = "#ff0000";
        this.myContext.stroke();
    }

    var latestGesture = "";

    switch (gestureType) {
        case this.TICK:
            latestGesture = "<h1>TICK</h1>";
            break;
        case this.CROSS:
            latestGesture = "<h1>CROSS</h1>";
            break;
        case this.CAROT:
            latestGesture = "<h1>CAROT</h1>";
            break;
        case this.LETTERL:
            latestGesture = "<h1>Letter L</h1>";
            break;
        case this.DOWN:
            latestGesture = "<h1>DOWN</h1>";
            break;
        case this.LEFT:
            latestGesture = "<h1>LEFT</h1>";
            break;
        case this.RIGHT:
            latestGesture = "<h1>RIGHT</h1>";
            break;
        case this.UP:
            latestGesture = "<h1>UP</h1>";
            break;
        case this.UP_LEFT:
            latestGesture = "<h1>UP_LEFT</h1>";
            break;
        case this.UP_RIGHT:
            latestGesture = "<h1>UP_RIGHT</h1>";
            break;
        case this.DOWN_LEFT:
            latestGesture = "<h1>DOWN_LEFT</h1>";
            break;
        case this.DOWN_RIGHT:
            latestGesture = "<h1>DOWN_RIGHT</h1>";
            break;
        default:
            latestGesture = "<h1>NOT HANDLED: " + gestureType + "</h1>";
            break;
    }
    this.gestureText.innerHTML = latestGesture;
}

var FRAMETOUCH;
if (FRAMETOUCH == null) {
    FRAMETOUCH = new function () {
        this.nbrFingers = 0;
        this.startTouchEvent = false;
        this.moveXArray = [];
        this.moveYArray = [];
        this.preventDefault = false;
    }
}

// Process touch events from iOS and other touch OSs.
FRAMETOUCH.touchFrameStart = function (e) {
    var frameTouch = FRAMETOUCH;
    var tFingers = e.touches.length;
    if (frameTouch.startTouchEvent == true) {
        return;
    }
    // a single finger touch
    if (tFingers == 1) {
        var touch = e.touches[0];
        if (touch && touch.pageX > 6) {
            // only accept one finger touches in the first 5 pixels of the frame
            frameTouch.clearTouch();
            return;
        }
    }
    else {
        frameTouch.clearTouch();
        return;
    }
    frameTouch.nbrFingers = tFingers;
    frameTouch.startTouchEvent = true;
    e.preventDefault();
}

FRAMETOUCH.touchFrameCancel = function (e) {
    FRAMETOUCH.clearTouch();
}

FRAMETOUCH.touchFrameMove = function (e) {
    var frameTouch = FRAMETOUCH;

    if (frameTouch.startTouchEvent == false) {
        return;
    }
    if (frameTouch.nbrFingers == 1) {
        e.preventDefault();
        frameTouch.moveXArray.push(e.changedTouches[0].clientX);
        frameTouch.moveYArray.push(e.changedTouches[0].clientY);
    }
}

FRAMETOUCH.touchFrameEnd = function (e) {
    var frameTouch = FRAMETOUCH;
    // Only process 1,2 or 3 finger events
    if (frameTouch.startTouchEvent == false || frameTouch.nbrFingers != 1) {
        frameTouch.clearTouch();
        return;
    }
    // 3 finger events need to move a certain distance before acceptance
    if (frameTouch.nbrFingers != 1) {
        frameTouch.clearTouch();
        return;
    }
    if (frameTouch.moveXArray.length < 1) {
        frameTouch.clearTouch();
        return;
    }
    TouchLib.init(frameTouch.moveXArray, frameTouch.moveYArray);
    var gesture = TouchLib.determineGesture(TouchLib.DIRECTION_SENSITIVITY_HIGH);

    if (!this.currentAppFrame) {
        this.currentAppFrame = document.getElementById("e1menuAppIframe");
    }

    var appDoc = this.currentAppFrame.contentDocument;
    if (appDoc) {
        TouchLib.processGesture(gesture, this.currentAppFrame.contentWindow, appDoc, e);
    }
    frameTouch.clearTouch();
}


FRAMETOUCH.clearTouch = function (clear) {
    TouchLib.clean();
    FRAMETOUCH.nbrFingers = 0;
    FRAMETOUCH.startTouchEvent = false;
    FRAMETOUCH.moveXArray = [];
    FRAMETOUCH.moveYArray = [];
}

FRAMETOUCH.disableTouchEvents = function (elem) {
    if (elem)
        elem.ontouchstart = elem.ontouchmove = elem.ontouchend = elem.ontouchcancel = null;
}

FRAMETOUCH.enableTouchEvents = function (elem) {
    if (elem) {
        elem.ontouchstart = FRAMETOUCH.touchFrameStart;
        elem.ontouchmove = FRAMETOUCH.touchFrameMove;
        elem.ontouchend = FRAMETOUCH.touchFrameEnd;
        elem.ontouchcancel = FRAMETOUCH.touchFrameCancel;
    }
}