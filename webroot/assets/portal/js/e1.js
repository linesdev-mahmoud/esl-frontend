// This file contains common javascript need to be loaded for every page of E1 which is using ResourceCanonicals
function E1URLFactory(containerId, type, baseUrl) {
    this.init(containerId, type, baseUrl);
}

E1URLFactory.prototype.URL_TYPE_RES = "RES";
E1URLFactory.prototype.URL_TYPE_SERVICE = "SERVICE";
E1URLFactory.prototype.CON_ID_WSRP = "Wsrp";
E1URLFactory.prototype.CON_ID_JPDK = "Jpdk";

E1URLFactory.prototype.init = function (containerId, baseUrl, URLBuilderService) {
    gContainerId = containerId;
    // Define E1RemotURL
    function E1RemoteURL(type) {
        this.mParameters = [];
        this.mHasParameters = false;
        this.setParameter("URLBuilderService_Type_Parm", type);
        this.mRequest = this.createXMLHttpRequest();
    }

    E1RemoteURL.prototype.processResponse = function () {
        if ((this.readyState == 4) && (this.status == 200)) {
            if (containerId == this.CON_ID_JPDK) {
                var matches = this.mRequest.responseText.match(/<JPDKWrapper>(.*?)<\/JPDKWrapper>/);
                return matches[1];
            }
            else {
                var response = this.responseText;
                if (response) {
                    var callback = this.informationRecipientFunction;
                    if (callback) {
                        if (callback == "setAppIframeURL")
                            setAppIframeURL(response);
                    }
                    else {
                        return response;
                    }
                }
                else {
                    return;
                }
            }
        }
    }

    if (containerId == this.CON_ID_WSRP) //WSRP     
    {
        E1URLFactory.prototype.createInstance = function (type) {
            return new E1RemoteURL(type);
        }
    }
    else // Jpdk, jsr168 and standalong
    {
        // Define E1LocalURL
        function E1LocalURL() {
            this.mUrl = [];
            this.mHasParameters = false;
        }

        E1LocalURL.prototype.mBaseUrl = baseUrl;
        E1URLFactory.prototype.createInstance = function (type) {
            if (type == this.URL_TYPE_RES) // resource
            {
                return new E1LocalURL();
            }
            else // service
            {
                return new E1RemoteURL(type);
            }
        }

        E1LocalURL.prototype.setParameter = function (n, v) {
            if (this.mHasParameters == false) {
                this.mUrl.push("?");
                this.mHasParameters = true;
            }
            else {
                this.mUrl.push("&");
            }
            this.mUrl.push(n);
            this.mUrl.push("=");
            this.mUrl.push(v);
        }

        E1LocalURL.prototype.toString = function () {
            return this.mUrl.join("");
        }

        E1LocalURL.prototype.setURI = function (uri) {
            this.mUri = uri;
            this.mUrl.push(this.mBaseUrl);
            this.mUrl.push(uri)
        }
    }

    E1RemoteURL.prototype.mURLBuilderService = URLBuilderService;
    E1RemoteURL.prototype.toStringAsync = function (callback) {
        //XMLHttpRequest Here
        if (this.mRequest) {
            this.initXMLHttpRequestAsync();
            this.mRequest.send(this.mParameters.join(""));
            if (callback != undefined) {
                this.mRequest.informationRecipientFunction = callback;
            }
            return this.processResponse();
        }
    }
    E1RemoteURL.prototype.toString = function () {
        //XMLHttpRequest Here
        if (this.mRequest) {
            this.initXMLHttpRequest();
            this.mRequest.send(this.mParameters.join(""));
            return this.getResponseText();
        }
    }

    E1RemoteURL.prototype.getRemoteServiceURL = function () {
        return this.mURLBuilderService;
    }


    E1RemoteURL.prototype.setParameter = function (n, v) {
        if (this.mHasParameters == false) {
            this.mHasParameters = true;
        }
        else {
            this.mParameters.push("&");
        }
        this.mParameters.push("e1.maf.");
        this.mParameters.push(encodeURIComponent(n));
        this.mParameters.push("=");
        this.mParameters.push(encodeURIComponent(v));
    }

    if (containerId == this.CON_ID_JPDK) {
        E1RemoteURL.prototype.getResponseText = function () {
            var matches = this.mRequest.responseText.match(/<JPDKWrapper>(.*?)<\/JPDKWrapper>/);
            return matches[1];
        }
    }
    else {
        E1RemoteURL.prototype.getResponseText = function () {
            return this.mRequest.responseText;
        }
    }

    E1RemoteURL.prototype.setURI = function (uri) {
        this.mUri = uri;
        this.setParameter("URLBuilderService_URI_Parm", uri);
    }

    E1RemoteURL.prototype.createXMLHttpRequest = function () {
        var request = false;
        if (window.XMLHttpRequest) {
            request = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            try {
                request = new ActiveXObject("Msxml2.XMLHTTP");
            }
            catch (othermicrosoft) {
                try {
                    request = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (failed) {
                    request = false;
                }
            }
        }
        else {
            alert("Error initializing XMLHttpRequest!");
        }

        return request;
    }

    E1RemoteURL.prototype.initXMLHttpRequestAsync = function () {
        this.mRequest.onreadystatechange = this.processResponse;
        this.mRequest.open("POST", this.getRemoteServiceURL(), true);
        this.mRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }

    E1RemoteURL.prototype.initXMLHttpRequest = function () {
        this.mRequest.open("POST", this.getRemoteServiceURL(), false);
        this.mRequest.onreadystatechange = function () { };
        this.mRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
}