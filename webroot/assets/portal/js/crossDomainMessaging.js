var JDEMessenger = new function () {
    this.debug = false;

    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }

    this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE = "CrossDomainValidation.mafService";
    this.JAS_HELP_SERVICE = "Help.mafService";
    this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_TARGET_URL_REQUEST_PARAM_NAME = "FuseTargetURLRequest";
    this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_SERVER_URL_VALIDATION_REQUEST_PARAM_NAME = "FuseServerURLValidationRequest";
    this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_HANDSHAKE = "FuseHandshake=";
    this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_JSESSIONID = "FuseJSessionID=";
    this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_JAS_SERVER = "jasServer=";

    //SERVICE TYPES    
    this.JAS_FUSE_TARGET_REQUEST_URL = 1;
    this.JAS_FUSE_SERVER_VALIDATION_URL = 2;
    this.HELP_URL = 3;

    // postMessage() MESSAGE TYPES
    // Head's up:  The constant values that follow are mirrored to the same constants on the Fuse UI client.
    // Not all are used here and may exist in this file for reference purposes only.
    //this.ACK = -1;

    //Messages to JAS    
    this.JAS_LOG_OUT = 1;
    this.ADF_JSESSION = 2;
    this.BTF_CLOSE_EXTERNAL = 8; //New type of close with support for external forms
    this.RUN_CLASSIC_APP = 4;
    this.RUN_E1_TASK = 5;
    this.HELP = 6;
    this.ABOUT = 7;

    //Messages to Fuse    
    this.FUSE_GO_HOME = 102;
    this.FUSE_RUN_NEW_APP = 103;

    this.sendMessage = function (targetWindow, targetWindowDomain, messageType, messageData) {
        if (targetWindow == null) {
            return -1;  //Target window required.
        }
        var messageObject = JSON.stringify({ "type": messageType, "message": messageData });
        if (targetWindowDomain == null || targetWindowDomain == "*") {
            var params = {};
            params[JDEMessenger.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_TARGET_URL_REQUEST_PARAM_NAME] = "true";
            E1AJAX.sendXMLReq("GET", "E1Container", JDEMessenger.buildServiceURL(JDEMessenger.JAS_FUSE_TARGET_REQUEST_URL, params),
                function (containerId, responseText) {
                    if (!responseText || responseText == "" || responseText == "null") {
                        JDEMessenger.logError(JDEMessenger.JAS_EMPTY_RESPONSE_ERROR);
                    }
                    else {
                        JDEMessenger.logMessage(JDEMessenger.MESSAGE_SENT_ACTION, window.location.origin, responseText, messageObject);
                        targetWindow.postMessage(messageObject, responseText);
                    }
                }
            );
        }
        else {
            JDEMessenger.logMessage(JDEMessenger.MESSAGE_SENT_ACTION, window.location.origin, targetWindowDomain, messageObject);
            targetWindow.postMessage(messageObject, targetWindowDomain);
        }
    }

    this.receiveMessage = function (srcWindow, srcOrigin, messageObject) {
        /* Validate the source origin before doing aything.
        * The function calls CrossDomainValidation service for the same.
        */
        messageObject = JSON.parse(messageObject);
        JDEMessenger.logMessage(JDEMessenger.MESSAGE_RECEIVED_ACTION, srcOrigin, window.location.origin, messageObject);
        var params = {};
        params[JDEMessenger.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_SERVER_URL_VALIDATION_REQUEST_PARAM_NAME] = srcOrigin;
        E1AJAX.sendXMLReq("GET", "E1Container", JDEMessenger.buildServiceURL(JDEMessenger.JAS_FUSE_SERVER_VALIDATION_URL, params),
            function (containerId, responseText) {
                if (responseText == "true") {
                    var messageType = messageObject.type,
                        message = messageObject.message;
                    switch (messageType) {
                        case JDEMessenger.ACK:
                            break;

                        case JDEMessenger.JAS_LOG_OUT:
                            JDEMessenger.doLogout();
                            break;

                        case JDEMessenger.ADF_JSESSION:
                            JDEMessenger.performRedirectAfterHandshake(messageObject.message);
                            break;

                        case JDEMessenger.BTF_CLOSE_EXTERNAL:
                            JDEMessenger.closeADFAppInJAS(messageObject.message.btfInfo, messageObject.message.stateId);
                            break;

                        case JDEMessenger.HELP:
                            //popup help window based on app name
                            showHelp(messageObject.message);
                            break;

                        case JDEMessenger.ABOUT:
                            //popup about window based on app name
                            //TODO
                            break;

                        case JDEMessenger.RUN_CLASSIC_APP:
                            //RunNewApp(launchAction, promptPO, appID, appType, formID, version, mode, menuSysCode, taskName, formDSTmpl, formDSData,launchInNewWindow, accessibilityMode, nodeId, taskNameLabel, RID, menuNodeInstanceId, queryId)
                            RunNewApp("launchForm", "", message["appId"], "APP", message["formId"], message["version"], "", "", "", message["formDSTmpl"], message["formDSData"], false, false, "", "", "", "", "");
                            break;

                        case JDEMessenger.RUN_E1_TASK:
                            doFastPath(message["taskId"], "");
                            break;

                        default:
                            break;
                    }
                }
                else {
                    JDEMessenger.logMessage(JDEMessenger.MESSAGE_REJECTED_ACTION, srcOrigin, window.location.origin, messageObject);
                    return;
                }
            });
    }

    function showHelp(msgData) {
        var appId = msgData["appId"];
        var productCode = msgData["productCode"];
        var stamp = new Date().getTime();
        var helpName = "OneWorldHelp" + stamp;
        var params = {};
        params["WideContextID"] = appId;
        params["id"] = stamp;
        params["ProductCode"] = productCode;
        e1UrlCreator = JDEMessenger.buildServiceURL(JDEMessenger.HELP_URL, params);
        window.open(e1UrlCreator, helpName, "toolbar=no,width=740,height=540,resizable");
    }

    /* Send AJAX Request with JSESSION to E1 Backend to perform the handshake, after the hanshake is established
    redirect the iframe to the ADF Container application*/
    this.performRedirectAfterHandshake = function (jsessiontext) {
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        }

        var postDoc = new Array(8), iStr = 0;
        postDoc[iStr++] = JDEMessenger.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_HANDSHAKE;
        postDoc[iStr++] = true;
        postDoc[iStr++] = "&";
        postDoc[iStr++] = JDEMessenger.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_FUSE_JSESSIONID;
        postDoc[iStr++] = jsessiontext;
        postDoc[iStr++] = "&";
        postDoc[iStr++] = JDEMessenger.JAS_CROSS_DOMAIN_VALIDATION_SERVICE_JAS_SERVER;
        postDoc[iStr++] = window.location.origin;

        var requestDocString = postDoc.join(""); //remove commas from array representation of parameters                

        E1AJAX.sendXMLReq("POST", "E1Container", JDEMessenger.buildServiceURL(JDEMessenger.JAS_FUSE_TARGET_REQUEST_URL),
          function (containerId, responseText) {
              if (responseText || responseText != "" || responseText != "null") {
                  //launch the container in the hidden iframe
                  launchContainer(responseText);
              }
          }
         , requestDocString);
    }

    /*Send POST to JAS
    btfInfo has instanceId(stackId).*/
    this.closeADFAppInJAS = function (btfInfo, stateId) {
        if (ExternalAppsHandler) {
            ExternalAppsHandler.closeExternalApplication(btfInfo, stateId);
        }
    }

    this.buildServiceURL = function (urlType, params) {
        var url = "";
        switch (urlType) {
            case this.JAS_FUSE_TARGET_REQUEST_URL:
                url = url.concat(this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE);
                break;

            case this.JAS_FUSE_SERVER_VALIDATION_URL:
                url = url.concat(this.JAS_CROSS_DOMAIN_VALIDATION_SERVICE);
                break;

            case this.HELP_URL:
                url = url.concat(this.JAS_HELP_SERVICE);
                break;
        }
        if (params) {
            url = url.concat("?");
            var paramArr = new Array();
            for (var property in params) {
                paramArr.push("".concat(property, "=", params[property]));
            }
            url = url.concat(paramArr.join("&"));
        }
        return url;
    }

    /*LOGGING*/
    this.MESSAGE_SENT_ACTION = "SENT";
    this.MESSAGE_RECEIVED_ACTION = "RECEIVED";
    this.MESSAGE_REJECTED_ACTION = "REJECTED";

    this.JAS_VALIDATION_FAILED_ERROR = 0;
    this.JAS_EMPTY_RESPONSE_ERROR = 1;
    this.FUSE_VALIDATION_FAILED_ERROR = 2;
    this.FUSE_EMPTY_RESPONSE_ERROR = 3;

    this.logMessage = function (action, from, to, messageObject) {
        if (typeof messageObject === 'string') {
            messageObject = JSON.parse(messageObject);
        }
        var type = messageObject.type,
            data = messageObject.message;
        if (JDEMessenger.debug) {
            var typeInStr;
            switch (type) {
                case JDEMessenger.JAS_LOG_OUT:
                    typeInStr = "JAS_LOG_OUT";
                    break;

                case JDEMessenger.FUSE_GO_HOME:
                    typeInStr = "FUSE_GO_HOME";
                    break;

                case JDEMessenger.ADF_JSESSION:
                    typeInStr = "ADF_JSESSION";
                    break;

                case JDEMessenger.FUSE_RUN_NEW_APP:
                    typeInStr = "FUSE_RUN_NEW_APP";
                    break;

                case JDEMessenger.BTF_CLOSE_EXTERNAL:
                    typeInStr = "BTF_CLOSE_EXTERNAL";
                    break;

                case JDEMessenger.RUN_CLASSIC_APP:
                    typeInStr = "RUN_CLASSIC_APP";
                    break;

                case JDEMessenger.RUN_E1_TASK:
                    typeInStr = "RUN_E1_TASK";
                    break;


                case JDEMessenger.HELP:
                    typeInStr = "HELP";
                    break;

                default:
                    typeInStr = "UNRECOGNIZED";
                    break;
            }

            console.log("\nACTION: " + action.toUpperCase() + " | TYPE: " + typeInStr + " | FROM: " + from + " | TO: " + to);

            if (data) {
                var dataAsString = '';
                for (var property in data) {
                    dataAsString += property + '|' + data[property] + '; ';
                }
                console.log("DATA: " + dataAsString);
            }
        }
    }

    this.logError = function (type) {
        if (JDEMessenger.debug) {
            var error = "ERROR: ";
            switch (type) {
                case JDEMEssenger.JAS_VALIDATION_FAILED_ERROR:
                    console.error(error + "FUSE URL is not present in whitelist on JAS.");
                    break;

                case JDEMEssenger.JAS_EMPTY_RESPONSE_ERROR:
                    console.error(error + "JAS response is empty for Fuse Target URL.");
                    break;

                case JDEMEssenger.FUSE_VALIDATION_FAILED_ERROR:
                    console.error(error + "JAS URL is not present in whitelist on FUSE");
                    break;

                case JDEMEssenger.FUSE_EMPTY_RESPONSE_ERROR:
                    console.error(error + "FUSE response is empty for JAS Target URL.");
                    break;

                default:
                    console.error(error + "Unknown Error.");
                    break;
            }
        }
    }

    this.doLogout = function () {
        var logoutLink = document.getElementById("e1LogoutLink");
        if (logoutLink != null) {
            logoutLink.click();
        }
    }

    this.doRedirect = function () {

    }

    this.initADFSessionImpl = function (target, handshakeId, isADFEnabled, theAdfHandshakeIframe) {

        if (isADFEnabled == "false") {
            if (target && target != "") {

                //For browsers with no window.location.origin support
                if (!window.location.origin) {
                    window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
                }
                theAdfHandshakeIframe.src = target + "?jasServer=" + window.location.origin;
            }
        }
        else //adf is already enabled put the handshake id info attributes on the iframe (F5 refresh issue)
        {
            theAdfHandshakeIframe.setAttribute("adfHandshake", "true");
            theAdfHandshakeIframe.setAttribute("adfHandshakeId", encodeURIComponent(handshakeId));
        }
    }

    this.launchContainerImpl = function (handshakeId, target) {
        if (target && target != "") {
            var theAdfHandshakeIframe = document.getElementById("e1ExternalAppIframe");
            //For browsers with no window.location.origin support
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            }

            theAdfHandshakeIframe.src = target + "?jasServer=" + window.location.origin + "&handshakeId=" + handshakeId;
            theAdfHandshakeIframe.setAttribute("adfHandshake", "true");
            theAdfHandshakeIframe.setAttribute("adfHandshakeId", encodeURIComponent(handshakeId));
        }
    }


};

if (window.addEventListener) {
    window.addEventListener('message', function (e) {
        JDEMessenger.receiveMessage(e.source, e.origin, e.data);
    });
}
else if (window.attachEvent) {
    window.attachEvent('onmessage', function (e) {
        JDEMessenger.receiveMessage(e.source, e.origin, e.data);
    });
}

 