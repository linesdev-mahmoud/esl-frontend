﻿/*
// dropdownMenu - Javascript code for executing and rendering DropDownMenus
*/
var DDMENU;
if (!DDMENU) {
    DDMENU = {
        CONST: {
            // all hard coded id names
            ELEMENT_NAVIGATOR_DIV: "e1MMenuRoot",
            ELEMENT_FAVORITES_DIV: "e1MMenuFav",
            ELEMENT_OPENAPPTABLE: "OPENAPPTABLE",
            ELEMENT_ROLETABLE: "e1MRoleTable",
            ELEMENT_PREFERENCE_DIV: "preferenceDiv",
            ELEMENT_MENU_FASTPATH_DIV: "menuAndFastPathContainer",
            ELEMENT_MENUTITLE_DIV: "menutitle",
            ELEMENT_OUTERRCUX_DIV: "outerRCUX",
            ELEMENT_TE_FAST_PATH_BOX: "TE_FAST_PATH_BOX",
            ELEMENT_MENUWATCHLISTS: "e1MMenuWatchlists",
            ELEMENT_RECRPTSDIV: "recRptsDiv",
            ELEMENT_DROP_MAINMENU: "drop_mainmenu",
            ELEMENT_DROP_FAVMENUS: "drop_fav_menus",
            ELEMENT_DROP_OPENAPPS: "drop_openapps",
            ELEMENT_DROP_REPORTMENU: "drop_reportmenu",
            ELEMENT_DROP_WATCHLIST_MENUS: "drop_watchlist_menus",
            ELEMENT_DROP_NAV_LABEL: "navlabel",
            ELEMENT_DROP_NAV_LABEL_TD: "navlabel_td",
            ELEMENT_DROP_NAV_LABEL_ICON: "drop_nav_icon",
            ELEMENT_DROP_OPEN_APP_ICON: "drop_Open_App_icon",
            ELEMENT_DROP_OPEN_APP_TD: "drop_Open_App_td",
            ELEMENT_DROP_OPEN_APP_DIV: "drop_Open_App_div",
            ELEMENT_DROP_RPT_ICON: "drop_reportmenu_icon",
            ELEMENT_DROP_RPT_TD: "drop_reportmenu_td",
            ELEMENT_DROP_RPT_DIV: "drop_reportmenu_div",
            ELEMENT_DROP_FAV_TD: "drop_fav_menus_td",
            ELEMENT_DROP_FAV_ICON: "drop_fav_menus_icon",
            ELEMENT_DROP_FAV_DIV: "drop_fav_menus_div",
            ELEMENT_DROP_WL_TD: "drop_watchlist_menus_td",
            ELEMENT_DROP_WL_DIV: "drop_watchlist_menus_div",
            ELEMENT_DROP_WL_ICON: "drop_watchlist_menus_icon",

            //dropdown downarrow image constants
            ELEMENT_MAINMENU_DOWNARROW: "mainmenu_downarrow",
            ELEMENT_FAVMENU_DOWNARROW: "favmenu_downarrow",
            ELEMENT_OPENAPPS_DOWNARROW: "openapps_downarrow",
            ELEMENT_REPORTMENU_DOWNARROW: "reportmenu_downarrow",
            ELEMENT_ROLE_DOWNARROW: "Role_downarrow",
            ELEMENT_ROLE_ICON: "rolesIcon",
            ELEMENT_ROLE_LABEL: "rolelabel",
            ELEMENT_PREFERENCES_DOWNARROW: "preferences_downarrow",
            ELEMENT_PREFERENCES_ICON: "personalizationIcon",
            ELEMENT_PREFERENCES_LABEL: "perlabel",
            ELEMENT_WATCHLISTMENU_DOWNARROW: "watchlistmenu_downarrow",

            ELEMENT_ROLE: "Role",
            ELEMENT_PREFERENCES: "preferences",
            ELEMENT_ROLEBACKLINK: "e1MRoleBacklink",
            ELEMENT_ROLEFORM: "e1MRoleForm",
            DELETEALL: "DeleteAll",
            //globalmaskframe
            ELEMENT_DEFAULTGLOBALMASKFRAME: "defaultGlobalMaskFrame",
            //manage fav constants
            ELEMENT_MANAGE_FAV: "0_1_Fav_Mang",
            ELEMENT_MANAGEFAV_ACTION: "E1MFavoritesAction",
            ELEMENT_MANAGEFAV_LAUNCH_MENUITEM_NODEID: "E1MFavoritesLaunchingMenuItemNodeId",
            ELEMENT_MANAGEFAV_APPID: "E1MFavoritesAppId",
            ELEMENT_MANAGEFAV_FORMID: "E1MFavoritesFormId",
            ELEMENT_MANAGEFAV_APPVERSION: "E1MFavoritesAppVersion",
            ELEMENT_MANAGEFAV_FORMMODE: "E1MFavoritesFormMode",
            ELEMENT_MANAGEFAV_FORMTITLE: "E1MFavoritesFormTitle",
            ELEMENT_MANAGEFAV_FORM: "E1FavoritesForm",
            ELEMENT_MANAGEFAV_TASKID: "E1MFavoritesTaskId",
            ELEMENT_MANAGEFAV_TASKVIEW: "E1MFavoritesTaskView",
            ELEMENT_MANAGEFAV_PARENTTASKID: "E1MFavoritesParentTaskId",
            ELEMENT_MANAGEFAV_SEQNUM: "E1MFavoritesSeqNum",
            ELEMENT_MANAGEFAV_NEWNAME: "E1MFavoritesNewName",
            ELEMENT_MANAGEFAV_USERDEFINED_FOLDER_TASKID: "E1MFavoritesUserDefinedFolderTaskId",
            ELEMENT_MANAGEFAV_SEQNUM_MOVED: "E1MFavoritesSeqNumMoved",
            ELEMENT_MANAGEFAV_TABLE: "FavManageTable",
            PROMPT_FOR_BLINDEXECUTION: "promptForBlindExecution",
            UBE: "UBE"
        }
    };
    DDMENU.CONST.LIST_OF_MENU_BUTTON_IDs =
        [DDMENU.CONST.ELEMENT_DROP_MAINMENU,
          DDMENU.CONST.ELEMENT_DROP_FAVMENUS,
          DDMENU.CONST.ELEMENT_DROP_OPENAPPS,
          DDMENU.CONST.ELEMENT_DROP_REPORTMENU,
          DDMENU.CONST.ELEMENT_DROP_WATCHLIST_MENUS,
          DDMENU.CONST.ELEMENT_PREFERENCES,
          DDMENU.CONST.ELEMENT_ROLE_DOWNARROW];

}
var userAgent = navigator.userAgent.toUpperCase();
// iOS check used with User Preferences/iOS specific processing.  Touch OS check is used for iOS/Android touch functionality.
var isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
var isTouchEnabled = (isIOS == true) || (userAgent.indexOf("ANDROID") > -1);
var hasLocalStorageAccess = (isIOS != true) && (userAgent.indexOf("ANDROID") == -1);
var isNativeContainer = (userAgent.indexOf("JDECONTAINER") > -1);
var isRTL = document.documentElement.dir == "rtl";
var isWebCenter = (window.adfpp_handleLocationChange && window.adfpp_formSub);
var userAccessibilityMode = false;
//menuItem height is 28 px
var menuItemHeight = 28;
var breadcrumbHistoryHtml = "";
if (localStorage.getItem("userAccessibilityMode"))
    userAccessibilityMode = (localStorage.getItem("userAccessibilityMode") == "true") ? true : false;

var favIntervalScrollUp = 0;
var favSetInvCalledScrollUp = false;
var favIntervalScrollDown = 0;
var favSetInvCalledScrollDown = false;
var iScrollFactor = 10;

function jdeMenu(currCompId, crystalNotFoundMsg, crystalUrlMsg, procIndMsg, loadingMsg, runAppServiceUrl, oclServiceUrl, recentReportsServiceUrl, fastPathServiceUrl, popupMsg, rightAltKeyDisabled, isAccessibilityEnabled, isRtlEnabled, menuNamespace, prefServiceUrl, useCacheManager, RID) {
    this.currCompId = currCompId;
    this.crystalNotFoundMsg = crystalNotFoundMsg;
    this.crystalUrlMsg = crystalUrlMsg;
    this.procIndMsg = procIndMsg;
    this.loadingMsg = loadingMsg;
    this.runAppServiceUrl = runAppServiceUrl;
    this.oclServiceUrl = oclServiceUrl;
    this.recentReportsServiceUrl = recentReportsServiceUrl;
    this.fastPathServiceUrl = fastPathServiceUrl;
    this.popupMsg = popupMsg;
    this.rightAltKeyDisabled = rightAltKeyDisabled;
    this.isAccessibilityEnabled = isAccessibilityEnabled;
    this.isRtlEnabled = isRtlEnabled;
    this.menuNamespace = menuNamespace;
    this.e1MenuState = new Object();
    this.runAppState = new Object();
    this.elementActiveTreeItem;
    this.flyoutLabels;
    this.lastOpeningCompId = 0;
    this.prefServiceUrl = prefServiceUrl;
    this.closeAfterOCLUpdate = false;
    this.RID = RID;
    this.isMenuMaximized = false;
    this.isMainMenu = true;
    this.oneTimeNewWindowLaunch = false;
    this.isMultipleBrowserEnabled = true;
    this.requestURI = "";
    this.menuWindowName = "";
    this.showRestore = true;
    this.preferencesOptions = 0x00000000;
    this.MENU_NO_PREFERNCES = 0x00000000;
    this.MENU_MINIMIZE_ON_LAUNCH = 0x00000001;
    this.OPEN_APP_IN_NEW_WINDOW = 0x00000002;
    this.MENU_SHOW_MAXIMIZED = 0x00000004;
    this.MENU_PREFERENCES_SHOWN = 0x00000008;
    this.MENU_ACTIONS_SHOWN = 0x00000010;
    this.MENU_ROLES_SHOWN = 0x00000020;
    this.SHOW_RELATED_INFO = 0x00000040;
    this.IPAD_SMALL_FORM_FACTOR = 0x00000080;
    this.ENABLE_RIGHTCLICK_ROWEXIT = 0x00000100;
    this.SHOW_ADD_FAV_POPUP = 0x00000200;
    this.ENABLE_AUTO_SUGGEST = 0x00000400;
    this.ENABLE_POPUP_OPTION = 0x00000800;
    this.USE_LARGE_ICONS_IN_CAROUSEL = 0x00001000;
    this.SHOW_BREADCRUMBS = 0x00002000;
    this.ENABLE_SIMPLIFIED_EXITS = 0x00004000;
    this.USE_THUNDERBIRD_AS_CALENDAR_CLIENT = 0x00008000;
    this.TURN_OFF_THUNDERBIRD_AS_CALENDAR_CLIENT = 0x00010000;

    // these arrays contain a list of children to attach to parents
    this.moveParents = new Array();
    this.moveChildren = new Array();

    this.clearMoveChildren = function () {
        this.moveParents = new Array();
        this.moveChildren = new Array();
    }

    // register a child to attach to a parent
    this.registerChildrenForMove = function (parentId, childId) {
        this.moveParents.push(parentId);
        this.moveChildren.push(childId);
    }

    // this is called from the MenuEventQueue.eventComplete() function to ensure
    // that it is executed after the page has fully loaded
    this.moveAllChildren = function () {
        // We are adding a 'div' element to surround the node divs. This fixes a bug with highlighting.
        // see bug #19305538
        if (this.moveParents.length > 0) {
            var newDiv = document.createElement('div');
            parentElem = this.getElementByIdFromMenu("treechild" + this.moveParents[0]);
            parentElem.appendChild(newDiv);
        }

        for (var i = 0; i < this.moveParents.length; i++) {
            this.moveChild(this.moveParents[i], this.moveChildren[i]);
        }

        this.clearMoveChildren();
    }

    this.moveChild = function (parentId, childId) {
        var fromNode = "node" + childId;
        var fromDiv = this.getElementByIdFromMenu(fromNode);
        var toNode = "treechild" + parentId;
        var parentIcon = "I" + parentId;
        // removeNode not supported in Mozilla...get parent and remove child instead.
        fromDiv.parentNode.removeChild(fromDiv);
        var toDiv = this.getElementByIdFromMenu(toNode);
        toDiv.firstChild.appendChild(fromDiv);
        toDiv.style.display = "block";
    }

    this.getElementByIdFromMenu = function (id) {
        // This method is called by both gui change event and full page render to assicate the child menu items to the parent menu item.
        // For gui change event like user entry menu fast path g42 or expending menu, all menu items are attached to the dom dirrectly.
        // However, for full page render, the e1menu items are rendered in document fragment first before they are attach to the root element, since WPS 6.1 adoption.
        // This mehod is called in between, and we have to cover these two different cases by using different document objects.
        if (menu) // full render with e1menu rendered in a document fragment
        {
            if (isIE) // IE
                return menuFragment.getElementById(id);
            else // FF does not support getElementById for document fragment
                return this.getElementByIdFF(menuFragment, id)

        }
        else // GUI change event with e1menu rendered in dom
        {
            return document.getElementById(id);
        }
    }

    // recursivelly search through the children nodes from root element until find node with matching id, otherwise, return null
    this.getElementByIdFF = function (root, id) {
        if (root.id == id)
            return root;

        var length = root.childNodes.length;
        if (length == 0)
            return null;

        for (var i = 0; i < length; i++) {
            var retObj = this.getElementByIdFF(root.childNodes[i], id);
            if (retObj)
                return retObj;
        }

        return null;
    }

    this.focusOnMenuElement = function () {
        var doc = document;
        if (!doc.getElementById(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV)) {
            return;
        }

        try {
            if (this.elementActiveTreeItem) {
                self.webGuiInteractiveTree.getFirstFocusableChild(document.getElementById(this.elementActiveTreeItem)).focus();
                return;
            }

            var fastPathBox = doc.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
            if (fastPathBox) {
                if (this.doFocusOnFastPath()) {
                    fastPathBox.focus();
                }
                doActivateNodeWebGui("");
                return;
            }
            var foundElement = self.webGuiInteractiveTree.getFirstFocusableElement(doc.getElementById(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV).childNodes);
            foundElement.focus();
            while (foundElement.parentNode) {
                foundElement = foundElement.parentNode;
                if (foundElement.id && foundElement.id.length > 4 && foundElement.id.substring(0, 4) == "head") {
                    doActivateNodeWebGui(foundElement.id.substring(4, foundElement.id.length));
                }
            }
        } catch (p) { }
    }


    this.storeMenuState = function (key, expanded) {
        var isMainMenuHidden = true;
        var isBreadcrumbMenuHidden = true;
        var isFavMenuHidden = true;
        var isOpenTableHidden = true;
        var isRoleTableHidden = true;
        var isPreferenceHidden = true;
        var isManageFavHidden = true;

        var doc = document;
        var fastPathBox = doc.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
        var mainmenuDiv = doc.getElementById(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV);
        var breadcrumbMenuDiv = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV);
        var favDiv = doc.getElementById(DDMENU.CONST.ELEMENT_FAVORITES_DIV);
        var openappTable = doc.getElementById(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
        var roleTable = doc.getElementById(DDMENU.CONST.ELEMENT_ROLETABLE);
        var preferenceDiv = doc.getElementById(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
        var manageFivDiv = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV);
        var manageFavAction = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION);

        if (mainmenuDiv != null && mainmenuDiv.className == "showMenu") {
            isMainMenuHidden = false;
        }
        if (breadcrumbMenuDiv != null && areBreadcrumbDropdownMenusVisible()) {
            isBreadcrumbMenuHidden = false;
        }
        if (favDiv != null && favDiv.className == "showMenu") {
            isFavMenuHidden = false;
        }
        if (openappTable != null && openappTable.className == "showMenu") {
            isOpenTableHidden = false;
        }
        if (roleTable != null && roleTable.className == "showMenu") {
            isRoleTableHidden = false;
        }
        if (preferenceDiv != null && preferenceDiv.className == "showMenu") {
            isPreferenceHidden = false;
        }
        if (manageFivDiv != null && manageFivDiv.style.display != 'none') {
            isManageFavHidden = false;
        }
        if (fastPathBox) {
            if (key && key.length > 0 && fastPathBox.value != "" && isMainMenuHidden && isBreadcrumbMenuHidden && isFavMenuHidden && isOpenTableHidden && isRoleTableHidden && isPreferenceHidden && isManageFavHidden) {
                var keyValue = key.substr(0, 2);
                if (keyValue != '98' && prevKeyValue != '98') {
                    showMenu(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV, "event");
                }
                prevKeyValue = keyValue;
            }
        }
        if (null != manageFivDiv && manageFivDiv.style.display != 'none') {
            if (manageFavAction.value == 'frAll') {
                manageFavorites();
            }
            else if (isManageRendered == false) {
                isManageRendered = true;
                return;
            }
            else {
                isManageRendered = false;
                manageFavorites();
                if (manageFavAction.value == 'frNewFolder') {
                    var j = jdeMenuParent.favoriteLabels.length - 1;
                    var newFolderLabelId = "favLabel_" + j;
                    var newFolderDivLabelID = "favLabelDiv_" + j;
                    if (jdeMenuParent.favoriteType[j] == '21') {
                        newFolderRename(newFolderLabelId, jdeMenuParent.favoriteID[j], jdeMenuParent.favoriteParentTaskID[j], jdeMenuParent.favoriteView[j], jdeMenuParent.favoriteSequenceNumber[j], jdeMenuParent.favoriteLabels[j], newFolderDivLabelID);
                    }
                }
            }
        }

        //		if (expanded)
        //		{
        //			this.e1MenuState[key]=expanded;
        //		} else {
        //			delete this.e1MenuState[key];
        //		}
        //		var sb = "";
        //		for (var key in this.e1MenuState)
        //		{
        //			sb+=key + "|";
        //		}
        //		this.setCookie("e1MenuState", sb, "", "/", "", "");
        return;
    }

    this.storeRunAppState = function (compId, targetWindow) {
        var sb = "";
        var addComponent = true;
        var compArray = this.getRunAppState();
        var appId;
        for (var i = compArray.length - 1; i >= 0; i--) {
            appId = compArray[i];
            if (compId == this.runAppState[appId]) {
                addComponent = false;
            }
            if (this.runAppState[appId] == targetWindow && compId != this.runAppState[appId]) {
                delete this.runAppState[appId];
            } else if (appId != compId) {
                sb += appId + ":" + this.runAppState[appId] + "|";
            }
        }

        if (addComponent == true) {
            this.currCompId = compId;
            if (window.webkit) {
                window.webkit.messageHandlers.jde.postMessage({ "type": "activeCompId", "action": "updateJSParam", "value": compId });
                window.webkit.messageHandlers.jde.postMessage({ "type": "jdemafjasLinkTarget", "action": "updateJSParam", "value": targetWindow });
            }
            if (!isWSRPContainer() && CARO != null) {
                CARO.setActiveAppTile(compId);
            }
            this.runAppState[compId] = targetWindow;
            sb += compId + ":" + targetWindow + "|";
            this.setCookie("e1AppState", sb, "", "/", "", "");
        }
        return;
    }

    this.removeRunAppState = function (compId, targetWindow) {
        var sb = "";
        var compArray = this.getRunAppState();
        var appId;
        for (var i = compArray.length - 1; i >= 0; i--) {
            appId = compArray[i];
            if (this.runAppState[appId] == targetWindow) {
                delete this.runAppState[appId];
            } else {
                sb += appId + ":" + this.runAppState[appId] + "|";
            }
        }
        this.setCookie("e1AppState", sb, "", "/", "", "");
        return;
    }

    this.getRunAppWin = function (compId) {
        var compArray = this.getRunAppState();
        var appId;
        for (var i = compArray.length - 1; i >= 0; i--) {
            appId = compArray[i];
            if (compId == appId) {
                return this.runAppState[appId];
            }
        }
        return null;
    }

    this.getRunAppState = function () {
        // There are issues using associative arrays in loops
        // Populate an array to return
        this.runAppState = {};
        var returnArray = new Array();
        var l = 0;
        var runAppCookie = this.getCookie("e1AppState");

        if (runAppCookie != null) {
            var i = 0;
            var j = 0;
            var clen = runAppCookie.length;
            while (i < clen) {
                i = runAppCookie.indexOf("|", i) + 1;
                if (i == 0) break;
                var singleEntry = runAppCookie.substring(j, i - 1);
                j = i;
                var k = singleEntry.indexOf(":");
                var compId = singleEntry.substring(0, k)
                this.runAppState[compId] = singleEntry.substring(k + 1);
                returnArray[l++] = compId;
            }
        }

        return returnArray;
    }

    this.updateOCLWindows = function () {
        MENUUTIL.updateOCLIndex(-1);

        /* As of 9.1, only the master window has an OCL, so there is no need to iterate through and 
        update the OCL in each  window only. */

        if (this.closeAfterOCLUpdate) {
            this.closeAfterOCLUpdate = false;
            // call browser close with the 'controlled' flag set to prevent
            // the actual browserclose logic running (popping new window) since we are
            // closing manually

            if (navigator.userAgent.toLowerCase().indexOf("safari") != -1) {
                window.handleOnBrowserClose(true, true);
            }
            else {
                var windowCount = this.getRunAppState().length;
                window.controlledBrowserClose = (windowCount > 0) ? true : false;
                window.handleOnBrowserClose(true, window.controlledBrowserClose);
            }
        }

    }

    this.setCookie = function (name, value, expires, path, domain, secure) {
        document.cookie = name + "=" + value +
            ((expires) ? "; expires=" + expires.toGMTString() : "") +
            ((path) ? "; path=" + path : "") +
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
        if (window.webkit) {
            window.webkit.messageHandlers.jde.postMessage({ "type": "cookie", "action": "setCookie", "value": document.cookie });
        }
    }

    this.getCookie = function (name) {
        var arg = name + "=";
        var alen = arg.length;
        var clen = document.cookie.length;
        var i = 0;
        while (i < clen) {
            var j = i + alen;
            if (document.cookie.substring(i, j) == arg)
                return this.getCookieVal(j);
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) break;
        }
        return null;
    }

    this.getCookieVal = function (offset) {
        var endstr = document.cookie.indexOf(";", offset);
        if (endstr == -1)
            endstr = document.cookie.length;
        return unescape(document.cookie.substring(offset, endstr));
    }

    this.doFocusOnFastPath = function () {
        // Do not set focus on the Fast Path field for certain touch devices where doing
        // so will trigger the appearance of a virtual keyboard on the device.  Note,
        // with iOS, Apple appears to be rejecting the focus statement for us.
        return (userAgent.indexOf("ANDROID") <= -1);
    }
}

function checkHrefKeyPress(e, evt) {
    var menuObject = jdeMenuParent;
    menuObject.oneTimeNewWindowLaunch = false;
    if (!MENUUTIL.allowNewWindow()) {
        return;
    }
    evt = evt ? evt : window.event;
    if (isCtrlDown(evt) || isShiftDown(evt)) {
        menuObject.oneTimeNewWindowLaunch = true;
        var href = (e.parentNode.href) ? e.parentNode.href : e.href;
        if (evt.stopPropagation) {
            evt.stopPropagation();
            evt.preventDefault();
            eval(href);
            // hide the drop menus
            hideAllMainMenus();
            hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
            hideMenu(DDMENU.CONST.ELEMENT_FAVORITES_DIV);
            hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
            hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
            return false;
        }
        else if (window.event) {
            evt.cancelBubble = true;
            evt.returnValue = false;
            eval(href);
            //hide the drop menus
            hideAllMainMenus();
            hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
            hideMenu(DDMENU.CONST.ELEMENT_FAVORITES_DIV);
            hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
            hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
            return false;
        }
        else {
            return true;
        }
    }
}

function onloadAppIframe() {
}

function maxMenuOnLoad() {
    teExpandMenu(true);
    jdeMenuParent.focusOnMenuElement();
}

function returnToMainMenu() {
    var openerRef = window.opener;
    var newWin = (openerRef == null || openerRef.closed) ? true : false;

    if (newWin) {
        openerRef = window.open(jdeMenuParent.requestURI, jdeMenuParent.mainMenuName);
        self.opener = openerRef;
    }
    else {
        openerRef = window.open("", jdeMenuParent.mainMenuName, "height=1,width=1,top=0,left=5000,screenX=0,screenY=5000");
    }
    openerRef.focus();
}

function setupMenuDisplayOptions(isMainMenu, openURL, windowName, rolesSubMenuState, isMultipleBrowserEnabled, mainMenuName) {
    var menuObject = jdeMenuParent;
    menuObject.isMainMenu = isMainMenu;
    menuObject.requestURI = openURL;
    menuObject.menuWindowName = windowName;
    menuObject.isMultipleBrowserEnabled = isMultipleBrowserEnabled;
    menuObject.mainMenuName = mainMenuName;

    menuObject.focusOnMenuElement();

    if (isWSRPContainer()) {
        window.teOnLoadOrResize();
        syncRecentReports(false);
    }

    self.name = menuObject.menuWindowName;
}


function getSubMenuIcon(imgId, state) {
    var doc = document;
    var image_sb = "E1RES_share_images_";
    image_sb += state;
    if (jdeMenuParent.isRtlEnabled && state == "treecollapsed") {
        image_sb += "_rtl";
    }
    image_sb += "_gif";

    if (doc.getElementById(imgId)) {
        doc.getElementById(imgId).src = window[image_sb];
    }
    else {
        alert(imgId + " is not in the dom");
    }

    if (window[image_sb] == undefined) {

        if (isWebCenter && isIE && top.document.getElementById("T:mainC::edit")) {
            // This is in WebCenter edit (aka configuration) mode, do not display the error due to limitatio of IE 7. IE 7 has a limitation on size of url.  The url to ResourceCanonical.js is too long
            // In the dynamically generated js file, all images are assigned to window properties.
        }
        else {
            alert("getSubMenuIcon found undefined " + image_sb)
        }
    }
}

function updateOCLAndCloseWindow() {
    // update the master window's OCL and then close this window
    if (jdeMenuParent) {
        jdeMenuParent.closeAfterOCLUpdate = true;
        jdeMenuParent.removeRunAppState(null, self.name);
    }
    updateOCL();
}

function setMenuPreferences() {
    var doc = document;
    if (!doc.getElementById("webTree")) {
        return;
    }
    var menuObject = jdeMenuParent;

    if (doc.getElementById(DDMENU.CONST.ELEMENT_ROLETABLE).style.display != "none") {
        newOptions += menuObject.MENU_ROLES_SHOWN;
    }
}
// script to purge cache and show default mainmenu
function PurgeUiCache() {
    var doc = document;
    var roleSelector = doc.getElementById("e1MRoleSelect");
    if (roleSelector.options != undefined) {
        doc.getElementById("E1PurgeCacheFormRoleId").value = roleSelector.options[roleSelector.selectedIndex].value;
    }
    else {
        doc.getElementById("E1PurgeCacheFormRoleId").value = roleSelector.value;
    }
    e1MRoleboxVisible(true)
    doc.getElementById('E1PurgeCacheForm').submit();
    updateOCL();
}

function RunUrl(url) {
    if (url.indexOf(":") == -1) {
        window.open("http://" + url);
    }
    else {
        window.open(url);
    }
}

function RunCrystal(host, port, mode, langCode, data, crystalURL) {

    if (mode == null || mode == "1" || mode == "" || mode == "null")
        mode = 3;

    if (host == null || host == "ERROR" || host == "" || host == "null") {
        alert(jdeMenuParent.crystalNotFoundMsg);
        return;
    }

    var today = new Date();

    langCode = 'en';

    var url = "http://" + host;
    if (port == "" || port == "0" || port == "80")
        ;
    else
        url = url + ":" + port;

    var frm = null;
    if (isIE)
        frm = document.frames("RunAppHiddenIframe");
    else
        frm = window.frames["RunAppHiddenIframe"];

    var doc = frm.document;
    doc.open("text/html");
    doc.write("<html><head>\n<title>Inter Form</title>\n<script language='JavaScript'>\n");
    doc.write("<" + "/script></head>\n<body>\n");

    if (mode == 3) {
        var hostField = "cms";
        url = url + crystalURL;

        if (url.indexOf("enterprise10") != -1) {
            // We need to pass in the language with
            // "/crystal/enterprise10/desktoplaunch/ePortfolio/logon/logon.do"

            url = url + "?webapplanguage=" + langCode;
            hostField = "aps";
        }

        doc.write("<form id='cr_inter_form' method='post' action='" + url + "' target=_ePortfolio_" + today.getTime() + ">\n");
        doc.write("<input type=hidden name=" + hostField + "  size=\"0\" value='" + host + "'>\n");
        doc.write("<input type=hidden name=username        size=\"0\" value=''>\n");
        doc.write("<input type=hidden name=authType        size=\"0\" value='secPSE1'>\n");
        doc.write("<input type=hidden name=isFromLogonPage size=\"0\" value=\"true\">\n");
        doc.write("</form>\n");

    }
    else if (mode == 2) {
        url = url + "/crystal/enterprise10/admin/" + langCode + "/admin.cwr";

        doc.write("<form id='cr_inter_form' method='post' action='" + url + "' target=_CMC_" + today.getTime() + ">\n");
        doc.write("<input type=hidden name=aps    size=\"0\" value='" + host + "'>\n");
        doc.write("<input type=hidden name=usr    size=\"0\" value=''>\n");
        doc.write("<input type=hidden name=auth   size=\"0\" value='secPSE1'>\n");
        doc.write("<input type=hidden name=action size=\"0\" value=\"logon\">\n");
        doc.write("</form>\n");

    }
    else {
        alert(jdeMenuParent.crystalUrlMsg);
        doc.write("</body>\n</html>\n");
        doc.close();
        return;
    }
    doc.write("<script language='JavaScript'>\n");
    doc.write("	document.getElementById('cr_inter_form').submit();\n");
    doc.write("<" + "/script>\n");

    doc.write("</body>\n</html>\n");
    doc.close();
}

function activateE1Page(id, key) {
    var fName = "e1menuAppIframe";
    var appFrame = document.getElementById(fName);

    if (!appFrame) {
        appFrame = window.frames[fName];
    }

    if ((appFrame) && (appFrame.contentWindow) && (appFrame.contentWindow.CHP)) {
        appFrame.contentWindow.CHP.activateE1Page(key);
    }
    else {
        saveActivePage(key);
        setTimeout('goHome()', 500);
    }
}

function saveActivePage(key) {
    if (STATE) {
        STATE.sendStateUpdate(STATE.setXMLDocument('e1pages', 'active', key), STATE.createStateUrl());
    }
}

function panE1PageLeft() {
    var fName = "e1menuAppIframe";
    var appFrame = document.getElementById(fName);

    if (!appFrame) {
        appFrame = window.frames[fName];
    }

    if ((appFrame) && (appFrame.contentWindow) && (appFrame.contentWindow.CHP)) {
        appFrame.contentWindow.CHP.traversePagesByOffestOfCurrentIndex(1);
    }
}

function panE1PageRight() {
    var fName = "e1menuAppIframe";
    var appFrame = document.getElementById(fName);

    if (!appFrame) {
        appFrame = window.frames[fName];
    }

    if ((appFrame) && (appFrame.contentWindow) && (appFrame.contentWindow.CHP)) {
        appFrame.contentWindow.CHP.traversePagesByOffestOfCurrentIndex(-1);
    }
}

function RunNewApp(launchAction, promptPO, appID, appType, formID, version, mode, menuSysCode, taskName, formDSTmpl, formDSData, launchInNewWindow, accessibilityMode, nodeId, taskNameLabel, RID, menuNodeInstanceId, queryId, webObjectName, watchlistId, lpContext) {
    hideAllDropdownMenus();
    if (isIOS) {
        updateAppFrameHeight();
        var fastPathField = document.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
        if (fastPathField) {
            fastPathField.blur();
        }
    }

    /*
    * Disable the camera when entering new apps to prevent unintended access to it.
    * Ideally, apps enabling the camera disable it but launching RunNewApp offers a
    * bypass to proper cleanup.
    */
    if (isNativeContainer) {
        if (NativeContainer) {
            NativeContainer.disableCamera();
        }
    }

    var menuObject = jdeMenuParent;
    // Logic to launch a new window and then launch application if preference is set or menu is maximized.
    // Flag launchInNewWindow will be set when parms are saved in launchNewWindow and will keep this logic from
    // looping when we come in the second time
    // only check the preference if we are not WebCenter or Portal (i.e. WSRP)
    if (!isWSRPContainer()) {
        if (isPreferenceChecked("openAppInNewWindowPreference")) {
            menuObject.oneTimeNewWindowLaunch = true;
        }
    }
    if (menuObject.oneTimeNewWindowLaunch &&
            launchInNewWindow != "1" &&
            MENUUTIL.allowNewWindow()) {
        menuObject.oneTimeNewWindowLaunch = false;
        launchNewWindow(launchAction, promptPO, appID, appType, formID, version, mode, menuSysCode, unescape(taskName), formDSTmpl, formDSData, nodeId, menuNodeInstanceId, queryId, webObjectName, watchlistId);
        return;
    }

    // When there's no launch in a new window, we need to make sure the e1 app iframe is visible.
    ExternalAppsHandler.showE1MenuAppIframe();

    if (CheckAppStatus != null) {
        window.clearTimeout(CheckAppStatus);
        CheckAppStatus = null;
    }
    var doc = document;
    var topoffset = (isWSRPContainer() || isWebCenter) ? getAbsoluteTopPos(doc.getElementById(DDMENU.CONST.ELEMENT_MENU_FASTPATH_DIV)) + 45 : getAbsoluteTopPos(doc.getElementById(DDMENU.CONST.ELEMENT_MENUTITLE_DIV)) + 51;
    //for the case that user starts another app without wait the first app is loaded
    if (ProcessingIndicator.getInstance() != null) {
        ProcessingIndicator.stopProcessingIndicator();
    }
    // no need to show the Open app msg when the launchAction is promptForBlindExecution.
    if ((launchAction != DDMENU.CONST.PROMPT_FOR_BLINDEXECUTION) && (launchAction != "lcmLoadMo")) {
        var rightOffset = 48;
        var procIndicatorWidth = 210;
        if (isWSRPContainer() || isWebCenter) {
            /*
            Nullify the "document.body.clientWidth + document.body.scrollLeft" calculation in createPIElem()/position() function
            Since it is called from top frame the document will be the main document, not the e1 container document  */
            var tempDocOffset = (document.body.clientWidth + document.body.scrollLeft) * -1;
            var outerRCUXDiv = doc.getElementById(DDMENU.CONST.ELEMENT_OUTERRCUX_DIV);
            var outerRCUXClientWidth = outerRCUXDiv.clientWidth;
            var outerRCUXParentOffsetleft = outerRCUXDiv.parentElement.offsetLeft;
            //if the frame width is less than processing indicator width the procesing indicator should display inside the frame width
            if (outerRCUXClientWidth < (procIndicatorWidth + 48)) {
                procIndicatorWidth = outerRCUXClientWidth;
                rightOffset = (tempDocOffset + procIndicatorWidth + outerRCUXParentOffsetleft) * -1;
            }
            else {
                rightOffset = (tempDocOffset + (outerRCUXParentOffsetleft + outerRCUXClientWidth + 48)) * -1;
            }

        }
        ProcessingIndicator.createInstance(menuObject.procIndMsg, procIndicatorWidth, 18, topoffset, rightOffset).display(true); //width, height, topoffset, rightoffset
        OpenAppIndicator.createInstance().display(false);

        OpenAppShow = window.setTimeout("OpenAppIndicator.showOpenAppIndicator()", 2000);
    }

    var sb = "<html><body><form id=\"theForm\" method=post action=\"";
    sb += menuObject.runAppServiceUrl;
    sb += "\">\n";
    sb += "<input type=hidden name=timestamp value=\"";
    sb += (new Date()).getTime();
    sb += "\">\n";

    //taskName = determineTaskName(nodeId, taskName, taskNameLabel);
    if (launchAction != "") { sb += "<input type=hidden name=launchAction value=\""; sb += launchAction; sb += "\">\n"; }
    if (promptPO != '') { sb += "<input type=hidden name=promptPO value=\""; sb += promptPO; sb += "\">\n"; }
    if (appID != '') { sb += "<input type=hidden name=appID value=\""; sb += appID; sb += "\">\n"; }
    if (appType != '') { sb += "<input type=hidden name=appType value=\""; sb += appType; sb += "\">\n"; }
    if (formID != '') { sb += "<input type=hidden name=formID value=\""; sb += formID; sb += "\">\n"; }
    if (version != '') { sb += "<input type=hidden name=version value=\""; sb += version; sb += "\">\n"; }
    if (mode != '') { sb += "<input type=hidden name=mode value=\""; sb += mode; sb += "\">\n"; }
    if (menuSysCode != '') { sb += "<input type=hidden name=menuSysCode value=\""; sb += menuSysCode; sb += "\">\n"; }
    if (taskName != '') { sb += "<input type=hidden id=taskName name=taskName value=\"\">\n"; }
    if (formDSTmpl != '') { sb += "<input type=hidden name=FormDSTmpl value=\""; sb += formDSTmpl; sb += "\">\n"; }
    if (formDSData != '') { sb += "<input type=hidden name=FormDSData value=\""; sb += formDSData; sb += "\">\n"; }
    if (queryId != undefined && queryId != '') { sb += "<input type=hidden name=FormQueryId value=\""; sb += queryId; sb += "\">\n"; }
    //passing the webObjectName
    if (webObjectName != undefined && webObjectName != '') { sb += "<input type=hidden name=WebObjectName value=\""; sb += webObjectName; sb += "\">\n"; }
    if (watchlistId != undefined && watchlistId != '') { sb += "<input type=hidden name=FormWatchlistId value=\""; sb += watchlistId; sb += "\">\n"; }
    sb += "<input type=hidden name=jdemafjasLinkTarget value=\""; sb += self.name; sb += "\">\n";
    sb += "<input type=hidden name=namespace value=\""; sb += menuObject.menuNamespace; sb += "\">\n";
    sb += "<input type=hidden name=RID value=\""; sb += menuObject.RID; sb += "\">\n";
    // The ID of the menu node instance that initiated the application launch.
    sb += "<input type=hidden name=" + BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID + " value=\"";
    if (menuNodeInstanceId == undefined)
        sb += "";
    else
        sb += menuNodeInstanceId;
    sb += "\">\n";
    //lpcontext for springboard from landing page
    if (lpContext !== undefined && lpContext != "") { sb += "<input type=hidden name=lpContext value=\""; sb += lpContext; sb += "\">\n"; }
    sb += "</form>";

    if (accessibilityMode) {
        sb += jdeMenuParent.procIndMsg;
    }
    sb += "</body></html>";

    var frm = null;
    var fName = "e1menuAppIframe";
    var appFrame = document.getElementById(fName);
    if (appFrame) {
        frm = appFrame.contentWindow;
    }
    if (!frm) {
        frm = window.frames[fName];
    }

    var newDoc = frm.document;
    newDoc.open("text/html");
    newDoc.charset = "UTF-8";
    newDoc.write(sb);
    newDoc.close();

    var taskName = UTIL.htmlDecoder(taskName);

    if (taskName != '') {
        frm.document.getElementById("taskName").value = jdeWebGUIReplaceSubstring(taskName, "\"", "''"); //"
    }

    menuObject.currCompId = 0;

    // Once we launch the application, clear out the breadcrumb bar content
    // to minimize refresh lag in the bar while the application launches.
    BREADCRUMBS.updateBreadcrumbContent("");

    frm.document.getElementById("theForm").submit();
    //Report is submitted via task update Carousel rec reports
    if (appType == DDMENU.CONST.UBE) {
        setTimeout('syncRecentReports(true)', 3000);
    }
    /*
    * Need to call this after the service submission to work on iOS7.
    *
    * Disable the camera when entering new apps to prevent unintended access to it.
    * Ideally, apps enabling the camera disable it but launching RunNewApp offers a
    * bypass to proper cleanup.
    */
    if (isNativeContainer) {
        if (NativeContainer) {
            NativeContainer.disableCamera();
        }
    }
}

function determineTaskName(nodeId, taskName, taskNameLabel) {
    if (nodeId && nodeId != "0") {
        var nodeIdElem = document.getElementById(nodeId);
        if (null != nodeIdElem) {
            taskName = nodeIdElem.getAttribute("taskLabel");
        }
        else {
            taskName = isIE ? unescape(taskNameLabel) : taskNameLabel;
        }
    }
    else {
        taskName = unescape(taskName);
    }

    return taskName;
}

function pollServerForOpeningApp() {

    RunOldApp(jdeMenuParent.lastOpeningCompId, true);
}

function RunOldApp(compId, force, launchInNewWindow) {
    if (isIOS) {
        updateAppFrameHeight();
        var fastPathField = document.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
        if (fastPathField) {
            fastPathField.blur();
        }
    }
    if (!force && !ExternalAppsHandler.isAnExternalAppInstance(compId)) {
        if (compId == self.jdeMenuParent.currCompId) {
            return;
        }

        var compWin = jdeMenuParent.getRunAppWin(compId);
        if (compWin != null && compWin != window.name) {
            RunApp("", compWin, compId, "");
            return;
        }
    }

    if (CheckAppStatus != null) {
        window.clearTimeout(CheckAppStatus);
        CheckAppStatus = null;
    }

    if (launchInNewWindow) {
        RunAppChangeTarget(compId, self.name, null, launchInNewWindow);
    }
    else {
        // Once we launch the application, clear out the breadcrumb bar content
        // to minimize refresh lag in the bar while the application launches.
        BREADCRUMBS.updateBreadcrumbContent("");

        RunAppChangeTarget(compId, self.name);
    }
}

function RunAppChangeTarget(compId, target, winToClose, launchInNewWindow) {
    var menuObject = jdeMenuParent;
    // only check the preference if we are not WebCenter or Portal (i.e. WSRP)
    if (!isWSRPContainer()) {
        if (isPreferenceChecked("openAppInNewWindowPreference")) {
            menuObject.oneTimeNewWindowLaunch = true;
        }
    }
    if ((menuObject.isMenuMaximized ||
            menuObject.oneTimeNewWindowLaunch) &&
            launchInNewWindow != "1" &&
            MENUUTIL.allowOpenInNewWindow(compId)) {
        menuObject.oneTimeNewWindowLaunch = false;
        // change target to a new window since the preference is checked
        launchNewWindowExistingComp(compId);
        if (winToClose) {
            winToClose.location = "javascript:window.close()";
        }
        return;
    }
    // change target window to ourselves
    menuObject.storeRunAppState(compId, target);

    // With the launch in the same window, make sure the e1 menu app iframe is visible over the external app iframe.
    ExternalAppsHandler.showE1MenuAppIframe();

    var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI("RunApp");
    e1UrlCreator.setParameter("timestamp", (new Date()).getTime());
    e1UrlCreator.setParameter("compId", compId);
    e1UrlCreator.setParameter("currCompId", menuObject.currCompId);
    e1UrlCreator.setParameter("jdemafjasLinkTarget", self.name);
    e1UrlCreator.setParameter("namespace", menuObject.menuNamespace);
    e1UrlCreator.setParameter("RID", menuObject.RID);

    var appIframe = document.getElementById("e1menuAppIframe");
    // IE10 won't re-render the content of the Iframe when changing the src value, 
    // so forcing the re-render of the e1menuAppIframe.
    if (isIE && !isWSRPContainer() && appIframe) {
        UTIL.forceRedraw(appIframe);
    }

    appIframe.src = e1UrlCreator.toString();

    if (winToClose) {
        winToClose.location = "javascript:window.close()";
    }
}


function launchNewWindow(launchAction, promptPO, appID, appType, formID, version, mode, menuSysCode, taskName, formDSTmpl, formDSData, nodeId, menuNodeInstanceId, queryId, webObjectName, watchlistId) {
    var encodedTaskName = encodeURIComponent(taskName);
    var launchUrl = window.newLocationURL;
    // The name of the window that opened this new one
    launchUrl += "&e1menu.parent=" + self.name;
    // The timestamp to attach to the new window name
    launchUrl += "&e1.newWinTS=" + (new Date()).getTime();
    if (launchAction) {
        launchUrl += "&launchAction=" + launchAction;
        launchUrl += "&promptPO=" + promptPO;
        launchUrl += "&appID=" + appID;
        launchUrl += "&appType=" + appType;
        launchUrl += "&formID=" + formID;
        launchUrl += "&version=" + version;
        launchUrl += "&mode=" + mode;
        launchUrl += "&menuSysCode=" + menuSysCode;
        launchUrl += "&taskName=" + encodedTaskName;
        launchUrl += "&formDSTmpl=" + formDSTmpl;
        launchUrl += "&formDSData=" + formDSData;
        launchUrl += "&oneTimeWindow=true";
        launchUrl += "&nodeId=" + nodeId;
        launchUrl += "&taskNameLabel=" + encodedTaskName;
        if (typeof watchlistId != "undefined") {
            launchUrl += "&FormQueryId=" + queryId;
            launchUrl += "&FormWatchlistId=" + watchlistId;
        }
        // The ID of the menu node instance that initiated the application launch.
        launchUrl += "&" + BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID + "=";
        if (menuNodeInstanceId == undefined)
            launchUrl += "";
        else
            launchUrl += menuNodeInstanceId;
    }
    else {
        launchUrl += "&showSimplifiedNavigation=true";  // No app is being launched, just a new window w/o carousel      
    }

    window.open(launchUrl, '', 'directories=no,scrollbars=yes,location=no,toolbar=no,menubar=no,resizable=yes');
}

function launchNewWindowExistingComp(compId) {
    var launchUrl = window.newLocationURL;
    // The name of the window that opened this new one
    launchUrl += "&e1menu.parent=" + self.name;
    // The timestamp to attach to the new window name
    launchUrl += "&e1.newWinTS=" + (new Date()).getTime();
    launchUrl += "&compId=" + compId;
    launchUrl += "&oneTimeWindow=true";

    window.open(launchUrl, '', 'directories=no,scrollbars=yes,location=no,toolbar=no,menubar=no,resizable=yes');
}

var runAppEventNotifiers = new Array();

function RunAppRegisterEventNotification(script) {
    runAppEventNotifiers[runAppEventNotifiers.length] = script;
}

function RunAppEvent(event) {
    for (i = 0; i < runAppEventNotifiers.length; i++) {
        eval(runAppEventNotifiers[i] + "('" + event + "')");
    }
}

function RunApp(runappurl, runapptarget, runappcompid, runappnamespace) {
    var thisTarget = self.name;
    if (runapptarget != thisTarget) {
        var targetWin = window.open("", runapptarget, "height=1,width=1,top=0,left=5000,screenX=0,screenY=5000");
        if (!targetWin) {
            alert(jdeMenuParent.popupMsg);
        }
        var dropdownmenuNav = targetWin.document.getElementById("topnav");
        if (dropdownmenuNav) {
            try {
                if (targetWin.jdeMenuParent.currCompId == runappcompid) {
                    // blur the window for IE7.
                    // IE 7 will find the menuNav element even if it is on another tab.
                    if (window.blur)
                        window.blur();
                    if (window.focus)
                        targetWin.focus();
                    return false;
                }
                else {
                    RunOldApp(runappcompid);
                }
            }
            catch (problem) { }
        }
        else {
            RunAppChangeTarget(runappcompid, thisTarget, targetWin);
            return;
        }
    }
    document.getElementById("e1menuAppIframe").src = runappurl;
}

function handleFPEventOCL(event) {
    if (event == '3') {
        updateOCL();
    }
}
function updateOCL() {
    // Use the current component ID to determine what breadcrumbs to display.
    var compId = jdeMenuParent.currCompId;
    // Check if this script is running in the main window.  Breadcrumbs only display in
    // the main window, so if special handling is required if running in a separate window.
    if (window.name != jdeMenuParent.mainMenuName) {
        // If we are running from a child window, set the current component to an invalid value.
        // This will effectively suppress breadcrumb updates within the child window.
        // 
        // The main window will always be responsible for updating its OCL state, which
        // currently happens.
        compId = -1;
    }
    var sb = "<html><body><form id=\"theForm\" method=post action=\"";
    sb += jdeMenuParent.oclServiceUrl;
    sb += "\">\n";
    sb += "<input type=hidden name=timestamp value=\"";
    sb += (new Date()).getTime();
    sb += "\">\n";
    sb += "<input type=hidden name=" + BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_ACTIVE_COMPONENT_ID + " value=\""; sb += compId; sb += "\">\n";
    sb += "</form>";
    sb += "</body></html>";

    var frm = null;
    frm = document.getElementById("SilentOCLIFrame").contentWindow;
    if (!frm) {
        frm = window.frames["SilentOCLIFrame"];
    }

    var newDoc = frm.document;
    newDoc.open("text/html");
    newDoc.charset = "UTF-8";
    newDoc.write(sb);
    newDoc.close();

    frm.document.getElementById("theForm").submit();
}

function syncRecentReports(forceGoToDb) {
    document.getElementById('RecentReportsIFrame').src = jdeMenuParent.recentReportsServiceUrl + '&forceGoToDb=' + ((forceGoToDb) ? 'true' : 'false') + '&amp;timestamp=' + (new Date()).getTime();
}

var teFastPathEventNotifiers = new Array();

function teFastPathRegisterEventNotification(script) {
    teFastPathEventNotifiers[teFastPathEventNotifiers.length] = script;
}

function teFastPathEvent(event) {
    for (i = 0; i < teFastPathEventNotifiers.length; i++) {
        eval(teFastPathEventNotifiers[i] + "('" + event + "')");

    }
}

function setFocusOnFastPath(openNavigatorFirst) {
    var doc = document;
    var fastPathField = doc.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
    if (fastPathField) {
        if (openNavigatorFirst) {
            //hide breadcrumbs history before Navigator opens
            hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
            showMenu('e1MMenuRoot', (new Object()));
        }
        fastPathField.focus();
    }
    window.topActiveNavigationMenu = doc.getElementById("drop_mainmenu");
}

function e1MChangeRootMenu() {
    document.getElementById(DDMENU.CONST.ELEMENT_ROLEFORM).submit();
}

function e1MRoleboxVisibleRefresh(state) {
    e1MRoleboxVisible(state);
    e1MChangeRootMenu();
}

function e1MRoleboxVisible(state) {
    var doc = document;
    if (state) {
        doc.getElementById(DDMENU.CONST.ELEMENT_ROLEBACKLINK).style.display = "none";
    }
    else {
        doc.getElementById(DDMENU.CONST.ELEMENT_ROLEBACKLINK).style.display = "inline";
    }
    hideAllMainMenus();
}

function e1MMonitorFastpath(event) {
    if (event == "1") {
        e1MRoleboxVisible(false);
    }
}

function teDoRunCrystal(nodeId) {
    var doc = document;
    doc.getElementById("E1MCrystalFormNodeId").value = nodeId;
    doc.getElementById("E1CrystalForm").submit();
}

function teDoRunOvrTask(nodeId) {
    var doc = document;
    doc.getElementById("E1MOVRFormNodeId").value = nodeId;
    doc.getElementById("E1OVRForm").submit();
}

function teDoRunExternalAppTask(nodeId, menuNodeInstanceId) {
    var doc = document;
    doc.getElementById("E1MExternalAppFormNodeId").value = nodeId;
    doc.getElementById("E1MExternalAppNodeInstanceId").value = menuNodeInstanceId;
    doc.getElementById("E1ExternalAppForm").submit();
}

function teDoDocWindow(tmTaskName, taskview, taskId, parentTaskId, sequence, role, name) {
    e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI("TEDocWindow");
    if (tmTaskName != '') { e1UrlCreator.setParameter("tmTaskName", teEscape(tmTaskName)); }
    if (taskview != '') { e1UrlCreator.setParameter("taskview", teEscape(taskview)); }
    if (taskId != '') { e1UrlCreator.setParameter("taskId", teEscape(taskId)); }
    if (parentTaskId != '') { e1UrlCreator.setParameter("parentTaskId", teEscape(parentTaskId)); }
    if (sequence != '') { e1UrlCreator.setParameter("sequence", teEscape(sequence)); }
    if (role != '') { e1UrlCreator.setParameter("role", teEscape(role)); }
    if (name != '') { e1UrlCreator.setParameter("name", teEscape(name)); }

    E1Url = e1UrlCreator.toString();
    var theWindow;
    theWindow = window.open(E1Url, "DocWindow", "toolbar,location,status,menubar,scrollbars,resizable,,width=740,height=540");
    theWindow.focus();
}
function doLoadInitialNodeWebGUI() {
    document.getElementById("E1RefreshRootForm").submit();
}
// Handler for 'add app/form to favorites' DTA event.
function addActiveAppFormToFavorites(nodeId, appId, formId, version, formMode, formTitle) {
    // Call the dropdown menu service indicating that the user requested
    // the active app/form be added to favorites.
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = 'frmAdd';
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_LAUNCH_MENUITEM_NODEID).value = nodeId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_APPID).value = appId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORMID).value = formId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_APPVERSION).value = version;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORMMODE).value = formMode;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORMTITLE).value = formTitle;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

// Retrieve the complete favorites information for all immediate children of the 
// task represented by the carousel tile object and the tile itself if necessary.
function loadChildFavoritesForTile(tile, silent) {
    // The tile object passed to loadChildFavoritesForTile method may not 
    // be a DOM element.  Use CARO.loadChildFavoritesForTileById if the tile 
    // should be retrieved from the DOM.
    var tileUniqueId;
    var tileTaskId;
    var tileParentId;
    var tileTaskView;
    var doc = document;
    // If we don't have a tile, send dummy data to have the server determine the 
    // proper course of action.
    if ((!tile) || (!tile.favInfo)) {
        tileUniqueId = "";
        tileTaskId = "";
        tileParentId = "";
        tileTaskView = "";
    }
    else {
        tileUniqueId = tile.favInfo.uniqueId;
        tileTaskId = tile.favInfo.trueTaskId;
        tileParentId = tile.favInfo.parentTaskId;
        tileTaskView = tile.favInfo.taskView;
        // Save off the tile in a known property of the CARO object.  The response 
        // script from the server will use that known object to assign the retrieved
        // fav information to back to the tile.
        if (CARO) {
            CARO["tile" + tile.favInfo.trueTaskId.replace(/[^\w]/gi, '_')] = tile;
        }
    }
    // Check if we have a fully-initialized favInfo object on the tile.
    // Minimal tile favInfo objects are created when favorite task folders 
    // are restored on login.  These minimal objects contain only the path 
    // task ID information.  
    // Checking uniqueId should be sufficient to verify if the tile favInfo is fully 
    // populated.
    if (tileUniqueId) {
        // The tile's favInfo object is fully populated.  Only fetch fav information
        // for the tile's children.
        doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = 'favLC';
    }
    else {
        // The tile's favInfo object is not fully populated.  Fetch fav information
        // for both the tile and it's children.
        doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = 'favLCFull';
    }
    var taskView = '';
    if ((tileTaskView) && (tileTaskView.length > 0)) {
        taskView = tileTaskView;
    }
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKVIEW).value = taskView;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKID).value = tileTaskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = tileParentId;
    if (silent != true) {
        silent = false;
    }
    doc.getElementById("E1MFavoritesFormActivateSilent").value = silent;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function doFavoritesAction(taskId, parentTaskId, taskView, seqNum, action) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKID).value = taskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = parentTaskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKVIEW).value = taskView;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = action;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function doFavoritesActionManage(rowId, taskId, parentTaskId, taskView, seqNum, action, label, event) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKID).value = taskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = parentTaskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKVIEW).value = taskView;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = action;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_NEWNAME).value = label;
    if (action == "fr") {
        removeFavoriteManage(rowId, event);
        return;
    }
    if (action == "fm") {
        moveFavoriteManage(rowId, event);
        return;
    }
}

function doFavoritesAction(taskId, parentTaskId, taskView, seqNum, action, label, appId, formId, version, formMode) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKID).value = taskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = parentTaskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKVIEW).value = taskView;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = action;
    // The AOK values of the application/form that initiate the request.
    // If these values are present, the app/form was not launched from 
    // a task and a new one needs to be created.
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_APPID).value = appId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORMID).value = formId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_APPVERSION).value = version;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORMMODE).value = formMode;
    var unescapeLabel = unescape(label);
    // The UI presentation for a normal favorite add operation and a new task creation 
    // operation are the same on the client.
    if ((action == "fa") || (action == "frmNT")) {
        if (jdeMenuParent.favoriteLabels && jdeMenuParent.favoriteLabels.length != 0) {
            if (checkFavoriteDuplicate(taskId)) {
                showDuplicateFavoriteMessage(getFavoriteDuplicateName(taskId));
                return;
            }
        }
        if (isPreferenceChecked("addFavPreference")) {
            addToFavorite(unescapeLabel);
            return;
        }
        else {
            if (action == "frmNT") {
                doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_NEWNAME).value = unescapeLabel;
            }
            doFavoritesActionDirectSubmit();
            return;
        }
    }
    else if (action == "fo") {
        favoriteProperties(unescapeLabel);
        return;
    }
    else if (action == "frAll") {
        removeFavoriteAll();
        return;
    }
    else if (action == "fr") {
        removeFavorite();
        return;
    }
}
function checkFavoriteDuplicate(taskId) {
    var isDuplicateFavorite = false;
    for (var i = 0, len = jdeMenuParent.favoriteLabels.length; i < len; i++) {
        if (jdeMenuParent.favoriteID[i] == taskId) {
            isDuplicateFavorite = true;
            break;
        }
    }
    return isDuplicateFavorite;
}

function getFavoriteDuplicateName(taskId) {
    var duplicateFavoriteName = "";
    for (var i = 0, len = jdeMenuParent.favoriteLabels.length; i < len; i++) {
        if (jdeMenuParent.favoriteID[i] == taskId) {
            duplicateFavoriteName = jdeMenuParent.favoriteLabels[i];
            break;
        }
    }
    return duplicateFavoriteName;
}

function removeFavoriteAll() {
    var title = favoriteStaticText.favorites[0].favRemoveAll;
    var ID = '0_1_Fav_Remv_All';
    var depth = 5;

    if (isWSRPContainer()) {
        var okDeleteScript = "javascript: doFavoritesDeleteSubmit(); window['0_1_Fav_Remv_All'].onClose(); doFavoritesAllDeletefromDOM();"
        var cancelScript = "javascript:window['0_1_Fav_Remv_All'].onClose();return;"
    }
    else {
        var okDeleteScript = "javascript: doFavoritesDeleteSubmit(); window['0_1_Fav_Remv_All" + depth + "'].onClose(); doFavoritesAllDeletefromDOM();"
        var cancelScript = "javascript:window['0_1_Fav_Remv_All" + depth + "'].onClose();return;"
    }
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"
    var htmlContent = '<form><div align="center"><table align="center" summary=""><tr><td colspan=2><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favDeleteAllConfirmation + '</label></td></tr><tr><td colspan=2 align="right"><input type="button" class=button id="ok" value=' + favoriteStaticText.favorites[0].favOK + ' onClick="' + okDeleteScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="' + favoriteStaticText.favorites[0].favCancel + '" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input></td></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 150;
    var top = (screenHeight / 2) - 37;

    if (isWSRPContainer()) {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', '102', null, htmlContent);
        moveFocusToCloseForPopupWindow(ID, null);
    }
    else {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', '102', null, htmlContent, null, null, depth);
        moveFocusToCloseForPopupWindow(ID, depth);
    }
}

function doFavoritesActionSubmit(NewName) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_NEWNAME).value = NewName;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function doFavoritesActionDirectSubmit() {
    var doc = document;
    var manageFavActElem = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION);
    if (manageFavActElem.value == "frmNT") {
        manageFavActElem.value = "frmNT";
    }
    else {
        manageFavActElem.value = "faDirect";
    }
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function doFavoritesDeleteSubmit() {
    document.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function doFavoritesMoveSubmit() {
    document.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}


function addToFavorite(label) {
    var title = favoriteStaticText.favorites[0].favAdding;
    var ID = '0_1_Fav_Add';
    var defaultName = label;

    var okScript = "javascript: doFavoritesActionSubmit(form.FavoriteName.value); javascript: storeAddFavPreferences(); javascript: window['0_1_Fav_Add'].onClose();"
    var cancelScript = "javascript: window['0_1_Fav_Add'].onClose();"
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"
    var keyDownScript = "javascript:  if (event.keyCode == 13) {document.getElementById('ok').click(); stopEventBubbling(event);}";
    if (userAccessibilityMode)
        var htmlContent = '<form id="favFrom" name="favFrom"><div align="center"><table align="center" onmouseover="javascript:setFocusOnPopup(\'hidbuttonFav\');"><tr><td colspan=2 align="center"><label align="center" class=FavoritesPopupLabel> ' + favoriteStaticText.favorites[0].favNameToAdd + ':</label><INPUT  align="center" class=FavoritesPopupLabel size=26 name=FavoriteName id=FavoriteName MAXLENGTH=80 onkeydown ="' + keyDownScript + '" value="' + defaultName + '"></input></td></tr><tr><td colspan=2 align="center">&nbsp;&nbsp;&nbsp;&nbsp;<input aria-labelledby=\"cbLabel\" type="checkbox" name="addFavCheckbox" value="check";><label id=\"cbLabel\" class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favMessage + '</label></input></td></tr><tr><td colspan=2 align="center"><a href="javascript:manageFavorites()" onClick="' + cancelScript + '";>' + favoriteStaticText.favorites[0].favManage + '</a></td></tr><tr align="right"><td colspan=2 align="right"><input type="button" class=button id="ok" value="' + favoriteStaticText.favorites[0].favCreate + '" onClick="' + okScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="' + favoriteStaticText.favorites[0].favCancel + '" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" style="visibility:hidden;" name="hidbuttonFav" id="hidbuttonFav"/></td></tr></table></div></form>';
    else
        var htmlContent = '<form id="favFrom" name="favFrom"><div align="center"><table align="center" onmouseover="javascript:setFocusOnPopup(\'hidbuttonFav\');"><tr><td colspan=2 align="center"><label align="center" class=FavoritesPopupLabel> ' + favoriteStaticText.favorites[0].favNameToAdd + ':</label><INPUT  align="center" class=FavoritesPopupLabel size=26 name=FavoriteName id=FavoriteName MAXLENGTH=80 onkeydown ="' + keyDownScript + '" value="' + defaultName + '"></input></td></tr><tr><td colspan=2 align="center">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="addFavCheckbox" value="check";><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favMessage + '</label></input></td></tr><tr><td colspan=2 align="center"><a href="javascript:manageFavorites()" onClick="' + cancelScript + '";>' + favoriteStaticText.favorites[0].favManage + '</a></td></tr><tr align="right"><td colspan=2 align="right"><input type="button" class=button id="ok" value="' + favoriteStaticText.favorites[0].favCreate + '" onClick="' + okScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="' + favoriteStaticText.favorites[0].favCancel + '" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" style="visibility:hidden;" name="hidbuttonFav" id="hidbuttonFav"/></td></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 115;
    var top = (screenHeight / 2) - 60;
    var height = !isIOS ? '135' : '165';
    createPopup(ID, SHOW_POPUP_FAV, true, false, true, false, title, null, null, null, null, null, left, top, '230', height, null, htmlContent);
    document.getElementById('FavoriteName').select();
}

function favoriteProperties(task) {
    var title = favoriteStaticText.favorites[0].favProperties;
    var ID = '0_1_Fav_Pro';
    var defaultName = task;

    var okScript = "javascript: doFavoritesActionSubmit(form.FavoriteName.value); window['0_1_Fav_Pro'].onClose();"
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"

    var cancelScript = "javascript:window['0_1_Fav_Pro'].onClose();"
    var keyDownScript = "javascript:  if (event.keyCode == 13) {form.ok.click();}";
    var htmlContent = '<form><div align="center"><table align="center"><tr><td colspan=2><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favNameToAdd + ':</label><INPUT class=FavoritesPopupLabel size=26 name=FavoriteName id=FavoriteName MAXLENGTH=80 onkeydown ="' + keyDownScript + '" value="' + defaultName + '"></input></td></tr></tr><tr><td colspan=2 align="center"><a href="javascript:manageFavorites()" onClick="' + cancelScript + '";>' + favoriteStaticText.favorites[0].favManage + '</a></td></tr><tr><td colspan=2 align="right"><input type="button" class=button id="ok" value=' + favoriteStaticText.favorites[0].favSaveChanges + ' onClick="' + okScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="' + favoriteStaticText.favorites[0].favCancel + '" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 115;
    var top = (screenHeight / 2) - 50;
    var height = !isIOS ? '115' : '145';

    createPopup(ID, SHOW_POPUP_FAV, true, false, true, false, title, null, null, null, null, null, left, top, '230', height, null, htmlContent);

    document.getElementById('FavoriteName').select();
}

function removeFavoriteManage(rowId, event) {
    var title = favoriteStaticText.favorites[0].favRemove;
    var ID = '0_1_Fav_Remv';
    var depth = 5;

    if (isWSRPContainer()) {
        var okDeleteScript = "javascript: doFavoritesDeleteSubmit(); window['0_1_Fav_Remv'].onClose(); doFavoritesDeletefromDOM('" + rowId + "');"
        var cancelScript = "javascript:window['0_1_Fav_Remv'].onClose();return;"
    }
    else {
        var okDeleteScript = "javascript: doFavoritesDeleteSubmit(); window['0_1_Fav_Remv" + depth + "'].onClose(); doFavoritesDeletefromDOM('" + rowId + "');"
        var cancelScript = "javascript:buttonAreaMouseUp(event);javascript:window['0_1_Fav_Remv" + depth + "'].onClose();return;"
    }
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"
    var htmlContent = '<form><div align="center"><table align="center" summary=""><tr><td colspan=2><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favDeleteConfirmation + '</label></td></tr><tr><td colspan=2 align="right"><input type="button" class=button id="ok" value="OK" onClick="' + okDeleteScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="' + favoriteStaticText.favorites[0].favCancel + '" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input></td></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 150;
    var top = (screenHeight / 2) - 37;
    var height = !isIOS ? '102' : '132';

    if (isWSRPContainer()) {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', height, null, htmlContent);
    }
    else {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', height, null, htmlContent, null, null, depth);
    }
}

function moveFavoriteManage(rowId, event) {
    var title = favoriteStaticText.favorites[0].favMove;
    var ID = '0_1_Fav_Move';
    var depth = 5;

    if (isWSRPContainer()) {
        var okDeleteScript = "javascript: doFavoritesMoveSubmit(); window['0_1_Fav_Move'].onClose(); doFavoritesDeletefromDOM('" + rowId + "');"
        var cancelScript = "javascript:window['0_1_Fav_Move'].onClose();return;"
    }
    else {
        var okDeleteScript = "javascript: doFavoritesMoveSubmit(); window['0_1_Fav_Move" + depth + "'].onClose(); doFavoritesDeletefromDOM('" + rowId + "');"
        var cancelScript = "javascript:buttonAreaMouseUp(event);javascript:window['0_1_Fav_Move" + depth + "'].onClose();return;"
    }

    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"

    var htmlContent = '<form><div align="center"><table align="center"><tr><td colspan=2><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favMoveConfirmation + '</label></td></tr><tr><td colspan=2 align="right"><input type="button" class=button id="ok" value="OK" onClick="' + okDeleteScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="' + favoriteStaticText.favorites[0].favCancel + '" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input></td></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 150;
    var top = (screenHeight / 2) - 37;
    if (isWSRPContainer()) {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', '102', null, htmlContent);
    }
    else {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', '102', null, htmlContent, null, null, depth);
    }

    // The popup may render while other events hang in the balance--for example, buttonAreaMouseUp() per bug #18456966.
    // Prevent other, unrelated events from getting tangled in the events of the popup.
    var returnValue = stopEventBubbling(event);
    return returnValue;
}

function doFavoritesDeletefromDOM(rowId) {
    var doc = document;
    if (doc.getElementById(rowId) == null) {
        return;
    }
    var rowCount = doc.getElementById(rowId).rowIndex;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TABLE).deleteRow(rowCount);
    if (jdeMenuParent.favoriteParent && jdeMenuParent.favoriteLabels.length <= 1) {
        doc.getElementById(DDMENU.CONST.DELETEALL).disabled = true;
    }
}

function doFavoritesAllDeletefromDOM() {
    var j = 0;
    var rowID = "";
    var doc = document;
    var rowIDElem = doc.getElementById(rowID);
    for (var i = 0, len = jdeMenuParent.favoriteLabels.length; i < len; i++) {
        rowID = "Row_" + (i + j);
        if (rowIDElem == null) {
            j++;
            rowID = "Row_" + (i + j);
        }
        if (rowIDElem != null) {
            var rowCount = rowIDElem.rowIndex;
            doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TABLE).deleteRow(rowCount);
        }
    }
    doc.getElementById(DDMENU.CONST.DELETEALL).disabled = true;
}

function removeFavorite() {
    var title = favoriteStaticText.favorites[0].favRemove;
    var ID = '0_1_Fav_Remv';
    var depth = 5;

    if (isWSRPContainer()) {
        var cancelScript = "javascript:window['0_1_Fav_Remv'].onClose();return;"
        var okDeleteScript = "javascript: doFavoritesDeleteSubmit(); window['0_1_Fav_Remv'].onClose();"
    }
    else {
        var cancelScript = "javascript:window['0_1_Fav_Remv" + depth + "'].onClose();return;"
        var okDeleteScript = "javascript: doFavoritesDeleteSubmit(); window['0_1_Fav_Remv" + depth + "'].onClose();"
    }
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"
    var htmlContent = '<form><div align="center"><table align="center" summary=""><tr><td colspan=2><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favDeleteConfirmation + '</label></td></tr><tr><td colspan=2 align="right"><input type="button" class=button id="ok" value="OK" onClick="' + okDeleteScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="Cancel" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input></td></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 150;
    var top = (screenHeight / 2) - 37;
    var height = !isIOS ? '102' : '132';
    if (isWSRPContainer()) {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', height, null, htmlContent);
    }
    else {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', height, null, htmlContent, null, null, depth);
    }
}

function showDuplicateFavoriteMessage(unescapeLabel) {
    var title = favoriteStaticText.favorites[0].favAdding; ;
    var ID = '0_1_Fav_Dup';
    var depth = 5;

    if (isWSRPContainer()) {
        var cancelScript = "javascript:window['0_1_Fav_Dup'].onClose();return;"
        var okRemaneScript = "javascript:favoriteProperties('" + unescapeLabel + "'); window['0_1_Fav_Dup'].onClose();"
    }
    else {
        var cancelScript = "javascript:window['0_1_Fav_Dup" + depth + "'].onClose();return;"
        var okRemaneScript = "javascript:favoriteProperties('" + unescapeLabel + "'); window['0_1_Fav_Dup" + depth + "'].onClose();"
    }
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"
    var htmlContent = '<form><div align="center"><table align="center"><tr><td colspan=2><label class=FavoritesPopupLabel>' + favoriteStaticText.favorites[0].favDuplicateConfirmation + '</label></td></tr><tr><td colspan=2 align="right"><input type="button" class=button id="ok" value="OK" onClick="' + okRemaneScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input><input type="button" class=button id="Cancel" value="Cancel" onClick="' + cancelScript + '"; onmouseover="' + onMouseOverScript + '"; onmouseout="' + onMouseOutScript + '"; onmousedown="' + onMouseDownScript + '";></input></td></tr></table></div></form>';

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 150;
    var top = (screenHeight / 2) - 37;
    if (isWSRPContainer()) {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', '102', null, htmlContent);
        moveFocusToCloseForPopupWindow(ID, null);
    }
    else {
        createPopup(ID, SHOW_POPUP_FAV, true, false, false, false, title, null, null, null, null, null, left, top, '300', '102', null, htmlContent, null, null, depth);
        moveFocusToCloseForPopupWindow(ID, depth);
    }
}

// set the focus to the close icon on the popup window, so that keyboard user can tab to other controls on the popup easily
function moveFocusToCloseForPopupWindow(popupId, depth) {
    var domId = "popupWindowTitle" + popupId + (depth ? depth : "");
    var titleDiv = document.getElementById(domId);
    if (titleDiv && titleDiv.lastChild && titleDiv.lastChild.lastChild)
        titleDiv.lastChild.lastChild.focus();
}

function deleteFavRowManage(event, rowId, taskId, parentTaskId, taskView, seqNum, encodedLabel) {
    doFavoritesActionManage(rowId, taskId, parentTaskId, taskView, seqNum, 'fr', encodedLabel, event);
}

function moveFavRowManage(event, rowId, taskId, parentTaskId, taskView, seqNum, label) {
    doFavoritesActionManage(rowId, taskId, parentTaskId, taskView, seqNum, 'fm', label, event);
}

function focusFavContent(el) { el.select(); }

function renameChangeContent(event, renameID, taskId, parentTaskId, taskView, seqNum, encodedLabel, favLabelID) {
    var doc = document;
    var renameCell = doc.getElementById(renameID);
    var labelDivCell = doc.getElementById(favLabelID);
    var label = unescape(encodedLabel);
    var size = (label.length + 3);
    if (!isIE) {
        renameCell.innerHTML = "<input type=\"text\" size=" + size + " MAXLENGTH=80 name=\"favRename\" id=\"favoriteRename\"  onkeydown =\"javascript:  if (event.keyCode == 13) {submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + encodedLabel + "');}\" onClick=\"javascript:setFocus();\" onMouseDown = \"javascript:stopEventBubbling(event);\" onBlur=\"javascript:submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + encodedLabel + "');\" value=\"" + label + "\">";
    }
    else {
        renameCell.innerHTML = "<input type=\"text\" size=" + size + " MAXLENGTH=80 name=\"favRename\" id=\"favoriteRename\"  onkeydown =\"javascript:  if (event.keyCode == 13) {submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + encodedLabel + "');}\" onMouseDown = \"javascript:stopEventBubbling(event);\" onBlur=\"javascript:submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + encodedLabel + "');\" value=\"" + label + "\">";
    }
    renameCell.style.display = "inline-block";
    labelDivCell.style.display = "none";
    renameCell.firstChild.focus();
    renameCell.firstChild.select();
    renameCell.firstChild.style.cursor = "text";
    stopEventBubbling(event);
}

function showUDFSelWindow(event, rowId, favRowIndex, parentTaskId, taskView, seqNum) {
    var title = favoriteStaticText.favorites[0].favReorganizeTitle;
    var newRowIndex = document.getElementById(rowId).rowIndex;
    var ID = 'favReOrganizePopup';
    var screenWidth;
    var screenHeight;
    var okScript = "<input type=\"button\" class=button id=\"ok\" onClick=\"javascript: window['favReOrganizePopup'].onClose();\" value=\"Ok\"></input>";
    var cancelScript = "javascript: window['favReOrganizePopup'].onClose();"

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var left = (screenWidth / 2) - 115;
    var top = (screenHeight / 2) - 60;
    var height = 135;
    var sb = new PSStringBuffer();
    sb.append("<form id='favReOrgFrom' name='favReOrgFrom'><div align='center'>");
    sb.append("<table summary=\"\" id=\"favReOrgTable\"><tbody><tr rowspan=2><td>").append(favoriteStaticText.favorites[0].favReorganizeSelection).append("</td></tr><tr><td nowrap=\"nowrap\"><select id=\"selectedUDF\">");

    for (var i = 0, len = jdeMenuParent.favoriteLabels.length; i < len; i++) {
        if (jdeMenuParent.favoriteType[i] == "21" && jdeMenuParent.favoriteLabels[i].length > 0) {
            sb.append("<option value='");
            sb.append(i);
            sb.append("'>");
            sb.append(jdeMenuParent.favoriteLabels[i]);
            sb.append("</option>");
        }
    }
    sb.append("</select></td>");
    sb.append("<td><input type=\"button\" class=button id=\"ok\" value=\"OK\" onclick=\"javascript: reorganizeManageFavorites(event, '" + rowId + "', '" + favRowIndex + "', '" + newRowIndex + "', document.favReOrgFrom.selectedUDF.options[document.favReOrgFrom.selectedUDF.selectedIndex].value); window['favReOrganizePopup'].onClose();\"></input></td>");
    sb.append("</tr></tbody></table></div></form>");
    createPopup(ID, SHOW_POPUP_FAV, true, false, true, false, title, null, null, null, null, null, left, top, '230', height, null, sb);
}

function setFocus() {
    var doc = document;
    var renameValue = doc.getElementById("favoriteRename").value;
    var favoriteRenameElem = doc.getElementById("favoriteRename");
    if (renameValue != null) {
        favoriteRenameElem.value = "";
        favoriteRenameElem.value = renameValue;
        favoriteRenameElem.focus();
    }
}

function newFolderRename(renameID, taskId, parentTaskId, taskView, seqNum, label, favLabelID) {
    var doc = document;
    var renameCell = doc.getElementById(renameID);
    var labelDivCell = doc.getElementById(favLabelID);
    var cellValue = (renameCell.textContent === undefined) ? renameCell.innerText : renameCell.textContent;
    var size = (label.length + 3);
    if (!isIE) {
        renameCell.innerHTML = "<input type=\"text\" size=" + size + " MAXLENGTH=80 name=\"favRename\" id=\"favoriteRename\" onkeydown =\"javascript:  if (event.keyCode == 13) {submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + cellValue + "');}\" onClick=\"javascript:setFocus();\" onMouseDown = \"javascript:stopEventBubbling(event);\" onBlur=\"javascript:submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + cellValue + "');\" value=\"" + cellValue + "\">";
    }
    else {
        renameCell.innerHTML = "<input type=\"text\" size=" + size + " MAXLENGTH=80 name=\"favRename\" id=\"favoriteRename\" onkeydown =\"javascript:  if (event.keyCode == 13) {submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + cellValue + "');}\" onMouseDown = \"javascript:stopEventBubbling(event);\" onBlur=\"javascript:submitNewName(this,'" + renameID + "','" + taskId + "','" + parentTaskId + "','" + taskView + "','" + seqNum + "','" + cellValue + "');\" value=\"" + cellValue + "\">";
    }
    renameCell.style.display = "inline-block";
    labelDivCell.style.display = "none";
    if (!isNativeContainer) {
        renameCell.firstChild.focus();
        renameCell.firstChild.select();
    }
    renameCell.firstChild.style.cursor = "text";
}

function submitNewName(textfield, renameID, taskId, parentTaskId, taskView, seqNum, oldRenameValue) {
    var textValue = textfield.value;
    if (textValue == "") {
        textfield.value = oldRenameValue;
    }
    var renameElement = document.getElementById(renameID);
    if (renameElement.textContent !== undefined) {
        renameElement.textContent = textfield.value;
    }
    else {
        renameElement.innerText = textfield.value;  // IE8
    }
    if (textValue == oldRenameValue) {
        return;
    }
    var newName = textfield.value;
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKID).value = taskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = parentTaskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKVIEW).value = taskView;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "fo";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_NEWNAME).value = newName;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function reOrderFavoritesFrom(seqNum, parentTaskId) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = parentTaskId;
}

function reOrderFavoritesTo(seqNum) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM_MOVED).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "frReOrder";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function reOrderFavorites(seqNum, parentTaskId, seqNumMoved) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = seqNum;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = parentTaskId;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM_MOVED).value = seqNumMoved;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "frReOrder";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function favItemClicked(favRowIndex) {
    var doc = document;
    if (prevClickedRowIndex != null || prevClickedRowIndex != undefined) {
        if (doc.getElementById('rename_' + prevClickedRowIndex) != null) {
            if (!jdeMenuParent.favoriteParent) {
                doc.getElementById('move_' + prevClickedRowIndex).style.display = "none";
            }
            doc.getElementById('rename_' + prevClickedRowIndex).style.display = "none";
            doc.getElementById('delete_' + prevClickedRowIndex).style.display = "none";
            if (userAccessibilityMode) {
                if (doc.getElementById('reorganize_' + prevClickedRowIndex) != null)
                    doc.getElementById('reorganize_' + prevClickedRowIndex).style.display = "none";
                if (doc.getElementById('moveup_' + prevClickedRowIndex) != null)
                    doc.getElementById('moveup_' + prevClickedRowIndex).style.display = "none";
                if (doc.getElementById('movedown_' + prevClickedRowIndex) != null)
                    doc.getElementById('movedown_' + prevClickedRowIndex).style.display = "none";
            }
        }
    }
    if (!jdeMenuParent.favoriteParent) {
        doc.getElementById('move_' + favRowIndex).style.display = "block";
    }
    doc.getElementById('rename_' + favRowIndex).style.display = "block";
    doc.getElementById('delete_' + favRowIndex).style.display = "block";
    if (userAccessibilityMode) {
        if (doc.getElementById('reorganize_' + favRowIndex) != null)
            doc.getElementById('reorganize_' + favRowIndex).style.display = "block";
        if (doc.getElementById('moveup_' + favRowIndex) != null)
            doc.getElementById('moveup_' + favRowIndex).style.display = "block";
        if (doc.getElementById('movedown_' + favRowIndex) != null)
            doc.getElementById('movedown_' + favRowIndex).style.display = "block";
    }
    prevClickedRowIndex = favRowIndex;
}

function buttonAreaMouseUp(ev) {
    if (dragObject != null) {
        dragObject = null;
        document.getElementById(draggedRowId).removeChild(favItemDragClone);
        favItemDragClone = null;
        isDragInProgress = false;
        clearFavDivIntervalScrollUp();
        clearFavDivIntervalScrollDown();
    }
    stopEventBubbling(ev);
}

function manageFavoritesMouseOut(event) {
    var limitOffset = 30;
    var manageFavElem = document.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV);
    var manageFavLeft = manageFavElem.style.left;
    var manageFavTop = manageFavElem.style.top;
    var manageFavWidth = manageFavElem.style.width;
    var manageFavHeight = manageFavElem.style.height;
    var leftValue = parseInt(manageFavLeft.substring(0, manageFavLeft.length - 2));
    var topValue = parseInt(manageFavTop.substring(0, manageFavTop.length - 2));
    var widthValue = parseInt(manageFavWidth.substring(0, manageFavWidth.length - 2));
    var heightValue = parseInt(manageFavElem.offsetHeight);
    var bottomValue = topValue + heightValue - limitOffset;
    var topLimit = topValue + limitOffset;
    var widthLimit = leftValue + widthValue - limitOffset;
    var leftLimit = leftValue + (limitOffset - 10);

    var mouseX = event.pageX;
    var mouseY = event.pageY;
    if (mouseX < leftLimit || mouseY > bottomValue || mouseY < topLimit || mouseX > widthLimit) {
        buttonAreaMouseUp(event);
    }
}

function favItemTouchStart(ev, favRowIndex) {
    if (ev.target.tagName.toLowerCase() == "input" && ev.target.id == "favoriteRename") {
        ev.target.select();
        return;
    }
    else {
        favItemMouseDown(ev, favRowIndex);
    }
}

function favItemMouseDown(ev, favRowIndex) {
    var doc = document;
    var rowId = "Row_" + favRowIndex;
    var taskLabelId = "label_" + favRowIndex;
    var tdId1 = 'label_' + favRowIndex;
    var tableId = DDMENU.CONST.ELEMENT_MANAGEFAV_TABLE;
    var newRowIndex = doc.getElementById(rowId).rowIndex;
    var element = doc.getElementById(tdId1);
    favItemDragClone = element.cloneNode(true);
    var renameCell = doc.getElementById('favoriteRename');
    draggedRowId = rowId;
    if (renameCell != null) {
        renameCell.blur();
    }
    favItemClicked(favRowIndex);

    element.unselectable = "on";
    element.style.userSelect = "none"; // w3c standard
    if (ev.preventDefault) {
        // Prevents trying to select when dragging in firefox and chrome
        ev.preventDefault();
    }
    else {
        // Should prevent selection in IE but does not seem to work reliably on its own
        ev.returnValue = false;
    }
    favItemDragClone.style.position = 'absolute';
    favItemDragClone.setAttribute('id', 'favItemDragClone');
    doc.getElementById(rowId).appendChild(favItemDragClone);

    dragObject = favItemDragClone;
    isUserFolderDragged = doc.getElementById("favType_" + favRowIndex) == null ? false : true;
    var touchX = touchY = 0;
    var lastTouchElem = null;


    favIntervalScrollUp = 0;
    favSetInvCalledScrollUp = false;
    favIntervalScrollDown = 0;
    favSetInvCalledScrollDown = false;

    var favTableObj = doc.getElementById(tableId);
    favTableObj.onmousemove = favTableObj.ontouchmove = favItemMouseMove;
    favTableObj.onmouseup = favTableObj.ontouchend = favItemMouseUp;

    function favItemMouseMove(e) {
        if (dragObject != null) {
            var e = e || window.event;
            isDragInProgress = true;

            var favMang = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV);
            var manageFavLeft = favMang.style.left;
            var leftValue = manageFavLeft.substring(0, manageFavLeft.length - 2);
            var manageFavTop = favMang.style.top;
            var topValue = manageFavTop.substring(0, manageFavTop.length - 2);
            var x = ((isTouchEnabled ? e.touches[0].pageX : e.pageX) - parseInt(leftValue)) + "px"; //subtract the pop up window's offset from the document's mouse left
            var y = ((isTouchEnabled ? e.touches[0].pageY : e.pageY) - parseInt(topValue)) + "px";  //subtract the pop up window's offset from the document's mouse top
            dragObject.style.top = y;
            dragObject.style.left = x;
            /*Code for scrolling while dragging - Begin*/
            var mouseOffset = ((isTouchEnabled ? e.touches[0].pageY : e.pageY) - parseInt(topValue));
            favItemDrag(mouseOffset);
            /*Code for scrolling while dragging - End*/
            if (isTouchEnabled) {
                touchX = e.touches[0].pageX;
                touchY = e.touches[0].pageY;
                var elem = document.elementFromPoint(touchX - 5, touchY - 5);
                if (elem && elem.id && elem.id != null && elem.id != '') {
                    if (elem.id.indexOf("label_") != -1 || elem.id.indexOf("emptyRowLabel_") != -1) {
                        if (lastTouchElem != elem) {
                            if (lastTouchElem != null) {
                                lastTouchElem.parentNode.onmouseout(e);
                            }
                            lastTouchElem = elem;
                            lastTouchElem.parentNode.onmouseover(e);
                        }
                    }
                }
            }
            var manageFavLeft = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV).style.left;
            var manageFavTop = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV).style.top;

            return false;
        }
    };
    function favItemMouseUp(e) {
        clearFavDivIntervalScrollUp();
        clearFavDivIntervalScrollDown();

        if (dragObject != null) {
            var e = e || window.event;
            dragObject = null;
            doc.getElementById(rowId).removeChild(favItemDragClone);
            favItemDragClone = null;
            isDragInProgress = false;
            if (rowId == currFavoriteId) {
                return;
            }
            if (currFavoriteId.substr(0, 3) == "Row") {
                if (isUserFolderDragged) {
                    isUserFolderDragged = false;
                    return;
                }
                reorganizeManageFavorites(e, rowId, favRowIndex, newRowIndex, null);
            }
            else {
                reorderManageFavorites(e, rowId, favRowIndex, newRowIndex);
                isUserFolderDragged = false;
            }
        }
        touchX = touchY = 0;
        lastTouchElem = null;

    };

    //New function added to scroll while dragging the favorites   
    function favItemDrag(mouseOffset) {
        var favTableObj = doc.getElementById(tableId);
        var topLimit = favTableObj.rows[0].offsetHeight * 2;
        var scrollDivObj = favTableObj.parentElement;
        if (mouseOffset <= topLimit) {
            if (favSetInvCalledScrollUp == false) {
                favSetInvCalledScrollUp = true;
                favIntervalScrollUp = setInterval(function () { favDivScroll(scrollDivObj, iScrollFactor * -1); }, 50);
            }
            if (scrollDivObj.scrollTop <= 0) {
                clearFavDivIntervalScrollUp();
            }
        }
        else {
            clearFavDivIntervalScrollUp();
        }
        var height = scrollDivObj.offsetHeight - favTableObj.rows[0].offsetHeight;
        if (mouseOffset >= height) {
            if (favSetInvCalledScrollDown == false) {
                favSetInvCalledScrollDown = true;
                favIntervalScrollDown = setInterval(function () { favDivScroll(scrollDivObj, iScrollFactor); }, 50);
            }
            if (scrollDivObj.scrollTop >= scrollDivObj.scrollHeight) {
                clearFavDivIntervalScrollDown();
            }
        }
        else {
            clearFavDivIntervalScrollDown();
        }
    };


    function favDivScroll(scrollDivObj, iScrollFactor) {
        scrollDivObj.scrollTop += iScrollFactor;
    };

}

function clearFavDivIntervalScrollUp() {
    clearInterval(favIntervalScrollUp);
    favSetInvCalledScrollUp = false;
}

function clearFavDivIntervalScrollDown() {
    clearInterval(favIntervalScrollDown);
    favSetInvCalledScrollDown = false;
}


function reorderManageFavorites(e, rowId, favRowIndex, newRowIndex) {
    var rowIndex = currFavoriteId.substring(9, currFavoriteId.length);
    if (rowIndex == favRowIndex || rowIndex == (favRowIndex + 1)) {
        unHighlightFavoriteBorder(rowIndex);
        return;
    }
    if (rowIndex > favRowIndex) {
        reOrderFavoritesTo(jdeMenuParent.favoriteSequenceNumber[rowIndex - 1]);
    }
    else {
        reOrderFavoritesTo(jdeMenuParent.favoriteSequenceNumber[rowIndex]);
    }
    var doc = document;
    var table = doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TABLE);
    var tdId1 = 'uLabel_' + favRowIndex;
    var tdId2 = 'uTypeLabel_' + favRowIndex;
    var row;
    var row1;
    if (rowIndex > favRowIndex) {
        row1 = table.insertRow(doc.getElementById(currFavoriteId).rowIndex);
        row = table.insertRow(doc.getElementById(currFavoriteId).rowIndex);
    }
    else {
        row = table.insertRow(doc.getElementById(currFavoriteId).rowIndex + 1);
        row1 = table.insertRow(doc.getElementById(currFavoriteId).rowIndex + 2);
    }
    row.id = "uRow_" + favRowIndex;

    var cellOne = row.insertCell(0);
    cellOne.id = "uLabel_" + favRowIndex;
    cellOne.align = "left";
    cellOne.setAttribute('class', 'GridCell');

    var labelElement = doc.getElementById("label_" + favRowIndex);
    if (labelElement && labelElement.textContent !== undefined) {
        cellOne.textContent = "     " + labelElement.textContent;
    }
    else {
        // IE8
        cellOne.innerText = "     " + doc.getElementById("favLabel_" + favRowIndex).innerText;
    }
    var cellTwo = row.insertCell(1);
    cellTwo.id = "uTypeLabel_" + favRowIndex;
    cellTwo.align = "left";
    cellTwo.setAttribute('class', 'GridCell');

    var typeLabelElement = doc.getElementById("typeLabel_" + favRowIndex);
    if (typeLabelElement && typeLabelElement.textContent !== undefined) {
        cellTwo.textContent = typeLabelElement.textContent;
    }
    else {
        // IE8
        cellTwo.innerText = typeLabelElement.innerText;
    }
    if (rowIndex > favRowIndex) {
        table.deleteRow(newRowIndex - 1);
        table.deleteRow(newRowIndex - 1);
    }
    else {
        table.deleteRow(newRowIndex + 2);
        table.deleteRow(newRowIndex + 2);
    }
}

function reorganizeManageFavorites(e, rowId, favRowIndex, newRowIndex, currRow) {
    var currRowIndex;
    if (userAccessibilityMode) {
        currRowIndex = currRow;
    }
    else {
        currRowIndex = Math.floor((document.getElementById(currFavoriteId).rowIndex - 1) / 2);
    }
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKID).value = jdeMenuParent.favoriteID[favRowIndex];
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = jdeMenuParent.favoriteParentTaskID[favRowIndex];
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_TASKVIEW).value = jdeMenuParent.favoriteView[favRowIndex];
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_SEQNUM).value = jdeMenuParent.favoriteSequenceNumber[favRowIndex];
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "moveToUserFolder";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_NEWNAME).value = jdeMenuParent.favoriteLabels[favRowIndex];
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_USERDEFINED_FOLDER_TASKID).value = jdeMenuParent.favoriteID[currRowIndex];
    doFavoritesDeletefromDOM(rowId);
    document.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function highlightFavoriteBorder(rowIndex) {
    var emptyId = 'emptyRowLabel_' + rowIndex;
    var rowId = "emptyRow_" + rowIndex;
    if (isDragInProgress) {
        document.getElementById(rowId).style.backgroundColor = "#87AFC7";
    }
    currFavoriteId = rowId;
}

function unHighlightFavoriteBorder(rowIndex) {
    var emptyId = 'emptyRowLabel_' + rowIndex;
    var rowId = "emptyRow_" + rowIndex;
    var doc = document;
    if (rowIndex % 2 == 0) {
        doc.getElementById(rowId).style.backgroundColor = "#F2F2F5";
    }
    else {
        doc.getElementById(rowId).style.backgroundColor = "#F9F9F9";
    }
}

function highlightFavoriteRow(rowIndex) {
    var highlightcolor = "#CFE0F1";
    var rowId = "Row_" + rowIndex;
    var tdId1 = 'label_' + rowIndex;
    var tdId2 = 'typeLabel_' + rowIndex;
    var labelId = 'favLabel_' + rowIndex;
    var emptyId = "emptyRow_" + rowIndex;
    var doc = document;
    var favType = document.getElementById("favType_" + rowIndex) == null ? false : true;
    if (isDragInProgress && !favType) {
        return;
    }
    else if (isDragInProgress && isUserFolderDragged) {
        return;
    }
    else {
        doc.getElementById(tdId1).style.backgroundColor = highlightcolor;
        doc.getElementById(tdId1).style.cursor = "move";
        doc.getElementById(labelId).style.cursor = "move";
        doc.getElementById(tdId2).style.backgroundColor = highlightcolor;
        if (!userAccessibilityMode)
            doc.getElementById(emptyId).style.backgroundColor = highlightcolor;
    }

    currFavoriteId = rowId;
}


function unHighlightFavoriteRow(rowIndex) {
    var highlightcolor = "";
    if (rowIndex % 2 == 0) {
        highlightcolor = "#F2F2F5";
    }
    else {
        highlightcolor = "#F9F9F9";
    }
    var rowId = "Row_" + rowIndex;
    var tdId1 = 'label_' + rowIndex;
    var tdId2 = 'typeLabel_' + rowIndex;
    var labelId = 'favLabel_' + rowIndex;
    var emptyId = "emptyRow_" + rowIndex;


    var favType;
    var tdTypeElement = document.getElementById(tdId2);
    if (tdTypeElement) {
        tdTypeElement.style.backgroundColor = highlightcolor;
        favType = (tdTypeElement.textContent === undefined) ? tdTypeElement.innerText : tdTypeElement.textContent;
    }
    var doc = document;
    doc.getElementById(tdId1).style.backgroundColor = highlightcolor;
    doc.getElementById(tdId1).style.cursor = "move";
    doc.getElementById(labelId).style.cursor = "move";
    if (!userAccessibilityMode)
        doc.getElementById(emptyId).style.backgroundColor = highlightcolor;
}

function newFolderScript() {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "frNewFolder";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function showDeleteHover(delete_ID) {
    var doc = document;
    doc.getElementById(delete_ID).src = window["E1RES_img_FAV_deletefavorites_ovr_png"];
    doc.getElementById(delete_ID).style.cursor = "pointer";
}

function unShowDeleteHover(delete_ID) {
    document.getElementById(delete_ID).src = window["E1RES_img_FAV_deletefavorites_ena_png"];
}

function showRenameHover(rename_Id) {
    var doc = document;
    doc.getElementById(rename_Id).src = window["E1RES_img_FAV_edit_all_ovr_png"];
    doc.getElementById(rename_Id).style.cursor = "pointer";
}

function unShowRenameHover(rename_Id) {
    document.getElementById(rename_Id).src = window["E1RES_img_FAV_edit_all_ena_png"];
}

function unShowMoveHover(move_Id) {
    document.getElementById(move_Id).src = window["E1RES_img_FAV_move_png"];
}

function showMoveHover(move_Id) {
    document.getElementById(move_Id).src = window["E1RES_img_FAV_move_png"];
    document.getElementById(move_Id).style.cursor = "pointer";
}

// These two functions are meant to work with elements that already have a CSS class
function addHover(elem) {
    elem.className += " hover";
}
function removeHover(elem) {
    var idx = elem.className.indexOf(" hover");
    if (idx > -1) {
        elem.className = elem.className.substring(0, idx);
    }
}

function showArrow(type_Id) {
    document.getElementById(type_Id).style.cursor = "arrow";
}

function teProps(taskview, taskId, parentTaskId, sequence, role, name) {
    e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI("TETaskProperties");

    if (taskview != '') { e1UrlCreator.setParameter("taskview", teEscape(taskview)); }
    if (taskId != '') { e1UrlCreator.setParameter("taskId", teEscape(taskId)); }
    if (parentTaskId != '') { e1UrlCreator.setParameter("parentTaskId", teEscape(parentTaskId)); }
    if (sequence != '') { e1UrlCreator.setParameter("sequence", teEscape(sequence)); }
    if (role != '') { e1UrlCreator.setParameter("role", teEscape(role)); }
    if (name != '') { e1UrlCreator.setParameter("name", teEscape(name)); }

    E1Url = e1UrlCreator.toString();
    var theWindow;
    theWindow = window.open(E1Url, "TaskPropsWindow", "toolbar,location,status,menubar,scrollbars,resizable,,width=740,height=540");
    theWindow.focus();
}

function teEscape(parameter) {
    var jsEscapedParameter = escape(parameter);
    var tokens = jsEscapedParameter.split("+");
    var newParameter = "";
    var i = 0;
    for (; i < tokens.length - 1; i++)
        newParameter += tokens[i] + "%2B";
    newParameter += tokens[i];
    return newParameter;
}

//Global variables
//avoid the new keyword on js primitives
var submenufloatingDivStack = [];
var menuLevel = [];
var clickedMenuItemStack = [];
var scrollMenuStackObj = new Object();

var floatingDivstackSize = 0;
var menuLevelstackSize = 0;
var index = 0;
var displaySubmenu = 0;
var displayTopLevelMenuCallback = 0;
var clickEventObj;
var recRptsObj;
var isDragInProgress = false;
var currFavoriteId = null;
var isUserFolderDragged = false;
var prevClickedRowIndex;
var isManageRendered = false;
var dragObject = null;
var favItemDragClone = null;
var draggedRowId = null;
var isRICheckBoxClicked = false;
var clickedItem = null;
var prevClickedItem = null;
var clickedIndentLevel = 0;
var prevClickedIndentLevel = 0;
var curMainMenuItem = null;
var prevKeyValue = "";
var isActionMenuExpanded = false;
var actionsMenuParentItem = null;
var isBreadCrumbsMenuOpened = false;

function clearBreadCrumbs() {
    //Clearing the top most level highlight since the mouseout will not happen when the menu is closed
    if (curMainMenuItem) {
        curMainMenuItem.className = "MenuNormal";
    }
    clickedItem = null;
    prevClickedItem = null;
    clickedIndentLevel = 0;
    prevClickedIndentLevel = 0;
    curMainMenuItem = null;
    if (isActionMenuExpanded) {
        if (actionsMenuParentItem) {
            actionsMenuParentItem.className = "MenuNormal";
        }
        isActionMenuExpanded = false;
    }
    //Clear the array elems
    if (clickedMenuItemStack.length > 0) {
        clickedMenuItemStack.length = 0;
    }
}

function clearScrollMenuStackObj() {
    //iterate over the properties of Object and Delete props
    for (var prop in scrollMenuStackObj) {
        if (scrollMenuStackObj.hasOwnProperty(prop)) {
            delete scrollMenuStackObj[prop];
        }
    }
}

function menuItemHover(menuItem) {
    menuItem.className = "HoverMenuItem";
    if (prevClickedItem != null && prevClickedItem != undefined) {
        prevClickedItem.className = "HoverMenuItem";
    }
    if (clickedItem != null && clickedItem != undefined) {
        clickedItem.className = "HoverMenuItem";
    }
}

function menuItemUnhover(menuItem, indentLevel) {
    if ((clickedMenuItemStack.length > 0) && clickedMenuItemStack[indentLevel] == menuItem) //When the menu item is a part of breadcrumbs higlight
        menuItem.className = "HoverMenuItem";
    else
        menuItem.className = "MenuNormal";
}

function menuItemClick(menuItem, indentLevel, event) //When a menu item is clicked
{
    if (clickedItem) {
        prevClickedItem = clickedItem;
    }
    clickedItem = menuItem;
    prevClickedIndentLevel = clickedIndentLevel;
    clickedIndentLevel = indentLevel;
    if (indentLevel == 0) {
        curMainMenuItem = menuItem;
    }
    //When the same menuitem is clicked twice
    //When the toggle submenu happens the previous parent menu highlight should be cleared
    //When an already highlighted item is clicked again
    if (clickedMenuItemStack.length > 0) {
        if ((menuLevel.contains(indentLevel) && prevClickedItem != menuItem) || (prevClickedItem != null && prevClickedItem != menuItem && prevClickedIndentLevel == indentLevel && prevClickedItem.className != 'MenuNormal')) {
            if (clickedMenuItemStack[indentLevel] != null && clickedMenuItemStack[indentLevel] != undefined)
                clickedMenuItemStack[indentLevel].className = "MenuNormal";
            prevClickedItem = null;
            for (var i = indentLevel + 1, len = clickedMenuItemStack.length; i < len; i++) {
                clickedMenuItemStack.splice(i, 1);
            }
        }
    }
    clickedMenuItemStack[indentLevel] = menuItem;
    if (isActionMenuExpanded) {
        if (actionsMenuParentItem != null && actionsMenuParentItem != undefined) {
            actionsMenuParentItem.className = "MenuNormal";
        }
    }
    isActionMenuExpanded = false;
    checkFullClickableArea(menuItem, event);
}

// This function allows the full menuItem area to be clickable.
// Clicking anywhere in the item area will activate the link.
// ONLY do this if the event.target is not the actual anchor element.
function checkFullClickableArea(menuItem, event) {
    if (isCtrlDown(event)) return; // pass through if ctrl key is down
    var srcElem = getEventSourceElement(event);
    var nodeID = 'fldnode' + menuItem.id.substring(8);  //remove 'menuItem' from start to get node number
    if (srcElem.nodeName != 'A' && srcElem.id != nodeID) {
        var elem = document.getElementById(nodeID);
        if (!elem) // elem was not found - this should be OCL menu
        {
            // this is for OCL - currentTarget should give us the correct element
            if (event.currentTarget && event.currentTarget.id.slice(0, 6) == "OCLTR_") {
                nodeID = 'OCLHREF_' + event.currentTarget.id.substring(6);
                elem = document.getElementById(nodeID);
            }
        }
        if (elem) {
            if (elem.nodeName == 'A') {
                elem.click();
                stopEventBubbling(event);
            }
        }
    }
}

function menuItemKeyDown(event, menuItem, indentLevel) //When a menu item is accessed using Keyboard
{
    if (event.type == "keydown") {
        if ((!isRTL && event.keyCode == 39) || (isRTL && event.keyCode == 37)) // Check if its Right Arrrow Key
        {
            menuItemClick(menuItem, indentLevel, event);
        }
        else if ((!isRTL && event.keyCode == 37) || (isRTL && event.keyCode == 39))  // Check if its left Arrow Key
        {
            hideLastSubMenu(menuItem, indentLevel);
            if (clickedMenuItemStack != null && clickedMenuItemStack != undefined && indentLevel != 0) {
                clickedMenuItemStack[indentLevel - 1].focus();
                clickedMenuItemStack[indentLevel - 1] = null;
                clickedItem = null;
                prevClickedItem = null;
            }
        }
    }
}

function actionsMenuHover(actionsMenuItem) {
    if (curMainMenuItem != null && curMainMenuItem != undefined) {
        curMainMenuItem.className = "HoverMenuItem";
    }
    actionsMenuItem.className = "HoverMenuItem";
}

function actionsMenuUnhover(actionsMenuItem) {
    if (!isActionMenuExpanded) {
        actionsMenuItem.className = "MenuNormal";
    }
}
function taskNodeActionsClick(parentItem, event) {
    clearBreadCrumbs();
    if (event.type == "click" || ((event.type == "keydown") && ((!isRTL && event.keyCode == 39) || (isRTL && event.keyCode == 37)))) {
        // need to manually add actions to the menu stack
        clickedMenuItemStack[0] = parentItem;
    }
    showActionMenu(parentItem, event, 'dropdown_actions');
}

function showActionMenu(parentItem, event, popupContentDivId) {
    isActionMenuExpanded = true;
    showSubMenu(event, popupContentDivId, 0, popupContentDivId);
}

function showSubMenu(event, popupContentDivId, indentLevel, eventElementId) {
    var event = event ? event : window.event;
    var srcElement = getEventSourceElement(event);
    var doc = document;

    if (doc.getElementById(popupContentDivId) == null) {
        return;
    }
    if (event.ctrlKey || ((event.type == "keydown") && ((!isRTL && event.keyCode != 39) || (isRTL && event.keyCode != 37)))) {
        return;
    }
    if (menuLevel.length > 0 && menuLevel.contains(indentLevel)) {
        togglesubMenu(indentLevel);
    }

    clearTimeout(displaySubmenu);
    mymouseClickstop = function () {
        showSubMenu(clickEventObj, popupContentDivId, indentLevel, eventElementId);
    };
    var cacheContainer = doc.getElementById(popupContentDivId);

    // search the cachedInnerHTML in cacheContainer to see if innerHTML was already been cached as attribute before, otherwise it is the first time to access it.
    var strInnerHTML = cacheContainer.getAttribute("cachedInnerHTML");
    if (!strInnerHTML)
        strInnerHTML = cacheContainer.innerHTML;
    if (strInnerHTML == null || strInnerHTML == "" || strInnerHTML == "<div></div>") {
        //submenu Div content is not loaded
        if (event.pageX === undefined) {
            // IE8 loses the Event in setTimeout,  copy the properties we want to use of it and store it in a new object
            clickEventObj = new Object();
            clickEventObj.srcElement = event.srcElement;
            clickEventObj.type = event.type;
            clickEventObj.keyCode = event.keyCode;
            clickEventObj.pageX = event.pageX;
            clickEventObj.clientX = event.clientX;
            clickEventObj.clientY = event.clientY;

            displaySubmenu = setTimeout(mymouseClickstop, 100);
            return;
        }
        else {
            displaySubmenu = setTimeout(function () { showSubMenu(event, popupContentDivId, indentLevel, eventElementId); }, 50);
            return;
        }
    }

    // we have cache the innerHTML as attribute of the cacheContainer and remove the innnerHTML of cacheContainerfrom DOM, 
    // so that we can avoid the duplicated DOM ID between the origianl template and new copy (hard requirement for accessibility)
    cacheContainer.setAttribute("cachedInnerHTML", strInnerHTML);
    // need to calculate the popupHeight before we clear the innerHTML (see bug 19866834)
    var popupHeight = cacheContainer.getAttribute("popupHeight");
    if (!popupHeight) {
        popupHeight = (cacheContainer.lastChild.children.length) * menuItemHeight;
        cacheContainer.setAttribute("popupHeight", popupHeight);
    }
    cacheContainer.innerHTML = "";

    var floatingDiv = getFloatingDiv("submenufloatingDiv");
    doc.getElementById(popupContentDivId).className = "showMenu";
    floatingDiv.setAttribute('indentLevel', indentLevel);
    addmenuLevelStack(indentLevel);
    floatingDiv.style.display = "block";

    //get X-Y pos of parent table
    var parentTable = getHoverMenuItem(event);
    var mousepos = getCursorPositioninPopup(parentTable);
    //When a parent mennu is closed before the submenu can expand
    if ((mousepos[0] == null && mousepos[1] == null) || (mousepos[0] == 0 && mousepos[1] == 0)) {
        clearBreadCrumbs();
        return;
    }

    // if the submenu popup is out of window height render scrollable menu
    var visibleWindowHeight = UTIL.pageHeight();
    if (popupHeight > visibleWindowHeight) {
        //set the height of Floating Div
        floatingDiv.style.height = (visibleWindowHeight - 30) + "px";
        floatingDiv.style.overflowY = "scroll";
        addonClickEvent(floatingDiv, 'scroll', menuScrollEventHandler);
    }
    else
        if ((mousepos[1] + popupHeight) > visibleWindowHeight) ///if the submenu is outside window width, need to show in top
        {
            var extraMenuHeight = visibleWindowHeight - (mousepos[1] + popupHeight);
            floatingDiv.style.top = mousepos[1] + extraMenuHeight + "px";
        }

    //IE has issues with loading the InnerHTML in the <TD>, using the DOM API to construct table
    //setting InnerHTML is faster than DOM API
    if (isIE8OrEarlier) {
        // create elements from the top down; then you attach the children to the parents from the bottom up.
        var innerTable = doc.createElement('table');
        var innerTbody = doc.createElement('tbody');
        var innerRow = doc.createElement('tr');
        var innerCell = doc.createElement('td');
        //set attributes to table
        //minimize reflows in DOM
        innerTable.setAttribute("id", "flyoutInnerTable" + popupContentDivId);
        innerTable.setAttribute("border", "0");
        innerTable.setAttribute("cellpadding", "0");
        innerTable.setAttribute("cellspacing", "0");
        innerTable.setAttribute("nowrap", "nowrap");

        innerRow.appendChild(innerCell);
        innerTbody.appendChild(innerRow);
        innerTable.appendChild(innerTbody);
        floatingDiv.appendChild(innerTable);

        innerCell.innerHTML = strInnerHTML;
    }
    else {
        var sb = new PSStringBuffer();
        sb.append("<table id=\"flyoutInnerTable").append(popupContentDivId).append("\"cellpadding=0 cellspacing=0><tbody><tr><td nowrap=\"nowrap\">");
        sb.append(strInnerHTML);
        sb.append("</td></tr></tbody></table>");
        floatingDiv.innerHTML = sb.toString();
    }

    //parentTable.parentNode.appendChild(floatingDiv);

    if (eventElementId == '980submenu') {
        floatingDiv.style.top = mousepos[1] + 10 + "px";
        floatingDiv.style.left = mousepos[0] - 15 + "px";
    }
    else {
        setDisplayPosToSubMenu(floatingDiv, mousepos, event, parentTable);
    }

    //get the Table in the submenu and set the Focus on the first menuItem
    var firstTable = doc.getElementById("flyoutInnerTable" + popupContentDivId).firstChild.firstChild.firstChild;
    if (firstTable != undefined) {
        var firstMenuTable;
        var menuTable;
        var children = firstTable.childNodes;
        var lastDiv = getLastDiv(children);
        // the actions submenu has an extra div...
        if (lastDiv && lastDiv.id == 'actionsSubMenu') {
            lastDiv = lastDiv.firstChild;
        }
        if (lastDiv != undefined) {
            if (lastDiv.firstChild != null)
                menuTable = lastDiv.firstChild.firstChild;

            if (menuTable.nodeName == "TABLE" && menuTable.id == "emptyChildNodeTable") // check is it empty
            {
                firstMenuTable = menuTable;
            }
            else {
                if (isIE8OrEarlier) {
                    firstMenuTable = menuTable.firstChild;
                }
                else {
                    var fChild = lastDiv.firstChild.firstChild.firstChild;
                    if (fChild != null && fChild != undefined)
                        firstMenuTable = fChild.nextSibling;
                }
            }
            if (firstMenuTable != undefined && firstMenuTable.focus)
                firstMenuTable.focus();
        }
    }
    // cancel event bubble
    stopEventBubbling(event);
}

/* return the left and top position of the given element */
function getCursorPositioninPopup(elem) {
    return [getAbsoluteLeftPos(elem), getAbsoluteTopPos(elem)];
}

/* return the menu item that is hovered over when the event occurred */
function getHoverMenuItem(evt) {
    if (evt) {
        var tableElement = getEventSourceElement(evt);
        var prevElement = tableElement;
        while ("HoverMenuItem" != tableElement.className) {
            prevElement = tableElement;
            tableElement = tableElement.parentNode;
            if (tableElement == null) //When a parent mennu is closed before the submenu can expand
                return null;
            if (tableElement.tagName == "DIV") //When you click an already clicked menu item make sure that the loop is broken when the element is a table
            {
                tableElement = prevElement;
                break;
            }
        }
        return tableElement;
    }
    return null;
}

/* This function iterate the DOM to get the First menuItem in submenu*/
function getFirstMenuItemInSubmenu(clickElementRowContainer) {
    var rowContainerElement = clickElementRowContainer;
    while (rowContainerElement.previousSibling) {
        rowContainerElement = rowContainerElement.previousSibling;
    }
    return rowContainerElement;
}
/* This function iterate the DOM to get the submenu floating DIV*/
function getSubmenuDivContainer(firstMenuItem) {
    var firstMenuItemElem = firstMenuItem.parentNode;
    while (firstMenuItemElem.parentNode) {
        firstMenuItemElem = firstMenuItemElem.parentNode;
        if (firstMenuItemElem.nodeName == "DIV") {
            return firstMenuItemElem;
        }
    }
}

/* return the last DIV element in an array of elements */
function getLastDiv(elems) {
    for (var i = elems.length - 1; i >= 0; i--) {
        if (elems[i].nodeName == "DIV") {
            return elems[i];
        }
    }
    return null;
}

/* Set the display position of the sub menu. */
/* NOTE: srcElement is expected to be the menu item table with HoverMenuItem class */
function setDisplayPosToSubMenu(floatingDiv, mousepos, evt, srcElement) {
    var windowHeight = UTIL.pageHeight();
    var windowWidth = UTIL.pageWidth();
    var newMenuLeft = mousepos[0] + srcElement.offsetWidth - 9;
    var newMenuTop = mousepos[1];
    var offsetParent = srcElement.offsetParent;
    if (offsetParent != null && offsetParent != undefined && offsetParent.id == DDMENU.CONST.ELEMENT_FAVORITES_DIV) {
        newMenuTop = newMenuTop - offsetParent.scrollTop;
    }
    if (floatingDiv.previousSibling.className == "showMenu" && floatingDiv.previousSibling.scrollTop != null) {
        newMenuTop = newMenuTop - floatingDiv.previousSibling.scrollTop;
    }
    var newMenuBottom = newMenuTop + floatingDiv.offsetHeight;
    //submenu outside of window height
    if (newMenuBottom > windowHeight) {
        newMenuTop = newMenuTop - (newMenuBottom - windowHeight);
        if (newMenuTop < 0) newMenuTop = 0;
    }
    floatingDiv.style.top = newMenuTop + "px";

    // avoid reflows in DOM, cache the offset values
    var preSiblingoffsetLeft = getAbsoluteLeftPos(floatingDiv.previousSibling);
    var preSiblingoffsetWidth = floatingDiv.previousSibling.offsetWidth;
    var fdivoffsetWidth = floatingDiv.offsetWidth;

    if (!isRTL) {
        if (preSiblingoffsetLeft != 0 && (preSiblingoffsetWidth != windowWidth) && (preSiblingoffsetLeft + preSiblingoffsetWidth + fdivoffsetWidth) > windowWidth) // if the submenu outof window width
        {
            floatingDiv.style.left = preSiblingoffsetLeft - fdivoffsetWidth + 3 + "px";
        }
        else {
            floatingDiv.style.left = newMenuLeft + "px";
        }
    }
    else  //RTL
    {
        if (preSiblingoffsetLeft != 0 && (fdivoffsetWidth > preSiblingoffsetLeft)) // if the submenu outof window width flip to right
        {
            floatingDiv.style.left = preSiblingoffsetLeft + preSiblingoffsetWidth + "px";
        }
        else {
            floatingDiv.style.left = mousepos[0] - fdivoffsetWidth + 9 + "px";
        }
    }
}

function isAnchorNode(srcElement) {
    // stop search when we find a accend who's NodeName  is 'A'
    while (srcElement.firstChild) {
        srcElement = srcElement.firstChild;
        if (srcElement.nodeName == "A")
            return true;
    }
    return false;
}

function getAnchorNode(srcElement) {
    // stop search when we find a accend who's NodeName  is 'A'
    while (srcElement.firstChild) {
        srcElement = srcElement.firstChild;
        if (srcElement.nodeName == "A")
            return srcElement;
    }
}

function displayTaskContextMenu(event, popupContentDivId, menuItemCount, eventElementId) {
    var event = event ? event : window.event;
    var srcElement = getEventSourceElement(event);
    var openInNewWindowText = "Open in New Window";

    //Open in New Window context menu is not available on manage favorites
    if (srcElement.innerHTML == "Manage Favorites") {
        return;
    }

    if (event.type == "contextmenu" || (event.type == "keydown") || (event.type == "touchstart")) {
        var floatingDiv = getFloatingDiv("flyOutfloatingDiv");
        var mousepos;

        //get mouse Cursor positions
        if (event.type == "keydown")
            mousepos = [getAbsoluteLeftPos(srcElement), getAbsoluteTopPos(srcElement)];
        else
            mousepos = getCursorPosition(event);

        if (event.type == "touchstart")
            mousepos = [event.touches[0].clientX, event.touches[0].clientY];

        if (prefText.label != null) {
            openInNewWindowText = prefText.label[0].openInNewWindowText;
        }

        // Check it is an anchor Task Menu Item
        // do not add the Open in New Window entry if we are a One View Report or an ADF Application task
        if (((srcElement.nodeName == 'A' || isAnchorNode(srcElement)) && !isWSRPContainer() && !isIOS && MENUUTIL.allowNewWindow())
            && ((srcElement.href != null) && ((srcElement.href).indexOf("teDoRunOvrTask") < 0) && ((srcElement.href).indexOf("teDoRunExternalAppTask") < 0))) {
            // Click happened other than Anchor Node, get the Anchor Node srcElement
            if (srcElement.href == undefined) {
                srcElement = getAnchorNode(srcElement);
            }
            var href = srcElement.href;
            var strhref = href.substring(href.indexOf("("));
            var sb = new PSStringBuffer();
            sb.append("<table class=MenuNormal width=100% cellspacing=0 cellpadding=0 onmouseout=\"this.className='MenuNormal';\" onmouseover=\"this.className='HoverMenuItem';\"><tbody>");
            sb.append("<tr onblur=\"this.className='MenuNormal';\" onfocus=\"this.className='HoverMenuItem';\"><td align=\"left\" class=\"ContextMenuAnchorItem\"");
            sb.append("onClick=OpenTaskItemInNewWindow('" + strhref + "'");
            sb.append(");>");
            sb.append(openInNewWindowText);
            sb.append("</td></tr></tbody></table>");

            // Add Open in New window Context MenuItem
            floatingDiv.innerHTML = "<table id=flyoutInnertab cellpadding=0 cellspacing=0><tr><td>" + sb.toString() + document.getElementById(popupContentDivId).innerHTML + "</td></tr></table>";
        }
        else {
            floatingDiv.innerHTML = "<table id=flyoutInnertab cellpadding=0 cellspacing=0><tr><td>" + document.getElementById(popupContentDivId).innerHTML + "</td></tr></table>";
        }

        floatingDiv.style.display = "block";

        var visibleWindowHeight = UTIL.pageHeight();
        var offsetHeightValue = floatingDiv.offsetHeight;

        if ((mousepos[1] + offsetHeightValue) > visibleWindowHeight) ///if the contextmenu is outside window width, need to show in top
        {
            var outsideMenuHeight = visibleWindowHeight - (mousepos[1] + offsetHeightValue);
            floatingDiv.style.top = mousepos[1] + outsideMenuHeight - 5 + "px";
        }
        else {
            floatingDiv.style.top = mousepos[1] + "px";
        }

        floatingDiv.style.left = mousepos[0] + "px";

        // cancel event bubble
        stopEventBubbling(event);

        //prevent to display the browser default Context Menu
        return false;

    }
}


/*
This function display the TaskItem in New Window
*/
function OpenTaskItemInNewWindow(href) {

    var hrefstr = "javascript:RunNewApp" + href;
    var menuObject = jdeMenuParent;
    menuObject.oneTimeNewWindowLaunch = false;
    if (!MENUUTIL.allowNewWindow()) {
        return;
    }

    menuObject.oneTimeNewWindowLaunch = true;
    eval(hrefstr);
}

/*
This function display the Open App in New Window
*/
function OpenAppInNewWindow(id) {
    var menuObject = jdeMenuParent;
    menuObject.oneTimeNewWindowLaunch = false;
    if (!MENUUTIL.allowNewWindow()) {
        return;
    }

    menuObject.oneTimeNewWindowLaunch = true;

    var sb = new PSStringBuffer();
    sb.append("javascript:RunOldApp");
    sb.append("('");
    sb.append(id);
    sb.append("');");

    eval(sb.toString());
}

/*
This function display Open Apps ContextMenu
*/
function displayOpenAppsContextMenu(id, event) {
    var event = event ? event : window.event;

    if (event.type == "contextmenu" && !isWSRPContainer() && !isIOS) {
        if (MENUUTIL.allowOpenInNewWindow(id)) {
            var floatingDiv = getFloatingDiv("flyOutfloatingDiv");

            //get Cursor positions
            var mousepos = getCursorPosition(event);

            var sb = new PSStringBuffer();
            sb.append("<TABLE class=MenuNormal id=flyoutInnerTable border=0 cellSpacing=0 cellPadding=0 onmouseover=\"this.className='HoverMenuItem';\" onmouseout=\"this.className='MenuNormal';\" ");
            sb.append("onClick=OpenAppInNewWindow(" + id + "");
            sb.append(");");
            sb.append(">\r\n<TBODY>\r\n<TR>\r\n<TD>Open in New Window</TD></TR></TBODY></TABLE>");
            floatingDiv.innerHTML = sb.toString();
            floatingDiv.style.display = "block";
            //set display postions
            floatingDiv.style.top = mousepos[1] + "px";
            floatingDiv.style.left = mousepos[0] + "px";
        }
        // We'll unconditionally prevent the default context menu from appearing--new windows permitted or not.
        stopEventBubbling(event);
        return false;
    }
}

function dismissFloatingDiv() {
    var floatingDiv = document.getElementById("flyOutfloatingDiv");
    try {
        if (floatingDiv && floatingDiv.style.display == "block") {
            floatingDiv.style.className = "hideMenu";
            floatingDiv.parentNode.removeChild(floatingDiv);          //remove from the DOM
            //remove onClick event handler
            removeonClickEvent(document, 'click', dismissFloatingDiv);
        }
    }
    catch (problem) { }
}

function getFavMenuId() {
    return document.getElementById('favMenuId').title;
}

//toggle submenu
function togglesubMenu(indentLevel) {
    for (j = floatingDivstackSize - 1; j >= 0; j--) {
        var floatDiv = document.getElementById(submenufloatingDivStack[j]);
        if (floatDiv != null && floatDiv.getAttribute('indentLevel') >= indentLevel) {
            floatDiv.className = "hideMenu";
            removeChildren(floatDiv);
            floatDiv.parentNode.removeChild(floatDiv);
            submenufloatingDivStack.splice(j, 1);
            menuLevel.splice(menuLevel.indexOf(indentLevel), 1);
            floatingDivstackSize--;
            menuLevelstackSize--;
        }
    }
}

function getFloatingDiv(newDivName) {

    var newDiv = document.createElement("DIV");
    index = index + 1;
    var zIndex = 1000 + index;

    //remove any existing contextMenu
    dismissFloatingDiv();

    if (newDivName == "submenufloatingDiv")  // sub menus get added to the stack
    {
        newDiv.id = newDivName + zIndex;
        addsubmenufloatingDivStack(newDiv.id);
    }
    else  // context menus get an event handler to dismiss them
    {
        newDiv.id = newDivName;
        addonClickEvent(document, 'click', dismissFloatingDiv);
    }

    newDiv.className = "showMenu";

    newDiv.style.zIndex = zIndex;

    document.body.appendChild(newDiv);
    return newDiv;
}

function addsubmenufloatingDivStack(floatingDivId) {
    //update submenufloatingDivStack Array and increment the floatingDivstackSize
    submenufloatingDivStack[floatingDivstackSize] = floatingDivId;
    while ((submenufloatingDivStack[floatingDivstackSize] != null) && (submenufloatingDivStack[floatingDivstackSize] != "") && (floatingDivstackSize < submenufloatingDivStack.length)) {
        floatingDivstackSize++;
    }
}

function addmenuLevelStack(indentLevel) {
    //update menuLevel Array and increment the menuLevelstackSize
    menuLevel[menuLevelstackSize] = indentLevel;
    if (menuLevelstackSize < menuLevel.length) {
        menuLevelstackSize++;
    }
}

function doActivateNodeWebGui(nodeId, parentNodeId, indentLevel, activeNodeRememberedInCookie) {
    var currentActiveTreeItem = jdeMenuParent.elementActiveTreeItem;
    if (currentActiveTreeItem && currentActiveTreeItem != "") {
        try {
            document.getElementById(currentActiveTreeItem).className = "";
        }
        catch (problem) { }
    }
    currentActiveTreeItem = "head" + nodeId;
    jdeMenuParent.elementActiveTreeItem = currentActiveTreeItem;
}
function doFavActivateNodeWebGui(indentLevel) {
    var favId = getFavMenuId();
    doActivateNodeWebGui(favId, '', indentLevel);
}


function doTreeIframeWebGui(nodeId, indentLevel, nodeIconId, toggleMode, roleId) {
    if (nodeIconId = ".") {
        nodeIconId = "I" + nodeId;
    }
    var eventObj =
    {
        nodeId: nodeId,
        indentLevel: indentLevel,
        nodeIconId: nodeIconId,
        toggleMode: toggleMode,
        roleId: roleId
    };
    MenuEventQueue.addEvent(eventObj);
}

function doFavTreeIframeWebGui(indentLevel, nodeIconId, toggleMode, roleId) {
    var favNodeId = '98_.._' + getFavMenuId();
    doTreeIframeWebGui(favNodeId, indentLevel, nodeIconId, toggleMode, roleId);
}

var MenuEventQueue = new function () {
    this.SECONDS_TO_WAIT = 30;
    this._events = new Array();
    this._waiting = false;
    this._eventStartTime = -1;
    this.addEvent = function (eventObj) {
        this._events.push(eventObj);
        this.execute();
    };
    this.execute = function () {
        if (this._waiting == false) {
            this._executeFirstEvent();
        }
        else {
            currentTime = (new Date()).getTime();
            var timeDelta = currentTime - this._eventStartTime;
            var continueWaiting = true;
            if ((this._events.length > 0) &&
                (timeDelta >= 1000 * this.SECONDS_TO_WAIT)) {
                continueWaiting = confirm("The server is taking an excessive time to process your request.  Click \"OK\" to continue waiting.");
                if (continueWaiting) {
                    this._eventStartTime = (new Date()).getTime();
                }
            }
            if (continueWaiting) {
                setTimeout("MenuEventQueue.execute()", 1000);
            }
            else {
                this._waiting = false;
                this._executeFirstEvent();
            }
        }
    };
    this.eventComplete = function () {
        this._waiting = false;
        // The last thing to do is to attach any children to their parent.
        jdeMenuParent.moveAllChildren();
        setTimeout("MenuEventQueue.execute()", 1);
    }
    this._executeFirstEvent = function () {
        if (this._events.length == 0) {
            this._waiting = false;
            return;
        }
        var eventObj = this._events.shift();
        this._waiting = true;
        this._eventStartTime = (new Date()).getTime();
        try {
            var nodeIdArray = eventObj.nodeId.split("_");
            var shortId = eventObj.nodeId;
            var doc = document;
            if (nodeIdArray.length > 2) {
                shortId = nodeIdArray[2];
            }
            doActivateNodeWebGui(shortId, shortId/*parent*/, eventObj.indentLevel);
            var childDivId = "treechild" + shortId;
            eventObj.nodeIconId = "I" + shortId;
            var childDiv = doc.getElementById(childDivId);
            var expanded = (childDiv.style.display == "block");

            /* IE has issues loading the large InnerHTML, so fetch from the server again*/
            if (isIE && isWSRPContainer()) {
                childDiv.innerHTML = "";
                expanded = false;
            }

            if (expanded && (eventObj.toggleMode == 'expand')) {
                this._waiting = false;
                return;
            }
            else if (expanded) {
                var theToggleIcon = doc.getElementById(eventObj.nodeIconId);
                var sb = window["E1RES_share_images_open_submenu_ena_png"];
                if (jdeMenuParent.isRtlEnabled) {
                    sb = window["E1RES_share_images_open_submenu_ena_rtl_png"]
                }
                theToggleIcon.src = sb;
                childDiv.style.display = "none";
                jdeMenuParent.storeMenuState(shortId, !expanded);
                this._waiting = false;
                return;
            }
            else if (eventObj.toggleMode == 'collapse') {
                this._waiting = false;
                return;
            }
            else {
                var theToggleIcon = doc.getElementById(eventObj.nodeIconId);
                childDiv.style.display = "block";
                jdeMenuParent.storeMenuState(shortId, !expanded);
                if (childDiv.innerHTML == "" || childDiv.innerHTML.length < 1) {
                } else {
                    this._waiting = false;
                    return;
                }
            }
            var taskLabel = document.getElementById(jdeMenuParent.elementActiveTreeItem).innerText;
            if (taskLabel == undefined) {
                taskLabel = "";
            }
            doc.getElementById("E1MTreeFormDiv").value = childDivId;
            doc.getElementById("E1MTreeFormNodeId").value = eventObj.nodeId;
            //nodeTitle is used by RUEI
            doc.getElementById("E1MTreeFormNodeTitle").value = taskLabel.trim();
            doc.getElementById("E1MTreeFormIndent").value = eventObj.indentLevel;
            doc.getElementById("E1MTreeFormRoleId").value = eventObj.roleId;
            doc.getElementById("E1MTreeFormCollapsed").value = expanded;
            var treeForm = doc.getElementById("E1MTreeForm");
            // WebCenter form.submit() takes the window.event target as the submitted element,
            // or assumes the clicked target is within the form.  This isn't the case here
            // so we have to bypass that logic and call into adfppSub() directly.
            if (isWebCenter) {
                window.adfppSub(window.event, treeForm, "", false);
            }
            else {
                treeForm.submit();
            }
        }
        catch (problem) {
            this._waiting = false;
            this.execute();
        }
    };
}

function doLoadActiveNodeWebGUI() {
    try {
        var nodeId = jdeWebGUIgetSubCookie("webguiinteractivetree", "ActiveNodeId");
        var parentNodeId = jdeWebGUIgetSubCookie("webguiinteractivetree", "ActiveParent");
        var indentLevel = jdeWebGUIgetSubCookie("webguiinteractivetree", "ActiveIndent");
        var indentLevelInt = 0;
        if (indentLevel != "") {
            indentLevelInt = parseInt(indentLevel);
        }
        if (nodeId != "") {
            doActivateNodeWebGui(nodeId, parentNodeId, indentLevelInt);
        }
    }
    catch (problem) { }
}

function menuListener(event) {
    if (event == "1") {
        doLoadInitialNodeWebGUI();
    }
}

var OpenAppShow;
var CheckAppStatus;

function OpenAppIndicator() {
}

OpenAppIndicator.createInstance = function () {
    OpenAppIndicator.theInstance = new OpenAppIndicator();
    OpenAppIndicator.theInstance.isOpenIndDisplayed = false;
    OpenAppIndicator.theInstance.OpenIndicatorElem = null;
    OpenAppIndicator.theInstance.moveOAIndicatorTimer = null;
    OpenAppIndicator.theInstance.topOffset = getAbsoluteTopPos(document.getElementById("menutitle")) + 231;
    OpenAppIndicator.theInstance.rightOffset = 0;
    return OpenAppIndicator.theInstance;
}

OpenAppIndicator.getInstance = function () {
    return OpenAppIndicator.theInstance;
}

OpenAppIndicator.positionOpenAppIndicator = function () {
}
OpenAppIndicator.stopOpenAppIndicator = function () {
    if (OpenAppIndicator.theInstance != null) {
        OpenAppIndicator.theInstance.display(false);

        var OpenIndicatorDiv = document.getElementById("OpenIndicatorFloatLyr");
        //remove all Children 
        removeChildren(OpenIndicatorDiv);

        //Remove the OpenIndicatorFloatLyr div from the parent outerRCUX
        if (OpenIndicatorDiv.parentNode) {
            OpenIndicatorDiv.parentNode.removeChild(OpenIndicatorDiv);
        }
        // the Opening application msg never Closes in webcenter wsrp,close the processing indicator if exists 
        if ((gContainerId == E1URLFactory.prototype.CON_ID_WSRP) && ProcessingIndicator.getInstance() != null) {
            ProcessingIndicator.stopProcessingIndicator();
        }
        //delete OpenAppIndicator.theInstance;
        OpenAppIndicator.theInstance = null;
        ariaLog(jdeWebGUIStaticStrings.JH_PAGE_COMPLETE);
    }

    try {
        if (OpenAppShow != null) {
            window.clearTimeout(OpenAppShow);
            OpenAppShow = null;
        }
        if (CheckAppStatus != null) {
            window.clearTimeout(CheckAppStatus);
            CheckAppStatus = null;
        }

    }
    catch (e) { }
}

OpenAppIndicator.showOpenAppIndicator = function () {
    ariaLog(jdeWebGUIStaticStrings.TE_PROC_INDICATOR_TEXT);

    if (OpenAppIndicator.theInstance != null) {
        OpenAppIndicator.theInstance.display(true);
        if (ProcessingIndicator.getInstance() != null) {
            ProcessingIndicator.stopProcessingIndicator();
        }
    }

    if (OpenAppShow != null) {
        window.clearTimeout(OpenAppShow);
        OpenAppShow = null;
    }

    CheckAppStatus = window.setTimeout("pollServerForOpeningApp()", 60000);
    updateOCL();
}

// Create the processing indicator element
OpenAppIndicator.prototype.createOAElem = function () {
    var doc = document;
    var elem = doc.createElement("div");
    if (doc.documentElement.dir == "rtl")
        leftPos = this.rightOffset;

    elem.id = "OpenIndicatorFloatLyr";
    elem.noWrap = "true";
    elem.style.display = "none";

    var myElement = doc.createElement('TABLE');
    myElement.cellPadding = 0;
    myElement.cellSpacing = 0;
    myElement.style.height = "100%";
    myElement.style.width = "100%";
    myElement = this._addElementAndReparent(myElement, elem);

    myElement = this._addQuickElement('TBODY', myElement);
    myElement = this._addQuickElement('TR', myElement);

    var myTd = doc.createElement('TD');
    myTd.style.textAlign = "center";
    myElement = this._addElementAndReparent(myTd, myElement);

    var myImage = doc.createElement("IMG");
    myImage.src = window["WEBGUIRES_images_loading_52x52_gif"];
    this._addElementAndReparent(myImage, myElement);

    var mySpan = doc.createElement("SPAN");
    mySpan.id = "loadingMessageText";
    mySpan.innerHTML = jdeMenuParent.loadingMsg;
    mySpan.setAttribute("class", "loadingMessageText");
    this._addElementAndReparent(mySpan, myElement);

    var rcuxFrame = doc.getElementById("outerRCUX");
    if (rcuxFrame != null)
        this._addElementAndReparent(elem, rcuxFrame);
}

function getsupportedprop(proparray) {
    var root = document.documentElement //reference root element of document
    for (var i = 0; i < proparray.length; i++) { //loop through possible properties
        if (typeof root.style[proparray[i]] == "string") { //if the property value is a string (versus undefined)
            return proparray[i] //return that string
        }
    }
}

OpenAppIndicator.prototype._addQuickElement = function (elementType, parent) {
    return this._addElementAndReparent(document.createElement(elementType), parent);
}

OpenAppIndicator.prototype._addElementAndReparent = function (child, parent) {
    parent.appendChild(child);
    return child;
}

OpenAppIndicator.prototype.display = function (show) {
    this.OpenIndicatorElem = document.getElementById("OpenIndicatorFloatLyr");
    if (this.OpenIndicatorElem == null) {
        this.createOAElem();
        this.OpenIndicatorElem = document.getElementById("OpenIndicatorFloatLyr");
    }
    if (this.OpenIndicatorElem != null) {

        if (show && !this.isOpenIndDisplayed) {
            this.OpenIndicatorElem.style.display = "block";
            this.isOpenIndDisplayed = true;
        }
        else {
            this.OpenIndicatorElem.style.display = "none";
            if (this.isOpenIndDisplayed) { // clear our timer
                window.clearInterval(this.moveOAIndicatorTimer);
            }
            this.isOpenIndDisplayed = false;
        }
    }
}

var controlledBrowserClose = false;
var dtaDebug = false;
var debugWindow = null;

/**
* The MENUUTIL singleton owns the following:
*  - Cycling through OCL List (F8 keypress)
*  - Controls the resizing of the menu
*/
var MENUUTIL;
if (MENUUTIL == null) {
    MENUUTIL = new function () {
        // Menu resizing
        this.MENU_MIN_SIZE = 195;
        this.MENU_SIZE_MODIFIER = 6;
        this.menuStartLeft = 0;
        this.menuWidth = 0;

        // OCL cycle
        this.OCL_HREFTAG = "OCLHREF_";
        this.OCL_NOOPENAPP = "OCLHREF";
        this.OCLTR = "OCLTR_";
        this.currentOCLIndex = -1;
        this.previousFocusedElement = null;
    }
}

MENUUTIL.allowNewWindow = function () {
    var menuObject = jdeMenuParent;
    return menuObject.isMultipleBrowserEnabled;
}

// This function exists to "append" logic onto the MENUUTIL.allowNewWindow() rerturned value where we consider that
// value but also restrict it's new window usage to non-external applications only when necessary.
MENUUTIL.allowOpenInNewWindow = function (id) {
    var allow = MENUUTIL.allowNewWindow();

    // There currently is only one option on the context menu:  Launch in a new window.  Restrict this option when external apps are involved.
    if (ExternalAppsHandler.isAnExternalAppInstance(id)) {
        allow = false;
    }

    return allow;
}

MENUUTIL.showNewWindowButton = function () {
    // Ensure that the menu is fully rendered in the check also. The menu is not fully rendered
    // in child windows that were not opened with the new window button (or key).

    var menuObject = jdeMenuParent;
    return (menuObject.isNewWindowButtonEnabled && !menuObject.isMenuMaximized && document.getElementById("webTree"))
}

MENUUTIL.saveFocusedElement = function (focusedElement) {
    if (focusedElement) {
        this.previousFocusedElement = focusedElement;
    }
}

MENUUTIL.processOpenApplicationList = function (direction, allowLooping) {
    var oclContainer = document.getElementById("oclContainer");
    // if no container then there are no OCL entries ("You have no running applications.")
    if (oclContainer == null && jdeMenuParent.isAccessibilityEnabled) {
        try {
            document.getElementById(this.OCL_NOOPENAPP).focus();
        }
        catch (error) {
        }
    }

    if (oclContainer) {
        var numberOclEntries = oclContainer.childNodes.length;

        // clear up the previous focus
        if (this.currentOCLIndex >= 0) {
            var oclComponentId = oclContainer.childNodes[this.currentOCLIndex].id.substring(6);
            document.getElementById(this.OCLTR + oclComponentId).className = "MenuNormal";
        }

        // find and hightlight new focus
        switch (direction) {
            case 1:
                switch (this.currentOCLIndex) {
                    case -1:
                        for (var i = 0; i < numberOclEntries; i++) {
                            if (oclContainer.childNodes[i].id.substring(6) == jdeMenuParent.currCompId) {
                                this.currentOCLIndex = i;
                                break;
                            }
                        }
                        break;
                    default:
                        if (allowLooping && this.currentOCLIndex + 1 >= numberOclEntries)
                            this.currentOCLIndex = 0; // loop back to begin
                        else
                            this.currentOCLIndex = Math.min(Math.max(0, this.currentOCLIndex + 1), numberOclEntries - 1);
                }
                break;

            case -1:
                switch (this.currentOCLIndex) {
                    case -1:
                        this.currentOCLIndex = Math.max(0, numberOclEntries - 1);
                        break;
                    default:
                        if (allowLooping && this.currentOCLIndex - 1 < 0)
                            this.currentOCLIndex = numberOclEntries - 1; // loop back to end
                        else
                            this.currentOCLIndex = Math.max(0, this.currentOCLIndex - 1);
                }
                break;

            default:
                this.currentOCLIndex = 0;
                break;
        }
        // use the first one if there is no active application (Home E1 Page)
        if (this.currentOCLIndex == -1)
            this.currentOCLIndex = 0;

        var oclComponentId = oclContainer.childNodes[this.currentOCLIndex].id.substring(6);
        var doc = document;
        try {
            doc.getElementById(this.OCLTR + oclComponentId).focus();
        }
        catch (error) { }
        doc.getElementById(this.OCLTR + oclComponentId).className = "HoverMenuItem";
        return oclComponentId;
    }
}

MENUUTIL.updateOCLIndex = function (index) {
    this.currentOCLIndex = index;
}

MENUUTIL.restoreFocus = function () {
    this.previousFocusedElement = (this.previousFocusedElement == null) ? document.getElementById("e1menuAppIframe") : this.previousFocusedElement;
    try {
        this.previousFocusedElement.focus();
    }
    catch (permissionDenied) {
        this.previousFocusedElement = null;
        document.getElementById("e1menuAppIframe").focus();
    }
}
MENUUTIL.getPageX = function (e) {
    if (!e || e.pageX === undefined) {
        // IE8
        return this.getPageXIE(e);
    }

    return e.pageX;
}

MENUUTIL.getPageXIE = function (e) {
    e = window.event;
    var elm = e.srcElement
    var x = e.offsetX;
    while (elm.offsetParent != null) {
        x += elm.offsetLeft;
        elm = elm.offsetParent;
    }
    x += elm.offsetLeft;
    return x;
}

// This function takes touch event from the child (form) and processes their meaning for the menu
MENUUTIL.processChildTouchEvents = function (e, gesture) {
    switch (gesture) {
        case TouchLib.LEFT:
            // if currently open then close

            if (CARO && (CARO.carousel.side == CARO.WEST) && CARO.carousel.expanded) {
                CARO.caroToggleBar();
            }
            else if (CARO && (CARO.carousel.side == CARO.EAST) && !CARO.carousel.expanded) {
                CARO.caroToggleBar();
            }
            break;
        // TODO: Qiu - need to support another dirrection of navigation using this.processOpenApplicationList(-1); 
        case TouchLib.RIGHT:

            if (CARO && (CARO.carousel.side == CARO.WEST) && !CARO.carousel.expanded) {
                CARO.caroToggleBar();
            }
            else if (CARO && (CARO.carousel.side == CARO.EAST) && CARO.carousel.expanded) {
                CARO.caroToggleBar();
            }
            else {
                // Select the next application
                var oclComponentId = this.processOpenApplicationList(1, false);
                // it is the current visible application so need to find the next one
                if (oclComponentId == self.jdeMenuParent.currCompId) {
                    oclComponentId = this.processOpenApplicationList(1, false);
                }
                // if now it is different then change app
                if (oclComponentId != self.jdeMenuParent.currCompId) {
                    RunOldApp(oclComponentId);
                }
            }
            break;
    }
}

MENUUTIL.performWindowLocation = function (url) {
    if (isNativeContainer) {
        NativeContainer.navigateToURL(url);
    }
    else {
        window.location = url;
    }
}

function teOnLoadOrResize() {
    var menuObject = jdeMenuParent;
    var doc = document;
    var e1menuAppIframe = doc.getElementById("e1menuAppIframe");
    var isWPS60 = (window.top.javascriptEventController != null);

    if (!isWSRPContainer()) {
        return;
    }

    if (!isIE) {
        // multiple scrollbar prevention for Mozilla/Safari
        e1menuAppIframe.style.height = (e1menuAppIframe.offsetHeight - 25) + "px";
    }
    var topBarHeight = 0;
    try {
        var standAloneTopBar = doc.getElementById("topnav");
        topBarHeight = standAloneTopBar.offsetHeight;
    }
    catch (notStandAlone) {
        try {
            var wpsTopBar = document.getElementById("jdeowpTopBarDiv");
            var portletBarHeight = 35;
            topBarHeight = wpsTopBar.offsetHeight + portletBarHeight;
        }
        catch (notBrandedWPS) {
            topBarHeight = 50;
        }
    }
    var menuTitleDiv = doc.getElementById("menutitle");
    var browserHeight = 300;
    var browserWidth = 222;
    if (window.innerHeight !== undefined) {
        browserHeight = window.innerHeight;
        browserWidth = window.innerWidth - 2;
    }
    // IE8
    else {
        if (isWSRPContainer()) { // cover OPS 10g and 11g
            browserHeight = Math.max(document.documentElement.clientHeight, document.body.clientHeight);
        }
        else // default standalone or others
        {
            browserHeight = document.body.clientHeight;
            browserWidth = document.body.clientWidth;
        }
    }

    var newAppFrameHeight = browserHeight - topBarHeight - menuTitleDiv.offsetHeight;
    if (isWPS60)
        newAppFrameHeight = newAppFrameHeight - getAbsoluteTopPos(doc.getElementById(DDMENU.CONST.ELEMENT_DROP_MAINMENU));

    if (newAppFrameHeight >= 0) {
        // Oracle Portal - the above variable can come in negative on a refresh
        if (!isWPS60)
            e1menuAppIframe.style.height = newAppFrameHeight + "px";
    }
}

function showGlobalMaskFrame() {
    //check is the maskFrame exists in DOM
    var doc = document;
    var defaultMaskIframe = doc.getElementById(DDMENU.CONST.ELEMENT_DEFAULTGLOBALMASKFRAME);
    var e1menuAppIframe = doc.getElementById("e1menuAppIframe");

    if (defaultMaskIframe != null && e1menuAppIframe != null) {
        //show the defaultMaskIframe
        defaultMaskIframe.className = 'showGlobalMaskFrame';

        //resize the defaultMaskIframe to cover the entire e1menuAppIframe
        defaultMaskIframe.style.width = e1menuAppIframe.offsetWidth + "px";
        defaultMaskIframe.style.height = e1menuAppIframe.offsetHeight + "px";

        var innerDoc = defaultMaskIframe.contentDocument || defaultMaskIframe.contentWindow.document;
        if (innerDoc != null) {
            //attach onClick event handler
            addonClickEvent(innerDoc, 'click', defaultMaskFrameEventHandler);
        }
    }
}

function hideGlobalMaskFrame() {
    // Since local variables are first in the scope chain, they�re always faster than globals.
    // So anytime we use a global variable more than once we should redefine it locally.
    var doc = document;
    var navMenu = doc.getElementById(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV);
    var favMenu = doc.getElementById(DDMENU.CONST.ELEMENT_FAVORITES_DIV);
    var oclMenu = doc.getElementById(DDMENU.CONST.ELEMENT_FAVORITES_DIV);
    var rptMenu = doc.getElementById(DDMENU.CONST.ELEMENT_RECRPTSDIV);
    // hide the globalmaskframe only when these top level menus are not open
    if ((navMenu && navMenu.className == "showMenu") ||
        (oclMenu && oclMenu.className == "showMenu") ||
        (rptMenu && rptMenu.className == "showMenu") ||
        (favMenu && favMenu.className == "showMenu") ||
        (isBreadCrumbsMenuOpened)) {
        return;
    }

    var defaultMaskIframe = doc.getElementById(DDMENU.CONST.ELEMENT_DEFAULTGLOBALMASKFRAME);
    if (defaultMaskIframe != null && defaultMaskIframe.className != 'hideGlobalMaskFrame') {
        var innerDoc = defaultMaskIframe.contentDocument || defaultMaskIframe.contentWindow.document;
        if (innerDoc != null) {
            //remove onClick event handler
            removeonClickEvent(innerDoc, 'click', defaultMaskFrameEventHandler);
        }
        defaultMaskIframe.className = 'hideGlobalMaskFrame';
        //reset the Global flag
        isBreadCrumbsMenuOpened = false;
    }
    //remove the reference
    innerDoc = null;
}

//OnClick eventHandler for the defaultGlobalMaskFrame
function defaultMaskFrameEventHandler() {
    // reset the Global flag  
    if (isBreadCrumbsMenuOpened) {
        isBreadCrumbsMenuOpened = false;
    }
    hideAllMainMenus();
    hideAllFavMenus();
    hideRRMenu();
    hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
    //close the breadcrumb menus
    hideAllBreadcrumbMenus();
    hideMenu(DDMENU.CONST.ELEMENT_MENUWATCHLISTS); // hide Watchlists
    hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
    hideUserAndEnvDropdownMenu();
}

//onScroll eventHandler for the topLevel menu
function menuScrollEventHandler(event) {
    var evt = event ? event : window.event;
    var srcElement = getEventSourceElement(event);
    //store key[menu ID] and value[scrolltop] pair in Object
    scrollMenuStackObj[srcElement.id] = srcElement.scrollTop;
    // Hide the submenu when scrolling the favorite list. 
    // Otherwise menu and submenu items will be not aligned 
    if (srcElement && srcElement.id == DDMENU.CONST.ELEMENT_FAVORITES_DIV) {
        hidesubMenus(DDMENU.CONST.ELEMENT_FAVORITES_DIV);
    }
}

// Add defaultGlobalMaskFrame if this is a top-level menu.  
var requiresMaskFrameList =
    {
        e1MMenuRoot: true,
        OPENAPPTABLE: true,
        recRptsDiv: true,
        e1MMenuFav: true,
        e1MMenuWatchlists: true,
        preferenceDiv: true
    };

/*show the Dropdown menu*/
function showMenu(elmntId, evt, callback) {
    var doc = document;
    var elmnt = doc.getElementById(elmntId);
    //Initialize onClick event Handler
    InitonClickEventHandler();

    clearTimeout(displayTopLevelMenuCallback);

    if (isIE && isActiveXAvailable() && !isWSRPContainer()) {
        //check is the Click happened on breadCrumbs 
        var clicked_on_breadcrumbs = (elmntId.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) != -1);
        if (requiresMaskFrameList[elmntId] || clicked_on_breadcrumbs) {
            if (elmnt != null) {
                //set the zIndex
                elmnt.style.zIndex = 995;
            }
            // add the Maskframe to cover the application
            showGlobalMaskFrame();
            //update the Global flag
            isBreadCrumbsMenuOpened = true;
        }
    }

    if (evt != null) {
        showTopLevelMenuCallback = function () {
            showMenu(elmntId, new Object(), callback);
        };

        if (elmntId == DDMENU.CONST.ELEMENT_MENUWATCHLISTS) {
            WLIST.initialize();
            WLIST.renderMenu();
            elmnt = doc.getElementById(elmntId);  // might not have existed before now.
        }
        else {
            if (elmnt) {
                var strInnerHTML = elmnt.innerHTML.replace(/^\s+|\s+$/g, '');
            }
            if (strInnerHTML == null || strInnerHTML == "") {
                // The menu content has not come down from the server yet.  Callback until 
                // it's received.
                if (isIE) {
                    displayTopLevelMenuCallback = setTimeout(showTopLevelMenuCallback, 100);
                    return;
                }
                else {
                    displayTopLevelMenuCallback = setTimeout(function () { showMenu(elmntId, evt, callback); }, 50);
                    return;
                }
            }
        }

        var thisMenuWasAlreadyOpen = (elmnt.className == "showMenu");

        /* Additional logic for cascading menus (navigator, favorites), which calls this function 
        from multiple places to expand E1Menus:
        Need to make sure click was on menu button itself, not element within menu.
        */
        if (thisMenuWasAlreadyOpen) {
            var clickedTarget = UTIL.getEventTarget(evt);

            if ((!clickedTarget) || evt.keyCode == 120 || (!DDMENU.CONST.LIST_OF_MENU_BUTTON_IDs.contains(clickedTarget.id))) {
                thisMenuWasAlreadyOpen = false;
            }
        }

        srcElem = getEventSourceElement(evt);
        if (srcElem) {
            if (srcElem.className == 'e1MenuBarItemChild') {
                srcElem.parentNode.setAttribute("active", "true");
            }
        }

        //set the ClassName
        elmnt.className = "showMenu";

        if (navigator.userAgent.toLowerCase().indexOf("webkit") > -1) {
            /* This is a lousy hack put in because some submenus were coming in the full 
            * width of the screen, probably because the child elements have width:100% or 
            * something to that effect.  
            * 
            * Let's not perpetuate this by always setting display:table on things that 
            * aren't tables and instead code child elements and their css properly so 
            * the hack isn't necessary.  For any future items that we know do not need
            * the hack, add them to the excludeList below.  
            */

            var excludeList = { e1MMenuWatchlists: true };

            if (!excludeList[elmntId]) {
                elmnt.style.display = "table";
            }
        }
    }

    applyScrollToTopLevelMenu(elmntId);

    if (elmntId == 'e1MMenuRoot') {
        setFocusOnFastPath(false);
    }

    if (thisMenuWasAlreadyOpen) {
        /* If user clicked on the button for the menu that is currently up, then user wanted 
        to close it, not open it. */
        hideAllDropdownMenus();
    }
    adjustForIEandPortal(elmnt);

    // A function can be passed into showMenu to allow for actions after
    // the menu is shown - such as setting focus on the menu.
    // Look at 'bcFocus' in menuHotKey.js for an example.
    if (callback && callback instanceof Function)
        callback(elmntId, evt);
}

function adjustForIEandPortal(elmnt) {
    // The following is a HACK to relocate menu div to document.body at the same position, so that it is not truncated by e1 form iframe.
    // It is currently necessary because WebCenter renders HTML with IE7 document mode standards.  This should become a no-op in the future.
    if (isWebCenter && document.documentMode == 7) {
        if (elmnt.parentNode != document.body) {
            elmnt.savedLeft = getAbsoluteLeftPos(elmnt);
            elmnt.savedTop = getAbsoluteTopPos(elmnt);
            document.body.appendChild(elmnt);
        }
        elmnt.style.left = elmnt.savedLeft + "px";
        elmnt.style.top = elmnt.savedTop + "px";
        elmnt.style.marginTop = "0px";
        elmnt.style.width = "240px";
    }
}

function getTopLevelMenuParentLabelElementByMenuElement(menuElementId) {
    var parentLabelElement;
    var doc = document;
    if (menuElementId == DDMENU.CONST.ELEMENT_NAVIGATOR_DIV) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_MAINMENU);
    }
    else if (menuElementId == DDMENU.CONST.ELEMENT_FAVORITES_DIV) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_FAVMENUS);
    }
    else if (menuElementId == DDMENU.CONST.ELEMENT_RECRPTSDIV) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_REPORTMENU);
    }
    else if (menuElementId == DDMENU.CONST.ELEMENT_OPENAPPTABLE) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_OPENAPPS);
    }
    else if (menuElementId == DDMENU.CONST.ELEMENT_ROLETABLE) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_ROLE);
    }
    else if (menuElementId == DDMENU.CONST.ELEMENT_PREFERENCE_DIV) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_PREFERENCES);
    }
    else if (menuElementId == DDMENU.CONST.ELEMENT_MENUWATCHLISTS) {
        parentLabelElement = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_WATCHLIST_MENUS);
    }
    else if (menuElementId) {
        // Breadcrumb menus
        if (menuElementId.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) != -1) {
            parentLabelElement = doc.getElementById(menuElementId + BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE);
        }
    }
    else if (menuElementId) {
        // Breadcrumb menus
        if (menuElementId.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE) != -1) {
            parentLabelElement = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV);
        }
    }
    return parentLabelElement;
}

function applyScrollToTopLevelMenu(menuElem) {
    if (!menuElem) {
        return;
    }
    var elmnt = document.getElementById(menuElem);
    if (!elmnt) {
        return;
    }

    if (elmnt.customScrollLogic) {
        // This menu element has a custom scroll logic function - use it instead.
        elmnt.customScrollLogic();
        return;
    }

    // Because this is typically called before the menu is displayed, we have to determine the 
    // menu's top position from the parent anchor rather than the actual top of the menu.
    var parentAnchor = getTopLevelMenuParentLabelElementByMenuElement(menuElem);
    var menuTop = 0;
    if (parentAnchor) {
        menuTop = getAbsoluteTopPos(parentAnchor) + 25;
    }
    var menuHeight = 0;
    var pageHeight = UTIL.pageHeight();
    var availableWindowHeight = pageHeight - menuTop;

    if (elmnt.children && elmnt.children[0] && elmnt.children[0].children) {
        // elmnt.children[0].children.length gives a wrong while in IE, 
        // favorites LeafNodes are appended to the parentNode DIV as children in IE,
        // since IE has issues in loading the Large innerHTML inside the <TD> tag.
        if (isIE && elmnt.id == DDMENU.CONST.ELEMENT_FAVORITES_DIV) {
            var favmenuItem = elmnt.children[0].firstChild;
            //add ManageFavDiv Element -it's default allways
            var favChildrenLength = favmenuItem.children.length;
            while (favmenuItem.nextSibling) {
                favmenuItem = favmenuItem.nextSibling;
                var sibling = favmenuItem.firstChild;
                if (sibling.nodeName == "DIV" && sibling.id.substring(0, 4) == "head") {
                    favChildrenLength += sibling.children.length;
                }
            }
            menuHeight = (favChildrenLength) * menuItemHeight;
        }
        else {
            if (elmnt.id == DDMENU.CONST.ELEMENT_RECRPTSDIV) {
                menuHeight = (elmnt.children.length) * menuItemHeight;
            }
            else {
                menuHeight = (elmnt.children[0].children.length) * menuItemHeight;
            }
        }

        if (menuHeight > availableWindowHeight) {
            elmnt.style.height = (availableWindowHeight) + "px";
            elmnt.style.overflowY = "scroll";
            //attach scrollEventhandler
            addonClickEvent(elmnt, 'scroll', menuScrollEventHandler);

            if ((navigator.userAgent.toLowerCase().indexOf("webkit") > -1) &&
                (elmnt.id == DDMENU.CONST.ELEMENT_FAVORITES_DIV)) {
                // Don't recalculate the div width if we already have.
                // Recalculating the width requires clearing the width 
                // style first, otherwise the div will grow by the width
                // of the scrollbar on each open of the menu.
                if (!elmnt.style.width) {
                    var width = 0;
                    var menuItems = elmnt.children[0].children;
                    if (menuItems) {
                        for (var i = 0; i < menuItems.length; i++) {
                            if ((menuItems[i]) && (menuItems[i].clientWidth > width)) {
                                width = menuItems[i].clientWidth;
                            }
                        }

                        // Need to account for the approximate width of the scroll bar plus some padding.
                        width += 25;
                    }
                }

                elmnt.style.display = "block";
                elmnt.style.width = width + "px";
            }
        }
        else {
            if (navigator.userAgent.toLowerCase().indexOf("webkit") > -1) {
                //elmnt.style.width = null;  leave the width once it has been calculated - resolved a bug in chrome.
                elmnt.style.height = null;
                elmnt.style.overflowY = null;
                elmnt.style.display = "table";
            }
            else {
                elmnt.removeAttribute("style");
            }
        }
    }
}

/*Hide the Dropdown menu*/
function hideMenu(elmntId) {
    elmnt = document.getElementById(elmntId);
    if (elmnt != null) {
        elmnt.className = "hideMenu";
        parentElem = getTopLevelMenuParentLabelElementByMenuElement(elmntId);
        if (parentElem) {
            if (parentElem.className == 'e1MenuBarItemChild') {
                parentElem.parentNode.setAttribute("active", "false");
                removeHover(parentElem.parentNode);  // needed for touch devices
            }
        }
        if (isIE && !isWSRPContainer()) {
            if (requiresMaskFrameList[elmntId]) {
                hideGlobalMaskFrame();
            }
        }
    }

    if ((elmntId == DDMENU.CONST.ELEMENT_PREFERENCE_DIV) ||
	(elmntId == DDMENU.CONST.ELEMENT_ROLETABLE)) {
        hideUserAndEnvDropdownMenu();
    }

    if (navigator.userAgent.toLowerCase().indexOf("webkit") > -1) {
        if (elmnt != null && (elmnt.style.display == "table" || elmnt.style.display == "block")) {
            elmnt.style.display = "none";
        }
    }
}

/*
This function remove all descendants of a div/Elem
*/
function removeChildren(elem) {
    while (elem.hasChildNodes()) {
        removeChildren(elem.lastChild)
        elem.removeChild(elem.lastChild);
    }
}

/*Hide the Dropdown submenus and Clear the submenu floating divs from the DOM*/
function hidesubMenus(elmnt) {
    var doc = document;
    for (j = floatingDivstackSize - 1; j >= 0; j--) {
        var floatingdiv = submenufloatingDivStack[j];
        if (floatingdiv != null) {
            var floatDiv = doc.getElementById(floatingdiv);

            floatDiv.className = "hideMenu";
            removeChildren(floatDiv);
            floatDiv.innerHTML = "";
            floatDiv.parentNode.removeChild(floatDiv);
            floatingdiv = null;
            floatDiv = null;

            submenufloatingDivStack.splice(j, 1);
            floatingDivstackSize--;
            menuLevel.splice(j, 1);
            menuLevelstackSize--;
        }
    }
}

/*Hide the Dropdown last submenu and Clear the submenu floating div from the DOM*/
function hideLastSubMenu(elmnt, indentLevel) {
    var floatingdiv = submenufloatingDivStack[floatingDivstackSize - 1];
    if (floatingdiv != null) {
        var floatDiv = document.getElementById(floatingdiv);
        floatDiv.className = "hideMenu";

        removeChildren(floatDiv);
        floatDiv.innerHTML = "";
        floatDiv.parentNode.removeChild(floatDiv);
        floatingdiv = null;
        floatDiv = null;

        submenufloatingDivStack.splice(floatingDivstackSize - 1, 1);
        floatingDivstackSize--;
        menuLevel.splice(floatingDivstackSize - 1, 1);
        menuLevelstackSize--;
    }
}

function getE1menuAppIframeDoc() {
    var e1menuAppIframe = document.getElementById("e1menuAppIframe");
    var innerDocument = e1menuAppIframe.contentDocument || e1menuAppIframe.contentWindow.document;
    return innerDocument;
}

//Initialize onClick event Handler
function InitonClickEventHandler() {
    addonClickEvent(document, 'click', dropdownMenuEventHandler);
    addonClickEvent(document, 'touchend', dropdownMenuEventHandler);
    // For WSRP Container,  attach onClick event handler on the e1menuAppIframe Document
    if (gContainerId == E1URLFactory.prototype.CON_ID_WSRP) {
        var innerDocument = getE1menuAppIframeDoc();
        //attach onClick event handler
        if (innerDocument != null) {
            addonClickEvent(innerDocument, 'click', dropdownMenuEventHandler)
        }
    }
}
//generic fun to add event Handler to the Document
function addonClickEvent(obj, evt, fn) {
    if (obj.addEventListener)
        obj.addEventListener(evt, fn, false);
    else if (obj.attachEvent)
        obj.attachEvent('on' + evt, fn);
}
//generic fun to remove event Handler from Document
function removeonClickEvent(obj, evt, fn) {
    if (obj.removeEventListener)
        obj.removeEventListener(evt, fn, false);
    else if (obj.detachEvent)
        obj.detachEvent('on' + evt, fn);
}

function isDropdownSection(e) {

    var srcElement = getEventSourceElement(e);

    // stop search when we find a accend who's className is 'showMenu'
    while (srcElement.parentNode) {
        srcElement = srcElement.parentNode;
        if (srcElement.className == "showMenu")
            return true;
    }

    return false;
}

//clicking on the watchlist, the click somehow fell through the 
//watchlist items all the way to the top level <html> tag in IE8
function clickedOnWLMenuInIE8(srcElement) {
    if (isIE) {
        //Check IE browser verion, IE9 userAgent Contains TRIDENT/5.0; IE10 - TRIDENT/6.0; IE8- TRIDENT/4.0
        var isIE8Verion = (userAgent.toUpperCase().indexOf("TRIDENT/4.0") > -1);
        if (isIE8Verion && srcElement.tagName == "HTML") {
            return true;
        }
    }
    return false;
}

//OnClick eventHandler for the dropdown menus
function dropdownMenuEventHandler(event) {
    var evt = event ? event : window.event;
    // In webcenter, WPS the evt is "NULL" when User CLICK ON WHITESPACE BELOW NAVIGATOR
    if (evt == null && (gContainerId == E1URLFactory.prototype.CON_ID_WSRP)) {
        evt = event;
    }
    //Event.target, tells us which element was clicked
    var srcElement = getEventSourceElement(event);
    //checking is onClick event in Dropdownmenu
    var isdropdownsec = isDropdownSection(evt);
    //menu labels array
    var menulabels = new Array('drop_fav_menus', 'drop_mainmenu', 'drop_openapps', 'drop_reportmenu', 'Role', 'preferences', 'drop_watchlist_menus', 'favmenu_downarrow', 'mainmenu_downarrow', 'openapps_downarrow', 'reportmenu_downarrow', 'Role_downarrow', 'preferences_downarrow', 'watchlistmenu_downarrow', 'drop_mainmenu_tab', 'drop_openapps_tab', 'drop_fav_menus_tab', 'drop_watchlist_menus_tab', 'navlabel', 'navlabel_td', 'drop_nav_icon', 'drop_Open_App_icon', 'drop_Open_App_td', 'drop_Open_App_div', 'drop_reportmenu_icon', 'drop_reportmenu_td', 'drop_reportmenu_div', 'drop_fav_menus_td', 'drop_fav_menus_icon', 'drop_fav_menus_div', 'drop_watchlist_menus_td', 'drop_watchlist_menus_div', 'drop_watchlist_menus_icon', 'rolesIcon', 'rolelabel', 'personalizationIcon', 'perlabel');

    var menuLabelId = srcElement.id;

    // Since breadcrumb menu IDs are dynamically generated, we can't add them to the labels array.
    if ((menulabels.contains(menuLabelId)) || (menuLabelId.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) != -1)) {
        // Determine the target of the click.
        var clicked_on_navigator = (menuLabelId == DDMENU.CONST.ELEMENT_DROP_MAINMENU || menuLabelId == DDMENU.CONST.ELEMENT_MAINMENU_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_DROP_NAV_LABEL || menuLabelId == DDMENU.CONST.ELEMENT_DROP_NAV_LABEL_TD || menuLabelId == DDMENU.CONST.ELEMENT_DROP_NAV_LABEL_ICON);
        var clicked_on_favorites = (menuLabelId == DDMENU.CONST.ELEMENT_DROP_FAVMENUS || menuLabelId == DDMENU.CONST.ELEMENT_FAVMENU_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_DROP_FAV_TD || menuLabelId == DDMENU.CONST.ELEMENT_DROP_FAV_ICON || menuLabelId == DDMENU.CONST.ELEMENT_DROP_FAV_DIV);
        var clicked_on_open_apps = (menuLabelId == DDMENU.CONST.ELEMENT_DROP_OPENAPPS || menuLabelId == DDMENU.CONST.ELEMENT_OPENAPPS_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_DROP_OPEN_APP_ICON || menuLabelId == DDMENU.CONST.ELEMENT_DROP_OPEN_APP_TD || menuLabelId == DDMENU.CONST.ELEMENT_DROP_OPEN_APP_DIV);
        var clicked_on_rec_reports = (menuLabelId == DDMENU.CONST.ELEMENT_DROP_REPORTMENU || menuLabelId == DDMENU.CONST.ELEMENT_REPORTMENU_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_DROP_RPT_ICON || menuLabelId == DDMENU.CONST.ELEMENT_DROP_RPT_TD || menuLabelId == DDMENU.CONST.ELEMENT_DROP_RPT_DIV);
        var clicked_on_role_chsr = (menuLabelId == DDMENU.CONST.ELEMENT_ROLE || menuLabelId == DDMENU.CONST.ELEMENT_ROLE_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_ROLE_ICON || menuLabelId == DDMENU.CONST.ELEMENT_ROLE_LABEL);
        var clicked_on_preferences = (menuLabelId == DDMENU.CONST.ELEMENT_PREFERENCES || menuLabelId == DDMENU.CONST.ELEMENT_PREFERENCES_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_PREFERENCES_ICON || menuLabelId == DDMENU.CONST.ELEMENT_PREFERENCES_LABEL);
        var clicked_on_watchlists = (menuLabelId == DDMENU.CONST.ELEMENT_DROP_WATCHLIST_MENUS || menuLabelId == DDMENU.CONST.ELEMENT_WATCHLISTMENU_DOWNARROW || menuLabelId == DDMENU.CONST.ELEMENT_DROP_WL_TD || menuLabelId == DDMENU.CONST.ELEMENT_DROP_WL_DIV || menuLabelId == DDMENU.CONST.ELEMENT_DROP_WL_ICON);
        var clicked_on_breadcrumbs = (menuLabelId.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) != -1);


        // Close everything except the thing that was clicked
        // Always close these:
        hideAddToFavorite();
        hideFavoriteDelete();
        hideFavoriteDeleteAll();
        hideFavoriteProperties();
        hideManageFavorite();
        hidePreferences();
        hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);

        //close dynamic menus in e1menuAppIframe
        var e1menuAppIframe = top.document.getElementById("e1menuAppIframe");
        if (e1menuAppIframe) {
            var innerDocument = e1menuAppIframe.contentWindow;
            if (innerDocument && innerDocument.jdeWebGUIhideAllMenus) {
                innerDocument.jdeWebGUIhideAllMenus(null, null, event);
            }
        }

        // Conditionally close these:
        if (!clicked_on_navigator) {
            hideAllMainMenus();
        }
        if (!clicked_on_favorites) {
            hideAllFavMenus();
        }
        if (!clicked_on_open_apps) {
            hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
        }
        if (!clicked_on_rec_reports) {
            hideRRMenu();
        }
        if (!clicked_on_role_chsr) {
            hideMenu(DDMENU.CONST.ELEMENT_ROLETABLE);
        }
        if (!clicked_on_preferences) {
            hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
        }
        if (!(clicked_on_preferences || clicked_on_role_chsr)) {
            hideUserAndEnvDropdownMenu();
        }
        if (!clicked_on_watchlists) {
            hideMenu(DDMENU.CONST.ELEMENT_MENUWATCHLISTS);
        }
        if (!clicked_on_breadcrumbs) {
            hideAllBreadcrumbMenus();
        }
        else /* If the click is on breadcrumbs */
        {
            hideBreadcrumbMenusOtherThanSelected(menuLabelId);
        }
    }
    else if (isdropdownsec && evt.button != 2 && srcElement.className != "PreferenceMenuItem")//Hide Open apps, mainMenu Dropdowns,task ContextMenu's, Open in new window Context menus
    {
        if (srcElement.tagName == "A" || srcElement.className == "ContextMenuAnchorItem") //TaskItem is Clicked/Open apps App is Clicked (OR) TaskContextMenu Option is selected
        {
            if (menuLabelId == "emptyChildNode")		 // Clicking on the empty node label
            {
                return;
            }
            hideAllMainMenus();
            if (menuLabelId != "") {
                hideOpenApps(menuLabelId);
            }
            hideAllFavMenus();
            hideAllBreadcrumbMenus();
            dismissFloatingDiv();
        }
        else if (menuLabelId == "recRptsMenuItem") // On Click of rec rpts menuitem Hide recent Report dropdown
        {
            hideRRMenu();
        }
        else if (srcElement.id == "ManageFav" || srcElement.id == "ManageFavIcon" || srcElement.id == "ManageFavTD" || srcElement.id == "DROP_MANAGE_FAVORITES") {
            hideAllFavMenus();
        }
        else if (menuLabelId == "Preference" || menuLabelId == "SysOptions" || menuLabelId == "SysProfile") // hide the preference dropdown when user click on any menuitem in this dropdown
        {
            hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
        }
        else if (srcElement.className != "" && srcElement.className == "HoverMenuItem") // Hide the Open apps dropdown when "Openin new window" Option is selected from the ContextMenu
        {
            if (menuLabelId != "")
                hideOpenApps(menuLabelId);
            else
                hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
        }
    }
    else
        if (isdropdownsec && srcElement.tagName == "TD" && srcElement.className == "PreferenceMenuItem") { // Hide Preference dropdown
            hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV); // hide preferences
        }
        else
            if ((srcElement.tagName != "A" && ((srcElement.tagName != "DIV") || (menuLabelId == BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV)) && !isdropdownsec
            && !(menulabels.contains(menuLabelId)) && (!clickedOnWLMenuInIE8(srcElement))) || menuLabelId == "menutitle")  // Hide all dropdown menu's on document.onClick()
            {
                //remove onClick eventHandler
                removeonClickEvent(document, 'click', dropdownMenuEventHandler);
                removeonClickEvent(document, 'touchend', dropdownMenuEventHandler);
                // For WSRP Container,  remove onClick event handler on the e1menuAppIframe Document
                if (gContainerId == E1URLFactory.prototype.CON_ID_WSRP) {
                    var innerDocument = getE1menuAppIframeDoc();
                    if (innerDocument != null) {
                        removeonClickEvent(innerDocument, 'click', dropdownMenuEventHandler)
                    }
                }
                hideAllDropdownMenus();
                //cancel event bubbling..
                stopEventBubbling(event);
            }
            else if (menuLabelId == DDMENU.CONST.ELEMENT_ROLEBACKLINK) //Clicking on Back to Navigator
            {
                hideAllFavMenus();
                hideAllMainMenus();
                hideAllBreadcrumbMenus();
                hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
                hideRRMenu();
                hideMenu(DDMENU.CONST.ELEMENT_ROLETABLE);
                hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
                hideManageFavorite();
            }
}

function hideAllDropdownMenus() {
    hideAllMainMenus();
    hideAllBreadcrumbMenus();
    hideAllFavMenus();
    hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE); // hide Open Apps
    hideRRMenu();
    hideMenu(DDMENU.CONST.ELEMENT_MENUWATCHLISTS); // hide Watchlists
    hideMenu(DDMENU.CONST.ELEMENT_ROLETABLE);  // hide Roles
    hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV); // hide preferences
    hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
    hideUserAndEnvDropdownMenu();
}

function hideOpenApps(menuLabelId) {
    var substringId = menuLabelId.substring(0, menuLabelId.lastIndexOf("_"));
    if (substringId == "OCLHREF") //open Apps
        hideMenu(DDMENU.CONST.ELEMENT_OPENAPPTABLE);
}

function hideAllMainMenus() {
    hideMenu(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV); // hide mainmenu
    hidesubMenus(DDMENU.CONST.ELEMENT_NAVIGATOR_DIV); //hide all mainmenu submenu's
    clearBreadCrumbs();
    clearScrollMenuStackObj();
}

function hideAllBreadcrumbMenus() {
    hideBreadcrumbMenusOtherThanSelected();
}

function hideBreadcrumbMenusOtherThanSelected(selectedMenuId) {
    var bcBar = document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV);
    if (bcBar) {
        var bcBarChildren1 = bcBar.getElementsByTagName('TABLE');
        if (bcBarChildren1 && bcBarChildren1[0]) {
            var bcBarChildren2 = bcBarChildren1[0].getElementsByTagName('TBODY');
            if (bcBarChildren2 && bcBarChildren2[0]) {
                var bcBarChildren3 = bcBarChildren2[0].getElementsByTagName('TR');
                if (bcBarChildren3 && bcBarChildren3[0]) {
                    var bcBarChildren4 = bcBarChildren3[0].getElementsByTagName('TD');
                    if (bcBarChildren4) {
                        for (var i = 0; i < bcBarChildren4.length; i++) {
                            var bcBarElements = bcBarChildren4[i].children;
                            for (var j = 0; j < bcBarElements.length; j++) {
                                if ((bcBarElements[j].id.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) != -1) && (bcBarElements[j].id.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE) == -1)) {
                                    if ((bcBarElements[j].id + BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE) != selectedMenuId) {
                                        hideMenu(bcBarElements[j].id); // hide mainmenu
                                        hidesubMenus(bcBarElements[j].id); //hide all mainmenu submenu's
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    clearBreadCrumbs();
}

function areBreadcrumbDropdownMenusVisible() {
    if (this.jdeMenuParent.breadcrumbLoading == true) {
        return true;
    }
    var bcBar = document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV);
    if (bcBar) {
        var bcBarChildren1 = bcBar.getElementsByTagName('TABLE');
        if (bcBarChildren1 && bcBarChildren1[0]) {
            var bcBarChildren2 = bcBarChildren1[0].getElementsByTagName('TBODY');
            if (bcBarChildren2 && bcBarChildren2[0]) {
                var bcBarChildren3 = bcBarChildren2[0].getElementsByTagName('TR');
                if (bcBarChildren3 && bcBarChildren3[0]) {
                    var bcBarChildren4 = bcBarChildren3[0].getElementsByTagName('TD');
                    if (bcBarChildren4) {
                        for (var i = 0; i < bcBarChildren4.length; i++) {
                            var bcBarElements = bcBarChildren4[i].children;
                            for (var j = 0; j < bcBarElements.length; j++) {
                                if ((bcBarElements[j].id.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) != -1) && (bcBarElements[j].id.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE) == -1)) {
                                    if (bcBarElements[j].className == "showMenu") {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return false;
}

function hideAllFavMenus() {
    hideMenu(DDMENU.CONST.ELEMENT_FAVORITES_DIV); // hide favmenu
    hidesubMenus(DDMENU.CONST.ELEMENT_FAVORITES_DIV); //hide all favsubmenu's
    clearBreadCrumbs();
    clearScrollMenuStackObj();
}

function hideManageFavorite() {
    var doc = document;
    if (doc.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV) != null || doc.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV) != undefined) {
        window["0_1_Fav_Mang"].onClose();
    }
}
function hideAddToFavorite() {
    var doc = document;
    if (doc.getElementById('0_1_Fav_Add') != null || doc.getElementById('0_1_Fav_Add') != undefined) {
        window["0_1_Fav_Add"].onClose();
    }
}

function hideFavoriteProperties() {
    var doc = document;
    if (doc.getElementById('0_1_Fav_Pro') != null || doc.getElementById('0_1_Fav_Pro') != undefined) {
        window["0_1_Fav_Pro"].onClose();
    }
}

function hidePreferences() {
    var doc = document;
    if (doc.getElementById('UI_Pref') != null || doc.getElementById('UI_Pref') != undefined) {
        window["UI_Pref"].onClose();
    }
}

function hideFavoriteDelete() {
    var doc = document;
    if (isWSRPContainer()) {
        if (doc.getElementById('0_1_Fav_Remv') != null || doc.getElementById('0_1_Fav_Remv') != undefined) {
            window["0_1_Fav_Remv"].onClose();
        }
    }
    else {
        if (doc.getElementById('0_1_Fav_Remv5') != null || doc.getElementById('0_1_Fav_Remv5') != undefined) {
            window["0_1_Fav_Remv5"].onClose();
        }
    }
}

function hideFavoriteDeleteAll() {
    var doc = document;
    if (isWSRPContainer()) {
        if (doc.getElementById('0_1_Fav_Remv_All') != null || doc.getElementById('0_1_Fav_Remv_All') != undefined) {
            window["0_1_Fav_Remv_All"].onClose();
        }
    }
    else {
        if (doc.getElementById('0_1_Fav_Remv_All5') != null || doc.getElementById('0_1_Fav_Remv_All5') != undefined) {
            window["0_1_Fav_Remv_All5"].onClose();
        }
    }
}

// this function hide the recent reports dropdown
// and Clean up the DOM
function hideRRMenu() {
    var doc = document;
    var rrMenuDiv = doc.getElementById(DDMENU.CONST.ELEMENT_RECRPTSDIV);
    if (rrMenuDiv != null) {
        removeMaskFrame(rrMenuDiv, true);
        rrMenuDiv.className = "hideMenu";
        rrMenuDiv.parentNode.removeChild(rrMenuDiv);
        var menuBarItem = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_REPORTMENU).parentNode;
        menuBarItem.setAttribute("active", "false");
        removeHover(menuBarItem);  // needed for touch devices
    }
}

function stopEventBubbling(event) {
    if (event.preventDefault) { //meaning is Firefox
        event.stopPropagation();
        event.preventDefault();
        if (event.cancelBubble) {
            event.cancelBubble = true;
        }
    }
    else {
        event.cancelBubble = true;
        event.returnValue = false;
    }
    return false;
}

//function to expose running an e1 application from an Welcome HTML override page for customers
//Customer must send in at least appId and formId
//This information will come from information button when running the application.
function runE1App(appId, formId, version, menuSysCode, taskName, formDSTmpl, formDSData, mode) {
    if (isIOS) {
        updateAppFrameHeight();
        var fastPathField = document.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
        if (fastPathField) {
            fastPathField.blur();
        }
    }

    if (appId == undefined) { return; }
    if (formId == undefined) { return; }
    if (version == undefined) { version = ""; }
    if (menuSysCode == undefined) { menuSysCode = ""; }
    if (taskName == undefined) { taskName = ""; }
    if (formDSTmpl == undefined) { formDSTmpl = ""; }
    if (formDSData == undefined) { formDSData = ""; }
    if (mode == undefined) { mode = 1; }

    window.RunNewApp('launchForm', '0', appId, 'APP', formId, version, mode, menuSysCode, taskName, formDSTmpl, formDSData);
}

//function to expose running an e1 application from an Welcome HTML override page for customers with a query
//Customer must send in at least appId and formId
//This information will come from information button when running the application.

function runE1AppWithQuery(appId, formId, version, queryWOBNM, formDSTmpl, formDSData) {
    var OID = appId + "_" + formId;
    if (version) {
        OID = OID + "_" + version;
    }
    runE1AppQuery(OID, queryWOBNM, formDSTmpl, formDSData);
}

function runE1AppQuery(OID, queryWOBNM, formDSTmpl, formDSData) {
    if (isIOS) {
        updateAppFrameHeight();
        var fastPathField = document.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
        if (fastPathField) {
            fastPathField.blur();
        }
    }
    var splitFormId = OID.split("_");
    var appId = splitFormId[0];
    var formId = "";
    var version = "";
    if (splitFormId.length > 1) {
        formId = splitFormId[1];
    }
    if (splitFormId.length > 2) {
        version = splitFormId[2];
    }
    if (appId == undefined) { return; }
    if (formId == undefined) { return; }
    if (version == undefined) { version = ""; }
    var menuSysCode = "";
    var taskName = "";
    if (formDSTmpl == undefined) { formDSTmpl = ""; }
    if (formDSData == undefined) { formDSData = ""; }
    var mode = 1;

    window.RunNewApp('launchForm', '0', appId, 'APP', formId, version, mode, menuSysCode, taskName, formDSTmpl, formDSData, "", "", "", "", "", "", queryWOBNM);
}

function runE1AppFromWatchlist(appId, formId, version, watchlistId) {
    var OID = appId + "_" + formId;
    if (version) {
        OID = OID + "_" + version;
    }

    runE1AppWatchlist(OID, watchlistId);
}


// Function to an app using the information from a watchlist
function runE1AppWatchlist(OID, watchlistId) {
    if (isIOS) {
        updateAppFrameHeight();
    }
    var splitFormId = OID.split("_");
    var appId = splitFormId[0];
    var formId = "";
    var version = "";
    if (splitFormId.length > 1) {
        formId = splitFormId[1];
    }
    if (splitFormId.length > 2) {
        version = splitFormId[2];
    }
    if (appId == undefined) { return; }
    if (formId == undefined) { return; }
    if (version == undefined) { version = ""; }
    var menuSysCode = "";
    var taskName = "";
    var formDSTmpl = "";
    var formDSData = "";
    var mode = 1;

    window.RunNewApp('launchForm', '0', appId, 'APP', formId, version, mode, menuSysCode, taskName, formDSTmpl, formDSData, "", "", "", "", "", "", "", "", watchlistId);
}


//function to expose running an e1 ube from an Welcome HTML override page for customers
//Customer must send in at least ....................
//This information will come from information button when running the application.
function runE1UBE(appId, launchAction, version, promptPO) {
    if (appId == undefined) { return; }
    if (launchAction == undefined) { launchAction = "promptForVersion"; }
    if (version == undefined) { version = ""; }
    if (promptPO == undefined) { promptPO = "0"; }

    window.RunNewApp(launchAction, promptPO, appId, 'UBE', '', version, '1', '', '', '', '');
}

function initRunE1App() {
    window.jdeMenuParent.focusOnMenuElement();
    window.updateOCL();
}

// Wrappers to support existing portal RunOWApp functions
var tempTempl = "";
var tempValues = "";
function RunOWApp(callingFormId) {
    if (callingFormId && callingFormId.length > 0) {
        var splitFormId = callingFormId.split("_");
        var appId = splitFormId[0];
        var formId = "";
        var version = "";
        if (splitFormId.length > 1) {
            formId = splitFormId[1];
        }
        if (splitFormId.length > 2) {
            version = splitFormId[2];
        }
        window.runE1App(appId, formId, version, "", "", window.tempTempl, window.tempValues);
    }
    window.tempTempl = "";
    window.tempValues = "";
}

// Wrapper for backward compatibility of portal RunOWApp
function addRunOWAppFI(templ, values) {
    window.tempTempl = templ;
    window.tempValues = values;
}

function showHomePageManagementPane(event, isImage) {
    var e1menuAppIframeAbove = top.document.getElementById("e1menuAppIframe");
    var e1Win = e1menuAppIframeAbove.contentWindow;
    var vCHP_SP = (CHP_SP) ? CHP_SP : e1Win.CHP_SP;
    vCHP_SP.showManageE1Pages(event, 0, null, isImage);
}

function onCloseOrphanWindows() {
    try {
        var e1menuAppIframeAbove = top.document.getElementById("e1menuAppIframe");
    } catch (err) {
        //This try/catch was added to handle forms embedded in iFrames 
        //that don�t have access to the parent or top document.  
        var e1menuAppIframeAbove = null;
    }
    if (e1menuAppIframeAbove != null) {
        var e1Win = e1menuAppIframeAbove.contentWindow;
        var vCHP_SP = (window.CHP_SP) ? window.CHP_SP : e1Win.CHP_SP;
        if (vCHP_SP != null) {
            vCHP_SP.browserCloseVAWindow();
        }
        onCloseDatabrowserWindow();
    }
    else {
        try {
            var e1Form = top.document.getElementById("E1PaneForm");
        } catch (err) {
            //This try/catch was added to handle forms embedded in iFrames 
            //that don�t have access to the parent or top document.
            var elForm = null;
        }
        if (e1Form != null && e1Form.jdemafjasComponent != null) {
            var value = e1Form.jdemafjasComponent.defaultValue;
            if (window.opener != null &&
           ((value == "JDE_AHQCLIENT_VIRTUAL") ||
            (value == "JDE_AHQCLIENT_VIRTUAL_VA") ||
            (value == "JDE AHQCLIENT VIRTUAL") ||
            (value == "JDE AHQCLIENT VIRTUAL VA"))) {
                setCloseMainWin(true);

                onCloseDatabrowserWindow();
            }
        }
    }
}


// Function which launches the Prferences Window
function showPreferences() {
    var preferenceText = prefText.label[0].preferenceText;
    var saveCloseText = prefText.label[0].saveCloseText;
    var cancelText = prefText.label[0].cancelText;
    var linkRedirectToAppText = prefText.label[0].linkRedirectToAppText;
    var linkRedirectToWebText = prefText.label[0].linkRedirectToWebText;
    var nativeContainerConnectionsAllowed = prefText.label[0].nativeContainerConnectionsAllowedValue;
    var resetE1PagesText = prefText.label[0].resetDeviceSpecificE1PagesText;
    var clearBreadcrumbHistoryText = prefText.label[0].clearBreadcrumbHistoryText;
    var ID = "UI_Pref";
    var cancelScript = "javascript:window['UI_Pref'].onClose();"
    var saveCloseScript = "javascript:prefSaveClose();"
    var prefButtonHoverScript = "javascript:prefButtonHover(this.id, event);"
    var toggleRedirectScript = "setNativeContainerRedirect();"
    var resetDeviceSpecificE1PagesScript = "resetDeviceSpecificE1Pages();"
    var clearBcHistoryButtonResetScript = "clearBcHistoryButtonReset();"
    var menuObject = jdeMenuParent;
    var sb = new PSStringBuffer();
    var width = 288;
    var doc = document;

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();
    var height = (isIOS ? ((prefObj.preferences.length + 2) * 24) + 45 : (prefObj.preferences.length + 2) * 24) + 75; //+1 is for reset device e1 pages pref
    if (isIOS && navigator.cookieEnabled && nativeContainerConnectionsAllowed == "true") {
        height = height + 20;
        width = 365;
    }

    if (isSimplifiedNavigation) // deduct the clearBreadcrumbHistoryText
        height = height - 34;

    var left = (screenWidth / 2) - (width / 2);
    var top = (screenHeight / 2) - (height / 2);
    sb.append(" <div><form id=\"Preferences\" name=\"Preferences\">");

    for (var i = 0; i < prefObj.preferences.length; i++) {
        sb.append("\n<div class='prefItem'>");
        sb.append("<input type=\"checkbox\"");
        sb.append(" value=\"" + prefObj.preferences[i].value + "\"");
        sb.append(" id=\"" + prefObj.preferences[i].checkboxId + "\"");
        sb.append(" name=\"" + prefObj.preferences[i].name + "\"");
        sb.append(" aria-label=\"" + prefObj.preferences[i].text + "\"");
        if (prefObj.preferences[i].checked == "true") {
            sb.append(" checked");
        }
        sb.append(" onclick=\"javascript:switchPrefStatus(\'");
        sb.append(prefObj.preferences[i].checkboxId);
        sb.append("\');\"/>");
        sb.append("<span>" + prefObj.preferences[i].text + "</span>");
        sb.append("</div>");
    }
    // Reset device specific E1 pages option
    sb.append("\n<div class='prefItem'>");
    sb.append("<input id=\"resetDeviceSpecificPages\" class=\"button\" value=\"" + resetE1PagesText + "\" type=\"button\" onmouseover=\"" + prefButtonHoverScript + "\" onmouseup=\"" + prefButtonHoverScript + "\" onmouseout=\"" + prefButtonHoverScript + "\" onmousedown=\"" + prefButtonHoverScript + "\" onClick=\"" + resetDeviceSpecificE1PagesScript + "\"/>");
    sb.append("</div>");

    // Clear breadcrumb history
    if (!isSimplifiedNavigation && !isNativeContainer) {
        sb.append("\n<div class='prefItem'>");
        sb.append("<input id=\"clearBcHistory\" class=\"button\"  value=\"" + clearBreadcrumbHistoryText + "\" type=\"button\" onmouseover=\"" + prefButtonHoverScript + "\" onmouseup=\"" + prefButtonHoverScript + "\" onmouseout=\"" + prefButtonHoverScript + "\" onmousedown=\"" + prefButtonHoverScript + "\" onClick=\"" + clearBcHistoryButtonResetScript + "\"/>");
        sb.append("</div>");
    }

    if (isIOS && navigator.cookieEnabled && (nativeContainerConnectionsAllowed == "true") && !isNativeContainer) {
        sb.append("\n<div class='prefItem'>");
        var redirectLabel = linkRedirectToAppText;
        if (jdeMenuParent.getCookie("jdeE1OpenIniPadContainer") == "true") {
            redirectLabel = linkRedirectToWebText;
        }
        sb.append("<input type=\"button\" id=\"PrefChangeNativeContainerLinkButton\" value=\"" + redirectLabel + "\" class=\"button\" onmouseover=\"" + prefButtonHoverScript + "\" onmouseout=\"" + prefButtonHoverScript + "\" onmousedown=\"" + prefButtonHoverScript + "\" onClick=\"" + toggleRedirectScript + "\"/>");
        sb.append("</div>");
    }
    sb.append("\n<div class='prefItem'>");
    sb.append("<input type=\"button\" style=\"visibility:hidden;\" name=\"hidbutton\" id=\"hidbutton\"/>");
    sb.append("<input type=\"button\" id=\"PrefSaveClose\"  value=\"" + saveCloseText + "\" class=\"Prefbuttondisabled\" disabled=\"disabled\" onmouseover=\"" + prefButtonHoverScript + "\" onmouseout=\"" + prefButtonHoverScript + "\" onmousedown=\"" + prefButtonHoverScript + "\" onClick=\"" + saveCloseScript + "\" aria-label=\"" + saveCloseText + "\"/>");
    sb.append("<input type=\"button\" id=\"PrefCancel\" value=\"" + cancelText + "\" class=\"button\" onmouseover=\"" + prefButtonHoverScript + "\" onmouseout=\"" + prefButtonHoverScript + "\" onmousedown=\"" + prefButtonHoverScript + "\" onClick=\"" + cancelScript + "\" aria-label=\"" + cancelText + "\"/>");
    sb.append("</div>");
    sb.append("</form></div>");

    var eventListener = {};
    eventListener.onClose = function (e) {
        window.prefereWin.destory();
        if (userAccessibilityMode) {
            var brandingDiv = doc.getElementById('brandingDiv');
            brandingDiv.setAttribute("aria-hidden", false);

            var caroHolder = doc.getElementById('caroHolder');
            caroHolder.setAttribute("aria-hidden", false);
        }
    }
    window.prefereWin = createPopup(ID, SHOW_UI_PREF, true, false, true, true, preferenceText, null, null, null, null, null, left, top, width, height, null, sb, eventListener);

    if (isIOS) {
        setFocusOnPopup('hidbutton');
    }
    else {
        var e = document.getElementById('prefCheckBox1');
        if (e) {
            if (userAccessibilityMode) {
                var brandingDiv = doc.getElementById('brandingDiv');
                if (brandingDiv)
                    brandingDiv.setAttribute("aria-hidden", true);

                var caroHolder = doc.getElementById('caroHolder');
                if (caroHolder)
                    caroHolder.setAttribute("aria-hidden", true);
            }
            window.setTimeout(function () { moveFocusToCloseForPopupWindow(ID, null) }, 500); // set the focuse on X for consistent behavior like other popup.   
        }
    }
}

function setFocusOnPopup(id) {
    document.getElementById(id).click();
}

function switchPrefStatus(id) {
    if (id == "prefCheckBox1" && isRICheckBoxClicked == false) {
        isRICheckBoxClicked = true;
    }

    var doc = document;
    //Enabling the Save&Close button if any of the prefCheckBox status is changed.
    if (doc.getElementById('PrefSaveClose').className == "Prefbuttondisabled") {
        doc.getElementById('PrefSaveClose').disabled = false;
        doc.getElementById('PrefSaveClose').className = "button";
    }
}

function prefButtonHover(id, evt) {
    var elem = document.getElementById(id);
    if (elem) {
        if (elem.className == "Prefbuttondisabled") {
            return;
        }
        else if (evt.type == "mouseover") {
            elem.className = "FAVbuttonMouseOver";
        }
        else if (evt.type == "mouseout") {
            elem.className = "button";
        }
        else if (evt.type == "mousedown") {
            elem.className = "FAVbuttonMouseDown";
        }
        else if (evt.type == "mouseup") {
            document.getElementById(id).className = "button";
        }
        else {
            return;     // No need to handel other mouse events
        }
    }
    else {
        return;
    }
}

function prefSaveClose() {
    if (isRICheckBoxClicked) {
        storeRelatedInfoShownPreferences();
    }
    else {
        storePreferences();
    }

    if (!isWSRPContainer() && CARO && CARO.setTileSize) {
        CARO.setTileSize(isPreferenceChecked("UseLargeIconsInCarouselPreference"));
    }

    // After the preferences dialog is dismissed, update the UI to reflect the 
    // Show Breadcrumbs preference.
    BREADCRUMBS.showBreadcrumbBar(isPreferenceChecked("showBreadcrumbsPreference"));

    javascript: window['UI_Pref'].onClose();
}

function storeRelatedInfoShownPreferences() {
    var menuObject = jdeMenuParent;
    if (!menuObject.isMainMenu) {
        return;
    }

    storePreferences();

    // only need to update view when there is E1 form in E1 pane.
    var e1menuAppIframeAbove = top.document.getElementById("e1menuAppIframe");
    var e1Win = e1menuAppIframeAbove.contentWindow;
    if (e1Win.RIUTIL) {
        e1Win.RIUTIL.postAjaxRequest(null, e1Win.RIUTIL.PRODUCT_ALL, e1Win.RIUTIL.AJAX_CMD_RI_SHOW_HIDE_CHANGE);
    }
}

function storePreferences() {
    var menuObject = jdeMenuParent;
    if (!menuObject.isMainMenu) {
        return;
    }

    window.setPreferences();

    var sb = "<html><body><form id=\"pref\" method=post action=\""; //"
    sb += menuObject.prefServiceUrl;
    sb += "\">"; //"
    sb += "<input type=hidden name=timestamp value='"; //"
    sb += (new Date()).getTime();
    sb += "'>";
    sb += "<input type=hidden name=preferencesOptions value='";
    sb += menuObject.preferencesOptions;
    sb += "'>";
    sb += "<input type=hidden name=resetDeviceSpecificE1Pages value=";
    sb += resetDeviceSpecificPages ? 'true' : 'false';
    sb += ">";

    sb += "<input type=hidden name=clearBreadcrumbHistory value=";
    sb += clearBreadcrumbHistory ? 'true' : 'false';
    sb += ">";
    sb += "</form></body></html>";
    var frm = null;
    if (isIE)
        frm = document.frames["RunAppHiddenIframe"];
    else
        frm = window.frames["RunAppHiddenIframe"];
    try {
        var newDoc = frm.document;
        newDoc.open("text/html");
        newDoc.charset = "UTF-8";
        newDoc.write(sb);
        newDoc.close();
        frm.document.getElementById("pref").submit();
        resetDeviceSpecificPages = false;

        if (clearBreadcrumbHistory) {

            document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ARROW).style.display = 'none';
            breadcrumbHistoryHtml = "";

        }
        clearBreadcrumbHistory = false;
    }
    catch (problem) { };

    //if (isIOS)
    {
        menuObject.setCookie("jdeE1OpenIniPadContainer", prefObj.toApp, new Date("1/1/2100"), "/", "", "");

        try {
            var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
            e1UrlCreator.setURI('NativeContainerRedirectService');
            var formData = new FormData();
            formData.append("bypassRedirect", "true");
            var nativeContainerRedirectDecisionSubmission = new XMLHttpRequest();
            nativeContainerRedirectDecisionSubmission.open("POST", e1UrlCreator.toString());
            nativeContainerRedirectDecisionSubmission.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            nativeContainerRedirectDecisionSubmission.setRequestHeader("Cache-Control", "no-cache");
            nativeContainerRedirectDecisionSubmission.send(formData);
        }
        catch (problem) {
            // If anything went wrong, point the preference to the web browser as a fallback behavior.
            menuObject.setCookie("jdeE1OpenIniPadContainer=", "false", new Date("1/1/2100"), "/", "", "");
        }
    }
}

function setPreferences() {
    prefForm = document.getElementById("Preferences");
    var menuObject = jdeMenuParent;
    var oldOptions = prefText.label[0].prefOptionsValue; // existing preferences value in DB
    var newOptions = menuObject.MENU_NO_PREFERNCES;

    // Show Related information preference - default value is ENABLED
    if (prefForm.showRelatedInfoPreference) {
        if (prefForm.showRelatedInfoPreference.checked == false) {
            newOptions += menuObject.SHOW_RELATED_INFO;
            changePreference("showRelatedInfoPreference", "false");
        }
        else {
            changePreference("showRelatedInfoPreference", "true");
        }
    }

    // Enable Hover popup preference - default value is ENABLED
    if (prefForm.enablePopupPreference) //default override is visible
    {
        if (prefForm.enablePopupPreference.checked == false) {
            newOptions += menuObject.ENABLE_POPUP_OPTION;
            changePreference("enablePopupPreference", "false");
        }
        else {
            changePreference("enablePopupPreference", "true");
        }
    }

    // Enable row/form exit on right mouse click prefence - default value is ENABLED
    if (prefForm.rowExitPreference) {
        if (prefForm.rowExitPreference.checked == false) {
            newOptions += menuObject.ENABLE_RIGHTCLICK_ROWEXIT;
            changePreference("rowExitPreference", "false");
        }
        else {
            changePreference("rowExitPreference", "true");
        }
    }

    // Show Add Favorites Popup preference - default value is ENABLED
    if (prefForm.addFavPreference) {
        if (prefForm.addFavPreference.checked == false) {
            newOptions += menuObject.SHOW_ADD_FAV_POPUP;
            changePreference("addFavPreference", "false");
        }
        else {
            changePreference("addFavPreference", "true");
        }
    }

    // Enable Auto Suggest preference - default value is ENABLED
    if (prefForm.autoSuggestPreference) {
        if (prefForm.autoSuggestPreference.checked == false) {
            newOptions += menuObject.ENABLE_AUTO_SUGGEST;
            changePreference("autoSuggestPreference", "false");
        }
        else {
            changePreference("autoSuggestPreference", "true");
        }
    }

    // Zoom in preference for iPad - default value is DISABLED
    if (prefForm.iPadSmallFormFactorPreference) {
        if (prefForm.iPadSmallFormFactorPreference.checked) {
            newOptions += menuObject.IPAD_SMALL_FORM_FACTOR;
            changePreference("iPadSmallFormFactorPreference", "true");
        }
        else {
            changePreference("iPadSmallFormFactorPreference", "false");
        }
    }
    else {
        if (!isIOS && ((oldOptions & menuObject.IPAD_SMALL_FORM_FACTOR) > 0)) {
            newOptions += menuObject.IPAD_SMALL_FORM_FACTOR;
        }
    }

    // Use large icons in carousel preference - default value is DISABLED
    if (prefForm.UseLargeIconsInCarouselPreference) {
        if (prefForm.UseLargeIconsInCarouselPreference.checked) {
            newOptions += menuObject.USE_LARGE_ICONS_IN_CAROUSEL;
            changePreference("UseLargeIconsInCarouselPreference", "true");
        }
        else {
            changePreference("UseLargeIconsInCarouselPreference", "false");
        }
    }

    // Show menu breadcrumbs preference - default value is ENABLED
    if (prefForm.showBreadcrumbsPreference) {
        if (prefForm.showBreadcrumbsPreference.checked == false) {
            newOptions += menuObject.SHOW_BREADCRUMBS;
            changePreference("showBreadcrumbsPreference", "false");
        }
        else {
            changePreference("showBreadcrumbsPreference", "true");
        }
    }

    //Open Application In New Window preference - default value is DISABLED
    if (prefForm.openAppInNewWindowPreference) {
        if (prefForm.openAppInNewWindowPreference.checked) {
            newOptions += menuObject.OPEN_APP_IN_NEW_WINDOW;
            changePreference("openAppInNewWindowPreference", "true");
        }
        else {
            changePreference("openAppInNewWindowPreference", "false");
        }
    }

    //Enable Simpflied Exits preference - default value is ENABLED
    if (prefForm.enableSimplifiedExits) {
        if (prefForm.enableSimplifiedExits.checked) {
            newOptions += menuObject.ENABLE_SIMPLIFIED_EXITS;
            changePreference("enableSimplifiedExits", "true");
        }
        else {
            changePreference("enableSimplifiedExits", "false");
        }
    }

    // Use Thunderbird as default calendar client preference
    if (prefForm.useThunderbirdAsCalendarClientPreference) {
        if (prefForm.useThunderbirdAsCalendarClientPreference.checked) {
            newOptions += menuObject.USE_THUNDERBIRD_AS_CALENDAR_CLIENT;
            changePreference("useThunderbirdAsCalendarClientPreference", "true");
        }
        else {
            newOptions += menuObject.TURN_OFF_THUNDERBIRD_AS_CALENDAR_CLIENT;
            changePreference("useThunderbirdAsCalendarClientPreference", "false");
        }
    }

    if ((oldOptions & menuObject.MENU_MINIMIZE_ON_LAUNCH) > 0) {
        newOptions += menuObject.MENU_MINIMIZE_ON_LAUNCH;
    }

    if ((oldOptions & menuObject.MENU_SHOW_MAXIMIZED) > 0) {
        newOptions += menuObject.MENU_SHOW_MAXIMIZED;
    }

    if ((oldOptions & menuObject.MENU_PREFERENCES_SHOWN) > 0) {
        newOptions += menuObject.MENU_PREFERENCES_SHOWN;
    }

    if ((oldOptions & menuObject.MENU_ACTIONS_SHOWN) > 0) {
        newOptions += menuObject.MENU_ACTIONS_SHOWN;
    }

    if ((oldOptions & menuObject.MENU_ROLES_SHOWN) > 0) {
        newOptions += menuObject.MENU_ROLES_SHOWN;
    }

    menuObject.preferencesOptions = newOptions;
}

function showRecRptsMenu(recRptsDivName, event) {
    //check is the recRptsDiv exists
    var doc = document;
    var recReportsDiv = doc.getElementById(DDMENU.CONST.ELEMENT_RECRPTSDIV);
    if (recReportsDiv != null) {
        // Recent Reports menu is already open - close it
        hideRRMenu();
        return recReportsDiv;
    }
    var sb = new PSStringBuffer();
    //do soft refresh
    syncRecentReports(false);

    //crete a DIV and set attributes
    var recRptsDiv = doc.createElement('div');
    recRptsDiv.id = DDMENU.CONST.ELEMENT_RECRPTSDIV;
    recRptsDiv.style.display = "none";
    // set ARIA Role 'menu' for the Div
    if (jdeMenuParent.isAccessibilityEnabled) {
        recRptsDiv.setAttribute('role', 'menu');
    }

    // webcenter in IE8 has issues with 100% width
    if (isWSRPContainer() && isIE)
        recRptsDiv.style.width = "180px";

    var menuBarItem = doc.getElementById(DDMENU.CONST.ELEMENT_DROP_REPORTMENU).parentNode;
    menuBarItem.setAttribute("active", "true");
    menuBarItem.appendChild(recRptsDiv);

    //Add WSJ menuItem
    sb.append("<table id=\"RecRptsTable\" class=\"MenuNormal\" cellspacing=0 border=0 tabIndex=0 onmouseover=\"this.className='HoverMenuItem';\" onmouseout=\"this.className='MenuNormal';\"");
    sb.append("onfocus=\"this.className='HoverMenuItem';\" onblur=\"this.className='MenuNormal';\" onkeydown=\"return doMenuKeyDown(this,event,false,'');\" style=\"cursor: pointer;\"");
    if (jdeMenuParent.isAccessibilityEnabled) {
        sb.append(" role=\"menuitem\" aria-labelledby=\"Submitted\"");
    }
    if (!isTouchEnabled)
        sb.append(" onClick");
    else
        sb.append(" ontouchend");
    sb.append("=\"javascript:runE1App('P986110B','W986110BA', '");
    sb.append(WSJNewVersion);
    sb.append("')\" ");
    sb.append("title=\"Submitted Job Search Application: P986110B, Form: W986110BA");
    if (WSJNewVersion != "")
        sb.append(", Version: ZJDE0001");
    sb.append("\">");

    sb.append("<tr><td valign=middle style=\"height:16px;width:16px;\">");
    sb.append("<table cellspacing=0 cellpadding=0><tbody><tr><td>");
    sb.append("<span class=\"RecRptsAppIcon\">").append("<IMG SRC='").append(window["WEBGUIRES_images_spacer_gif"]).append("'  width=16 alt=\"\">");
    sb.append("</span></td></tr></tbody></table></td>");
    sb.append("<td style=\"width:1px;\">&nbsp;</td>");
    sb.append("<td nowrap id=\"recRptsMenuItem\" valign=middle ");
    if (!isRTL) {
        sb.append("align=\"left\">");
    }
    else {
        sb.append("align=\"right\">");
    }

    if (jdeMenuParent.isAccessibilityEnabled) {
        sb.append("<span id=\"\">");
    }

    if (isWSRPContainer()) {
        sb.append(RECRPTS_STRING_CONSTS.viewJobStatus);
    }
    else {
        sb.append(CARO_STRING_CONSTS.viewJobStatus);
    }

    if (jdeMenuParent.isAccessibilityEnabled) {
        sb.append("</span>");
    }
    sb.append("</td></tr></table>");

    if (recRptsObj != undefined && recRptsObj != null) {
        for (var i = 0, len = recRptsObj.length; i < len; i++) {
            var rpt = recRptsObj[i];

            sb.append("<table id=\"RecRptsTable").append(i).append("\" class=\"MenuNormal\" cellspacing=0 border=0 tabIndex=0 onmouseover=\"this.className='HoverMenuItem';\" onmouseout=\"this.className='MenuNormal';\"");
            sb.append("onfocus=\"this.className='HoverMenuItem';\" onblur=\"this.className='MenuNormal';\" onkeydown=\"return doMenuKeyDown(this,event,false,'');\" style=\"cursor: pointer;\"");
            if (jdeMenuParent.isAccessibilityEnabled) {
                sb.append(" role=\"menuitem\" aria-labelledby=\"rpt");
                sb.append(rpt.reportName);
                sb.append("_");
                sb.append(i); //append a index to make sure uniqueness of dom id 
                sb.append("\"");
            }
            var prefix = rpt.reportName + ' ' + rpt.reportVersion;
            var dataValues = ['', rpt.server, ' ', rpt.jobId, rpt.port, '', '1'].join('|');
            if (!isTouchEnabled)
                sb.append(" onClick");
            else
                sb.append(" ontouchend");
            sb.append("=\"javascript:runE1App('P986110B', 'W986110BA', '");
            sb.append(WSJNewVersion);
            sb.append("', '','");
            sb.append(prefix)
            sb.append("', '|4|7|10|11|13|', '");
            sb.append(dataValues);
            sb.append("')\" title=\"");
            sb.append(rpt.description + ' (Submitted: ' + rpt.timeSubmitted + ')');
            sb.append("\">");

            sb.append("<tr><td valign=middle style=\"height:16px;width:16px;\">");
            sb.append("<table cellspacing=0 cellpadding=0><tbody><tr><td>");

            var statusValue = rpt.statusCode.Trim();
            // Report has following status 1) waiting(W) 2) in Queue(S)- not for As 400 3) Processing (P) 4) Done (D) 5) Error (E)
            if (statusValue == "D") {
                sb.append("<span class=\"ReportDoneIcon\">");
            }
            else if (statusValue == "W") {
                sb.append("<span class=\"ReportWaitIcon\">");
            }
            else if (statusValue == "E") {
                sb.append("<span class=\"ReportErrorIcon\">");
            }
            else if (statusValue == "S") {
                sb.append("<span class=\"ReportQueueIcon\">");
            }
            else if (statusValue == "P") {
                sb.append("<span class=\"ReportProcessIcon\">");
            }

            sb.append("<IMG SRC='").append(window["WEBGUIRES_images_spacer_gif"]).append("'  width=16 height=16 alt=\"\">");
            sb.append("</span></td></tr></tbody></table></td>");
            sb.append("<td style=\"width:1px;\">&nbsp;</td>");
            sb.append("<td nowrap id=\"recRptsMenuItem").append(i).append("\" valign=middle ");
            if (!isRTL) {
                sb.append("align=\"left\">");
            }
            else {
                sb.append("align=\"right\">");
            }

            if (jdeMenuParent.isAccessibilityEnabled) {
                sb.append("<span id=\"rpt");
                sb.append(rpt.reportName);
                sb.append("_");
                sb.append(i);
                sb.append("\">");
            }
            sb.append(rpt.reportNameDesc);
            if (jdeMenuParent.isAccessibilityEnabled) {
                sb.append("</span>");
            }
            sb.append("</td></tr></table>");
        }
    }

    recRptsDiv.innerHTML = sb.toString();

    //Initialize onClick event Handler
    InitonClickEventHandler();

    recRptsDiv.className = "showMenu";
    recRptsDiv.style.display = "block";

    applyScrollToTopLevelMenu(DDMENU.CONST.ELEMENT_RECRPTSDIV);

    // add maskFrame to the rec rpts menu in IE
    if (isIE && isActiveXAvailable() && !isWSRPContainer()) {
        showGlobalMaskFrame();
        var menuMaskFrame = new MaskFrame(recRptsDiv.id + '-maskframe');
        addmaskFrameStack(menuMaskFrame);
        showItemShadow(recRptsDiv, menuMaskFrame, true);
    }
    adjustForIEandPortal(recRptsDiv);
}

/* This function will be called everytime the recent reports are updated
* rrData is an array of objects, each of which has the following fields:
*   - server, port, jobId, description, statusCode, statusText, timeSubmitted, timeLastActivity.
*/
function dropdownUpdateRecentReports(rrData, dataSource) {
    //store the rrData in Global recReptsObj
    recRptsObj = rrData;
}

function isPreferenceChecked(prefName) {
    var preferenceObj;
    if (window.modalSSIndex != undefined && modalSSIndex != 0)
        preferenceObj = parent.parent.prefObj.preferences;
    else if (parent.prefObj)
        preferenceObj = parent.prefObj.preferences;
    else if (self.parent.opener != null && !self.parent.opener.closed) //Apps opened in New window 
        preferenceObj = self.parent.opener.prefObj.preferences;

    // fall over in web center
    if (!preferenceObj)
        preferenceObj = window.prefObj.preferences;

    for (i = 0; i < preferenceObj.length; i++) {
        if (preferenceObj[i].name == prefName) {
            if (preferenceObj[i].checked == "true") {
                return true;
            }
            else {
                return false;
            }
        }
    }
}

function changePreference(prefName, val) {
    for (i = 0; i < prefObj.preferences.length; i++) {
        if (prefObj.preferences[i].name == prefName) {
            prefObj.preferences[i].checked = val;
        }
    }
}

function storeAddFavPreferences() {
    var doc = document;
    var favForm = doc.getElementById("favFrom");
    if (favForm.addFavCheckbox.checked == false)     // Not required to turn off the 'Add to Favorite' preference if the 'Do not show window again' is unchecked
    {
        return;
    }

    showPreferences();  // Launching the Preferences Window
    if (doc.getElementById("UI_Pref") != null) {
        prefForm = doc.getElementById("Preferences");
        doc.getElementById("UI_Pref").style.display = 'none';
        removeItemShadow(document.getElementById("UI_Pref"));
        if (prefForm.addFavPreference && prefForm.addFavPreference.checked) {
            prefForm.addFavPreference.checked = false;
            if (isPreferenceChecked("addFavPreference")) {
                changePreference("addFavPreference", "false");
            }
        }
    }
    if ((prefForm.addFavPreference.checked == false) && (isPreferenceChecked("addFavPreference") == false)) // To make sure that the addFavPreference is not checked in prefFrom and in prefObj
    {
        storePreferences();
        javascript: window['UI_Pref'].onClose();
    }
}

function manageFavorites() {
    var title = favoriteStaticText.favorites[0].favManage;
    var ID = '0_1_Fav_Mang';
    var cancelScript = "javascript: window['0_1_Fav_Mang'].onClose();"
    var cancelChildScript = "javascript: cancelChild();"
    var DeleteAllScript = "javascript: doFavoritesAction(null, null, null, null, 'frAll', null);"
    var okScript = "javascript: doFavoritesActionSubmit(form.FavoriteName.value); window['0_1'].onClose();"
    var newFolderScript = "javascript: newFolderScript();"
    var width = 570;
    var height = !isIOS ? 65 : 110;
    var favRowCount = jdeMenuParent.favoriteLabels.length + 1; // Adding 1 for the table header
    var favTableHeight;
    var favRowHeight = 20;
    var emptyRowHeight = 8;
    var favScrollLimit = 11;    // Max no of favorites shown without scrollbar is 10 along with the table header, hence setting favScrollLimit as 11 
    var enableScrollBar = (favRowCount > favScrollLimit) ? true : false;
    if (enableScrollBar) {
        favTableHeight = userAccessibilityMode ? favRowHeight * favScrollLimit : ((favRowHeight + emptyRowHeight) * favScrollLimit) + emptyRowHeight;
    }
    else {
        favTableHeight = userAccessibilityMode ? favRowHeight * favRowCount : ((favRowHeight + emptyRowHeight) * favRowCount) + emptyRowHeight;
    }
    height += favTableHeight;
    var onMouseOverScript = "javascript:className='FAVbuttonMouseOver';"
    var onMouseOutScript = "javascript:className='FAVbutton';"
    var onMouseDownScript = "javascript:className='FAVbuttonMouseDown';"

    var screenWidth = UTIL.pageWidth();
    var screenHeight = UTIL.pageHeight();

    var sb = new PSStringBuffer();
    sb.append("<div align=\"center\" onmousemove = \"stopEventBubbling(event)\"");
    sb.append(" onmouseout = \"manageFavoritesMouseOut(event);\">");
    sb.append("<form id=\"form1\"><div style=\"height:").append(favTableHeight).append("px");
    if (enableScrollBar) {
        sb.append(";overflow:auto;-webkit-overflow-scrolling:touch;");
    }
    sb.append("\">");
    sb.append("<table id=\"FavManageTable\" summary=\"" + favoriteStaticText.favorites[0].favManage + "\" align=center cellpadding=1 cellspacing=\"0.5px\" border=0");
    sb.append(" onmouseout = \"manageFavoritesMouseOut(event);\">");
    sb.append("<tr height = \"").append(favRowHeight).append("px\"><th class=\"GridHeaderCell\" scope=\"col\"><label class=\"FavoritesPopupLabel\">");
    sb.append(favoriteStaticText.favorites[0].favName);
    sb.append("</label></th><th class=\"GridHeaderCell\" scope=\"col\"><label class=\"FavoritesPopupLabel\">");
    sb.append(favoriteStaticText.favorites[0].favType);
    sb.append("</label></th></tr>");
    for (var i = 0; i < jdeMenuParent.favoriteLabels.length; i++) {

        var favoriteGridCell = "";
        var backGourndColor = "";
        if (i % 2 == 0) {
            favoriteGridCell = "FavoriteGridCellEven";
            backGourndColor = "background-color:#F9F9F9";
        }
        else {
            favoriteGridCell = "FavoriteGridCellOdd";
            backGourndColor = "background-color:#F2F2F5";
        }

        if (i == 0 && !userAccessibilityMode) {
            sb.append("<tr style = \"background-color:#F2F2F5\" height = \"").append(emptyRowHeight).append("px\"id = \"emptyRow_");
            sb.append(i + "\" onMouseover=\"highlightFavoriteBorder(");
            sb.append(i + ")\" onMouseout=\"unHighlightFavoriteBorder(");
            sb.append(i + ")\"><td id = \"emptyRowLabel_");
            sb.append(i + "\"></td><td></td></tr>");
        }
        if (jdeMenuParent.favoriteType[i].length > 0 && jdeMenuParent.favoriteLabels[i].length > 0) {
            sb.append("<tr id=\"Row_");
            sb.append(i);
            sb.append("\" height = \"").append(favRowHeight).append("px\" onMouseover=\"highlightFavoriteRow(");
            sb.append(i + ");\" onMouseout=\"unHighlightFavoriteRow(");
            sb.append(i + ");\" ontouchstart=\"highlightFavoriteRow(");
            sb.append(i + ");\" ontouchend=\"unHighlightFavoriteRow(");
            sb.append(i + ");\" ><td width=\"400px\" tabIndex=\"0\" scope=\"row\" align=\"left\" id=\"label_");
            sb.append(i + "\" class=\"");
            sb.append(favoriteGridCell).append("\"");
            if (userAccessibilityMode) {
                sb.append(" onclick=\"favItemClicked(").append(i).append(");\">");
            }
            else {
                sb.append(" onMouseDown=\"favItemMouseDown(event,");
                sb.append(i + "); reOrderFavoritesFrom('");
                sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                if (isTouchEnabled) {
                    sb.append("');\" ontouchstart=\"favItemTouchStart(event,");
                    sb.append(i + "); reOrderFavoritesFrom('");
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                }
                sb.append("');\">");
            }
            switch (jdeMenuParent.favoriteType[i]) {
                case "01": //Interactive Application
                case "02": // Batch Application
                case "07": // Folder
                case "08": // URL
                case "11": // UDC
                case "18": // Workflow
                case "20": // Crystal Report task
                case "22": // User Defined App Form
                case "23": // One View Report
                case "30": // ADF/External Application
                    sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                    break;
                case "21": // User Defined Folder
                    if (!userAccessibilityMode) {
                        sb.append("<label onMouseDown = \"getChildOfUserFolder(event,'");
                        if (isTouchEnabled) {
                            sb.append(jdeMenuParent.favoriteID[i] + "');\" ontouchend=\"getChildOfUserFolder(event,'");
                        }
                        sb.append(jdeMenuParent.favoriteID[i] + "');\" class=\"FavoritesPopupLabel\">");
                        sb.append("<img alt=\"");
                        sb.append(favoriteStaticText.favorites[0].favUserCreatedFolder);
                        sb.append("\" title=\"");
                        sb.append(favoriteStaticText.favorites[0].favUserCreatedFolder);
                        sb.append("\" src=\"");
                        sb.append(window["E1RES_img_FAV_managefavorites_ena_png"]);
                        sb.append("\" onmouseover=\"this.src='");
                        sb.append(window["E1RES_img_FAV_managefavorites_ovr_png"]);
                        sb.append("'\" onmouseout=\"this.src='");
                        sb.append(window["E1RES_img_FAV_managefavorites_ena_png"]);
                        sb.append("'\">");
                    }
                    break;
            }
            sb.append("</label> &nbsp;");
            sb.append("<label style=\"display:inline-block;\" width=\"0px\" id = \"favLabel_");
            sb.append(i + "\">");
            sb.append(jdeMenuParent.favoriteLabels[i]);
            sb.append("</label>");
            sb.append("<div id =\"favLabelDiv_");
            sb.append(i);
            sb.append("\" style=\"display:none;\">");
            sb.append(" </div>");
            if (!jdeMenuParent.favoriteParent) {
                if (userAccessibilityMode) {
                    sb.append("<a  onClick=\"moveFavRowManage(event,'Row_");
                    sb.append(i);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteView[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteLabels[i]);
                    sb.append("');\"/>");
                }
                sb.append("<img alt=\"" + favoriteStaticText.favorites[0].favMoveToolTip + "\" title=\"" + favoriteStaticText.favorites[0].favMoveToolTip + "\" id=\"move_");
                sb.append(i);
                sb.append("\" src=\"");
                sb.append(window["E1RES_img_FAV_move_png"]);
                sb.append("\" align = \"right\" style = \"display:none\" onmouseover=\"showMoveHover('move_");
                sb.append(i);
                sb.append("');\"");
                sb.append(" onmouseout=\"unShowMoveHover('move_");
                sb.append(i);
                sb.append("');\"onMouseDown=\"moveFavRowManage(event,'Row_");
                sb.append(i);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteView[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteLabels[i]);
                if (isTouchEnabled) {
                    sb.append("');\" ontouchend=\"moveFavRowManage(event,'Row_");
                    sb.append(i);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteView[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteLabels[i]);
                }
                sb.append("');\"/>");
            }
            if (userAccessibilityMode) {
                if (!jdeMenuParent.favoriteParent)
                    sb.append("</a>"); // Closing Move anchor
                if (jdeMenuParent.favoriteType[i] == "21") {
                    sb.append("<a onclick=\"getChildOfUserFolder(event,'");
                    sb.append(jdeMenuParent.favoriteID[i] + "');\" class=\"FavoritesPopupLabel\">");
                    sb.append("<img alt=\"");
                    sb.append(jdeMenuParent.favoriteLabels[i] + " " + favoriteStaticText.favorites[0].favUserCreatedFolder);
                    sb.append("\" title=\"");
                    sb.append(favoriteStaticText.favorites[0].favUserCreatedFolder);
                    sb.append("\" src=\"");
                    sb.append(window["E1RES_img_FAV_managefavorites_ena_png"]);
                    sb.append("\" onmouseover=\"this.src='");
                    sb.append(window["E1RES_img_FAV_managefavorites_ovr_png"]);
                    sb.append("'\" onmouseout=\"this.src='");
                    sb.append(window["E1RES_img_FAV_managefavorites_ena_png"]);
                    sb.append("'\">");
                    sb.append("</a>");
                }
                sb.append("<a  onClick=\"renameChangeContent(event,'favLabel_");
                sb.append(i);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteView[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                sb.append("','");
                sb.append(escape(jdeMenuParent.favoriteLabels[i]));
                sb.append("','");
                sb.append("favLabelDiv_");
                sb.append(i);
                sb.append("');\"/>");
            }
            sb.append("<img alt=\"" + favoriteStaticText.favorites[0].favRename + "\" title=\"" + favoriteStaticText.favorites[0].favRename + "\" id=\"rename_");
            sb.append(i);
            sb.append("\" src=\"");
            sb.append(window["E1RES_img_FAV_edit_all_ena_png"]);
            sb.append("\" align = \"right\" style = \"display:none\" onmouseover=\"showRenameHover('rename_");
            sb.append(i);
            sb.append("');\"");
            sb.append(" onmouseout=\"unShowRenameHover('rename_");
            sb.append(i);
            sb.append("');\" onMouseDown=\"renameChangeContent(event,'favLabel_");
            sb.append(i);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteID[i]);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteParentTaskID[i]);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteView[i]);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
            sb.append("','");
            sb.append(escape(jdeMenuParent.favoriteLabels[i]));
            sb.append("','");
            sb.append("favLabelDiv_");
            sb.append(i);
            if (isTouchEnabled) {
                sb.append("');\" ontouchend=\"renameChangeContent(event,'favLabel_");
                sb.append(i);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteView[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                sb.append("','");
                sb.append(escape(jdeMenuParent.favoriteLabels[i]));
                sb.append("','");
                sb.append("favLabelDiv_");
                sb.append(i);
            }
            sb.append("');\"/>");
            if (userAccessibilityMode) {
                sb.append("</a>"); // Closing Rename anchor
                sb.append("<a  onClick=\"deleteFavRowManage(event,'Row_");
                sb.append(i);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteView[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                sb.append("','");
                sb.append(escape(jdeMenuParent.favoriteLabels[i]));
                sb.append("');\"/>");
            }
            sb.append("<img alt=\"" + favoriteStaticText.favorites[0].favDelete + "\" title=\"" + favoriteStaticText.favorites[0].favDelete + "\" id=\"delete_");
            sb.append(i);
            sb.append("\" src=\"");
            sb.append(window["E1RES_img_FAV_deletefavorites_ena_png"]);
            sb.append("\" align = \"right\" style = \"display:none\" onmouseover=\"showDeleteHover('delete_");
            sb.append(i);
            sb.append("');\"");
            sb.append(" onmouseout=\"unShowDeleteHover('delete_");
            sb.append(i);
            sb.append("');\" onMouseDown=\"deleteFavRowManage(event,'Row_");
            sb.append(i);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteID[i]);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteParentTaskID[i]);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteView[i]);
            sb.append("','");
            sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
            sb.append("','");
            sb.append(escape(jdeMenuParent.favoriteLabels[i]));
            if (isTouchEnabled) {
                sb.append("');\" ontouchend=\"deleteFavRowManage(event,'Row_");
                sb.append(i);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteView[i]);
                sb.append("','");
                sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                sb.append("','");
                sb.append(escape(jdeMenuParent.favoriteLabels[i]));
            }
            sb.append("');\"/>");
            if (userAccessibilityMode) {
                sb.append("</a>"); // Closing Delete anchor
                if (jdeMenuParent.favoriteType[i] != "21" && jdeMenuParent.favoriteParent) // ReOrganize Option not applicable for UDF and Favorites inside an UDF
                {
                    sb.append("<a onclick=\"showUDFSelWindow(event,'Row_");   // Anchor for Reorganize
                    sb.append(i);
                    sb.append("',");
                    sb.append(i);
                    sb.append(",'");
                    sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteView[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                    sb.append("');\"/>");
                    sb.append("<img alt=\"" + favoriteStaticText.favorites[0].favReorganize + "\" title=\"" + favoriteStaticText.favorites[0].favReorganize + "\" id=\"reorganize_");
                    sb.append(i);
                    sb.append("\" src=\"");
                    sb.append(window["E1RES_img_FAV_reorganizefavorites_ena_png"]);
                    sb.append("\" align = \"right\" style = \"display:none\"/>");
                    sb.append("</a>"); // Closing reOrder anchor  
                }

                if (i != jdeMenuParent.favoriteLabels.length - 1) // MoveDown Option not applicable for the First Favorite
                {
                    sb.append("<a onclick=\"reOrderFavorites('");  // Anchor for Move Down
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i + 1]);
                    sb.append("');\">");
                    sb.append("<img alt=\"" + favoriteStaticText.favorites[0].favMoveDown + "\" title=\"" + favoriteStaticText.favorites[0].favMoveDown + "\" id=\"movedown_");
                    sb.append(i);
                    sb.append("\" src=\"");
                    sb.append(window["E1RES_img_FAV_movedownfavorites_ena_png"]);
                    sb.append("\" align = \"right\" style = \"display:none\"/>");
                    sb.append("</a>"); // Closing Move Down anchor  
                }

                if (i != 0) // MoveUp Option not applicable for the Last Favorite
                {
                    sb.append("<a onclick=\"reOrderFavorites('");   // Anchor for MoveUp
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteParentTaskID[i]);
                    sb.append("','");
                    sb.append(jdeMenuParent.favoriteSequenceNumber[i - 1]);
                    sb.append("');\"/>");
                    sb.append("<img alt=\"" + favoriteStaticText.favorites[0].favMoveUp + "\" title=\"" + favoriteStaticText.favorites[0].favMoveUp + "\" id=\"moveup_");
                    sb.append(i);
                    sb.append("\" src=\"");
                    sb.append(window["E1RES_img_FAV_moveupfavorites_ena_png"]);
                    sb.append("\" align = \"right\" style = \"display:none\"/>");
                    sb.append("</a>"); // Closing MoveUp anchor  
                }
            }
            sb.append("</td><td width=\"170px\" id = \"typeLabel_");
            sb.append(i);
            sb.append("\" align=\"left\" class=\"");
            sb.append(favoriteGridCell);
            sb.append("\">");
            switch (jdeMenuParent.favoriteType[i]) {
                case "01": //Interactive Application
                    sb.append(favoriteStaticText.favorites[0].favInteractiveApplication);
                    break;
                case "02": // Batch Application
                    sb.append(favoriteStaticText.favorites[0].favBatchApplication);
                    break;
                case "07": // Folder
                    sb.append(favoriteStaticText.favorites[0].favFolder);
                    break;
                case "08": // URL
                    sb.append(favoriteStaticText.favorites[0].favURL);
                    break;
                case "11": // URL
                    sb.append(favoriteStaticText.favorites[0].favUDC);
                    break;
                case "18": // URL
                    sb.append(favoriteStaticText.favorites[0].favWorkflow);
                    break;
                case "20": // Crystal Report task
                    sb.append(favoriteStaticText.favorites[0].favCrystalReport);
                    break;
                case "21": // User Created Folder
                    sb.append(favoriteStaticText.favorites[0].favUserCreatedFolder);
                    sb.append(" <div id = \"favType_");
                    sb.append(i + "\" display = \"none\"></div>");
                    break;
                case "22": // User Defined App Form
                    sb.append(favoriteStaticText.favorites[0].favUserDefinedAppForm);
                    break;
                case "23": // One View Report
                    sb.append(favoriteStaticText.favorites[0].favOneView);
                    break;
                case "30": // ADF/External App
                    sb.append(favoriteStaticText.favorites[0].favExternalApplication);
                    break;
            }
            sb.append(" </center></td></tr>");
        }
        if (!userAccessibilityMode) {
            sb.append("<tr style = \"");
            sb.append(backGourndColor);
            sb.append("\" height = \"").append(emptyRowHeight).append("px\" id = \"emptyRow_");
            sb.append((i + 1) + "\" onMouseover=\"highlightFavoriteBorder(");
            sb.append((i + 1) + ")\" onMouseout=\"unHighlightFavoriteBorder(");
            sb.append((i + 1) + ")\"><td id = \"emptyRowLabel_");
            sb.append((i + 1) + "\"></td><td></td></tr>");
        }
    }
    sb.append("</table></div>");
    sb.append("<table onmouseup = \"buttonAreaMouseUp(event);\"");
    if (isIOS) {
        sb.append(" onTouchStart=\"stopEventBubbling(event);\"");
    }
    sb.append(" align=\"right\" border=0>");
    sb.append("<tr><td width=\"570px\" align=\"right\" colspan = 5>");
    if (jdeMenuParent.favoriteParent) {
        sb.append("&nbsp;<input type=\"button\" id=\"DeleteAll\" class=\"button\" size=26 MAXLENGTH=80 onmouseover=\"" + onMouseOverScript + "\" onmouseout=\"" + onMouseOutScript + "\" onmousedown=\"" + onMouseDownScript + "\" value=\"");
        sb.append(favoriteStaticText.favorites[0].favDeleteAll);
        sb.append("\" onClick=\"");
        sb.append(DeleteAllScript);
        if (isIOS) {
            sb.append("\" onTouchStart=\"");
            sb.append(DeleteAllScript);
        }
        sb.append("\"></input>");
        sb.append("&nbsp;");
        sb.append("<input type=\"button\" id=\"NewFolder\" class=\"button\" size=26 MAXLENGTH=80 onmouseover=\"" + onMouseOverScript + "\" onmouseout=\"" + onMouseOutScript + "\" onmousedown=\"" + onMouseDownScript + "\" value=\"");
        sb.append(favoriteStaticText.favorites[0].favNewFolder);
        sb.append("\" onClick=\"");
        sb.append(newFolderScript);
        if (isIOS) {
            sb.append("\" onTouchStart=\"");
            sb.append(newFolderScript);
        }
        sb.append("\"></input>&nbsp;");
    }
    sb.append("<input type=\"button\" id=\"Cancel\" class=\"button\" size=26 MAXLENGTH=80 onmouseover=\"" + onMouseOverScript + "\" onmouseout=\"" + onMouseOutScript + "\" onmousedown=\"" + onMouseDownScript + "\" value=\"");
    sb.append(favoriteStaticText.favorites[0].favClose);
    sb.append("\" onClick=\"");
    if (!jdeMenuParent.favoriteParent) {
        cancelScript = cancelChildScript;
    }
    sb.append(cancelScript);
    if (isIOS) {
        sb.append("\" onTouchStart=\"");
        sb.append(cancelScript);
    }
    sb.append("\"> </input>&nbsp;");
    if (!jdeMenuParent.favoriteParent) {
        sb.append("<input type=\"button\" id=\"back\" class=\"button\" size=26 MAXLENGTH=80 onmouseover=\"" + onMouseOverScript + "\" onmouseout=\"" + onMouseOutScript + "\" onmousedown=\"" + onMouseDownScript + "\" value=\"Back\" onClick=\"javascript: showAllFavorites();");
        if (isIOS) {
            sb.append("\" onTouchStart=\"");
            sb.append("javascript: showAllFavorites();");
        }
        sb.append("\"> </input>");
    }
    sb.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>");
    sb.append("</table></form></div>");

    var left = (screenWidth / 2) - (width / 2);
    var top = (screenHeight / 2) - (height / 2);

    createPopup(ID, SHOW_POPUP_FAV, true, false, true, false, title, null, null, null, null, null, left, top, width, height, null, sb);

    moveFocusToCloseForPopupWindow(ID, null);

    if (jdeMenuParent.favoriteParent && jdeMenuParent.favoriteLabels.length == 0) {
        document.getElementById("DeleteAll").disabled = true;
    }
}

function showAllFavorites() {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "showAllFavorites";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
}

function addFormToFavorites(nameSpace) {
    var e1FormDTAFactory = window.document.getElementById('e1menuAppIframe').contentWindow.JDEDTAFactory;
    if (e1FormDTAFactory != null) {
        e1FormDTAFactory.getInstance(nameSpace).post("form:AddFavorite");
    }
}

function cancelChild() {
    window['0_1_Fav_Mang'].onClose();
    document.getElementById(DDMENU.CONST.ELEMENT_MANAGE_FAV).style.display = 'none';
    showAllFavorites();
}

function getChildOfUserFolder(ev, id) {
    var doc = document;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_ACTION).value = "expandUserFolder";
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_PARENTTASKID).value = id;
    doc.getElementById(DDMENU.CONST.ELEMENT_MANAGEFAV_FORM).submit();
    stopEventBubbling(ev);
}

function isRightClickPrefCheckEnabled() {
    var rightClickPrefCheck = true;
    if (window.modalSSIndex != undefined && modalSSIndex != 0)
        rightClickPrefCheck = parent.parent.isPreferenceChecked("rowExitPreference");
    else if (parent.prefObj)
        rightClickPrefCheck = parent.isPreferenceChecked("rowExitPreference");
    else if (self.parent.opener != null && !self.parent.opener.closed) //app opened in New window 
        rightClickPrefCheck = self.parent.opener.isPreferenceChecked("rowExitPreference");

    return rightClickPrefCheck;
}

function updateAppFrameHeight() {
    var frameDiv = top.document.getElementById("innerRCUX");
    var appFrame = top.document.getElementById("e1menuAppIframe");
    if (appFrame && frameDiv) {
        if (appFrame.offsetHeight > frameDiv.offsetHeight) {
            appFrame.style.height = '';
        }
    }
}

//////////////////////////////////////
// Singleton Touch Handler
///////////////////////////////////////
var DROPDOWNMENUTOUCH;
if (DROPDOWNMENUTOUCH == null) {
    DROPDOWNMENUTOUCH = new function () {
        this.nbrFingers = 0;
        this.startTouchEvent = false;
        this.moveTouchEvent = false;
        this.endTouchEvent = false;
        this.cancelTouchEvent = false;
    }
}
DROPDOWNMENUTOUCH.OCL = 2;
DROPDOWNMENUTOUCH.TASK = 1;
DROPDOWNMENUTOUCH.touchMenuStart = function (e, cmType, popupContentDivId, menuItemCount, eventElementId) {
    var menuTouch = DROPDOWNMENUTOUCH;
    if (menuTouch.startTouchEvent == true) {
        return;
    }
    // a single finger touch
    if (e.touches.length == 1) {
        window.setTimeout(function () { DROPDOWNMENUTOUCH.openContextMenu(e, cmType, popupContentDivId, menuItemCount, eventElementId) }, 500);
    }
    else {
        menuTouch.clearTouch(e);
        return;
    }
    menuTouch.nbrFingers = e.touches.length;
    menuTouch.startTouchEvent = true;
}
DROPDOWNMENUTOUCH.touchMenuMove = function (e) {
    var menuTouch = DROPDOWNMENUTOUCH;
    menuTouch.moveTouchEvent = true;
    menuTouch.clearTouch(e);
}
DROPDOWNMENUTOUCH.touchMenuEnd = function (e) {
    var menuTouch = DROPDOWNMENUTOUCH;
    menuTouch.endTouchEvent = true;
    menuTouch.clearTouch(e);
}
DROPDOWNMENUTOUCH.touchMenuCancel = function (e) {
    var menuTouch = DROPDOWNMENUTOUCH;
    menuTouch.cancelTouchEvent = true;
    menuTouch.clearTouch(e);
}
DROPDOWNMENUTOUCH.openContextMenu = function (e, cmType, popupContentDivId, menuItemCount, eventElementId) {
    var menuTouch = DROPDOWNMENUTOUCH;
    if (!menuTouch.moveTouchEvent && !menuTouch.endTouchEvent && !menuTouch.cancelTouchEvent && menuTouch.startTouchEvent && menuTouch.nbrFingers == 1) {
        e.preventDefault();
        if (cmType == DROPDOWNMENUTOUCH.TASK) {
            displayTaskContextMenu(e, popupContentDivId, menuItemCount, eventElementId);
        }
        window.scrollTo(window.screenX + 1, window.screenY + 1);
    }
    menuTouch.clearTouch(e);
}
DROPDOWNMENUTOUCH.clearTouch = function (e) {
    DROPDOWNMENUTOUCH.nbrFingers = 0;
    DROPDOWNMENUTOUCH.startTouchEvent = false;
    DROPDOWNMENUTOUCH.moveTouchEvent = false;
    DROPDOWNMENUTOUCH.endTouchEvent = false;
    DROPDOWNMENUTOUCH.cancelTouchEvent = false;
    e.stopPropagation();

}

///////////////////////////////////////////////
// Breadcrumbs 
///////////////////////////////////////////////

var BREADCRUMBS;
if (!BREADCRUMBS) {
    BREADCRUMBS = new Object();

    BREADCRUMBS.CONSTANTS = new Object();
    BREADCRUMBS.CONSTANTS.SERVICE_ID_BREADCRUMBS = "BreadcrumbService";
    BREADCRUMBS.CONSTANTS.SERVICE_ACTION_BREADCRUMB_LOAD_NODE_MENU = "loadMenu";
    BREADCRUMBS.CONSTANTS.SERVICE_ACTION_BREADCRUMB_REFRESH_BC_CONTENT = "fullRefresh";
    BREADCRUMBS.CONSTANTS.SERVICE_ACTION_SELECT_BREADCRUMB = "selectBreadcrumb";
    BREADCRUMBS.CONSTANTS.SERVICE_ACTION_CLEAR_BREADCRUMB_HISTORY = "clearBreadcrumbHistory";
    BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SERVICE_ACTION = "serviceAction";
    BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID = "menuNodeInstanceId";
    BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SHOULD_UPDATE_CONTENT = "updateBreadcrumbContent";
    BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_HTML_CONTENT = "breadcrumbHTML";
    BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_ACTIVE_COMPONENT_ID = "activeCompId";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_BAR = "breadcrumbBarDiv";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV = "e1BreadcrumbBar";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_TABLE = "breadcrumbBar";
    BREADCRUMBS.CONSTANTS.ELEMENT_CARO_CONTAINER_ID = "caroHolder";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_KEYHOST_ID_BASE = "breadcrumb";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE = "LabelHost";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_MENUNODE_ID_BASE = "node";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_SUBMENU_INDICATOR_TAG = "IMG";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE = "bcroot";
    BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITH_BC = "71px";
    BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITH_BC_FOR_NATIVE_CONTAINER = "5px";
    BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITHOUT_BC = "43px";
    BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITHOUT_BC_FOR_NATIVE_CONTAINER = "5px";
    BREADCRUMBS.CONSTANTS.STYLE_VALUE_TOPMENU_CONTAINER_HEIGHT_WITH_BC = "24px";
    BREADCRUMBS.CONSTANTS.STYLE_VALUE_TOPMENU_CONTAINER_HEIGHT_WITHOUT_BC = "30px";
    BREADCRUMBS.CONSTANTS.SERVICE_ACTION_BREADCRUMB_LOAD_HISTORY = "loadHistory"; //added
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_ITEM_ID_BASE = "HistoryBase";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE = "bcHistoryRoot";
    BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ARROW = "bcDropdownArrow";
    BREADCRUMBS.CONSTANTS.ELEMENT_SHADOW_PROVIDER_WHEN_NO_BREADCRUMBS = "brandingBarShadowProvider";
}

BREADCRUMBS.createServiceUrlFactory = function () {
    var e1UrlCreator = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);
    e1UrlCreator.setURI(BREADCRUMBS.CONSTANTS.SERVICE_ID_BREADCRUMBS);
    return e1UrlCreator;
}

BREADCRUMBS.getServiceURL = function () {
    var urlFactory = BREADCRUMBS.createServiceUrlFactory();
    return urlFactory.toString();
}

BREADCRUMBS.callService = function (params) {
    try {
        var sb = "<html><body><form id=\"breadcrumbServiceSubmissionForm\" method=post action=\"";
        sb += BREADCRUMBS.getServiceURL();
        sb += "\">\n";
        for (var i = 0; i < params.length; i++) {
            sb += "<input type=hidden name="; sb += params[i].name; sb += " value=\""; sb += params[i].value; sb += "\">\n";
        }
        sb += "<\/form><\/body><\/html>";

        var frm = null;
        frm = document.getElementById("e1BreadcrumbIframe").contentWindow;
        if (!frm) {
            frm = window.frames["e1BreadcrumbIframe"];
        }

        var newDoc = frm.document;
        newDoc.open("text/html");
        newDoc.charset = "UTF-8";
        newDoc.write(sb);
        newDoc.close();

        var bcServiceForm = frm.document.getElementById("breadcrumbServiceSubmissionForm");
        // WebCenter form.submit() takes the window.event target as the submitted element,
        // or assumes the clicked target is within the form.  This isn't the case here
        // so we have to bypass that logic and call into adfppSub() directly.
        if (isWebCenter) {
            bcServiceForm.target = "e1BreadcrumbIframe";
            window.adfppSub(window.event, bcServiceForm, "", false);
        }
        else {
            bcServiceForm.submit();
        }
    }
    catch (problem) {
        LOGGER.log('Breadcrumb form submission error.');
    }
}

BREADCRUMBS.updateBreadcrumbHistory = function (newContent, showMenu) {

    if (newContent != "") {
        breadcrumbHistoryHtml = newContent;
    }

    if (showMenu) {
        InitonClickEventHandler();
        hideAllDropdownMenus();

        var doc = document;
        var bchist = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
        bchist.className = "showMenu";
        bchist.style.top = getAbsoluteTopPos(doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV)) + 15 + "px";
        bchist.innerHTML = newContent;

        if (isIE && isActiveXAvailable()) {
            // add the Maskframe to cover the application
            showGlobalMaskFrame();
        }
    }
}

BREADCRUMBS.updateSelectedBreadcrumb = function (activeApplicationNodeId) {
    var actionParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SERVICE_ACTION, BREADCRUMBS.CONSTANTS.SERVICE_ACTION_SELECT_BREADCRUMB);
    var nodeIdParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID, activeApplicationNodeId);
    var params = new Array(actionParam, nodeIdParam);
    BREADCRUMBS.callService(params);
    document.getElementById('bchistory').focus();
}



BREADCRUMBS.updateBreadcrumbContent = function (newContent, showDropdownArrow) {
    try {
        var doc = document;
        hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
        var breadcrumbContainerDiv = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DIV);

        if (!breadcrumbContainerDiv) {
            // We'll hit this if breadcrumbs are disabled at the 
            // system level.
            return;
        }

        if (showDropdownArrow) {
            doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ARROW).style.display = 'block';
        }

        breadcrumbContainerDiv.innerHTML = (!newContent) ? "" : newContent;
    }
    catch (problem) {
        LOGGER.log("error updating breadcrumb content");
    }
}

BREADCRUMBS.refresh = function (activeApplicationNodeId) {
    var actionParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SERVICE_ACTION, BREADCRUMBS.CONSTANTS.SERVICE_ACTION_BREADCRUMB_REFRESH_BC_CONTENT);
    var nodeIdParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID, activeApplicationNodeId);
    var params = new Array(actionParam, nodeIdParam);
    BREADCRUMBS.callService(params);
}

BREADCRUMBS.loadMenu = function (dropdownMenuNodeId, menuRootId) {
    var doc = document;
    var menuRootElement = doc.getElementById(menuRootId);
    if ((!menuRootElement) || (menuRootElement.children.length < 1)) {
        var actionParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SERVICE_ACTION, BREADCRUMBS.CONSTANTS.SERVICE_ACTION_BREADCRUMB_LOAD_NODE_MENU);
        var nodeIdParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID, dropdownMenuNodeId);
        var params = new Array(actionParam, nodeIdParam);
        BREADCRUMBS.callService(params);
        // The way the breadcrumb structure was built, it is relying on the
        // click event to get propagated from the <div> to the <td> element.
        // For WebCenter, the click event is canceled during the submit,
        // so we need to force it to run on the parent <td>.
        // NOTE: It would probably be better to fix this by fixing the DOM
        // structure, but this is a less intrusive fix for now.
        if (isWebCenter) {
            var bcId = menuRootId.substring(6);
            doc.getElementById("breadcrumb" + bcId).click();
        }
    }
}

BREADCRUMBS.loadBreadCrumbsHistory = function (event) {
    var bcroot = document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
    if (bcroot && bcroot.className == "showMenu") {
        bcroot.className = "hideMenu";
        return;
    }

    if (breadcrumbHistoryHtml == "") {
        var actionParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SERVICE_ACTION, BREADCRUMBS.CONSTANTS.SERVICE_ACTION_BREADCRUMB_LOAD_HISTORY);
        var params = new Array(actionParam);
        BREADCRUMBS.callService(params);
    }
    else {
        BREADCRUMBS.updateBreadcrumbHistory(breadcrumbHistoryHtml, true);
        event.returnValue = false;
        // event.preventDefault();
        event.cancelBubble = true;
    }
}
BREADCRUMBS.clearBreadcrumbHistory = function () {

    document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ARROW).style.display = 'none';
    var actionParam = new BREADCRUMBSERVICEPARAM(BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_SERVICE_ACTION, BREADCRUMBS.CONSTANTS.SERVICE_ACTION_CLEAR_BREADCRUMB_HISTORY);
    var params = new Array(actionParam);
    BREADCRUMBS.callService(params);

}
BREADCRUMBS.showBreadcrumbBar = function (visible) {
    var doc = document;
    var caroContainer = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_CARO_CONTAINER_ID);
    var breadcrumbBarDiv = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_BAR);
    var shadowProviderForNoBreadcrumbs = doc.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_SHADOW_PROVIDER_WHEN_NO_BREADCRUMBS);

    if (breadcrumbBarDiv) {
        if (visible == true) {
            breadcrumbBarDiv.style.display = "block";
            if (shadowProviderForNoBreadcrumbs)
                shadowProviderForNoBreadcrumbs.style.display = "none";
            // marginTop for breadcrumbs is not set correctly on IE8 unles we re-render the topnav
            if (isIE8OrEarlier) {
                // 'topnav' element doesn't exist on portals - we might be able to find a better element to use
                var topnav = doc.getElementById("topnav");
                if (topnav)
                    UTIL.forceRedrawIEOnly(topnav);
            }
        }
        else {
            breadcrumbBarDiv.style.display = "none";
            if (shadowProviderForNoBreadcrumbs)
                shadowProviderForNoBreadcrumbs.style.display = "";
        }
    }
    if (caroContainer) {
        if (visible == true) {
            if (isNativeContainer) {
                caroContainer.style.top = BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITH_BC_FOR_NATIVE_CONTAINER;
            }
            else {
                caroContainer.style.top = BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITH_BC;
            }
        }
        else {
            if (isNativeContainer) {
                caroContainer.style.top = BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITHOUT_BC_FOR_NATIVE_CONTAINER;
            }
            else {
                caroContainer.style.top = BREADCRUMBS.CONSTANTS.STYLE_VALUE_CARO_CONTAINER_HEIGHT_WITHOUT_BC;
            }
        }
    }
    else if (isWSRPContainer()) {
        var e1menuAppIframediv = doc.getElementById("e1menuAppIframediv");
        var e1menuAppIframe = doc.getElementById("e1menuAppIframe");
        // This checks specifically for WebCenter.
        if (e1menuAppIframediv && e1menuAppIframediv.className == "isWebCenter") {
            var prevFrameHeight = parseInt(e1menuAppIframe.style.height);
            if (visible) {
                e1menuAppIframediv.style.top = (e1menuAppIframediv.offsetTop + 27) + "px";
                e1menuAppIframe.style.height = (prevFrameHeight - 27) + "px";
            }
            else {
                e1menuAppIframediv.style.top = "";
                e1menuAppIframe.style.height = (prevFrameHeight + 27) + "px";
            }
        }
    }
}

function BREADCRUMBSERVICEPARAM(name, value) {
    this.name = name;
    this.value = value;
}

/* This function is called before the logout iframe redirects to the logout URL.
* It is important to note that this function needs to return true in order for 
* the redirect to happen as necessary.  Add in any tasks that should happen 
* client-side before executing the logout. 
*/
function doE1PreLogout(event) {
    try {
        if (window.WLIST && window.WLIST.killRefreshThreads) {
            window.WLIST.killRefreshThreads();
        }

        var OVRLogoutURL = CookieUtil.GetCookie("EURLogoutURL");
        if (OVRLogoutURL) {
            document.cookie = "EURLogoutURL=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
            window.EURLogoutWin = window.open(OVRLogoutURL, "EURLogoutWin", "toolbar=no,scrollbars=yes,resizable,left=0px,top=0px,width=10px,height=10px");
            window.EURLogoutWin.blur();
            window.EURLogoutWin.close();
        }
        onCloseOrphanWindows();
    }
    catch (excp) {
    }

    UTIL.stopEventBubbling(event);
    return true;
}

////////////////////////
// LOGGER 
////////////////////////

var LOGGER;
if (!LOGGER) {
    LOGGER = new Object();
}

LOGGER.log = function (message) {
    if (window.console && window.console.log) {
        console.log(message);
    }
}

/* Mimicks a fastpath call to the given string.  In general, this function 
* will still work, even if the fastpath is disabled.  The one exception is
* in fastpathing to a task folder.  In this case, the function will be a
* no-op when fastpath is blocked.
*/
function doFastPath(str, e1c, menuContext) {
    var doc = document;
    var fastPathIFrame = doc.getElementById('E1MFastpathHiddenIframe');
    if (!fastPathIFrame) {
        // If fastpath is disabled, we can hijack the OCL iframe.
        fastPathIFrame = doc.getElementById('SilentOCLIFrame');

        // The OCL iframe gets dummied out between uses anyway, so this won't interfer with 
        // OCL at all.  The one downside is that using this to fastpath to G01 for instance
        // will not work because the javascript to handle that looks specifically for
        // the fastpath iframe.  
    }

    if (!fastPathIFrame) {
        // Failsafe: this shouldn't happen, but abort if it does.
        return;
    }

    var url =
	jdeMenuParent.fastPathServiceUrl +
	'&timestamp=' + (new Date()).getTime() +
	'&a=' + encodeURIComponent(str);
    if (null != e1c) {
        url += '&e1c=' + e1c;
    }
    if (menuContext !== undefined) {
        url += '&lpContext=' + menuContext;
    }

    // The timestamp is included to circumvent browser caching by forcing an always unique url
    fastPathIFrame.src = url;
}


/////////////////////////////////
//Adds methods to core Array class
//////////////////////////////////
/*
Checks if this array contains an item
*/
Array.prototype.contains = function (item) {
    for (var i = 0; i < this.length; i++) {
        if (item == this[i]) {
            return true;
        }
    }

    return false;
}

/*
returns the index of this item
*/
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (elt /*, from*/) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

function setNativeContainerRedirect() {
    if (isIOS) {
        if ((prefObj.toApp == null) || (prefObj.toApp == undefined)) {
            prefObj.toApp = true;
        }

        var cookieUtility = new jdeMenu();
        var currentValue = cookieUtility.getCookie("jdeE1OpenIniPadContainer");
        prefObj.toApp = (currentValue == "true") ? false : true;

        var nativeContainerRedirectPreferencButton = document.getElementById('PrefChangeNativeContainerLinkButton');
        if (nativeContainerRedirectPreferencButton) {
            if (prefObj.toApp == true) {
                nativeContainerRedirectPreferencButton.value = (prefText) ? prefText.label[0].linkRedirectToWebText : "Open Links on This Device in Web Browser";
            }
            else {
                nativeContainerRedirectPreferencButton.value = (prefText) ? prefText.label[0].linkRedirectToAppText : "Open Links on This Device in EnterpriseOne for iPad";
            }
        }
        switchPrefStatus('PrefChangeNativeContainerLinkButton');
    }
}

function clearBcHistoryButtonReset() {

    var clearButton = document.getElementById('clearBcHistory');
    if (clearButton) {
        clearButton.value = (clearButton.value.indexOf(prefText.label[0].undoButtonText) != -1) ? prefText.label[0].clearBreadcrumbHistoryText : prefText.label[0].undoButtonText + ' ' + prefText.label[0].clearBreadcrumbHistoryText;
        clearBreadcrumbHistory = !clearBreadcrumbHistory;
    }
    switchPrefStatus('clearBcHistory');
}

function resetDeviceSpecificE1Pages() {
    var resetButton = document.getElementById('resetDeviceSpecificPages');
    if (resetButton) {
        resetButton.value = (resetButton.value.indexOf(prefText.label[0].undoButtonText) != -1) ? prefText.label[0].resetDeviceSpecificE1PagesText : prefText.label[0].undoButtonText + ' ' + prefText.label[0].resetDeviceSpecificE1PagesText;
        resetDeviceSpecificPages = !resetDeviceSpecificPages;
    }
    switchPrefStatus('resetDeviceSpecificPages');
}

/*
* Returns the source element based on the particular browser's appropriate value.
*/
function getEventSourceElement(event) {
    var sourceElement;

    if (document.documentMode !== undefined) // IE
    {
        if (event.target === undefined || event.target == null) // IE8 or menu load is delayed making the target unavailable.
        {
            sourceElement = event.srcElement;
        }
        else {
            sourceElement = event.target;
        }
    }
    else // Non-IE
    {
        sourceElement = event.target;
    }

    return sourceElement;
}

function showUserAndEnvDropdownMenu() {
    var doc = document;
    var userMenu = doc.getElementById('userSessionDropdown');

    if (userMenu) {
        hideAllDropdownMenus();
        userMenu.style.display = 'block';
        userMenu.shown = true;
        if (!isSimplifiedNavigation) {
            showMenu(DDMENU.CONST.ELEMENT_ROLETABLE, null);
        }
        showMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV, null);
        doc.getElementById('userAndEnvContainer').setAttribute('menuExpanded', 'true');
    }
}

function hideUserAndEnvDropdownMenu() {
    var doc = document;
    var userMenu = doc.getElementById('userSessionDropdown');

    if (userMenu) {
        userMenu.style.display = '';
        userMenu.shown = false;
        doc.getElementById('userAndEnvContainer').setAttribute('menuExpanded', 'false');
    }
}

function toggleShowUserAndEnvDropdownMenu() {
    var userMenu = document.getElementById('userSessionDropdown');
    if (userMenu) {
        if (userMenu.shown) {
            hideUserAndEnvDropdownMenu()
        }
        else {
            showUserAndEnvDropdownMenu();
        }
    }
}


/* Function to dynamically repositions items along the top bar if necessary 
* to accomodate small monitor/window widths.  Should be run once onload and
* again on window resize
*/
function reSpaceTopBar() {
    var doc = document;
    /* First time, gather some data about the preferred sizes of various objects. */
    if (!window.idealSizes) {
        var ideal = window.idealSizes = new Object();

        ideal.menuContainerId = 'menuContainer';
        ideal.inSingleAppInNewWindowMode = false;
        if (document.body.parentNode.getAttribute('isAppOpenedInNewWindow') != "false") {
            ideal.inSingleAppInNewWindowMode = true;
            ideal.menuContainerId = 'helpAndBackToE1MenuLink';
        }

        ideal.oraBranding = doc.getElementById('topnavOracleBranding').offsetWidth;
        ideal.menuButtonMin = 0;
        ideal.menuButton = 0;
        ideal.numMenuButtons = 1;
        ideal.menuButtonInBetweens = 0;
        var menuContainer = doc.getElementById(ideal.menuContainerId);
        if (menuContainer) {
            ideal.menuButton = menuContainer.children[0].offsetWidth;
            ideal.menuButtonMin = 24;
            ideal.numMenuButtons = doc.getElementById(ideal.menuContainerId).children.length;

            if (!ideal.inSingleAppInNewWindowMode) {
                ideal.numMenuButtons--;  // One of these is just a <script> tag.
            }
            ideal.menuButtonInBetweens = menuContainer.offsetWidth - ideal.menuButton * ideal.numMenuButtons;
            if (ideal.inSingleAppInNewWindowMode) {
                ideal.menuButtonInBetweens = 3;
            }
        }
        ideal.menuPadding = 60;
        ideal.menuPaddingMin = 10;

        ideal.userName = doc.getElementById('userAndEnvContainer').offsetWidth;
        ideal.userNameSoftMin = Math.min(200, ideal.userName);
        ideal.userNameHardMin = Math.min(120, ideal.userName);

        doc.getElementById('usernameDiv').savedUsername = doc.getElementById('usernameDiv').innerHTML.trim();

        ideal.total =
            ideal.oraBranding +
            ideal.menuButton * ideal.numMenuButtons +
            ideal.menuButtonInBetweens +
            ideal.menuPadding * 2 +
            ideal.userName;

        ideal.everythingCurrentlyAtIdeal = true;
    }

    var ideal = window.idealSizes;

    /* Now re-space things if necessary. */
    var pageWidth = UTIL.pageWidth();
    if (pageWidth >= ideal.total) {
        if (ideal.everythingCurrentlyAtIdeal) {
            // Nothing to do.
            return;
        }
        else {
            // We are now wide enough for everything to be at its default, but at some point in the past we had shrunk.  Need to reset everything.
            var menuContainer = doc.getElementById(ideal.menuContainerId);
            for (var i = 0; i < menuContainer.children.length; i++) {
                menuContainer.children[i].style.width = '';
            }
            menuContainer.style.marginLeft = '';
            menuContainer.style.marginRight = '';
            doc.getElementById('userAndEnvContainer').style.width = '';
            var usernameDiv = doc.getElementById('usernameDiv');
            if (!usernameDiv.savedUsername) {
                usernameDiv.savedUsername = usernameDiv.innerHTML.trim();
            }
            usernameDiv.innerHTML = usernameDiv.savedUsername;
            usernameDiv.title = '';

            ideal.everythingCurrentlyAtIdeal = true;
        }
    }
    else {
        // We will have to shrink something.
        ideal.everythingCurrentlyAtIdeal = false;

        var pixelsRemainingToSave = ideal.total - pageWidth;

        /* Priority list of things to shrink 
        *  1. username down to softMin  
        *  2. menu item width
        *  3. padding on either side of menu
        *  4. username down to hard min.
        */

        // 1. Shrink username down to softMin.
        var reclaimablePixels = ideal.userName - ideal.userNameSoftMin;
        var userAndEnvContainer = doc.getElementById('userAndEnvContainer');

        if (reclaimablePixels > pixelsRemainingToSave) {
            resizeUsernameTo(ideal.userName - pixelsRemainingToSave);
            pixelsRemainingToSave = 0;
        }
        else {
            resizeUsernameTo(ideal.userNameSoftMin);
            pixelsRemainingToSave -= (ideal.userName - doc.getElementById('userAndEnvContainer').offsetWidth);
            // This operation can save extra pixels
            if (pixelsRemainingToSave <= 0) {
                pixelsRemainingToSave = 0;
            }
        }

        // 2. Menu button width
        var menuContainer = doc.getElementById(ideal.menuContainerId);
        reclaimablePixels = ideal.numMenuButtons * (ideal.menuButton - ideal.menuButtonMin);
        if (reclaimablePixels >= pixelsRemainingToSave) {
            var targetWidth = ideal.menuButton - (pixelsRemainingToSave / ideal.numMenuButtons);
            for (var i = 0; i < menuContainer.children.length; i++) {
                menuContainer.children[i].style.width = targetWidth + 'px';
            }
            pixelsRemainingToSave = 0;
        }
        else {
            for (var i = 0; i < menuContainer.children.length; i++) {
                menuContainer.children[i].style.width = ideal.menuButtonMin + 'px';
            }
            pixelsRemainingToSave -= reclaimablePixels;
        }

        // 3. Padding on either side of menu
        reclaimablePixels = 2 * (ideal.menuPadding - ideal.menuPaddingMin);

        if (reclaimablePixels >= pixelsRemainingToSave) {
            menuContainer.style.marginLeft = menuContainer.style.marginRight = (ideal.menuPadding - (pixelsRemainingToSave / 2)) + 'px';
            pixelsRemainingToSave = 0;
        }
        else {
            menuContainer.style.marginLeft = menuContainer.style.marginRight = ideal.menuPaddingMin + 'px';
            pixelsRemainingToSave -= reclaimablePixels;
        }

        // 4. Shrink username down to hardMin
        if (pixelsRemainingToSave <= 0) {
            return;
        }

        var envContaineroffsetW = doc.getElementById('userAndEnvContainer').offsetWidth;
        reclaimablePixels = envContaineroffsetW - ideal.userNameHardMin;

        if (reclaimablePixels > pixelsRemainingToSave) {
            resizeUsernameTo(envContaineroffsetW - pixelsRemainingToSave);
            pixelsRemainingToSave = 0;
        }
        else {
            resizeUsernameTo(ideal.userNameHardMin);
            pixelsRemainingToSave -= (ideal.userName - envContaineroffsetW);
            // This operation can save extra pixels
            if (pixelsRemainingToSave <= 0) {
                pixelsRemainingToSave = 0;
            }
        }

    }
}

function resizeUsernameTo(widthGoal) {
    if (window.idealSizes && (!window.idealSizes.inSingleAppInNewWindowMode)) {
        widthGoal -= 32; // Constant for the padding alotted to the arrow.
    }
    else {
        widthGoal -= 12; // Non-arrow padding.
    }
    var usernameDiv = document.getElementById('usernameDiv');

    if (!usernameDiv.savedUsername) {
        usernameDiv.savedUsername = usernameDiv.innerHTML.trim();
    }

    usernameDiv.innerHTML = usernameDiv.savedUsername;
    usernameDiv.title = '';

    if (usernameDiv.offsetWidth <= widthGoal) {
        return;
    }

    var name = usernameDiv.savedUsername;
    var min = 0;
    var max = Math.max(0, name.length - 1);

    while (max > min) {
        var tryLength = Math.ceil((max + min) / 2);
        usernameDiv.innerHTML = name.substr(0, tryLength) + '...';
        if (usernameDiv.offsetWidth <= widthGoal) {
            min = tryLength;
        }
        else {
            max = tryLength - 1;
        }
    }

    usernameDiv.innerHTML = name.substr(0, max) + '...';
    usernameDiv.title = name;
}

function dismissDropdownsIfAppropriate(event) {
    var target = UTIL.getEventTarget(event);
    if (target && target.id) {
        var ELEMS_THAT_TRIGGER_MENU_CLOSE =
        {
            'topnavOracleBranding': true,
            'menuContainer': true,
            'menuAndFastPathContainer': true,
            'topnavSessionInfo': true
        }

        if (ELEMS_THAT_TRIGGER_MENU_CLOSE[target.id]) {
            hideAllDropdownMenus();
        }
    }
}

/*
* This function is based on RunNewApp but applies to external applications such as Adf and Fusion ones.
* Logic and some design structure from RunNewApp, when applicable, remain here.
*/
function RunExternalApp(launchAction, taskflowPath, taskId, taskName, taskType, appId, version, jargonCode, menuNodeInstanceId, lpContext) {
    hideAllDropdownMenus();

    if (isIOS) {
        updateAppFrameHeight();
        var fastPathField = document.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
        if (fastPathField) {
            fastPathField.blur();
        }
    }

    // We do not support launching external apps in the portal.
    if (isWSRPContainer()) {
        alert(externalAppStaticText.externalApps[0].appNotSupportedError);
        return;
    }

    var menuObject = jdeMenuParent;

    // We do not support launching external apps in a new window. New window launches via user actions, such as hotkey shift/ctrl + normal launch
    // receive an error message.  Note that for the user preference for all apps to run in new windows, normally we'd have a condition here based on
    // the following code: (!isWSRPContainer() &&  isPreferenceChecked("openAppInNewWindowPreference")).  However, for that condition, we will
    // merely run the app in the current browser window.
    if (menuObject.oneTimeNewWindowLaunch && MENUUTIL.allowNewWindow()) {
        // Reset the new window var to prevent unrelated task launches after this one from being inadvertently launched in a new window.
        menuObject.oneTimeNewWindowLaunch = false;
        alert(externalAppStaticText.externalApps[0].openInNewWindowError);
        return;
    }

    if (CheckAppStatus != null) {
        window.clearTimeout(CheckAppStatus);
        CheckAppStatus = null;
    }

    var topoffset = getAbsoluteTopPos(document.getElementById("menutitle")) + 51;

    //for the case that user starts another app without wait the first app is loaded
    if (ProcessingIndicator.getInstance() != null) {
        ProcessingIndicator.stopProcessingIndicator();
    }

    // no need to show the Open app msg when the launchAction is promptForBlindExecution.
    if (launchAction != DDMENU.CONST.PROMPT_FOR_BLINDEXECUTION) {
        ProcessingIndicator.createInstance(menuObject.procIndMsg, 210, 18, topoffset, 48).display(true); //width, height, topoffset, rightoffset

        OpenAppIndicator.createInstance().display(false);

        OpenAppShow = window.setTimeout("OpenAppIndicator.showOpenAppIndicator()", 2000);
    }

    var sb = "<html><body><form id=\"theForm\" method=post action=\"";
    sb += menuObject.runAppServiceUrl;
    sb += "\">\n";
    sb += "<input type=hidden name=timestamp value=\"";
    sb += (new Date()).getTime();
    sb += "\">\n";

    if (launchAction != "") { sb += "<input type=hidden name=launchAction value=\""; sb += launchAction; sb += "\">\n"; }
    if (taskflowPath != '') { sb += "<input type=hidden name=taskflow value=\""; sb += taskflowPath; sb += "\">\n"; }
    if (taskId != '') { sb += "<input type=hidden name=taskId value=\""; sb += taskId; sb += "\">\n"; }
    if (taskName != '') { sb += "<input type=hidden id=taskName name=taskName value=\""; sb += taskName; sb += "\">\n"; }
    if (taskType != '') { sb += "<input type=hidden name=taskType value=\""; sb += taskType; sb += "\">\n"; }
    if (appId != '') { sb += "<input type=hidden name=appID value=\""; sb += appId; sb += "\">\n"; }
    if (version != '') { sb += "<input type=hidden name=version value=\""; sb += version; sb += "\">\n"; }
    if (jargonCode != '') { sb += "<input type=hidden name=menuSysCode value=\""; sb += jargonCode; sb += "\">\n"; }
    sb += "<input type=hidden name=appType value=\""; sb += ExternalAppsHandler.EXTERNAL_APP_APPTYPE; sb += "\">\n";
    sb += "<input type=hidden name=jdemafjasLinkTarget value=\""; sb += self.name; sb += "\">\n";
    sb += "<input type=hidden name=namespace value=\""; sb += menuObject.menuNamespace; sb += "\">\n";
    sb += "<input type=hidden name=RID value=\""; sb += menuObject.RID; sb += "\">\n";
    // The ID of the menu node instance that initiated the application launch.
    sb += "<input type=hidden name=" + BREADCRUMBS.CONSTANTS.PARAM_BREADCRUMB_NODE_ID + " value=\"";
    if (menuNodeInstanceId == undefined || menuNodeInstanceId == "") {
        sb += "";
    }
    else {
        sb += menuNodeInstanceId;
    }
    sb += "\">\n";
    //lpcontext for springboard from landing page
    if (lpContext !== undefined && lpContext != "") { sb += "<input type=hidden name=lpContext value=\""; sb += lpContext; sb += "\">\n"; }

    sb += "</form>";

    //    if (accessibilityMode)
    //    {
    //        sb+= jdeMenuParent.procIndMsg;
    //    }
    sb += "</body></html>";

    var frm = null;
    frm = document.getElementById("e1menuAppIframe").contentWindow;
    if (!frm) {
        frm = window.frames["e1menuAppIframe"];
    }

    var newDoc = frm.document;
    newDoc.open("text/html");
    newDoc.charset = "UTF-8";
    newDoc.write(sb);
    newDoc.close();

    menuObject.currCompId = 0;

    // Once we launch the application, clear out the breadcrumb bar content
    // to minimize refresh lag in the bar while the application launches.
    BREADCRUMBS.updateBreadcrumbContent("");

    frm.document.getElementById("theForm").submit();
}

function toggleDisplayOfMangaeUDOSubMenuConatiner() {
    var ManageUDOSubMenuContainer = document.getElementById("ManageUDOSubMenuContainer");
    var collapseState = ManageUDOSubMenuContainer.style.display == "none";
    collapseState = !collapseState;
    ManageUDOSubMenuContainer.style.display = collapseState ? "none" : "inline";
    var ManagePageSubMenuCollapseState = document.getElementById("ManagePageSubMenuCollapseState");
    var ManagePageSubMenuExpendState = document.getElementById("ManagePageSubMenuExpendState");
    ManagePageSubMenuExpendState.style.display = collapseState ? "none" : "inline";
    ManagePageSubMenuCollapseState.style.display = collapseState ? "inline" : "none";
}