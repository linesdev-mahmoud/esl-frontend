/*
** Copyright (c) Oracle Corporation 2006. All Rights Reserved.
**
** @author Nan Li
*/
//constant defines the layer(z-index) for the UIBlocking DIV
var UIBLOCKING_LAYER_ZINDEX = 100000;

function UIBlockingPane() {
    this.Init();
}

UIBlockingPane.prototype.Init = function () {
    // blocking support
    this._uiBlockingDiv = null; // Div used for ui blocking
}

//crates the div used for UI blocking
UIBlockingPane.prototype._createUIBlockingDiv = function (ownerDocument) {
    var agent = Agent.getAgent();
    var uiBlockingDiv = ownerDocument.createElement("div");
    uiBlockingDiv.id = "UIBlockingDiv";
    uiBlockingDiv.className = "UIBlockingDiv";
    var divStyle = uiBlockingDiv.style;
    divStyle.position = "absolute";
    divStyle.left = "0px";
    //divStyle.top = "25px"; //for enable the about
    divStyle.top = "0px";
    divStyle.zIndex = UIBLOCKING_LAYER_ZINDEX;
    divStyle.visibility = "visible";

    agent.disableBrowserContextMenu(uiBlockingDiv);
    //by default the blocking div is 40% transparent
    agent.setOpacity(uiBlockingDiv, 40);
    //uiBlockingDiv.style.backgroundColor = "#E8E8E8"; 

    ownerDocument.body.appendChild(uiBlockingDiv);

    this._uiBlockingDiv = uiBlockingDiv;
    this._positionBlockingDiv(ownerDocument);
}

UIBlockingPane.prototype._positionBlockingDiv = function (ownerDocument) {
    var documentElement = ownerDocument.documentElement;
    var bodyElem = ownerDocument.body;
    var height = Math.max(bodyElem.offsetHeight, bodyElem.scrollHeight),
      width = Math.max(bodyElem.offsetWidth, bodyElem.scrollWidth),
      divStyle = this._uiBlockingDiv.style;

    // Sometimes the document content could be smaller than the window, in which
    // case use the clientHeight, clientWidth of the documentElement.
    if (divStyle.visibility != 'hidden') {
        height = Math.max(height, documentElement.clientHeight);
        width = Math.max(width, documentElement.clientWidth);
        divStyle.height = height + "px";
        divStyle.width = width + "px";
    }
}

UIBlockingPane.prototype._hideBlockingDiv = function () {
    this._uiBlockingDiv.style.visibility = "hidden";
    this._uiBlockingDiv.style.height = "0px";
    this._uiBlockingDiv.style.width = "0px";
}

UIBlockingPane.prototype._showBlockingDiv = function (ownerDocument) {
    this._uiBlockingDiv.style.visibility = "visible";
    this._positionBlockingDiv(ownerDocument);
}

/**
* Prevents the user from interacting with the client UI
*@param block   boolean indicating whether to block/unblock the ui
*/
UIBlockingPane.prototype.blockUI = function (ownerDocument) {
    this._createUIBlockingDiv(ownerDocument);
}