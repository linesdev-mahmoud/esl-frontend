/*
* watchlist.js - Runtime Javascript code for executing and rendering Watchlists in the 
*     dropdown menu, carousel and e1Pages.
*/

var WLIST;
if (!WLIST) {
    WLIST = {
        CONST: {
            MENU_CONTENT_ID: 'e1MMenuWatchlists',
            MENU_CONTENT_PARENT_ID: 'drop_watchlist_menus',
            ITEM_PREFIX: 'wListItem_',
            ARIA_PREFIX: 'wListLabel_',
            VERTICAL_BUFFER_PIXELS: 3,
            MANAGER_SERVICE: "WatchlistManagerService",
            WLCOMP_ITEM_PREFIX: "watchlist_",
            ALL_FILTER: "allFilter",
            MATCHES_FILTER: "matchesFilter",
            WARNING_FILTER: "warningFilter",
            CRITICAL_FILTER: "criticalFilter",
            NOTIFICATION_QUEUE_SIZE: 1,
            WAIT_FOR_SHARED_LIST: {
                FIRST_DELAY: 3000,
                SUBSEQUENT_DELAY: 15000,
                NUM_EXTRA_TRIES: 4
                /* These values describe the pattern with which we attempt to get the list
                of shared WL's if the security cache is not done building.  At current 
                values, we will check once after 3 seconds, then every 15 seconds 4 more
                times.  That is, we will check at:
                -  3 seconds
                - 18 seconds
                - 33 seconds
                - 48 seconds
                - 63 seconds
                At that point, in the (very unlikely) event the security cache still isn't
                built, we just give up. */
            }
        },
        data: null
    };

    if (!LOGGER) {
        LOGGER = new Object();
    }

}


WLIST.log = function (message) {
    if (LOGGER && LOGGER.log) {
        LOGGER.log('WLIST: ' + message);
    }
}

WLIST.initialize = function () {
    if (WLIST.alreadyInitialized) {
        return;
    }
    WLIST.alreadyInitialized = true;

    if (window.dropdownWatchlists) {
        WLIST.data = window.dropdownWatchlists;
    }
    else {
        WLIST.data = [];
    }

    if (WLIST.data.length == 0) {
        var wlMenu = WLIST.getMenuContentParent().parentNode;
        if (wlMenu) {
            wlMenu.style.display = 'none';
        }
    }

    if (window.watchlistServerSideConstants) {
        WLIST.CONST.FROM_SERVER = window.watchlistServerSideConstants;
    }

    WLIST.activeRefreshThreads = 0;

    if (!(window.watchlistsSharedListComplete)) {
        WLIST.securityCacheRetries = 0;
        setTimeout(WLIST.reFetchListOfWatchlistsForDropdownMenu, WLIST.CONST.WAIT_FOR_SHARED_LIST.FIRST_DELAY);
    }

    WLIST.isNativeContainer = (navigator.userAgent.toUpperCase().indexOf("JDECONTAINER") > -1);

    WLIST.notificationEvaluationQueue = new Array();
    WLIST.activeNotificationRefreshThreads = 0;
}

WLIST.renderMenu = function (fromNotification) {
    if (WLIST.renderLock) {
        return;
    }
    WLIST.renderLock = true;

    // Revoke kill-signal.  (In case someone changes their mind about logging out--e.g., they
    // clicked logout but were told they had apps running and canceled it.  Next time they pull
    // down dropdown, we want the refresh threads to start again.
    WLIST.disallowRefresh = false;

    // See if the data exists
    if (WLIST.data == null) {
        // Have not yet fetched initial data.
        WLIST.renderLock = false;
        return false;
    }

    // Force re-render if marked as needing such. (For when WL's added/deleted/updated)
    if (WLIST.fullRerenderNecessary) {
        WLIST.destroyMenuContent();
        WLIST.fullRerenderNecessary = false;
    }

    // Create HTML object if it doesn't exist already.
    var menuContent = WLIST.getMenuContent();

    if (!menuContent.alreadyPopulated) {
        WLIST.buildMenuFromScratch(menuContent);
    }
    else {
        WLIST.identifyListsForUpdate();
    }

    if (!fromNotification) {
        for (var i = WLIST.activeRefreshThreads; i < WLIST.CONST.FROM_SERVER.NUM_REFRESH_THREADS; i++) {
            WLIST.updateNextWaitingWatchlist();
        }
    }
    WLIST.renderLock = false;
}

WLIST.identifyListsForUpdate = function () {
    for (var i = 0; i < WLIST.data.length; i++) {
        if (WLIST.data[i].items) {
            for (var x = 0; x < WLIST.data[i].items.length; x++) {
                var item = document.getElementById(WLIST.data[i].items[x]);
                if (!item) {
                    item = WLIST.findElementInIframes(WLIST.data[i].items[x], WLIST.data[i].id);
                }
                if (item) {
                    WLIST.testWhetherItemNeedsUpdate(item);
                }
            }
        }
    }
}

WLIST.testWhetherItemNeedsUpdate = function (item) {
    var now = (new Date()).getTime();
    var timeSinceUpdate = now - item.lastUpdate;

    if (((timeSinceUpdate > item.updateIntervalMillis) &&
           !(item.getAttribute('update') == 'inProgress') &&
           !(item.getAttribute('update') == 'failed'))
                        ||
         (item.getAttribute('update') == 'interrupted')
                                                           ) {
        item.markedForUpdate = true;
        item.setAttribute('update', 'pending');
        if (item.isDDWatchlist) {
            if (item.refreshCtl.getAttribute("aria-pressed") == 'true') {
                if (jdeMenuParent.isAccessibilityEnabled) {
                    UTIL.forceRedraw(item.refreshCtl); // With JAWS on, In Firefox focus is lost after Refresh unless we do a Redraw. So do Redraw for all Browsers.
                }
                else {
                    UTIL.forceRedrawIEOnly(item.refreshCtl);
                }
                item.refreshCtl.focus();
                item.refreshCtl.setAttribute('aria-pressed', 'false');
            }
            else {
                UTIL.forceRedrawIEOnly(item.refreshCtl) // For the benefit of IE.  
            }
        }
        else {
            if (item.isE1PageWatchlist) {
                UTIL.forceRedrawIEOnly(item.refreshCtl); // For the benefit of IE.  
                UTIL.forceRedraw(item.refreshCtl);
            }
            if (item.isCompWatchlist) {
                UTIL.forceRedrawIEOnly(item.table.col.refreshCtl); // For the benefit of IE.  
                UTIL.forceRedraw(item.table.col.refreshCtl);
            }
        }
    }
}

WLIST.buildMenuFromScratch = function (menuContent) {
    menuContent.alreadyPopulated = true;
    // menuContent.innerHTML = ''; 

    if (WLIST.data.length == 0) {
        menuContent.appendChild(WLIST.createNoWatchlistsMessage());
        return;
    }

    if (!WLIST.organized) {
        WLIST.organizeWatchlistData();
    }

    var personalListsSection = WLIST.createSectionIfNonEmpty(WLIST.CONST.FROM_SERVER.TEXT.UDO_PERSONAL, WLIST.personalLists);
    var pendingApprovalListsSection = WLIST.createSectionIfNonEmpty(WLIST.CONST.FROM_SERVER.TEXT.UDO_PENDING_APPROVAL, WLIST.pendingApprovalLists);
    var reworkListsSection = WLIST.createSectionIfNonEmpty(WLIST.CONST.FROM_SERVER.TEXT.UDO_REWORK, WLIST.reworkLists);
    var reservedListsSection = WLIST.createSectionIfNonEmpty(WLIST.CONST.FROM_SERVER.TEXT.UDO_RESERVED, WLIST.reservedLists);
    var sharedListsSection = WLIST.createSectionIfNonEmpty(WLIST.CONST.FROM_SERVER.TEXT.UDO_SHARED, WLIST.sharedLists);

    /* So that the WL's will update in order, we now reconstitute the original WLIST.data array to 
    * reflect the sorted sections */
    WLIST.data = WLIST.personalLists.concat(WLIST.pendingApprovalLists, WLIST.reworkLists, WLIST.reservedLists, WLIST.sharedLists);

    if (personalListsSection) {
        menuContent.appendChild(menuContent.personalLists = personalListsSection);
        if (reservedListsSection || sharedListsSection) {
            menuContent.appendChild(UTIL.createElement('div', 'watchlistDividerBetweenSections'));
        }
    }
    if (pendingApprovalListsSection) {
        menuContent.appendChild(menuContent.pendingApprovalLists = pendingApprovalListsSection);
        if (reservedListsSection || sharedListsSection) {
            menuContent.appendChild(UTIL.createElement('div', 'watchlistDividerBetweenSections'));
        }
    }
    if (reworkListsSection) {
        menuContent.appendChild(menuContent.reworkLists = reworkListsSection);
        if (reservedListsSection || sharedListsSection) {
            menuContent.appendChild(UTIL.createElement('div', 'watchlistDividerBetweenSections'));
        }
    }
    if (reservedListsSection) {
        menuContent.appendChild(menuContent.reservedLists = reservedListsSection);
        if (sharedListsSection) {
            menuContent.appendChild(UTIL.createElement('div', 'watchlistDividerBetweenSections'));
        }
    }
    if (sharedListsSection) {
        menuContent.appendChild(menuContent.sharedLists = sharedListsSection);
    }
}
// This function retruns  the personal WL items.
WLIST.getPersonalWLItems = function (items) {
    var tempPerArrray = [];
    for (var i = 0, len = items.length; i < len; i++) {
        if ((items[i].lineType == 1) && (!items[i].isRequestPublished) && (!items[i].isRework)) {
            tempPerArrray.push(items[i]);
        }
    }
    return tempPerArrray;
}

// This function retruns the Personal WL items which are in Pending Approval status(05)
WLIST.getPendingApprovalWLItems = function (items) {
    var tempPenAppArray = [];
    for (var i = 0, len = items.length; i < len; i++) {
        if ((items[i].isRequestPublished)) {
            tempPenAppArray.push(items[i]);
        }
    }
    return tempPenAppArray;
}

// This function retruns the Personal WL items which are in Rework Status(25)
WLIST.getReworkWLItems = function (items) {
    var tempReworkArray = [];
    for (var i = 0, len = items.length; i < len; i++) {
        if ((items[i].isRework)) {
            tempReworkArray.push(items[i]);
        }
    }
    return tempReworkArray;
}

// The watchlist array (WLIST.data) originates from the F952420 and is sorted by LNTYP (line type) and then WOBNM (web object name).
// Prior to bug #21099716, we loaded the reserved watchlists bucket on an assumption that these watchlists followed pending approval
// and rework watchlists. Pending approval/rework attributes tie to the ULCM status value (F9861W) but are not a component of the sort.
// As such, the reserved watchlist bucket must be based on being a catch-all for non-personal, non-reworked, and non-pending approval items.
WLIST.getReservedWLItems = function (items) {
    var tempReservedArray = [];
    for (var i = 0, len = items.length; i < len; i++) {
        if ((items[i].lineType != 1) && !(items[i].isRework) && !(items[i].isRequestPublished)) {
            tempReservedArray.push(items[i]);
        }
    }
    return tempReservedArray;
}

WLIST.organizeWatchlistData = function () {
    // Find the first public WL.
    var firstPublicWL = WLIST.data.length;
    for (var i = 0; i < WLIST.data.length; i++) {
        if (!(WLIST.data[i].isPrivate)) {
            firstPublicWL = i;
            break;
        }
    }

    WLIST.AllPersonalLists = WLIST.data.slice(0, firstPublicWL);

    // WLIST.personalLists list contains both personal and pending approval wl items
    WLIST.personalLists = WLIST.getPersonalWLItems(WLIST.AllPersonalLists);
    WLIST.pendingApprovalLists = WLIST.getPendingApprovalWLItems(WLIST.AllPersonalLists);
    WLIST.reworkLists = WLIST.getReworkWLItems(WLIST.AllPersonalLists);
    WLIST.reservedLists = WLIST.getReservedWLItems(WLIST.AllPersonalLists);
    WLIST.sharedLists = WLIST.data.slice(firstPublicWL, WLIST.data.length);

    WLIST.sortWatchlistsByName(WLIST.personalLists);
    WLIST.sortWatchlistsByName(WLIST.pendingApprovalLists);
    WLIST.sortWatchlistsByName(WLIST.reworkLists);
    WLIST.sortWatchlistsByName(WLIST.reservedLists);
    WLIST.sortWatchlistsByName(WLIST.sharedLists);

    //Set a global variable so that other watchlist functionality can test this
    //(i.e. - watchlist component on an e1page
    WLIST.organized = true;
}

WLIST.sortWatchlistsByName = function (items) {
    for (var endIndex = items.length - 1; endIndex >= 1; endIndex--) {
        for (var i = 0; i < endIndex; i++) {
            var name1 = items[i].name.toLowerCase();
            var name2 = items[i + 1].name.toLowerCase();
            if (name1 > name2) {
                var item = items[i];
                items[i] = items[i + 1];
                items[i + 1] = item;
            }
        }
    }
}

WLIST.createSectionIfNonEmpty = function (sectionLabel, data) {
    if (data.length == 0) {
        return null;
    }

    var section = UTIL.createElement('div', 'watchlistSection');
    section.heading = UTIL.createElement('div', 'watchlistSectionHeading');
    section.heading.setAttribute('role', 'menuitem');
    section.heading.setAttribute('tabindex', '0');
    section.heading.onkeydown = WLIST.onkeydownHandler;

    // When Accessibility mode is set to true
    // each section heading is labelled by using a SPAN  
    if (jdeMenuParent.isAccessibilityEnabled) {
        section.heading.setAttribute('aria-haspopup', 'true');
        if (sectionLabel == WLIST.CONST.FROM_SERVER.TEXT.UDO_PERSONAL) {
            section.heading.setAttribute('aria-labelledby', 'privateWLSectionHeading');
            section.heading.labelWrapper = UTIL.createElement('span', 'pWLSectionHeadingLabelWrapper', 'privateWLSectionHeading');
        }
        else if (sectionLabel == WLIST.CONST.FROM_SERVER.TEXT.UDO_PENDING_APPROVAL) {
            section.heading.setAttribute('aria-labelledby', 'pendingApprovalWLSectionHeading');
            section.heading.labelWrapper = UTIL.createElement('span', 'paWLSectionHeadingLabelWrapper', 'pendingApprovalWLSectionHeading');
        }
        else if (sectionLabel == WLIST.CONST.FROM_SERVER.TEXT.UDO_REWORK) {
            section.heading.setAttribute('aria-labelledby', 'reworkWLSectionHeading');
            section.heading.labelWrapper = UTIL.createElement('span', 'reWLSectionHeadingLabelWrapper', 'reworkWLSectionHeading');
        }
        else if (sectionLabel == WLIST.CONST.FROM_SERVER.TEXT.UDO_SHARED) {
            section.heading.setAttribute('aria-labelledby', 'sharedWLSectionHeading');
            section.heading.labelWrapper = UTIL.createElement('span', 'sWLSectionHeadingLabelWrapper', 'sharedWLSectionHeading');
        }
        else if (sectionLabel == WLIST.CONST.FROM_SERVER.TEXT.UDO_RESERVED) {
            section.heading.setAttribute('aria-labelledby', 'reservedWLSectionHeading');
            section.heading.labelWrapper = UTIL.createElement('span', 'rWLSectionHeadingLabelWrapper', 'reservedWLSectionHeading');
        }
        section.heading.labelWrapper.innerHTML = sectionLabel;
        section.heading.appendChild(section.heading.labelWrapper);
    }
    else {
        section.heading.innerHTML = sectionLabel;
    }
    section.appendChild(section.heading);

    for (var i = 0; i < data.length; i++) {
        var wListItem = WLIST.createItem(data[i]);
        if (!data[i].items) {
            data[i].items = [wListItem.id];
        }
        else {
            if (!WLIST.alreadyInArray(data[i].items, wListItem)) {
                data[i].items.push(wListItem.id);
            }
        }
        section.appendChild(wListItem);
    }

    return section;
}


WLIST.createNoWatchlistsMessage = function () {
    var mesg = UTIL.createElement('span')
    mesg.innerHTML = WLIST.CONST.FROM_SERVER.TEXT.EMPTY_DROPDOWN;
    return mesg;
}

WLIST.updateNextWaitingWatchlist = function () {
    WLIST.activeRefreshThreads++;

    /* TODO: Technically, this use of a marked array to simulate a queue is somewhat inelegant, as 
    * it is an O(n^2) operation to hit every element.  That being said, since n will rarely reach
    * a value even of 10, this method's simplicity is attractive. 
    * 
    * There are also some easy ways we could resume our search where we left off. */

    for (var i = 0; i < WLIST.data.length; i++) {
        var item = WLIST.data[i];
        if (item && item.items && item.items[0]) {
            var itemElement = document.getElementById(item.items[0]);
            if (!itemElement) {
                itemElement = WLIST.findElementInIframes(item.items[0], item.id);
                //if watchlist is not available by id then do a lookup with obnm
                if (!itemElement) {
                    itemElement = WLIST.findElementInIframes(item.items[0], item.omwObjectName);
                }
            }

            if (itemElement && itemElement.markedForUpdate) {
                itemElement.reEvaluateOnServer();
                return; /* Do not decrement activeRefreshThreads because the thread is still running */
            }
        }
    }

    /* If we made it through the list without finding an actionable item, then this thread
    * has no work to do and will now die. */
    WLIST.activeRefreshThreads--;
}

WLIST.createItem = function (datum) {
    var itemId = WLIST.CONST.ITEM_PREFIX + datum.id;
    var ariaId = WLIST.CONST.ARIA_PREFIX + datum.id;

    var item = UTIL.createElement('div', 'watchlistMenuItem', itemId);

    item.isDDWatchlist = true;
    item.data = datum;
    var existingUpdate = null;
    if (item.data.items) {
        var itemElement = document.getElementById(item.data.items[0]);
        if (!itemElement) {
            itemElement = WLIST.findElementInIframes(item.data.items[0], item.data.id);
        }
        if (itemElement && itemElement.lastUpdate) {
            existingUpdate = itemElement.lastUpdate;
        }
    }
    item.lastUpdate = existingUpdate ? existingUpdate : -1;
    item.updateIntervalMillis = WLIST.getUpdateThreshholdMilliseconds(item);
    item.maxRecordCountToUse = WLIST.determineMaxRecordCount(item);

    item.onmouseover = item.recomputeTooltip = WLIST.setTooltip;
    item.onkeydown = WLIST.onkeydownHandler;

    item.refreshCtl = UTIL.createElement('a', 'refreshWatchlistControl');
    item.labelWrapper = UTIL.createElement('span', 'wlLabelWrapper', ariaId);
    item.titleLabel = UTIL.createElement('span', 'wlLabel');
    item.refreshCtl.setAttribute('tabindex', '0');
    item.refreshCtl.setAttribute('aria-pressed', 'false');
    item.refreshCtl.onkeydown = WLIST.onkeydownHandler;
    item.refreshCtl.onblur = WLIST.onblur;
    item.labelWrapper.setAttribute('tabindex', '0');
    item.labelWrapper.onkeydown = WLIST.onkeydownHandler;
    item.labelWrapper.onfocus = WLIST.onfocus;
    item.labelWrapper.onblur = WLIST.onblur;

    // For Accessiblity create a Label for Refresh Ctrl so that JAWS can read whatever comes up in the Tooltip of the Refresh Ctrl
    if (jdeMenuParent.isAccessibilityEnabled) {
        item.refreshCtl.setAttribute('id', 'refreshWL_' + ariaId);
        item.refreshCtl.setAttribute('role', 'menuitem');
        item.refreshCtl.setAttribute('aria-describedby', 'refreshWLDesc_' + ariaId);
        item.refreshCtl.onfocus = item.recomputeTooltip = WLIST.setTooltip;
        item.refreshCtlLabel = UTIL.createElement('span', 'refreshWLCtrlLabel', 'refreshWLDesc_' + ariaId);
        item.refreshCtlLabel.setAttribute('role', 'tooltip');
        item.refreshCtlLabel.style.display = 'none';
        item.refreshCtl.appendChild(item.refreshCtlLabel);
        item.labelWrapper.setAttribute('role', 'menuitem');
    }
    else {
        item.refreshCtl.onfocus = WLIST.onfocus;
    }

    item.spacerText = UTIL.createElement('span');
    item.countLabel = UTIL.createElement('span');

    UTIL.forceRedrawIEOnly(item.refreshCtl) // For the benefit of IE.  
    item.titleLabel.innerHTML = datum.name;
    item.spacerText.innerHTML = (navigator && navigator.appName == "Microsoft Internet Explorer") ? '&nbsp;' : ' ';

    item.refreshCtl.onclick = function (event) {
        if (!event) {
            // IE doesn't pass in the event; instead, there is one universal window.event
            event = window.event;
        }
        if (event.keyCode == 13) // if Enter key has been pressed
        {
            item.refreshCtl.setAttribute('aria-pressed', 'true');
        }
        UTIL.stopEventBubbling(event);
        item.manualRefresh = true;
        item.reEvaluateOnServer();
    }

    var OID = [item.data.appId, item.data.formId, item.data.version].join('_');
    var id = item.data.id;

    item.onclick = function (event) {
        if (item.data.appId != "DATABROWSE") {
            UTIL.stopEventBubbling(event)

            hideMenu('e1MMenuWatchlists');
            item.setAttribute('update', 'pending');
            item.markedForUpdate = true;
            runE1AppWatchlist(OID, id);
        }
    }

    item.actOnResponse = function (response) {
        WLIST.actionResponse(response, item);
    }

    item.updateCount = function (count, overrideTimestamp) {
        var countStr = count;
        if (count >= item.maxRecordCountToUse) // Shouldn't ever be >, only ever ==, but using >= to be safe
        {
            countStr = item.maxRecordCountToUse + '+';
        }
        item.countLabel.innerText = item.countLabel.textContent = '(' + countStr + ')';

        var alertLevel = 'normal';

        if ((item.data.critThresh >= 0) && (count >= item.data.critThresh)) {
            alertLevel = 'critical';
        }
        else {
            if ((item.data.warnThresh >= 0) && (count >= item.data.warnThresh)) {
                alertLevel = 'warn';
            }
        }

        item.setAttribute('alertLevel', alertLevel);
        // For Accessiblity create a Label for the alert level so that JAWS can read 
        // Count is greater than or equal to Warning Threshold OR
        // Count is greater than or equal to Critical Threshold
        if (jdeMenuParent.isAccessibilityEnabled) {
            var labelText = " ";
            if (alertLevel == 'warn') {
                labelText = WLIST.CONST.FROM_SERVER.TEXT.WARNING_THRESHOLD;
            }
            else if (alertLevel == 'critical') {
                labelText = WLIST.CONST.FROM_SERVER.TEXT.CRITICAL_THRESHOLD;
            }
            item.labelWrapper.setAttribute('aria-label', item.data.name + " " + labelText);
        }

        UTIL.forceRedrawIEOnly(item) // For the benefit of IE.          
        if (overrideTimestamp) {
            item.lastUpdate = overrideTimestamp;
        }

        item.data.lastResult = {
            count: count,
            timeStamp: item.lastUpdate
        };
    };

    //Need to check and see if this has been created
    //already as we now have three types of watchlist
    //items now.
    if (item.data.items && item.data.items.length > 0) {
        var itemElement = document.getElementById(item.data.items[0]);
        if (!itemElement) {
            itemElement = WLIST.findElementInIframes(item.data.items[0], item.data.id);
        }
        if (itemElement && itemElement.updateCount) {
            var update = itemElement.getAttribute('update');
            var values = itemElement.data.lastResult;
            if (update == 'done' && values) {
                item.updateCount(values.count, values.timeStamp);
            }
            item.setAttribute('update', update);
            item.markedForUpdate = itemElement.markedForUpdate;
        }
        else {
            item.setAttribute('update', 'pending');
            item.markedForUpdate = true;
        }
    }
    else {
        item.setAttribute('update', 'pending');
        item.markedForUpdate = true;
    }

    item.reEvaluateOnServer = function () {
        WLIST.doReEvaluation(item);
    };

    // IE doesn't support forEach loops, so we have to do this as a traditional loop.
    var elemsToAdd = ['titleLabel', 'spacerText', 'countLabel'];
    for (var i = 0; i < elemsToAdd.length; i++) {
        item.labelWrapper.appendChild(item[elemsToAdd[i]]);
    }

    var elemsToAdd = ['refreshCtl', 'labelWrapper'];
    for (var i = 0; i < elemsToAdd.length; i++) {
        item.appendChild(item[elemsToAdd[i]]);
    }

    // We create the title, the spacer, and the count as three separate items so that they will
    // automatically order correctly in RTL use-case.


    // If the item has a lastResult, than we are rebuilding one that has been run before.
    if (item.data.lastResult) {
        //Synchronize with other elements
        if (item.data.items && item.data.items.length != 0) {
            for (var x = 0; x < item.data.items.length; x++) {
                var itemElement = document.getElementById(item.data.items[x]);
                if (!itemElement) {
                    itemElement = WLIST.findElementInIframes(item.data.items[x], item.data.id);
                }
                if (itemElement && itemElement.updateCount) {
                    itemElement.updateCount(item.data.lastResult.count, item.data.lastResult.timeStamp);
                    itemElement.setAttribute('update', 'done');
                    itemElement.markedForUpdate = false;
                }
            }
            //Make sure to update the item itself.
            item.updateCount(item.data.lastResult.count, item.data.lastResult.timeStamp);
            item.setAttribute('update', 'done');
            item.markedForUpdate = false;
            WLIST.testWhetherItemNeedsUpdate(item);
        }
    }

    return item;
}

WLIST.getMenuContent = function () {
    if (WLIST.menuContent) {
        return WLIST.menuContent;
    }

    WLIST.menuContent = UTIL.createElement('div', 'hideMenu', WLIST.CONST.MENU_CONTENT_ID)
    WLIST.getMenuContentParent().appendChild(WLIST.menuContent);

    WLIST.menuContent.customScrollLogic = WLIST.applyScrollLogicToWListDropdown;

    return WLIST.menuContent;
}

WLIST.getMenuContentParent = function () {
    return document.getElementById(WLIST.CONST.MENU_CONTENT_PARENT_ID);
}

WLIST.applyScrollLogicToWListDropdown = function () {
    // "this" is the menuElement

    var desiredHeight = this.scrollHeight;
    var availableHeight = UTIL.pageHeight() - this.offsetTop - WLIST.CONST.VERTICAL_BUFFER_PIXELS;

    if (availableHeight < 0) {
        // The window isn't even tall enough for the E1 chrome.  This is well under the MTR
        // and there is nothing we can do to make it usable.  
        return;
    }

    if (availableHeight < desiredHeight) {
        this.style.height = availableHeight + 'px';
        this.style.overflowY = 'scroll';
    }
    else {
        this.style.height = '';
        this.style.overflowY = '';
    }
}

WLIST.destroyMenuContent = function () {
    if (WLIST.menuContent) {
        WLIST.menuContent.parentNode.removeChild(WLIST.menuContent);
        WLIST.menuContent = null;
    }
}

WLIST.setTooltip = function () {

    var updateStatus = null;
    var sibling = null;
    if (jdeMenuParent.isAccessibilityEnabled && this != null && this.className == "refreshWatchlistControl") {
        updateStatus = this.parentNode.getAttribute('update');
    }
    else {
        updateStatus = this.getAttribute('update');
    }

    if (updateStatus == 'inProgress') {
        // For Accessibility set the Label for the Refresh Ctrl with the appropriate text which is
        // Update in progress for followed by the Watchlist Name
        if (jdeMenuParent.isAccessibilityEnabled) {
            if (this.className == "refreshWatchlistControl") {
                this.firstChild.innerHTML = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_IN_PROGRESS_FOR + " " + this.parentNode.titleLabel.innerHTML;
            }
            else {
                this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_IN_PROGRESS;
            }

        }
        else {
            this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_IN_PROGRESS;
        }
        return;
    }

    if (updateStatus == 'pending') {

        if (jdeMenuParent.isAccessibilityEnabled) {
            // For Accessibility set the Label for the Refresh Ctrl with the appropriate text which is
            // Update pending... followed by Refresh and then the Watchlist Name
            if (this.className == "refreshWatchlistControl") {
                this.firstChild.innerHTML = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_PENDING + " " + WLIST.CONST.FROM_SERVER.TEXT.REFRESH_WATCHLIST + " " + this.parentNode.titleLabel.innerHTML;
            }
            else {
                this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_PENDING;
            }

        }
        else {
            this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_PENDING;
        }
        return;
    }

    if (updateStatus == 'failed') {

        if (jdeMenuParent.isAccessibilityEnabled) {
            // For Accessibility set the Label for the Refresh Ctrl with the appropriate text which is
            // Update failed followed by Refresh and then the Watchlist Name
            if (this.className == "refreshWatchlistControl") {
                this.firstChild.innerHTML = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_FAILED + " " + WLIST.CONST.FROM_SERVER.TEXT.REFRESH_WATCHLIST + " " + this.parentNode.titleLabel.innerHTML;

            }
            else {
                this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_FAILED;
            }

        }
        else {
            this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_FAILED;
        }
        return;
    }

    if (updateStatus == 'interrupted') {

        if (jdeMenuParent.isAccessibilityEnabled) {
            // For Accessibility set the Label for the Refresh Ctrl with the appropriate text which is
            // Update interrupted followed by Refresh and then the Watchlist Name
            if (this.className == "refreshWatchlistControl") {
                this.firstChild.innerHTML = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_INTERRUPTED + " " + WLIST.CONST.FROM_SERVER.TEXT.REFRESH_WATCHLIST + " " + this.parentNode.titleLabel.innerHTML;
            }
            else {
                this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_INTERRUPTED;
            }

        }
        else {
            this.title = WLIST.CONST.FROM_SERVER.TEXT.UPDATE_INTERRUPTED;
        }
        return;
    }

    var now = (new Date()).getTime();
    var deltaT = null;
    if (jdeMenuParent.isAccessibilityEnabled && this != null && this.className == "refreshWatchlistControl") {
        deltaT = now - this.parentNode.lastUpdate;
    }
    else {
        deltaT = now - this.lastUpdate;
    }

    var str_since_update = WLIST.getTimeDurationString(deltaT);
    var tooltip;

    if (jdeMenuParent.isAccessibilityEnabled) {
        // For Accessibility set the Label for the Refresh Ctrl with the appropriate text which is
        // Time since last update: followed by elapsed Time follwed by Refresh and then the Watchlist Name
        if (this.className == "refreshWatchlistControl") {
            this.firstChild.innerHTML = WLIST.CONST.FROM_SERVER.TEXT.TIME_SINCE_UPDATE + ' ' + str_since_update + ' ' + WLIST.CONST.FROM_SERVER.TEXT.REFRESH_WATCHLIST + ' ' + this.parentNode.titleLabel.innerHTML;
        }
        else {
            this.title = WLIST.CONST.FROM_SERVER.TEXT.TIME_SINCE_UPDATE + ' ' + str_since_update;
        }
    }
    else {
        this.title = WLIST.CONST.FROM_SERVER.TEXT.TIME_SINCE_UPDATE + ' ' + str_since_update;
    }

    //Added to synchronize the last updated time between the dropdown list and a possible E1Page element.
    if (this.data != undefined && this.data.items && this.data.items.length > 1) {
        for (var x = 0; x < this.data.items.length; x++) {
            var itemElement = document.getElementById(this.data.items[x]);
            if (!itemElement) {
                itemElement = WLIST.findElementInIframes(this.data.items[x], this.data.id);
            }
            if (itemElement) {
                itemElement.lastUpdate = this.lastUpdate;
            }
        }
    }
}

WLIST.getTimeDurationString = function (deltaT) {
    var seconds = deltaT / 1000;
    var str_since_update;

    if (seconds < 60) {
        var roundedSeconds = Math.round(seconds);
        str_since_update = roundedSeconds + ' ' + ((roundedSeconds == 1)
                                                   ? WLIST.CONST.FROM_SERVER.TEXT.TIME_SECONDS_SINGULAR
                                                   : WLIST.CONST.FROM_SERVER.TEXT.TIME_SECONDS_PLURAL);
    }
    else {
        var minutes = seconds / 60;
        if (minutes < 60) {
            str_since_update = minutes.toFixed(1) + ' ' + WLIST.CONST.FROM_SERVER.TEXT.TIME_MINUTES;
        }
        else {
            var hours = minutes / 60;
            str_since_update = hours.toFixed(1) + ' ' + WLIST.CONST.FROM_SERVER.TEXT.TIME_HOURS;
        }
    }
    return str_since_update;
}

WLIST.getUpdateThreshholdMilliseconds = function (item) {
    // If no refreshInt is set, set it to the default.
    if (item.data.refreshInt == -1) {
        item.data.refreshInt = WLIST.CONST.FROM_SERVER.DEFAULT_REFRESH_INTERVAL;
        item.data.minHour = 'm';
    }

    // Convert to minutes.
    var refreshIntervalMinutes = item.data.refreshInt;
    if (item.data.minHour == 'h') {
        refreshIntervalMinutes *= 60;
    }

    // Clamp to the hard-minimum
    refreshIntervalMinutes = Math.max(refreshIntervalMinutes, WLIST.CONST.FROM_SERVER.HARD_LIMIT_REFRESH_INTERVAL);

    return refreshIntervalMinutes * 60000;
}

WLIST.determineMaxRecordCount = function (item) {
    if (item.data.maxRecs < 0) {
        return WLIST.CONST.FROM_SERVER.DEFAULT_NUM_RECORDS;
    }

    return Math.min(item.data.maxRecs, WLIST.CONST.FROM_SERVER.HARD_LIMIT_NUM_RECORDS);
}

WLIST.updateDropdownWithNewDataWrapper = function (containerId, watchlistModelsJsonString) {
    /* containerId is an unused argument but has to exist because this method is 
    * called as a callback from a generic handler, and some of the other callbacks
    * do use the containerId.
    */

    try {
        eval('var updatedDropdownWatchlistsData = ' + watchlistModelsJsonString);
        WLIST.updateDropdownWithNewData(updatedDropdownWatchlistsData.watchlists);

        if (!(updatedDropdownWatchlistsData.securityCacheDone)) {
            WLIST.securityCacheRetries++;
            if (WLIST.securityCacheRetries <= WLIST.CONST.WAIT_FOR_SHARED_LIST.NUM_EXTRA_TRIES) {
                setTimeout(WLIST.reFetchListOfWatchlistsForDropdownMenu,
                           WLIST.CONST.WAIT_FOR_SHARED_LIST.SUBSEQUENT_DELAY);
            }
        }
        else {
            window.WLSecCacheDone = true;
        }
    }
    catch (problem) {
        // Not much we can do
    }
}

WLIST.updateDropdownWithNewData = function (newData) {
    /* This method will redo the watchlist dropdown menu based on the new list of WL's */

    WLIST.initialize();  // In case the menu has never been pulled down.
    if (!isSimplifiedNavigation) {
        // Step 0.  Hide or show WL dropdown menu based on whether or not it is empty
        if (newData.length > 0) {
            WLIST.getMenuContentParent().style.display = '';
            WLIST.getMenuContentParent().parentNode.style.display = '';
        }
        else {
            WLIST.getMenuContentParent().parentNode.style.display = 'none';
        }
    }

    // Step 1.  For any WL that is unchanged, save the last result
    for (var i = 0; i < newData.length; i++) {
        /* Try to find the WL (it may not exist, e.g. if this is a new one).  
        * Note that we assume id will never change */
        var existingWL = document.getElementById(WLIST.CONST.ITEM_PREFIX + newData[i].id);

        if (existingWL && (existingWL.data.hashValue == newData[i].hashValue)) {
            // This WL has not changed.  Save a copy of its last result (if there is one)
            if (existingWL.data.lastResult) {
                newData[i].lastResult = existingWL.data.lastResult;
            }
        }
    }

    // Step 2.  Set Dirty flag so that we destroy existing menu HTML/DOM elements before rendering
    WLIST.fullRerenderNecessary = true;

    // Step 3.  Swap in the new data.
    WLIST.data = newData;
    WLIST.organized = false;
    if (window.dropdownWatchlists) {
        window.dropdownWatchlists = newData;
    }

    // That's it.  Next time the menu is pulled down, it will rebuild from scratch, but it will 
    // use the cached results for those WL's that remain unchanged.  
}

WLIST.killRefreshThreads = function () {
    /* The most important part is that we do not innitiate any NEW refreshes.  This one line
    * takes care of that. */
    WLIST.disallowRefresh = true;

    /* For cleanliness sake, we do attempt to terminate any active threads.  Empirically, 
    * this isn't really that important because the server side will terminate the app anyway
    */
    try {
        if (WLIST.data) {
            for (var i = 0; i < WLIST.data.length; i++) {
                if (WLIST.data[i].items && WLIST.data[i].items.length != 0) {
                    for (var x = 0; x < WLIST.data[i].items.length; x++) {
                        try {
                            var itemElement = document.getElementById(WLIST.data[i].items[x]);
                            if (!itemElement) {
                                itemElement = WLIST.findElementInIframes(WLIST.data[i].items[x], WLIST.data[i].id);
                            }
                            if (itemElement &&
                                 itemElement.myXmlReq) {
                                itemElement.myXmlReq.abort();
                                itemElement.setAttribute('update', 'interrupted');
                            }
                        }
                        catch (error) {
                            //We tried.
                        }
                    }
                }

            }
        }
    }
    catch (excp) {
        // No action necessary
    }
    return;
}

WLIST.reFetchListOfWatchlistsForDropdownMenu = function () {
    var e1URL = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_SERVICE);

    e1URL.setURI(WLIST.CONST.MANAGER_SERVICE);
    e1URL.setParameter("cmd", "getNamesMenu");

    E1AJAX.sendXMLReq("POST", null, e1URL.toString(), WLIST.updateDropdownWithNewDataWrapper);
    if (WLIST.isNativeContainer && NativeContainer) {
        NativeContainer.updateData(NativeContainer.PARAM_UPDATE_DATA_WATCHLISTS);
    }
}

WLIST.onkeydownHandler = function (event) {
    if (!event) {
        // IE doesn't pass in the event; instead, there is one universal window.event
        event = window.event;
    }

    // "this" will be the object that received the event

    doMenuKeyDown(this, event, false);
}

WLIST.onfocus = function (event) {
    if (!event) {
        // IE doesn't pass in the event; instead, there is one universal window.event
        event = window.event;
    }
}

WLIST.onblur = function (event) {
    if (!event) {
        // IE doesn't pass in the event; instead, there is one universal window.event
        event = window.event;
    }
}
//Watchlist initialization for e1Page processing of a watchlist item
WLIST.e1PageInit = function (mainFlowElement, hideSecurityBlockedItems, watchlistHolder) {
    if (!WLIST.alreadyInitialized) {
        WLIST.initialize();
    }
    if (!window.WLSecCacheDone && WLIST.securityCacheRetries <= WLIST.CONST.WAIT_FOR_SHARED_LIST.NUM_EXTRA_TRIES) {
        window.e1PageWatchListInitRetries = (!window.e1PageWatchListInitRetries) ? 1 : (window.e1PageWatchListInitRetries + 1);
        if (window.e1PageWatchListInitRetries < 30) {
            setTimeout(function () { WLIST.e1PageInit(mainFlowElement, hideSecurityBlockedItems, watchlistHolder) }, 2000);
            return;
        }
    }
    WLIST.disallowRefresh = false;

    if (!WLIST.data) {
        //Add in a change to the message for a watchlist component if no watchlists exist.
        if (watchlistHolder) {
            watchlistHolder.content = 'empty';
        }
        return; //This means there are no watchlists to process for any usage here.
    }

    //A call to the watchlist component that needs building.
    if (watchlistHolder) {
        WLIST.watchlistCompHolder = watchlistHolder; //Pointer for use later
        WLIST.populateWatchlistComponent();
    }

    if (hideSecurityBlockedItems == undefined) {
        hideSecurityBlockedItems = false;
    }

    if (mainFlowElement) {
        //Get all the td nodes
        var tbody = mainFlowElement.children[0];

        for (var y = 0; y < tbody.children.length; y++) {
            var row = tbody.children[y];
            for (var x = 0; x < row.children.length; x++) {
                var node = row.children[x];
                //Get the nodes that represent the watchlist type and save them off.
                if (node.className.indexOf('wlItem') != -1) {
                    var foundit = false;
                    var WLid = node.getAttribute('wlId');
                    //Check to see if it's in the watchlist DD
                    for (var i = 0; i < WLIST.data.length; i++) {
                        var dataId = WLIST.data[i].id;
                        var wobnm = WLIST.data[i].omwObjectName;
                        if (WLid.indexOf(dataId) != -1 || WLid.indexOf(wobnm) != -1) {
                            if (!WLIST.data[i].items) {
                                WLIST.data[i].items = [node.id];
                            }
                            else {
                                if (!WLIST.alreadyInArray(WLIST.data[i].items, node)) {
                                    WLIST.data[i].items.push(node.id);
                                }
                            }
                            //Now we need to take those items and start the process of executing the watchlist item.
                            WLIST.prepareE1PageWLItem(node, WLIST.data[i]);
                            foundit = true;
                        }
                    }
                    if (!foundit) {
                        if (hideSecurityBlockedItems) {
                            WLIST.makeTileInvisible(node);
                        }
                        else {
                            node.setAttribute('update', 'failed');
                            // Handle if the watchlist Id wasn't found in the dropdown data, but was found on the server side
                            // This is probably node.watchlistInfo.exists == true and then we give the error message appropriate
                            // From the server side.
                            if (node.watchlistDetails.exists && node.watchlistDetails.exists == 'true') {
                                var descLabelValue = node.getAttribute('aria-labelledby');
                                var parentDoc = node.ownerDocument;
                                var descElement = parentDoc.getElementById(descLabelValue);
                                descElement.innerHTML = node.watchlistDetails.notDropDownWatchlistMsg;
                            }
                            if (node.className.indexOf('brokenTask') == -1) {
                                node.className += ' brokenTask';
                            }
                            if (node.className.indexOf('enabled') != -1) {
                                node.className = node.className.replace('enabled', 'disabled');
                            }
                            node.title = node.watchlistDetails.notDropDownWatchlistMsg;
                        }
                    }

                }
            }
        }
    }

    if (WLIST.CONST.FROM_SERVER) {
        for (var i = WLIST.activeRefreshThreads; i < WLIST.CONST.FROM_SERVER.NUM_REFRESH_THREADS; i++) {
            WLIST.updateNextWaitingWatchlist();
        }
    }
}

WLIST.prepareE1PageWLItem = function (item, datum) {
    item.isE1PageWatchlist = true;
    item.data = datum;
    var existingUpdate = null;
    if (item.data.items) {
        var itemElement = document.getElementById(item.data.items[0]);
        if (!itemElement) {
            itemElement = WLIST.findElementInIframes(item.data.items[0], item.data.id);
            //if watchlist is not available by id then do a lookup with obnm
            if (!itemElement) {
                itemElement = WLIST.findElementInIframes(item.data.items[0], item.data.omwObjectName);
            }
        }
        if (itemElement && itemElement.lastUpdate) {
            existingUpdate = itemElement.lastUpdate;
        }
    }
    item.lastUpdate = existingUpdate ? existingUpdate : -1;
    item.updateIntervalMillis = WLIST.getUpdateThreshholdMilliseconds(item);
    item.maxRecordCountToUse = WLIST.determineMaxRecordCount(item);

    item.onmouseover = item.recomputeTooltip = WLIST.setTooltip;

    //Setting the label and tileText    
    item.setAttribute('tileText', item.data.name);
    var descLabelValue = item.getAttribute('aria-labelledby');
    var parentDoc = item.ownerDocument;
    var descElement = parentDoc.getElementById(descLabelValue);
    descElement.innerHTML = item.data.name;

    //This must be there, but if it's not we will create it.
    var ctlValue = item.getAttribute('aria-wlcontrol-labeledby');
    var ctlElement = parentDoc.getElementById(ctlValue);
    item.refreshCtl = ctlElement;

    //Mark it so the watchlist will execute against the server.
    item.markedForUpdate = true;

    var OID = [item.data.appId, item.data.formId, item.data.version].join('_');
    var queryWOBNM = item.data.queryOBNM;

    if (item.className.indexOf('disabled') == -1) {
        //add click event
        UTIL.addEvent(item, 'click'
        , function (event) {
            if (event) {
                UTIL.stopEventBubbling(event);
                var elem = event.target ? event.target : event.srcElement;
                if (elem.id.indexOf('wlControl') != -1) {
                    item.manualRefresh = true;
                    item.reEvaluateOnServer();
                }
                else {
                    if (item.data.appId != "DATABROWSE") {
                        item.setAttribute('update', 'pending');
                        item.markedForUpdate = true;
                        runE1AppWatchlist(OID, item.data.id);
                    }
                }
            }
        });
        //add keyboard handler
        UTIL.addEvent(item, 'keydown'
        , function (event) {
            if (event.keyCode == 32 || event.keyCode == 13) {
                this.click(event);
            }
        });
    }

    item.actOnResponse = function (response) {
        WLIST.actionResponse(response, item);
    };

    item.updateCount = function (count, overrideTimestamp) {
        var countLabelValue = item.getAttribute('aria-wlcount-labeledby');
        var parentDoc = item.ownerDocument;
        var countLabel = parentDoc.getElementById(countLabelValue);
        var countStr = count;
        if (count >= item.maxRecordCountToUse) // Shouldn't ever be >, only ever ==, but using >= to be safe
        {
            countStr = item.maxRecordCountToUse + '+';
        }
        if (countLabel) {
            countLabel.innerText = countLabel.textContent = countStr;
            countLabel.style.display = '';
        }
        else {
            return;
        }

        var alertLevel = 'standardValue'; //TODO: Make a constant in the Watchlist constants when language is supported!!
        var stateTitle = 'Within threshold limit';

        if ((item.data.critThresh >= 0) && (count >= item.data.critThresh)) {
            alertLevel = 'criticalValue';
            stateTitle = 'Critical threshold exceeded ' + item.data.critThresh;
        }
        else {
            if ((item.data.warnThresh >= 0) && (count >= item.data.warnThresh)) {
                alertLevel = 'warningValue';
                stateTitle = 'Warning threshold exceeded ' + item.data.warnThresh;
            }
        }

        countLabel.className = alertLevel;
        countLabel.title = stateTitle;

        var docElement = parentDoc.documentElement;
        if (docElement.dir && docElement.dir == 'rtl') {
            countLabel.style.left = '-9px';
        }
        else {
            countLabel.style.right = '-8px';
        }
        var activeElement = WLIST.getFocuseElement();
        UTIL.forceRedrawIEOnly(item) // For the benefit of IE.       
        if (activeElement) {
            activeElement.focus();
        }
        if (overrideTimestamp) {
            item.lastUpdate = overrideTimestamp;
        }

        item.data.lastResult = {
            count: count,
            timeStamp: item.lastUpdate
        };
    };

    item.reEvaluateOnServer = function () {
        WLIST.doReEvaluation(item);
    };

    // If the item has a lastResult, than we are rebuilding one that has been run before.
    if (item.data.lastResult) {
        //Synchronize with other elements
        if (item.data.items && item.data.items.length != 0) {
            for (var x = 0; x < item.data.items.length; x++) {
                var itemElement = document.getElementById(item.data.items[x]);
                if (!itemElement) {
                    itemElement = WLIST.findElementInIframes(item.data.items[x], item.data.id);
                }
                if (itemElement && itemElement.updateCount) {
                    itemElement.updateCount(item.data.lastResult.count, item.data.lastResult.timeStamp);
                    itemElement.setAttribute('update', 'done');
                    itemElement.markedForUpdate = false;
                }
            }
            //Make sure to update the item itself.
            item.updateCount(item.data.lastResult.count, item.data.lastResult.timeStamp);
            item.setAttribute('update', 'done');
            item.markedForUpdate = false;
            WLIST.testWhetherItemNeedsUpdate(item);
        }
    }
}

WLIST.makeTileInvisible = function (tile) {    /* This function is used to fully kill an item we do not want to exist
     * We cannot delete the item or give it display:none because we still 
     * need a placeholder table cell of the appropriate width.
     * This is a copy of a function in jdeflow.js 
     * fix in the generator code if necessary.
     */

    // Destroy all contents
    while (tile.firstChild) {
        tile.removeChild(tile.firstChild);
    }
    tile.className += ' dead';
}

WLIST.populateWatchlistComponent = function () {
    //Check to see if the dropdown ran and already organized the data of the watchlists
    if (!WLIST.organized) {
        WLIST.organizeWatchlistData();
    }

    WLIST.disallowRefresh = false;

    var personalLists = null;
    var pendingApprovalLists = null;
    var reworkLists = null;
    var reservedLists = null;
    var sharedLists = null;

    //Build the My Watchlists section
    if (WLIST.personalLists && WLIST.personalLists.length > 0) {
        personalLists = WLIST.createWLCompSection('MyWL', WLIST.CONST.FROM_SERVER.TEXT.UDO_PERSONAL, WLIST.personalLists);
    }
    //Build the pending Approval Watchlists section
    if (WLIST.pendingApprovalLists && WLIST.pendingApprovalLists.length > 0) {
        pendingApprovalLists = WLIST.createWLCompSection('PendingApprovalWL', WLIST.CONST.FROM_SERVER.TEXT.UDO_PENDING_APPROVAL, WLIST.pendingApprovalLists);
    }
    //Build the Rework Watchlists section
    if (WLIST.reworkLists && WLIST.reworkLists.length > 0) {
        reworkLists = WLIST.createWLCompSection('ReworkWL', WLIST.CONST.FROM_SERVER.TEXT.UDO_REWORK, WLIST.reworkLists);
    }
    //Build the Reserved Watchlists section
    if (WLIST.reservedLists && WLIST.reservedLists.length > 0) {
        reservedLists = WLIST.createWLCompSection('ReservedWL', WLIST.CONST.FROM_SERVER.TEXT.UDO_RESERVED, WLIST.reservedLists);
    }
    //Build the Shared Watchlists section
    if (WLIST.sharedLists && WLIST.sharedLists.length > 0) {
        sharedLists = WLIST.createWLCompSection('SharedWL', WLIST.CONST.FROM_SERVER.TEXT.UDO_SHARED, WLIST.sharedLists);
    }
    /* So that the WL's will update in order, we now reconstitute the original WLIST.data array to 
    * reflect the sorted sections */
    WLIST.data = WLIST.personalLists.concat(WLIST.pendingApprovalLists, WLIST.reworkLists, WLIST.reservedLists, WLIST.sharedLists);

    //Add the sections to the component div
    if (personalLists) {
        WLIST.watchlistCompHolder.appendChild(personalLists);
        WLIST.watchlistCompHolder.personalLists = personalLists; //Pointer to this section
    }
    if (pendingApprovalLists) {
        WLIST.watchlistCompHolder.appendChild(pendingApprovalLists);
        WLIST.watchlistCompHolder.pendingApprovalLists = pendingApprovalLists; //Pointer to this section
    }
    if (reworkLists) {
        WLIST.watchlistCompHolder.appendChild(reworkLists);
        WLIST.watchlistCompHolder.reworkLists = reworkLists; //Pointer to this section
    }
    if (reservedLists) {
        WLIST.watchlistCompHolder.appendChild(reservedLists);
        WLIST.watchlistCompHolder.reservedLists = reservedLists; //Pointer to this section
    }
    if (sharedLists) {
        WLIST.watchlistCompHolder.appendChild(sharedLists);
        WLIST.watchlistCompHolder.sharedLists = sharedLists; //Pointer to this section
    }

    //Clear the empty icon
    WLIST.watchlistCompHolder.setAttribute('content', 'full');

    //Size and position the sections and list items
    if (WLIST.appliedComponentFilter) {
        WLIST.positionAndFilterWLCompSections(WLIST.appliedComponentFilter);
    }
    else {
        WLIST.positionAndFilterWLCompSections(WLIST.CONST.ALL_FILTER);
    }

    //Begin processing the watchlist items
    //Now start the threading to call the SilentAppLaunch?
    for (var i = WLIST.activeRefreshThreads; i < WLIST.CONST.FROM_SERVER.NUM_REFRESH_THREADS; i++) {
        WLIST.updateNextWaitingWatchlist();
    }
}

WLIST.createWLCompSection = function (sectionLabel, sectionText, data) {
    if (data.length == 0) {
        return null;
    }

    var section = UTIL.createElement('div', 'watchList', 'list' + sectionLabel);
    section.heading = UTIL.createElement('div', 'listHeader', 'list' + sectionLabel + 'Header');
    section.heading.txtHelper = UTIL.createElement('div', 'listHeaderTextPositionHelper');
    section.heading.txtHelper.txt = UTIL.createElement('span', 'listHeaderText');
    section.heading.txtHelper.txt.innerHTML = sectionText;
    section.heading.txtHelper.appendChild(section.heading.txtHelper.txt);
    section.heading.appendChild(section.heading.txtHelper);
    section.appendChild(section.heading);
    section.outer = UTIL.createElement('div', 'listContentOuter', 'list' + sectionLabel + 'Outer');
    section.inner = UTIL.createElement('div', 'listContentInner', 'list' + sectionLabel + 'Inner');
    section.outer.appendChild(section.inner);
    section.appendChild(section.outer);

    for (var i = 0; i < data.length; i++) {
        var wListItem = WLIST.createComponentItem(data[i]);
        if (!data[i].items) {
            data[i].items = [wListItem.id];
        }
        else {
            if (!WLIST.alreadyInArray(data[i].items, wListItem)) {
                data[i].items.push(wListItem.id);
            }
        }
        section.inner.appendChild(wListItem);
    }

    return section;
}

WLIST.createComponentItem = function (datum) {
    var itemId = WLIST.CONST.WLCOMP_ITEM_PREFIX + datum.id;
    var ariaId = WLIST.CONST.ARIA_PREFIX + datum.id;

    var item = UTIL.createElement('div', 'listItem', itemId);
    item.isCompWatchlist = true;
    item.data = datum;
    var existingUpdate = null;
    if (item.data.items) {
        var itemElement = document.getElementById(item.data.items[0]);
        if (!itemElement) {
            itemElement = WLIST.findElementInIframes(item.data.items[0], item.data.id);
        }
        if (itemElement && itemElement.lastUpdate) {
            existingUpdate = itemElement.lastUpdate;
        }
    }
    item.lastUpdate = existingUpdate ? existingUpdate : -1;
    item.updateIntervalMillis = WLIST.getUpdateThreshholdMilliseconds(item);
    item.maxRecordCountToUse = WLIST.determineMaxRecordCount(item);

    item.setAttribute('tabindex', '0');
    item.onmouseover = item.recomputeTooltip = WLIST.setTooltip;
    item.onkeydown = WLIST.onkeydownHandler;

    item.table = UTIL.createElement('table', 'listItem');
    item.table.row = UTIL.createElement('tr');
    //Build the first column, there are three to add to this row element
    item.table.col = UTIL.createElement('td', 'listIcon wlIcon');
    item.table.col.refreshCtl = UTIL.createElement('div', 'refreshWLCompControl');

    UTIL.forceRedrawIEOnly(item.table.col.refreshCtl) // For the benefit of IE.  

    //Add the refresh icon control to the column
    item.table.col.appendChild(item.table.col.refreshCtl);
    //Add the column to the row element
    item.table.row.appendChild(item.table.col);
    //Bulid the second column now
    item.table.col2 = UTIL.createElement('td', 'listText wlText');
    item.table.col2.innerHTML = datum.name;
    //Add the column to the row element
    item.table.row.appendChild(item.table.col2);
    //Build the third column now
    item.table.col3 = UTIL.createElement('td');
    item.table.col3.span = UTIL.createElement('span');
    item.table.col3.span.style.display = 'none';
    item.table.col3.appendChild(item.table.col3.span);
    item.countText = item.table.col3.span;
    //Add the column to the row element
    item.table.row.appendChild(item.table.col3);
    //Add the row to the table
    item.table.appendChild(item.table.row);
    //Add the table to the item div
    item.appendChild(item.table);

    //Here is all the processing functions and setup to do that work
    var OID = [item.data.appId, item.data.formId, item.data.version].join('_');
    var queryWOBNM = item.data.queryOBNM;

    UTIL.addEvent(item, 'click'
    , function (event) {
        if (event) {
            UTIL.stopEventBubbling(event);
            var elem = event.target ? event.target : event.srcElement;
            if (elem.className.indexOf('refreshWLCompControl') != -1) {
                item.manualRefresh = true;
                item.reEvaluateOnServer();
            }
            else {
                item.setAttribute('update', 'pending');
                item.markedForUpdate = true;
                runE1AppWatchlist(OID, item.data.id);
            }
        }
    });

    item.actOnResponse = function (response) {
        WLIST.actionResponse(response, item);
    };

    item.updateCount = function (count, overrideTimestamp) {
        var countStr = count;
        if (count >= item.maxRecordCountToUse) // Shouldn't ever be >, only ever ==, but using >= to be safe
        {
            countStr = item.maxRecordCountToUse + '+';
        }

        var alertLevel = 'standardValue';
        var stateTitle = 'Within threshold limit';
        item.setAttribute(WLIST.CONST.ALL_FILTER, 'true');

        if ((item.data.critThresh >= 0) && (count >= item.data.critThresh)) {
            alertLevel = 'criticalValue';
            stateTitle = 'Critical threshold exceeded ' + item.data.critThresh;
            item.setAttribute(WLIST.CONST.WARNING_FILTER, 'true');
            item.setAttribute(WLIST.CONST.CRITICAL_FILTER, 'true');
        }
        else {
            if ((item.data.warnThresh >= 0) && (count >= item.data.warnThresh)) {
                alertLevel = 'warningValue';
                stateTitle = 'Warning threshold exceeded ' + item.data.warnThresh;
                item.setAttribute(WLIST.CONST.WARNING_FILTER, 'true');
            }
        }

        item.countText.style.display = ''; //Reset to css default value.  Taking off the display:none.
        item.countText.setAttribute('stealth', 'true'); //Hide it while the positioning occurs
        item.countText.className = alertLevel;
        item.countText.title = stateTitle;
        item.countText.innerText = item.countText.textContent = countStr;
        //Process the centering of the count text
        var diff = item.countText.parentNode.offsetWidth - item.countText.offsetWidth;
        if (diff > 0)//Can't be less than 0 so we will just set it to zero in the case it's 0 or negative
        {
            item.countText.style.right = (diff / 2) + 'px';
        }
        else {
            item.countText.style.right = '0px';
        }
        item.countText.setAttribute('stealth', 'false'); //Show again.

        UTIL.forceRedrawIEOnly(item); // For the benefit of IE.          
        if (overrideTimestamp) {
            item.lastUpdate = overrideTimestamp;
        }

        if (count > 0) {
            item.setAttribute(WLIST.CONST.MATCHES_FILTER, 'true');
        }

        item.data.lastResult = {
            count: count,
            timeStamp: item.lastUpdate
        };

        //Run the filter again, or find a way to add the item to the filtered data.
        WLIST.positionAndFilterWLCompSections(WLIST.appliedComponentFilter);

    };

    if (item.data.items && item.data.items.length > 0) {
        var itemElement = document.getElementById(item.data.items[0]);
        if (!itemElement) {
            itemElement = WLIST.findElementInIframes(item.data.items[0], item.data.id);
        }
        if (itemElement && itemElement.updateCount) {
            var update = itemElement.getAttribute('update');
            var values = itemElement.data.lastResult;
            if (update == 'done' && values) {
                item.updateCount(values.count, values.timeStamp);
            }
            item.setAttribute('update', update);
            item.markedForUpdate = itemElement.markedForUpdate;
        }
        else {
            item.setAttribute('update', 'pending');
            item.markedForUpdate = true;
        }
    }
    else {
        item.setAttribute('update', 'pending');
        item.markedForUpdate = true;
    }

    item.reEvaluateOnServer = function () {
        WLIST.doReEvaluation(item);
    };

    // If the item has a lastResult, than we are rebuilding one that has been run before.
    if (item.data.lastResult) {
        //Synchronize with other elements
        if (item.data.items && item.data.items.length != 0) {
            for (var x = 0; x < item.data.items.length; x++) {
                var itemElement = document.getElementById(item.data.items[x]);
                if (!itemElement) {
                    itemElement = WLIST.findElementInIframes(item.data.items[x], item.data.id);
                }
                if (itemElement && itemElement.updateCount) {
                    itemElement.updateCount(item.data.lastResult.count, item.data.lastResult.timeStamp);
                    itemElement.setAttribute('update', 'done');
                    itemElement.markedForUpdate = false;
                }
            }
            //Make sure to update the item itself.
            item.updateCount(item.data.lastResult.count, item.data.lastResult.timeStamp);
            item.setAttribute('update', 'done');
            item.markedForUpdate = false;
            WLIST.testWhetherItemNeedsUpdate(item);
        }
    }

    return item;
}

WLIST.positionAndFilterWLCompSections = function (filter) {

    if (!WLIST.watchlistCompHolder || !filter) {
        //No watchlist holder element so we can continue
        return;
    }
    //Default to process only this watchlist if the frame search returns nothing
    var compToProcess = WLIST.watchlistCompHolder;
    //If it does do the logic to filter the content.
    var docElem = compToProcess.ownerDocument;

    //Get the iframes for the e1pages
    var iFrameElems = WLIST.getE1PageIframes();

    //iterate through each one to see if it has a watchlist component in it.
    //If the iframes array is empty, then just process the default component
    if (iFrameElems && iFrameElems.length > 1) {
        for (var x = 0; x < iFrameElems.length; x++) {
            docElem = iFrameElems[x].contentDocument;
            compToProcess = docElem.getElementById('watchlistContentHolder');
            if (compToProcess) {
                WLIST.executePositionFilter(compToProcess, docElem, filter);
            }
        }
    }
    else {
        WLIST.executePositionFilter(compToProcess, docElem, filter);
    }
}

WLIST.showSection = function (content) {
    if (navigator && navigator.appName == "Microsoft Internet Explorer") {
        // Because IE incorrectly handles inherited opacity
        for (var i = 0; i < content.children.length; i++) {
            //content.children[i].filter = 'alpha(opacity=0)';
            ANIM.fadeObjIE(content.children[i], 0, 1, 500);
            UTIL.forceRedrawIEOnly(content.children[i]);
        }
    }
    else {
        for (var i = 0; i < content.children.length; i++) {
            content.children[i].opacity = '0';
            ANIM.fadeObj(content.children[i], 0, 1, 500);
        }
        ANIM.fadeObj(content, 0, 1, 500);
    }

}

WLIST.hideSection = function (content) {
    if (navigator && navigator.appName == "Microsoft Internet Explorer") {
        // Because IE incorrectly handles inherited opacity
        for (var i = 0; i < content.children.length; i++) {
            content.children[i].filter = 'alpha(opacity=1)';
            ANIM.fadeObjIE(content.children[i], 1, 0, 500);
        }
        ANIM.fadeObjIE(content, 1, 0, 500);
    }
    else {
        for (var i = 0; i < content.children.length; i++) {
            content.children[i].opacity = '1';
            ANIM.fadeObj(content.children[i], 1, 0, 500);
        }
        ANIM.fadeObj(content, 1, 0, 500);
    }
}

WLIST.doFilter = function (filter) {
    var type = filter.className;
    //Checks to see is we do the work to filter
    if (type && WLIST.appliedComponentFilter) {
        if (type == WLIST.appliedComponentFilter) {
            //No work to do, cause the user clicked the filter that is already applied.
            return;
        }
    }
    else {
        //If one of the above doesn't exist we will not be able to process any further
        return;
    }

    if (WLIST.watchlistCompHolder) {
        WLIST.positionAndFilterWLCompSections(type);
    }

}

WLIST.actionResponse = function (response, item) {
    try {
        eval('item.response = ' + response);
    }
    catch (problem) {
        item.response = null;
    }
    //Test for permission issues
    try {
        var test = item.data;
    }
    catch (error) {
        //Nothing we can do but return;
        return;
    }

    if (item.response && (!item.response.error)) {
        if (item.response.watchlistUpdated == false) {
            item.setAttribute('update', 'done');
            item.markedForUpdate = false;
            return;
        }

        if (item.data.items && item.data.items.length > 0) {
            for (var x = 0; x < item.data.items.length; x++) {
                var itemElements = new Array();
                var itemElement = document.getElementById(item.data.items[x]);
                if (!itemElement) {
                    itemElements = WLIST.findElementsInIframes(item.data.items[x], item.data.id);
                    //if still not found, fetch by WOBNM
                    if (itemElements.length == 0) {
                        itemElements = WLIST.findElementsInIframes(item.data.items[x], item.data.omwObjectName);
                    }
                }
                else
                    itemElements.push(itemElement);

                for (var i = 0; i < itemElements.length; i++) {
                    var itemElement = itemElements[i];
                    if (itemElement && itemElement.updateCount) {
                        itemElement.updateCount(item.response.rowcount.records, item.response.lastRunTime);
                        itemElement.setAttribute('update', 'done');
                        itemElement.markedForUpdate = false;
                    }
                }
            }
        }
        else {
            item.updateCount(item.response.rowcount.records, item.response.lastRunTime);
            item.setAttribute('update', 'done');
            item.markedForUpdate = false;
        }
    }
    else {
        if (item) {
            if (item.data.items && item.data.items.length > 1) {
                for (var x = 0; x < item.data.items.length; x++) {
                    var itemElement = document.getElementById(item.data.items[x]);
                    if (!itemElement) {
                        itemElement = WLIST.findElementInIframes(item.data.items[x], item.data.id);
                        //if still not found, fetch by WOBNM
                        if (!itemElement) {
                            itemElement = WLIST.findElementInIframes(item.data.items[x], item.data.omwObjectName);
                        }
                    }
                    if (itemElement) {
                        itemElement.setAttribute('update', 'failed');
                    }
                }
            }
            item.setAttribute('update', 'failed');
        }
        else {
            WLIST.killRefreshThreads(); //The item doesn't exist anymore.
            return;
        }
    }

    if (item.isDDWatchlist) {
        if (item.refreshCtl.getAttribute("aria-pressed") == 'true') {
            if (jdeMenuParent.isAccessibilityEnabled) {
                UTIL.forceRedraw(item.refreshCtl); // With JAWS on, In Firefox focus is lost after Refresh unless we do a Redraw. So do Redraw for all Browsers.
            }
            else {
                UTIL.forceRedrawIEOnly(item.refreshCtl);
            }
            item.refreshCtl.focus();
            item.refreshCtl.setAttribute('aria-pressed', 'false');
        }
        else {
            UTIL.forceRedrawIEOnly(item.refreshCtl) // For the benefit of IE.  
        }
    }
    else {
        if (item.isE1PageWatchlist) {
            var activeElement = WLIST.getFocuseElement();
            UTIL.forceRedrawIEOnly(item.refreshCtl); // For the benefit of IE.  
            UTIL.forceRedraw(item.refreshCtl);
            if (activeElement) {
                activeElement.focus();
            }
        }
        if (item.isCompWatchlist) {
            UTIL.forceRedrawIEOnly(item.table.col.refreshCtl); // For the benefit of IE.  
            UTIL.forceRedraw(item.table.col.refreshCtl);
        }
    }
    item.recomputeTooltip();

    // We're done with the xml request
    item.myXmlReq = null;

    // Check for more watchlists to update and use this thread to process one.

    if (!(item.manualRefresh)) {
        // This is an auto-refresh thread, and we should process next pending list.
        WLIST.activeRefreshThreads--;
        WLIST.updateNextWaitingWatchlist();
    }
    else {
        // This was a one-time, manual refresh and we don't want to do anything else except
        // clear the flag
        item.manualRefresh = false;
        // this was run from a notification poll. so process the next notification watchlist in the queue
        if (item.fromNotification) {
            item.fromNotification = false;
            WLIST.activeNotificationRefreshThreads--
            WLIST.updateNextWaitingNotification();
        }
    }
}

// for get the refresh holder div for watch list item tile
WLIST.getFocuseElement = function () {
    try {
        var activeElement = document.activeElement.contentDocument.activeElement.contentDocument.activeElement;
        return activeElement;
    }
    catch (e) {
        return null;
    }
}

WLIST.doReEvaluation = function (item, noUserSessionUpdate) {
    var id = item.data.id;

    if (WLIST.disallowRefresh) {
        // We have received the kill signal.  Terminate.

        if (!item.manualRefresh) {
            WLIST.activeRefreshThreads--;
        }

        return;
    }

    //Synchronize with other elements
    if (item.data.items && item.data.items.length != 0) {
        for (var x = 0; x < item.data.items.length; x++) {
            var itemElements = new Array();
            var itemElement = document.getElementById(item.data.items[x]);
            if (!itemElement) {
                itemElements = WLIST.findElementsInIframes(item.data.items[x], item.data.id);
                if (itemElements.length == 0) {
                    itemElements = WLIST.findElementsInIframes(item.data.items[x], item.data.omwObjectName);
                }
            }
            else
                itemElements.push(itemElement);

            for (var i = 0; i < itemElements.length; i++) {
                var itemElement = itemElements[i];
                itemElement.setAttribute('update', 'inProgress');
                itemElement.markedForUpdate = false;
                if (itemElement.isDDWatchlist) {
                    UTIL.forceRedrawIEOnly(itemElement.refreshCtl) // For the benefit of IE.  
                }
                if (itemElement.isE1PageWatchlist) {
                    var activeElement = WLIST.getFocuseElement();
                    UTIL.forceRedrawIEOnly(itemElement.refreshCtl) // For the benefit of IE. 
                    UTIL.forceRedrawIEOnly(itemElement) // For the benefit of IE.  
                    if (activeElement) {
                        activeElement.focus();
                    }
                }
                if (itemElement.isCompWatchlist) {
                    UTIL.forceRedrawIEOnly(itemElement.table.col.refreshCtl);
                }
                itemElement.data = item.data;
            }
        }
    }

    var xmlReqObject;
    if (window.XMLHttpRequest) {
        xmlReqObject = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlReqObject = new ActiveXObject("MSXML2.XMLHTTP");
    }

    if (xmlReqObject) {
        xmlReqObject.onreadystatechange = function () {
            if (xmlReqObject.readyState == 4) {
                //Test for permission issues
                try {
                    var test = item.data;
                }
                catch (error) {
                    //Try to fix the refresh thread count and keep things moving
                    WLIST.activeRefreshThreads--;
                    WLIST.updateNextWaitingWatchlist();
                    //Nothing we can do but return now;
                    return;
                }
                item.actOnResponse(xmlReqObject.responseText);
            }
        };

        var postURL = "SilentAppLaunch?WatchlistId=" + id + "&timeStamp=" + (new Date()).getTime(); //Changed to use the new format
        if (item.manualRefresh == true || item.fromNotification == true) {
            postURL += "&forceUpdate=true";
        }
        if (noUserSessionUpdate == true) {
            postURL += "&noSessionUpdate=true";
        }
        var urlFactory = _e1URLFactory.createInstance(_e1URLFactory.URL_TYPE_RES);
        urlFactory.setURI(postURL);
        postURL = urlFactory.toString();

        item.myXmlReq = xmlReqObject;

        xmlReqObject.open("GET", postURL, true);
        xmlReqObject.send();
    }
}

WLIST.addToGlobalData = function (wListItem) {
    if (WLIST.data) {
        for (var i = 0; i < WLIST.data.length; i++) {
            var matchId = WLIST.data[i].id;
            if (wListItem.data) {
                var itemId = wListItem.data.id;
                if (matchId === itemId) {
                    if (!WLIST.data[i].items) {
                        WLIST.data[i].items = [wListItem.id];
                    }
                    else {
                        if (WLIST.data[i].items.indexOf(wListItem) == -1) {
                            WLIST.data[i].items.push(wListItem.id);
                        }
                    }
                    break;
                }
            }
        }
    }
}

WLIST.setFilterActive = function (filter, doc) {
    var all = doc.getElementById(WLIST.CONST.ALL_FILTER);
    var matches = doc.getElementById(WLIST.CONST.MATCHES_FILTER);
    var crit = doc.getElementById(WLIST.CONST.CRITICAL_FILTER);
    var warn = doc.getElementById(WLIST.CONST.WARNING_FILTER);
    var active = doc.getElementById(filter);
    try {
        all.setAttribute('active', 'false');
        matches.setAttribute('active', 'false');
        crit.setAttribute('active', 'false');
        warn.setAttribute('active', 'false');
        active.setAttribute('active', 'true');
        UTIL.forceRedrawIEOnly(all);
        UTIL.forceRedraw(all);
        UTIL.forceRedrawIEOnly(matches);
        UTIL.forceRedraw(matches);
        UTIL.forceRedrawIEOnly(warn);
        UTIL.forceRedraw(warn);
        UTIL.forceRedrawIEOnly(crit);
        UTIL.forceRedraw(crit);
        UTIL.forceRedrawIEOnly(active);
        UTIL.forceRedraw(active);
    }
    catch (error) {
        //Best attempt to set the active filter element
        WLIST.log(error);
    }
}

WLIST.alreadyInArray = function (array, node) {
    for (var y = 0; y < array.length; y++) {
        if (array[y] === node) {
            return true;
        }
    }
    return false;
}

WLIST.findElementInIframes = function (id, wlId) {
    var appIframe = document.getElementById('e1menuAppIframe');
    var node;
    if (appIframe) {
        var iFrameElems = appIframe.contentDocument.getElementsByTagName('iframe');
        for (var i = 0; i < iFrameElems.length; i++) {
            try {
                var doc = iFrameElems[i].contentDocument;
                node = doc.getElementById(id);
                if (node && wlId == node.getAttribute('wlId'))
                    return node;
                else // it could be in a content frame of a composite page
                {
                    var iSubFrameElems = doc.getElementsByTagName('iframe');
                    for (var j = 0; j < iSubFrameElems.length; j++) {
                        try {
                            var subDoc = iSubFrameElems[j].contentDocument;
                            node = subDoc.getElementById(id);
                            if (node && wlId == node.getAttribute('wlId'))
                                return node;
                        }
                        catch (e) // skip the frame throw permission deny exception
                    {
                        }
                    }
                }
            }
            catch (e) // skip the frame throw permission deny exception
          {
            }
        }
    }
    return null;
}


WLIST.findElementsInIframes = function (id, wlId) {
    var appIframe = document.getElementById('e1menuAppIframe');
    var nodes = new Array();
    if (appIframe) {
        var iFrameElems = appIframe.contentDocument.getElementsByTagName('iframe');
        for (var i = 0; i < iFrameElems.length; i++) {
            try {
                var doc = iFrameElems[i].contentDocument;
                node = doc.getElementById(id);
                if (node && wlId == node.getAttribute('wlId'))
                    nodes.push(node);

                var iSubFrameElems = doc.getElementsByTagName('iframe');
                for (var j = 0; j < iSubFrameElems.length; j++) {
                    try {
                        var subDoc = iSubFrameElems[j].contentDocument;
                        node = subDoc.getElementById(id);
                        if (node && wlId == node.getAttribute('wlId'))
                            nodes.push(node);
                    }
                    catch (e) // skip the frame throw permission deny exception
                    {
                    }
                }
            }
            catch (e) // skip the frame throw permission deny exception
          {
            }
        }
    }
    return nodes;
}

WLIST.getE1PageIframes = function () {
    var iFrameElems = null;
    var appIframe = document.getElementById('e1menuAppIframe');
    if (appIframe) {
        iFrameElems = appIframe.contentDocument.getElementsByTagName('iframe');
    }
    return iFrameElems;
}

WLIST.executePositionFilter = function (compHolderToProcess, docElem, filter) {
    if (!compHolderToProcess || !docElem || !filter) {
        //Can't continue processing
        return;
    }

    //Var to hold the location of the next item to add to the holder.
    var accumTopOffset = 0;
    //Process each watchlist element
    for (var i = 0; i < compHolderToProcess.children.length; i++) {
        var sectionFilterCount = 0;
        var section = compHolderToProcess.children[i];
        if (section.className === 'watchList') {
            section.style.top = accumTopOffset + 'px';
            accumTopOffset += 24; //Add the header css height
        }
        else {
            continue;
        }
        var accumItemOffsetTop = 0;
        var outerHolder = docElem.getElementById(section.id + 'Outer');
        var innerHolder = docElem.getElementById(section.id + 'Inner');

        if (innerHolder && outerHolder) {
            for (var j = 0; j < innerHolder.children.length; j++) {
                var wListItem = innerHolder.children[j];
                if (wListItem.className === 'listItem') {
                    var filterItem = false;
                    switch (filter) {
                        case WLIST.CONST.MATCHES_FILTER:
                            if (wListItem.getAttribute(WLIST.CONST.MATCHES_FILTER)) {
                                filterItem = true;
                            }
                            break;
                        case WLIST.CONST.WARNING_FILTER:
                            if (wListItem.getAttribute(WLIST.CONST.WARNING_FILTER)) {
                                filterItem = true;
                            }
                            break;
                        case WLIST.CONST.CRITICAL_FILTER:
                            if (wListItem.getAttribute(WLIST.CONST.CRITICAL_FILTER)) {
                                filterItem = true;
                            }
                            break;
                        case WLIST.CONST.ALL_FILTER:
                            filterItem = true;
                            break;
                    }
                    if (!filterItem) {
                        wListItem.setAttribute('hide', 'true');
                        wListItem.style.top = -100 + 'px';
                    }
                    else {
                        wListItem.style.top = accumItemOffsetTop + 'px';
                        accumItemOffsetTop += 32;
                        accumTopOffset += 32;
                        sectionFilterCount++;
                        wListItem.setAttribute('hide', 'false');
                    }
                    UTIL.forceRedrawIEOnly(wListItem);
                }
            }
            if (sectionFilterCount > 0) {
                //Set the height of the inner holder based on a filter
                var hite = sectionFilterCount * 32;
                innerHolder.style.height = outerHolder.style.height = hite + 'px';
                innerHolder.style.top = '0px';
                //Set the sections height
                section.style.height = hite + 24 + 'px';
            }
            section.sectionFilterCount = sectionFilterCount;
        }

        //Set the height of the holder to match where the next section would be put
        compHolderToProcess.style.height = accumTopOffset + 'px';

        var wlCompTable = docElem.getElementById('wlCompTable');
        var filterHolder = docElem.getElementById('filterHolder');
        if (compHolderToProcess.parentNode.offsetHeight < accumTopOffset) {
            compHolderToProcess.parentNode.style.overflowY = 'scroll';
            wlCompTable.style.width = compHolderToProcess.parentNode.style.width = compHolderToProcess.style.width = filterHolder.style.width = '200px';
        }
        else {
            compHolderToProcess.parentNode.style.overflowY = '';
            wlCompTable.style.width = compHolderToProcess.parentNode.style.width = compHolderToProcess.style.width = filterHolder.style.width = '';
        }
        //Show the section using the animation if possible
        //TODO: This may be changed based on UI review, but for now let's fade it in if we can.
        if (ANIM) {
            if (section.sectionFilterCount > 0) {
                WLIST.showSection(section);
                UTIL.forceRedrawIEOnly(section);
            }
            else {
                WLIST.hideSection(section);
                UTIL.forceRedrawIEOnly(section);
            }
        }
        else {
            if (section.sectionFilterCount > 0) {
                section.style.opacity = 1;
                section.style.filter = 'alpha(opacity=100)';
                UTIL.forceRedrawIEOnly(section); // For the benefit of IE.
            }
            else {
                //Apply the css style which is to hide the section
                section.style.opacity = '';
                section.style.filter = '';
                UTIL.forceRedrawIEOnly(section); // For the benefit of IE.          
            }
        }
    }

    //Set the filter that is applied at this time.
    WLIST.appliedComponentFilter = filter;
    //Set the active filter 
    WLIST.setFilterActive(filter, docElem);
}

WLIST.updateNextWaitingNotification = function () {
    // Process next watchlist in the queue notification queue
    WLIST.activeNotificationRefreshThreads++
    var queue = WLIST.notificationEvaluationQueue;
    if (queue.length > 0) {
        // kick off the work threads to start request for watch list update
        var id = queue.shift();
        var item = document.getElementById(WLIST.CONST.ITEM_PREFIX + id);
        if (item) {
            item.manualRefresh = true;
            item.fromNotification = true;
            WLIST.doReEvaluation(item, true);
        }
        else {
            // Watchlist item not found. The server and client are out of sync. 
            // Fix it by rebuilding the menu
            console.log("Didn't find the menu item should we rebuild it?");
            WLIST.fullRerenderNecessary = true;
            WLIST.renderMenu(true);
            WLIST.activeNotificationRefreshThreads--;
        }
        return;
    }

    WLIST.activeNotificationRefreshThreads--;
}

WLIST.addToNotificationQueue = function (id) {
    var queue = WLIST.notificationEvaluationQueue;
    if (queue.indexOf(id) == -1) {
        // only add if the id is not already waiting in queue
        queue.push(id);
    }
    // only execute if there are current notification watchlist queue threads available
    // otherwise the completion of a currently processing notification watchlist will
    // kick off the next one
    for (var i = WLIST.activeNotificationRefreshThreads; i < WLIST.CONST.NOTIFICATION_QUEUE_SIZE; i++) {
        WLIST.updateNextWaitingNotification();
    }
}

WLIST.evaluateByIdArray = function (dirtyWatchList) {
    // Only update one at a time when passing in an array of watchlists to evaluate
    // Build the menu if it has not already been built
    var menuContent = WLIST.getMenuContent();
    if (!menuContent.alreadyPopulated) {
        WLIST.renderMenu(true);
    }

    // add each watchlist id to the notification process queue
    for (var i = 0; i < dirtyWatchList.length; i++) {
        WLIST.addToNotificationQueue(dirtyWatchList[i]);
    }
}