var topActiveNavigationMenu = null;
function onKeyDownHandler(event) {
    var isIE = (document.documentMode !== undefined);
    var evt = event;
    if ((evt == null) || (evt == undefined)) {
        evt = window.event;
    }
    if (isCtrlDown(evt)) {
        // Ctrl
        if (evt.keyCode == 77 && !evt.shiftKey) // Ctrl+M: SetFocus on Home Label in Topmenu Bar
        {
            document.getElementById("drop_home").focus();
            return false;
        }
        // Ctrl+Shft
        else if (isShiftDown(evt)) {
            if (evt.keyCode == 75 || evt.keyCode == 191)  // Ctrl+Shift+K or Ctrl+Shift+?:  Summary of all the hotkeys provided by the system 
            {
                loadHotKeySummaryWindow();
                return false;
            }
        }
    }
    else if (isAltDown(evt)) {

        if (evt.keyCode == 220)  // Alt+\: Set focus on the fast path in the menu explorer
        {
            setFocusOnFastPath(true);
            return false;
        }
        else if (evt.keyCode == 78)  // Alt+N: Opens a new window
        {
            var isWSRP = (gContainerId == E1URLFactory.prototype.CON_ID_WSRP);
            //Alt+N is not supported for WSRP
            if (isWSRP) {
                return;
            }
            launchNewWindow();
            return false;
        }
        else if (evt.keyCode == 89)  // Alt+Y: set Focus on Open Applications 
        {
            document.getElementById("drop_openapps").focus();
            return false;
        }
        else if (evt.keyCode == 85)  // Alt+U: set Focus on Recent Reports 
        {
            document.getElementById("drop_reportmenu").focus();
            return false;
        }
        else if (evt.keyCode == 73)  // Alt+I: set Focus on Favorites
        {
            document.getElementById("drop_fav_menus").focus();
            return false;
        }
        else if (evt.keyCode == 87)  // Alt+W: set Focus on Watchlists
        {
            document.getElementById("drop_watchlist_menus").focus();
            return false;
        }
    }

    if (evt.keyCode == 120) // F9 key to cycle the open application list
    {
        topActiveNavigationMenu = document.getElementById("drop_openapps");
        //any dropdown is opened, Close it
        hideAllMainMenus();
        hideAllBreadcrumbMenus();
        hideAllFavMenus(); // hide Favorites
        hideRRMenu(); // hide recent reports
        hideMenu('e1MRoleTable');  // hide Roles
        hideMenu('preferenceDiv'); // hide preferences
        //mimic the Click Event on open apps
        document.getElementById("drop_openapps").click();
        showMenu('OPENAPPTABLE', event);

        return MENUUTIL.processOpenApplicationList(1, true);
    }

    if (evt.keyCode == 13) // capture Enter
    {
        if (isIE && isMOActivexEnabled(event))
            return false; //event handled in Activex control, so stop event bubbling.


        if (!MENUUTIL.allowNewWindow()) {
            return true;
        }
        var menuObject = jdeMenuParent;
        var target = (evt.srcElement) ? evt.srcElement : evt.target;
        if (target.nodeName == "INPUT" && target.type == "checkbox") {
            // this prevents window refresh when enter key is pressed within pref dialog (seems to only be on Firefox)
            return false;
        }
        if (isCtrlDown(evt) || isShiftDown(evt)) {
            if (target.tagName == "A" && target.href != null && (target.href.indexOf("RunNewApp(") != -1 || target.href.indexOf("RunOldApp(") != -1)) {
                menuObject.oneTimeNewWindowLaunch = true;
                eval(target.href);
                return false;
            }
            else if (target.id == "TE_FAST_PATH_BOX") {
                menuObject.oneTimeNewWindowLaunch = true;
                document.getElementById("e1MFastpathForm").submit();
            }
            return false;
        }
        // for carousel tiles or list items, execute the mouseup event to activate the tile
        if (target.className == "caroTile" || target.className == "listItem") {
            CARO.activateTile(target);
            return false;
        }
        return true;
    }

    if (evt.keyCode == 37 || evt.keyCode == 38 || evt.keyCode == 39 || evt.keyCode == 40 || evt.keyCode == 9) // capture Right-Left-Up-Down arrow keys and tab key also.
    {
        if (isIE && isMOActivexEnabled(event))
            return false; //event handled in Activex control, so stop event bubbling.
    }

    return true;
}

function isMOActivexEnabled(event) {
    //Added OBJECT tag also here.
    var target = (event.srcElement) ? event.srcElement : event.target;
    if (target.nodeName == "OBJECT") {
        var activeId = target.id;
        if (activeId != null && activeId.indexOf("jdeRTF") > -1) //if object tag and  id with "jdeRTF" and IE
        {
            return true;
        }
    }
    return false;
}

function onKeyUpHandler(event) {
}

function loadHotKeySummaryWindow() {
    window[jdeMenuParent.menuNamespace + "LaunchHotKeysPage"]();
}

function isCtrlDown(keyEvent) {
    var evt = keyEvent;
    var userAgent = navigator.userAgent.toUpperCase();
    var isIOS = (userAgent.indexOf("IPAD") > -1) || (userAgent.indexOf("IPOD") > -1) || (userAgent.indexOf("IPHONE") > -1);
    if (isIOS) {
        return false;
    }
    else {
        return evt.ctrlKey;
    }
}

function isAltDown(keyEvent) {
    var evt = keyEvent;

    if (document.all) {
        return (evt.altLeft || (evt.altKey && !isRightAltKeyDisabled(evt)))
    }
    else {
        return evt.altKey || evt.metaKey;
    }
}

function isShiftDown(keyEvent) {
    var evt = keyEvent;
    return evt.shiftKey;
}

function isRightAltKeyDisabled(evt) {
    if (jdeMenuParent.rightAltKeyDisabled != null) {
        if (document.all) {
            return jdeMenuParent.rightAltKeyDisabled;
        } else {
            return false;
        }
    }
    else {
        return false;
    }
}

function getFirstMenuItem(element) {
    // find the first child that has children
    var start = element.firstChild;
    while (start) {
        if (start.children && start.children.length > 0)
            break;
        start = start.nextSibling;
    }
    // stop search when we find a decendent who's id is 'menuItem...'
    if (start && start.firstElementChild === undefined) // IE8
    {
        while (start.firstChild) {
            start = start.firstChild;
            if (start.id.substring(0, 8) == "menuItem")
                return start;
        }
    }
    else if (start) {
        while (start.firstElementChild) {
            start = start.firstElementChild;
            if (start.id.substring(0, 8) == "menuItem")
                return start;
        }
    }
}

function bcFocus(menuRootId) {
    var menuRootElement = document.getElementById(menuRootId);
    if (menuRootElement) {
        var firstChildNodeElement = getFirstMenuItem(menuRootElement);
        if (firstChildNodeElement) {
            firstChildNodeElement.focus();
        }
    }
}

function doMenuKeyDown(eventElement, event, isFlyout, popupContentDivId, breadcrumbNodeId) {
    var keyCode = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
    var parentContainer = document.getElementById("webTree");
    var srcElement = document.all ? event.srcElement : event.target;
    var labelID = getLabelId(eventElement);
    var leftArrow = 37;
    var rightArrow = 39;

    if (jdeMenuParent.isRtlEnabled) {
        leftArrow = 39;
        rightArrow = 37;
    }

    if ((document.activeElement && document.activeElement.className && document.activeElement.className.indexOf("MenuBarFocusableDiv") >= 0) || eventElement.id == 'userAndEnvContainer' ||  // set the top level menu or breadcrumb to allow re-focus on ESCAPE
       eventElement.id == 'bchistory' || eventElement.id.indexOf('breadcrumb') == 0)
        topActiveNavigationMenu = document.activeElement;

    // convert enter/space key to down arrow key if the it is from the level 0 menu.
    if ((keyCode == 13 || keyCode == 32) && (labelID == "drop_mainmenu" || labelID == "drop_openapps" || labelID == "drop_fav_menus" || labelID == "drop_reportmenu" || labelID == "drop_watchlist_menus" || eventElement.id == 'userAndEnvContainer' || eventElement.id == 'Role_downarrow' || eventElement.id == "preferences"))
        keyCode = 40;

    switch (keyCode) {
        case 27: //escape
            //remove onClick eventHandler
            removeonClickEvent(document, 'click', dropdownMenuEventHandler);
            hideAllDropdownMenus();
            dismissFloatingDiv();
            //cancel event bubbling..
            stopEventBubbling(event);

            //set the focus back on the menu parent item 
            if (topActiveNavigationMenu) {
                topActiveNavigationMenu.focus();
            }
            return false;
        case 40: // down Arrow
            if (labelID == "e1MFastpathForm" || srcElement.id == "TE_FAST_PATH_BOX") //fast path
            {
                var curVal = document.getElementById("TE_FAST_PATH_BOX").value;
                // allow default browser auto-complete if the fast path value is non-blank.
                if (curVal == '') {
                    var foundElement = getFirstMenuItem(document.getElementById('e1MMenuRootInner'));
                    if (foundElement != null && foundElement != undefined) {
                        foundElement.focus();
                    }
                    stopEventBubbling(event);
                    return false;
                }
                return true;
            }
            else if (labelID == "drop_mainmenu") //mainmenu
            {
                //Hide the Other dropdown's if incase Opened
                hideAllDropdownMenus(); //hide Favorites
                //show the mainmenu dropdown on the down Arrow
                showMenu('e1MMenuRoot', event);
            }
            else if (eventElement.id.indexOf('menuItemBc') == 0) {
                var foundElement;
                var sibling = eventElement.parentNode;
                while (sibling.nextSibling) {
                    sibling = sibling.nextSibling;
                    foundElement = sibling.firstChild;
                    if (foundElement != null) {
                        eventElement.className = "MenuNormal";
                        foundElement.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
            }
            else if ((eventElement.id.substring(0, 8) == "menuItem" || eventElement.id == "ActionsSubMenuTable") && eventElement.className == "HoverMenuItem") {
                var foundElement;
                var sibling = eventElement.parentNode.parentNode;
                while (sibling.nextSibling) {
                    sibling = sibling.nextSibling;
                    foundElement = (isIE8OrEarlier) ? sibling.firstChild.firstChild : sibling.firstChild.lastChild;

                    if (foundElement != null && foundElement.nodeName == "TABLE") {
                        foundElement.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }

                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (labelID && labelID.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) == 0) // a breadcrumb dropdown
            {
                var menuRootId = labelID;
                var containerLabelPos = labelID.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE);
                if (containerLabelPos > 0) {
                    menuRootId = menuRootId.substring(0, (containerLabelPos));
                }

                BREADCRUMBS.loadMenu(breadcrumbNodeId, menuRootId);
                showMenu(menuRootId, event, bcFocus);
                event.returnValue = false;
                event.cancelBubble = true;
            }
            else if (labelID == BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ARROW) // bc history dropdown
            {
                var drpdown = document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE)
                if (drpdown) {
                    if (drpdown.className == "hideMenu") {
                        BREADCRUMBS.loadBreadCrumbsHistory(event);
                    }

                    var firstChildNodeElement = getFirstMenuItem(drpdown);
                    if (firstChildNodeElement) {
                        firstChildNodeElement.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
            }
            else if (labelID == "drop_fav_menus") //favorites
            {
                //Hide the Other dropdown's if incase Opened
                hideAllDropdownMenus();
                //show the favorties dropdown on the down Arrow
                showMenu('e1MMenuFav', event);

                var favorites = document.getElementById('e1MMenuFav');
                var manageFavorites = favorites.firstChild.firstChild.firstChild;
                if ("HoverMenuItem" != manageFavorites.className) {
                    manageFavorites.className = "HoverMenuItem";
                    manageFavorites.focus();
                }
                else {
                    manageFavorites.className = "MenuNormal";
                }
            }
            else if (labelID == "drop_openapps") //open Apps
            {
                hideAllDropdownMenus();

                showMenu('OPENAPPTABLE', event);
                //set Focus on the next Open application
                MENUUTIL.processOpenApplicationList(0, false);
                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (eventElement.id.indexOf("OCLTR_") != -1) //open Apps
            {
                //set Focus on the next Open application
                MENUUTIL.processOpenApplicationList(1, false);
                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (labelID == "drop_reportmenu") {
                hideAllDropdownMenus();

                showRecRptsMenu('recRptsDiv', event);

                var rrMenuDiv = document.getElementById('recRptsDiv');
                var sibling = rrMenuDiv.firstChild;
                if (sibling != null && sibling.nodeName == "TABLE") {
                    sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
            }
            else if (eventElement.id.indexOf("RecRptsTable") != -1) {
                var sibling = eventElement.nextSibling;
                if (sibling != null && sibling.nodeName == "TABLE") {
                    sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (labelID == "drop_watchlist_menus") // Watchlist DropDown Menu
            {
                //Hide the Other dropdown's if incase Opened
                hideAllDropdownMenus(); // hide MainMenus
                //show the watchlist dropdown on the down Arrow
                showMenu('e1MMenuWatchlists', event);

                var wlMenuDiv = document.getElementById('e1MMenuWatchlists');
                var sibling = wlMenuDiv.firstChild.firstChild;

                if (sibling != null && sibling.nodeName == "DIV" && sibling.className == "watchlistSectionHeading") {
                    sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
            }
            else if (eventElement.className == "watchlistSectionHeading") {
                var sibling = eventElement.nextSibling.firstChild;
                if (sibling != null && sibling.nodeName == "A" && sibling.className == "refreshWatchlistControl") {
                    sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
            }
            else if (eventElement.className == "refreshWatchlistControl") {
                var sibling = eventElement.nextSibling;
                if (sibling != null && sibling.nodeName == "SPAN" && sibling.className == "wlLabelWrapper") {
                    sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
            }
            else if (eventElement.className == "wlLabelWrapper") {
                while (eventElement.parentNode.nextSibling) {
                    var sibling = eventElement.parentNode.nextSibling.firstChild;
                    if (sibling != null && sibling.nodeName == "A" && sibling.className == "refreshWatchlistControl") {
                        sibling.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
                if (eventElement.parentNode.nextSibling == null) {
                    if (eventElement.parentNode.parentNode.nextSibling != null) {
                        var nextSibling = eventElement.parentNode.parentNode.nextSibling.nextSibling.firstChild;
                        if (nextSibling != null && nextSibling.nodeName == "DIV" && nextSibling.className == "watchlistSectionHeading") {
                            nextSibling.focus();
                            event.returnValue = false;
                            event.cancelBubble = true;
                            return false;
                        }
                    }
                }
                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (labelID == "usernameDiv") // Yser Menu DropDown
            {
                showUserAndEnvDropdownMenu();  // hideAllDropdownMenus() is called within this

                var menuDiv = document.getElementById('e1LogoutLink');

                if (menuDiv) {
                    menuDiv.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
            }
            else if (eventElement.id == "Role_downarrow") // Role 
            {
                showMenu('e1MRoleTable', event);
                hideAllMainMenus();
                hideAllBreadcrumbMenus();
                hideAllFavMenus();
                hideMenu('preferenceDiv', event);
                hideMenu('OPENAPPTABLE');
                hideRRMenu();
                hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);
                //In Case if the user has multiple Roles
                var roleList = document.getElementById("e1MRoleSelect");
                if (roleList != null) {
                    if (roleList.nodeName == "INPUT")
                        document.getElementById("e1MRoleOptions").focus();
                    else
                        roleList.focus();
                }
            }
            else if (eventElement.id == "preferences") // preferences 
            {
                showMenu('preferenceDiv', event);
                hideAllMainMenus();
                hideAllBreadcrumbMenus();
                hideAllFavMenus();
                hideMenu('OPENAPPTABLE');
                hideMenu('e1MRoleTable');
                hideRRMenu();
                hideMenu(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE);

                var preferenceDiv = document.getElementById('preferenceDiv');
                var sibling = (preferenceDiv.firstElementChild === undefined) ? preferenceDiv.firstChild.firstChild : preferenceDiv.firstElementChild.firstChild;
                if (sibling != null) {
                    sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
            }
            else if ((srcElement.id.indexOf("PreferencesTable") != -1 && srcElement.tagName == "TABLE") || srcElement.id == "PersonalizationTable" || srcElement.id == "ManageCHPTable" || isFlyout) {
                var sibling = (srcElement.nextElementSibling === undefined) ? srcElement.nextSibling : srcElement.nextElementSibling;

                if (sibling == null) {
                    if (srcElement.parentNode.nextElementSibling === undefined) {
                        sibling = srcElement.parentNode.nextSibling ? srcElement.parentNode.nextSibling.firstChild : null;
                    }
                    else if (srcElement.parentNode.nextElementSibling) {
                        // skip over the script - for edit pages link
                        if (srcElement.parentNode.nextElementSibling.nodeName == "SCRIPT") {
                            if (srcElement.parentNode.nextElementSibling.nextElementSibling) // if edit pages link exists
                                sibling = srcElement.parentNode.nextElementSibling.nextElementSibling.firstChild;
                        }
                        else {
                            sibling = srcElement.parentNode.nextElementSibling.firstChild;
                        }
                    }
                }

                if (sibling != null) {
                    if (sibling.focus) // Text object will not support focus().
                        sibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
                else {
                    var signoutLink = document.getElementById('e1LogoutLink');
                    if (signoutLink) {
                        signoutLink.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                    else if (gContainerId == E1URLFactory.prototype.CON_ID_WSRP) // for WSRP, we do not need to loop through preference menu for the consistency behavior as other top level menu for clear accessiblity behavior.
                    {
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
            }
            return false;

        case 38: //up arrow
            // move the Focus to the previous menuItem in the menu
            if (eventElement.id.indexOf('menuItemBc') == 0) {
                var foundElement;
                var sibling = eventElement.parentNode;
                while (sibling.previousSibling) {
                    sibling = sibling.previousSibling;
                    foundElement = sibling.firstChild;

                    if (foundElement != null) {
                        eventElement.className = "MenuNormal";
                        foundElement.focus();

                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
                return false;
            }
            else if (eventElement.id.substring(0, 8) == "menuItem" && eventElement.className == "HoverMenuItem") {
                var foundElement;
                var sibling = eventElement.parentNode.parentNode;
                if (!sibling.previousSibling)  // no previous sibling, focus on fast path
                {
                    var fastPathBox = document.getElementById(DDMENU.CONST.ELEMENT_TE_FAST_PATH_BOX);
                    if (fastPathBox && sibling.parentNode.parentNode.id == "e1MMenuRootInner") // only do it for the first level menu, but ignore it for other levels.
                    {
                        fastPathBox.focus();
                    }
                }
                while (sibling.previousSibling) {
                    sibling = sibling.previousSibling;
                    if (sibling.firstChild.id == "DROP_MANAGE_FAVORITES" || sibling.firstChild.id == "ADD_TO_FAVORITES" && sibling.firstChild.style.display != "none") {
                        foundElement = sibling.firstChild;
                        foundElement.className = "HoverMenuItem";
                    }
                    else {
                        foundElement = (isIE8OrEarlier) ? sibling.firstChild.firstChild : sibling.firstChild.firstChild.nextSibling;
                    }

                    if (foundElement != null && foundElement.nodeName == "TABLE") {
                        foundElement.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
                return false;
            }
            else if (eventElement.id.indexOf("RecRptsTable") != -1) {
                var preSibling = eventElement.previousSibling;
                if (preSibling != null && preSibling.nodeName == "TABLE") {
                    preSibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (eventElement.id.indexOf("OCLTR_") != -1) //open Apps
            {
                //set Focus on the previous open application
                MENUUTIL.processOpenApplicationList(-1, false);
                event.returnValue = false;
                event.cancelBubble = true;
                return false;
            }
            else if (srcElement.id == "e1LogoutLink") {
                var prefDiv = document.getElementById("preferenceDiv");
                if (prefDiv) {
                    var lastChild;
                    if (prefDiv.lastElementChild === undefined) {
                        lastChild = prefDiv.lastChild;
                        if (lastChild.nodeName == "SCRIPT")
                            lastChild = lastChild.previousSibling.firstChild;
                    }
                    else {
                        lastChild = prefDiv.lastElementChild;
                        if (lastChild.nodeName == "SCRIPT")
                            lastChild = lastChild.previousElementSibling.firstChild;
                    }
                    if (lastChild && lastChild.nodeName == "DIV") {
                        lastChild = lastChild.firstChild;
                    }
                    if (lastChild && lastChild.focus) {
                        lastChild.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
            }
            else if ((srcElement.id.indexOf("PreferencesTable") != -1 && srcElement.tagName == "TABLE") || srcElement.id == "PersonalizationTable" || srcElement.id == "ManageCHPTable" || isFlyout) {
                var preSibling = null;
                if (srcElement.previousSibling != null) {
                    preSibling = (srcElement.previousElementSibling === undefined) ? srcElement.previousSibling : srcElement.previousElementSibling;
                }
                else {
                    if (srcElement.parentNode.previousSibling != null) {
                        if (srcElement.parentNode.previousElementSibling === undefined) // IE8
                        {
                            // Beware of scenarios where we're looking at the first element.
                            if (srcElement.parentNode.previousSibling) {
                                preSibling = srcElement.parentNode.previousSibling.firstChild;
                            }
                        }
                        else {
                            // Beware of scenarios where we're looking at the first element.
                            if (srcElement.parentNode.previousElementSibling) {
                                // skip the script element
                                if (srcElement.parentNode.previousElementSibling.nodeName == "SCRIPT")
                                    preSibling = srcElement.parentNode.previousElementSibling.previousElementSibling.firstChild;
                                else
                                    preSibling = srcElement.parentNode.previousElementSibling.firstChild;
                            }
                        }
                    }
                }

                if (preSibling != null) {
                    if (preSibling.focus) preSibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                return false;
            }
            else if (srcElement.id == "ActionsSubMenuTable") {
                var preSibling = (srcElement.parentNode.parentNode.previousElementSibling === undefined) ? srcElement.parentNode.parentNode.previousSibling : srcElement.parentNode.parentNode.previousElementSibling;
                if (preSibling != null) {
                    var elemt = (preSibling.firstChild.firstElementChild === undefined) ? preSibling.firstChild.firstChild : preSibling.firstChild.firstElementChild;
                    elemt.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                return false;
            }
            else if (eventElement.className == "refreshWatchlistControl") {
                while (eventElement.parentNode.previousSibling) {
                    var preSibling = eventElement.parentNode.previousSibling;
                    if (preSibling != null && preSibling.nodeName == "DIV" && preSibling.className == "watchlistSectionHeading") {
                        preSibling.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                    else if (preSibling != null && preSibling.nodeName == "DIV" && preSibling.className == "watchlistMenuItem") {
                        var elemnt = preSibling.lastChild;
                        elemnt.focus();
                        event.returnValue = false;
                        event.cancelBubble = true;
                        return false;
                    }
                }
                return false;
            }
            else if (eventElement.className == "wlLabelWrapper") {
                var preSibling = eventElement.previousSibling;
                if (preSibling != null && preSibling.nodeName == "A" && preSibling.className == "refreshWatchlistControl") {
                    preSibling.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                return false;
            }
            else if (eventElement.className == "watchlistSectionHeading") {
                var preSibling = eventElement.parentNode.previousSibling;
                if (preSibling != null && preSibling.nodeName == "DIV" && preSibling.className == "watchlistDividerBetweenSections") {
                    var elemnt = preSibling.previousSibling.lastChild.lastChild;
                    elemnt.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                return false;
            }

            break;
        case leftArrow:

        case rightArrow:
            if (isCtrlDown(event)) {
                // find the flyout for this menu item
                var iconNode = eventElement.childNodes[0].id;
                var shortId = "";
                if (iconNode) {
                    shortId = iconNode.substring(1, iconNode.length);
                }
                else {
                    var parentNode = eventElement;
                    var foundIt = false;
                    while (parentNode.parentNode && !foundIt) {
                        parentNode = parentNode.parentNode;
                        if (parentNode.id == parentContainer.id) {
                            return false;
                        }
                        if (parentNode.id.length > 4 && parentNode.id.substring(0, 4) == "head") {
                            shortId = parentNode.id.substring(4, parentNode.id.length);
                            foundIt = true;
                        }
                    }
                }
                var popContentDivId = shortId + "pop";
                var elmID = "FlyOut" + shortId;

                displayTaskContextMenu(event, popContentDivId, "", elmID);

                var flyoutInnerTab = document.getElementById("flyoutInnertab").firstChild.firstChild.firstChild.firstChild;
                if (flyoutInnerTab != null) {
                    var contextMenuTab = flyoutInnerTab.firstChild.firstChild.firstChild.firstChild;
                    contextMenuTab.focus();
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                return false;
            }

        case 18:
            return true;
        case 16:
            return true;

        case 32: //space, we do not want to response space key on any sub menu, and also want to prevent it from bubbling to the root menu event handler
            if (srcElement.id == "TE_FAST_PATH_BOX") //fast path, just run the default action
            {
                return true;
            }
            event.returnValue = false;
            event.cancelBubble = true;
            break;

        case 13: //Enter
            {
                if (srcElement.id == "TE_FAST_PATH_BOX") //fast path, just run the default action
                {
                    return true;
                }
                else if (eventElement.id.indexOf('menuItemBc') == 0) {
                    BREADCRUMBS.updateSelectedBreadcrumb(breadcrumbNodeId);
                    return false;
                }
                else if (labelID && labelID.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_ROOT_ID_BASE) == 0) // a breadcrumb dropdown
                {
                    var menuRootId = labelID;
                    var containerLabelPos = labelID.indexOf(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_DROPDOWN_LABELITEM_ID_BASE);
                    if (containerLabelPos > 0) {
                        menuRootId = menuRootId.substring(0, (containerLabelPos));
                    }

                    BREADCRUMBS.loadMenu(breadcrumbNodeId, menuRootId);
                    showMenu(menuRootId, event, bcFocus);
                    event.returnValue = false;
                    event.cancelBubble = true;
                }
                else if (labelID == BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ARROW) // bc history dropdown
                {
                    var drpdown = document.getElementById(BREADCRUMBS.CONSTANTS.ELEMENT_BREADCRUMB_HISTORY_DROPDOWN_ROOT_ID_BASE)
                    if (drpdown) {
                        if (drpdown.className == "hideMenu") {
                            BREADCRUMBS.loadBreadCrumbsHistory(event);
                        }

                        var firstChildNodeElement = getFirstMenuItem(drpdown);
                        if (firstChildNodeElement) {
                            firstChildNodeElement.focus();
                            event.returnValue = false;
                            event.cancelBubble = true;
                            return false;
                        }
                    }
                }
                else if (eventElement.className == "refreshWatchlistControl") {
                    eventElement.onclick(event);
                    return false;
                }
                else if (eventElement.className == "wlLabelWrapper") {
                    var element = eventElement.parentNode;
                    element.onclick(event);
                    return false;
                }
                else if (eventElement.className == "watchlistSectionHeading") {
                    event.returnValue = false;
                    event.cancelBubble = true;
                    return false;
                }
                var anchor = eventElement.getElementsByTagName('a');
                var anchorId = (anchor[0] === undefined) ? "" : anchor[0].id;
                var strAnchor = anchorId.substring(0, anchorId.lastIndexOf("_"));
                //Enter Key pressed on open apps Dropdown
                if (strAnchor == "OCLHREF") {
                    srcElement.click();
                    stopEventBubbling(event);
                    return false;
                }
                else {
                    //Enterkey press on the Navigator,Fav menuitems
                    if (anchor[0] !== undefined) {
                        anchor[0].click();
                        stopEventBubbling(event);
                        return false;
                    }
                }

                //Enter key pressed breadcrumb history dropdown 


                //Enterkey press on Rec repts
                if (eventElement.id.indexOf("RecRptsTable") != -1) {
                    var cells = eventElement.getElementsByTagName('TD');
                    for (var i = 0; i < cells.length; i++) {
                        var cellId = cells[i].getAttribute("id");
                        if (cellId != null && cellId.indexOf("recRptsMenuItem") != -1)
                            cells[i].click();
                    }
                    stopEventBubbling(event);
                    return false;
                }
                else if (eventElement.id.indexOf("PreferencesTable") != -1) // hide the preference dropdown when user use enter key on any menuitem in this dropdown
                {
                    srcElement.click();
                    hideMenu(DDMENU.CONST.ELEMENT_PREFERENCE_DIV);
                    stopEventBubbling(event);
                    return false;
                }
                else if (eventElement.id == 'helpLinkDiv' || eventElement.id == 'PersonalizationTable' || eventElement.id == 'ManageCHPTable') {
                    srcElement.click();
                    stopEventBubbling(event);
                    return false;
                }
            }
        case 9: //Tab
            {
                if (isShiftDown(event)) {
                    var roleSelect = document.getElementById('e1MRoleSelect');
                    if (srcElement.id == "e1MRoleSelect" ||
                    (srcElement.id == "PreferencesTable1" && !roleSelect) ||
                    (srcElement.id == "PreferencesTable1" && roleSelect.type == "hidden")) // when shift-tabbing from roleSelect (or PreferenceTable1 when roleSelect is hidden), keep focus within user menu
                    {
                        var helpLink = document.getElementById('helpLinkDiv');
                        if (helpLink) {
                            helpLink.focus();
                            event.returnValue = false;
                            event.cancelBubble = true;
                            return false;
                        }
                    }
                }
                else {
                    if (srcElement.id == "helpLinkDiv") // when tabbing from helpLink, keep focus within user menu
                    {
                        var nextTab = document.getElementById('e1MRoleSelect');
                        if (!nextTab || nextTab.type == "hidden") // roleSelect is not there, or hidden
                        {
                            nextTab = document.getElementById('PreferencesTable1');
                        }

                        if (nextTab) {
                            nextTab.focus();
                            event.returnValue = false;
                            event.cancelBubble = true;
                            return false;
                        }
                    }
                }
            }
    }
    return true;
}

function manageFavMenuKeyDown(evt, element, namespace) {
    if (evt.keyCode == 13) //enter
    {
        hideAllDropdownMenus();
        if (namespace === undefined) {
            manageFavorites();
        }
        else {
            addFormToFavorites(namespace);
        }
    }
    else if (evt.keyCode == 27) //escape
    {
        hideAllDropdownMenus();
        dismissFloatingDiv();
        //set the Default Focus back on the favorites icon
        var favmenu = document.getElementById("drop_fav_menus");
        if (favmenu) {
            favmenu.focus();
        }
    }
    else if (evt.keyCode == 40) //down arrow
    {
        var parentDiv = element.parentNode.parentNode;
        var childNodes = parentDiv.childNodes;
        var focusElement = null;
        var firstVisibleSibling;
        if (namespace === undefined) {
            if (childNodes[1].childNodes[0].style.display == "none") {
                firstVisibleSibling = childNodes[2];
            }
            else {
                focusElement = childNodes[1].firstElementChild;
            }
        }
        else {
            firstVisibleSibling = childNodes[2];
        }
        if (firstVisibleSibling) {
            if (firstVisibleSibling.firstElementChild) {
                focusElement = firstVisibleSibling.firstElementChild.firstElementChild;
            }
        }

        if (focusElement != null) {
            focusElement.focus();
        }
    }
    else if (evt.keyCode == 38 && namespace !== undefined) //down arrow
    {
        var parentDiv = element.parentNode.parentNode;
        var childNodes = parentDiv.childNodes;
        var focusElement = focusElement = childNodes[0].firstElementChild;
        focusElement.focus();
    }
    stopEventBubbling(evt);
    return false;
}

function getLabelId(eventElement) {
    var labelID = null;
    // Additional Checks are required for Items in Watchlist Drop Down Menu and Recent Reports Menu since the child elements are only defined for Accessibility Users 
    if (eventElement.firstElementChild === undefined && eventElement.className != "watchlistSectionHeading" && eventElement.id != "drop_reportmenu" && eventElement.className != "refreshWatchlistControl") // IE8
    {
        labelID = eventElement.childNodes[0].id;
    }
    else if (eventElement.firstElementChild != null) {
        labelID = eventElement.firstElementChild.id;
    }
    else {
        labelID = "";
    }
    return labelID;
}