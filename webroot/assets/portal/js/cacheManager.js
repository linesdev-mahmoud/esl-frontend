var cacheManager = null;

// The Cache Manager precaches common resources in threads before they are required.
// This allows for resources that are known to be probably used to be cached in the
// browser before they are demanded.
// 
// The order in which they are processed is ascending, so those resources that are
// required first should be inserted at the beginning of the array. For user experience
// it is good practice to insert the javascript required at the end of the array before the
// mouse over images. This prevents the 'chunking' of the page load (i.e. images slowly turning up.)
// The request for resources are run in threads to allow for normal user interaction to 
// continue while the cache process is running
function cacheManagerObject(prependArray) {
    this.cacheComponents = null;
    if (prependArray)
        this.cacheComponents = prependArray;
    else
        this.cacheComponents = new Array();

    // These are the form resources to be pre-cached. They will be cached in the order
    // listed.
    for (var i = 0; i < window._jdeCachedResources.length; i++) {
        this.cacheComponents.push(window._jdeCachedResources[i]);
    }

    this.currentCacheComponent = 0;
    // Idealy this should match the MaxConnectionsPerServer setting for the Browser
    // The default for FireFox is 8, for IE is 2 (can be changed by the user).
    this.concurrentConnections = 4;
    this.xmlHttpReq = new Array(this.concurrentConnections);
    this.handleCacheResponse = window.handleCacheResponse;
    this.isWebkit = navigator.userAgent.toUpperCase().indexOf("WEBKIT") > -1;

    this.processPreLoad = function (connectionId) {
        this.getXMLHttpReq(connectionId);

        try {
            var processComponent = this.cacheComponents[this.currentCacheComponent++];
            if (processComponent) {
                var localConnection = this.xmlHttpReq[connectionId];
                localConnection.open("GET", processComponent, true);
                var handlerFunction = this.handleCacheResponse(localConnection, connectionId);
                localConnection.onreadystatechange = handlerFunction;
                //Webkit browsers through Javascript error while setting connection header
                if (!this.isWebkit) {
                    localConnection.setRequestHeader("Connection", "close");
                }
                localConnection.send(null);
            }
            else {
                this.xmlHttpReq[connectionId] = null;
            }
        } catch (ex) {
            this.xmlHttpReq[connectionId] = null;
        }
    }

    this.startProcessPreLoad = function () {
        for (var i = this.concurrentConnections; i >= 0; i--) {
            // create a thread for each of the connections
            window.setTimeout("window.cacheManager.processPreLoad(" + i + ")", 0);
        }

    }

    this.getXMLHttpReq = function (connectionId) {
        if (this.xmlHttpReq[connectionId] == null) {
            // code for FireFox, Safari, etc.
            if (window.XMLHttpRequest) {
                try {
                    this.xmlHttpReq[connectionId] = new XMLHttpRequest();
                } catch (ex) { }
            }
            // For IE
            else if (window.ActiveXObject) {
                try {
                    // Version 3
                    this.xmlHttpReq[connectionId] = new ActiveXObject("MSXML2.XMLHTTP");
                }
                catch (ex) {
                    try {
                        // Older Version
                        this.xmlHttpReq[connectionId] = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    catch (ex) {
                        this.xmlHttpReq[connectionId] = null;
                    }
                }
            }
            else {
                alert("Your browser does not support XMLHTTP.")
            }
        }
    }
}

function handleCacheResponse(req, connectionId) {
    return function () {
        if (req.readyState == 4) {
            // go and get some more
            window.setTimeout("window.cacheManager.processPreLoad(" + connectionId + ")", 0);
        }
    }
}