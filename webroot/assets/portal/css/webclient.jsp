/*
# Seven-character color codes:
mainColorCode=#4A598C
mainColorTextColorCode=#FFFFFF
pageBackgroundColorCode=#FFFFFF
pageTextColorCode=#333333
topBackgroundColorCode=#FAFAFA
topTextColorCode=#FFFFFF
topHeadingTextColorCode=#454545
borderColorCode=#D9DFE3
borderTextColorCode=#FFFFFF
activeTabColorCode=#0572CE
activeTabTextColorCode=#145C9E
inactiveTabColorCode=#D9E3EC
inactiveTabTextColorCode=#145C9E
nav1ColorCode=#C9DAEB
nav1TextColorCode=#000000
nav2ColorCode=#4A71AD
nav2TextColorCode=#FFFFFF
nav3ColorCode=#F5F5F5
nav3TextColorCode=#454545
nav4ColorCode=#FFFFFF
nav4TextColorCode=#000000
nav5ColorCode=#F7F8F9
nav5TextColorCode=#000000
tableControlColorCode=#EDF6FF
tableControlTextColorCode=#000000
tableHeaderColorCode=#CFE0F1
tableHeaderTextColorCode=#343434
tableCellColorCode=#FFFFFF
tableCellTextColorCode=#333333
tableCellAltColorCode=#D3DCE8
tableCellAltTextColorCode=#000000
tableSelectedHeaderColorCode=#939CBA
tableSelectedHeaderTextColorCode=#000000
tableSelectedColorCode=#C9CEDD
tableSelectedTextColorCode=#000000
linkColorCode=#333333
linkVisitedColorCode=#333333
linkHoverColorCode=#CC0033
linkHoverOnDarkColorCode=#FCFC5B
buttonColorCode=#FCFEFE
buttonTextColorCode=#586072
# URLs:
leftNavBackgroundImageURL=
pageBackgroundImageURL=/jde/share/images/bodybg.png
topBackgroundImageURL=/jde/share/images/title-banner.jpg
logoImageURL=/jde/share/images/fndsscorp.gif
treeBulletImageURL=/jde/share/images/treebullet.gif
treeBulletActiveImageURL=/jde/share/images/treebullet-ondark.gif
reqdIndImageURL=/jde/share/images/required.gif
reqdIndDarkImageURL=/jde/share/images/required-dark.gif
ascDescArrowImageURL=/jde/share/images/ascdescarrow.gif
ascDescArrowDarkImageURL=/jde/share/images/ascdesarrow.png
# Dimensions:
logoImageWidth=410
logoImageHeight=30
topLinksOffset=25
# Fonts:
fontFamily="Helvetica Neue", Helvetica, Arial, sans-serif
fontSizeHuge=18px
fontSizeBig=14px
fontSizeMain=11px
fontSizeLittle=8pt
fontSizeFinePrint=7pt
*/
.pagelogo {
background-image: url('/jde/share/images/fndsscorp.gif');
}
.RIMenuTable{
background-image: url('/jde/share/images/RI_menuBG.gif');
}
.RIMenuTableDynamic{
background-image: url('/jde/share/images/RI_menuBGDynamic.gif');
}
.RIMenuTableHover{
background-image: url('/jde/share/images/RI_menuBGHover.gif');
}
.RIActionHover>div {
background-image: url('/jde/share/images/RI_menuBGHover.gif');
}
.RIPalletteRowHover {
background-image: url('/jde/share/images/RI_paletteBGHover.gif');
}
.RIDragable{
cursor: url('/jde/share/images/openhand.cur'), default;
}
.RIDragging{
cursor: url('/jde/share/images/closedhand.cur'), default;
}
.RITab{
background-image: url('/jde/share/images/rcux/button_bg_ena.png');
}
.RITabHover{
background-image: url('/jde/share/images/rcux/button_bg_ovr.png');
}
.RIFloadingContainer{
background-image: url('/jde/share/images/RI_menuBGHover.gif');
}
.RIMenuTableDesign{
background-image: url('/jde/share/images/RI_menuBGDesign.gif');
}
.RIResizeVertical{
background-image: url('/jde/share/images/RI_resizeEW.png');
}
.RIResizeVerticalHover{
background-image: url('/jde/share/images/RI_resizeEWHover.png');
}
.RIResizeVerticalMove{
background-image: url('/jde/share/images/RI_resizeEWMove.png');
}
.RIResizeVertical{
background-image: url('/jde/share/images/RI_resizeNS.png');
}
.RIResizeHorizontalHover{
background-image: url('/jde/share/images/RI_resizeNSHover.png');
}
.RIResizeHorizontalMove{
background-image: url('/jde/share/images/RI_resizeNSMove.png');
}
.RISourceBG{
background-image: url('/jde/share/images/RI_SourceBG.gif');
}
.RILogicBG{
background-image: url('/jde/share/images/RI_LogicBG.gif');
}
.RIRuleBG{
background-image: url('/jde/share/images/RI_RuleBG.gif');
}
.RITriggerBG{
background-image: url('/jde/share/images/RI_TriggerBG.gif');
}
.RICompositeHoverBG{
background-image: url('/jde/share/images/RI_paletteBGHover.gif');
}
.LVCafeOneCursorRow, .CafeOneCursorRow td{
background-image: url('/jde/share/images/RI_menuBGDynamic.gif') !important;
}
.e1toolbar {
background-image: url('/jde/share/images/hbg-bar.gif');
}
.mainnav TD.mainnavcell {
background-image: url('/jde/share/images/title-banner.jpg');
}
.GridHeaderReqdInd {
background-image: url('/jde/share/images/required-dark.gif');
}
.ShowPopup {
background-image: url('/jde/share/images/required-dark.gif');
}
.GridHeaderAscDescArrow {
background-image: url('/jde/share/images/ascdesarrow.png');
}
/* Leaving these next three classes in place in case they are still coming from app specs */
.tabCenterTableSubBg {
    background-image: url('/jde/share/images/tab_mid_a.png');
    background-repeat: repeat-x;
}
.tabCenterTableSubDimBg {
    background-image: url('/jde/share/images/tab_mid_n.png');
    background-repeat: repeat-x;
}
.tabCenterTableDisabledBg {
    background-image: url('/jde/share/images/tab_mid_d.png');
    background-repeat: repeat-x;
}
.SubForm_header-undisclosed-icon-style {
    background-image: url('/jde/share/images/disclosecollapsed_ena.png');
}
.af_progressIndicator_indeterminate {
    background-image: url('/jde/share/images/indeterminate_progress_bar.gif');
}
.InYourFaceErrorBg {
background-image: url('/jde/share/images/error-header-bkgd.gif');
}
.InYourFaceWarningBg {
background-image: url('/jde/share/images/warn-header-bkgd.gif');
}
.treebullet {
background-image: url('/jde/share/images/treebullet.gif');
}
.treeactiveitem .treebullet {
background-image: url('/jde/share/images/treebullet-ondark.gif');
}
.textfield {
background-image: url('/jde/share/images/rcux/inputText_bg.png');
}
.iosLoginPageTextField {
background-image: url('/jde/share/images/rcux/inputText_bg.png');
}
.iosLoginPageButtonStyle {
background-image: url('/jde/share/images/btn-bg.gif');
}
.yearSelImgUp {
background-image: url('/jde/share/images/alta/up-arrow-ena.png');
background-repeat: no-repeat;
background-position: center;
}
.yearSelImgDn {
background-image: url('/jde/share/images/alta/down-arrow-ena.png');
background-repeat: no-repeat;
background-position: center;
}
.yearSelImgUp:HOVER {
background-image: url('/jde/share/images/alta/up-arrow-ovr.png');
}
.yearSelImgDn:HOVER {
background-image: url('/jde/share/images/alta/down-arrow-ovr.png');
}
.yearSelImgUp:ACTIVE {
background-image: url('/jde/share/images/alta/up-arrow-dwn.png');
}
.yearSelImgDn:ACTIVE {
background-image: url('/jde/share/images/alta/down-arrow-dwn.png');
}
.customNodeFlyOut {
background-image: url('/jde/share/images/treeflyouttrigger.gif');
}
.customNodeFlyOut_rtl {
background-image: url('/jde/share/images/treeflyouttrigger_rtl.gif');
}
.customNodeFlyOutHidden {
background-image: url('/jde/share/images/treeflyouttrigger-hidden.gif');
}
.customNodeFlyOutHidden_rtl {
background-image: url('/jde/share/images/treeflyouttrigger-hidden_rtl.gif');
}
.treeactiveitem .customNodeFlyOutHidden {
background-image: url('/jde/share/images/treeflyouttrigger.gif');
}
.treeactiveitem .customNodeFlyOutHidden_rtl {
background-image: url('/jde/share/images/treeflyouttrigger_rtl.gif');
}
.treebulletbatch {
background-image: url('/jde/share/images/icon-batch.gif');
}
.treebulletother {
background-image: url('/jde/share/images/icon-others.gif');
}
.loginLogoTABLE {
background-image: url('/jde/share/images/login/title-banner.jpg');
}
.loginTopLinesTD {
background-image: url('/jde/share/images/login/top-lines.gif');
}
.loginPeopleTD {
background-image: url('/jde/share/images/login/people.jpg');
}
.loginGlobalTopTD {
background-image: url('/jde/share/images/login/global-top.jpg');
}
.loginGlobalTD {
background-image: url('/jde/share/images/login/global.jpg');
}
.loginLightBlueTable {
background-image: url('/jde/share/images/login/lightblue-back.jpg');
}
.MenuNormal .ManageFavIcon {
    background-image: url('/jde/share/images/managefavorites_ena.png');
}
.MenuNormal .AddToFavIcon {
    background-image: url('/jde/share/images/addfavorites_ena.png');
}
.HoverMenuItem .ManageFavIcon {
    background-image: url('/jde/share/images/managefavorites_ena.png');
}
.HoverMenuItem .AddToFavIcon {
    background-image: url('/jde/share/images/addfavorites_ena.png');
}
.GenericAppIcon {
    background-image: url('/jde/share/images/menu_default_app.png');
}
.GenericReportIcon {
    background-image: url('/jde/share/images/menu_default_report.png');
}
.GenericOVRIcon {
    background-image: url('/jde/share/images/menu_default_ovr.png');
}
.RecRptsAppIcon {
    background-image: url('/jde/share/images/menu_wsj.png');
}
.ReportDoneIcon {
background-image: url('/jde/share/images/rpt_done_green.png');
}
.ReportErrorIcon {
background-image: url('/jde/share/images/rpt_error_red.png');
}
.ReportProcessIcon {
background-image: url('/jde/share/images/rpt_inprogress.gif');
}
.ReportQueueIcon {
background-image: url('/jde/share/images/rpt_waiting.png');
}
.ReportWaitIcon {
background-image: url('/jde/share/images/rpt_waiting_orange.png');
}
div#carousel.caroBottom
{
    background-image: url('/jde/share/images/carousel/caroBottomTopBorder.png');
    background-repeat:repeat-x;
    background-position:top;
}
div#carousel.caroTop
{
    background-image: url('/jde/share/images/carousel/caroBottomTopBorder_flipped.png');
    background-repeat:repeat-x;
    background-position:bottom;
}

div.caroTop.caroExpanded div#caroBar div.caroBarArrow,
div.caroBottom.caroCollapsed div#caroBar div.caroBarArrow
{
  background-image: url('/jde/share/images/carousel/splitter_arrow_up.png');
  background-position: center;
  background-repeat: no-repeat;
}
div.caroBottom.caroExpanded div#caroBar div.caroBarArrow,
div.caroTop.caroCollapsed div#caroBar div.caroBarArrow
{
  background-image: url('/jde/share/images/carousel/splitter_arrow_down.png');
  background-position: center;
  background-repeat: no-repeat;
}
div.caroTop.caroExpanded div#caroBar div:hover.caroBarArrow,
div.caroBottom.caroCollapsed div#caroBar div:hover.caroBarArrow
{
  background-image: url('/jde/share/images/carousel/splitter_arrow_up_hover.png');
}
div.caroBottom.caroExpanded div#caroBar div:hover.caroBarArrow,
div.caroTop.caroCollapsed div#caroBar div:hover.caroBarArrow
{
  background-image: url('/jde/share/images/carousel/splitter_arrow_down_hover.png');
}
div.caroLeft.caroExpanded div#caroBar,div.caroRight.caroCollapsed div#caroBar
{
  background-image: url('../../jde/img/splitter_left.png');
  background-position: center;
  background-repeat: no-repeat;
}
div.caroRight.caroExpanded div#caroBar,div.caroLeft.caroCollapsed div#caroBar
{
  background-image: url('/jde/share/images/carousel/splitter_right.png');
  background-position: center;
  background-repeat: no-repeat;
}
div.caroLeft.caroExpanded div:hover#caroBar,div.caroRight.caroCollapsed div:hover#caroBar
{
  background-image: url('/jde/share/images/carousel/splitter_left_hover.png');
}
div.caroRight.caroExpanded div:hover#caroBar,div.caroLeft.caroCollapsed div:hover#caroBar
{
  background-image: url('/jde/share/images/carousel/splitter_right_hover.png');
}

div.listArrow
{
    background-image: url('../../jde/img/disclosure_arrow_ena.png');
    background-repeat: no-repeat;
    background-position: center;
}
div:hover.listHeader div.listArrow
{
    background-image: url('../../jde/img/disclosure_arrow_ovr.png');
}
div:active.listHeader div.listArrow
{
    background-image: url('../../jde/img/disclosure_arrow_dwn.png');
}
/*
div.listArrowBox
{
    background-image:url('/jde/share/images/carousel/disclosure_box_ena.png');
}
div:hover.listHeader div.listArrowBox
{
    background-image:url('/jde/share/images/carousel/disclosure_box_ovr.png');
}
div:active.listHeader div.listArrowBox
{
    background-image:url('/jde/share/images/carousel/disclosure_box_dwn.png');
}
*/
div#showHideCaroTabs>div.caroTabM>span
{
    background-image: url('../../jde/img/showHideTabs_left.png');
    background-repeat: no-repeat;
    background-position: center;
}
html[dir="rtl"] div#showHideCaroTabs>div.caroTabM>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_right.png');
}
div:hover#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_left_hover.png');
}
html[dir="rtl"] div:hover#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_right_hover.png');
}
div:active#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_left_mousedown.png');
}
html[dir="rtl"] div:active#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_right_mousedown.png');
}
div#caroTabBar[expanded="false"]>div#showHideCaroTabs>div.caroTabM>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_right.png');
}
html[dir="rtl"] div#caroTabBar[expanded="false"]>div#showHideCaroTabs>div.caroTabM>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_left.png');
}   
div#caroTabBar[expanded="false"]>div:hover#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_right_hover.png');
}
html[dir="rtl"] div#caroTabBar[expanded="false"]>div:hover#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_left_hover.png');
}
div#caroTabBar[expanded="false"]>div:active#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_right_mousedown.png');
}
html[dir="rtl"] div#caroTabBar[expanded="false"]>div:active#showHideCaroTabs>div>span
{
    background-image: url('/jde/share/images/carousel/showHideTabs_left_mousedown.png');
}
a.panControl.up
{
    background-image: url('../../jde/img/carat_up_ena.png');
    background-position: center;
    background-repeat: no-repeat;
}
a.panControl.down
{    
    background-image: url('../../jde/img/carat_down_ena.png');
    background-position: center;
    background-repeat: no-repeat;
}
a:active.panControl.up
{
    background-image: url('jde/img/carat_up_act.png');
}
a:active.panControl.down
{
    background-image: url('jde/img/carat_down_act.png');
}
a.panControl.left
{
    background-image: url('jde/img/carat_left_ena.png');
    background-position: center;
    background-repeat: no-repeat;
}
a.panControl.right
{
    background-image: url('/jde/share/images/carousel/carat_right_ena.png');
    background-position: center;
    background-repeat: no-repeat;
}
a:active.panControl.left
{
    background-image: url('/jde/share/images/carousel/carat_left_act.png');
}
a:active.panControl.right
{
    background-image: url('/jde/share/images/carousel/carat_right_act.png');
}
a.recentReportsRefreshControl
{
    background-image: url('../../jde/img/refresh_sm_ena.png');
    background-repeat: no-repeat;
    background-position: center;
}
a.refreshWatchlistControl
{
    background-image: url('/jde/share/images/refresh_wl_ena.png');
}
a.closeFolderControl
{
    background-image: url('/jde/share/images/carousel/close_enabled.png');
    background-repeat: no-repeat;
    background-position: center;
}
a.backFolderControl
{
    background-image: url('/jde/share/images/carousel/back_arrow_ena.png');
    background-repeat: no-repeat;
    background-position: center;
}
html[dir="rtl"] a.backFolderControl
{
    background-image: url('/jde/share/images/carousel/back_arrow_ena_rtl.png');
}
a.backFolderControl[ctlDisabled="true"]
{
    background-image: url('/jde/share/images/carousel/back_arrow_dis.png');
    display: none;
}
a:hover.recentReportsRefreshControl
{
    background-image: url('/jde/share/images/carousel/refresh_sm_ovr.png');
}
a:hover.refreshWatchlistControl, a:active.refreshWatchlistControl
{
    background-image: url('/jde/share/images/refresh_wl_mo.png');
}
[update="inProgress"] a.refreshWatchlistControl 
{
    background-image: url('/jde/share/images/refresh_wl_updating.gif');
}
[update="pending"] a.refreshWatchlistControl 
{
    background-image: url('/jde/share/images/watchlist_pending_update.png');
}
[update="failed"] a.refreshWatchlistControl, [update="interrupted"] a.refreshWatchlistControl
{
    background-image: url('/jde/share/images/watchlist_update_failed.png');
}
a:hover.closeFolderControl
{
    background-image: url('/jde/share/images/carousel/close_mouseover.png');
}
div.caroTab[active="true"] a:hover.backFolderControl,
div.caroList a:hover.backFolderControl
{
    background-image: url('/jde/share/images/carousel/back_arrow_ovr.png');
}
html[dir="rtl"] div.caroTab[active="true"] a:hover.backFolderControl,
html[dir="rtl"] div.caroList a:hover.backFolderControl
{
    background-image: url('/jde/share/images/carousel/back_arrow_ovr_rtl.png');
}
a:hover.backFolderControl[ctlDisabled="true"]
{
    background-image: url('/jde/share/images/carousel/back_arrow_dis.png');
}
a:active.recentReportsRefreshControl
{
    background-image: url('/jde/share/images/carousel/refresh_sm_dwn.png');
}
a:active.closeFolderControl
{
    background-image: url('/jde/share/images/carousel/close_mousedown.png');
}
div.caroTab[active="true"] a:active.backFolderControl,
div.caroList a:active.backFolderControl
{
    background-image: url('/jde/share/images/carousel/back_arrow_dwn.png');
}
html[dir="rtl"] div.caroTab[active="true"] a:active.backFolderControl,
html[dir="rtl"] div.caroList a:active.backFolderControl
{
    background-image: url('/jde/share/images/carousel/back_arrow_dwn_rtl.png');
}
a:active.backFolderControl[ctlDisabled="true"]
{
    background-image: url('/jde/share/images/carousel/back_arrow_dis.png');
}
div.caroTab[active="true"] div.caroTabM
{
    background-image:url('../../jde/img/caroTab_active.png');  
    background-repeat: no-repeat;
    background-position: center bottom;
}
div.caroTop div.caroTab[active="true"] div.caroTabM
{
    background-image:url('/jde/share/images/carousel/caroTab_active_flipped.png');
    background-repeat: no-repeat;
    background-position: center top;
}
div.tab_left_end
{
  background-image:url('/jde/share/images/welcome/left-end-tabbar.png');
}
div.tab_right_end
{
  background-image:url('/jde/share/images/welcome/right-end-tabbar.png');
}
div#tab0>div#testTab
{
  background-image: url('/jde/share/images/welcome/close_ena.png');
}
div#tab0>div#testTab:hover
{
  background-image: url('/jde/share/images/welcome/close_ovr.png');
}
div.panButton.panButtonSmall.panButtonLeft.panButtonEnabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_l_ena.png');
}
div:hover.panButton.panButtonSmall.panButtonLeft.panButtonEnabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_l_ovr.png');
}
div:active.panButton.panButtonSmall.panButtonLeft.panButtonEnabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_l_dwn.png');
}
div.panButton.panButtonSmall.panButtonLeft.panButtonDisabled,
div:active.panButton.panButtonSmall.panButtonLeft.panButtonDisabled,
div:hover.panButton.panButtonSmall.panButtonLeft.panButtonDisabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_l_dis.png');
}
div.panButton.panButtonSmall.panButtonRight.panButtonEnabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_r_ena.png');
}
div:hover.panButton.panButtonSmall.panButtonRight.panButtonEnabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_r_ovr.png');
}
div:active.panButton.panButtonSmall.panButtonRight.panButtonEnabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_r_dwn.png');
}
div.panButton.panButtonSmall.panButtonRight.panButtonDisabled,
div:hover.panButton.panButtonSmall.panButtonRight.panButtonDisabled,
div:active.panButton.panButtonSmall.panButtonRight.panButtonDisabled
{
  background-image: url('/jde/share/images/alta/e1pages_conveyor/conv_r_dis.png');
}
div.showHideChpTabs>div
{
   background-image: url('/jde/share/images/carousel/splitter_arrow_up.png');
   background-repeat: no-repeat;
   background-position: center center;
}
div.showHideChpTabs>div:hover
{
   background-image: url('/jde/share/images/carousel/splitter_arrow_up_hover.png');
}
div.showHideChpTabs>div:active
{
  background-image: url('/jde/share/images/carousel/splitter_arrow_up.png');
}
div#wcHolder[expanded="false"]>div.showHideChpTabs>div
{
   background-image: url('/jde/share/images/carousel/splitter_arrow_down.png');
}
div#wcHolder[expanded="false"]>div.showHideChpTabs>div:hover
{
   background-image: url('/jde/share/images/carousel/splitter_arrow_down_hover.png');
}
div#wcHolder[expanded="false"]>div.showHideChpTabs>div:active
{
   background-image: url('/jde/share/images/carousel/splitter_arrow_down.png');
}
div.tab.activeTab
{
   background-image: url('/jde/share/images/e1pages_selection_arrow.png');
}
button#drawButton
{
    background-image: url('/jde/share/images/moDraw.png');
    background-size: 18px 18px;
    background-repeat: no-repeat;
    background-position:center;
}

button#eraserButton
{
    background-image: url('/jde/share/images/moEraseDrawing.png');
    background-size: 18px 18px;
    background-repeat: no-repeat;
    background-position:center;
}

button#clearButton
{
    background-image: url('/jde/share/images/moClearDrawing.png');
    background-size: 18px 18px;
    background-repeat: no-repeat;
    background-position:center;
}

.dropdown-icon-style {
    background-image: url('/jde/share/images/alta/dropdown_ena.png');
}
#drop_home
{
    background-image: url('../../jde/img/home.png');
}
#drop_home.hover
{
    background-image: url('/jde/share/images/alta/mainmenu/home_ovr.png');
}
.Nav_Navigator_icon
{
    background-image: url('../../jde/img/navigator.png');
}
.Nav_Navigator_icon.hover,
.Nav_Navigator_icon[active="true"]
{
    background-image: url('/jde/share/images/alta/mainmenu/navigator_ovr.png');
}
.Nav_OpenApp_icon
{
    background-image: url('../../jde/img/open-app.png');
}
.Nav_OpenApp_icon.hover,
.Nav_OpenApp_icon[active="true"]
{
    background-image: url('/jde/share/images/alta/mainmenu/open-app_ovr.png');
}
.Nav_RecentReports_icon
{
    background-image: url('../../jde/img/recent-reports.png');
}
.Nav_RecentReports_icon.hover,
.Nav_RecentReports_icon[active="true"]
{
    background-image: url('../../jde/img/recent-reports_ovr.png');
}
.Nav_Favorites_icon
{
    background-image: url('../../jde/img/favorites.png');
}
.Nav_Favorites_icon.hover,
.Nav_Favorites_icon[active="true"]
{
    background-image: url('/jde/share/images/alta/mainmenu/favorites_ovr.png');
}
.Nav_Watchlists_icon
{
    background-image: url('../../jde/img/watchlist.png');
}
.Nav_Watchlists_icon.hover,
.Nav_Watchlists_icon[active="true"]
{
    background-image: url('/jde/share/images/alta/mainmenu/watchlist_ovr.png');
}
div#fastPathContainer a
{
    background-image: url('/jde/share/images/alta/go_arrow_ena.png');
}
div#fastPathContainer a:hover, div#fastPathContainer a:focus
{
    background-image: url('/jde/share/images/alta/go_arrow_ovr.png');
}
div#fastPathContainer a:active
{
    background-image: url('/jde/share/images/alta/go_arrow_down.png');
}
html[dir="rtl"] div#fastPathContainer a
{
    background-image: url('/jde/share/images/alta/go_arrow_ena_rev.png');
}
html[dir="rtl"] div#fastPathContainer a:hover, html[dir="rtl"] div#fastPathContainer a:focus
{
    background-image: url('/jde/share/images/alta/go_arrow_ovr_rev.png');
}
html[dir="rtl"] div#fastPathContainer a:active
{
    background-image: url('/jde/share/images/alta/go_arrow_down_rev.png');
}
div#fastPathFieldAndButtonDivider
{
    background-image: url('/jde/share/images/alta/fast-path_pipe.png');
}
#rolesIcon
{
    background-image: url('/jde/share/images/alta/mainmenu/roles.png');
}
#personalizationIcon
{
    background-image: url('/jde/share/images/alta/mainmenu/personalize.png');
}
#logoutIcon
{
    background-image: url('/jde/share/images/alta/mainmenu/sign-out.png');
}
div:hover>#logoutIcon
{
    background-image: url('/jde/share/images/alta/mainmenu/sign-out_ovr.png');
}
#helpIcon
{
    background-image: url('../../jde/img/mainmenu/help.png');
}
div:hover>#helpIcon
{
    background-image: url('../../jde/img/help_ovr.png');
}
.loginBodyAuto
{
    background-image: url('../../jde/img/watermark-oracle.png');
}
div#userSessionDropdownArrow
{
    background-image: url('../../jde/img/user-drop-down.png');
}
div#userAndEnvContainer[menuExpanded="true"] div#userSessionDropdownArrow
{  
    /* TODO: If we can come up with an expanded state, put it here */  
    /* background-image: url('/jde/share/images/alta/mainmenu/user-drop-down_down.png'); */
}
div:active>div#userSessionDropdownArrow
{
    background-image: url('../../jde/img/user-drop-down_down.png');
}
div:hover>div#userSessionDropdownArrow
{
    /* TODO: Roger, remove this if you decide no hover state */
    /* background-image: url('/jde/share/images/alta/mainmenu/user-drop-down_ovr.png'); */
}
a#newWindowHelpButton
{
    background-image: url('../../jde/img/help_grey_ena.png');
}
a:hover#newWindowHelpButton
{
    background-image: url('../../jde/img/help_grey_ovr.png');
}
a#newWindowBackToNavButton
{
    background-image: url('../../jde/img/back-to-nav_ena.png');
}
a:hover#newWindowBackToNavButton
{
    background-image: url('../../jde/img/back-to-nav_ovr.png');
}
div.nav_roleChooser_icon
{
    background-image: url('../../jde/img/roles_portal.png');
}
div:hover.nav_roleChooser_icon
{
    background-image: url('../../jde/img/roles_portal_ovr.png');
}
div.e1MenuBarItem#preferences
{
    background-image: url('../../jde/img/personalize_portal.png');   
}
div:hover.e1MenuBarItem#preferences
{
    background-image: url('../../jde/img/personalize_portal_ovr.png');   
}